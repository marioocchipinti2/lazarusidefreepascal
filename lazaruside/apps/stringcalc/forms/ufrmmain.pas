unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, ComCtrls,
  StdCtrls, Buttons, ClipBrd,
  umlcstrings,
  ResStrs, ufrmabout,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnClipboardCopy: TBitBtn;
    btnRemoveChar: TBitBtn;
    btnReplaceCharByChar: TBitBtn;
    btnReplaceCharByStr: TBitBtn;
    btnReplaceStrByStr: TBitBtn;
    btnRemoveChars: TBitBtn;
    btnTrim: TBitBtn;
    btnLength: TBitBtn;
    btnAbout: TBitBtn;
    btnExit: TBitBtn;
    btnUnTrim: TBitBtn;
    btnTrimLeft: TBitBtn;
    btnSQuote: TBitBtn;
    btnDQuote: TBitBtn;
    btnUnTrimLeft: TBitBtn;
    btnTrimRight: TBitBtn;
    btnConcat: TBitBtn;
    btnUnTrimRight: TBitBtn;
    btnReverse: TBitBtn;
    edSourceA: TEdit;
    edDest: TEdit;
    edSourceB: TEdit;
    edSourceC: TEdit;
    lblSourceA: TLabel;
    lblDest: TLabel;
    lblSourceB: TLabel;
    lblSourceC: TLabel;
    pnlCenter: TPanel;
    pnlTop: TPanel;
    pnlMain: TPanel;
    stbarMain: TStatusBar;
    procedure btnAboutClick(Sender: TObject);
    procedure btnClipboardCopyClick(Sender: TObject);
    procedure btnConcatClick(Sender: TObject);
    procedure btnDQuoteClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnLengthClick(Sender: TObject);
    procedure btnRemoveCharClick(Sender: TObject);
    procedure btnRemoveCharsClick(Sender: TObject);
    procedure btnReplaceCharByCharClick(Sender: TObject);
    procedure btnReplaceStrByStrClick(Sender: TObject);
    procedure btnReverseClick(Sender: TObject);
    procedure btnSQuoteClick(Sender: TObject);
    procedure btnReplaceCharByStrClick(Sender: TObject);
    procedure btnTrimClick(Sender: TObject);
    procedure btnTrimLeftClick(Sender: TObject);
    procedure btnTrimRightClick(Sender: TObject);
    procedure btnUnTrimClick(Sender: TObject);
    procedure btnUnTrimLeftClick(Sender: TObject);
    procedure btnUnTrimRightClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private

  protected

    procedure LoadValues();
    procedure LoadStrings();

  public

  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure TfrmMain.LoadValues();
//var AppPath, ConfigFilename: string; F: TextFile;
begin
//  AppPath := ParamStr(0);
//  //@to-do: folder sep.
//  ConfigFilename := ExtractFileDir(AppPath) + DirectorySeparator + 'ExtReplacer.txt';
//
//  if (umlcossys.FileFound(ConfigFilename)) then
//  begin
//    System.Assign(F, ConfigFilename);
//    System.Reset(F);
//
////    if not System.EoF(F)
////      then System.ReadLn(F, WindowsFolder);
//
//    System.Close(F);
//  end else
//  begin
////    WindowsFolder := 'C:\WINDOWS\SYSTEM';
//  end
end;

procedure TfrmMain.LoadStrings();
begin
  Self.Caption := resTfrMain_Caption;
  Application.Title := resTfrMain_Caption;

  btnExit.Caption    := resbtnExit_Caption;
  btnAbout.Caption   := resbtnAbout_Caption;

  lblSourceA.Caption :=
    PadPosfixCopy(reslblSourceA_Caption, ':');
  lblSourceB.Caption :=
    PadPosfixCopy(reslblSourceB_Caption, ':');
  lblSourceC.Caption :=
    PadPosfixCopy(reslblSourceC_Caption, ':');

  lblDest.Caption   :=
    PadPosfixCopy(reslblDest_Caption, ':');

  btnClipboardCopy.Caption   :=
    resbtnClipboardCopy_Caption;

  btnLength.Caption   :=
    resbtnLength_Caption;
  btnSQuote.Caption   :=
    resbtnSQuote_Caption;
  btnDQuote.Caption   :=
    resbtnDQuote_Caption;

  btnTrimLeft.Caption   :=
    resbtnTrimLeft_Caption;
  btnTrimRight.Caption   :=
    resbtnTrimRight_Caption;
  btnTrim.Caption   :=
    resbtnTrim_Caption;

  btnUnTrimLeft.Caption   :=
    resbtnUnTrimLeft_Caption;
  btnUnTrimRight.Caption   :=
    resbtnUnTrimRight_Caption;
  btnUnTrim.Caption   :=
    resbtnUnTrim_Caption;

  btnConcat.Caption   :=
    resbtnConcat_Caption;
  btnReverse.Caption   :=
    resbtnReverse_Caption;

  btnReplaceCharByChar.Caption   :=
    resbtnReplaceCharByChar_Caption;
  btnReplaceCharByStr.Caption   :=
    resbtnReplaceCharByStr_Caption;
  btnReplaceStrByStr.Caption   :=
    resbtnReplaceStrByStr_Caption;

  btnRemoveChar.Caption   :=
    resbtnRemoveChar_Caption;
  btnRemoveChars.Caption   :=
    resbtnRemoveChars_Caption;

end;

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

procedure Tfrmmain.btnLengthClick(Sender: TObject);
begin
  edDest.Text :=
    IntToStr(umlcstrings.getLength(edSourceA.Text));
end;

procedure Tfrmmain.btnRemoveCharClick(Sender: TObject);
var B: char;
begin
  B := umlcstrings.StrToChar(edSourceB.Text);
  edDest.Text :=
    umlcstrings.RemoveCharCopy
      (edSourceA.Text, B);
end;

procedure Tfrmmain.btnRemoveCharsClick(Sender: TObject);
begin
  edDest.Text :=
    umlcstrings.RemoveCharsCopy
      (edSourceA.Text, edSourceB.Text);
end;

procedure Tfrmmain.btnReplaceCharByCharClick(Sender: TObject);
var B, C: char;
begin
  B := umlcstrings.StrToChar(edSourceB.Text);
  C := umlcstrings.StrToChar(edSourceC.Text);
  edDest.Text :=
    umlcstrings.ReplaceCharByCharCopy
      (edSourceA.Text, B, C);
end;

procedure Tfrmmain.btnReplaceStrByStrClick(Sender: TObject);
begin
  edDest.Text :=
    umlcstrings.ReplaceStrCopy
      (edSourceA.Text, edSourceB.Text, edSourceC.Text);
end;

procedure Tfrmmain.btnReverseClick(Sender: TObject);
begin
  edDest.Text :=
    umlcstrings.ReverseCopy(edSourceA.Text);
end;

procedure Tfrmmain.btnSQuoteClick(Sender: TObject);
begin
  edDest.Text :=
    #39 + edSourceA.Text + #39;
end;

procedure Tfrmmain.btnReplaceCharByStrClick(Sender: TObject);
var B: char;
begin
  B := umlcstrings.StrToChar(edSourceB.Text);
  edDest.Text :=
    umlcstrings.ReplaceCharByStringCopy
      (edSourceA.Text, B, edSourceC.Text);
end;

procedure Tfrmmain.btnTrimClick(Sender: TObject);
begin
  edDest.Text :=
    umlcstrings.TrimCopy(edSourceA.Text);
end;

procedure Tfrmmain.btnTrimLeftClick(Sender: TObject);
begin
  edDest.Text :=
    umlcstrings.TrimLeftCopy(edSourceA.Text);
end;

procedure Tfrmmain.btnTrimRightClick(Sender: TObject);
begin
  edDest.Text :=
    umlcstrings.TrimRightCopy(edSourceA.Text);
end;

procedure Tfrmmain.btnUnTrimClick(Sender: TObject);
var ACount: Integer;
begin
  ACount :=
      StrToIntDef(edSourceB.Text, 0);

  edDest.Text :=
    umlcstrings.UnTrimCopy(edSourceA.Text, ACount);
end;

procedure Tfrmmain.btnUnTrimLeftClick(Sender: TObject);
var ACount: Integer;
begin
  ACount :=
      StrToIntDef(edSourceB.Text, 0);

  edDest.Text :=
    umlcstrings.UnTrimLeftCopy(edSourceA.Text, ACount);
end;

procedure Tfrmmain.btnUnTrimRightClick(Sender: TObject);
var ACount: Integer;
begin
  ACount :=
      StrToIntDef(edSourceB.Text, 0);

  edDest.Text :=
    umlcstrings.UnTrimRightCopy(edSourceA.Text, ACount);
end;

procedure Tfrmmain.btnAboutClick(Sender: TObject);
begin
  ufrmAbout.Execute();
end;

procedure Tfrmmain.btnClipboardCopyClick(Sender: TObject);
begin
  Clipboard.AsText := edDest.Text;
end;

procedure Tfrmmain.btnConcatClick(Sender: TObject);
begin
  edDest.Text :=
    umlcstrings.ConcatCopy
      (edSourceA.Text,
       edSourceB.Text);
end;

procedure Tfrmmain.btnDQuoteClick(Sender: TObject);
begin
  edDest.Text :=
    #34 + edSourceA.Text + #34;
end;

procedure Tfrmmain.FormActivate(Sender: TObject);
begin
  edSourceA.SetFocus;
end;

procedure Tfrmmain.FormCreate(Sender: TObject);
begin
  LoadValues();
  LoadStrings();
end;

procedure Tfrmmain.FormDestroy(Sender: TObject);
begin
  //
end;

end.

