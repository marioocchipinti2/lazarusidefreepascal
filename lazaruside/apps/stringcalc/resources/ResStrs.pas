unit ResStrs;

interface

{$INCLUDE 'stringcalc_language.inc'}

{$IFDEF LanguageEnglish}
const
  resProductName      = 'String Calculator';
resourcestring
  resTfrMain_Caption  = resProductName;
  resTfrmAbout_Caption  = 'About '+ resProductName;
  resTfrmHelp_Caption   =
    'Changes the source substring in the filename, ' +
    'for the destination substring';

  resbtnOK_Caption      = 'OK';
  resbtnCancel_Caption  = 'Cancel';
  resbtnExit_Caption    = 'E&xit';
  resbtnAbout_Caption   = '&About';
  resbtnOptions_Caption = '&Options';
  resbtnHelp_Caption    = 'Help';
  resbtnSelectALL_Caption  = 'Select &All';
  resbtnSelectNone_Caption = 'Select &None';

  resbtnFileReplace_Caption = 'Replace';
  resbtnFileSearch_Caption  = 'Search';

  reschbConfirmReplaced_Caption = 'Confirm Files Replaced';

  reslblSourceA_Caption   = '(Source) A';
  reslblSourceB_Caption   = '(Source) B';
  reslblSourceC_Caption   = '(Source) C';

  reslblDest_Caption     = 'Destination';

  resbtnClipboardCopy_Caption     = 'Copy';

  resbtnLength_Caption     = 'Get Length';
  resbtnSQuote_Caption     = 'Single Quoted';
  resbtnDQuote_Caption     = 'Double Quoted';

  resbtnTrimLeft_Caption   = 'Trim Left';
  resbtnTrimRight_Caption  = 'Trim Right';
  resbtnTrim_Caption       = 'Trim';

  resbtnUnTrimLeft_Caption   = 'UnTrim Left';
  resbtnUnTrimRight_Caption  = 'UnTrim Right';
  resbtnUnTrim_Caption       = 'UnTrim';

  resbtnConcat_Caption   = 'Concat';
  resbtnReverse_Caption  = 'Reverse';

  resbtnReplaceCharByChar_Caption       = 'Replace Char';
  resbtnReplaceCharByStr_Caption        = 'Replace Char by Str';
  resbtnReplaceStrByStr_Caption = 'Replace Str by Str';

  resbtnRemoveChar_Caption  = 'Remove Char';
  resbtnRemoveChars_Caption = 'Remove Chars';

  reslblFilePath_Caption    = 'File Path:';
  reslblSourceFileName_Caption   = 'Text in Source Filename:';
  reslblDestFileName_Caption     = 'Text in Dest. Filename:';

  reslblProductName_Caption = resProductName;
  reslblVersion_Caption     = '2.0 Version';
  reslblCopyright_Caption   = 'umlcat (c)';
  reslblComments_Caption    = 'umlcat';

  resConfirmReplacement = 'Are you sure you want to replace "%s" by "%s" ?';
  resReplaced           = ' replaced.';
  resNotFound           = 'Files not Found.';
  resUnderConstruction  = 'Under Construction';
  resFileAlreadyExists  = 'Error: Destination filename already exists.';
{$ENDIF}

{$IFDEF LanguageSpanish}
const
  resProductName      = 'Calculadora para Cadenas de Texto';
resourcestring
  resTfrMain_Caption  = resProductName;
  resTfrmAbout_Caption  = 'Acerca de ' + resProductName;
  resTfrmHelp_Caption   =
    'Changes the source substring in the filename, '
    'for the destination substring';

  resbtnOK_Caption      = 'Aceptar';
  resbtnCancel_Caption  = 'Cancelar';
  resbtnExit_Caption    = 'Salir';
  resbtnAbout_Caption   = '&Acerca';
  resbtnOptions_Caption = '&Opciones';
  resbtnHelp_Caption    = 'Ayuda';
  resbtnSelectALL_Caption  = 'Seleccionar To&dos';
  resbtnSelectNone_Caption = 'Seleccionar &Ninguno';

  resbtnFileReplace_Caption = 'Reemplazar';
  resbtnFileSearch_Caption  = 'Buscar';

  reschbConfirmReplaced_Caption = 'Confirmar Ficheros Reemplazados';

  reslblSourceA_Caption   = '(Fuente) A';
  reslblSourceB_Caption   = '(Fuente) B';
  reslblSourceC_Caption   = '(Fuente) C';

  reslblDest_Caption      = 'Destino';

  resbtnClipboardCopy_Caption     = 'Copiar';

  resbtnLength_Caption     = 'Longuitud';
  resbtnSQuote_Caption     = 'Encomillar Simple';
  resbtnDQuote_Caption     = 'Encomillar Doble';

  resbtnTrimLeft_Caption   = 'Acortar Izq.';
  resbtnTrimRight_Caption  = 'Acortar Der.';
  resbtnTrim_Caption       = 'Acortar';

  resbtnUnTrimLeft_Caption   = 'Extender Izq.';
  resbtnUnTrimRight_Caption  = 'Extender Der.';
  resbtnUnTrim_Caption       = 'Extender';

  resbtnConcat_Caption   = 'Concatenar';
  resbtnReverse_Caption  = 'Revertir';

  resbtnReplaceCharByChar_Caption       = 'Reempl Car. por Car.';
  resbtnReplaceCharByStr_Caption        = 'Reempl Car por Cad.';
  resbtnReplaceStrByStr_Caption = 'Reempl Cad. por Cad.';


  resbtnRemoveChar_Caption  = 'Remover Car';
  resbtnRemoveChars_Caption = 'Remover Cars';

  reslblFilePath_Caption    = 'Ruta de Acceso:';
  reslblSourceFileName_Caption   = 'Texto en NombreArchivo Fuente:';
  reslblDestFileName_Caption     = 'Texto en NombreArchivo Dest.:';

  reslblProductName_Caption = resProductName;
  reslblVersion_Caption     = 'Version 2.0';
  reslblCopyright_Caption   = 'umlcat (c)';
  reslblComments_Caption    = 'umlcat';

  resConfirmReplacement = 'Esta seguro que quiere reemplazar "%s" por "%s" ?';
  resReplaced           = ' reemplazado.';
  resNotFound           = 'No se encontraron Ficheros.';
  resUnderConstruction  = 'Bajo Construccion';
  resFileAlreadyExists  = 'Error: Fichero destino ya existe.';
{$ENDIF}

{$IFDEF LanguageLatam}
const
  resProductName      = 'Calculadora para Cadenas de Texto';
resourcestring
  resTfrMain_Caption  = resProductName;
  resTfrmAbout_Caption  = 'Acerca de ' + resProductName;
  resTfrmHelp_Caption   =
    'Changes the source substring in the filename, ' +
    'for the destination substring';

  resbtnOK_Caption      = 'Aceptar';
  resbtnCancel_Caption  = 'Cancelar';
  resbtnExit_Caption    = 'Salir';
  resbtnAbout_Caption   = '&Acerca';
  resbtnOptions_Caption = '&Opciones';
  resbtnHelp_Caption    = 'Ayuda';
  resbtnSelectALL_Caption  = 'Seleccionar To&dos';
  resbtnSelectNone_Caption = 'Seleccionar &Ninguno';

  resbtnFileReplace_Caption = 'Reemplazar';
  resbtnFileSearch_Caption  = 'Buscar';

  reschbConfirmReplaced_Caption = 'Confirmar Archivos Reemplazados';

  reslblSourceA_Caption   = '(Fuente) A';
  reslblSourceB_Caption   = '(Fuente) B';
  reslblSourceC_Caption   = '(Fuente) C';

  reslblDest_Caption      = 'Destino';

  resbtnClipboardCopy_Caption     = 'Copiar';

  resbtnLength_Caption     = 'Longuitud';
  resbtnSQuote_Caption     = 'Encomillar Simple';
  resbtnDQuote_Caption     = 'Encomillar Doble';

  resbtnTrimLeft_Caption   = 'Acortar Izq.';
  resbtnTrimRight_Caption  = 'Acortar Der.';
  resbtnTrim_Caption       = 'Acortar';

  resbtnConcat_Caption   = 'Concatenar';
  resbtnReverse_Caption  = 'Revertir';

  resbtnUnTrimLeft_Caption   = 'Extender Izq.';
  resbtnUnTrimRight_Caption  = 'Extender Der.';
  resbtnUnTrim_Caption       = 'Extender';

  resbtnReplaceCharByChar_Caption       = 'Reempl Car. por Car.';
  resbtnReplaceCharByStr_Caption        = 'Reempl Car por Cad.';
  resbtnReplaceStrByStr_Caption = 'Reempl Cad. por Cad.';

  resbtnRemoveChar_Caption  = 'Remover Car';
  resbtnRemoveChars_Caption = 'Remover Cars';

  reslblFilePath_Caption    = 'Ruta de Acceso:';
  reslblSourceFileName_Caption   = 'Texto en NombreArchivo Fuente:';
  reslblDestFileName_Caption     = 'Texto en NombreArchivo Dest.:';

  reslblProductName_Caption = resProductName;
  reslblVersion_Caption     = 'Version 2.0';
  reslblCopyright_Caption   = 'umlcat (c)';
  reslblComments_Caption    = 'umlcat';

  resConfirmReplacement = 'Esta seguro que quiere reemplazar "%s" por "%s" ?';
  resReplaced           = ' reemplazado.';
  resNotFound           = 'No se encontraron archivos.';
  resUnderConstruction  = 'Bajo Construccion';
  resFileAlreadyExists  = 'Error: Archivo destino ya existe.';
{$ENDIF}

{$IFDEF LanguageFrench}
const
  resProductName      = 'Reemplazateur pour Nom d*Archives';
resourcestring
  resTfrMain_Caption  = resProductName;
  resTfrmAbout_Caption  = 'Sur ' + resProductName;
  resTfrmHelp_Caption   =
    'Changes the source substring in the filename, ' +
    'for the destination substring';

  resbtnOK_Caption      = 'OK';
  resbtnCancel_Caption  = 'Annuler';
  resbtnExit_Caption    = 'Sortir';
  resbtnAbout_Caption   = '&Sur...';
  resbtnOptions_Caption = '&Options';
  resbtnHelp_Caption    = 'Aide';
  resbtnSelectALL_Caption  = 'Choisir To&us;
  resbtnSelectNone_Caption = 'Choisir &Aucun';

  resbtnFileReplace_Caption = 'Remplacez';
  resbtnFileSearch_Caption  = 'Chercher';

  reschbConfirmReplaced_Caption = 'Confirmer des Archives Remplacés';

  reslblSourceA_Caption   = '(Fuente) A';
  reslblSourceB_Caption   = '(Fuente) B';
  reslblSourceC_Caption   = '(Fuente) C';

  reslblDest_Caption      = 'Destino';

  resbtnClipboardCopy_Caption     = 'Copiar';


  resbtnLength_Caption     = 'Longuitud';
  resbtnSQuote_Caption     = 'Encomillar Simple';
  resbtnDQuote_Caption     = 'Encomillar Doble';

  resbtnTrimLeft_Caption   = 'Acortar Izq.';
  resbtnTrimRight_Caption  = 'Acortar Der.';
  resbtnTrim_Caption       = 'Acortar';

  resbtnUnTrimLeft_Caption   = 'Extender Izq.';
  resbtnUnTrimRight_Caption  = 'Extender Der.';
  resbtnUnTrim_Caption       = 'Extender';

  resbtnConcat_Caption   = 'Concatenar';
  resbtnReverse_Caption  = 'Revertir';

  resbtnReplaceCharByChar_Caption       = 'Reempl Car. por Car.';
  resbtnReplaceCharByStr_Caption        = 'Reempl Car por Cad.';
  resbtnReplaceStrByStr_Caption = 'Reempl Cad. por Cad.';

  resbtnRemoveChar_Caption  = 'Remover Car';
  resbtnRemoveChars_Caption = 'Remover Cars';

  reslblFilePath_Caption    = 'Acces Chemin:';
  reslblSourceFileName_Caption   = 'Text du Source:';
  reslblDestFileName_Caption     = 'Text du Destination:';

  reslblProductName_Caption = resProductName;
  reslblVersion_Caption     = 'Version 2.0';
  reslblCopyright_Caption   = 'umlcat (c)';
  reslblComments_Caption    = 'umlcat';

  resConfirmReplacement = 'Il est sûr qu*il veut reemplazer "%s" par "%s" ?';
  resReplaced           = ' remplacé.';
  resNotFound           = 'On n*a pas trouvé d*archives.';
  resUnderConstruction  = 'Sous Construction';
  resFileAlreadyExists  = 'Error: Il*ya le archive du destination.';
{$ENDIF}

{$IFDEF LanguageGerman}
const
  resProductName      = 'Reemplazateur pour Nom d*Archives';
resourcestring
  resTfrMain_Caption  = resProductName;
  resTfrmAbout_Caption  = 'Sur ' + resProductName;
  resTfrmHelp_Caption   =
    'Changes the source substring in the filename, ' +
    'for the destination substring';

  resbtnOK_Caption      = 'OK';
  resbtnCancel_Caption  = 'Annuler';
  resbtnExit_Caption    = 'Sortir';
  resbtnAbout_Caption   = '&Sur...';
  resbtnOptions_Caption = '&Options';
  resbtnHelp_Caption    = 'Aide';
  resbtnSelectALL_Caption  = 'Choisir To&us;
  resbtnSelectNone_Caption = 'Choisir &Aucun';

  resbtnFileReplace_Caption = 'Remplacez';
  resbtnFileSearch_Caption  = 'Chercher';

  resbtnRemoveChar_Caption  = 'Remover Car';
  resbtnRemoveChars_Caption = 'Remover Cars';  reschbConfirmReplaced_Caption = 'Confirmer des Archives Remplacés';

  reslblSourceA_Caption   = '(Fuente) A';
  reslblSourceB_Caption   = '(Fuente) B';
  reslblSourceC_Caption   = '(Fuente) C';

  reslblDest_Caption      = 'Destino';

  resbtnClipboardCopy_Caption     = 'Copiar';

  resbtnLength_Caption     = 'Longuitud';
  resbtnSQuote_Caption     = 'Encomillar Simple';
  resbtnDQuote_Caption     = 'Encomillar Doble';

  resbtnTrimLeft_Caption   = 'Acortar Izq.';
  resbtnTrimRight_Caption  = 'Acortar Der.';
  resbtnTrim_Caption       = 'Acortar';

  resbtnUnTrimLeft_Caption   = 'Extender Izq.';
  resbtnUnTrimRight_Caption  = 'Extender Der.';
  resbtnUnTrim_Caption       = 'Extender';

  resbtnReplaceCharByChar_Caption       = 'Reempl Car. por Car.';
  resbtnReplaceCharByStr_Caption        = 'Reempl Car por Cad.';
  resbtnReplaceStrByStr_Caption = 'Reempl Cad. por Cad.';


  reslblFilePath_Caption    = 'Acces Chemin:';
  reslblSourceFileName_Caption   = 'Nom Archive du Source:';
  reslblDestFileName_Caption     = 'Nom Archive du Destination:';

  reslblProductName_Caption = resProductName;
  reslblVersion_Caption     = 'Version 2.0';
  reslblCopyright_Caption   = 'umlcat (c)';
  reslblComments_Caption    = 'umlcat';

  resConfirmReplacement = 'Il est sûr qu*il veut reemplazer "%s" par "%s" ?';
  resReplaced           = ' remplacé.';
  resNotFound           = 'On n*a pas trouvé d*archives.';
  resUnderConstruction  = 'Sous Construction';
  resFileAlreadyExists  = 'Error: Il*ya le archive du destination.';
{$ENDIF}

implementation

end.
