unit ufrmmainstrs;

{$mode objfpc}{$H+}

interface

{$INCLUDE 'resources\uuidgen_language.inc'}

uses
  Classes,
  SysUtils,
  ResStrs,
  dummy;

{$IFDEF LanguageNeutral}
resourcestring
  resTfrmMain_Caption  = resProductName;

  resUUIDToTextTabSheet_Caption = 'UUID To Text';
  resTextToUUIDTabSheet_Caption = 'Text To UUID';

  resExecuteButton_Caption = 'Generate';
  resExitButton_Caption    = 'Exit';

  resSingleUUIDStrButton_Caption   = 'Copy';
  resDoubleUUIDStrButton_Caption   = 'Copy';
  resLongUUIDStrButton_Caption     = 'Copy';
  resPascalByteArrayButton_Caption = 'Copy';
  resPlainCByteArrayButton_Caption = 'Copy';

  resSingleUUIDStrLabel_Caption   = 'Single String:';
  resDoubleUUIDStrLabel_Caption   = 'Double String:';
  resLongUUIDStrLabel_Caption     = 'Long String:';
  resPascalByteArrayLabel_Caption = 'Pascal Byte Array:';
  resPlainCByteArrayLabel_Caption = 'Plain C Byte Array:';

  resSourceSingleUUIDStrRadioButton_Caption   = 'Single String:';
  resDoubleUUIDStrSourceRadioButton_Caption   = 'Double String:';
  resLongUUIDStrSourceRadioButton_Caption     = 'Long String:';
  resPascalByteArraySourceRadioButton_Caption = 'Pascal Byte Array:';
  resPlainCByteArraySourceRadioButton_Caption = 'Plain C Byte Array:';
  resResultLabel_Caption                      = 'Result:';

  resSingleUUIDStrPasteButton_Caption   = 'Paste';
  resDoubleUUIDStrPasteButton_Caption   = 'Paste';
  resLongUUIDStrPasteButton_Caption     = 'Paste';
  resPascalByteArrayPasteButton_Caption = 'Paste';
  resPlainCByteArrayPasteButton_Caption = 'Paste';

{$ENDIF}

{$IFDEF LanguageEnglish}
resourcestring
  resTfrmMain_Caption  = resProductName;

  resUUIDToTextTabSheet_Caption = 'UUID To Text';
  resTextToUUIDTabSheet_Caption = 'Text To UUID';

  resExecuteButton_Caption = 'Generate';
  resExitButton_Caption    = 'Exit';

  resSingleUUIDStrButton_Caption   = 'Copy';
  resDoubleUUIDStrButton_Caption   = 'Copy';
  resLongUUIDStrButton_Caption     = 'Copy';
  resPascalByteArrayButton_Caption = 'Copy';
  resPlainCByteArrayButton_Caption = 'Copy';

  resSingleUUIDStrLabel_Caption   = 'Single String:';
  resDoubleUUIDStrLabel_Caption   = 'Double String:';
  resLongUUIDStrLabel_Caption     = 'Long String:';
  resPascalByteArrayLabel_Caption = 'Pascal Byte Array:';
  resPlainCByteArrayLabel_Caption = 'Plain C Byte Array:';

  resSourceSingleUUIDStrRadioButton_Caption   = 'Single String:';
  resDoubleUUIDStrSourceRadioButton_Caption   = 'Double String:';
  resLongUUIDStrSourceRadioButton_Caption     = 'Long String:';
  resPascalByteArraySourceRadioButton_Caption = 'Pascal Byte Array:';
  resPlainCByteArraySourceRadioButton_Caption = 'Plain C Byte Array:';
  resResultLabel_Caption                      = 'Result:';

  resSingleUUIDStrPasteButton_Caption   = 'Paste';
  resDoubleUUIDStrPasteButton_Caption   = 'Paste';
  resLongUUIDStrPasteButton_Caption     = 'Paste';
  resPascalByteArrayPasteButton_Caption = 'Paste';
  resPlainCByteArrayPasteButton_Caption = 'Paste';

{$ENDIF}

{$IFDEF LanguageSpanish}
resourcestring
  resTfrmMain_Caption  = resProductName;

  resUUIDToTextTabSheet_Caption = 'UUID a Texto';
  resTextToUUIDTabSheet_Caption = 'Texto a UUID';

  resExecuteButton_Caption = 'Generar';
  resExitButton_Caption    = 'Salir';

  resSingleUUIDStrButton_Caption   = 'Copiar';
  resDoubleUUIDStrButton_Caption   = 'Copiar';
  resLongUUIDStrButton_Caption     = 'Copiar';
  resPascalByteArrayButton_Caption = 'Copiar';
  resPlainCByteArrayButton_Caption = 'Copiar';

  resSingleUUIDStrLabel_Caption   = 'String Sencillo:';
  resDoubleUUIDStrLabel_Caption   = 'String Doble:';
  resLongUUIDStrLabel_Caption     = 'String Largo:';
  resPascalByteArrayLabel_Caption = 'Arreglo Pascal Byte:';
  resPlainCByteArrayLabel_Caption = 'Arreglo C Plano:';

  resSourceSingleUUIDStrRadioButton_Caption   = 'String Sencillo:';
  resDoubleUUIDStrSourceRadioButton_Caption   = 'String Doble:';
  resLongUUIDStrSourceRadioButton_Caption     = 'String Largo:';
  resPascalByteArraySourceRadioButton_Caption = 'Arreglo Pascal Byte:';
  resPlainCByteArraySourceRadioButton_Caption = 'Arreglo C Plano Byte:';
  resResultLabel_Caption                      = 'Resultado:';

  resSingleUUIDStrPasteButton_Caption   = 'Pegar';
  resDoubleUUIDStrPasteButton_Caption   = 'Pegar';
  resLongUUIDStrPasteButton_Caption     = 'Pegar';
  resPascalByteArrayPasteButton_Caption = 'Pegar';
  resPlainCByteArrayPasteButton_Caption = 'Pegar';

{$ENDIF}

{$IFDEF LanguageLatam}
resourcestring
  resTfrmMain_Caption  = resProductName;

  resUUIDToTextTabSheet_Caption = 'UUID a Texto';
  resTextToUUIDTabSheet_Caption = 'Texto a UUID';

  resExecuteButton_Caption = 'Generar';
  resExitButton_Caption    = 'Salir';

  resSingleUUIDStrButton_Caption   = 'Copiar';
  resDoubleUUIDStrButton_Caption   = 'Copiar';
  resLongUUIDStrButton_Caption     = 'Copiar';
  resPascalByteArrayButton_Caption = 'Copiar';
  resPlainCByteArrayButton_Caption = 'Copiar';

  resSingleUUIDStrLabel_Caption   = 'String Sencillo:';
  resDoubleUUIDStrLabel_Caption   = 'String Doble:';
  resLongUUIDStrLabel_Caption     = 'String Largo:';
  resPascalByteArrayLabel_Caption = 'Arreglo Pascal Byte:';
  resPlainCByteArrayLabel_Caption = 'Arreglo C Plano:';

  resSourceSingleUUIDStrRadioButton_Caption   = 'String Sencillo:';
  resDoubleUUIDStrSourceRadioButton_Caption   = 'String Doble:';
  resLongUUIDStrSourceRadioButton_Caption     = 'String Largo:';
  resPascalByteArraySourceRadioButton_Caption = 'Arreglo Pascal Byte:';
  resPlainCByteArraySourceRadioButton_Caption = 'Arreglo C Plano Byte:';
  resResultLabel_Caption                      = 'Resultado:';

  resSingleUUIDStrPasteButton_Caption   = 'Pegar';
  resDoubleUUIDStrPasteButton_Caption   = 'Pegar';
  resLongUUIDStrPasteButton_Caption     = 'Pegar';
  resPascalByteArrayPasteButton_Caption = 'Pegar';
  resPlainCByteArrayPasteButton_Caption = 'Pegar';
{$ENDIF}

{$IFDEF LanguageFrench}
resourcestring
  resTfrmMain_Caption  = resProductName;

  resUUIDToTextTabSheet_Caption = 'UUID To Text';
  resTextToUUIDTabSheet_Caption = 'Text To UUID';

  resExecuteButton_Caption = 'Generate';
  resExitButton_Caption    = 'Exit';

  resSingleUUIDStrButton_Caption   = 'Copy';
  resDoubleUUIDStrButton_Caption   = 'Copy';
  resLongUUIDStrButton_Caption     = 'Copy';
  resPascalByteArrayButton_Caption = 'Copy';
  resPlainCByteArrayButton_Caption = 'Copy';

  resSingleUUIDStrLabel_Caption   = 'Single String:';
  resDoubleUUIDStrLabel_Caption   = 'Double String:';
  resLongUUIDStrLabel_Caption     = 'Long String:';
  resPascalByteArrayLabel_Caption = 'Pascal Byte Array:';
  resPlainCByteArrayLabel_Caption = 'Plain C Byte Array:';

  resSourceSingleUUIDStrRadioButton_Caption   = 'Single String:';
  resDoubleUUIDStrSourceRadioButton_Caption   = 'Double String:';
  resLongUUIDStrSourceRadioButton_Caption     = 'Long String:';
  resPascalByteArraySourceRadioButton_Caption = 'Pascal Byte Array:';
  resPlainCByteArraySourceRadioButton_Caption = 'Plain C Byte Array:';
  resResultLabel_Caption                      = 'Result:';

  resSingleUUIDStrPasteButton_Caption   = 'Paste';
  resDoubleUUIDStrPasteButton_Caption   = 'Paste';
  resLongUUIDStrPasteButton_Caption     = 'Paste';
  resPascalByteArrayPasteButton_Caption = 'Paste';
  resPlainCByteArrayPasteButton_Caption = 'Paste';
{$ENDIF}

{$IFDEF LanguageGerman}
resourcestring
  resTfrmMain_Caption  = resProductName;

  resUUIDToTextTabSheet_Caption = 'UUID To Text';
  resTextToUUIDTabSheet_Caption = 'Text To UUID';

  resExecuteButton_Caption = 'Generate';
  resExitButton_Caption    = 'Exit';

  resSingleUUIDStrButton_Caption   = 'Copy';
  resDoubleUUIDStrButton_Caption   = 'Copy';
  resLongUUIDStrButton_Caption     = 'Copy';
  resPascalByteArrayButton_Caption = 'Copy';
  resPlainCByteArrayButton_Caption = 'Copy';

  resSingleUUIDStrLabel_Caption   = 'Single String:';
  resDoubleUUIDStrLabel_Caption   = 'Double String:';
  resLongUUIDStrLabel_Caption     = 'Long String:';
  resPascalByteArrayLabel_Caption = 'Pascal Byte Array:';
  resPlainCByteArrayLabel_Caption = 'Plain C Byte Array:';

  resSourceSingleUUIDStrRadioButton_Caption   = 'Single String:';
  resDoubleUUIDStrSourceRadioButton_Caption   = 'Double String:';
  resLongUUIDStrSourceRadioButton_Caption     = 'Long String:';
  resPascalByteArraySourceRadioButton_Caption = 'Pascal Byte Array:';
  resPlainCByteArraySourceRadioButton_Caption = 'Plain C Byte Array:';
  resResultLabel_Caption                      = 'Result:';

  resSingleUUIDStrPasteButton_Caption   = 'Paste';
  resDoubleUUIDStrPasteButton_Caption   = 'Paste';
  resLongUUIDStrPasteButton_Caption     = 'Paste';
  resPascalByteArrayPasteButton_Caption = 'Paste';
  resPlainCByteArrayPasteButton_Caption = 'Paste';
{$ENDIF}


{$IFDEF LanguageElse}
resourcestring
  resTfrmMain_Caption  = resProductName;

  resUUIDToTextTabSheet_Caption = 'UUID To Text';
  resTextToUUIDTabSheet_Caption = 'Text To UUID';

  resExecuteButton_Caption = 'Generate';
  resExitButton_Caption    = 'Exit';

  resSingleUUIDStrButton_Caption   = 'Copy';
  resDoubleUUIDStrButton_Caption   = 'Copy';
  resLongUUIDStrButton_Caption     = 'Copy';
  resPascalByteArrayButton_Caption = 'Copy';
  resPlainCByteArrayButton_Caption = 'Copy';

  resSingleUUIDStrLabel_Caption   = 'Single String:';
  resDoubleUUIDStrLabel_Caption   = 'Double String:';
  resLongUUIDStrLabel_Caption     = 'Long String:';
  resPascalByteArrayLabel_Caption = 'Pascal Byte Array:';
  resPlainCByteArrayLabel_Caption = 'Plain C Byte Array:';

  resSourceSingleUUIDStrRadioButton_Caption   = 'Single String:';
  resDoubleUUIDStrSourceRadioButton_Caption   = 'Double String:';
  resLongUUIDStrSourceRadioButton_Caption     = 'Long String:';
  resPascalByteArraySourceRadioButton_Caption = 'Pascal Byte Array:';
  resPlainCByteArraySourceRadioButton_Caption = 'Plain C Byte Array:';
  resResultLabel_Caption                      = 'Result:';

  resSingleUUIDStrPasteButton_Caption   = 'Paste';
  resDoubleUUIDStrPasteButton_Caption   = 'Paste';
  resLongUUIDStrPasteButton_Caption     = 'Paste';
  resPascalByteArrayPasteButton_Caption = 'Paste';
  resPlainCByteArrayPasteButton_Caption = 'Paste';
{$ENDIF}



implementation

end.

