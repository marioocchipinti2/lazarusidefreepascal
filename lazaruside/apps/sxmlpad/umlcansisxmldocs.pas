(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

unit umlcansisxmldocs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcstrings,
  umlcstatestreams,
  umlcfilestreams,
  umlcansitextstreams,
  umlcsrchtypes,
  umlcdocs,
  umlcscanneroptions,
  umlcxmlfileansiscanners,
  umlcxmlfiletokens,
  umlcxmlfiletreenodetokens,
  umlcxmlfileansisymbols,
  umlctreenodes,
  umlctreecntrs,
  umlcsxmltreecntrs,
  umlcansisxmldocres,
  dummy;

(**
 ** Description:
 ** This unit implements the "Document" Software Design Pattern,
 ** which allows to use a file in an application,
 ** with common features like Opening a file, saving a file.
 **
 ** Examples: Bitmap editor, text or word processor editor,
 ** spreadsheet application.
 **
 ** In this specific case, supports a Simple X.M.L. file.
 **)

const

  // ---

  ID_TCustomUMLCANSIXMLDocument : TUMLCType =
    ($4E,$26,$DC,$06,$91,$AA,$86,$4F,$98,$07,$7B,$E7,$02,$12,$0E,$85);

  ID_TUMLCANSIXMLDocument : TUMLCType =
    ($24,$2B,$6F,$8E,$70,$CE,$24,$4A,$9D,$9A,$BE,$F7,$2D,$3A,$F7,$E2);

  // ---

type

(* TCustomUMLCANSIXMLDocument *)

  TCustomUMLCANSIXMLDocument = class(TCustomUMLCDocument)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    CurrentNode:   TUMLCSXMLTreeNode;
  public
    (* Public declarations *)

    RootNode:      TUMLCSXMLTreeNode;
    EncodingNode:  TUMLCSXMLTreeNode;
    MainNode:      TUMLCSXMLTreeNode;
    EoFNode:       TUMLCSXMLTreeNode;

    // provides a filesystem access stream,
    // used by "TextStream"
    FileStream:    TUMLCFileStream;

    // provides a text line based stream
    TextStream:    TCustomUMLCAnsiTextStream;

    Scanner:       TCustomUMLCXMLFileANSIScanner;

    TreeContainer: TCustomUMLCSXMLTreeContainer;
  protected
    (* Protected declarations *)

    (* Accesors declarations *)

    procedure setFullPath(const AValue: string); override;
  protected
    (* Protected declarations *)

    procedure InsertEncoding
      (var ASymbol: TUMLCXMLFileANSISymbol);

    procedure InsertComment
      (var ASymbol: TUMLCXMLFileANSISymbol);

    procedure InsertEoLn
      (var ASymbol: TUMLCXMLFileANSISymbol);
    procedure InsertEoPg
      (var ASymbol: TUMLCXMLFileANSISymbol);
    procedure InsertEoF
      (var ASymbol: TUMLCXMLFileANSISymbol);

    procedure InsertSpace
      (var ASymbol: TUMLCXMLFileANSISymbol);
    procedure InsertTab
      (var ASymbol: TUMLCXMLFileANSISymbol);

    procedure InsertSingle
      (var ASymbol: TUMLCXMLFileANSISymbol);

    procedure InsertText
      (var ASymbol: TUMLCXMLFileANSISymbol);

    procedure StartBlock
      (var ASymbol: TUMLCXMLFileANSISymbol);
    procedure FinishBlock
      (var ASymbol: TUMLCXMLFileANSISymbol);

    procedure ProcessSymbol
      (var ASymbol: TUMLCXMLFileANSISymbol);

    procedure InternalNewFile(); override;
    procedure InternalNewAsFile(); override;

    procedure InternalSaveFile(); override;

    procedure InternalOpenFile(); override;
  protected
    (* Protected declarations *)

    procedure GenerateSXML
      (var ANode: TUMLCTreeNode;
       const AParam: pointer; const ADirection: TUMLCTreeDirection);
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    function CreateRoot(): TUMLCSXMLTreeNode;
    procedure GenerateDocument(var AText: string);
  end;

(* TUMLCANSIXMLDocument *)

  TUMLCANSIXMLDocument = class(TCustomUMLCANSIXMLDocument)
  published
    (* published declarations *)

    (* TCustomUMLCDocument: *)

    property AskOverwrite;
    property Encoding;
    property ExtNewAs;
    property ExtOpen;
    property ExtSave;
    property ExtSaveAs;
    property HasName;
    property HasEncoding;
    property IsClipboardEmpty;
    property Modified;
    property ReadOnly;
    property FullPath;
    property SearchMode;

    property OnModified;
    property OnPathChanged;
    property OnSearchModeChanged;
    property OnConfirmUser;

    property BeforeNewFile;
    property AfterNewFile;

    property BeforeNewAsFile;
    property AfterNewAsFile;

    property BeforeOpenFile;
    property AfterOpenFile;

    property BeforeSaveFile;
    property AfterSaveFile;

    property BeforeSaveAsFile;
    property AfterSaveAsFile;

    (* TCustomUMLCANSIXMLDocument: *)
  end;

implementation

(* TCustomUMLCANSIXMLDocument *)

procedure TCustomUMLCANSIXMLDocument.setFullPath(const AValue: string);
begin
  FFullPath := AValue;

  if (FileStream <> nil) then
  begin
    FileStream.Path := AValue;
  end;

  DelegateOnPathChanged(AValue);
end;

procedure TCustomUMLCANSIXMLDocument.InsertEncoding
  (var ASymbol: TUMLCXMLFileANSISymbol);
var ANewNode: TUMLCSXMLTreeNode;
begin
  // add child node to non-visual node
  ANewNode :=
    (Self.CurrentNode.Insert() as TUMLCSXMLTreeNode);

  ANewNode.CanInsert := false; // < --
  ANewNode.CanEdit   := true;
  ANewNode.CanRemove := true;
  ANewNode.Symbol    := ASymbol;
  ANewNode.TreeToken := UMLCxmlfiletreenodetokens.xmlfiletrntkEncoding;
  ANewNode.TextValue
    := UMLCstrings.TrimDelimitersCopy(ASymbol.Text, '<?', '?>');
  ANewNode.Text
    := UMLCstrings.PadDelimitersCopy(ASymbol.Text, '<?', '?>');
end;

procedure TCustomUMLCANSIXMLDocument.InsertComment
  (var ASymbol: TUMLCXMLFileANSISymbol);
var ANewNode: TUMLCSXMLTreeNode;
begin
  // add child node to non-visual node
  ANewNode :=
    (Self.CurrentNode.Insert() as TUMLCSXMLTreeNode);

  ANewNode.CanInsert := false; // < --
  ANewNode.CanEdit   := true;
  ANewNode.CanRemove := true;
  ANewNode.Symbol    := ASymbol;
  ANewNode.TreeToken := UMLCxmlfiletreenodetokens.xmlfiletrntkComment;

  ANewNode.TextValue
    := UMLCstrings.TrimDelimitersCopy(ASymbol.Text, '<!--', '-->');
  ANewNode.Text
    := UMLCstrings.PadDelimitersCopy(ASymbol.Text, '<!--', '-->');
end;

procedure TCustomUMLCANSIXMLDocument.InsertEoLn
  (var ASymbol: TUMLCXMLFileANSISymbol);
var ANewNode: TUMLCSXMLTreeNode;
begin
  // add child node to non-visual node
  ANewNode :=
    (Self.CurrentNode.Insert() as TUMLCSXMLTreeNode);

  ANewNode.CanInsert := false; // < --
  ANewNode.CanEdit   := true;
  ANewNode.CanRemove := true;
  ANewNode.Symbol    := ASymbol;
  ANewNode.TreeToken := UMLCxmlfiletreenodetokens.xmlfiletrntkEoLn;
  ANewNode.TextValue := #13#10;
  ANewNode.Text := '[EoLn]';
end;

procedure TCustomUMLCANSIXMLDocument.InsertEoPg
  (var ASymbol: TUMLCXMLFileANSISymbol);
var ANewNode: TUMLCSXMLTreeNode;
begin
  // add child node to non-visual node
  ANewNode :=
    (Self.CurrentNode.Insert() as TUMLCSXMLTreeNode);

  ANewNode.CanInsert := false; // < --
  ANewNode.CanEdit   := true;
  ANewNode.CanRemove := true;
  ANewNode.Symbol    := ASymbol;
  ANewNode.TreeToken := UMLCxmlfiletreenodetokens.xmlfiletrntkEoLn;
  ANewNode.TextValue := #12;
  ANewNode.Text := '[EoPg]';
end;

procedure TCustomUMLCANSIXMLDocument.InsertEoF
  (var ASymbol: TUMLCXMLFileANSISymbol);
var ANewNode: TUMLCSXMLTreeNode;
begin
  // add child node to non-visual node
  ANewNode :=
    (Self.CurrentNode.Insert() as TUMLCSXMLTreeNode);

  ANewNode.CanInsert := false; // < --
  ANewNode.CanEdit   := true;
  ANewNode.CanRemove := true;
  ANewNode.Symbol    := ASymbol;
  ANewNode.TreeToken := UMLCxmlfiletreenodetokens.xmlfiletrntkEoLn;
  ANewNode.TextValue := #26;
  ANewNode.Text := '[FILEBREAK]';

  // --> check if its "main" node
  if (EoFNode = nil) then
  begin
    EoFNode := ANewNode;
  end;
end;

procedure TCustomUMLCANSIXMLDocument.InsertSpace
  (var ASymbol: TUMLCXMLFileANSISymbol);
var ANewNode: TUMLCSXMLTreeNode;
begin

end;

procedure TCustomUMLCANSIXMLDocument.InsertTab
  (var ASymbol: TUMLCXMLFileANSISymbol);
var ANewNode: TUMLCSXMLTreeNode;
begin
  // add child node to non-visual node
  ANewNode :=
    (Self.CurrentNode.Insert() as TUMLCSXMLTreeNode);

  ANewNode.CanInsert := false; // < --
  ANewNode.CanEdit   := true;
  ANewNode.CanRemove := true;
  ANewNode.Symbol    := ASymbol;
  ANewNode.TreeToken := UMLCxmlfiletreenodetokens.xmlfiletrntkTab;

  ANewNode.TextValue := #9;
  ANewNode.Text := '[TAB]';
end;

procedure TCustomUMLCANSIXMLDocument.InsertSingle
  (var ASymbol: TUMLCXMLFileANSISymbol);
var ANewNode: TUMLCSXMLTreeNode;
begin
  // add child node to non-visual node
  ANewNode :=
    (Self.CurrentNode.Insert() as TUMLCSXMLTreeNode);

  ANewNode.CanInsert := false; // < --
  ANewNode.CanEdit   := true;
  ANewNode.CanRemove := true;
  ANewNode.Symbol    := ASymbol;
  ANewNode.TreeToken := UMLCxmlfiletreenodetokens.xmlfiletrntkSingle;
  ANewNode.TextValue
    := UMLCstrings.TrimDelimitersCopy(ASymbol.Text, '<', '/>');
  ANewNode.Text
    := UMLCstrings.PadDelimitersCopy(ASymbol.Text, '<', '/>');
end;

procedure TCustomUMLCANSIXMLDocument.InsertText
  (var ASymbol: TUMLCXMLFileANSISymbol);
var ANewNode: TUMLCSXMLTreeNode;
begin
  ANewNode :=
    (Self.CurrentNode.Insert() as TUMLCSXMLTreeNode);

  ANewNode.CanInsert := false; // < --
  ANewNode.CanEdit   := true;
  ANewNode.CanRemove := true;
  ANewNode.Symbol    := ASymbol;
  ANewNode.TreeToken := UMLCxmlfiletreenodetokens.xmlfiletrntkText;
  ANewNode.TextValue := ASymbol.Text;
  ANewNode.Text      := ASymbol.Text;
end;

procedure TCustomUMLCANSIXMLDocument.StartBlock
  (var ASymbol: TUMLCXMLFileANSISymbol);
var ANewNode: TUMLCSXMLTreeNode;
begin
  // add child node to non-visual node
  ANewNode :=
    (Self.CurrentNode.Insert() as TUMLCSXMLTreeNode);

  ANewNode.CanInsert := false; // < --
  ANewNode.CanEdit   := true;
  ANewNode.CanRemove := true;
  ANewNode.Symbol    := ASymbol;
  ANewNode.TreeToken := UMLCxmlfiletreenodetokens.xmlfiletrntkBlock;
  ANewNode.TextValue
    := UMLCstrings.TrimDelimitersCopy(ASymbol.Text, '<', '>');
  ANewNode.Text
    := UMLCstrings.PadDelimitersCopy(ASymbol.Text, '<', '>');

  // --> check if its "main" node
  if (MainNode = nil) then
  begin
    MainNode := ANewNode;
  end;

  // "start" tags move "current" as parent node
  Self.CurrentNode := ANewNode;
end;

procedure TCustomUMLCANSIXMLDocument.FinishBlock
  (var ASymbol: TUMLCXMLFileANSISymbol);
begin
  // "start" tags move "current" to parent node
  Self.CurrentNode := TUMLCSXMLTreeNode(Self.CurrentNode.Parent());
end;

procedure TCustomUMLCANSIXMLDocument.ProcessSymbol
  (var ASymbol: TUMLCXMLFileANSISymbol);
begin
  // --> there must be a root "document" node,
  // --> at this moment
  case (ASymbol.Token) of
    UMLCxmlfiletokens.xmlfiletkEoLn:
    begin
      InsertEoLn((* var *) ASymbol);
    end;

    UMLCxmlfiletokens.xmlfiletkEoPg:
    begin
      InsertEoPg((* var *) ASymbol);
    end;

    UMLCxmlfiletokens.xmlfiletkEoF:
    begin
      InsertEoF((* var *) ASymbol);
    end;

    UMLCxmlfiletokens.xmlfiletkSpace:
    begin
      InsertSpace((* var *) ASymbol);
    end;

    UMLCxmlfiletokens.xmlfiletkTab:
    begin
      InsertTab((* var *) ASymbol);
    end;

    UMLCxmlfiletokens.xmlfiletkText:
    begin
      InsertText((* var *) ASymbol);
    end;

    UMLCxmlfiletokens.xmlfiletkEncoding:
    begin
      InsertEncoding((* var *) ASymbol);
    end;
    UMLCxmlfiletokens.xmlfiletkStart:
    begin
      StartBlock((* var *) ASymbol);
    end;
    UMLCxmlfiletokens.xmlfiletkFinish:
    begin
      FinishBlock((* var *) ASymbol);
    end;

    UMLCxmlfiletokens.xmlfiletkSingle:
    begin
      InsertSingle((* var *) ASymbol);
    end;

    UMLCxmlfiletokens.xmlfiletkBlock:
    begin
      Self.DoNothing();
    end

    else
      begin
      end;
  end;
end;

procedure TCustomUMLCANSIXMLDocument.InternalNewFile();
begin
  // --> assign internal fields
  RootNode     := nil;
  inherited InternalNewFile();

  EncodingNode := nil;
  MainNode     := nil;
  EoFNode      := nil;
end;

procedure TCustomUMLCANSIXMLDocument.InternalNewAsFile();
var ASymbol: TUMLCXMLFileANSISymbol;
begin
  // --> assign internal fields
  FAskOverwrite := FALSE;
  FHasName      := FALSE;

  FSearchIndex := 1;
  FSearchMode  := UMLCsrchtypes.srrSearch;
  SearchMode   := UMLCsrchtypes.srrNone;
  // sera actualizado al activar ventana
  // will be updated when window is activated

  FIsClipboardEmpty := TRUE;
  FModified     := FALSE;
  FHasName      := FALSE;
  FReadOnly     := FALSE;
  FHasEncoding  := FALSE;

  FAskOverwrite := FALSE;

  RootNode     := nil;
  EncodingNode := nil;
  MainNode     := nil;
  EoFNode      := nil;

  // update full source path
  FileStream.Path  := Self.FullPath;
  // indicate requested initial state of stream
  FileStream.State := UMLCstatestreams.ssReset;

  // --> scan or parse file
  if (Scanner.Start()) then
  begin
    // initial assignation, will change
    Self.CurrentNode := TUMLCSXMLTreeNode(Self.TreeContainer.Items.Root());

    UMLCxmlfileansisymbols.Clear((* var *) ASymbol);

    while (Scanner.Next()) do
    begin
      Scanner.ReadCurrentSymbol((* var *) ASymbol);
      ProcessSymbol((* var *) ASymbol);
    end;

    ASymbol.Token := UMLCxmlfiletokens.xmlfiletkEoF;
    ASymbol.Text  := #26;
    ProcessSymbol((* var *) ASymbol);

    Scanner.Finish();
  end;

  // clear source path, avoiding overwrite
  FullPath := NewFileName(ExtNew);
  FileStream.Path  := Self.FullPath;
end;

procedure TCustomUMLCANSIXMLDocument.InternalSaveFile();
var AText: string; TextFile: System.Text;
begin
  AText := '';
  GenerateDocument(AText);

  System.Assign(TextFile, Self.FullPath);
  System.Rewrite(TextFile);

  Write(TextFile, AText);

  System.Close(TextFile);

  // --> assign internal fields
  inherited InternalSaveFile();
end;

procedure TCustomUMLCANSIXMLDocument.InternalOpenFile();
var ASymbol: TUMLCXMLFileANSISymbol;
begin
  // --> assign internal fields
  RootNode     := nil;
  inherited InternalOpenFile();

  EncodingNode := nil;
  MainNode     := nil;
  EoFNode      := nil;

  // update full source path
  FileStream.Path  := Self.FullPath;
  // indicate requested initial state of stream
  FileStream.State := UMLCstatestreams.ssReset;

  // --> scan or parse file
  if (Scanner.Start()) then
  begin
    // initial assignation, will change
    Self.CurrentNode := TUMLCSXMLTreeNode(Self.TreeContainer.Items.Root());

    UMLCxmlfileansisymbols.Clear((* var *) ASymbol);

    while (Scanner.Next()) do
    begin
      Scanner.ReadCurrentSymbol((* var *) ASymbol);
      ProcessSymbol((* var *) ASymbol);
    end;

    ASymbol.Token := UMLCxmlfiletokens.xmlfiletkEoF;
    ASymbol.Text  := #26;
    ProcessSymbol((* var *) ASymbol);

    Scanner.Finish();
  end;
end;

constructor TCustomUMLCANSIXMLDocument.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  // provides a filesystem access stream
  FileStream := TUMLCFileStream.Create(nil);

  // provides a text line based stream
  TextStream := TCustomUMLCAnsiTextStream.Create(nil);
  // assign source & destination to filesystem
  TextStream.Reference := FileStream;

  Scanner  := TCustomUMLCXMLFileANSIScanner.Create(nil);

  // assign options
  Scanner.Options().Clear();
  Scanner.Options().EoF      := UMLCscanneroptions.scnopReturnTag;
  Scanner.Options().EoPg     := UMLCscanneroptions.scnopReturnTag;
  Scanner.Options().EoLn     := UMLCscanneroptions.scnopReturnTag;
  Scanner.Options().Tabs     := UMLCscanneroptions.scnopReturnTag;
  Scanner.Options().Spaces   := UMLCscanneroptions.scnopReturnTag;
  Scanner.Options().Specials := UMLCscanneroptions.scnopReturnTag;

  // assign source of text
  Scanner.Stream := TextStream;

  // create tree-container & tree-collection.
  // without any items, no even, root-item
  TreeContainer := TCustomUMLCSXMLTreeContainer.Create(nil);
end;

destructor TCustomUMLCANSIXMLDocument.Destroy();
begin
  TreeContainer.Free();

  // assign source of text
  Scanner.Stream := nil;

  // assign options
  Scanner.Options().Clear();

  Scanner.Free();

  // assign source & destination to filesystem
  TextStream.Reference := nil;

  // provides a text line based stream
  TextStream.Free();

  // provides a filesystem access stream
  FileStream.Free();

  inherited Destroy();
end;

procedure TCustomUMLCANSIXMLDocument.GenerateSXML
(var ANode: TUMLCTreeNode;
 const AParam: pointer; const ADirection: TUMLCTreeDirection);
var S: PString;
    ASXMLNode: TUMLCSXMLTreeNode;
    ATag: TUMLCXMLFileTreeNodeToken;
begin
  S := PString(AParam);
  // ...

  ASXMLNode := TUMLCSXMLTreeNode(ANode);
  ATag := ASXMLNode.TreeToken;

  if (ADirection = UMLCtreenodes.tdStart) then
  begin
    case (ATag) of
      UMLCxmlfiletreenodetokens.xmlfiletrntkEoPg:
        begin
          S^ := S^ + #13#10 + UMLCstrings.StringOfChar('-', 40) + #13#10;
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkEoLn:
        begin
          S^ := S^ + #13#10;
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkSpace:
        begin
          S^ := S^ + resSharedTagSpace;
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkEncoding:
        begin
          S^ := S^ + '<?' + ASXMLNode.TextValue + '?>';
          //if (ASXMLNode.NewLineAfter) then
          if (ASXMLNode.Selected) then
          begin
            S^ := S^ + #13#10;
          end;
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkBlock:
        begin
          S^ := S^ + '<' + ASXMLNode.TextValue + '>';
          //if (ASXMLNode.NewLineAfter) then
          if (ASXMLNode.Selected) then
          begin
            S^ := S^ + #13#10;
          end;
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkSingle:
        begin
          S^ := S^ + '<' + ASXMLNode.TextValue + '>';
          //if (ASXMLNode.NewLineAfter) then
          if (ASXMLNode.Selected) then
          begin
            S^ := S^ + #13#10;
          end;
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkText:
        begin
          S^ := S^ + ASXMLNode.TextValue;
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkComment:
        begin
          S^ := S^ + ASXMLNode.TextValue;
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkEoF:
        begin
          Self.DoNothing();
        end;
    end;
  end else if
    (ATag = UMLCxmlfiletreenodetokens.xmlfiletrntkBlock) then
  begin
    S^ := S^ + '</' + ASXMLNode.TextValue + '>';

    (*
    //if (ASXMLNode.NewLineAfter) then
    if (ASXMLNode.Selected) then
    begin
      S^ := S^ + #13#10;
    end;
    *)
  end;
end;

function TCustomUMLCANSIXMLDocument.CreateRoot(): TUMLCSXMLTreeNode;
begin
  Result := nil;

    Self.RootNode :=
       TUMLCSXMLTreeNode(Self.TreeContainer.Items.InsertRoot());

    Self.CurrentNode := Self.RootNode;

  Result := Self.RootNode;
end;

procedure TCustomUMLCANSIXMLDocument.GenerateDocument(var AText: string);
begin
  AText := '';

  {$ifdef Delphi}
  TreeContainer.Items.Root().ForBoth(GenerateSXML, @AText);
  {$endif}

  {$ifdef FPC}
  TreeContainer.Items.Root().ForBoth(@GenerateSXML, @AText);
  {$endif}
end;


end.

