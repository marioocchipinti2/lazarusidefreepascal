unit tuifonts;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, matrices;

type
  TDOSCPIFontGlyphRecord = record
    ItemIndex: LongWord;

    // ...
  end;

  PDOSCPIFontGlyphRecord = ^TDOSCPIFontGlyphRecord;

  TDOSCPIFontGlyphExRecord = record
    Header: TDOSCPIFontGlyphRecord;

    Pixels: Matrix;

    // ...
  end;

  PDOSCPIFontGlyphExRecord = ^TDOSCPIFontGlyphExRecord;

  TDOSCPIFontResolutionRecord = record
    FontWidth:  Integer;
    FontHeight: Integer;

    FontGlyphCount: Integer;

    // ...
  end;

  TDOSCPIFontResolutionExRecord = record
    List:   TList;

    Header: TDOSCPIFontResolutionRecord;

    Extra:  LongInt;
  end;

  PDOSCPIFontResolutionExRecord = ^TDOSCPIFontResolutionExRecord;

  TDOSCPIFontCodePageRecord = record
    FontCodePageNum:     Integer;

    FontCodePageText:    string[50];

    FontResolutionCount: Integer;

    // ...
  end;

  PDOSCPIFontCodePageRecord = ^TDOSCPIFontCodePageRecord;

  TDOSCPIFontCodePageExRecord = record
    List:   TList;

    Header: TDOSCPIFontCodePageRecord;

    Extra:  LongInt;
  end;

  PDOSCPIFontCodePageExRecord = ^TDOSCPIFontCodePageExRecord;

  TDOSCPIFontFolderRecord = record
    FontName:  string[50];
    FontDescr: string[255];

    FontPath:  string[255];

    FontCodePageCount: Integer;

    // ...
  end;

  TDOSCPIFontFolderExRecord = record
    List:   TList;

    Header: TDOSCPIFontFolderRecord;

    Extra:  LongInt;
  end;

  PDOSCPIFontFolderExRecord = ^TDOSCPIFontFolderExRecord;

  procedure AddFontCodePage
    (var AFontFolder: PDOSCPIFontFolderExRecord;
     var AFontCodePage: PDOSCPIFontCodePageExRecord);

  procedure AddFontResolution
    (var AFontCodePage: PDOSCPIFontCodePageExRecord;
     var AFontResolution: PDOSCPIFontResolutionExRecord);

  procedure AddFontGlyph
    (var AFontResolution: PDOSCPIFontResolutionExRecord;
     var AFontGlyph: PDOSCPIFontGlyphExRecord);

  procedure ClearFontFolder
    (var AFontFolder: PDOSCPIFontFolderExRecord);

  procedure ClearFontCodePage
    (var AFontCodePage: PDOSCPIFontCodePageExRecord);

  procedure ClearFontResolution
    (var AFontResolution: PDOSCPIFontResolutionExRecord);

  function CreateFontFolder: PDOSCPIFontFolderExRecord;

  function CreateFontFolderByName
      (const AFolderName: string;
       const AFolderDescr: string): PDOSCPIFontFolderExRecord;

  procedure DropFontFolder
    (var AFontFolder: PDOSCPIFontFolderExRecord);

  procedure SaveFontGlyph
    (var F: File; var AFontGlyph: PDOSCPIFontGlyphExRecord);

  procedure SaveFontResolution
    (var F: File; var AFontResolution: PDOSCPIFontResolutionExRecord);

  procedure SaveFontCodePage
    (var F: File; var AFontCodePage: PDOSCPIFontCodePageExRecord);

  procedure SaveFontFolder
    (var F: File; var AFontFolder: PDOSCPIFontFolderExRecord);

  procedure SaveFont
    (var F: File; var AFontFolder: PDOSCPIFontFolderExRecord);

  function CreateFontGlyph: PDOSCPIFontGlyphExRecord;

  function CreateFontResolution: PDOSCPIFontResolutionExRecord;

  function CreateFontCodePage: PDOSCPIFontCodePageExRecord;

  procedure LoadFontGlyph
    (var F: File; var AFontGlyph: PDOSCPIFontGlyphExRecord);

  procedure LoadFontResolution
    (var F: File; var AFontResolution: PDOSCPIFontResolutionExRecord);

  procedure LoadFontCodePage
    (var F: File; var AFontCodePage: PDOSCPIFontCodePageExRecord);

  procedure LoadFontFolder
    (var F: File; var AFontFolder: PDOSCPIFontFolderExRecord);

  procedure LoadFont
    (var F: File; var AFontFolder: PDOSCPIFontFolderExRecord);

  function CreateAddFontCodePage
    (var   AFontFolder:       PDOSCPIFontFolderExRecord;
     const AFontCodePageNum:  Integer;
     const AFontCodePageText: string): PDOSCPIFontCodePageExRecord;

  function DeleteFontCodePage
    (var   AFontFolder: PDOSCPIFontFolderExRecord;
     const AIndex: Integer): Boolean;

  function CreateAddFontResolution
    (var   AFontCodePage: PDOSCPIFontCodePageExRecord;
     const AFontWidth:    Integer;
     const AFontHeight:   Integer): PDOSCPIFontResolutionExRecord;

  function DeleteFontResolution
    (var   AFontCodePage: PDOSCPIFontCodePageExRecord;
     const AIndex: Integer): Boolean;

    function CreateAddFontGlyph
      (var   AFontResolution: PDOSCPIFontResolutionExRecord;
       const AItemIndex: Integer): PDOSCPIFontGlyphExRecord;

implementation

procedure AddFontCodePage
  (var AFontFolder: PDOSCPIFontFolderExRecord;
   var AFontCodePage: PDOSCPIFontCodePageExRecord);
var CanContinue: Boolean;
begin
  CanContinue :=
    (AFontFolder <> nil);
  if (CanContinue) then
  begin
    AFontFolder^.List.Add(AFontCodePage);
    AFontFolder^.Header.FontCodePageCount :=
      (AFontFolder^.Header.FontCodePageCount + 1);
  end; // if (CanContinue) then
end;

procedure AddFontResolution
  (var AFontCodePage: PDOSCPIFontCodePageExRecord;
   var AFontResolution: PDOSCPIFontResolutionExRecord);
var CanContinue: Boolean;
begin
  CanContinue :=
    (AFontCodePage <> nil);
  if (CanContinue) then
  begin
    AFontCodePage^.List.Add(AFontResolution);
    AFontCodePage^.Header.FontResolutionCount :=
      (AFontCodePage^.Header.FontResolutionCount + 1);
  end; // if (CanContinue) then
end;

procedure AddFontGlyph
  (var AFontResolution: PDOSCPIFontResolutionExRecord;
   var AFontGlyph: PDOSCPIFontGlyphExRecord);
var CanContinue: Boolean;
begin
  CanContinue :=
    (AFontResolution <> nil);
  if (CanContinue) then
  begin
    AFontResolution^.List.Add(AFontGlyph);
    AFontResolution^.Header.FontGlyphCount :=
      (AFontResolution^.Header.FontGlyphCount + 1);
  end; // if (CanContinue) then
end;

procedure ClearFontFolder
  (var AFontFolder: PDOSCPIFontFolderExRecord);
var CanContinue: Boolean;
begin
  CanContinue :=
    (AFontFolder <> nil);
  if (CanContinue) then
  begin
    //AFontFolder^.List.Add(AFontGlyph);
    //AFontFolder^.Header.FontGlyphCount :=
    //  (AFontFolder^.Header.FontGlyphCount + 1);
  end; // if (CanContinue) then
end;

procedure ClearFontCodePage
  (var AFontCodePage: PDOSCPIFontCodePageExRecord);
var CanContinue: Boolean;
begin
  CanContinue :=
    (AFontCodePage <> nil);
  if (CanContinue) then
  begin
    //AFontCodePage^.List.Add(AFontGlyph);
    //AFontCodePage^.Header.FontGlyphCount :=
    //  (AFontCodePage^.Header.FontGlyphCount + 1);
  end; // if (CanContinue) then
end;

procedure ClearFontResolution
  (var AFontResolution: PDOSCPIFontResolutionExRecord);
var CanContinue: Boolean;
begin
  CanContinue :=
    (AFontResolution <> nil);
  if (CanContinue) then
  begin
    //AFontResolution^.List.Add(AFontGlyph);
    //AFontResolution^.Header.FontGlyphCount :=
    //  (AFontResolution^.Header.FontGlyphCount + 1);
  end; // if (CanContinue) then
end;

function CreateFontFolder: PDOSCPIFontFolderExRecord;
var AFontFolder: PDOSCPIFontFolderExRecord;
begin
  Result := nil;

  AFontFolder := new (PDOSCPIFontFolderExRecord);
  System.Fillchar(AFontFolder^, sizeof(TDOSCPIFontFolderExRecord), 0);

  AFontFolder^.List := TList.Create;

  AFontFolder^.Header.FontCodePageCount := 0;

  AFontFolder^.Header.FontName  := '';
  AFontFolder^.Header.FontDescr := '';
  AFontFolder^.Header.FontPath  := '';

  Result := AFontFolder;
end;

function CreateFontFolderByName
  (const AFolderName: string;
   const AFolderDescr: string): PDOSCPIFontFolderExRecord;
var AFontFolder: PDOSCPIFontFolderExRecord;
begin
  Result := nil;

  AFontFolder := new (PDOSCPIFontFolderExRecord);
  System.Fillchar(AFontFolder^, sizeof(TDOSCPIFontFolderExRecord), 0);

  AFontFolder^.List := TList.Create;

  AFontFolder^.Header.FontCodePageCount := 0;

  AFontFolder^.Header.FontName  := AFolderName;
  AFontFolder^.Header.FontDescr := AFolderDescr;
  AFontFolder^.Header.FontPath  := '';

  Result := AFontFolder;
end;

procedure DropFontFolder
  (var AFontFolder: PDOSCPIFontFolderExRecord);
var AFontResolutionRecord: PDOSCPIFontResolutionExRecord;
    I: Integer;
begin
  if (AFontFolder <> nil) then
  begin
    for I := Pred(AFontFolder^.List.Count) to 0 do
    begin
      AFontResolutionRecord :=
        PDOSCPIFontResolutionExRecord(AFontFolder^.List[I]);

      AFontFolder^.List.Extract(AFontResolutionRecord);

      Dispose(AFontResolutionRecord);
    end;
    AFontFolder^.List.Free;

    System.Fillchar(AFontFolder^, sizeof(TDOSCPIFontFolderExRecord), 0);
  end;
end;

procedure SaveFontGlyph
  (var F: File; var AFontGlyph: PDOSCPIFontGlyphExRecord);
var CanContinue: Boolean; BlockWriteCount: Int64;
    SizeInBytes: Integer;
    MatrixHeader: PMatrixHeader;
begin
  CanContinue :=
    (AFontGlyph <> nil);
  if (CanContinue) then
  begin
    // save each font glyph header
    BlockWriteCount := 0;
    SizeInBytes := SizeOf(AFontGlyph^.Header);
    BlockWrite
      (F, AFontGlyph^.Header, SizeInBytes, BlockWriteCount);

    // save each glyph items
    MatrixHeader := PMatrixHeader(AFontGlyph^.Pixels);
    SizeInBytes :=
      (MatrixHeader^.RowCount * MatrixHeader^.ColCount * MatrixHeader^.ItemSize);

    BlockWrite
      (F, AFontGlyph^.Pixels^, SizeInBytes, BlockWriteCount);
  end;
end;

procedure SaveFontResolution
  (var F: File; var AFontResolution: PDOSCPIFontResolutionExRecord);
var CanContinue: Boolean; BlockWriteCount: Int64;
    SizeInBytes: Integer;
    I:           Integer;
    AFontGlyphCount: Integer;
    AFontGlyph: PDOSCPIFontGlyphExRecord;
begin
  CanContinue :=
    (AFontResolution <> nil);
  if (CanContinue) then
  begin
    // save each font resolution header
    BlockWriteCount := 0;
    SizeInBytes := SizeOf(AFontResolution^.Header);
    BlockWrite
      (F, AFontResolution^.Header, SizeInBytes, BlockWriteCount);

    if (BlockWriteCount > 0) then
    begin
      // save glyph items
      AFontGlyphCount := AFontResolution^.Header.FontGlyphCount;
      if (AFontGlyphCount > 0) then
      begin
        for I := 0 to Pred(AFontGlyphCount) do
        begin
          AFontGlyph :=
            PDOSCPIFontGlyphExRecord(AFontResolution^.List[I]);

          SaveFontGlyph(F, AFontGlyph);
        end;
      end; // if (AFontGlyphCount > 0) then
    end; // if (BlockWriteCount > 0) then

  end; // if (CanContinue) then
end;

procedure SaveFontCodePage
  (var F: File; var AFontCodePage: PDOSCPIFontCodePageExRecord);
var CanContinue: Boolean; BlockWriteCount: Int64;
    SizeInBytes: Integer;
    I:           Integer;
    AFontResolutionCount: Integer;
    AFontResolution: PDOSCPIFontResolutionExRecord;
begin
  CanContinue :=
    (AFontCodePage <> nil);
  if (CanContinue) then
  begin
    // save each codepage header
    BlockWriteCount := 0;
    SizeInBytes := SizeOf(AFontCodePage^.Header);
    BlockWrite
      (F, AFontCodePage^.Header, SizeInBytes, BlockWriteCount);

    if (BlockWriteCount > 0) then
    begin
      // save codepage items
      AFontResolutionCount := AFontCodePage^.Header.FontResolutionCount;
      if (AFontResolutionCount > 0) then
      begin
        for I := 0 to Pred(AFontResolutionCount) do
        begin
          AFontResolution :=
            PDOSCPIFontResolutionExRecord(AFontCodePage^.List[I]);

          SaveFontResolution(F, AFontResolution);
        end;
      end; // if (AFontResolutionCount > 0) then
    end; // if (BlockWriteCount > 0) then

  end; // if (CanContinue) then
end;

procedure SaveFontFolder
  (var F: File; var AFontFolder: PDOSCPIFontFolderExRecord);
var CanContinue: Boolean; BlockWriteCount: Int64;
    SizeInBytes: Integer;
    I:           Integer;
    AFontCodePageCount: Integer;
    AFontCodePage: PDOSCPIFontCodePageExRecord;
begin
  CanContinue :=
    (AFontFolder <> nil);
  if (CanContinue) then
  begin
    // save fontfolder header
    BlockWriteCount := 0;
    SizeInBytes := SizeOf(AFontFolder^.Header);
    BlockWrite
      (F, AFontFolder^.Header, SizeInBytes, BlockWriteCount);

    if (BlockWriteCount > 0) then
    begin
      // save each codepage
      AFontCodePageCount := AFontFolder^.Header.FontCodePageCount;
      if (AFontCodePageCount > 0) then
      begin
        for I := 0 to Pred(AFontCodePageCount) do
        begin
          AFontCodePage :=
            PDOSCPIFontCodePageExRecord(AFontFolder^.List[I]);

          SaveFontCodePage(F, AFontCodePage);
        end;
      end; // if (AFontCodePageCount > 0) then
    end; // if (BlockWriteCount > 0) then

  end; // if (CanContinue) then
end;

procedure SaveFont
  (var F: File; var AFontFolder: PDOSCPIFontFolderExRecord);
var CanContinue: Boolean;
begin
  CanContinue :=
    (AFontFolder <> nil);
  if (CanContinue) then
  begin
    Rewrite(F);
    SaveFontFolder(F, AFontFolder);
    Close(F);
  end; // if (CanContinue) then
end;

function CreateFontGlyph: PDOSCPIFontGlyphExRecord;
begin
  Result := new (PDOSCPIFontGlyphExRecord);
  FillChar(Result^, sizeof(TDOSCPIFontGlyphExRecord), 0);

  Result^.Header.ItemIndex := 0;
  Result^.Pixels := nil;
end;

function CreateFontResolution: PDOSCPIFontResolutionExRecord;
begin
  Result := new (PDOSCPIFontResolutionExRecord);
  FillChar(Result^, sizeof(TDOSCPIFontResolutionExRecord), 0);

  Result^.Header.FontWidth  := 0;
  Result^.Header.FontHeight := 0;
  Result^.Header.FontGlyphCount := 0;

  Result^.List := TList.Create;
end;

function CreateFontCodePage: PDOSCPIFontCodePageExRecord;
begin
  Result := new (PDOSCPIFontCodePageExRecord);
  FillChar(Result^, sizeof(TDOSCPIFontCodePageExRecord), 0);

  Result^.Header.FontCodePageNum  := 0;
  Result^.Header.FontCodePageText := '';
  Result^.Header.FontResolutionCount := 0;

  Result^.List := TList.Create;
end;

procedure LoadFontGlyph
  (var F: File; var AFontGlyph: PDOSCPIFontGlyphExRecord);
var CanContinue: Boolean; BlockReadCount: Int64;
    SizeInBytes: Integer;
begin
  CanContinue :=
    (AFontGlyph <> nil);
  if (CanContinue) then
  begin
    // load each font glyph header
    BlockReadCount := 0;
    SizeInBytes := SizeOf(AFontGlyph^.Header);
    BlockRead
      (F, AFontGlyph^.Header, SizeInBytes, BlockReadCount);

    // load each glyph items
    SizeInBytes :=
      matrices.ReadSize(AFontGlyph^.Pixels);

    BlockRead
      (F, AFontGlyph^.Pixels^, SizeInBytes, BlockReadCount);
  end;
end;

procedure LoadFontResolution
  (var F: File; var AFontResolution: PDOSCPIFontResolutionExRecord);
var CanContinue: Boolean; BlockReadCount: Int64;
    I, SizeInBytes: Integer;
    AFontGlyphCount: Integer;
    AFontGlyph: PDOSCPIFontGlyphExRecord;
begin
  CanContinue :=
    (AFontResolution <> nil);
  if (CanContinue) then
  begin
    // load each font resolution header
    BlockReadCount := 0;
    SizeInBytes := SizeOf(AFontResolution^.Header);
    BlockRead
      (F, AFontResolution^.Header, SizeInBytes, BlockReadCount);

    // load glyph items
    AFontGlyphCount :=
      AFontResolution^.Header.FontGlyphCount;
    if (AFontGlyphCount > 0) then
    begin
      for I := 0 to Pred(AFontGlyphCount) do
      begin
        AFontGlyph :=
          CreateFontGlyph;

        AddFontGlyph
          (AFontResolution, AFontGlyph);

        LoadFontGlyph(F, AFontGlyph);
      end;
    end; // if (AFontGlyphCount > 0) then
  end; // if (CanContinue) then
end;

procedure LoadFontCodePage
  (var F: File; var AFontCodePage: PDOSCPIFontCodePageExRecord);
var CanContinue: Boolean; BlockReadCount: Int64;
    I, SizeInBytes: Integer;
    AFontResolutionCount: Integer;
    AFontResolution: PDOSCPIFontResolutionExRecord;
begin
  CanContinue :=
    (AFontCodePage <> nil);
  if (CanContinue) then
  begin
    // load each codepage header
    BlockReadCount := 0;
    SizeInBytes := SizeOf(AFontCodePage^.Header);
    BlockRead
      (F, AFontCodePage^.Header, SizeInBytes, BlockReadCount);

    // load codepage items
    AFontResolutionCount :=
      AFontCodePage^.Header.FontResolutionCount;
    if (AFontResolutionCount > 0) then
    begin
      for I := 0 to Pred(AFontResolutionCount) do
      begin
        AFontResolution :=
          CreateFontResolution;

        AddFontResolution
          (AFontCodePage, AFontResolution);

        LoadFontResolution(F, AFontResolution);
      end;
    end; // if (AFontResolutionCount > 0) then
  end; // if (CanContinue) then
end;

procedure LoadFontFolder
  (var F: File; var AFontFolder: PDOSCPIFontFolderExRecord);
var CanContinue: Boolean; BlockReadCount: Int64;
    I, SizeInBytes: Integer;
    AFontCodePageCount: Integer;
    AFontCodePage: PDOSCPIFontCodePageExRecord;
begin
  CanContinue :=
    (AFontFolder <> nil);
  if (CanContinue) then
  begin
    // load fontfolder header
    BlockReadCount := 0;
    SizeInBytes := SizeOf(TDOSCPIFontFolderRecord); // SizeOf(AFontFolder^.Header);
    BlockRead
      (F, AFontFolder^.Header, SizeInBytes, BlockReadCount);

    CanContinue :=
      (BlockReadCount > 0);
    if (CanContinue) then
    begin
      // load each codepage
      AFontCodePageCount :=
        AFontFolder^.Header.FontCodePageCount;
      if (AFontCodePageCount > 0) then
      begin
        for I := 0 to Pred(AFontCodePageCount) do
        begin
          AFontCodePage :=
            CreateFontCodePage();

          AddFontCodePage
            (AFontFolder, AFontCodePage);

          LoadFontCodePage(F, AFontCodePage);
        end;
      end; // if (AFontCodePageCount > 0) then
    end; // if (CanContinue) then
  end; // if (CanContinue) then
end;

procedure LoadFont
  (var F: File; var AFontFolder: PDOSCPIFontFolderExRecord);
begin
  AFontFolder :=
    CreateFontFolder;
  Reset(F);
  LoadFontFolder(F, AFontFolder);
  Close(F);
end;

function CreateAddFontCodePage
  (var   AFontFolder:       PDOSCPIFontFolderExRecord;
   const AFontCodePageNum:  Integer;
   const AFontCodePageText: string): PDOSCPIFontCodePageExRecord;
begin
  Result := nil;

  if (AFontFolder <> nil) then
  begin
    Result := CreateFontCodePage;

    Result^.Header.FontCodePageNum  := AFontCodePageNum;
    Result^.Header.FontCodePageText := AFontCodePageText;

    AddFontCodePage
      (AFontFolder, Result);
  end;
end;

function DeleteFontCodePage
  (var   AFontFolder: PDOSCPIFontFolderExRecord;
   const AIndex: Integer): Boolean;
begin
  Result := (AFontFolder <> nil);

  if (AFontFolder <> nil) then
  begin
    // toxdo: release memory

    AFontFolder^.List.Delete(AIndex);
    AFontFolder^.Header.FontCodePageCount :=
      (AFontFolder^.Header.FontCodePageCount - 1);
  end;
end;

function CreateAddFontResolution
  (var   AFontCodePage: PDOSCPIFontCodePageExRecord;
   const AFontWidth:    Integer;
   const AFontHeight:   Integer): PDOSCPIFontResolutionExRecord;
begin
  Result := nil;

  if (AFontCodePage <> nil) then
  begin
    Result :=
      CreateFontResolution;

    Result^.Header.FontWidth  := AFontWidth;
    Result^.Header.FontHeight := AFontHeight;

    AddFontResolution
      (AFontCodePage, Result);
  end;
end;

function DeleteFontResolution
  (var   AFontCodePage: PDOSCPIFontCodePageExRecord;
   const AIndex: Integer): Boolean;
begin
  Result := (AFontCodePage <> nil);

  if (AFontCodePage <> nil) then
  begin
    // toxdo: release subitems

    AFontCodePage^.List.Delete(AIndex);
    AFontCodePage^.Header.FontResolutionCount :=
      (AFontCodePage^.Header.FontResolutionCount - 1);
  end;
end;

function CreateAddFontGlyph
  (var   AFontResolution: PDOSCPIFontResolutionExRecord;
   const AItemIndex: Integer): PDOSCPIFontGlyphExRecord;
begin
  Result := nil;

  if (AFontResolution <> nil) then
  begin
    Result :=
      CreateFontGlyph;

    Result^.Header.ItemIndex := AItemIndex;

    Result^.Pixels :=
      matrices.Create
        (AFontResolution^.Header.FontWidth,
         AFontResolution^.Header.FontHeight,
         sizeof(Boolean));

    // ---

    AddFontGlyph
      (AFontResolution, Result);
  end;
end;

end.

