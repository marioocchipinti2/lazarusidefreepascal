unit matrices;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  dummy;

type
  Matrix = pointer;

type
  TMatrixHeader = record
    RowCount: Integer;
    ColCount: Integer;
    ItemSize: Integer;

    Items: Pointer;
  end;

  PMatrixHeader = ^TMatrixHeader;

  function Create
    (const ARowCount: Integer;
     const AColCount: Integer;
     const AItemSize: Integer): Matrix;

  procedure Destroy(var AMatrix: Matrix);

  function ReadSize
    (var AMatrix: Matrix): Integer;

  function At
    (var AMatrix: Matrix;
     const AColIndex: Integer;
     const ARowIndex: Integer
     ): pointer;

implementation

function Create
  (const ARowCount: Integer;
   const AColCount: Integer;
   const AItemSize: Integer): Matrix;
var ASize: Integer;
    MatrixHeader: PMatrixHeader;
begin
  Result := nil;

  MatrixHeader := new(PMatrixHeader);

  MatrixHeader^.RowCount := ARowCount;
  MatrixHeader^.ColCount := AColCount;
  MatrixHeader^.ItemSize := AItemSize;
  MatrixHeader^.Items    := nil;

  ASize := (ARowCount * AColCount * AItemSize);
  GetMem(MatrixHeader^.Items, ASize);

  Fillchar(MatrixHeader^.Items^, ASize, 0);

  Result := MatrixHeader;
end;

procedure Destroy(var AMatrix: Matrix);
var ASize: Integer;
    MatrixHeader: PMatrixHeader;
begin
  MatrixHeader := PMatrixHeader(AMatrix);

  ASize := (MatrixHeader^.RowCount * MatrixHeader^.ColCount * MatrixHeader^.ItemSize);
  Fillchar(MatrixHeader^.Items, ASize, 0);

  FreeMem(MatrixHeader^.Items, ASize);

  dispose(MatrixHeader);

  AMatrix := nil;
end;

function ReadSize
  (var AMatrix: Matrix): Integer;
var MatrixHeader: PMatrixHeader;
begin
  Result := 0;

  MatrixHeader := PMatrixHeader(AMatrix);

  Result :=
    (MatrixHeader^.RowCount * MatrixHeader^.ColCount * MatrixHeader^.ItemSize);
end;

function At
  (var AMatrix: Matrix;
   const AColIndex: Integer;
   const ARowIndex: Integer
   ): pointer;
var MatrixHeader: PMatrixHeader; SizeInBytes: Integer;
begin
  Result := nil;

  MatrixHeader := PMatrixHeader(AMatrix);

  SizeInBytes :=
    (AColIndex * ARowIndex * MatrixHeader^.ItemSize);
  Result :=
    (MatrixHeader^.Items + SizeInBytes);
end;


end.

