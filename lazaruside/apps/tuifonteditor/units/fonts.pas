unit fonts;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  FontFolderHeaderRecord = record
    Font:          array[0 .. 6] of char;
    reserved:    array[0 .. 7] of char;
    pnum:        Smallint;
    ptyp:        char;
    fih_offset:  Longint;
  end;

  FontInfoHeaderRecord = record
    num_codepages: Smallint;
  end;

implementation

end.

