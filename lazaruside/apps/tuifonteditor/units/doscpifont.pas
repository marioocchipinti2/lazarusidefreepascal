unit doscpifont;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  matrices;

type
  FontFileHeaderRecord = record
    id0:         char;
    id:          array[0 .. 6] of char;
    reserved:    array[0 .. 7] of char;
    pnum:        Smallint;
    ptyp:        char;
    fih_offset:  Longint;
  end;

  FontInfoHeaderRecord = record
    num_codepages: Smallint;
  end;

  CodePageEntryHeaderRecord = record
    cpeh_size:        Smallint;
    next_cpeh_offset: Longint;
    device_type:      Smallint;
    device_name:      array[0 .. 7] of char;
    codepage:         Smallint;
    reserved:         array[0 .. 5] of char;
    cpih_offset:      Longint;
  end;

  CodePageInfoHeaderRecord = record
    version:   Smallint;
    num_fonts: Smallint;
    size:      Smallint;
  end;

  ScreenFontHeaderRecord = record
    height:    char;
    width:     char;
    yaspect:   char;
    xaspect:   char;
    num_chars: Smallint;
  end;

  WindowsFontHeaderRecord = record
      version:               SmallInt;
      file_size:             LongInt;
      copyright:             array[0 .. 59] of byte;
      file_type:             SmallInt;
      nominal_point_size:    SmallInt;
      vertical_resolution:   SmallInt;
      horizontal_resolution: SmallInt;
      ascent:                SmallInt;
      internal_leading:      SmallInt;
      external_leading:      SmallInt;
      italic:                byte;
      underline:             byte;
      strike_out:            byte;
      weight:                SmallInt;
      charset:               byte;
      pixel_width:           SmallInt;
      pixel_height:          SmallInt;
      pitch_and_family:      byte;
      avg_width:             SmallInt;
      max_width:             SmallInt;
      first_char:            byte;
      last_char:             byte;
      default_char:          byte;
      break_char:            byte;
      bytes_per_row:         SmallInt;
      device_offset:         LongInt;
      face_name_offset:      LongInt;
      bits_pointer:          LongInt;
      bits_offset:           LongInt;
      reserved:              byte;
      flags:                 LongInt;
      A_space:               SmallInt;
      B_space:               SmallInt;
      C_space:               SmallInt;
      color_table_offset:    SmallInt;
      reserved1:             array[0 .. 3] of LongInt;
  end;

implementation

end.

