unit ufrmfontcodepage;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, Menus,
  ExtCtrls, StdCtrls, ActnList,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcinputdlgs, umlcmsgdlgs,
  udmbuttons,
  tuifonts,
  ufrmfontresolution;

type

  { Tfrmfontcodepage }

  Tfrmfontcodepage = class(TForm)
    aclsMain: TActionList;
    acFileOpen: TAction;
    acFileExit: TAction;
    acRecordAdd: TAction;
    acRecordEdit: TAction;
    acRecordDelete: TAction;
    acFileSave: TAction;
    edFontName: TEdit;
    edFontCodePage: TEdit;
    gbMain: TGroupBox;
    lblFontName: TLabel;
    lblFontCodePage: TLabel;
    lbResolutions: TListBox;
    miFileOpen: TMenuItem;
    N1: TMenuItem;
    miFileNew: TMenuItem;
    mmMainMenu: TMainMenu;
    mniFile: TMenuItem;
    mniFileExit: TMenuItem;
    pnMain: TPanel;
    pnTitle: TPanel;
    sbStatusBar: TStatusBar;
    tobaMain: TToolBar;
    tobtnFileExit: TToolButton;
    tobtnRecordAdd: TToolButton;
    tobtnRecordDelete: TToolButton;
    ToolButton1: TToolButton;
    tobtnRecordEdit: TToolButton;
    procedure acFileDeleteExecute(Sender: TObject);
    procedure acFileExitExecute(Sender: TObject);
    procedure acFileNewExecute(Sender: TObject);
    procedure acFileOpenExecute(Sender: TObject);
    procedure acFileSaveExecute(Sender: TObject);
    procedure acRecordAddExecute(Sender: TObject);
    procedure acRecordDeleteExecute(Sender: TObject);
    procedure acRecordEditExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  protected
    IsActivated: Boolean;
  public
    FontFolder:   PDOSCPIFontFolderExRecord;
    FontCodePage: PDOSCPIFontCodePageExRecord;

    procedure FormActivateFirst(Sender: TObject);

    procedure RefreshList();
  end;

  procedure Execute
    (const AFontFolder:   PDOSCPIFontFolderExRecord;
     const AFontCodePage: PDOSCPIFontCodePageExRecord);

implementation

{$R *.lfm}

procedure Execute
  (const AFontFolder:   PDOSCPIFontFolderExRecord;
   const AFontCodePage: PDOSCPIFontCodePageExRecord);
var ThisForm: Tfrmfontcodepage;
begin
  ThisForm := Tfrmfontcodepage.Create(Application);
  ThisForm.FontFolder   := AFontFolder;
  ThisForm.FontCodePage := AFontCodePage;
    ThisForm.ShowModal;
  ThisForm.Free;
end;

{ Tfrmfontcodepage }

procedure Tfrmfontcodepage.FormCreate(Sender: TObject);
begin
  Self.IsActivated := false;
end;

procedure Tfrmfontcodepage.FormActivate(Sender: TObject);
begin
  if (not Self.IsActivated) then
  begin
    Self.FormActivateFirst(Sender);
  end;
  Self.IsActivated := true;
end;

procedure Tfrmfontcodepage.acFileNewExecute(Sender: TObject);
begin
end;

procedure Tfrmfontcodepage.acFileOpenExecute(Sender: TObject);
begin
end;

procedure Tfrmfontcodepage.acFileSaveExecute(Sender: TObject);
begin
  //
end;

procedure Tfrmfontcodepage.acRecordAddExecute(Sender: TObject);
var FontWidth:  Integer;
    FontHeight: Integer;
    ATitle, ALabel: string;
    AIntValue: Integer;
begin
  ATitle := 'New Resolution';

  ALabel := 'Font Resolution Width:';
  AIntValue := 0;
  if (InputInteger(ATitle, ALabel, AIntValue)) then
  begin
    FontWidth := AIntValue;

    ALabel := 'Font Resolution Height:';
    AIntValue := 0;
    if (InputInteger(ATitle, ALabel, AIntValue)) then
    begin
      FontHeight := AIntValue;

      CreateAddFontResolution(Self.FontCodePage, FontWidth, FontHeight);

      RefreshList();
    end;
  end;
end;

procedure Tfrmfontcodepage.acRecordDeleteExecute(Sender: TObject);
begin
  //
end;

procedure Tfrmfontcodepage.acRecordEditExecute(Sender: TObject);
var I: Integer;
    AFontResolution: PDOSCPIFontResolutionExRecord;
begin
  I := lbResolutions.ItemIndex;

  AFontResolution :=
    Self.FontCodePage^.List[I];

  ufrmfontresolution.Execute
    (Self.FontFolder, Self.FontCodePage, AFontResolution);
end;

procedure Tfrmfontcodepage.acFileExitExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure Tfrmfontcodepage.acFileDeleteExecute(Sender: TObject);
begin
  //
end;

procedure Tfrmfontcodepage.FormActivateFirst(Sender: TObject);
var AFontName:     string[50];
    AFontCodePageText: string[50];
begin
  AFontName := FontFolder^.Header.FontName;
  edFontName.Text := AFontName;

  AFontCodePageText := FontCodePage^.Header.FontCodePageText;
  edFontCodePage.Text := AFontCodePageText;

  // display available resolutions per codepage on listbox control
  RefreshList();
end;

procedure Tfrmfontcodepage.RefreshList();
var I:             Integer;
    ACount:        Integer;
    AFontResolution: PDOSCPIFontResolutionExRecord;
    AFontResolutionText: string[50];
begin
  lbResolutions.Items.Clear;

  ACount := FontCodePage^.Header.FontResolutionCount;
  if (ACount > 0) then
  begin
    for I := 0 to Pred(ACount) do
    begin
      AFontResolution :=
        PDOSCPIFontResolutionExRecord(FontCodePage^.List[I]);
      AFontResolutionText :=
        IntToStr(AFontResolution^.Header.FontWidth) + ' * ' +
        IntToStr(AFontResolution^.Header.FontHeight);
      lbResolutions.Items.Add(AFontResolutionText);
    end;
  end;

  lbResolutions.SetFocus;
end;

end.

