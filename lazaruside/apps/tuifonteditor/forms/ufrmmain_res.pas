unit ufrmmain_res;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  dummy;

{.INCLUDE './resources/Language.inc'}

{.IFDEF LanguageEnglish}
const
  resProductName      = 'T.U.I. Font Editor';

const
  resTfrmmain_Caption  = resProductName;

  //resTfrmAbout_Caption  = 'About '+ resProductName;
{.ENDIF}

{$IFDEF LanguageSpanishEurope}
const
  resProductName      = 'Editor de Fuentes para T.U.I.';

const
  resTfrmmain_Caption  = resProductName;

  //resTfrmAbout_Caption  = 'About '+ resProductName;
{$ENDIF}

{$IFDEF LanguageSpanishLatam}
const
  resProductName      = 'Editor de Fuentes para T.U.I.';

const
  resTfrmmain_Caption  = resProductName;

  //resTfrmAbout_Caption  = 'About '+ resProductName;
{$ENDIF}

{$IFDEF LanguageFrench}
const
  resProductName      = 'Editor de Fuentes para T.U.I.';

const
  resTfrmmain_Caption  = resProductName;

  //resTfrmAbout_Caption  = 'About '+ resProductName;
{$ENDIF}


implementation

end.

