unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, Menus,
  ComCtrls, ActnList, StdCtrls,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcfilefilters,
  umlcinputdlgs, umlcmsgdlgs,
  ufrmmain_res,
  doscpifont, tuifonts,
  udmbuttons,
  ufrmfontfolder,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    aclsActions: TActionList;
    acFileNew: TAction;
    acFileExit: TAction;
    acFileOpen: TAction;
    acFileSave: TAction;
    acFileClose: TAction;
    acRecordAdd: TAction;
    acRecordEdit: TAction;
    acRecordDelete: TAction;
    gbMain: TGroupBox;
    lbMain: TListBox;
    mniFileClose: TMenuItem;
    mniFileSaveAs: TMenuItem;
    mniFileSave: TMenuItem;
    mniFileOpen: TMenuItem;
    N1: TMenuItem;
    mniFileNew: TMenuItem;
    mniFileExit: TMenuItem;
    mniFile: TMenuItem;
    mmMainMenu: TMainMenu;
    opdlgOpenDialog: TOpenDialog;
    pnlMain: TPanel;
    ffcntFileFilters: TUMLCFileFiltersContainer;
    svdlgSaveDialog: TSaveDialog;
    sbStatusBar: TStatusBar;
    ToolBar1: TToolBar;
    tobtnFileNew: TToolButton;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    tobtnFileOpen: TToolButton;
    tobtnFileSave: TToolButton;
    tobtnRecordDelete: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    tobtnRecordEdit: TToolButton;
    procedure acFileCloseExecute(Sender: TObject);
    procedure acFileExitExecute(Sender: TObject);
    procedure acFileNewExecute(Sender: TObject);
    procedure acFileOpenExecute(Sender: TObject);
    procedure acFilePropertiesExecute(Sender: TObject);
    procedure acFileSaveExecute(Sender: TObject);
    procedure acRecordAddExecute(Sender: TObject);
    procedure acRecordDeleteExecute(Sender: TObject);
    procedure acRecordEditExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mniFileCloseClick(Sender: TObject);
    procedure mniFileExitClick(Sender: TObject);
    procedure mniFileNewClick(Sender: TObject);
    procedure mniFileOpenClick(Sender: TObject);
    procedure mniFileSaveAsClick(Sender: TObject);
    procedure mniFileSaveClick(Sender: TObject);
  private

  protected
    IsActivated: Boolean;
  public
    Fonts: TList;

    procedure LoadValues();
    procedure LoadStrings();

    procedure FormActivateFirst(Sender: TObject);

    procedure UpdateToolbar;
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure Tfrmmain.mniFileExitClick(Sender: TObject);
begin
  self.close();
end;

procedure Tfrmmain.mniFileCloseClick(Sender: TObject);
var AText, ACaption: string;
begin
  AText := 'Close';
  ACaption := 'Close';
  Application.MessageBox(PChar(AText), PChar(ACaption), 0);
end;

procedure Tfrmmain.FormCreate(Sender: TObject);
begin
  Self.IsActivated := false;

  LoadValues;
  LoadStrings;

  // ...

  Self.Fonts := TList.Create;
end;

procedure Tfrmmain.FormDestroy(Sender: TObject);
begin
  //tuifonts.DropFontFolder(AFontFolder);

  Self.Fonts.Free;
end;

procedure Tfrmmain.FormActivate(Sender: TObject);
begin
  if (not Self.IsActivated) then
  begin
    Self.FormActivateFirst(Sender);
  end;
  Self.IsActivated := true;
end;

procedure Tfrmmain.acFileNewExecute(Sender: TObject);
begin
end;

procedure Tfrmmain.acFileOpenExecute(Sender: TObject);
var AFullPath: string; F: File;
    AFontFolder: PDOSCPIFontFolderExRecord;
begin
  opdlgOpenDialog.Filter:=
    ffcntFileFilters.Filter();

  opdlgOpenDialog.InitialDir :=
    'C:\softdev\temp';

  if (opdlgOpenDialog.Execute) then
  begin
    AFullPath := opdlgOpenDialog.FileName;

    System.Assign(F, AFullPath);

    AFontFolder := nil;

    tuifonts.LoadFont(F, AFontFolder);

    Self.Fonts.Add(AFontFolder);

    // ---

    lbMain.Items.Add(AFontFolder^.Header.FontName);
    UpdateToolbar;
  end;
end;

procedure Tfrmmain.acFilePropertiesExecute(Sender: TObject);
begin
//
end;

procedure Tfrmmain.acFileSaveExecute(Sender: TObject);
var AFullPath: string; F: File; I: Integer;
    AFontFolder: PDOSCPIFontFolderExRecord;
begin
  svdlgSaveDialog.Filter:=
    ffcntFileFilters.Filter();

  svdlgSaveDialog.InitialDir :=
    'C:\softdev\temp';

  if (svdlgSaveDialog.Execute) then
  begin
    AFullPath := svdlgSaveDialog.FileName;

    I := lbMain.ItemIndex;
    AFontFolder :=
      PDOSCPIFontFolderExRecord(Self.Fonts[I]);

    System.Assign(F, AFullPath);
    tuifonts.SaveFont(F, AFontFolder);
  end;
end;

procedure Tfrmmain.acRecordAddExecute(Sender: TObject);
var ATitle, ALabel1, ALabel2: string;

var AFontFolder:   PDOSCPIFontFolderExRecord;
    AFontCodePage: PDOSCPIFontCodePageExRecord;

    AFontName:  string;
    AFontDescr: string;
begin
  AFontName  := 'Helvetica';
  AFontDescr := 'Helvetica (Arial)';

  ATitle := 'New Font';

  ALabel1 := 'Font Name:';
  ALabel2 := 'Font Description:';

  if (InputNameDescr(ATitle, ALabel1, ALabel2, AFontName, AFontDescr)) then
  begin
    AFontFolder :=
      tuifonts.CreateFontFolderByName
        (AFontName, AFontDescr);

    AFontCodePage :=
      tuifonts.CreateAddFontCodePage(AFontFolder, 437, 'Bm437');

    tuifonts.CreateAddFontResolution(AFontCodePage, 8, 8);

    Self.Fonts.Add(AFontFolder);

    // ---

    lbMain.Items.Add(AFontName);
    UpdateToolbar;
  end;




  //ALabel := 'Font Name:';
  //AStrValue := '';
  //if (InputString(ATitle, ALabel, AStrValue)) then
  //begin
  //  AFontName := AStrValue;
  //  ALabel := 'Font Description:';
  //  AStrValue := '';
  //  if (InputString(ATitle, ALabel, AStrValue)) then
  //  begin
  //    AFontDescr := AStrValue;
  //
  //    AFontFolder :=
  //      tuifonts.CreateFontFolderByName
  //        (AFontName, AFontDescr);
  //
  //    AFontCodePage :=
  //      tuifonts.CreateAddFontCodePage(AFontFolder, 437, 'Bm437');
  //
  //    tuifonts.CreateAddFontResolution(AFontCodePage, 8, 8);
  //
  //    Self.Fonts.Add(AFontFolder);
  //
  //    // ---
  //
  //    lbMain.Items.Add(AFontName);
  //    UpdateToolbar;
  //  end;
  //end;

end;

procedure Tfrmmain.acRecordDeleteExecute(Sender: TObject);
begin
  //
end;

procedure Tfrmmain.acRecordEditExecute(Sender: TObject);
var AFontFolder: PDOSCPIFontFolderExRecord;
    I: Integer;
begin
  I := lbMain.ItemIndex;
  AFontFolder :=
    PDOSCPIFontFolderExRecord(Self.Fonts[I]);

  ufrmfontfolder.Execute(AFontFolder);
end;

procedure Tfrmmain.acFileExitExecute(Sender: TObject);
begin
  self.close();
end;

procedure Tfrmmain.acFileCloseExecute(Sender: TObject);
var I: Integer;
//    AFontFolder: PDOSCPIFontFolderExRecord;
begin
  I := lbMain.ItemIndex;
  //AFontFolder :=
  //  PDOSCPIFontFolderExRecord(Self.Fonts[I]);
  // delete
  lbMain.Items.Delete(I);
  Self.Fonts.Delete(I);
end;

procedure Tfrmmain.LoadValues();
//var AppPath, ConfigFilename: string; F: TextFile;
begin
  //AppPath := ParamStr(0);
end;

procedure Tfrmmain.LoadStrings();
begin
  Self.Caption := resTfrmmain_Caption;

  //chbConfirmReplacement.Caption := reschbConfirmReplaced_Caption;
  //
  //btnPathClear.Caption := resbtnPathClear_Caption;
  //
  //btnFileReplace.Caption := resbtnFileReplace_Caption;
  //btnFileSearch.Caption  := resbtnFileSearch_Caption;
  //
  //btnExit.Caption    := resbtnExit_Caption;
  //btnAbout.Caption   := resbtnAbout_Caption;
  //btnOptions.Caption := resbtnOptions_Caption;
  //btnSelectALL.Caption  := resbtnSelectALL_Caption;
  //btnSelectNone.Caption := resbtnSelectNone_Caption;
  //
  //lblSourceExt.Caption := reslblSourceExt_Caption;
  //lblDestExt.Caption   := reslblDestExt_Caption;
  //lblFilePath.Caption  := reslblFilePath_Caption;
  //deedFilePath.Text := '';
end;

procedure Tfrmmain.FormActivateFirst(Sender: TObject);
begin
  UpdateToolbar;
end;

procedure Tfrmmain.UpdateToolbar;
var AnyDoc: Boolean;
begin
  AnyDoc :=
    (Self.Fonts.Count > 0);

  acFileSave.Enabled := AnyDoc;
  //acFileSaveAs.Enabled := AnyDoc;
  acRecordEdit.Enabled := AnyDoc;

  acRecordDelete.Enabled := AnyDoc;

  acFileExit.Enabled := true;
end;

procedure Tfrmmain.mniFileNewClick(Sender: TObject);
begin

end;

procedure Tfrmmain.mniFileOpenClick(Sender: TObject);
begin
end;

procedure Tfrmmain.mniFileSaveAsClick(Sender: TObject);
var AText, ACaption: string;
begin
  AText := 'SaveAs';
  ACaption := 'SaveAs';
  Application.MessageBox(PChar(AText), PChar(ACaption), 0);
end;

procedure Tfrmmain.mniFileSaveClick(Sender: TObject);
var AText, ACaption: string;
begin
  AText := 'Save';
  ACaption := 'Save';
  Application.MessageBox(PChar(AText), PChar(ACaption), 0);
end;

end.

