unit ufrmfontfolder_res;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

{.INCLUDE './resources/Language.inc'}

{.IFDEF LanguageEnglish}
const
  resTfrmfontfolder_Caption  = 'Font Folder';

  //resTfrmAbout_Caption  = 'About '+ resProductName;
{.ENDIF}

{$IFDEF LanguageSpanishEurope}
const
  resTfrmfontfolder_Caption  = 'Folder de Fuentes';

  //resTfrmAbout_Caption  = 'About '+ resProductName;
{$ENDIF}

{$IFDEF LanguageSpanishLatam}
const
  resTfrmfontfolder_Caption  = 'Folder de Fuentes';

  //resTfrmAbout_Caption  = 'About '+ resProductName;
{$ENDIF}

{$IFDEF LanguageFrench}
const
  resTfrmfontfolder_Caption  = 'Folder de Fuentes';

  //resTfrmAbout_Caption  = 'About '+ resProductName;
{$ENDIF}

implementation

end.

