unit ufrmfontresolution;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ActnList,
  Menus, ExtCtrls, StdCtrls,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcinputdlgs, umlcmsgdlgs,
  tuifonts,
  udmbuttons,
  ufrmfontglyph,
  dummy;

type

  { Tfrmfontresolution }

  Tfrmfontresolution = class(TForm)
    acFileExit: TAction;
    aclsMain: TActionList;
    acRecordAdd: TAction;
    acRecordDelete: TAction;
    acRecordEdit: TAction;
    edFontCodePage: TEdit;
    edResolution: TEdit;
    edFontName: TEdit;
    gbMain: TGroupBox;
    lblFontCodePage: TLabel;
    lblResolution: TLabel;
    lblFontName: TLabel;
    lbGlyphs: TListBox;
    miFileNew: TMenuItem;
    miFileOpen: TMenuItem;
    mmMainMenu: TMainMenu;
    mniFile: TMenuItem;
    mniFileExit: TMenuItem;
    N1: TMenuItem;
    pnTitle: TPanel;
    sbStatusBar: TStatusBar;
    tobtnToolBar: TToolBar;
    tobtnRecordAdd: TToolButton;
    tobtnRecordDelete: TToolButton;
    tobtnRecordEdit: TToolButton;
    ToolButton3: TToolButton;
    tobtnFileExit: TToolButton;
    procedure acFileDeleteExecute(Sender: TObject);
    procedure acFileExitExecute(Sender: TObject);
    procedure acFileNewExecute(Sender: TObject);
    procedure acFileOpenExecute(Sender: TObject);
    procedure acRecordAddExecute(Sender: TObject);
    procedure acRecordDeleteExecute(Sender: TObject);
    procedure acRecordEditExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  protected
    IsActivated: Boolean;

  public
    FontFolder:     PDOSCPIFontFolderExRecord;
    FontCodePage:   PDOSCPIFontCodePageExRecord;
    FontResolution: PDOSCPIFontResolutionExRecord;

    procedure FormActivateFirst(Sender: TObject);

    procedure RefreshList();
  end;

  procedure Execute
    (var AFontFolder:     PDOSCPIFontFolderExRecord;
     var AFontCodePage:   PDOSCPIFontCodePageExRecord;
     var AFontResolution: PDOSCPIFontResolutionExRecord);

implementation

procedure Execute
  (var AFontFolder:     PDOSCPIFontFolderExRecord;
   var AFontCodePage:   PDOSCPIFontCodePageExRecord;
   var AFontResolution: PDOSCPIFontResolutionExRecord);
var ThisForm: Tfrmfontresolution;
begin
  ThisForm := Tfrmfontresolution.Create(Application);
    ThisForm.FontFolder     := AFontFolder;
    ThisForm.FontCodePage   := AFontCodePage;
    ThisForm.FontResolution := AFontResolution;
    ThisForm.ShowModal;
  ThisForm.Free;
end;

{$R *.lfm}

{ Tfrmfontresolution }

procedure Tfrmfontresolution.FormCreate(Sender: TObject);
begin
  Self.IsActivated := false;
end;

procedure Tfrmfontresolution.FormActivateFirst(Sender: TObject);
var AFontName:     string[50];
    AFontCodePageText: string[50];
    AFontResolutionText: string[50];
begin
  AFontName := FontFolder^.Header.FontName;
  edFontName.Text := AFontName;

  AFontCodePageText := FontCodePage^.Header.FontCodePageText;
  edFontCodePage.Text := AFontCodePageText;

  AFontResolutionText :=
    IntToStr(FontResolution^.Header.FontHeight) + ' * ' +
    IntToStr(FontResolution^.Header.FontWidth);
  edResolution.Text := AFontResolutionText;

  // display available resolutions per codepage on listbox control
  RefreshList();
end;

procedure Tfrmfontresolution.RefreshList();
var I, ACount: Integer;
    AText: string;
    AGlyph: PDOSCPIFontGlyphRecord;
begin
  lbGlyphs.Items.Clear;

  ACount :=
    FontResolution^.Header.FontGlyphCount;
  if (ACount > 0) then
  begin
    for I := Pred(ACount) to 0 do
    begin
      AGlyph :=
        PDOSCPIFontGlyphRecord(FontResolution^.List[I]);

      AText :=
        'Dec: [' + IntToStr(AGlyph^.ItemIndex) + '] + ' +
        'Hex: [' + IntToHex(AGlyph^.ItemIndex, 4);

      lbGlyphs.Items.Add(AText);
    end;
  end;

  lbGlyphs.SetFocus;
end;

procedure Tfrmfontresolution.FormActivate(Sender: TObject);
begin
  if (not Self.IsActivated) then
  begin
    Self.FormActivateFirst(Sender);
  end;

  Self.IsActivated := true;
end;

procedure Tfrmfontresolution.acFileNewExecute(Sender: TObject);
begin
end;

procedure Tfrmfontresolution.acFileDeleteExecute(Sender: TObject);
begin
  //
end;

procedure Tfrmfontresolution.acFileExitExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure Tfrmfontresolution.acFileOpenExecute(Sender: TObject);
begin
end;

procedure Tfrmfontresolution.acRecordAddExecute(Sender: TObject);
var ATitle, ALabel: string;
    AItemIndex: Integer;
begin
  ATitle := 'New Glyph';

  ALabel := 'Font Glyph Index';
  AItemIndex := 0;
  if (InputInteger(ATitle, ALabel, AItemIndex)) then
  begin
    CreateAddFontGlyph
      (Self.FontResolution, AItemIndex);

    RefreshList();
  end;
end;

procedure Tfrmfontresolution.acRecordDeleteExecute(Sender: TObject);
begin
  //
end;

procedure Tfrmfontresolution.acRecordEditExecute(Sender: TObject);
var I: Integer;
    AFontGyph: PDOSCPIFontGlyphExRecord;
begin
  I := lbGlyphs.ItemIndex;

  AFontGyph :=
    Self.FontResolution^.List[I];

  ufrmfontglyph.Execute
    (Self.FontFolder, Self.FontCodePage, Self.FontResolution, AFontGyph);
end;

end.

