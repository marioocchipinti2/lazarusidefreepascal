unit ufrmfontfolder;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, Menus,
  ExtCtrls, Buttons, StdCtrls, ActnList,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcinputdlgs, umlcmsgdlgs,
  ufrmnamedescr,
  ufrmfontfolder_res,
  tuifonts,
  udmbuttons,
  ufrmfontcodepage,
  dummy;

type

  { Tfrmfontfolder }

  Tfrmfontfolder = class(TForm)
    aclsMain: TActionList;
    acFileExit: TAction;
    acRecordEdit: TAction;
    acRecordAdd: TAction;
    acRecordDelete: TAction;
    edFontName: TEdit;
    gbCodePages: TGroupBox;
    lbCodePages: TListBox;
    lblFontName: TLabel;
    miFileProperties: TMenuItem;
    mniFileExit: TMenuItem;
    N1: TMenuItem;
    mniFileOpen: TMenuItem;
    mniFileNew: TMenuItem;
    mniFile: TMenuItem;
    mmMainMenu: TMainMenu;
    pnTitle: TPanel;
    sbStatusBar: TStatusBar;
    tobaToolbar: TToolBar;
    tobtnRecordAdd: TToolButton;
    tobtnRecordDelete: TToolButton;
    tobtnRecordEdit: TToolButton;
    ToolButton1: TToolButton;
    tobtnFileExit: TToolButton;
    procedure acFileDeleteExecute(Sender: TObject);
    procedure acFileExitExecute(Sender: TObject);
    procedure acFileNewExecute(Sender: TObject);
    procedure acFileOpenExecute(Sender: TObject);
    procedure acFilePropertiesExecute(Sender: TObject);
    procedure acRecordAddExecute(Sender: TObject);
    procedure acRecordDeleteExecute(Sender: TObject);
    procedure acRecordEditExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mniFileExitClick(Sender: TObject);
  private

  protected
    IsActivated: Boolean;
  public
    FontFolder: PDOSCPIFontFolderExRecord;

    procedure LoadValues();
    procedure LoadStrings();

    procedure FormActivateFirst(Sender: TObject);

    procedure RefreshList();

    procedure UpdateToolbar();
  end;

  procedure Execute
    (const AFontFolder: PDOSCPIFontFolderExRecord);

implementation

{$R *.lfm}

procedure Execute
  (const AFontFolder: PDOSCPIFontFolderExRecord);
var ThisForm: Tfrmfontfolder;
begin
  ThisForm := Tfrmfontfolder.Create(Application);
  ThisForm.FontFolder := AFontFolder;
    ThisForm.ShowModal;
  ThisForm.Free;
end;

{ Tfrmfontfolder }

procedure Tfrmfontfolder.mniFileExitClick(Sender: TObject);
begin
  //Self.Close();
end;

procedure Tfrmfontfolder.FormActivate(Sender: TObject);
begin
  if (not Self.IsActivated) then
  begin
    Self.FormActivateFirst(Sender);
  end;
end;

procedure Tfrmfontfolder.acFileExitExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure Tfrmfontfolder.acFileDeleteExecute(Sender: TObject);
var AMessage: string;
    I: Integer;
begin
  AMessage :=
    '¿ Are you sure you want to delete this item ?';
  if (ConfirmDlg(AMessage)) then
  begin
    I := lbCodePages.ItemIndex;
    DeleteFontCodePage(Self.FontFolder, I);

    RefreshList();
  end;
end;

procedure Tfrmfontfolder.acFileNewExecute(Sender: TObject);
begin
end;

procedure Tfrmfontfolder.acFileOpenExecute(Sender: TObject);
begin
//
end;

procedure Tfrmfontfolder.acFilePropertiesExecute(Sender: TObject);
begin
//
end;

procedure Tfrmfontfolder.acRecordAddExecute(Sender: TObject);
var FontCodePageNum:  Integer;
    FontCodePageText: string[50];
    ATitle, ALabel: string;
    AStrValue: string;
    AIntValue: Integer;
begin
  ATitle := 'New Code Page';

  ALabel := 'Code Page Description:';
  AStrValue := '';
  if (InputString(ATitle, ALabel, AStrValue)) then
  begin
    FontCodePageText := AStrValue;

    ALabel := 'Code Page Number:';

    AIntValue := 0;
    if (InputInteger(ATitle, ALabel, AIntValue)) then
    begin
      FontCodePageNum := AIntValue;

      CreateAddFontCodePage
        (Self.FontFolder, FontCodePageNum, FontCodePageText);

      RefreshList();
    end;
  end;
end;

procedure Tfrmfontfolder.acRecordDeleteExecute(Sender: TObject);
begin
  //
end;

procedure Tfrmfontfolder.acRecordEditExecute(Sender: TObject);
var I: Integer;
    AFontCodePage: PDOSCPIFontCodePageExRecord;
begin
  I := lbCodePages.ItemIndex;

  AFontCodePage :=
    PDOSCPIFontCodePageExRecord(FontFolder^.List[I]);

  ufrmfontcodepage.Execute(Self.FontFolder, AFontCodePage);
end;

procedure Tfrmfontfolder.FormCreate(Sender: TObject);
begin
  Self.IsActivated := false;

  // ---

  LoadValues;
  LoadStrings;
end;

procedure Tfrmfontfolder.LoadValues();
//var AppPath, ConfigFilename: string; F: TextFile;
begin
  //AppPath := ParamStr(0);
end;

procedure Tfrmfontfolder.LoadStrings();
begin
  Self.Caption := resTfrmfontfolder_Caption;

  //chbConfirmReplacement.Caption := reschbConfirmReplaced_Caption;
  //
  //btnPathClear.Caption := resbtnPathClear_Caption;
  //
  //btnFileReplace.Caption := resbtnFileReplace_Caption;
  //btnFileSearch.Caption  := resbtnFileSearch_Caption;
  //
  //btnExit.Caption    := resbtnExit_Caption;
  //btnAbout.Caption   := resbtnAbout_Caption;
  //btnOptions.Caption := resbtnOptions_Caption;
  //btnSelectALL.Caption  := resbtnSelectALL_Caption;
  //btnSelectNone.Caption := resbtnSelectNone_Caption;
  //
  //lblSourceExt.Caption := reslblSourceExt_Caption;
  //lblDestExt.Caption   := reslblDestExt_Caption;
  //lblFilePath.Caption  := reslblFilePath_Caption;
  //deedFilePath.Text := '';
end;

procedure Tfrmfontfolder.FormActivateFirst(Sender: TObject);
var AFontName: string[50];
begin
  Self.IsActivated := true;

  // ---

  AFontName := FontFolder^.Header.FontName;
  edFontName.Text := AFontName;

  RefreshList();
end;

procedure Tfrmfontfolder.RefreshList();
var I:             Integer;
    ACount:        Integer;
    AFontCodePage: PDOSCPIFontCodePageExRecord;
begin
  // display available codepages on listbox control
  lbCodePages.Items.Clear;

  ACount := FontFolder^.Header.FontCodePageCount;
  if (ACount > 0) then
  begin
    for I := 0 to Pred(ACount) do
    begin
      AFontCodePage :=
        PDOSCPIFontCodePageExRecord(FontFolder^.List[I]);
      lbCodePages.Items.Add(AFontCodePage^.Header.FontCodePageText);
    end;
  end; // if (ACount > 0) then

  lbCodePages.SetFocus;
end;

procedure Tfrmfontfolder.UpdateToolbar;
var AnyDoc: Boolean;
begin
  AnyDoc :=
    (FontFolder^.Header.FontCodePageCount > 0);

  acRecordAdd.Enabled := true;
  acRecordEdit.Enabled := AnyDoc;
  acRecordDelete.Enabled := AnyDoc;

  acFileExit.Enabled := true;
end;

end.

