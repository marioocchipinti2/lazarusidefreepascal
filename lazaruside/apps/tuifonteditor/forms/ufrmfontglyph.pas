unit ufrmfontglyph;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, ComCtrls,
  ExtCtrls, Buttons, StdCtrls, ActnList,
  umlcguids,
  umlctypes, umlcstdtypes,
  tuifonts, matrices,
  udmbuttons;

type

  { Tfrmfontglyph }

  Tfrmfontglyph = class(TForm)
    aclsMain: TActionList;
    acFileNew: TAction;
    acFileDelete: TAction;
    acFileOpen: TAction;
    acFileExit: TAction;
    acFileOK: TAction;
    acFileCancel: TAction;
    acToolsInvert: TAction;
    edFontCodePage: TEdit;
    edFontResolution: TEdit;
    edFontName: TEdit;
    edFontGlyph: TEdit;
    lblFontCodePage: TLabel;
    lblFontResolution: TLabel;
    lblFontName: TLabel;
    lblFontGlyph: TLabel;
    mniFileExit: TMenuItem;
    miFile: TMenuItem;
    mmMainMenu: TMainMenu;
    pnlMain: TPanel;
    pnTitle: TPanel;
    sbStatusBar: TStatusBar;
    sbMain: TScrollBox;
    tobaMain: TToolBar;
    tobtnToolsInvert: TToolButton;
    tobtnRecordOK: TToolButton;
    ToolButton2: TToolButton;
    tobtnRecordCancel: TToolButton;
    procedure acFileDeleteExecute(Sender: TObject);
    procedure acFileExitExecute(Sender: TObject);
    procedure acFileNewExecute(Sender: TObject);
    procedure acFileOpenExecute(Sender: TObject);
    procedure acFileCancelExecute(Sender: TObject);
    procedure acFileOKExecute(Sender: TObject);
    procedure acToolsInvertExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mniFileExitClick(Sender: TObject);
  private

  protected
    IsActivated: Boolean;

    Pixels: array of array of TToggleBox;
  public
    PixelsWidth: Integer;
    PixelsHeight: Integer;

    FontFolder:     PDOSCPIFontFolderExRecord;
    FontCodePage:   PDOSCPIFontCodePageExRecord;
    FontResolution: PDOSCPIFontResolutionExRecord;
    FontGyph:       PDOSCPIFontGlyphExRecord;

    procedure FormActivateFirst(Sender: TObject);

    procedure BuildPixels;

    procedure LoadGlyph;
    procedure SaveGlyph;

    procedure UpdateToolbar;
  end;

  function Execute
    (var AFontFolder:     PDOSCPIFontFolderExRecord;
     var AFontCodePage:   PDOSCPIFontCodePageExRecord;
     var AFontResolution: PDOSCPIFontResolutionExRecord;
     var AFontGyph:       PDOSCPIFontGlyphExRecord): Boolean;

implementation

{$R *.lfm}

function Execute
  (var AFontFolder:     PDOSCPIFontFolderExRecord;
   var AFontCodePage:   PDOSCPIFontCodePageExRecord;
   var AFontResolution: PDOSCPIFontResolutionExRecord;
   var AFontGyph:       PDOSCPIFontGlyphExRecord): Boolean;
var ThisForm: Tfrmfontglyph;
begin
  Result := false;

  ThisForm := Tfrmfontglyph.Create(Application);
    ThisForm.FontFolder     := AFontFolder;
    ThisForm.FontCodePage   := AFontCodePage;
    ThisForm.FontResolution := AFontResolution;
    ThisForm.FontGyph       := AFontGyph;

    ThisForm.PixelsWidth  := AFontResolution^.Header.FontWidth;
    ThisForm.PixelsHeight := AFontResolution^.Header.FontHeight;

    ThisForm.ShowModal;

    Result := (ThisForm.ModalResult = mrOK);

  ThisForm.Free;
end;

{ Tfrmfontglyph }

procedure Tfrmfontglyph.mniFileExitClick(Sender: TObject);
begin

end;

procedure Tfrmfontglyph.FormCreate(Sender: TObject);
begin
  Self.IsActivated := false;
end;

procedure Tfrmfontglyph.FormActivate(Sender: TObject);
begin
  if (not Self.IsActivated) then
  begin
    Self.FormActivateFirst(Sender);
  end;
end;

procedure Tfrmfontglyph.acFileNewExecute(Sender: TObject);
begin
  //
end;

procedure Tfrmfontglyph.acFileOpenExecute(Sender: TObject);
begin
  //
end;

procedure Tfrmfontglyph.acFileCancelExecute(Sender: TObject);
begin
  //
  Self.ModalResult := mrCancel;
end;

procedure Tfrmfontglyph.acFileOKExecute(Sender: TObject);
begin
  SaveGlyph;

  Self.ModalResult := mrOK;
end;

procedure Tfrmfontglyph.acToolsInvertExecute(Sender: TObject);
var I, J: Integer;
begin
  for J := 0 to Pred(Self.PixelsHeight) do
  begin
    for I := 0 to Pred(Self.PixelsWidth) do
    begin
      Pixels[I, J].Checked :=
        (not Pixels[I, J].Checked);
    end;
  end;
end;

procedure Tfrmfontglyph.acFileDeleteExecute(Sender: TObject);
begin
  //
end;

procedure Tfrmfontglyph.acFileExitExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure Tfrmfontglyph.FormActivateFirst(Sender: TObject);
var AFontName:     string[50];
    AFontCodePageText: string[50];
    AFontResolutionText: string[50];
    AFontGlyphText: string[50];
begin
  Self.IsActivated := true;

  // ---

  AFontName := FontFolder^.Header.FontName;
  edFontName.Text := AFontName;

  AFontCodePageText := FontCodePage^.Header.FontCodePageText;
  edFontCodePage.Text := AFontCodePageText;

  AFontResolutionText :=
    IntToStr(FontResolution^.Header.FontHeight) + ' * ' +
    IntToStr(FontResolution^.Header.FontWidth);
  edFontResolution.Text := AFontResolutionText;

  AFontGlyphText :=
    IntToStr(FontGyph^.Header.ItemIndex);
  edFontGlyph.Text := AFontGlyphText;

  // ---

  BuildPixels;
  LoadGlyph;
  UpdateToolbar;
end;

procedure Tfrmfontglyph.BuildPixels;
var EachToggleBox: TToggleBox;
var I, J, X, Y: Integer;
begin
  setLength
    (Pixels, Self.PixelsWidth, Self.PixelsHeight);

  EachToggleBox := nil;

  Y := 8;
  for J := 0 to Pred(Self.PixelsHeight) do
  begin
    X := 8;
    for I := 0 to Pred(Self.PixelsWidth) do
    begin
      EachToggleBox := TToggleBox.Create(Self);
      Pixels[I, J] := EachToggleBox;

      EachToggleBox.Left   := X;
      EachToggleBox.Top    := Y;
      EachToggleBox.Width  := 24;
      EachToggleBox.Height := 24;

      sbMain.InsertControl(EachToggleBox);

      X := X + 24;
    end;

    Y := Y + 24;
  end;
end;

procedure Tfrmfontglyph.LoadGlyph;
var I, J: Integer; Pixel: PBoolean;
begin
  for J := 0 to Pred(Self.PixelsHeight) do
  begin
    for I := 0 to Pred(Self.PixelsWidth) do
    begin
      Pixel :=
        PBoolean(matrices.At(FontGyph^.Pixels, I, J));
       Pixels[I, J].Checked := Pixel^;
    end;
  end;
end;

procedure Tfrmfontglyph.SaveGlyph;
var I, J: Integer; Pixel: PBoolean;
begin
  for J := 0 to Pred(Self.PixelsHeight) do
  begin
    for I := 0 to Pred(Self.PixelsWidth) do
    begin
      Pixel :=
        PBoolean(matrices.At(FontGyph^.Pixels, I, J));
      Pixel^ :=
       Pixels[I, J].Checked;
    end;
  end;
end;

procedure Tfrmfontglyph.UpdateToolbar;
//var AnyDoc: Boolean;
begin
  //AnyDoc :=
  //  (Self.Fonts.Count > 0);
  //
  //acFileSave.Enabled := AnyDoc;
  ////acFileSaveAs.Enabled := AnyDoc;
  //acRecordEdit.Enabled := AnyDoc;
  //
  //acFileClose.Enabled := AnyDoc;

  acFileOK.Enabled := true;
  acFileCancel.Enabled := true;
end;

end.

