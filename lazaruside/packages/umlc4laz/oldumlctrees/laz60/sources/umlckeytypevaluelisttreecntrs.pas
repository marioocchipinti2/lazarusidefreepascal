(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlckeytypevaluelisttreecntrs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlclists,
  umlconlystringkeytypevaluelists,
  umlctreenodes,
  umlctreecntrs,
  dummy;

(**
 ** Description:
 ** This unit contains several in order to support an non visual,
 ** hierarchical ( "tree" ) collection of data.
 **
 ** It was not designed with generics collections.
 **
 ** Each element of the tree-collection, has a sequential,
 ** non-hierarchical list of key-type-value elements.
 **)

type

(* TUMLCKeyTypeValueListTreeNode *)

  TUMLCKeyTypeValueListTreeNode = class(TUMLCContainerTreeNode)
  private
    (* Private declarations *)

  protected
    (* Protected declarations *)

    FKeyTypeValueList : TUMLCOnlyStringKeyTypeValueList;
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  public
    (* Public declarations *)

    (* read-only properties *)

    function Properties(): TUMLCOnlyStringKeyTypeValueList;
  end;

(* TCustomUMLCKeyTypeValueTreeCollection *)

  TCustomUMLCKeyTypeValueTreeCollection = class(TUMLCContainerTreeCollection)
  private
    (* Private declarations *)

  protected
    (* Protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;
  public
    (* Public declarations *)
  end;

(* TCustomUMLCKeyTypeValueTreeContainer *)

  TCustomUMLCKeyTypeValueTreeContainer = class(TCustomUMLCTreeContainer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function CreateCollectionByClass(): TUMLCContainerTreeCollection; override;
  public
    (* Public declarations *)
  end;

implementation

(* TUMLCKeyTypeValueListTreeNode *)

procedure TUMLCKeyTypeValueListTreeNode.DoCreate();
begin
  inherited DoCreate();
  FKeyTypeValueList := TUMLCOnlyStringKeyTypeValueList.Create();
end;

procedure TUMLCKeyTypeValueListTreeNode.DoDestroy();
begin
  FKeyTypeValueList.Empty();
  FKeyTypeValueList.Free();
  inherited DoDestroy();
end;

// "read-only"
function TUMLCKeyTypeValueListTreeNode.Properties(): TUMLCOnlyStringKeyTypeValueList;
begin
  Result := FKeyTypeValueList;
end;

(* TCustomUMLCKeyTypeValueTreeCollection *)

function TCustomUMLCKeyTypeValueTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCKeyTypeValueListTreeNode.Create();
  Result.DoCreate();
end;

(* TCustomUMLCKeyTypeValueTreeContainer *)

function TCustomUMLCKeyTypeValueTreeContainer.CreateCollectionByClass(): TUMLCContainerTreeCollection;
begin
  Result := TCustomUMLCKeyTypeValueTreeCollection.Create();
  Result.DoCreate();
end;

end.

