(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctextreenodes;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  umlccomparisons,
  umlcstrings,
  umlctreenodes,
  dummy;

(**
 ** Description:
 ** This unit contains several in order to support an non visual,
 ** hierarchical ( "tree" ) collection of data.
 **
 ** It was not designed with generics collections.
 **
 ** Additionally, a "Text" property and helper functions are provided,
 **
 ** The "Text" property is a reference-allocated Delphi/FreePascal string,
 ** also known as an "openstring".
 **)

type

 (* TUMLCTextTreeNode *)

   TUMLCTextTreeNode = class(TUMLCTreeNode)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     FText: string;
   protected
     (* Protected declarations *)

     (* Accessors declarations *)

     function getText(): string; virtual;

     procedure setText(const AValue: string); virtual;
   protected
     (* Protected declarations *)

     procedure ConfirmedCopyTo(var ADestNode: TUMLCTreeNode); override;
     procedure ConfirmedMoveTo(var ADestNode: TUMLCTreeNode); override;
   public
     (* Friend Protected declarations *)

     procedure ConfirmedChangeText(const AText: string); virtual;
   public
     (* Public declarations *)

     procedure DoCreate(); override;
     procedure DoDestroy(); override;
   public
     (* Public declarations *)

     (* readonly functions *)

     function IsEmpty(): Boolean;
   public
     (* Public declarations *)

     function EqualAs(const AStr: string): Boolean;
     function SameAs(const AStr: string): Boolean;

     function StartsWith(const ASubStr: string): Boolean;
     function FinishesWith(const ASubStr: string): Boolean;

     function ComparesWith(const AStr: string): TComparison;

     procedure Clear();
   public
     (* Public declarations *)

     property Text: string
      read getText write setText;
   end;

 (* TUMLCTextTreeCollection *)

   TUMLCTextTreeCollection = class(TUMLCTreeCollection)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     function CreateNodeByClass(): TUMLCTreeNode; override;
   protected
     (* Protected declarations *)

     procedure InternalBeforeChangeText
       (const ANode: TUMLCTextTreeNode;
        const AText: string); virtual;
     procedure InternalAfterChangeText
       (const ANode: TUMLCTextTreeNode); virtual;
     procedure InternalConfirmedChangeText
       (const ANode: TUMLCTextTreeNode;
        const AText: string); virtual;

     procedure InternalAfterInsert
       (const AParentNode, ANode: TUMLCTreeNode;
        const AIndex: Integer); override;
   public
     (* Friend Protected declarations *)

     procedure RequestChangeText
       (const ANode: TUMLCTextTreeNode;
        const AText: string); (* nonvirtual; *)

     function DefaultText(): string; virtual;
   public
     (* Public declarations *)
   end;

implementation

(* TUMLCTextTreeNode *)

function TUMLCTextTreeNode.getText(): string;
begin
  Result := FText;
end;

procedure TUMLCTextTreeNode.setText(const AValue: string);
var ACollection: TUMLCTextTreeCollection;
begin
  if (not EqualAs(AValue)) then
  begin
    ACollection := TUMLCTextTreeCollection(InternalCollection);
    ACollection.RequestChangeText(Self, AValue);
  end;
end;

procedure TUMLCTextTreeNode.ConfirmedCopyTo(var ADestNode: TUMLCTreeNode);
var ThisDestNode: TUMLCTextTreeNode;
begin
  // perform copy of fields specific to parent class
  inherited ConfirmedCopyTo(ADestNode);

  // cast to current type
  ThisDestNode := TUMLCTextTreeNode(ADestNode);

  // perform copy of fields specific to this class
  ThisDestNode.Text := Self.Text;
end;

procedure TUMLCTextTreeNode.ConfirmedMoveTo(var ADestNode: TUMLCTreeNode);
var ThisDestNode: TUMLCTextTreeNode;
begin
  // perform move of fields specific to parent class
  inherited ConfirmedMoveTo(ADestNode);

  // cast to current type
  ThisDestNode := TUMLCTextTreeNode(ADestNode);

  // perform move of fields specific to this class
  ThisDestNode.Text := Self.Text;
  Self.Text         := '';
end;

procedure TUMLCTextTreeNode.ConfirmedChangeText(const AText: string);
begin
  FText := AText;
end;

procedure TUMLCTextTreeNode.DoCreate();
begin
  inherited DoCreate();
  FText := '';
end;

procedure TUMLCTextTreeNode.DoDestroy();
begin
  FText := '';
  inherited DoDestroy();
end;

function TUMLCTextTreeNode.IsEmpty(): Boolean;
begin
  Result := (FText = '');
end;

function TUMLCTextTreeNode.EqualAs(const AStr: string): Boolean;
begin
  Result := (FText = AStr);
  // Goal: Case sensitive comparison.
end;

function TUMLCTextTreeNode.SameAs(const AStr: string): Boolean;
begin
  Result := umlcstrings.SameText(FText, AStr);
  // Goal: Case Insensitive comparison.
end;

function TUMLCTextTreeNode.StartsWith(const ASubStr: string): Boolean;
begin
  Result := umlcstrings.StartsWith(ASubStr, FText);
end;

function TUMLCTextTreeNode.FinishesWith(const ASubStr: string): Boolean;
begin
  Result := umlcstrings.FinishesWith(ASubStr, FText);
end;

function TUMLCTextTreeNode.ComparesWith(const AStr: string): TComparison;
begin
  Result := umlcstrings.Compare(AStr, FText);
end;

procedure TUMLCTextTreeNode.Clear();
begin
  Self.Text := '';
end;

(* TUMLCTextTreeCollection *)

function TUMLCTextTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCTextTreeNode.Create();
  Result.DoCreate();
end;

procedure TUMLCTextTreeCollection.InternalBeforeChangeText
  (const ANode: TUMLCTextTreeNode; const AText: string);
begin
  Self.DoNothing();
end;

procedure TUMLCTextTreeCollection.InternalAfterChangeText
  (const ANode: TUMLCTextTreeNode);
begin
  Self.DoNothing();
end;

procedure TUMLCTextTreeCollection.InternalConfirmedChangeText
  (const ANode: TUMLCTextTreeNode; const AText: string);
begin
  ANode.ConfirmedChangeText(AText);
end;

procedure TUMLCTextTreeCollection.RequestChangeText
  (const ANode: TUMLCTextTreeNode; const AText: string);
begin
  InternalBeforeChangeText(ANode, AText);
  InternalConfirmedChangeText(ANode, AText);
  InternalAfterChangeText(ANode);
end;

procedure TUMLCTextTreeCollection.InternalAfterInsert
  (const AParentNode, ANode: TUMLCTreeNode; const AIndex: Integer);
var ATextNode: TUMLCTextTreeNode; AText: string;
begin
  ATextNode      := TUMLCTextTreeNode(ANode);
  AText          := DefaultText();
  ATextNode.Text := AText;
end;

function TUMLCTextTreeCollection.DefaultText(): string;
begin
  Result := ClassName() + SysUtils.IntToStr((FGlobalSequencer + 1));
end;

end.

