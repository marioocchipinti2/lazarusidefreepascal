(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlckeytypevaluedocs;

{$mode objfpc}{$H+}

interface

uses
  // ##temp {
  Crt, umlcstrings,
  // ##temp }
  Classes, SysUtils,
  umlcnormobjects,
  umlcbooleans,
  umlclists,
  umlcreclists,
  umlctreenodes,
  umlctreecntrs,
  umlckeyvaluemodes,
  umlckeytypevaluetokens,
  umlconlystringkeytypevaluelists,
  umlckeytypevaluelisttreecntrs,
  dummy;

  //UMLCXMLFileTokens, xmlfileAnsiSymbols, xmlfileAnsiScanners,
  //UMLCsxmltreenodes, UMLCsxmldocs,
  //UMLCmcpptokens, UMLCmcpptreenodes, UMLCmcppdocs;

type

(* TUMLCKeyTypeValueListDocument *)

  TUMLCKeyTypeValueListDocument = class(TCustomUMLCTreeContainer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)
  public
    (* Public declarations *)
  end;

(* TUMLCKeyTypeValueXMLDocument *)

  TUMLCKeyTypeValueXMLDocument = class(TUMLCKeyTypeValueListDocument)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    //_SXMLDoc: TUMLCSimpleXMLDocument;

    KVRootNode: TUMLCKeyTypeValueListTreeNode;
    KVCurrentNode: TUMLCKeyTypeValueListTreeNode;

    CurrentKVType: TUMLCKeyValueModes;
    CurrentKey, CurrentValue, CurrentType: string;
  protected
    (* Protected declarations *)

    function getItems(): TCustomUMLCKeyTypeValueTreeCollection;
    procedure setItems(const Value: TCustomUMLCKeyTypeValueTreeCollection);
  protected
    (* Protected declarations *)

    function CreateCollectionByClass(): TUMLCContainerTreeCollection; override;

    procedure TransferNode
      (var Node: TUMLCTreeNode;
      const Param: pointer; const Direction: TUMLCTreeDirection);

    procedure DisplayNodeAsJSON
      (var Node: TUMLCTreeNode;
      const Param: pointer; const Direction: TUMLCTreeDirection);

    procedure DisplayNodeAsSXML
      (var Node: TUMLCTreeNode;
      const Param: pointer; const Direction: TUMLCTreeDirection);

    (*
    procedure TransferKVTNodeToMCPP
      (var Node: TUMLCTreeNode;
      const Param: pointer; const Direction: TUMLCTreeDirection);
    *)
  public
    (* Public declarations *)

    procedure TransferSXMLToKeyValueType();

    //function TransferSXMLToMCPP: TUMLCMCPPDocument;

    procedure DisplayAsJSON();
    procedure DisplayAsSXML();

    //property SXMLDoc: TUMLCSimpleXMLDocument
      //read _SXMLDoc write _SXMLDoc;

    property Items: TCustomUMLCKeyTypeValueTreeCollection
      read getItems write setItems;
  end;

implementation

(* TUMLCKeyTypeValueXMLDocument *)

function TUMLCKeyTypeValueXMLDocument.getItems(): TCustomUMLCKeyTypeValueTreeCollection;
begin
  Result := TCustomUMLCKeyTypeValueTreeCollection(FItems);
end;

procedure TUMLCKeyTypeValueXMLDocument.setItems
  (const Value: TCustomUMLCKeyTypeValueTreeCollection);
begin
  FItems := Value;
end;

function TUMLCKeyTypeValueXMLDocument.CreateCollectionByClass(): TUMLCContainerTreeCollection;
begin
  Result := TCustomUMLCKeyTypeValueTreeCollection.Create();
  Result.DoCreate();
end;

procedure TUMLCKeyTypeValueXMLDocument.TransferNode
  (var Node: TUMLCTreeNode;
  const Param: pointer; const Direction: TUMLCTreeDirection);
(*
var EachNode: TUMLCSXMLTreeNode;
    SymbolText: string;
    KTVToken: TKeyValueTypeFileToken;

    KTVNode: TUMLCKeyTypeValueListTreeNode;
    KeyTypeValue: TUMLCOnlyStringKeyTypeValueItem;
*)
begin
  (*
  EachNode := (Node as TUMLCSXMLTreeNode);
  SymbolText := umlcstrings.LowercaseCopy(EachNode.Symbol.Text);
  KTVToken := XMLTagToKeyValueType(SymbolText);

  if (Direction = tdStart) then
  begin
    if ((EachNode.IsRoot) and (KTVToken = kvtfiletkObject)) then
    begin
      // --> el marcador raiz, genera un objeto por nodo-arbol
      KTVNode := (Items.InsertRoot as TUMLCKeyTypeValueListTreeNode);
      KVRootNode := KTVNode;
      KVCurrentNode := KTVNode;
    end else
    begin
      // "items", y otros tags son ignorados
      if (EachNode.Symbol.Token = xmlfiletkBlock) then
      begin
        if (KTVToken = kvtfiletkObject) then
        begin
          // --> agrega un nodo al nodo actual
          KTVNode := (Items.Insert(KVCurrentNode) as TUMLCKeyTypeValueListTreeNode);
          KVCurrentNode := KTVNode;
        end else if (KTVToken = kvtfiletkKey) then
        begin
          CurrentKVType := kvtKey;
        end else if (KTVToken = kvtfiletkType) then
        begin
          CurrentKVType := kvtType;
        end else if (KTVToken = kvtfiletkValue) then
        begin
          CurrentKVType := kvtValue;
        end else
        begin
          CurrentKVType := kvtNone;
        end;
      end else if (EachNode.Symbol.Token = xmlfiletkText) then
      begin
        case (CurrentKVType) of
          kvtKey:   CurrentKey   := EachNode.Symbol.Text;
          kvtType:  CurrentType  := EachNode.Symbol.Text;
          kvtValue: CurrentValue := EachNode.Symbol.Text;
          else None();
        end;
      end else if (EachNode.Symbol.Token = xmlfiletkFinish) then
      begin
        if (KTVToken = kvtfiletkItem) then
        begin
          // al cerrar con el marcador "</item>",
          // guardar propiedad
          CurrentKVType := kvtNone;

          KeyTypeValue := TUMLCOnlyStringKeyTypeValueItem.Create();
            KeyTypeValue.Key    := CurrentKey;
            KeyTypeValue.TypeID := CurrentType;
            KeyTypeValue.Value  := CurrentValue;
          KVCurrentNode.Properties.Insert(KeyTypeValue);
        end;
      end else
      begin
        //
      end;
    end;
  end else
  begin
    if (EachNode.Symbol.Token = xmlfiletkBlock) then
    begin
      if (KTVToken = kvtfiletkItem) then
      begin
        // --> al cerrar con el marcador "</item>",
        // --> guardar propiedad
        CurrentKVType := kvtNone;

        KeyTypeValue := TUMLCOnlyStringKeyTypeValueItem.Create();
          KeyTypeValue.Key    := CurrentKey;
          KeyTypeValue.TypeID := CurrentType;
          KeyTypeValue.Value  := CurrentValue;
        KVCurrentNode.Properties.Insert(KeyTypeValue);
      end
      else if ((not EachNode.IsRoot) and (KTVToken = kvtfiletkObject)) then
      begin
        // --> al cerrar con el marcador "</object>",
        // --> regresar apuntador al nodo padre

        KVCurrentNode := (KVCurrentNode.Group as TUMLCKeyTypeValueListTreeNode);
      end;
    end;
  end;
  *)
end;

procedure TUMLCKeyTypeValueXMLDocument.TransferSXMLToKeyValueType();
begin
  (*
  if (SXMLDoc <> nil) then
  begin
    CurrentKVType := kvtNone;
    SXMLDoc.Items.ForBoth(@TransferNode, nil);
  end;
  *)
end;

(*
procedure TUMLCKeyTypeValueXMLDocument.TransferKVTNodeToMCPP
  (var Node: TUMLCTreeNode; const Param: pointer;
  const Direction: TUMLCTreeDirection);
var Result: TUMLCMCPPDocument;
    EachMCPPNode: TUMLCMCPPTreeNode;
    EachKVTNode: TUMLCKeyTypeValueListTreeNode;
    AIndex, ALast: Integer; Item: TUMLCOnlyStringKeyTypeValueItem;
begin
  Result := TUMLCMCPPDocument(Param);
  EachKVTNode := TUMLCKeyTypeValueListTreeNode(Node);

  if (Direction = tdStart) then
  begin
    if (EachKVTNode.IsRoot) then
    begin
      EachMCPPNode := TUMLCMCPPTreeNode(Result.Items.InsertRoot());

      ALast := (EachKVTNode.Properties().Count - 1);
      for AIndex := 0 to ALast do
      begin
        Item := EachKVTNode.Properties[AIndex];
        if (Item.MatchesKey('id')) then
        begin
          EachMCPPNode.Name := Item.Value;
        end else if (Item.MatchesKey('token')) then
        begin
          EachMCPPNode.Token:=
            UMLCmcpptokens.StrToMCPPToken(Item.Value);
        end else if (Item.MatchesKey('isautomatic')) then
        begin
          EachMCPPNode.TokenExtension.IsAutomatic :=
            UMLCBooleans.StrToBool(Item.Value);
        end;
      end;

      EachMCPPNode.AssignExtension();
    end else
    begin
      // ...
    end;

    // ...
  end else
  begin
    // ...
  end;

  // ...
end;
*)

(*
function TUMLCKeyTypeValueXMLDocument.TransferSXMLToMCPP: TUMLCMCPPDocument;
begin
  Result := TUMLCMCPPDocument.Create(nil);
  self.Items.ForBoth(@TransferKVTNodeToMCPP, Result);
end;
*)

procedure TUMLCKeyTypeValueXMLDocument.DisplayNodeAsJSON
  (var Node: TUMLCTreeNode;
  const Param: pointer; const Direction: TUMLCTreeDirection);
var EachNode: TUMLCKeyTypeValueListTreeNode;
    S: string;
    ID: string; AIndex: Integer;
    I, C, L: Integer; Item: TUMLCOnlyStringKeyTypeValueItem;
begin
  EachNode := TUMLCKeyTypeValueListTreeNode(Node);
  S := umlcstrings.StringOfChar(' ', EachNode.Level() * 2);

  if (Direction = tdStart) then
  begin
    // --> obtain identifier
    AIndex := EachNode.Properties.IndexOfKey('id');
    if (AIndex > -1) then
    begin
      Item := EachNode.Properties.Items[AIndex];
      ID := Item.Value;
    end else
    begin
      ID := '<no id>';
    end;

    WriteLn(S, ID, ' = {');

    // --> ahora si, seguir con el resto de las propiedades
    // imprimir lista de propiedades
    C := (EachNode.Properties.Count);
    if (C > 0) then
    begin
      L := (C - 1);
      for I := 0 to L do
      begin
        // obtener referencia
        Item := EachNode.Properties.Items[I];

        if (Item.Key <> 'id') then
        begin
          WriteLn(S, S, Item.Key, ': "', Item.Value, '": ', Item.TypeID, ',');
        end;
      end;
    end;

    C := (EachNode.List.Count);
    if (C > 0) then
    begin
      WriteLn(S, S, 'items = {');
    end;

  end else
  begin
    C := (EachNode.List.Count);
    if (C > 0) then
    begin
      WriteLn(S, S, '} // items');
    end;

    WriteLn(S, '} // ');
  end;
end;

procedure TUMLCKeyTypeValueXMLDocument.DisplayNodeAsSXML
  (var Node: TUMLCTreeNode;
  const Param: pointer; const Direction: TUMLCTreeDirection);
var EachNode: TUMLCKeyTypeValueListTreeNode;
    S: string;
    I, C, L: Integer; Item: TUMLCOnlyStringKeyTypeValueItem;
begin
  EachNode := TUMLCKeyTypeValueListTreeNode(Node);
  S := umlcstrings.StringOfChar(' ', EachNode.Level() * 2);

  if (Direction = tdStart) then
  begin
    // imprimir encabezado objeto
    WriteLn(S, '<object>');

    // imprimir lista de propiedades
    C := (EachNode.Properties.Count);
    if (C > 0) then
    begin
      WriteLn(S, S, '<properties>');

      L := (C - 1);
      for I := 0 to L do
      begin
        // obtener referencia
        Item := EachNode.Properties.Items[I];
        WriteLn(S, S, S, '<item>');
        Write(S, S, S, S, '<key>', Item.Key, '</key>');
        Write(S, S, S, S, '<value>', Item.Value, '</value>');
        Write(S, S, S, S, '<type>', Item.TypeID, '</type>');
        WriteLn(S, S, S, '</item>');
      end;

      WriteLn(S, S, '</properties>');
    end;

    C := (EachNode.List.Count);
    if (C > 0) then
    begin
      WriteLn(S, S, '<items>');
    end;
  end else
  begin
    C := (EachNode.List.Count);
    if (C > 0) then
    begin
      WriteLn(S, S, '</items>');
    end;

    // imprimir piepagina objeto
    WriteLn(S, '</object>');
  end;
end;

procedure TUMLCKeyTypeValueXMLDocument.DisplayAsJSON();
begin
  ClrScr();
  Items.Root().ForBoth(@DisplayNodeAsJSON, nil);
end;

procedure TUMLCKeyTypeValueXMLDocument.DisplayAsSXML();
begin
  ClrScr();
  Items.Root().ForBoth(@DisplayNodeAsSXML, nil);
end;

end.

