(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctreecntrs;

{$mode objfpc}{$H+}

interface
uses
  SysUtils, Classes,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlclists,
  umlctreestates,
  umlctreenodes,
  umlcactcntrls,
  umlctextreenodes,
  dummy;


const

  // ---

  ID_TCustomUMLCTreeContainer : TUMLCType =
    ($6E,$CC,$10,$AB,$63,$57,$94,$4C,$AB,$FB,$E9,$77,$E0,$C8,$5C,$95);

  ID_TUMLCContainerTreeNode : TUMLCType =
    ($FE,$FE,$CB,$9F,$7B,$30,$BD,$49,$B7,$D3,$30,$0D,$1A,$A9,$E0,$46);

  ID_TUMLCContainerTreeCollection : TUMLCType =
    ($31,$3B,$97,$AC,$28,$B6,$3F,$48,$A3,$90,$6F,$6B,$4B,$EC,$D2,$D7);

  ID_TUMLCTreeContainer : TUMLCType =
    ($27,$AB,$6C,$51,$49,$CE,$F1,$4F,$9A,$52,$1C,$F0,$13,$F4,$F4,$B3);

  // ---


type

  TUMLCContainerTreeNode = (* forward *) class;

  TOnUMLCBeforeChangeStringTreeNodeEvent =
    procedure
      (const ANode: TUMLCContainerTreeNode;
       const AValue: string)
      of object;

  TOnUMLCAfterChangeStringTreeNodeEvent =
    procedure
      (const ANode: TUMLCContainerTreeNode)
      of object;

  TOnUMLCBeforeBooleanTreeNodeEvent =
    procedure
      (const ANode: TUMLCContainerTreeNode;
       const AValue: Boolean)
      of object;

  TOnUMLCAfterBooleanTreeNodeEvent =
    procedure
      (const ANode: TUMLCContainerTreeNode)
      of object;

  TOnUMLCInsertAtTreeNodeEvent =
    procedure
    (const AParentNode, ANode: TUMLCContainerTreeNode;
     const AIndex: Integer)
      of object;

  TOnUMLCTreeContainerNodeEvent =
    procedure
      (const ANode: TUMLCContainerTreeNode)
      of object;

(* TUMLCContainerTreeNode *)

  TCustomUMLCTreeContainer = class;

  TUMLCContainerTreeNode = class(TUMLCTextTreeNode)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FSelected:   Boolean;
  protected
    (* Protected declarations *)

    (* Accessors declarations *)

    function getSelected(): Boolean; virtual;

    procedure setSelected(const AValue: Boolean); virtual;
  protected
    (* Protected declarations *)

    procedure ConfirmedCopyTo(var ADestNode: TUMLCTreeNode); override;
    procedure ConfirmedMoveTo(var ADestNode: TUMLCTreeNode); override;
  protected
    (* Protected declarations *)

    procedure ApplySelectAll
      (var ANode: TUMLCTreeNode; const AParam: pointer);
    procedure ApplySelectNone
      (var ANode: TUMLCTreeNode; const AParam: pointer);
    procedure ApplySelectToggle
      (var ANode: TUMLCTreeNode; const AParam: pointer);
    procedure ApplySelectAs
      (var ANode: TUMLCTreeNode; const AParam: pointer);
  public
    (* Friend Protected declarations *)

    procedure ConfirmedChangeSelected(const ASelected: Boolean); virtual;
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  public
    (* Public declarations *)

    procedure SelectAll();
    procedure SelectNone();
    procedure SelectToggle();
    procedure SelectAs(const AValue: Boolean);
  public
    (* Public declarations *)

    property Selected: Boolean
      read getSelected write setSelected;
  end;

(* TUMLCContainerTreeCollection *)

  TUMLCContainerTreeCollection = class(TUMLCTextTreeCollection)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FContainer:  TCustomUMLCTreeContainer;
  protected
    (* Protected declarations *)

    (* Accessors declarations *)

    function getContainer(): TCustomUMLCTreeContainer;

    procedure setContainer(const AValue: TCustomUMLCTreeContainer);
  protected
    (* Protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;
  protected
    (* Protected declarations *)

    procedure InternalBeforeChangeText
      (const ANode: TUMLCTextTreeNode;
       const AText: string); override;
    procedure InternalAfterChangeText
      (const ANode: TUMLCTextTreeNode); override;

    procedure InternalBeforeChangeSelected
      (const ANode: TUMLCContainerTreeNode;
       const ASelected: Boolean); virtual;
    procedure InternalAfterChangeSelected
      (const ANode: TUMLCContainerTreeNode); virtual;
    procedure InternalConfirmedChangeSelected
      (const ANode: TUMLCContainerTreeNode;
       const ASelected: Boolean); virtual;

    procedure InternalBeforeInsert
      (const AParentNode, ANewNode: TUMLCTreeNode;
       const AIndex: Integer); override;
    procedure InternalAfterInsert
      (const AParentNode, ANewNode: TUMLCTreeNode;
       const AIndex: Integer); override;

    procedure InternalBeforeRemove
      (const ANode: TUMLCTreeNode); override;
    procedure InternalAfterRemove
      (const ANode: TUMLCTreeNode); override;

    procedure InternalBeforeEmpty
      (const ANode: TUMLCTreeNode); override;
    procedure InternalAfterEmpty(
     const ANode: TUMLCTreeNode); override;
  public
    (* Friend Protected declarations *)

    procedure RequestChangeSelected
      (const ANode: TUMLCContainerTreeNode;
       const ASelected: Boolean); (* nonvirtual; *)
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  public
    (* Public declarations *)

    property Container: TCustomUMLCTreeContainer
      read getContainer write setContainer;
  end;

(* TCustomUMLCTreeContainer *)

  TCustomUMLCTreeContainer = class(TUMLCActivatedComponent)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FItems: TUMLCContainerTreeCollection;
  protected
    (* Protected declarations *)

    (* Accessors declarations *)

    function getItems(): TUMLCContainerTreeCollection;

    procedure setItems(const AValue: TUMLCContainerTreeCollection);
  protected
    (* Protected declarations *)

    function CreateCollectionByClass(): TUMLCContainerTreeCollection; virtual;

    procedure CreateCollection(); virtual;
    procedure DestroyCollection(); virtual;
  public
    (* Friend Protected declarations *)

    procedure NotifyBeforeChangeText
      (const ANode: TUMLCContainerTreeNode;
       const AText: string); virtual;
    procedure NotifyAfterChangeText
      (const ANode: TUMLCContainerTreeNode); virtual;

    procedure NotifyBeforeChangeSelected
      (const ANode: TUMLCContainerTreeNode;
       const ASelected: Boolean); virtual;
    procedure NotifyAfterChangeSelected
      (const ANode: TUMLCContainerTreeNode); virtual;

    procedure NotifyBeforeInsert
      (const AParentNode, ANode: TUMLCContainerTreeNode;
       const AIndex: Integer); virtual;
    procedure NotifyAfterInsert
      (const AParentNode, ANode: TUMLCContainerTreeNode;
       const AIndex: Integer); virtual;

    procedure NotifyBeforeRemove
      (const ANode: TUMLCContainerTreeNode); virtual;
    procedure NotifyAfterRemove
      (const ANode: TUMLCContainerTreeNode); virtual;

    procedure NotifyBeforeEmpty
      (const ANode: TUMLCContainerTreeNode); virtual;
    procedure NotifyAfterEmpty
      (const ANode: TUMLCContainerTreeNode); virtual;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    property Items: TUMLCContainerTreeCollection
      read getItems write setItems;
  end;

(* TCustomUMLCTreeContainer *)

  TUMLCTreeContainer = class(TCustomUMLCTreeContainer)
  published
    (* published declarations *)

    (* TUMLCActivatedComponent *)

    (* TCustomUMLCTreeContainer *)
  end;

implementation

(* TUMLCContainerTreeNode *)

function TUMLCContainerTreeNode.getSelected(): Boolean;
begin
  Result := FSelected;
end;

procedure TUMLCContainerTreeNode.setSelected(const AValue: Boolean);
var ACollection: TUMLCContainerTreeCollection;
begin
  if (FSelected <> AValue) then
  begin
    ACollection := TUMLCContainerTreeCollection(InternalCollection);
    ACollection.RequestChangeSelected(Self, AValue);
  end;
end;

procedure TUMLCContainerTreeNode.ConfirmedCopyTo(var ADestNode: TUMLCTreeNode);
begin
  inherited ConfirmedCopyTo(ADestNode);

  // this field does not applies to be copied or moved
  Self.FSelected := false;
end;

procedure TUMLCContainerTreeNode.ConfirmedMoveTo(var ADestNode: TUMLCTreeNode);
begin
  inherited ConfirmedMoveTo(ADestNode);

  // this field does not applies to be copied or moved
  Self.FSelected := false;
end;

procedure TUMLCContainerTreeNode.ApplySelectAll
 (var ANode: TUMLCTreeNode; const AParam: pointer);
var AContainerNode: TUMLCContainerTreeNode;
begin
  AContainerNode := TUMLCContainerTreeNode(ANode);
  AContainerNode.Selected := true;
end;

procedure TUMLCContainerTreeNode.ApplySelectNone
(var ANode: TUMLCTreeNode; const AParam: pointer);
var AContainerNode: TUMLCContainerTreeNode;
begin
  AContainerNode.Selected := false;
end;

procedure TUMLCContainerTreeNode.ApplySelectToggle
(var ANode: TUMLCTreeNode; const AParam: pointer);
var AContainerNode: TUMLCContainerTreeNode;
begin
  AContainerNode.Selected :=
    (not AContainerNode.Selected);
end;

procedure TUMLCContainerTreeNode.ApplySelectAs
  (var ANode: TUMLCTreeNode; const AParam: pointer);
var AContainerNode: TUMLCContainerTreeNode; AValue: Boolean;
begin
  AValue := PBoolean(AParam)^;
  AContainerNode.Selected := AValue;
end;

procedure TUMLCContainerTreeNode.ConfirmedChangeSelected
  (const ASelected: Boolean);
begin
  FSelected := ASelected;
end;

procedure TUMLCContainerTreeNode.DoCreate();
begin
  inherited DoCreate();
  FSelected := false;
end;

procedure TUMLCContainerTreeNode.DoDestroy();
begin
  FSelected := false;
  inherited DoDestroy();
end;

procedure TUMLCContainerTreeNode.SelectAll();
begin
  {$ifdef Delphi}
  ForEachForward(ApplySelectAll, nil);
  {$else}
  ForEachForward(@ApplySelectAll, nil);
  {$endif}
end;

procedure TUMLCContainerTreeNode.SelectNone();
begin
  {$ifdef Delphi}
  ForEachForward(ApplySelectNone, nil);
  {$else}
  ForEachForward(@ApplySelectNone, nil);
  {$endif}
end;

procedure TUMLCContainerTreeNode.SelectToggle();
begin
  {$ifdef Delphi}
  ForEachForward(ApplySelectToggle, nil);
  {$else}
  ForEachForward(@ApplySelectToggle, nil);
  {$endif}
end;

procedure TUMLCContainerTreeNode.SelectAs(const AValue: Boolean);
begin
  {$ifdef Delphi}
  ForEachForward(ApplySelectAs, @AValue);
  {$else}
  ForEachForward(@ApplySelectAs, @AValue);
  {$endif}
end;

(* TUMLCContainerTreeCollection *)

function TUMLCContainerTreeCollection.getContainer(): TCustomUMLCTreeContainer;
begin
  Result := FContainer;
end;

procedure TUMLCContainerTreeCollection.setContainer
  (const AValue: TCustomUMLCTreeContainer);
begin
  FContainer := AValue;
end;

function TUMLCContainerTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCContainerTreeNode.Create();
  Result.DoCreate();
end;

procedure TUMLCContainerTreeCollection.InternalBeforeChangeText
  (const ANode: TUMLCTextTreeNode; const AText: string);
var AContainerNode: TUMLCContainerTreeNode;
begin
  AContainerNode := TUMLCContainerTreeNode(ANode);
  Self.Container.NotifyBeforeChangeText(AContainerNode, AText);
end;

procedure TUMLCContainerTreeCollection.InternalAfterChangeText
  (const ANode: TUMLCTextTreeNode);
var AContainerNode: TUMLCContainerTreeNode;
begin
  AContainerNode := TUMLCContainerTreeNode(ANode);
  Self.Container.NotifyAfterChangeText(AContainerNode);
end;

procedure TUMLCContainerTreeCollection.InternalBeforeChangeSelected
  (const ANode: TUMLCContainerTreeNode; const ASelected: Boolean);
begin
  Self.Container.NotifyBeforeChangeSelected(ANode, ASelected);
end;

procedure TUMLCContainerTreeCollection.InternalAfterChangeSelected
  (const ANode: TUMLCContainerTreeNode);
begin
  Self.Container.NotifyAfterChangeSelected(ANode);
end;

procedure TUMLCContainerTreeCollection.InternalConfirmedChangeSelected
  (const ANode: TUMLCContainerTreeNode; const ASelected: Boolean);
begin
  ANode.ConfirmedChangeSelected(ASelected);
end;

procedure TUMLCContainerTreeCollection.InternalBeforeInsert
  (const AParentNode, ANewNode: TUMLCTreeNode; const AIndex: Integer);
var AContainerParentNode, AContainerNewNode: TUMLCContainerTreeNode;
begin
  AContainerParentNode := TUMLCContainerTreeNode(AParentNode);
  AContainerNewNode    := TUMLCContainerTreeNode(ANewNode);
  Self.Container.NotifyBeforeInsert
    (AContainerParentNode, AContainerNewNode, AIndex);
end;

procedure TUMLCContainerTreeCollection.InternalAfterInsert
  (const AParentNode, ANewNode: TUMLCTreeNode; const AIndex: Integer);
var AContainerParentNode, AContainerNewNode: TUMLCContainerTreeNode;
begin
  inherited InternalAfterInsert(AParentNode, ANewNode, AIndex);

  AContainerParentNode := TUMLCContainerTreeNode(AParentNode);
  AContainerNewNode    := TUMLCContainerTreeNode(ANewNode);
  Self.Container.NotifyAfterInsert
    (AContainerParentNode, AContainerNewNode, AIndex);
end;

procedure TUMLCContainerTreeCollection.InternalBeforeRemove
  (const ANode: TUMLCTreeNode);
var AContainerNode: TUMLCContainerTreeNode;
begin
  AContainerNode := TUMLCContainerTreeNode(ANode);
  Self.Container.NotifyBeforeRemove(AContainerNode);
end;

procedure TUMLCContainerTreeCollection.InternalAfterRemove
  (const ANode: TUMLCTreeNode);
var AContainerNode: TUMLCContainerTreeNode;
begin
  AContainerNode := TUMLCContainerTreeNode(ANode);
  Self.Container.NotifyAfterRemove(AContainerNode);
end;

procedure TUMLCContainerTreeCollection.InternalBeforeEmpty
  (const ANode: TUMLCTreeNode);
var AContainerNode: TUMLCContainerTreeNode;
begin
  AContainerNode := TUMLCContainerTreeNode(ANode);
  Self.Container.NotifyBeforeEmpty(AContainerNode);
end;

procedure TUMLCContainerTreeCollection.InternalAfterEmpty
  (const ANode: TUMLCTreeNode);
var AContainerNode: TUMLCContainerTreeNode;
begin
  AContainerNode := TUMLCContainerTreeNode(ANode);
  Self.Container.NotifyAfterEmpty(AContainerNode);
end;

procedure TUMLCContainerTreeCollection.RequestChangeSelected
  (const ANode: TUMLCContainerTreeNode; const ASelected: Boolean);
begin
  InternalBeforeChangeSelected(ANode, ASelected);
  InternalConfirmedChangeSelected(ANode, ASelected);
  InternalAfterChangeSelected(ANode);
end;

procedure TUMLCContainerTreeCollection.DoCreate();
begin
  inherited DoCreate();
end;

procedure TUMLCContainerTreeCollection.DoDestroy();
begin
  inherited DoDestroy();
end;

(* TCustomUMLCTreeContainer *)

function TCustomUMLCTreeContainer.getItems(): TUMLCContainerTreeCollection;
begin
  Result := FItems
  // Goal: "Items" property get method .
  // Objetivo: Metodo lectura para propiedad "Items".
end;

procedure TCustomUMLCTreeContainer.setItems
  (const AValue: TUMLCContainerTreeCollection);
begin
  FItems := AValue;
  // Goal: "Items" property set method .
  // Objetivo: Metodo escritura para propiedad "Items".
end;

function TCustomUMLCTreeContainer.CreateCollectionByClass(): TUMLCContainerTreeCollection;
begin
  Result := TUMLCContainerTreeCollection.Create();
  Result.DoCreate();
  // Goal: Create inheretable (polimorphic) collection.
  // Objetivo: Crear coleccion heredable (polimorfica).
end;

procedure TCustomUMLCTreeContainer.CreateCollection();
begin
  FItems := CreateCollectionByClass();
  FItems.Container := Self;
end;

procedure TCustomUMLCTreeContainer.DestroyCollection();
begin
  FItems.DoDestroy();
end;

procedure TCustomUMLCTreeContainer.NotifyBeforeChangeText
  (const ANode: TUMLCContainerTreeNode; const AText: string);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCTreeContainer.NotifyAfterChangeText
  (const ANode: TUMLCContainerTreeNode);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCTreeContainer.NotifyBeforeChangeSelected
  (const ANode: TUMLCContainerTreeNode; const ASelected: Boolean);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCTreeContainer.NotifyAfterChangeSelected
  (const ANode: TUMLCContainerTreeNode);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCTreeContainer.NotifyBeforeInsert
  (const AParentNode, ANode: TUMLCContainerTreeNode; const AIndex: Integer);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCTreeContainer.NotifyAfterInsert
  (const AParentNode, ANode: TUMLCContainerTreeNode; const AIndex: Integer);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCTreeContainer.NotifyBeforeRemove
  (const ANode: TUMLCContainerTreeNode);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCTreeContainer.NotifyAfterRemove
  (const ANode: TUMLCContainerTreeNode);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCTreeContainer.NotifyBeforeEmpty
  (const ANode: TUMLCContainerTreeNode);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCTreeContainer.NotifyAfterEmpty
  (const ANode: TUMLCContainerTreeNode);
begin
  Self.DoNothing();
end;

constructor TCustomUMLCTreeContainer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  CreateCollection();
end;

destructor TCustomUMLCTreeContainer.Destroy();
begin
  DestroyCollection();
  inherited Destroy();
end;

end.

