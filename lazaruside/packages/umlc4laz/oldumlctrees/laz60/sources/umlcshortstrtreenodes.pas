(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcshortstrtreenodes;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  umlccomparisons,
  umlcshortansistrs,
  umlctreenodes,
  dummy;

(**
 ** Description:
 ** This unit contains several in order to support an non visual,
 ** hierarchical ( "tree" ) collection of data.
 **
 ** It was not designed with generics collections.
 **
 ** Additionally, a "Text" property and helper functions are provided.
 **
 ** The "Text" property is a standard Pascal 256 bytes array,
 ** with the first, zero indexed item, storing the count of characters.
 **)

type

 (* TUMLCShortStringTreeNode *)

   TUMLCShortStringTreeNode = class(TUMLCTreeNode)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     FText: shortstring;
   protected
     (* Protected declarations *)

     (* Accessors declarations *)

     function getText(): shortstring; virtual;

     procedure setText(const AValue: shortstring); virtual;
   public
     (* Public declarations *)

     function IsEmpty(): Boolean;

     function EqualAs(const AStr: shortstring): Boolean;
     function SameAs(const AStr: shortstring): Boolean;

     function StartsWith(const ASubStr: shortstring): Boolean;
     function FinishesWith(const ASubStr: shortstring): Boolean;

     function ComparesWith(const AStr: shortstring): TComparison;

     procedure Clear();
   public
     (* Public declarations *)

     property Text: shortstring
      read getText write setText;
   end;

 (* TUMLCPascalStringTreeCollection *)

   TUMLCPascalStringTreeCollection = class(TUMLCTreeCollection)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)
   protected
     (* Protected declarations *)

     function CreateNodeByClass(): TUMLCTreeNode; override;
   public
     (* Friend Protected declarations *)


   public
     (* Public declarations *)
   end;

implementation

(* TUMLCShortStringTreeNode *)

function TUMLCShortStringTreeNode.getText(): shortstring;
begin
  Result := FText;
end;

procedure TUMLCShortStringTreeNode.setText(const AValue: shortstring);
//var ACollection: TUMLCNullStringTreeCollection;
begin
  (*
  if (not EqualAs(FText, AValue)) then
  begin
    ACollection := TUMLCNullStringTreeCollection(InternalCollection);
    ACollection.RequestChangeText(Self, AValue);
  end;
  *)
end;

function TUMLCShortStringTreeNode.IsEmpty: Boolean;
begin
  Result := umlcshortansistrs.IsEmpty(FText);
end;

function TUMLCShortStringTreeNode.EqualAs(const AStr: shortstring): Boolean;
begin
  Result := (FText = AStr);
  // Goal: Case Sensitive comparison.
end;

function TUMLCShortStringTreeNode.SameAs(const AStr: shortstring): Boolean;
begin
  Result := umlcshortansistrs.SameText(FText, AStr);
  // Goal: Case Insensitive comparison.
end;

function TUMLCShortStringTreeNode.StartsWith
  (const ASubStr: shortstring): Boolean;
begin
  Result := umlcshortansistrs.StartsWith(ASubStr, FText);
end;

function TUMLCShortStringTreeNode.FinishesWith
  (const ASubStr: shortstring): Boolean;
begin
  Result := umlcshortansistrs.FinishesWith(ASubStr, FText);
end;

function TUMLCShortStringTreeNode.ComparesWith
  (const AStr: shortstring): TComparison;
begin
  Result := umlcshortansistrs.Compare(AStr, FText);
end;

procedure TUMLCShortStringTreeNode.Clear();
begin
  Self.Text := '';
end;

(* TUMLCPascalStringTreeCollection *)

function TUMLCPascalStringTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCShortStringTreeNode.Create();
  Result.DoCreate();
end;

end.

