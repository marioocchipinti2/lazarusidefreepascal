(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdelegtreecntrs;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  //umlcmsgctrls,
  umlctreenodes,
  umlctreecntrs,
  dummy;

(**
 ** Description:
 ** This unit contains several classes.
 **
 ** The main class is "TCustomUMLCMsgTreeContainer",
 ** a non-visual component that contains a hierarchical,
 ** tree structure.
 **
 ** Additionaly, it supports the Subject part,
 ** of the Subject-Observer Pattern,
 ** and its observers maybe visual controls, or not.
 **)

 type

(* TUMLCDelegateTreeNode *)

  TCustomUMLCDelegateTreeContainer = class;

  TUMLCDelegateTreeNode = class(TUMLCContainerTreeNode)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  end;

(* TUMLCDelegateTreeCollection *)

  TUMLCDelegateTreeCollection = class(TUMLCContainerTreeCollection)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  end;

(* TCustomUMLCDelegateTreeContainer *)

  TCustomUMLCDelegateTreeContainer = class(TCustomUMLCTreeContainer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FBeforeChangeText: TOnUMLCBeforeChangeStringTreeNodeEvent;
    FAfterChangeText:  TOnUMLCAfterChangeStringTreeNodeEvent;

    FBeforeChangeSelected: TOnUMLCBeforeBooleanTreeNodeEvent;
    FAfterChangeSelected:  TOnUMLCAfterBooleanTreeNodeEvent;

    FBeforeInsertNode: TOnUMLCInsertAtTreeNodeEvent;
    FAfterInsertNode:  TOnUMLCInsertAtTreeNodeEvent;

    FBeforeRemoveNode: TOnUMLCTreeContainerNodeEvent;
    FAfterRemoveNode:  TOnUMLCTreeContainerNodeEvent;

    FBeforeEmptyNode:  TOnUMLCTreeContainerNodeEvent;
    FAfterEmptyNode:   TOnUMLCTreeContainerNodeEvent;
  protected
    (* Protected declarations *)

    procedure DelegateBeforeChangeText
      (const ANode: TUMLCContainerTreeNode;
       const AValue: string); (* nonvirtual; *)
    procedure DelegateAfterChangeText
      (const ANode: TUMLCContainerTreeNode); (* nonvirtual; *)

    procedure DelegateBeforeChangeSelected
      (const ANode: TUMLCContainerTreeNode;
       const AValue: Boolean); (* nonvirtual; *)
    procedure DelegateAfterChangeSelected
      (const ANode: TUMLCContainerTreeNode); (* nonvirtual; *)

    procedure DelegateBeforeInsertNode
      (const AParentNode, ANode: TUMLCContainerTreeNode;
       const AIndex: Integer); (* nonvirtual; *)
    procedure DelegateAfterInsertNode
      (const AParentNode, ANode: TUMLCContainerTreeNode;
       const AIndex: Integer); (* nonvirtual; *)

    procedure DelegateBeforeRemoveNode
      (const ANode: TUMLCContainerTreeNode); (* nonvirtual; *)
    procedure DelegateAfterRemoveNode
      (const ANode: TUMLCContainerTreeNode); (* nonvirtual; *)

    procedure DelegateBeforeEmptyNode
      (const ANode: TUMLCContainerTreeNode); (* nonvirtual; *)
    procedure DelegateAfterEmptyNode
      (const ANode: TUMLCContainerTreeNode); (* nonvirtual; *)
  public
    (* Friend Protected declarations *)

    procedure NotifyBeforeChangeText
      (const ANode: TUMLCContainerTreeNode;
       const AText: string); override;
    procedure NotifyAfterChangeText
      (const ANode: TUMLCContainerTreeNode); override;

    procedure NotifyBeforeChangeSelected
      (const ANode: TUMLCContainerTreeNode;
       const ASelected: Boolean); override;
    procedure NotifyAfterChangeSelected
      (const ANode: TUMLCContainerTreeNode); override;

    procedure NotifyBeforeInsert
      (const AParentNode, ANode: TUMLCContainerTreeNode;
       const AIndex: Integer); override;
    procedure NotifyAfterInsert
      (const AParentNode, ANode: TUMLCContainerTreeNode;
       const AIndex: Integer); override;

    procedure NotifyBeforeRemove
      (const ANode: TUMLCContainerTreeNode); override;
    procedure NotifyAfterRemove
      (const ANode: TUMLCContainerTreeNode); override;

    procedure NotifyBeforeEmpty
      (const ANode: TUMLCContainerTreeNode); override;
    procedure NotifyAfterEmpty
      (const ANode: TUMLCContainerTreeNode); override;
  protected
    (* Protected declarations *)

    function CreateCollectionByClass(): TUMLCContainerTreeCollection; override;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
  public
    (* Public declarations *)

    (* Unpublished declarations *)

    property BeforeChangeText: TOnUMLCBeforeChangeStringTreeNodeEvent
      read FBeforeChangeText write FBeforeChangeText;
    property AfterChangeText: TOnUMLCAfterChangeStringTreeNodeEvent
      read FAfterChangeText write FAfterChangeText;

    property BeforeChangeSelected: TOnUMLCBeforeBooleanTreeNodeEvent
      read FBeforeChangeSelected write FBeforeChangeSelected;
    property AfterChangeSelected: TOnUMLCAfterBooleanTreeNodeEvent
      read FAfterChangeSelected write FAfterChangeSelected;

    property BeforeInsertNode: TOnUMLCInsertAtTreeNodeEvent
      read FBeforeInsertNode write FBeforeInsertNode;
    property AfterInsertNode: TOnUMLCInsertAtTreeNodeEvent
      read FAfterInsertNode write FAfterInsertNode;

    property BeforeEmptyNode: TOnUMLCTreeContainerNodeEvent
      read FBeforeEmptyNode write FBeforeEmptyNode;
    property AfterEmptyNode: TOnUMLCTreeContainerNodeEvent
      read FAfterEmptyNode write FAfterEmptyNode;

    property BeforeRemoveNode: TOnUMLCTreeContainerNodeEvent
      read FBeforeRemoveNode write FBeforeRemoveNode;
    property AfterRemoveNode: TOnUMLCTreeContainerNodeEvent
      read FAfterRemoveNode write FAfterRemoveNode;
  end;

  (* TUMLCDelegateTreeContainer *)

    TUMLCDelegateTreeContainer = class(TCustomUMLCDelegateTreeContainer)
    published
      (* Published declarations *)

      (* TCustomUMLCDelegateTreeContainer: *)

      property BeforeChangeText;
      property AfterChangeText;

      property BeforeChangeSelected;
      property AfterChangeSelected;

      property BeforeEmptyNode;
      property AfterEmptyNode;

      property BeforeRemoveNode;
      property AfterRemoveNode;
    end;

implementation

procedure TUMLCDelegateTreeNode.DoCreate();
begin
  inherited DoCreate();
end;

procedure TUMLCDelegateTreeNode.DoDestroy();
begin
  inherited DoDestroy();
end;

(* TUMLCDelegateTreeNode *)

(* TUMLCDelegateTreeCollection *)

function TUMLCDelegateTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCDelegateTreeNode.Create();
  Result.DoCreate();
end;

procedure TUMLCDelegateTreeCollection.DoCreate();
begin
  inherited DoCreate();
end;

procedure TUMLCDelegateTreeCollection.DoDestroy();
begin
  inherited DoDestroy();
end;

(* TCustomUMLCDelegateTreeContainer *)

procedure TCustomUMLCDelegateTreeContainer.DelegateBeforeChangeText
  (const ANode: TUMLCContainerTreeNode; const AValue: string);
begin
  if (FBeforeChangeText <> nil) then
  begin
    FBeforeChangeText(ANode, AValue);
  end;
end;

procedure TCustomUMLCDelegateTreeContainer.DelegateAfterChangeText
  (const ANode: TUMLCContainerTreeNode);
begin
  if (FAfterChangeText <> nil) then
  begin
    FAfterChangeText(ANode);
  end;
end;

procedure TCustomUMLCDelegateTreeContainer.DelegateBeforeChangeSelected
  (const ANode: TUMLCContainerTreeNode; const AValue: Boolean);
begin
  if (FBeforeChangeSelected <> nil) then
  begin
    FBeforeChangeSelected(ANode, AValue);
  end;
end;

procedure TCustomUMLCDelegateTreeContainer.DelegateAfterChangeSelected
  (const ANode: TUMLCContainerTreeNode);
begin
  if (FAfterChangeSelected <> nil) then
  begin
    FAfterChangeSelected(ANode);
  end;
end;

procedure TCustomUMLCDelegateTreeContainer.DelegateBeforeInsertNode
  (const AParentNode, ANode: TUMLCContainerTreeNode;
   const AIndex: Integer);
begin
  if (FBeforeInsertNode <> nil) then
  begin
    FBeforeInsertNode(AParentNode, ANode, AIndex);
  end;
end;

procedure TCustomUMLCDelegateTreeContainer.DelegateAfterInsertNode
  (const AParentNode, ANode: TUMLCContainerTreeNode;
   const AIndex: Integer);
begin
  if (FAfterInsertNode <> nil) then
  begin
    FAfterInsertNode(AParentNode, ANode, AIndex);
  end;
end;

procedure TCustomUMLCDelegateTreeContainer.DelegateBeforeRemoveNode
  (const ANode: TUMLCContainerTreeNode);
begin
  if (FBeforeRemoveNode <> nil) then
  begin
    FBeforeRemoveNode(ANode);
  end;
end;

procedure TCustomUMLCDelegateTreeContainer.DelegateAfterRemoveNode
  (const ANode: TUMLCContainerTreeNode);
begin
  if (FAfterRemoveNode <> nil) then
  begin
    FAfterRemoveNode(ANode);
  end;
end;

procedure TCustomUMLCDelegateTreeContainer.DelegateBeforeEmptyNode
  (const ANode: TUMLCContainerTreeNode);
begin
  if (FBeforeEmptyNode <> nil) then
  begin
    FBeforeEmptyNode(ANode);
  end;
end;

procedure TCustomUMLCDelegateTreeContainer.DelegateAfterEmptyNode
  (const ANode: TUMLCContainerTreeNode);
begin
  if (FAfterEmptyNode <> nil) then
  begin
    FAfterEmptyNode(ANode);
  end;
end;

procedure TCustomUMLCDelegateTreeContainer.NotifyBeforeChangeText
  (const ANode: TUMLCContainerTreeNode; const AText: string);
begin
  DelegateBeforeChangeText(ANode, AText);
end;

procedure TCustomUMLCDelegateTreeContainer.NotifyAfterChangeText
  (const ANode: TUMLCContainerTreeNode);
begin
  DelegateAfterChangeText(ANode);
end;

procedure TCustomUMLCDelegateTreeContainer.NotifyBeforeChangeSelected
  (const ANode: TUMLCContainerTreeNode; const ASelected: Boolean);
begin
  DelegateBeforeChangeSelected(ANode, ASelected);
end;

procedure TCustomUMLCDelegateTreeContainer.NotifyAfterChangeSelected
  (const ANode: TUMLCContainerTreeNode);
begin
  DelegateAfterChangeSelected(ANode);
end;

procedure TCustomUMLCDelegateTreeContainer.NotifyBeforeInsert
  (const AParentNode, ANode: TUMLCContainerTreeNode;
   const AIndex: Integer);
begin
  DelegateBeforeInsertNode(AParentNode, ANode, AIndex);
  // Goal: Delegate insert operation.
  // Objetivo: Delegar la operacion de insertar.
end;

procedure TCustomUMLCDelegateTreeContainer.NotifyAfterInsert
  (const AParentNode, ANode: TUMLCContainerTreeNode;
   const AIndex: Integer);
begin
  DelegateAfterInsertNode(AParentNode, ANode, AIndex);
  // Goal: Delegate insert operation.
  // Objetivo: Delegar la operacion de insertar.
end;

procedure TCustomUMLCDelegateTreeContainer.NotifyBeforeRemove
  (const ANode: TUMLCContainerTreeNode);
begin
  DelegateBeforeRemoveNode(ANode);
  // Goal: Delegate remove operation.
  // Objetivo: Delegar la operacion de remover.
end;

procedure TCustomUMLCDelegateTreeContainer.NotifyAfterRemove
  (const ANode: TUMLCContainerTreeNode);
begin
  DelegateAfterRemoveNode(ANode);
  // Goal: Delegate remove operation.
  // Objetivo: Delegar la operacion de remover.
end;

procedure TCustomUMLCDelegateTreeContainer.NotifyBeforeEmpty
  (const ANode: TUMLCContainerTreeNode);
begin
  DelegateBeforeEmptyNode(ANode);
end;

procedure TCustomUMLCDelegateTreeContainer.NotifyAfterEmpty
  (const ANode: TUMLCContainerTreeNode);
begin
  DelegateAfterEmptyNode(ANode);
end;

function TCustomUMLCDelegateTreeContainer.CreateCollectionByClass(): TUMLCContainerTreeCollection;
begin
  Result := TUMLCDelegateTreeCollection.Create();
  Result.DoCreate();
  // Goal: Create inheretable (polimorphic) collection.
  // Objetivo: Crear coleccion heredable (polimorfica).
end;

constructor TCustomUMLCDelegateTreeContainer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  // clear event delegate (s)
  FBeforeChangeText := nil;
  FAfterChangeText  := nil;

  FBeforeChangeSelected := nil;
  FAfterChangeSelected  := nil;

  FBeforeInsertNode := nil;
  FAfterInsertNode  := nil;

  FBeforeEmptyNode  := nil;
  FAfterEmptyNode   := nil;

  FBeforeRemoveNode := nil;
  FAfterRemoveNode  := nil;
  // create inheretable (polimorphic) collection
  // crear coleccion heredable (polimorfica)
end;

end.

