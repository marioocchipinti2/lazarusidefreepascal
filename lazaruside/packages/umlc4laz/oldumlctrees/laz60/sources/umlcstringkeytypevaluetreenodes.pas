(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstringkeytypevaluetreenodes;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  umlcstrings,
  umlctreenodes,
  dummy;

(**
 ** Description:
 ** This unit contains several in order to support an non visual,
 ** hierarchical ( "tree" ) collection of data.
 **
 ** It was not designed with generics collections.
 **
 ** Additionally, a "Key", "Type, "Value" group of properties,
 ** and helper functions are provided,
 ** for each item.
 **
 ** Note: Its not a "KeyTypeValue" list property.
 **)

 type

 (* TUMLCStringKeyTypeValueTreeNode *)

   TUMLCStringKeyTypeValueTreeNode = class(TUMLCTreeNode)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     FKey:    string;
     FTypeID: string;
     FValue:  string;
   protected
     (* Protected declarations *)

   public
     (* Public declarations *)

     (* accesor declarations *)

     function getKey(): string; virtual;
     function getTypeID(): string; virtual;
     function getValue(): string; virtual;

     procedure setKey(const AValue: string); virtual;
     procedure setTypeID(const AValue: string); virtual;
     procedure setValue(const AValue: string); virtual;
   public
     (* Public declarations *)

     procedure DoCreate(); override;
     procedure DoDestroy(); override;
   public
     (* Public declarations *)

     function MatchKey(const AKey: string): Boolean;
     function MatchType(const AType: string): Boolean;
     function MatchValue(const AValue: string): Boolean;

     function EqualKey(const AKey: string): Boolean;
     function EqualType(const AType: string): Boolean;
     function EqualValue(const AValue: string): Boolean;

     function SameKey(const AKey: string): Boolean;
     function SameType(const AType: string): Boolean;
     function SameValue(const AValue: string): Boolean;

     function InsertByKeyTypeValue
       (const AKey, AType, AValue: string): TUMLCStringKeyTypeValueTreeNode;
     function InsertByKey
       (const AKey: string): TUMLCStringKeyTypeValueTreeNode;
     function InsertByKeyValue
       (const AKey, AValue: string): TUMLCStringKeyTypeValueTreeNode;
     function InsertByKeyType
       (const AKey, AType: string): TUMLCStringKeyTypeValueTreeNode;
   public
     (* Public declarations *)

     function NodeOfKey
       (const AKey: string): TUMLCStringKeyTypeValueTreeNode;
     function NodeOfType
       (const AType: string): TUMLCStringKeyTypeValueTreeNode;
     function NodeOfValue
       (const AValue: string): TUMLCStringKeyTypeValueTreeNode;

     function KeyFound(const AKey: string): Boolean;
     function TypeFound(const AValue: string): Boolean;
     function ValueFound(const AValue: string): Boolean;

     function ReplaceFirstKey
       (const APrevKey, ANewKey: string): Boolean;
     function ReplaceFirstTypeByKey
       (const AKey, AType: string): Boolean;
     function ReplaceFirstValueByKey
       (const AKey, AValue: string): Boolean;
     function ReplaceFirstValue
       (const APrevValue, ANewValue: string): Boolean;
     function ReplaceAllValues
       (const APrevValue, ANewValue: string): Boolean;
     function ReplaceAllTypes
       (const APrevType, ANewType: string): Boolean;
     function ReplaceAllValuesByType
       (const AType, ANewValue: string): Boolean;
   public
     (* Public declarations *)

     property Key: string
       read getKey write setKey;
     property TypeID: string
       read getTypeID write setTypeID;
     property Value: string
       read getValue write setValue;
   end;

 (* TUMLCStringKeyTypeValueTreeCollection *)

   TUMLCStringKeyTypeValueTreeCollection = class(TUMLCTreeCollection)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     _AllowDuplicates: boolean;
     _IgnoreKeyCase:   boolean;
     _IgnoreTypeCase:  boolean;
     _IgnoreValueCase: boolean;
   protected
     (* Protected declarations *)

     (* accesor declarations *)

     function getAllowDuplicates(): boolean;
     function getIgnoreKeyCase(): boolean;
     function getIgnoreTypeCase(): boolean;
     function getIgnoreValueCase(): boolean;

     procedure setAllowDuplicates(const AValue: boolean);
     procedure setIgnoreKeyCase(const AValue: boolean);
     procedure setIgnoreTypeCase(AValue: boolean);
     procedure setIgnoreValueCase(const AValue: boolean);
   protected
     (* Protected declarations *)

     function CreateNodeByClass(): TUMLCTreeNode; override;
   public
     (* Friend Protected declarations *)

     function InternalMatchKeys(const A, B: string): Boolean;
     function InternalMatchTypes(const A, B: string): Boolean;
     function InternalMatchValues(const A, B: string): Boolean;
   public
     (* Public declarations *)

     property AllowDuplicates: boolean
       read getAllowDuplicates write setAllowDuplicates;
     property IgnoreKeyCase: boolean
       read getIgnoreKeyCase write setIgnoreKeyCase;
     property IgnoreTypeCase: boolean
       read getIgnoreTypeCase write setIgnoreTypeCase;
     property IgnoreValueCase: boolean
       read getIgnoreValueCase write setIgnoreValueCase;
   end;

implementation

(* TUMLCStringKeyTypeValueTreeNode *)

function TUMLCStringKeyTypeValueTreeNode.getKey(): string;
begin
  Result := Self.FKey;
end;

function TUMLCStringKeyTypeValueTreeNode.getTypeID(): string;
begin
  Result := Self.FTypeID;
end;

function TUMLCStringKeyTypeValueTreeNode.getValue(): string;
begin
  Result := Self.FValue;
end;

procedure TUMLCStringKeyTypeValueTreeNode.setKey(const AValue: string);
begin
  Self.FKey := AValue;
end;

procedure TUMLCStringKeyTypeValueTreeNode.setTypeID(const AValue: string);
begin
  Self.FTypeID := AValue;
end;

procedure TUMLCStringKeyTypeValueTreeNode.setValue(const AValue: string);
begin
  Self.FValue := AValue;
end;

procedure TUMLCStringKeyTypeValueTreeNode.DoCreate();
begin
  inherited DoCreate();

  Self.FKey   := '';
  Self.FValue := '';
end;

procedure TUMLCStringKeyTypeValueTreeNode.DoDestroy();
begin
  Self.FValue := '';
  Self.FKey   := '';

  inherited DoDestroy();
end;

function TUMLCStringKeyTypeValueTreeNode.MatchKey(const AKey: string): Boolean;
var ACollection: TUMLCStringKeyTypeValueTreeCollection;
begin
  ACollection := TUMLCStringKeyTypeValueTreeCollection(InternalCollection);
  Result := ACollection.InternalMatchKeys(Self.FKey, AKey);
end;

function TUMLCStringKeyTypeValueTreeNode.MatchType(const AType: string): Boolean;
var ACollection: TUMLCStringKeyTypeValueTreeCollection;
begin
  ACollection := TUMLCStringKeyTypeValueTreeCollection(InternalCollection);
  Result := ACollection.InternalMatchKeys(Self.FTypeID, AType);
end;

function TUMLCStringKeyTypeValueTreeNode.MatchValue(const AValue: string): Boolean;
var ACollection: TUMLCStringKeyTypeValueTreeCollection;
begin
  ACollection := TUMLCStringKeyTypeValueTreeCollection(InternalCollection);
  Result := ACollection.InternalMatchKeys(Self.FValue, AValue);
end;

function TUMLCStringKeyTypeValueTreeNode.EqualKey(const AKey: string): Boolean;
begin
  Result := (Self.FKey = AKey);
end;

function TUMLCStringKeyTypeValueTreeNode.EqualType(const AType: string): Boolean;
begin
  Result := (Self.FTypeID = AType);
end;

function TUMLCStringKeyTypeValueTreeNode.EqualValue(const AValue: string): Boolean;
begin
  Result := (Self.FValue = AValue);
end;

function TUMLCStringKeyTypeValueTreeNode.SameKey(const AKey: string): Boolean;
begin
  Result := umlcstrings.SameText(Self.FKey, AKey);
end;

function TUMLCStringKeyTypeValueTreeNode.SameType(const AType: string): Boolean;
begin
  Result := umlcstrings.SameText(Self.FTypeID, AType);
end;

function TUMLCStringKeyTypeValueTreeNode.SameValue(const AValue: string): Boolean;
begin
  Result := umlcstrings.SameText(Self.FValue, AValue);
end;

function TUMLCStringKeyTypeValueTreeNode.InsertByKeyTypeValue
  (const AKey, AType, AValue: string): TUMLCStringKeyTypeValueTreeNode;
var CanInsert: Boolean; ACollection: TUMLCStringKeyTypeValueTreeCollection;
begin
  CanInsert := true;
  ACollection := TUMLCStringKeyTypeValueTreeCollection(InternalCollection);
  if (not ACollection.AllowDuplicates) then
  begin
    CanInsert := (not MatchKey(AKey));
  end;

  if (CanInsert) then
  begin
    Result := (Self.Insert() as TUMLCStringKeyTypeValueTreeNode);
    Result.Key    := AKey;
    Result.TypeID := AType;
    Result.Value  := AValue;
  end;
end;

function TUMLCStringKeyTypeValueTreeNode.InsertByKey
  (const AKey: string): TUMLCStringKeyTypeValueTreeNode;
begin
  Result := Self.InsertByKeyTypeValue(AKey, '', '');
end;

function TUMLCStringKeyTypeValueTreeNode.InsertByKeyValue
  (const AKey, AValue: string): TUMLCStringKeyTypeValueTreeNode;
begin
  Result := Self.InsertByKeyTypeValue(AKey, '', AValue);
end;

function TUMLCStringKeyTypeValueTreeNode.InsertByKeyType
  (const AKey, AType: string): TUMLCStringKeyTypeValueTreeNode;
begin
  Result := Self.InsertByKeyTypeValue(AKey, AType, '');
end;

function TUMLCStringKeyTypeValueTreeNode.NodeOfKey
  (const AKey: string): TUMLCStringKeyTypeValueTreeNode;
begin
  Result := nil;
end;

function TUMLCStringKeyTypeValueTreeNode.NodeOfType
  (const AType: string): TUMLCStringKeyTypeValueTreeNode;
begin
  Result := nil;
end;

function TUMLCStringKeyTypeValueTreeNode.NodeOfValue
  (const AValue: string): TUMLCStringKeyTypeValueTreeNode;
begin
  Result := nil;
end;

function TUMLCStringKeyTypeValueTreeNode.KeyFound(const AKey: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyTypeValueTreeNode.TypeFound
  (const AValue: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyTypeValueTreeNode.ValueFound(const AValue: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyTypeValueTreeNode.ReplaceFirstKey
  (const APrevKey, ANewKey: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyTypeValueTreeNode.ReplaceFirstTypeByKey
  (const AKey, AType: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyTypeValueTreeNode.ReplaceFirstValueByKey
  (const AKey, AValue: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyTypeValueTreeNode.ReplaceFirstValue
  (const APrevValue, ANewValue: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyTypeValueTreeNode.ReplaceAllValues
  (const APrevValue, ANewValue: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyTypeValueTreeNode.ReplaceAllTypes
  (const APrevType, ANewType: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyTypeValueTreeNode.ReplaceAllValuesByType
  (const AType, ANewValue: string): Boolean;
begin
  //
end;

(* TUMLCStringKeyTypeValueTreeCollection *)

function TUMLCStringKeyTypeValueTreeCollection.getAllowDuplicates(): boolean;
begin
  Result := Self._AllowDuplicates;
end;

function TUMLCStringKeyTypeValueTreeCollection.getIgnoreKeyCase(): boolean;
begin
  Result := Self._IgnoreKeyCase;
end;

function TUMLCStringKeyTypeValueTreeCollection.getIgnoreTypeCase: boolean;
begin
  Result := Self._IgnoreTypeCase;
end;

function TUMLCStringKeyTypeValueTreeCollection.getIgnoreValueCase(): boolean;
begin
  Result := Self._IgnoreValueCase;
end;

procedure TUMLCStringKeyTypeValueTreeCollection.setAllowDuplicates(const AValue: boolean);
begin
  Self._AllowDuplicates := AValue;
end;

procedure TUMLCStringKeyTypeValueTreeCollection.setIgnoreKeyCase(const AValue: boolean);
begin
  Self._IgnoreKeyCase := AValue;
end;

procedure TUMLCStringKeyTypeValueTreeCollection.setIgnoreTypeCase
  (AValue: boolean);
begin
  Self._IgnoreTypeCase := AValue;
end;

procedure TUMLCStringKeyTypeValueTreeCollection.setIgnoreValueCase(const AValue: boolean);
begin
  Self._IgnoreValueCase := AValue;
end;

function TUMLCStringKeyTypeValueTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCStringKeyTypeValueTreeNode.Create();
  Result.DoCreate();
end;

function TUMLCStringKeyTypeValueTreeCollection.InternalMatchKeys
  (const A, B: string): Boolean;
begin
  Result := false;
  if (IgnoreKeyCase) then
  begin
    Result := umlcstrings.SameText(A, B);
  end else
  begin
    Result := (A = B);
  end;
end;

function TUMLCStringKeyTypeValueTreeCollection.InternalMatchTypes
  (const A, B: string): Boolean;
begin
  Result := false;
  if (IgnoreTypeCase) then
  begin
    Result := umlcstrings.SameText(A, B);
  end else
  begin
    Result := (A = B);
  end;
end;

function TUMLCStringKeyTypeValueTreeCollection.InternalMatchValues
  (const A, B: string): Boolean;
begin
  Result := false;
  if (IgnoreValueCase) then
  begin
    Result := umlcstrings.SameText(A, B);
  end else
  begin
    Result := (A = B);
  end;
end;

end.

