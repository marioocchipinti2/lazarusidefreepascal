(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

(* "umlctrees_language.incpas" *)

{$UNDEF umlctrees_language_english}
{$UNDEF umlctrees_language_spanisheurope}
{$UNDEF umlctrees_language_spanishlatam}
{$UNDEF umlctrees_language_french}
{$UNDEF umlctrees_language_german}
{$UNDEF umlctrees_language_portuguese}
{$UNDEF umlctrees_language_italian}

{$define umlctrees_language_english}
{.define umlctrees_language_spanisheurope}
{.define umlctrees_language_spanishlatam}
{.define umlctrees_language_french}
{.define umlctrees_language_german}
{.define umlctrees_language_portuguese}
{.define umlctrees_language_italian}
