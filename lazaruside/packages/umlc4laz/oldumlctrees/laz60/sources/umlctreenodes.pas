(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctreenodes;

interface
uses
  SysUtils,
  Classes,
  //Forms, Dialogs,
  umlcguids,
  umlctypes, umlcstdtypes,
  //umlcBooleans,
  umlclists,
  umlccomponents,
  umlcactcntrls,
  dummy;

(**
 ** Description:
 ** This unit contains several in order to support an non visual,
 ** hierarchical ( "tree" ) collection of data.
 ** It was not designed with generics collections.
 **)

const

  // ---

  ID_TUMLCTreeDirection : TUMLCType =
    ($64,$09,$A3,$8C,$EA,$41,$54,$47,$A2,$B7,$70,$3A,$C0,$AC,$A3,$E6);

  ID_TUMLCTreeNode : TUMLCType =
    ($F8,$A2,$C7,$11,$B3,$BC,$9E,$4C,$8C,$99,$02,$C6,$47,$2E,$A1,$FE);

  ID_TUMLCTreeNodeList : TUMLCType =
    ($12,$7A,$A1,$28,$55,$2F,$14,$47,$91,$89,$94,$37,$A9,$2F,$20,$8E);

  ID_TUMLCTreeCollection : TUMLCType =
    ($8C,$23,$81,$41,$92,$C8,$A5,$4D,$9F,$5B,$0A,$9C,$39,$AC,$88,$70);

  // ---

type

  TUMLCTreeDirection = (tdNone, tdStart, tdFinish);

(* TUMLCTreeNodeForEachProc *)

  TUMLCTreeNode = (* forward *) class;

  TUMLCTreeNodeFunc =
    function
      (const ANode: TUMLCTreeNode; const AParam: pointer): Boolean of object;
  TUMLCTreeNodeForEachProc =
    procedure
      (var ANode: TUMLCTreeNode; const AParam: pointer) of object;
  TUMLCTreeNodeForBothProc =
    procedure
      (var ANode: TUMLCTreeNode;
       const AParam: pointer; const ADirection: TUMLCTreeDirection) of object;

(* TUMLCTreeNode *)

  TUMLCTreeNodeList = class;
  TUMLCTreeCollection = class;

  TUMLCTreeNode =
    class(TUMLCExtendedNormalizedPersistent)
  private
    (* Private declarations *)

  protected
    (* Protected declarations *)

    // unique identifier in collection
    FID:          TGUID;

    // not used, available to the controls user
    FData:        pointer;
  protected
    (* Protected declarations *)

    function getData(): pointer;
    function getID(): TGUID;

    procedure setData(const AValue: pointer);
    procedure setID(const AValue: TGUID);
  protected
    (* Protected declarations *)

    function MatchesNode
      (const ANode: TUMLCTreeNode; const AParam: pointer): Boolean;
    function MatchesID
      (const ANode: TUMLCTreeNode; const AParam: pointer): Boolean;
    function MatchesData
      (const ANode: TUMLCTreeNode; const AParam: pointer): Boolean;
  protected
    (* Protected declarations *)

    function CreateList(): TUMLCTreeNodeList; (* nonvirtual; *)

    procedure ConfirmedCopyTo(var ADestNode: TUMLCTreeNode); virtual;
    procedure ConfirmedMoveTo(var ADestNode: TUMLCTreeNode); virtual;
    procedure ConfirmedDuplicateTo(var ADestNode: TUMLCTreeNode); virtual;
  public
    (* Protected Friend declarations *)

    (* DO NOT become public or published declarations *)

    // reference to collection that contains all treenodes
    // it's not the subitems
    FInternalCollection:  TUMLCTreeCollection;
    FInternalIsRoot:      Boolean;
    FInternalLevel:       Integer;
    // similar libraries use "items" or "children" instead of "list"
    FInternalList:        TUMLCTreeNodeList;
    FInternalLocalIndex:  Integer;
    FInternalGlobalIndex: Integer;
    FInternalParent:      TUMLCTreeNode;
  public
    (* Protected Friend declarations *)

    (* DO NOT become public or published declarations *)

    function getInternalCollection(): TUMLCTreeCollection;
    function getInternalIsRoot(): Boolean;
    function getInternalLevel(): Integer;
    function getInternalList(): TUMLCTreeNodeList;
    function getInternalLocalIndex(): Integer;
    function getInternalGlobalIndex(): Integer;
    function getInternalParent(): TUMLCTreeNode;

    procedure setInternalCollection(const AValue: TUMLCTreeCollection);
    procedure setInternalIsRoot(const AValue: Boolean);
    procedure setInternalLevel(const AValue: Integer);
    procedure setInternalList(const AValue: TUMLCTreeNodeList);
    procedure setInternalLocalIndex(const AValue: Integer);
    procedure setInternalGlobalIndex(const AValue: Integer);
    procedure setInternalParent(const AValue: TUMLCTreeNode);
  public
    (* Protected Friend declarations *)

    (* DO NOT become public or published declarations *)

    // reference to collection that contains all treenodes
    // it's not the subitems
    property InternalCollection: TUMLCTreeCollection
      read getInternalCollection write setInternalCollection;
    property InternalIsRoot: Boolean
      read getInternalIsRoot write setInternalIsRoot;
    property InternalLevel: Integer
      read getInternalLevel write setInternalLevel;
    property InternalList:  TUMLCTreeNodeList
      read getInternalList write setInternalList;
    property InternalLocalIndex: Integer
      read getInternalLocalIndex write setInternalLocalIndex;
    property InternalGlobalIndex: Integer
      read getInternalGlobalIndex write setInternalGlobalIndex;
    property InternalParent: TUMLCTreeNode
      read getInternalParent write setInternalParent;
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  public
    (* Friend Protected declarations *)

    procedure ConfirmedInsertRoot(); virtual;
    procedure ConfirmedInsert(var ANode: TUMLCTreeNode); virtual;
    procedure ConfirmedInsertAt
      (const AIndex: Integer; var ANode: TUMLCTreeNode); virtual;

    procedure ConfirmedRemove(); virtual;
    procedure ConfirmedEmpty(); virtual;

    procedure UpdateLocalIndex
      (var ANode: TUMLCTreeNode; const AParam: pointer);
    procedure UpdateLocalIndices();

    procedure DuplicateNode
      (var ANode: TUMLCTreeNode;
       const AParam: pointer; const ADirection: TUMLCTreeDirection);
  public
    (* Public declarations *)

    function Insert(): TUMLCTreeNode; (* nonvirtual; *)
    function InsertAt
      (const AIndex: Integer): TUMLCTreeNode; (* nonvirtual; *)

    function InsertFirst(): TUMLCTreeNode; (* nonvirtual; *)

    function InsertAfterFirst(): TUMLCTreeNode; (* nonvirtual; *)
    function InsertBeforeLast(): TUMLCTreeNode; (* nonvirtual; *)

    procedure Remove(); (* nonvirtual; *)
    procedure Empty(); (* nonvirtual; *)
  public
    (* Public declarations *)

    procedure CopyTo(var ADestNode: TUMLCTreeNode); (* nonvirtual; *)
    procedure MoveTo(var ADestNode: TUMLCTreeNode); (* nonvirtual; *)

    function CanDuplicateTo(var ADestNode: TUMLCTreeNode): Boolean;
    procedure DuplicateTo(var ADestNode: TUMLCTreeNode); (* nonvirtual; *)
  public
    (* Public declarations *)

    function NodeByID(const ANID: TGUID): TUMLCTreeNode;
    function NodeByData(const AData: pointer): TUMLCTreeNode;

    function First(): TUMLCTreeNode;
    function Prev(): TUMLCTreeNode;
    function Next(): TUMLCTreeNode;
    function Last(): TUMLCTreeNode;
  protected
    (* Protected declarations *)

    function ConfirmedFirstThat
      (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
    function ConfirmedLastThat
      (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
    function ConfirmedFirstUp
      (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
  protected
    (* Protected declarations *)

    procedure ConfirmedForEachForward
      (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
    procedure ConfirmedForEachBackward
      (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
    procedure ConfirmedForUp
      (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
    procedure ConfirmedForBoth
      (const AProc: TUMLCTreeNodeForBothProc; const AParam: pointer);
  public
    (* Public declarations *)

    function FirstThat
      (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
    function LastThat
      (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
    function FirstUp
      (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;

    function FirstThatInmediate
      (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
    function LastThatInmediate
      (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
  public
    (* Public declarations *)

    procedure ForEachForward
      (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
    procedure ForEachBackward
      (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
    procedure ForUp
      (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
    procedure ForBoth
      (const AProc: TUMLCTreeNodeForBothProc; const AParam: pointer);

    procedure ForEachForwardInmediate
      (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
    procedure ForEachBackwardInmediate
      (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
  public
    (* Public declarations *)

    (* Read-Only properties *)

    function Count(): Integer;
    function HasItems(): Boolean;

    function Collection(): TUMLCTreeCollection;
    function GlobalIndex(): Integer;
    function Level(): Integer;
    function List(): TUMLCTreeNodeList;
    function LocalIndex(): Integer;
    function IsRoot(): Boolean;
    function Parent(): TUMLCTreeNode;
  public
    (* Public declarations *)

    (* DO NOT become published declarations *)

    property Data: pointer
      read getData write setData;
    property ID: TGUID
      read getID write setID;
  end;

 (*
  TUMLCTreeNodeList. It's a sequential collection,
  not a hierarchical collection,
  that stores references to treenodes.

  It's used for memory management.
 *)

(* TUMLCTreeNodeList *)

  TUMLCTreeNodeList =
    class(TCustomUMLCList)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function getItems(Index: Integer): TUMLCTreeNode;

    procedure setItems(Index: Integer; Item: TUMLCTreeNode);
  public
    (* Public declarations *)

    function IndexOf(const Item: TObject): Integer;
    function First(): TObject;
    function Last(): TObject;

    function Insert(const Item: TUMLCTreeNode): Integer;
    procedure InsertAt
      (const AIndex: Integer; const Item: TUMLCTreeNode);

    procedure Empty();
    procedure DeleteAt(const AIndex: Integer);

    function Remove(const Item: TObject): Integer;
    function Extract(const AIndex: Integer): TObject;

    function FirstThatInmediate
      (const Func: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
    function FirstBackInmediate
      (const Func: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;

    procedure ForEachForwardInmediate
      (const Proc: TUMLCTreeNodeForEachProc; const AParam: pointer);
    procedure ForEachBackwardInmediate
      (const Proc: TUMLCTreeNodeForEachProc; const AParam: pointer);

    property Items[Index: Integer]: TUMLCTreeNode
      read getItems write setItems; default;
  end;

(**
 ** TUMLCTreeCollection. It's a non visual, non G.U.I., non R.A.D.,
 ** hierarchical collection of items.
 **
 ** The collection its also the shared "Public Relations Department",
 ** for all nodes. External delegates, events or notifications,
 ** of nodes, to other objects, pass thru the collection.
 **)

(* TUMLCTreeCollection *)

  TUMLCTreeCollection =
    class(TUMLCActivatedPersistent)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FInternalRoot: TUMLCTreeNode;

    // this increments & reduces
    FGlobalCount: Integer;
    // this does not reduces as List.Count()
    FGlobalSequencer: Integer;
  protected
    (* Protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; virtual;

    procedure CreateRoot();
    procedure DestroyRoot();
  public
    (* Friend Protected declarations *)

    FBeenIterated: Boolean;

    FClipboardSourceRootNode: TUMLCTreeNode;
    FClipboardDestRootNode:   TUMLCTreeNode;
    FClipboardDestPtrNode:    TUMLCTreeNode;

    (* DO NOT become published declarations *)

    property InternalRoot: TUMLCTreeNode
      read FInternalRoot write FInternalRoot;
  public
    (* Friend Protected declarations *)

    (* Read-Only properties *)

    function GlobalSequencer(): Integer;
  protected
    (* Protected declarations *)

    procedure InternalBeforeInsert
      (const AParentNode, ANewNode: TUMLCTreeNode;
       const AIndex: Integer); virtual;
    procedure InternalAfterInsert
      (const AParentNode, ANewNode: TUMLCTreeNode;
       const AIndex: Integer); virtual;

    procedure InternalConfirmedInsert
      (const AParentNode: TUMLCTreeNode; var ANewNode: TUMLCTreeNode); virtual;
    procedure InternalConfirmedInsertAt
      (const AParentNode: TUMLCTreeNode;
       const AIndex: Integer; var ANewNode: TUMLCTreeNode); virtual;

    procedure InternalBeforeRemove
      (const ANode: TUMLCTreeNode); virtual;
    procedure InternalAfterRemove
      (const ANode: TUMLCTreeNode); virtual;

    procedure InternalConfirmedRemove
      (const AParentNode: TUMLCTreeNode;
       const ANode: TUMLCTreeNode); virtual;

    procedure InternalBeforeEmpty
      (const ANode: TUMLCTreeNode); virtual;
    procedure InternalAfterEmpty(
     const ANode: TUMLCTreeNode); virtual;

    procedure InternalConfirmedEmpty
      (const ANode: TUMLCTreeNode); virtual;
  public
    (* Friend Protected declarations *)

    function RequestInsert
      (const AParentNode: TUMLCTreeNode): TUMLCTreeNode;(* nonvirtual; *)
    function RequestInsertAt
      (const AParentNode: TUMLCTreeNode;
       const AIndex: Integer): TUMLCTreeNode; (* nonvirtual; *)

    procedure RequestRemove
      (const AParentNode: TUMLCTreeNode;
       const ANode: TUMLCTreeNode); (* nonvirtual; *)
    procedure RequestRemoveAt
      (const AParentNode: TUMLCTreeNode;
       const AIndex: Integer); (* nonvirtual; *)

    procedure RequestEmpty
      (const ANode: TUMLCTreeNode); (* nonvirtual; *)
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  public
    (* Public declarations *)

    function InsertRoot(): TUMLCTreeNode; virtual;
    procedure DropRoot(); virtual;
  public
    (* Public declarations *)

    (* Read-Only properties *)

    function GlobalCount(): Integer;
    function IsEmpty(): Boolean;
    function BeenIterated(): Boolean;
    function Root(): TUMLCTreeNode; virtual;
  end;

implementation

(* TUMLCTreeNode *)

function TUMLCTreeNode.getData(): pointer;
begin
  Result := FData;
end;

function TUMLCTreeNode.getID(): TGUID;
begin
  Result := FID;
end;

procedure TUMLCTreeNode.setData(const AValue: pointer);
begin
  if (FData <> AValue) then
  begin
    FData := AValue;
  end;
end;

procedure TUMLCTreeNode.setID(const AValue: TGUID);
begin
  if (umlcguids.AreEqualGUID(FID, AValue)) then
  begin
    FID := AValue;
  end;
end;

function TUMLCTreeNode.MatchesNode
  (const ANode: TUMLCTreeNode; const AParam: pointer): Boolean;
begin
  Result := (ANode = Self);
end;

function TUMLCTreeNode.MatchesID
  (const ANode: TUMLCTreeNode; const AParam: pointer): Boolean;
var AID: PGUID;
begin
  AID := PGUID(AParam);
  Result := AreEqualGUID(ANode.ID, AID^);
  // Goal: Returns if the selected node stores the given data.
  // Objetivo: Regresa si el nodo seleccionado almacena el dato dado.
end;

function TUMLCTreeNode.MatchesData
  (const ANode: TUMLCTreeNode; const AParam: pointer): Boolean;
begin
  Result := (ANode.Data = AParam);
  // Goal: Returns if the selected node stores the given data.
  // Objetivo: Regresa si el nodo seleccionado almacena el dato dado.
end;

function TUMLCTreeNode.CreateList(): TUMLCTreeNodeList;
begin
  Result := TUMLCTreeNodeList.Create();
end;

procedure TUMLCTreeNode.ConfirmedCopyTo(var ADestNode: TUMLCTreeNode);
var ThisDestNode: TUMLCTreeNode;
begin
  // perform copy of fields specific to parent class
  Self.DoNothing();

  // cast to current type
  ThisDestNode := TUMLCTreeNode(ADestNode);

  // perform copy of fields specific to this class
  ThisDestNode.Data := Self.Data;
end;

procedure TUMLCTreeNode.ConfirmedMoveTo(var ADestNode: TUMLCTreeNode);
var ThisDestNode: TUMLCTreeNode;
begin
  // perform copy of fields specific to parent class
  Self.DoNothing();

  // cast to current type
  ThisDestNode := TUMLCTreeNode(ADestNode);

  // perform move of fields specific to this class
  ThisDestNode.Data := Self.Data;
  Self.Data      := nil;

  ThisDestNode.FID := Self.FID;
  umlcguids.ClearGUID((* var *) Self.FID);

  ThisDestNode.FInternalGlobalIndex := Self.FInternalGlobalIndex;
  Self.FInternalGlobalIndex      := 0;

  // pendiente
  //FInternalIsRoot:      Boolean;

  // similar libraries use "items" or "children" instead of "list"
  // omitir, se maneja con otra operacion
  // FInternalLevel:       Integer;
  //FInternalCollection:  TUMLCTreeCollection;
  // FInternalList:        TUMLCTreeNodeList;
  // FInternalLocalIndex:  Integer;
  // FInternalParent:      TUMLCTreeNode;

  // Note: "Move" means same treecollection, "export" or "import"
  // means different treecollection.
end;

procedure TUMLCTreeNode.ConfirmedDuplicateTo(var ADestNode: TUMLCTreeNode);
begin
  // store source root-node & destination root-node
  InternalCollection.FClipboardSourceRootNode := Self;
  InternalCollection.FClipboardDestRootNode   := ADestNode;

  // the duplication starts with he already assigned
  // destination parent node
  InternalCollection.FClipboardDestPtrNode :=
    InternalCollection.FClipboardDestRootNode;

  {$ifdef Delphi}
  // does not apply under iteration block !!!
  InternalCollection.FClipboardSourceContainerNode.ConfirmedForBoth
    (DuplicateNode, nil);
  {$endif}

  {$ifdef FPC}
  // does not apply under iteration block !!!
  InternalCollection.FClipboardSourceRootNode.ConfirmedForBoth
    (@DuplicateNode, nil);
  {$endif}
end;

function TUMLCTreeNode.getInternalCollection(): TUMLCTreeCollection;
begin
  Result := FInternalCollection;
end;

function TUMLCTreeNode.getInternalGlobalIndex(): Integer;
begin
  Result := FInternalGlobalIndex;
end;

function TUMLCTreeNode.getInternalLevel(): Integer;
begin
  Result := FInternalLevel;
end;

function TUMLCTreeNode.getInternalList(): TUMLCTreeNodeList;
begin
  Result := FInternalList;
end;

function TUMLCTreeNode.getInternalIsRoot(): Boolean;
begin
  Result := FInternalIsRoot;
end;

function TUMLCTreeNode.getInternalLocalIndex(): Integer;
begin
  Result := FInternalLocalIndex;
end;

function TUMLCTreeNode.getInternalParent(): TUMLCTreeNode;
begin
  Result := FInternalParent;
end;

procedure TUMLCTreeNode.setInternalGlobalIndex(const AValue: Integer);
begin
  if (FInternalGlobalIndex <> AValue) then
  begin
    FInternalGlobalIndex := AValue;
  end;
end;

procedure TUMLCTreeNode.setInternalCollection
  (const AValue: TUMLCTreeCollection);
begin
  if (FInternalCollection <> AValue) then
  begin
    FInternalCollection := AValue;
  end;
end;

procedure TUMLCTreeNode.setInternalIsRoot(const AValue: Boolean);
begin
  if (FInternalIsRoot <> AValue) then
  begin
    FInternalIsRoot := AValue;
  end;
end;

procedure TUMLCTreeNode.setInternalLevel(const AValue: Integer);
begin
  if (FInternalLevel <> AValue) then
  begin
    FInternalLevel := AValue;
  end;
end;

procedure TUMLCTreeNode.setInternalList(const AValue: TUMLCTreeNodeList);
begin
  if (FInternalList <> AValue) then
  begin
    FInternalList := AValue;
  end;
end;

procedure TUMLCTreeNode.setInternalLocalIndex(const AValue: Integer);
begin
  if (FInternalLocalIndex <> AValue) then
  begin
    FInternalLocalIndex := AValue;
  end;
end;

procedure TUMLCTreeNode.setInternalParent(const AValue: TUMLCTreeNode);
begin
  if (FInternalParent <> AValue) then
  begin
    FInternalParent := AValue;
  end;
end;

procedure TUMLCTreeNode.DoCreate();
begin
  inherited DoCreate();

  EmptyGUID( (* var *) FID);

  FInternalGlobalIndex := 0;
  FInternalLocalIndex  := -1;
  FInternalLevel       := 0;

  FData := nil;

  FInternalParent := nil;
  FInternalList   := CreateList();
  FInternalCollection := nil;
end;

procedure TUMLCTreeNode.DoDestroy();
begin
  FData := nil;

  FInternalList.Free();
  FInternalList := nil;
  inherited DoDestroy();
end;

procedure TUMLCTreeNode.ConfirmedInsertRoot();
begin
  Self.InternalIsRoot := TRUE;
  Self.InternalLevel  := 0;
  // assign default properties
  // asignar propiedades default

  Self.InternalParent  := nil;
  // insert into parent node*s item*s list
  // insertar en lista de elementos del nodo padre

  Self.InternalLocalIndex := 0;
  // insert into global treenodes* list
  // insertar en lista de nodosarboles global
end;

procedure TUMLCTreeNode.ConfirmedInsert(var ANode: TUMLCTreeNode);
begin
  ANode.InternalIsRoot := FALSE;
  ANode.InternalLevel  := (InternalLevel + 1);
  // assign default properties
  // asignar propiedades default

  //ShowMessage(IntToStr(AParentNode.List.Count));

  ANode.InternalParent  := Self;
  InternalList.Insert(ANode);
  ANode.InternalLocalIndex := (InternalList.Count - 1);
  // insert into parent node*s item*s list
  // insertar en lista de elementos del nodo padre
end;

procedure TUMLCTreeNode.ConfirmedInsertAt
  (const AIndex: Integer; var ANode: TUMLCTreeNode);
begin
  ANode.InternalIsRoot := FALSE;
  ANode.InternalLevel  := (InternalLevel + 1);
  // assign default properties
  // asignar propiedades default

  ANode.InternalParent  := Self;
  InternalList.InsertAt(AIndex, ANode);
  ANode.InternalLocalIndex := AIndex;
  // insert into parent node*s item*s list
  // insertar en lista de elementos del nodo padre

  // to-do: check index for all nodes
end;

procedure TUMLCTreeNode.ConfirmedRemove();
begin
  if (InternalParent <> nil) then
  begin
    // remove from parent node's subitems list
    InternalParent.InternalList.DeleteAt(Self.InternalLocalIndex);
  end;

  // delete itself from memory
  Self.DoDestroy();
  Self.Free();
end;

procedure TUMLCTreeNode.ConfirmedEmpty();
var EachIndex, LastIndex: Integer;
  EachNode: TUMLCTreeNode;
begin
  // when deleting pointers, doesn't apply iterators here
  // al eliminar apuntadores, no aplican interadores aqui
  LastIndex := (InternalList.Count + 1);
  for EachIndex := 0 to LastIndex do
  begin
    EachNode := TUMLCTreeNode(Self.InternalList.Items[EachIndex]);
    EachNode.Remove();
  end;

  // delete from internal list
  // eliminar de lista interna
  List.Empty();
end;

procedure TUMLCTreeNode.UpdateLocalIndex
  (var ANode: TUMLCTreeNode; const AParam: pointer);
var AIndex: ^Integer absolute AParam;
begin
  ANode.InternalLocalIndex := AIndex^;
  Inc(AIndex^);
end;

procedure TUMLCTreeNode.UpdateLocalIndices();
var AIndex: Integer;
begin
  AIndex := 0;

  {$ifdef Delphi}
  ForEachForwardInmediate(UpdateLocalIndex, @AIndex);
  {$endif}

  {$ifdef FPC}
  ForEachForwardInmediate(@UpdateLocalIndex, @AIndex);
  {$endif}
end;

procedure TUMLCTreeNode.DuplicateNode
  (var ANode: TUMLCTreeNode;
   const AParam: pointer; const ADirection: TUMLCTreeDirection);
begin
  if (ADirection = umlctreenodes.tdStart) then
  begin
    // add a new node, and make dest-ptr reference it
    InternalCollection.FClipboardDestPtrNode :=
      InternalCollection.FClipboardDestPtrNode.Insert();

    // assign fields that can be copied
    ANode.CopyTo(InternalCollection.FClipboardDestPtrNode);
  end;

  if (ADirection = umlctreenodes.tdFinish) then
  begin
    // make dest-ptr reference its own parent node
    InternalCollection.FClipboardDestPtrNode :=
      InternalCollection.FClipboardDestPtrNode.Parent();
  end;
end;

function TUMLCTreeNode.Insert(): TUMLCTreeNode;
begin
  Result := nil;
  if (not InternalCollection.FBeenIterated) then
  begin
    Result := InternalCollection.RequestInsert(Self);
  end;
end;

function TUMLCTreeNode.InsertAt(const AIndex: Integer): TUMLCTreeNode;
begin
  Result := nil;
  if (not InternalCollection.FBeenIterated) then
  begin
    Result := InternalCollection.RequestInsertAt(Self, AIndex);
  end;
end;

function TUMLCTreeNode.InsertFirst(): TUMLCTreeNode;
begin
  Result := Self.InsertAt(0);
end;

function TUMLCTreeNode.InsertAfterFirst(): TUMLCTreeNode;
var ACount: Integer;
begin
  Result := nil;

  ACount := Self.List().Count;
  if (ACount > 1) then
  begin
    Result := Self.InsertAt(1);
  end else
  begin
    Result := Self.Insert();
  end;
end;

function TUMLCTreeNode.InsertBeforeLast(): TUMLCTreeNode;
var ACount, AIndex: Integer;
begin
  Result := nil;

  ACount := Self.List().Count;
  if (ACount > 1) then
  begin
    AIndex := (ACount - 1);
    Result := Self.InsertAt(AIndex);
  end else
  begin
    Result := Self.Insert();
  end;
end;

procedure TUMLCTreeNode.Remove();
begin
  // Notify collection of removal,
  // does not remove from memory here, yet
  if (not InternalCollection.FBeenIterated) then
  begin
    InternalCollection.RequestRemove(Self.Parent(), Self);
  end;
end;

procedure TUMLCTreeNode.Empty();
begin
  // Notify collection of removal,
  // does not remove from memory here, yet
  if (not InternalCollection.FBeenIterated) then
  begin
    InternalCollection.RequestEmpty(Self);
  end;
end;

procedure TUMLCTreeNode.CopyTo(var ADestNode: TUMLCTreeNode);
begin
  if (ADestNode <> nil) then
  begin
    Self.ConfirmedCopyTo((* var *) ADestNode);
  end;
end;

procedure TUMLCTreeNode.MoveTo(var ADestNode: TUMLCTreeNode);
begin
  if (ADestNode <> nil) then
  begin
    Self.ConfirmedMoveTo((* var *) ADestNode);
  end;
end;

function TUMLCTreeNode.CanDuplicateTo(var ADestNode: TUMLCTreeNode): Boolean;
var ANode: TUMLCTreeNode;
begin
  Result := (ADestNode <> nil);
  if (Result) then
  begin
    // verify destnode should NOT be the child of the sourcenode,
    // or the sourcenode, itself

    // matches
    {$ifdef FPC}
    ANode := ADestNode.FirstUp(@MatchesNode, Self);
    Result := (ANode = nil);
    {$endif}
  end;
end;

procedure TUMLCTreeNode.DuplicateTo(var ADestNode: TUMLCTreeNode);
begin
  if (CanDuplicateTo(ADestNode)) then
  begin
    ConfirmedDuplicateTo(ADestNode);
  end;
end;

function TUMLCTreeNode.NodeByID(const ANID: TGUID): TUMLCTreeNode;
begin
  {$ifdef Delphi}
  Result := FirstThat(MatchesID, @ANID);
  {$endif}
  {$ifdef FPC}
  Result := FirstThat(@MatchesID, @ANID);
  {$endif}
  // Goal: Returns the node that stores the given data.
  // Objetivo: Regresa el nodo que almacena el dato indicado.
end;

function TUMLCTreeNode.NodeByData(const AData: pointer): TUMLCTreeNode;
begin
  {$ifdef Delphi}
  Result := FirstThat(MatchesData, Data);
  {$else}
  Result := FirstThat(@MatchesData, AData);
  {$endif}
  // Goal: Returns the node that stores the given data.
  // Objetivo: Regresa el nodo que almacena el dato indicado.
end;

function TUMLCTreeNode.First(): TUMLCTreeNode;
begin
  Result := (List.First() as TUMLCTreeNode);
  // Goal: Returns the first child node.
  // Objetivo: Regresa el primer nodo hijo.
end;

function TUMLCTreeNode.Prev(): TUMLCTreeNode;
begin
  Result := nil;
  if ((InternalParent <> nil) and (InternalGlobalIndex > 0)) then
  begin
    Result := InternalParent.List.Items[InternalGlobalIndex - 1]
  end;
  // Goal: Returns the previous node.
  // Objetivo: Regresa el nodo previo.
end;

function TUMLCTreeNode.Next(): TUMLCTreeNode;
begin
  Result := nil;
  if ((InternalParent <> nil) and (InternalGlobalIndex < InternalParent.Count)) then
  begin
    Result := InternalParent.List.Items[InternalGlobalIndex + 1];
  end;
  // Goal: Returns the next node.
  // Objetivo: Regresa el siguiente nodo.
end;

function TUMLCTreeNode.Last(): TUMLCTreeNode;
begin
  Result := (List.Last() as TUMLCTreeNode);
  // Goal: Returns the last child node.
  // Objetivo: Regresa el ultimo nodo hijo.
end;

function TUMLCTreeNode.ConfirmedFirstThat
 (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
var AIndex, LastIndex: Integer; Item: TUMLCTreeNode; Found: Boolean;
begin
  Result := nil;
  Found  := AFunc(Self, AParam);

  if (Found) then
  begin
    Result := Self;
  end else
  begin
    AIndex := 0; Found := FALSE; LastIndex := (Count() - 1);
    while ((AIndex <= LastIndex) and (not Found)) do
    begin
      Item := List.Items[AIndex];

      Result := Item.ConfirmedFirstThat(AFunc, AParam);
      Found  := (Result <> nil);

      if (not Found) then
      begin
        Inc(AIndex);
      end;
    end;
  end;
end;

function TUMLCTreeNode.ConfirmedLastThat
  (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
var AIndex: Integer; Item: TUMLCTreeNode; Found: Boolean;
begin
  Result := nil;
  Found := AFunc(Self, AParam);
  if (Found) then
  begin
    Result := Self;
  end else
  begin
    AIndex := (Count - 1); Found := FALSE;
    while ((AIndex >= 0) and (not Found)) do
    begin
      Item := List.Items[AIndex];

      Result := Item.ConfirmedLastThat(AFunc, AParam);
      Found  := (Result <> nil);

      if (not Found) then
      begin
        Dec(AIndex);
      end;
    end;
  end;
end;

function TUMLCTreeNode.ConfirmedFirstUp
  (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
begin
  Result := nil;
  if (not AFunc(Self, AParam)) then
  begin
    if (InternalParent <> nil) then
    begin
      Result := InternalParent.ConfirmedFirstUp(AFunc, AParam);
    end;
  end else
  begin
    Result := Self;
  end;
end;

procedure TUMLCTreeNode.ConfirmedForEachForward
  (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
var AIndex, LastIndex: Integer; Item: TUMLCTreeNode;
  D: string;
begin
  AProc(Self, AParam);

  LastIndex := (List.Count - 1);
  if (List.Count > 0) then
  begin
    for AIndex := 0 to LastIndex do
    begin
      Item := List.Items[AIndex];
      Item.ConfirmedForEachForward(AProc, AParam);
    end;
  end;
  // Goal: Execute a procedure in each treenode,
  // starting with the parentnode.

  // Objetivo: Ejecutar un procedimiento en cada nodoarbol,
  // comenzando con el nodopadre.
end;

procedure TUMLCTreeNode.ConfirmedForEachBackward
  (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
var AIndex, LastIndex: Integer; Item: TUMLCTreeNode;
begin
  LastIndex := (List.Count - 1);
  for AIndex := LastIndex downto 0 do
  begin
    Item := List.Items[AIndex];
    Item.ForEachBackward(AProc, AParam);
  end;
  AProc(Self, AParam);
  // Goal: Execute a procedure in each treenode,
  // starting with the children nodes.

  // Objetivo: Ejecutar un procedimiento en cada nodoarbol,
  // comenzando con los nodos hijo.
end;

procedure TUMLCTreeNode.ConfirmedForUp
  (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
begin
  AProc(Self, AParam);
  if (Self.InternalParent <> nil) then
  begin
    Self.InternalParent.ConfirmedForUp(AProc, AParam);
  end;
  // Goal: Execute a procedure in each treenode,
  // starting with the current node and continue with each InternalParent.

  // Objetivo: Ejecutar un procedimiento en cada nodoarbol,
  // comenzando con el nodo actual y continuando con cada nodo padre.
end;

procedure TUMLCTreeNode.ConfirmedForBoth
 (const AProc: TUMLCTreeNodeForBothProc; const AParam: pointer);
var AIndex, LastIndex: Integer; Item: TUMLCTreeNode;
begin
  AProc(Self, AParam, tdStart);

  LastIndex := (List.Count - 1);
  for AIndex := 0 to LastIndex do
  begin
    Item := List.Items[AIndex];
    Item.ConfirmedForBoth(AProc, AParam);
  end;
  AProc(Self, AParam, tdFinish);
  // Goal: Execute a procedure in each treenode,
  // starting with the children nodes.

  // Objetivo: Ejecutar un procedimiento en cada nodoarbol,
  // comenzando con los nodos hijo.
end;

function TUMLCTreeNode.FirstThat
  (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
begin
  Result := nil;
  InternalCollection.FBeenIterated := true;
  Result := ConfirmedFirstThat(AFunc, AParam);
  InternalCollection.FBeenIterated := false;
end;

function TUMLCTreeNode.LastThat
  (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
begin
  Result := nil;
  InternalCollection.FBeenIterated := true;
  Result := ConfirmedLastThat(AFunc, AParam);
  InternalCollection.FBeenIterated := false;
end;

function TUMLCTreeNode.FirstUp
  (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
begin
  Result := nil;
  InternalCollection.FBeenIterated := true;
  Result := ConfirmedFirstUp(AFunc, AParam);
  InternalCollection.FBeenIterated := false;
end;

function TUMLCTreeNode.FirstThatInmediate
  (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
begin
  Result := nil;
  InternalCollection.FBeenIterated := true;
  Result := List.FirstThatInmediate(AFunc, AParam);
  InternalCollection.FBeenIterated := false;
end;

function TUMLCTreeNode.LastThatInmediate
  (const AFunc: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
begin
  Result := nil;
  InternalCollection.FBeenIterated := true;
  Result := List.FirstBackInmediate(AFunc, AParam);
  InternalCollection.FBeenIterated := false;
end;

procedure TUMLCTreeNode.ForEachForward
  (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
begin
  InternalCollection.FBeenIterated := true;
  ConfirmedForEachForward(AProc, AParam);
  InternalCollection.FBeenIterated := false;
  // Goal: Execute a procedure in each treenode,
  // starting with the parentnode.

  // Objetivo: Ejecutar un procedimiento en cada nodoarbol,
  // comenzando con el nodopadre.
end;

procedure TUMLCTreeNode.ForEachBackward
  (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
begin
  InternalCollection.FBeenIterated := true;
  ConfirmedForEachBackward(AProc, AParam);
  InternalCollection.FBeenIterated := false;
  // Goal: Execute a procedure in each treenode,
  // starting with the children nodes.

  // Objetivo: Ejecutar un procedimiento en cada nodoarbol,
  // comenzando con los nodos hijo.
end;

procedure TUMLCTreeNode.ForUp
  (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
begin
  InternalCollection.FBeenIterated := true;
  ConfirmedForUp(AProc, AParam);
  InternalCollection.FBeenIterated := false;
  // Goal: Execute a procedure in each treenode,
  // starting with the current node and continue with each InternalParent.

  // Objetivo: Ejecutar un procedimiento en cada nodoarbol,
  // comenzando con el nodo actual y continuando con cada nodo padre.
end;

procedure TUMLCTreeNode.ForBoth
  (const AProc: TUMLCTreeNodeForBothProc; const AParam: pointer);
begin
  InternalCollection.FBeenIterated := true;
  ConfirmedForBoth(AProc, AParam);
  InternalCollection.FBeenIterated := false;
  // Goal: Execute a procedure in each treenode,
  // starting with the children nodes.

  // Objetivo: Ejecutar un procedimiento en cada nodoarbol,
  // comenzando con los nodos hijo.
end;

procedure TUMLCTreeNode.ForEachForwardInmediate
  (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
begin
  InternalCollection.FBeenIterated := true;
  List.ForEachForwardInmediate(AProc, AParam);
  InternalCollection.FBeenIterated := false;
end;

procedure TUMLCTreeNode.ForEachBackwardInmediate
  (const AProc: TUMLCTreeNodeForEachProc; const AParam: pointer);
begin
  InternalCollection.FBeenIterated := true;
  List.ForEachBackwardInmediate(AProc, AParam);
  InternalCollection.FBeenIterated := false;
end;

function TUMLCTreeNode.Count(): Integer;
begin
  Result := List.Count;
end;

function TUMLCTreeNode.HasItems(): Boolean;
begin
  Result := not (List.IsEmpty());
end;

function TUMLCTreeNode.Collection(): TUMLCTreeCollection;
begin
  Result := FInternalCollection;
end;

function TUMLCTreeNode.GlobalIndex(): Integer;
begin
  Result := FInternalGlobalIndex;
end;

function TUMLCTreeNode.Level(): Integer;
begin
  Result := FInternalLevel;
end;

function TUMLCTreeNode.List(): TUMLCTreeNodeList;
begin
  Result := FInternalList;
end;

function TUMLCTreeNode.LocalIndex(): Integer;
begin
  Result := FInternalLocalIndex;
end;

function TUMLCTreeNode.IsRoot(): Boolean;
begin
  Result := FInternalIsRoot;
end;

function TUMLCTreeNode.Parent(): TUMLCTreeNode;
begin
  Result := FInternalParent;
end;

(* TUMLCTreeNodeList *)

function TUMLCTreeNodeList.getItems(Index: Integer): TUMLCTreeNode;
begin
  Result := TUMLCTreeNode(getInternalItems(Index));
end;

procedure TUMLCTreeNodeList.setItems(Index: Integer; Item: TUMLCTreeNode);
begin
  setInternalItems(Index, Pointer(Item));
end;

function TUMLCTreeNodeList.IndexOf(const Item: TObject): Integer;
begin
  Result := InternalIndexOf(Item);
end;

function TUMLCTreeNodeList.First(): TObject;
begin
  Result := TObject(InternalFirst());
end;

function TUMLCTreeNodeList.Last(): TObject;
begin
  Result := TObject(InternalLast());
end;

function TUMLCTreeNodeList.Insert(const Item: TUMLCTreeNode): Integer;
begin
  Result := InternalInsert(Item);
end;

procedure TUMLCTreeNodeList.InsertAt(const AIndex: Integer; const Item: TUMLCTreeNode);
begin
  InternalInsertAt(AIndex, Pointer(Item));
end;

procedure TUMLCTreeNodeList.Empty();
var AIndex, ALast: Integer;
begin
  if (Count <> 0) then
  begin
    ALast := (Count - 1);
    for AIndex := ALast to 0 do
    begin
      InternalDeleteAt(AIndex);
    end;
  end;
  InternalEmpty();
end;

procedure TUMLCTreeNodeList.DeleteAt(const AIndex: Integer);
begin
  InternalDeleteAt(AIndex);
end;

function TUMLCTreeNodeList.Remove(const Item: TObject): Integer;
begin
  Result := InternalRemove(Pointer(Item));
end;

function TUMLCTreeNodeList.Extract(const AIndex: Integer): TObject;
begin
  Result := TObject(InternalExtract(AIndex));
end;

function TUMLCTreeNodeList.FirstThatInmediate
  (const Func: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
var AIndex, LastIndex: Integer; AItem: TUMLCTreeNode; Found: Boolean;
begin
  Result := nil; AItem:= nil;

  AIndex := 0; Found := FALSE; LastIndex := (Count - 1);
  while ((AIndex <= LastIndex) and (not Found)) do
  begin
    AItem := TUMLCTreeNode(getInternalItems(AIndex));
    Found := Func(AItem, AParam);

    if (not Found) then
    begin
      Inc(AIndex);
    end;
  end;

  if (Found) then
  begin
    Result := AItem;
  end;
end;

function TUMLCTreeNodeList.FirstBackInmediate
  (const Func: TUMLCTreeNodeFunc; const AParam: pointer): TUMLCTreeNode;
var AIndex: Integer; AItem: TUMLCTreeNode; Found: Boolean;
begin
  Result := nil; AItem:= nil;
  AIndex := (Count - 1); Found := FALSE;
  while ((AIndex >= 0) and (not Found)) do
  begin
    AItem := TUMLCTreeNode(getInternalItems(AIndex));
    Found := Func(AItem, AParam);

    if (not Found) then
    begin
      Dec(AIndex);
    end;
  end;

  if (Found) then
  begin
    Result := AItem;
  end;
end;

procedure TUMLCTreeNodeList.ForEachForwardInmediate
  (const Proc: TUMLCTreeNodeForEachProc; const AParam: pointer);
var AIndex, LastIndex: Integer; AItem: TUMLCTreeNode;
begin
  LastIndex := (Count - 1);
  for AIndex := 0 to LastIndex do
  begin
    AItem := TUMLCTreeNode(getInternalItems(AIndex));
    Proc(AItem, AParam);
  end;
end;

procedure TUMLCTreeNodeList.ForEachBackwardInmediate
  (const Proc: TUMLCTreeNodeForEachProc; const AParam: pointer);
var AIndex, LastIndex: Integer; AItem: TUMLCTreeNode;
begin
  LastIndex := (Count - 1);
  for AIndex := LastIndex downto 0 do
  begin
    AItem := TUMLCTreeNode(getInternalItems(AIndex));
    Proc(AItem, AParam);
  end;
end;

(* TUMLCTreeCollection *)

function TUMLCTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCTreeNode.Create();
end;

procedure TUMLCTreeCollection.CreateRoot();
begin
  FInternalRoot := RequestInsert(nil);
  FInternalRoot.InternalCollection := Self;
end;

procedure TUMLCTreeCollection.DestroyRoot();
begin
  if (FInternalRoot <> nil) then
  begin
    FInternalRoot.Remove();
    FInternalRoot := nil;
  end;
end;

function TUMLCTreeCollection.GlobalSequencer(): Integer;
begin
  Result := FGlobalSequencer;
end;

procedure TUMLCTreeCollection.InternalBeforeInsert
  (const AParentNode, ANewNode: TUMLCTreeNode;
   const AIndex: Integer);
begin
  Self.DoNothing();
end;

procedure TUMLCTreeCollection.InternalAfterInsert
   (const AParentNode, ANewNode: TUMLCTreeNode;
    const AIndex: Integer);
begin
  Self.DoNothing();
end;

procedure TUMLCTreeCollection.InternalConfirmedInsert
  (const AParentNode: TUMLCTreeNode; var ANewNode: TUMLCTreeNode);
var BecomeRoot: Boolean;
begin
  // "CreateNodeByClass()" is polymorphic
  ANewNode := Self.CreateNodeByClass();

  // assign relevant properties
  ANewNode.InternalCollection  := Self;
  umlcguids.CreateGUID(ANewNode.FID);

  // perform insertion
  BecomeRoot := (AParentNode = nil);
  if (BecomeRoot) then
  begin
    ANewNode.InternalGlobalIndex := 0;
    ANewNode.ConfirmedInsertRoot();
  end else
  begin
    ANewNode.InternalGlobalIndex := GlobalSequencer();
    AParentNode.ConfirmedInsert(ANewNode);
    AParentNode.UpdateLocalIndices();
  end;

  // some "shared" after operations
  Inc(FGlobalSequencer);
end;

procedure TUMLCTreeCollection.InternalConfirmedInsertAt
  (const AParentNode: TUMLCTreeNode;
   const AIndex: Integer;
   var ANewNode: TUMLCTreeNode);
begin
  // "CreateNodeByClass()" is polymorphic
  ANewNode := Self.CreateNodeByClass();

  // assign relevant properties
  ANewNode.InternalCollection  := Self;
  ANewNode.InternalGlobalIndex := GlobalSequencer();
  umlcguids.CreateGUID(ANewNode.FID);

  // perform insertion
  AParentNode.ConfirmedInsertAt(AIndex, ANewNode);

  // some "shared" after operations
  AParentNode.UpdateLocalIndices();
  Inc(FGlobalSequencer);
end;

procedure TUMLCTreeCollection.InternalBeforeRemove
    (const ANode: TUMLCTreeNode);
begin
  Self.DoNothing();
end;

procedure TUMLCTreeCollection.InternalAfterRemove
  (const ANode: TUMLCTreeNode);
begin
  Self.DoNothing();
end;

procedure TUMLCTreeCollection.InternalConfirmedRemove
  (const AParentNode: TUMLCTreeNode;
   const ANode: TUMLCTreeNode);
begin
  if (ANode = FInternalRoot) then
  begin
    FInternalRoot := nil;
  end;

  // now, apply the removal
  ANode.ConfirmedRemove();

  // some "shared" after operations
  if (AParentNode <> nil) then
  begin
    AParentNode.UpdateLocalIndices();
  end;
end;

procedure TUMLCTreeCollection.InternalBeforeEmpty
  (const ANode: TUMLCTreeNode);
begin
  Self.DoNothing();
end;

procedure TUMLCTreeCollection.InternalAfterEmpty
  (const ANode: TUMLCTreeNode);
begin
  Self.DoNothing();
end;

procedure TUMLCTreeCollection.InternalConfirmedEmpty
  (const ANode: TUMLCTreeNode);
begin
  // now, apply the empty action
  ANode.ConfirmedEmpty();
end;

function TUMLCTreeCollection.RequestInsert
  (const AParentNode: TUMLCTreeNode): TUMLCTreeNode;
begin
  Result := nil;

  InternalBeforeInsert(AParentNode, Result, umlclists.IgnoreIndex);
  InternalConfirmedInsert(AParentNode, (* var *) Result);
  InternalAfterInsert(AParentNode, Result, umlclists.IgnoreIndex);
end;

function TUMLCTreeCollection.RequestInsertAt
  (const AParentNode: TUMLCTreeNode;
   const AIndex: Integer): TUMLCTreeNode;
begin
  Result := nil;

  InternalBeforeInsert(AParentNode, Result, AIndex);
  InternalConfirmedInsertAt(AParentNode, AIndex, (* var *) Result);
  InternalAfterInsert(AParentNode, Result, AIndex);
end;

procedure TUMLCTreeCollection.DoCreate();
begin
  inherited DoCreate();

  // root will be assigned, later
  FInternalRoot := nil;
  FBeenIterated := false;

  FGlobalCount     := 0;
  FGlobalSequencer := 0;
  // add reference to treenodes
end;

procedure TUMLCTreeCollection.DoDestroy();
begin
  // maybe, there is a root
  DestroyRoot();

  inherited DoDestroy();
end;

procedure TUMLCTreeCollection.RequestRemove
  (const AParentNode: TUMLCTreeNode;
   const ANode: TUMLCTreeNode);
begin
  InternalBeforeRemove(ANode);
  InternalConfirmedRemove(AParentNode, ANode);
  InternalAfterRemove(ANode);
  // Goal: Removes the given item from the collection.
  // Objetivo: Remueve el elemento dado de la coleccion.
end;

procedure TUMLCTreeCollection.RequestRemoveAt
  (const AParentNode: TUMLCTreeNode; const AIndex: Integer);
begin
  (*
  InternalBeforeRemoveAt(Result, AIndex);
  InternalConfirmedRemoveAt(AParentNode, AIndex, .* var *. Result);
  InternalAfterRemoveAt(AParentNode, Result, AIndex);
  *)
end;

procedure TUMLCTreeCollection.RequestEmpty
  (const ANode: TUMLCTreeNode);
begin
  InternalBeforeEmpty(ANode);
  InternalConfirmedEmpty(ANode);
  InternalAfterEmpty(ANode);
  // Goal: Removes all the subitems from the given item,
  // without removing the item itself.

  // Objetivo: Remueve todos los subelementos del elemento indicado,
  // sin remover el elemento en si mismo.
end;

function TUMLCTreeCollection.GlobalCount(): Integer;
begin
  Result := FGlobalCount;
end;

function TUMLCTreeCollection.InsertRoot(): TUMLCTreeNode;
begin
  Result := nil;
  if (not FBeenIterated) then
  begin
    // check previous existing root node
    Self.DestroyRoot();

    Self.CreateRoot();
    Result := Self.InternalRoot;
  end;
  // Goal: Inserts the root item to the collection.
  // Objetivo: Inserta el elemento raiz en la coleccion.
end;

procedure TUMLCTreeCollection.DropRoot();
begin
  if (not FBeenIterated) then
  begin
    // check previous existing root node
    Self.DestroyRoot();
  end;
end;

function TUMLCTreeCollection.IsEmpty(): Boolean;
begin
  Result := (FInternalRoot = nil);
end;

function TUMLCTreeCollection.BeenIterated(): Boolean;
begin
  Result := FBeenIterated;
end;

function TUMLCTreeCollection.Root(): TUMLCTreeNode;
begin
  Result := FInternalRoot;
end;

end.
