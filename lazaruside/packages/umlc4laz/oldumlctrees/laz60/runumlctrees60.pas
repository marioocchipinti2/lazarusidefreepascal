{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit runumlctrees60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcansimemotreenodes, umlcansinullstrtreenodes, umlcdelegtreecntrs, 
  umlcidtreenodes, umlckeytypevaluedocs, umlckeytypevaluelistmsgtreecntrs, 
  umlckeytypevaluelisttreecntrs, umlckeytypevaluelisttreenodes, 
  umlckeyvaluelisttreenodes, umlcmsgtreecntrs, umlcshortstrtreenodes, 
  umlcstringkeytypevaluetreenodes, umlcstringkeyvaluetreenodes, 
  umlctextreenodes, umlctreecntrs, umlctreeiterators, umlctreenodes;

implementation

end.
