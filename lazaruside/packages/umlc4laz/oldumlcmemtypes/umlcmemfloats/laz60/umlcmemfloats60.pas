{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcmemfloats60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcbinfloat_16s, umlcbinfloat_32s, umlcbinfloat_64s, umlcbinfloat_128s, 
  umlcbinfloat_256s, umlcbinfloattypes, umlcdecfloat_32s, umlcdecfloat_64s, 
  umlcdecfloat_128s, umlcdecfloattypes, umlcmemfloats_rtti;

implementation

end.
