(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdecfloattypes;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement numerical floating point / real types,
 ** based on I.E.E.E. standards.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  dummy;

// ---

const

 MOD_umlcdecfloattypes : TUMLCModule =
   ($81,$FC,$CB,$44,$08,$51,$65,$45,$BF,$4B,$85,$11,$7A,$1D,$4C,$9D);

 // ---

const

 // ---

 ID_umlcdecfloat_32 : TUMLCType =
   ($28,$8F,$91,$04,$6E,$9F,$8E,$45,$83,$87,$A0,$24,$5D,$F0,$12,$CD);

 ID_umlcdecfloat_64 : TUMLCType =
   ($EC,$B3,$24,$A2,$86,$8D,$02,$4C,$A7,$B6,$00,$BA,$BD,$CC,$3B,$1E);

 ID_umlcdecfloat_128 : TUMLCType =
   ($04,$BA,$B8,$B4,$EE,$EE,$BB,$46,$9C,$60,$42,$E5,$37,$D6,$9E,$E5);

 // ---

(* global standard types *)

type

  umlcdecfloat_32  = (* redefine *) type umlcmemint_32;
  umlcdecfloat_64  = (* redefine *) type umlcmemint_64;
  umlcdecfloat_128 = (* redefine *) type umlcmemint_128;

type

  umlctdecfloat_32  = (* redefine *) type umlcdecfloat_32;
  umlctdecfloat_64  = (* redefine *) type umlcdecfloat_64;
  umlctdecfloat_128 = (* redefine *) type umlcdecfloat_128;

type

  umlcpdecfloat_32  = ^umlcdecfloat_32;
  umlcpdecfloat_64  = ^umlcdecfloat_64;
  umlcpdecfloat_128 = ^umlcdecfloat_128;

(* global additional types *)

type

  TumlcdecfloatTypes = (* enum of *)
  (
    umlcdecfloattyp_Unknown,
    umlcdecfloattyp_32,
    umlcdecfloattyp_64,
    umlcdecfloattyp_128
  );

(* global functions *)

function SizeInBytesOf
  (const ADecFloatType: TumlcdecfloatTypes): Cardinal;

implementation

(* global functions *)

function SizeInBytesOf
  (const ADecFloatType: TumlcdecfloatTypes): Cardinal;
begin
  //umlcdecfloattyp_Unknown:
  Result := 0;

  case ADecFloatType of
    umlcdecfloattyp_32:
      Result := sizeof(umlcdecfloat_32);
    umlcdecfloattyp_64:
      Result := sizeof(umlcdecfloat_64);
    umlcdecfloattyp_128:
      Result := sizeof(umlcdecfloat_128);
  end;
end;



end.

