(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdecfloat_128s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations for I.E.E.E. floating point numerical types.
 ** Note: 128 Bits.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcdecfloattypes,
  umlcmem128s,
  umlcmem128byptrs,
  dummy;

// ---

const

  MOD_umlcdecfloat_128s : TUMLCModule =
    ($AE,$21,$37,$C1,$74,$3A,$A4,$41,$BC,$23,$29,$F9,$F4,$3E,$74,$67);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcdecfloat_128): Boolean; overload;

(* global standard procedures *)

  procedure Clear
    (out ADest: umlcdecfloat_128); overload;


(* global operators *)

  procedure Assign
    (out   ADest:   umlcdecfloat_128;
     const ASource: umlcdecfloat_128); overload; // operator :=

  function Compare
    (const A, B: umlcdecfloat_128): umlctcomparison;

  function Different
    (const A, B: umlcdecfloat_128): Boolean; overload; // operator <>
  function Equal
    (const A, B: umlcdecfloat_128): Boolean; overload; // operator =

  function Greater
    (const A, B: umlcdecfloat_128): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcdecfloat_128): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcdecfloat_128): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcdecfloat_128): Boolean; overload; // operator <=


implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcdecfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.IsEmpty(umlcpmemint_128(@ASource));
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcdecfloat_128);
begin
  System.FillByte(ADest, sizeof(umlcdecfloat_128), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcdecfloat_128;
   const ASource: umlcdecfloat_128);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcdecfloat_128): umlccomparison;
begin
  Result :=
    umlcmem128byptrs.Compare
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
end;

function Different
  (const A, B: umlcdecfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.Different
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Equal
  (const A, B: umlcdecfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.Equal
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Greater
  (const A, B: umlcdecfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.Greater
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcdecfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.Lesser
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcdecfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.GreaterEqual
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcdecfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.LesserEqual
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;



end.

