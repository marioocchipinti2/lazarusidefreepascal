(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdecfloat_64s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations for I.E.E.E. floating point numerical types.
 ** Note: 64 Bits.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcdecfloattypes,
  umlcmem64s,
  umlcmem64byptrs,
  dummy;

// ---

const

  MOD_umlcdecfloat_64s : TUMLCModule =
    ($6F,$A1,$8A,$07,$DE,$E8,$DF,$45,$AE,$49,$B2,$9A,$B9,$76,$DF,$0F);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcdecfloat_64): Boolean; overload;

(* global standard procedures *)

  procedure Clear
    (out ADest: umlcdecfloat_64); overload;


(* global operators *)

  procedure Assign
    (out   ADest:   umlcdecfloat_64;
     const ASource: umlcdecfloat_64); overload; // operator :=

  function Compare
    (const A, B: umlcdecfloat_64): umlctcomparison;

  function Different
    (const A, B: umlcdecfloat_64): Boolean; overload; // operator <>
  function Equal
    (const A, B: umlcdecfloat_64): Boolean; overload; // operator =

  function Greater
    (const A, B: umlcdecfloat_64): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcdecfloat_64): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcdecfloat_64): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcdecfloat_64): Boolean; overload; // operator <=


implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcdecfloat_64): Boolean;
begin
  Result :=
    umlcmem64byptrs.IsEmpty(umlcpmemint_64(@ASource));
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcdecfloat_64);
begin
  System.FillByte(ADest, sizeof(umlcdecfloat_64), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcdecfloat_64;
   const ASource: umlcdecfloat_64);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcdecfloat_64): umlccomparison;
begin
  Result :=
    umlcmem64byptrs.Compare
      (umlcpmemint_64(@A), umlcpmemint_64(@B));
end;

function Different
  (const A, B: umlcdecfloat_64): Boolean;
begin
  Result :=
    umlcmem64byptrs.Different
      (umlcpmemint_64(@A), umlcpmemint_64(@B));
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Equal
  (const A, B: umlcdecfloat_64): Boolean;
begin
  Result :=
    umlcmem64byptrs.Equal
      (umlcpmemint_64(@A), umlcpmemint_64(@B));
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Greater
  (const A, B: umlcdecfloat_64): Boolean;
begin
  Result :=
    umlcmem64byptrs.Greater
      (umlcpmemint_64(@A), umlcpmemint_64(@B));
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcdecfloat_64): Boolean;
begin
  Result :=
    umlcmem64byptrs.Lesser
      (umlcpmemint_64(@A), umlcpmemint_64(@B));
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcdecfloat_64): Boolean;
begin
  Result :=
    umlcmem64byptrs.GreaterEqual
      (umlcpmemint_64(@A), umlcpmemint_64(@B));
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcdecfloat_64): Boolean;
begin
  Result :=
    umlcmem64byptrs.LesserEqual
      (umlcpmemint_64(@A), umlcpmemint_64(@B));
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;




end.

