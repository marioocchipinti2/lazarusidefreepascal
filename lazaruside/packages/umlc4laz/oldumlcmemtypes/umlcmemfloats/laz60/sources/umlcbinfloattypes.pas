(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbinfloattypes;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement numerical floating point / real types,
 ** based on I.E.E.E. standards.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  dummy;

// ---

const

 MOD_umlcbinfloattypes : TUMLCModule =
   ($C3,$97,$F6,$AE,$0A,$D4,$D6,$40,$96,$C7,$18,$C7,$79,$84,$02,$90);

 // ---

const

 // ---

 ID_umlcbinfloat_16 : TUMLCType =
   ($B6,$B3,$33,$E8,$90,$E3,$46,$43,$89,$20,$59,$7F,$C5,$6C,$35,$FC);

 ID_umlcbinfloat_32 : TUMLCType =
   ($82,$32,$E9,$78,$9A,$8E,$94,$44,$9F,$45,$8A,$03,$DF,$99,$F6,$9E);

 ID_umlcbinfloat_64 : TUMLCType =
   ($70,$A3,$9B,$B9,$79,$93,$9F,$4B,$92,$05,$85,$36,$B6,$11,$91,$65);

 ID_umlcbinfloat_128 : TUMLCType =
   ($79,$34,$5A,$9D,$C9,$EE,$56,$40,$A7,$2D,$9D,$64,$58,$F8,$A7,$7E);

 ID_umlcbinfloat_256 : TUMLCType =
   ($7D,$23,$74,$B7,$BF,$62,$97,$48,$87,$40,$03,$BB,$2D,$16,$12,$BB);

 // ---

 ID_umlchalffloat : TUMLCType =
   ($B6,$B3,$33,$E8,$90,$E3,$46,$43,$89,$20,$59,$7F,$C5,$6C,$35,$FC);

 ID_umlcsinglefloat : TUMLCType =
   ($82,$32,$E9,$78,$9A,$8E,$94,$44,$9F,$45,$8A,$03,$DF,$99,$F6,$9E);

 ID_umlcdoublefloat : TUMLCType =
   ($70,$A3,$9B,$B9,$79,$93,$9F,$4B,$92,$05,$85,$36,$B6,$11,$91,$65);

 ID_umlcquadfloat : TUMLCType =
   ($79,$34,$5A,$9D,$C9,$EE,$56,$40,$A7,$2D,$9D,$64,$58,$F8,$A7,$7E);

 ID_umlcoctafloat : TUMLCType =
   ($7D,$23,$74,$B7,$BF,$62,$97,$48,$87,$40,$03,$BB,$2D,$16,$12,$BB);


 // ---

(* global standard types *)

type

  umlcbinfloat_16  = (* redefine *) type umlcmemint_16;
  umlcbinfloat_32  = (* redefine *) type umlcmemint_32;
  umlcbinfloat_64  = (* redefine *) type umlcmemint_64;
  umlcbinfloat_128 = (* redefine *) type umlcmemint_128;
  umlcbinfloat_256 = (* redefine *) type umlcmemint_256;

type

  umlctbinfloat_16  = (* alias of *) umlcbinfloat_16;
  umlctbinfloat_32  = (* alias of *) umlcbinfloat_32;
  umlctbinfloat_64  = (* alias of *) umlcbinfloat_64;
  umlctbinfloat_128 = (* alias of *) umlcbinfloat_128;
  umlctbinfloat_256 = (* alias of *) umlcbinfloat_256;

type

  umlcpbinfloat_16  = ^umlcbinfloat_16;
  umlcpbinfloat_32  = ^umlcbinfloat_32;
  umlcpbinfloat_64  = ^umlcbinfloat_64;
  umlcpbinfloat_128 = ^umlcbinfloat_128;
  umlcpbinfloat_256 = ^umlcbinfloat_256;

type

  umlchalffloat   = (* alias of *) umlcbinfloat_16;
  umlcsinglefloat = (* alias of *) umlcbinfloat_32;
  umlcdoublefloat = (* alias of *) umlcbinfloat_64;
  umlcquadfloat   = (* alias of *) umlcbinfloat_128;
  umlcoctafloat   = (* alias of *) umlcbinfloat_256;

type

  umlcthalffloat   = (* alias of *) umlchalffloat;
  umlctsinglefloat = (* alias of *) umlcsinglefloat;
  umlctdoublefloat = (* alias of *) umlcdoublefloat;
  umlctquadfloat   = (* alias of *) umlcquadfloat;
  umlctoctafloat   = (* alias of *) umlcoctafloat;

type

  umlcphalffloat   = ^umlchalffloat;
  umlcpsinglefloat = ^umlcsinglefloat;
  umlcpdoublefloat = ^umlcdoublefloat;
  umlcpquadfloat   = ^umlcquadfloat;
  umlcpoctafloat   = ^umlcoctafloat;

  (* global additional types *)

type

  TumlcbinfloatTypes = (* enum of *)
  (
    umlcbinfloattyp_Unknown,
    umlcbinfloattyp_16,
    umlcbinfloattyp_32,
    umlcbinfloattyp_64,
    umlcbinfloattyp_128,
    umlcbinfloattyp_256
  );

(* global functions *)

function SizeInBytesOf
  (const ABinFloatType: TumlcbinfloatTypes): Cardinal;

implementation

(* global functions *)

function SizeInBytesOf
  (const ABinFloatType: TumlcbinfloatTypes): Cardinal;
begin
  //umlcbinfloattyp_Unknown:
  Result := 0;

  case ABinFloatType of
    umlcbinfloattyp_16:
      Result := sizeof(umlcbinfloat_16);
    umlcbinfloattyp_32:
      Result := sizeof(umlcbinfloat_32);
    umlcbinfloattyp_64:
      Result := sizeof(umlcbinfloat_64);
    umlcbinfloattyp_128:
      Result := sizeof(umlcbinfloat_128);
    umlcbinfloattyp_256:
      Result := sizeof(umlcbinfloat_256);
  end;
end;




end.

