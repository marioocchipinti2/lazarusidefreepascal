(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbinfloat_16s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations for I.E.E.E. floating point numerical types.
 ** Note: 16 Bits.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcbinfloattypes,
  umlcmem16s,
  dummy;

// ---

const

  MOD_umlcbinfloat_16s : TUMLCModule =
    ($28,$1F,$EB,$5F,$56,$EA,$6D,$4F,$AD,$F5,$BB,$EF,$92,$3F,$57,$8E);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcbinfloat_16): Boolean; overload;

(* global standard procedures *)

  procedure Clear
    (out ADest: umlcbinfloat_16); overload;

(* global operators *)

  procedure Assign
    (out   ADest:   umlcbinfloat_16;
     const ASource: umlcbinfloat_16); overload; // operator :=

  function Compare
    (const A, B: umlcbinfloat_16): umlctcomparison;

  function Equal
    (const A, B: umlcbinfloat_16): Boolean; overload; // operator =
  function Different
    (const A, B: umlcbinfloat_16): Boolean; overload; // operator <>

  function Greater
    (const A, B: umlcbinfloat_16): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcbinfloat_16): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcbinfloat_16): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcbinfloat_16): Boolean; overload; // operator <=

implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcbinfloat_16): Boolean;
begin
  Result :=
    umlcmem16s.IsEmpty(ASource);
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcbinfloat_16);
begin
  System.FillByte(ADest, sizeof(umlcbinfloat_16), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;




(* global operators *)

procedure Assign
  (out   ADest:   umlcbinfloat_16;
   const ASource: umlcbinfloat_16);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcbinfloat_16): umlccomparison;
begin
  if (A = B)
    then Result := cmpEqual
  else if (A < B)
    then Result := cmpLower
  else Result := cmpHigher;
end;

function Different
  (const A, B: umlcbinfloat_16): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Equal
  (const A, B: umlcbinfloat_16): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Greater
  (const A, B: umlcbinfloat_16): Boolean;
begin
  Result :=
    (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcbinfloat_16): Boolean;
begin
  Result :=
    (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcbinfloat_16): Boolean;
begin
  Result :=
    (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcbinfloat_16): Boolean;
begin
  Result :=
    (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;





end.

