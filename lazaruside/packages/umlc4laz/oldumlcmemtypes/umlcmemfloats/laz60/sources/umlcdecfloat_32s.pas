(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdecfloat_32s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations for I.E.E.E. floating point numerical types.
 ** Note: 32 Bits.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcdecfloattypes,
  umlcmem32s,
  umlcmem32byptrs,
  dummy;

// ---

const

  MOD_umlcdecfloat_32s : TUMLCModule =
    ($C8,$35,$A0,$6B,$5B,$5E,$C0,$4E,$98,$3C,$3C,$17,$20,$C5,$5B,$56);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcdecfloat_32): Boolean; overload;

(* global standard procedures *)

  procedure Clear
    (out ADest: umlcdecfloat_32); overload;


(* global operators *)

  procedure Assign
    (out   ADest:   umlcdecfloat_32;
     const ASource: umlcdecfloat_32); overload; // operator :=

  function Compare
    (const A, B: umlcdecfloat_32): umlctcomparison;

  function Different
    (const A, B: umlcdecfloat_32): Boolean; overload; // operator <>
  function Equal
    (const A, B: umlcdecfloat_32): Boolean; overload; // operator =

  function Greater
    (const A, B: umlcdecfloat_32): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcdecfloat_32): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcdecfloat_32): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcdecfloat_32): Boolean; overload; // operator <=


implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcdecfloat_32): Boolean;
begin
  Result :=
    umlcmem32byptrs.IsEmpty(umlcpmemint_32(@ASource));
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcdecfloat_32);
begin
  System.FillByte(ADest, sizeof(umlcdecfloat_32), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcdecfloat_32;
   const ASource: umlcdecfloat_32);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcdecfloat_32): umlccomparison;
begin
  Result :=
    umlcmem32byptrs.Compare
      (umlcpmemint_32(@A), umlcpmemint_32(@B));
end;

function Different
  (const A, B: umlcdecfloat_32): Boolean;
begin
  Result :=
    umlcmem32byptrs.Different
      (umlcpmemint_32(@A), umlcpmemint_32(@B));
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Equal
  (const A, B: umlcdecfloat_32): Boolean;
begin
  Result :=
    umlcmem32byptrs.Equal
      (umlcpmemint_32(@A), umlcpmemint_32(@B));
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Greater
  (const A, B: umlcdecfloat_32): Boolean;
begin
  Result :=
    umlcmem32byptrs.Greater
      (umlcpmemint_32(@A), umlcpmemint_32(@B));
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcdecfloat_32): Boolean;
begin
  Result :=
    umlcmem32byptrs.Lesser
      (umlcpmemint_32(@A), umlcpmemint_32(@B));
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcdecfloat_32): Boolean;
begin
  Result :=
    umlcmem32byptrs.GreaterEqual
      (umlcpmemint_32(@A), umlcpmemint_32(@B));
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcdecfloat_32): Boolean;
begin
  Result :=
    umlcmem32byptrs.LesserEqual
      (umlcpmemint_32(@A), umlcpmemint_32(@B));
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;




end.

