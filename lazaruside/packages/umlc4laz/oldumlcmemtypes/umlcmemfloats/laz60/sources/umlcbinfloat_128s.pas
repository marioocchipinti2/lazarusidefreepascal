(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbinfloat_128s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations for I.E.E.E. floating point numerical types.
 ** Note: 128 Bits.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcbinfloattypes,
  umlcmem128s, umlcmem128byptrs,
  dummy;

// ---

const

  MOD_umlcbinfloat_128s : TUMLCModule =
    ($09,$DB,$7D,$6B,$ED,$D2,$0A,$46,$AC,$BC,$C9,$68,$CF,$BB,$AF,$D6);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcbinfloat_128): Boolean; overload;

(* global standard procedures *)

  procedure Clear
    (out ADest: umlcbinfloat_128); overload;


(* global operators *)

  procedure Assign
    (out   ADest:   umlcbinfloat_128;
     const ASource: umlcbinfloat_128); overload; // operator :=

  function Compare
    (const A, B: umlcbinfloat_128): umlctcomparison;

  function Equal
    (const A, B: umlcbinfloat_128): Boolean; overload; // operator =
  function Different
    (const A, B: umlcbinfloat_128): Boolean; overload; // operator <>

  function Greater
    (const A, B: umlcbinfloat_128): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcbinfloat_128): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcbinfloat_128): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcbinfloat_128): Boolean; overload; // operator <=


implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcbinfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.IsEmpty(umlcpmemint_128(@ASource));
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcbinfloat_128);
begin
  System.FillByte(ADest, sizeof(umlcbinfloat_128), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;





(* global operators *)

procedure Assign
  (out   ADest:   umlcbinfloat_128;
   const ASource: umlcbinfloat_128);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcbinfloat_128): umlccomparison;
begin
  Result :=
    umlcmem128byptrs.Compare
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
end;

function Different
  (const A, B: umlcbinfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.Different
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Equal
  (const A, B: umlcbinfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.Equal
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Greater
  (const A, B: umlcbinfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.Greater
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcbinfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.Lesser
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcbinfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.GreaterEqual
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcbinfloat_128): Boolean;
begin
  Result :=
    umlcmem128byptrs.LesserEqual
      (umlcpmemint_128(@A), umlcpmemint_128(@B));
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;


end.

