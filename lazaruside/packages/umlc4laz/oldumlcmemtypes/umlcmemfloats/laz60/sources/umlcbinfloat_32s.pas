(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbinfloat_32s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations for I.E.E.E. floating point numerical types.
 ** Note: 32 Bits.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcbinfloattypes,
  umlcmem32s,
  dummy;

// ---

const

  MOD_umlcbinfloat_32s : TUMLCModule =
    ($CF,$C4,$3E,$56,$AD,$68,$8A,$44,$9A,$85,$03,$01,$67,$A7,$79,$78);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcbinfloat_32): Boolean; overload;

(* global standard procedures *)

  procedure Clear
    (out ADest: umlcbinfloat_32); overload;

(* global operators *)

  procedure Assign
    (out   ADest:   umlcbinfloat_32;
     const ASource: umlcbinfloat_32); overload; // operator :=

  function Compare
    (const A, B: umlcbinfloat_32): umlctcomparison;

  function Equal
    (const A, B: umlcbinfloat_32): Boolean; overload; // operator =
  function Different
    (const A, B: umlcbinfloat_32): Boolean; overload; // operator <>

  function Greater
    (const A, B: umlcbinfloat_32): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcbinfloat_32): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcbinfloat_32): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcbinfloat_32): Boolean; overload; // operator <=


implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcbinfloat_32): Boolean;
begin
  Result :=
    umlcmem32s.IsEmpty(ASource);
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcbinfloat_32);
begin
  System.FillByte(ADest, sizeof(umlcbinfloat_32), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;




(* global operators *)

procedure Assign
  (out   ADest:   umlcbinfloat_32;
   const ASource: umlcbinfloat_32);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcbinfloat_32): umlccomparison;
begin
  if (A = B)
    then Result := cmpEqual
  else if (A < B)
    then Result := cmpLower
  else Result := cmpHigher;
end;

function Different
  (const A, B: umlcbinfloat_32): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Equal
  (const A, B: umlcbinfloat_32): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Greater
  (const A, B: umlcbinfloat_32): Boolean;
begin
  Result :=
    (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcbinfloat_32): Boolean;
begin
  Result :=
    (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcbinfloat_32): Boolean;
begin
  Result :=
    (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcbinfloat_32): Boolean;
begin
  Result :=
    (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

end.

