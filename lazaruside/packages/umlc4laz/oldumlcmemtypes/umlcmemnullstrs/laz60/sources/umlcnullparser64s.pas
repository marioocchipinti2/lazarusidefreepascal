unit umlcnullparser64s;

{$mode objfpc}{$H+}

interface

uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemchartypes, umlcmemnullstrtypes,
  umlcchar64s,
  umlcnullstr64s,
  umlccharset64s,
  umlccharsetconst64s,
  dummy;

// ---

const
  MOD_umlcansinullstrs : TUMLCModule =
    ($FD,$20,$32,$C8,$10,$DC,$D1,$47,$95,$07,$D1,$C8,$8B,$91,$D1,$4F);

// ---


(* global functions *)

  function MatchesCharSet
    (const AHaystack: umlcnullstring_64; const ValidChars: umlccharset_64): umlcnullstring_64; overload;

  function IsStringInSet
    (const AHaystack: umlcnullstring_64; const ValidChars: umlccharset_64): Boolean; overload;

  function IsWildcard
     (const ASource: umlcnullstring_64): Boolean;

implementation

(* global functions *)

function MatchesCharSet
  (const AHaystack: umlcnullstring_64; const ValidChars: umlccharset_64): umlcnullstring_64;
var CanContinue: Boolean;
    ASource: umlcnullstring_64;
begin
  Result := nil;

  if (System.Assigned(AHaystack)) then
  begin
    ASource :=
      AHaystack;

    CanContinue := TRUE;
    while (not umlcnullstr64s.IsEmpty(ASource) and CanContinue) do
    begin
      CanContinue :=
        umlccharset64s.IsMember(ValidChars, ASource^);
      Inc(ASource);
    end;

    if (not CanContinue) then
    begin
      Dec(ASource);
    end;

    Result := ASource;
  end;
  // Goal: Moves a null termnated string pointer,
  // as long as each character, matches the valid set.

  // Objetivo: Mueve un apuntador cadena terminada en nulo,
  // mientras cada caracter, coincide con el conjunto valido.
end;

function IsStringInSet
  (const AHaystack: umlcnullstring_64; const ValidChars: umlccharset_64): Boolean;
begin
  Result :=
    (umlcnullparser64s.MatchesCharSet(AHaystack, ValidChars) <> AHaystack);
  // Goal: Returns if all the characters, a string are valid.
  // Objetivo: Regresa si todos los caracteres en una cadena son validos.
end;

function IsWildcard
   (const ASource: umlcnullstring_64): Boolean;
var P: umlcnullstring_64;
begin
  Result :=
    umlcnullparser64s.IsStringInSet(ASource, WildcardSet);
  // Goal: Returns if a string is a wildcard.
  // Objetivo: Regresa si una cadena es un comodin.
end;


end.

