(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemnullstrtypes;

{$mode objfpc}{$H+}

interface

uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemchartypes,
  dummy;

// ---

const
  MOD_umlcmemnullstrtypes : TUMLCModule =
    ($4A,$A2,$5A,$19,$8B,$DA,$D1,$4F,$84,$36,$BC,$07,$49,$82,$6B,$11);

// ---

const

  ID_umlcnullstring_8 : TUMLCType =
  ($69,$87,$18,$1E,$D9,$A4,$85,$48,$BB,$CC,$EB,$18,$F2,$DF,$B3,$2B);

  ID_umlcnullstring_16 : TUMLCType =
  ($40,$6C,$B7,$5F,$8F,$C2,$E5,$40,$BC,$4D,$7C,$71,$01,$74,$73,$B4);

  ID_umlcnullstring_32 : TUMLCType =
  ($D3,$9E,$E7,$E6,$87,$B0,$96,$4A,$97,$8F,$0F,$E0,$D3,$82,$E0,$54);

  ID_umlcnullstring_64 : TUMLCType =
  ($2B,$85,$62,$99,$06,$E6,$B6,$40,$A3,$50,$78,$75,$6B,$2F,$D9,$FC);

// ---

type

  // (Plain / Pure) "C" style integer based neutral encoding strings

  umlcnullstring_8   = (* alias of *) ^umlcchar_8;
  umlcnullstring_16  = (* alias of *) ^umlcchar_16;
  umlcnullstring_32  = (* alias of *) ^umlcchar_32;
  umlcnullstring_64  = (* alias of *) ^umlcchar_64;

implementation





end.

