(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnullstr64s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement null character marked,
 ** fixed length 64 bits character based text.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  umlcmemchartypes, umlcmemnullstrtypes,
  umlcchar64s,
  dummy;


// ---

const

 MOD_umlcansinullstr64s : TUMLCModule =
   ($03,$5D,$48,$08,$46,$04,$2E,$4D,$9C,$23,$36,$42,$6C,$DD,$D3,$60);

// ---

(* global standard functions *)

 function IsEmpty
  (const ASource: umlcnullstring_64): Boolean; overload;

 function IsStringOfChar
  (const S: umlcnullstring_64; C: umlcchar_64): Boolean; overload;

 function Length
  (const ASource: umlcnullstring_64): Cardinal; overload;
 function LengthSize
  (const ASource: umlcnullstring_64; const AMaxSize: Cardinal): Cardinal; overload;

(* global operators *)



(* global procedures *)



implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcnullstring_64): Boolean;
begin
  Result :=
    (ASource <> nil) and
    (ASource^ = nullchar_64);
  // Goal: Returns if a string is empty.
  // Objetivo: Regresa si una cadena esta vacia.
end;

function IsStringOfChar
  (const S: umlcnullstring_64; C: umlcchar_64): Boolean;
var I, L: Cardinal; Match: Boolean;
begin
  L :=
    umlcnullstr64s.Length(S);

  Result := (L > 0);
  if (Result) then
  begin
    I := 1; Match := TRUE;
    while ((I <= L) and (Match)) do
    begin
      Match := (S[i] = C);
      Inc(I);
    end;

    Result := Match;
  end;
  // Objetivo: Regresa si una cadena esta compuesta solo del mismo caracter.
  // Goal: Returns if a string is composed with the same character.
end;

function Length
   (const ASource: umlcnullstring_64): Cardinal;
begin
  Result :=
    umlcnullstr64s.LengthSize(ASource, sizeof(Cardinal));
  // Objetivo: Regresa la cantidad de caracteres de una cadena.
  // Goal: Returns the character count of a string.
end;

function LengthSize
  (const ASource: umlcnullstring_64; const AMaxSize: Cardinal): Cardinal;
var P: umlcnullstring_64; ACount: Cardinal;
begin
  Result := 0;

  if (ASource <> nil) then
  begin
    P := ASource;
    ACount := 0;
    while ((P^ <> nullchar_64) and (ACount < AMaxSize)) do
    begin
      Inc(P);
      Inc(ACount);
    end;

    Result := ACount;
  end;
end;


(* global operators *)



(* global procedures *)


end.

