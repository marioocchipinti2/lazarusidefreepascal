(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcquadstrings;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement four byte (s) character based text.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes, umlcmemstrtypes,
  umlcquadchars, umlcstring32s,
  dummy;

// ---

const

 MOD_umlcquadstrings : TUMLCModule =
   ($84,$93,$06,$08,$32,$D2,$03,$40,$92,$58,$8B,$6E,$A1,$F1,$BE,$0D);

// ---

(* global functions *)

function IsEmpty
  (const AValue: umlcquadstring): Boolean; overload;

function IsStringOfChar
  (const S: umlcquadstring; C: umlcquadchar): Boolean; overload;

function Length
  (const AValue: umlcquadstring): Cardinal; overload;

function UppercaseCopy
  (const AValue: umlcquadstring): umlcquadstring; overload;
function LowercaseCopy
  (const AValue: umlcquadstring): umlcquadstring; overload;
function TogglecaseCopy
  (const AValue: umlcquadstring): umlcquadstring; overload;
function CapitalizeCopy
  (const AValue: umlcquadstring): umlcquadstring; overload;

function ConcatCharCopy
  (const A: umlcquadstring; const B: umlcquadchar): umlcquadstring; overload;
function ConcatStrCopy
  (var A, B: umlcquadstring): umlcquadstring; overload;

function StringOfChar
  (const AValue: umlcquadchar; const ACount: Byte): umlcquadstring;

(* global operators *)

procedure Assign
  (out   ADest:   umlcquadchar;
   const ASource: umlcquadchar); overload; // operator :=

function Equal
  (const A, B: umlcquadchar): Boolean; overload; // operator =
function Greater
  (const A, B: umlcquadchar): Boolean; overload; // operator >
function Lesser
  (const A, B: umlcquadchar): Boolean; overload; // operator <
function Different
  (const A, B: umlcquadchar): Boolean; overload; // operator <>
function GreaterEqual
  (const A, B: umlcquadchar): Boolean; overload; // operator >=
function LesserEqual
  (const A, B: umlcquadchar): Boolean; overload; // operator <=

(* global procedures *)


procedure Clear
  (out ADest: umlcquadstring);

implementation

(* global functions *)

function IsEmpty
  (const AValue: umlcquadstring): Boolean;
begin
  Result :=
    umlcstring32s.IsEmpty(ASource);
end;

function IsStringOfChar
  (const S: umlcquadstring; C: umlcquadchar): Boolean;
begin
  Result :=
    umlcstring32s.IsStringOfChar(S, C);
end;

function Length
  (const AValue: umlcquadstring): Cardinal;
begin
  Result :=
    umlcstring32s.Length(ASource);
end;

function UppercaseCopy
  (const AValue: umlcquadstring): umlcquadstring;
begin
  Result :=
    umlcstring32s.UppercaseCopy(AValue);
end;

function LowercaseCopy
  (const AValue: umlcquadstring): umlcquadstring;
begin
  Result :=
    umlcstring32s.UppercaseCopy(AValue);
end;

function TogglecaseCopy
  (const AValue: umlcquadstring): umlcquadstring;
begin
  Result :=
    umlcstring32s.TogglecaseCopy(ASource);
end;

function CapitalizeCopy
  (const AValue: umlcquadstring): umlcquadstring;
begin
  Result :=
    umlcstring32s.CapitalizeCopy(ASource);
end;

function ConcatCharCopy
  (const A: umlcquadstring; const B: umlcquadchar): umlcquadstring;
begin
  Result :=
    umlcstring32s.ConcatCharCopy(A, B);
end;

function ConcatStrCopy
  (var A, B: umlcquadstring): umlcquadstring;
begin
  Result :=
    umlcstring32s.ConcatStrCopy(A, B);
end;

function StringOfChar
  (const AValue: umlcquadchar; const ACount: Byte): umlcquadstring;
begin
  Result :=
    umlcstring32s.StringOfChar(ASource, ACount);
end;

(* global operators *)

procedure Assign
  (out   ADest: umlcquadstring;
   const ASource: umlcquadstring);
begin
  umlcstring32s.Assign
     (ADest, ASource);
end;

function Equal
  (const A, B: umlcquadstring): Boolean;
begin
  Result :=
    umlcstring32s.Equal(A, B);
end;

function Greater
  (const A, B: umlcquadstring): Boolean;
begin
  Result :=
    umlcstring32s.Greater(A, B);
end;

function Lesser
  (const A, B: umlcquadstring): Boolean;
begin
  Result :=
    umlcstring32s.Lesser(A, B);
end;

function Different
  (const A, B: umlcquadstring): Boolean;
begin
  Result :=
    umlcstring32s.Different(A, B);
end;

function GreaterEqual
  (const A, B: umlcquadstring): Boolean;
begin
  Result :=
    umlcstring32s.GreaterEqual(A, B);
end;

function LesserEqual
  (const A, B: umlcquadstring): Boolean;
begin
  Result :=
    umlcstring32s.LesserEqual(A, B);
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcquadstring);
begin
  umlcstring32s.Clear
     (ADest);
end;


end.

