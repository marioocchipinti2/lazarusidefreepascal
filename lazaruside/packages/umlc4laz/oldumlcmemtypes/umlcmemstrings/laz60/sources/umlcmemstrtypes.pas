(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemstrtypes;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement multiple size neutral encoded character text types.
 **************************************************************************
 **)

interface


uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  dummy;

// ---

const

 MOD_umlcstrtypes : TUMLCModule =
   ($3A,$93,$03,$CA,$6D,$24,$E3,$42,$92,$12,$06,$C9,$33,$DE,$C2,$DD);

// ---

const

  // ---

  ID_umlcstring_8 : TUMLCType =
    ($E3,$02,$32,$51,$E2,$1C,$5E,$4E,$8B,$2F,$EB,$4C,$79,$AD,$BB,$56);

  ID_umlcstring_16 : TUMLCType =
    ($8A,$71,$6F,$7B,$22,$56,$98,$47,$9A,$08,$F5,$BB,$E2,$1C,$0C,$86);

  ID_umlcstring_32 : TUMLCType =
    ($64,$FD,$4E,$41,$89,$84,$71,$44,$8E,$CA,$6F,$54,$32,$38,$9D,$AB);

  ID_umlcstring_64 : TUMLCType =
    ($83,$68,$82,$2D,$AA,$4C,$36,$4E,$B5,$34,$2F,$20,$26,$8D,$13,$70);

  // ---

const

  // ---

  ID_umlctstring_8 : TUMLCType =
    ($E3,$02,$32,$51,$E2,$1C,$5E,$4E,$8B,$2F,$EB,$4C,$79,$AD,$BB,$56);

  ID_umlctstring_16 : TUMLCType =
    ($8A,$71,$6F,$7B,$22,$56,$98,$47,$9A,$08,$F5,$BB,$E2,$1C,$0C,$86);

  ID_umlctstring_32 : TUMLCType =
    ($64,$FD,$4E,$41,$89,$84,$71,$44,$8E,$CA,$6F,$54,$32,$38,$9D,$AB);

  ID_umlctstring_64 : TUMLCType =
    ($83,$68,$82,$2D,$AA,$4C,$36,$4E,$B5,$34,$2F,$20,$26,$8D,$13,$70);

  // ---

const

  // ---

  ID_umlcpstring_8 : TUMLCType =
    ($78,$A6,$88,$37,$EE,$01,$08,$47,$B3,$C8,$C1,$ED,$0F,$B2,$36,$65);

  ID_umlcpstring_16 : TUMLCType =
    ($9D,$66,$B8,$E8,$FD,$D2,$B1,$48,$83,$2D,$BB,$C8,$98,$45,$E3,$39);

  ID_umlcpstring_32 : TUMLCType =
    ($F9,$A6,$21,$83,$FE,$A6,$BB,$4F,$B1,$50,$E0,$B9,$50,$F2,$B3,$ED);

  ID_umlcpstring_64 : TUMLCType =
    ($65,$20,$9F,$A5,$02,$6F,$87,$48,$87,$4B,$7D,$12,$32,$9A,$7F,$1E);

  // ---

const

  // ---

  ID_umlcbytestring : TUMLCType =
    ($E3,$02,$32,$51,$E2,$1C,$5E,$4E,$8B,$2F,$EB,$4C,$79,$AD,$BB,$56);

  ID_umlcwordstring : TUMLCType =
    ($8A,$71,$6F,$7B,$22,$56,$98,$47,$9A,$08,$F5,$BB,$E2,$1C,$0C,$86);

  ID_umlcquadstring : TUMLCType =
    ($64,$FD,$4E,$41,$89,$84,$71,$44,$8E,$CA,$6F,$54,$32,$38,$9D,$AB);

  ID_umlcoctastring : TUMLCType =
    ($83,$68,$82,$2D,$AA,$4C,$36,$4E,$B5,$34,$2F,$20,$26,$8D,$13,$70);

  // ---

const

  // ---

  ID_umlctbytestring : TUMLCType =
    ($E3,$02,$32,$51,$E2,$1C,$5E,$4E,$8B,$2F,$EB,$4C,$79,$AD,$BB,$56);

  ID_umlctwordstring : TUMLCType =
    ($8A,$71,$6F,$7B,$22,$56,$98,$47,$9A,$08,$F5,$BB,$E2,$1C,$0C,$86);

  ID_umlctquadstring : TUMLCType =
    ($64,$FD,$4E,$41,$89,$84,$71,$44,$8E,$CA,$6F,$54,$32,$38,$9D,$AB);

  ID_umlctoctastring : TUMLCType =
    ($83,$68,$82,$2D,$AA,$4C,$36,$4E,$B5,$34,$2F,$20,$26,$8D,$13,$70);

// ---

(* global constants *)

const
  MemberNotFound = -1;

(* global standard types *)

type

  // "Pascal" style neutral integer based encoding strings

  umlcstring_8  =
    type array[0 .. 255] of umlcchar_8;  // 1 byte (s) * 256
  umlcstring_16 =
    type array[0 .. 255] of umlcchar_16; // 2 byte (s) * 256
  umlcstring_32 =
    type array[0 .. 255] of umlcchar_32; // 4 byte (s) * 256
  umlcstring_64 =
    type array[0 .. 255] of umlcchar_64; // 8 byte (s) * 256

type

  umlctstring_8   = (* alias of *) umlcstring_8;
  umlctstring_16  = (* alias of *) umlcstring_16;
  umlctstring_32  = (* alias of *) umlcstring_32;
  umlctstring_64  = (* alias of *) umlcstring_64;

type

  umlcpstring_8   = ^umlcstring_8;
  umlcpstring_16  = ^umlcstring_16;
  umlcpstring_32  = ^umlcstring_32;
  umlctptring_64  = ^umlcstring_64;

type

  umlcbytestring = (* alias of *) umlcstring_8;
  umlcwordstring = (* alias of *) umlcstring_16;
  umlcquadstring = (* alias of *) umlcstring_32;
  umlcoctastring = (* alias of *) umlcstring_64;

type

  umlctbytestring = (* alias of *) umlcstring_8;
  umlctwordstring = (* alias of *) umlcstring_16;
  umlctquadstring = (* alias of *) umlcstring_32;
  umlctoctastring = (* alias of *) umlcstring_64;

type

  umlcpbytestring = ^umlcstring_8;
  umlcpwordstring = ^umlcstring_16;
  umlcpquadstring = ^umlcstring_32;
  umlcpoctastring = ^umlcstring_64;

(* global functions *)

function RestrictIndex
  (const AValue: Byte): Byte; overload;

implementation

(* global functions *)

function RestrictIndex
  (const AValue: Byte): Byte;
begin
  Result :=
    Math.Min(Math.Max(AValue, 1), 255);
  // Goal: Restricts a character index into a valid value.
  // Objetivo: Restringe el indice de caracter a un valor valido.
end;





end.

