(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstring64s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement pascal array style,
 ** fixed length 64 bits character based string type.
 ** Note:
 ** - The character encoding is neutral.
 ** - All characters have the same size.
 ** - Data is stored as am unsigned integer, not text.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcmemchartypes,
  umlcmemchar64s,
  dummy;

// ---

const

 MOD_umlcstring64s : TUMLCModule =
   ($52,$62,$73,$DD,$A3,$35,$41,$45,$98,$E3,$25,$58,$0D,$DE,$85,$D4);

// ---

(* global functions *)

function IsEmpty
 (const ASource: umlcstring_64): Boolean; overload;

function IsStringOfChar
 (const S: umlcstring_64; C: umlcchar_64): Boolean; overload;

function Length
 (const ASource: umlcstring_64): Cardinal; overload;

function SafeIndexOf
  (const AValue: Cardinal): Cardinal;

function UppercaseCopy
 (const ASource: umlcstring_64): umlcstring_64; overload;
function LowercaseCopy
 (const ASource: umlcstring_64): umlcstring_64; overload;
function TogglecaseCopy
 (const ASource: umlcstring_64): umlcstring_64; overload;
function CapitalizeCopy
 (const ASource: umlcstring_64): umlcstring_64; overload;

function ConcatCharCopy
 (const A: umlcstring_64; const B: umlcchar_64): umlcstring_64; overload;
function ConcatStrCopy
 (var A, B: umlcstring_64): umlcstring_64; overload;

function StringOfChar
 (const ASource: umlcchar_64; const ACount: Byte): umlcstring_64;

(* global operators *)

procedure Assign
 (out   ADest:   umlcstring_64;
  const ASource: umlcstring_64); overload; // operator :=

function Equal
 (const A, B: umlcstring_64): Boolean; overload; // operator =
function Greater
 (const A, B: umlcstring_64): Boolean; overload; // operator >
function Lesser
 (const A, B: umlcstring_64): Boolean; overload; // operator <
function Different
 (const A, B: umlcstring_64): Boolean; overload; // operator <>
function GreaterEqual
 (const A, B: umlcstring_64): Boolean; overload; // operator >=
function LesserEqual
 (const A, B: umlcstring_64): Boolean; overload; // operator <=

(* global procedures *)

procedure Clear
 (out ADest: umlcstring_64);

implementation

(* global functions *)

function IsEmpty
  (const ASource: umlcstring_64): Boolean;
begin
  Result := false; //(ASource^ = nullchar_64);
  // Goal: Returns if a string is empty.
  // Objetivo: Regresa si una cadena esta vacia.
end;

function IsStringOfChar
  (const S: umlcstring_64; C: umlcchar_64): Boolean;
var I, L: Integer; Match: Boolean;
begin
  L :=
    umlcstring64s.Length(S);

  Result := (L > 0);
  if (Result) then
  begin
    I := 1; Match := TRUE;
    while ((I <= L) and (Match)) do
    begin
      Match :=
        (S[i] = C);
      Inc(I);
    end;

    Result := Match;
  end;
  // Objetivo: Regresa si una cadena esta compuesta solo del mismo caracter.
  // Goal: Returns if a string is composed with the same character.
end;

function Length
  (const ASource: umlcstring_64): Cardinal;
begin
  Result :=
    Math.Min(umlcmemint_64(ASource[0]), 255);
end;

function SafeIndexOf
  (const AValue: umlcstring_64): Cardinal;
begin
  Result :=
    Math.Min(Math.Max(1, AValue), 255);
end;

function UppercaseCopy
  (const ASource: umlcstring_64): umlcstring_64;
var I, Last: Integer;
begin
  umlcstring64s.Clear
    (Result);

  Last :=
    umlcstring64s.Length(ASource);
  for I := 1 to Last do
  begin
    Result[i] :=
      umlcchar64s.UppercaseCopy(ASource[i]);
  end;

  Result[0] :=
    Last;
  // Goal: Returns a uppercase copy of the given string.
  // Objetivo: Regresa una copia en mayusculas de la cadena dada.
end;

function LowercaseCopy
  (const ASource: umlcstring_64): umlcstring_64;
var I, Last: Integer;
begin
  umlcstring64s.Clear
    (Result);

  Last :=
    umlcstring64s.Length(ASource);
  for I := 1 to Last do
  begin
    Result[i] :=
      umlcchar64s.UppercaseCopy(ASource[i]);
  end;

  Result[0] :=
    Last;
  // Goal: Returns a lowercase copy of the given string.
  // Objetivo: Regresa una copia en minusculas de la cadena dada.
end;

function TogglecaseCopy
  (const ASource: umlcstring_64): umlcstring_64;
var I, Last: Integer;
begin
  umlcstring64s.Clear
    (Result);

  Last :=
    umlcstring64s.Length(ASource);
  for I := 1 to Last do
  begin
    if (umlcchar64s.IsLowercase(ASource[i]))
      then Result[i] :=
        umlcchar64s.LowercaseCopy(ASource[i])
      else Result[i] :=
        umlcchar64s.UppercaseCopy(ASource[i]);
  end;

  Result[0] :=
    Last;
  // Goal: Swaps the sensitive case of each character in the given string.
  // Objetivo: Cambia el caso sensitivo de cada caracter en la cadena dada.
end;

function CapitalizeCopy
  (const ASource: umlcstring_64): umlcstring_64;
var I, Last: Integer; C: umlcchar_64; MakeUppercase: Boolean;
begin
  umlcstring64s.Clear
    (Result);

  MakeUppercase := TRUE;
  // cambiar a la primera letra en mayusculas
  // change first letter into uppercase

  Last :=
    umlcstring64s.Length(ASource);
  for I := 1 to Last do
  begin
    C := ASource[i];
    if (C <> 32) then
    begin
      if (MakeUppercase) then
      begin
        Result[i] :=
          umlcchar64s.UppercaseCopy(ASource[i]);
        MakeUppercase := FALSE;
      end else
      begin
        Result[i] :=
          umlcchar64s.LowercaseCopy(ASource[i]);
      end;
    end else MakeUppercase := TRUE;

    Result[0] :=
      Last;
  end;
  // Goal: Returns a copy with uppercase initials of the given string.
  // Objetivo: Regresa una copia con iniciales en mayusculas de la cadena dada.
end;

function ConcatCharCopy
  (const A: umlcstring_64; const B: umlcchar_64): umlcstring_64;
var SourceSize, AIndex: Cardinal;
begin
  Result[0] := 0;

  AIndex :=
    umlcstring64s.Length(A);
  SourceSize :=
    (AIndex * sizeof(umlcchar_64));

  if (AIndex < (sizeof(A) - 2)) then
  begin
    System.Move
      (A[1], Result[1], SourceSize);

    Result[AIndex] := B;
    Result[0] := umlcchar_64(AIndex + 1);
  end;
  // Objetivo: Agregar un caracter al final de la cadena dada.
  // Goal: Add a character at the end of the given string.
end;

function ConcatStrCopy
  (var A, B: umlcstring_64): umlcstring_64;
var AvailableSize, RequestedSize, AIndex: Cardinal;
begin
  Result[0] := 0;

  AvailableSize :=
    (umlcstring64s.Length(A) * sizeof(umlcchar_64));
  RequestedSize :=
    (umlcstring64s.Length(A) * sizeof(umlcchar_64));

  AIndex :=
    A[0];
  if (AvailableSize >= RequestedSize) then
  begin
    System.Move(B[1], A[1], RequestedSize);

    A[AIndex] :=
     (A[0] + B[0]);
  end;
  // Objetivo: Agregar una cadena al final de la cadena dada.
  // Goal: Add a string at the end of the given string.
end;

function StringOfChar
  (const ASource: umlcchar_64; const ACount: Byte): umlcstring_64;
var ASize: Cardinal;
begin
  umlcstring64s.Clear(Result);
  // Result := '';

  ASize :=
    (ACount * sizeof(umlcchar_64));

  System.FillByte
    (Result[1], ASize, ASource);
  // Goal: Returns a string with the same character.
  // Objetivo: Regresa una cadena con el mismo caracter.
end;

(* global operators *)

procedure Assign
  (out   ADest: umlcstring_64;
   const ASource: umlcstring_64);
var ASize, ACount: Cardinal;
begin
  ACount :=
    umlcbytechar(ASource[0]);
  ASize :=
    (ACount * sizeof(umlcbytechar));
  System.Move(ASource, ADest, ASize);

  ADest[0] :=
    umlcbytechar(ACount);
end;

function Equal
  (const A, B: umlcstring_64): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Greater
  (const A, B: umlcstring_64): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcstring_64): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function Different
  (const A, B: umlcstring_64): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function GreaterEqual
  (const A, B: umlcstring_64): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcstring_64): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcstring_64);
begin
  System.FillByte
    (ADest, sizeof(ADest), 0);
  // Goal: Clear an string.
  // Objetivo: Limpia una cadena.
end;




end.

