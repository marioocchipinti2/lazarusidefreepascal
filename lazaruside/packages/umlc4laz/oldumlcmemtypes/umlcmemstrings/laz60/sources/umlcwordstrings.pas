(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwordstrings;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement two byte (s) character based text.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes, umlcmemstrtypes,
  umlcwordchars, umlcstring16s,
  dummy;

// ---

const

 MOD_umlcwordstrings : TUMLCModule =
   ($14,$C7,$F8,$CB,$F6,$D0,$48,$40,$9B,$2B,$EC,$0E,$A3,$5B,$0E,$01);

// ---

(* global functions *)

function IsEmpty
  (const AValue: umlcwordstring): Boolean; overload;

function IsStringOfChar
  (const S: umlcwordstring; C: umlcwordchar): Boolean; overload;

function Length
  (const AValue: umlcwordstring): Cardinal; overload;

function UppercaseCopy
  (const AValue: umlcwordstring): umlcwordstring; overload;
function LowercaseCopy
  (const AValue: umlcwordstring): umlcwordstring; overload;
function TogglecaseCopy
  (const AValue: umlcwordstring): umlcwordstring; overload;
function CapitalizeCopy
  (const AValue: umlcwordstring): umlcwordstring; overload;

function ConcatCharCopy
  (const A: umlcwordstring; const B: umlcwordchar): umlcwordstring; overload;
function ConcatStrCopy
  (var A, B: umlcwordstring): umlcwordstring; overload;

function StringOfChar
  (const AValue: umlcwordchar; const ACount: Byte): umlcwordstring;

(* global operators *)

procedure Assign
  (out   ADest:   umlcwordchar;
   const ASource: umlcwordchar); overload; // operator :=

function Equal
  (const A, B: umlcwordchar): Boolean; overload; // operator =
function Greater
  (const A, B: umlcwordchar): Boolean; overload; // operator >
function Lesser
  (const A, B: umlcwordchar): Boolean; overload; // operator <
function Different
  (const A, B: umlcwordchar): Boolean; overload; // operator <>
function GreaterEqual
  (const A, B: umlcwordchar): Boolean; overload; // operator >=
function LesserEqual
  (const A, B: umlcwordchar): Boolean; overload; // operator <=

(* global procedures *)


procedure Clear
  (out ADest: umlcwordstring);

implementation

(* global functions *)

function IsEmpty
  (const AValue: umlcwordstring): Boolean;
begin
  Result :=
    umlcstring16s.IsEmpty(ASource);
end;

function IsStringOfChar
  (const S: umlcwordstring; C: umlcwordchar): Boolean;
begin
  Result :=
    umlcstring16s.IsStringOfChar(S, C);
end;

function Length
  (const AValue: umlcwordstring): Cardinal;
begin
  Result :=
    umlcstring16s.Length(ASource);
end;

function UppercaseCopy
  (const AValue: umlcwordstring): umlcwordstring;
begin
  Result :=
    umlcstring16s.UppercaseCopy(AValue);
end;

function LowercaseCopy
  (const AValue: umlcwordstring): umlcwordstring;
begin
  Result :=
    umlcstring16s.UppercaseCopy(AValue);
end;

function TogglecaseCopy
  (const AValue: umlcwordstring): umlcwordstring;
begin
  Result :=
    umlcstring16s.TogglecaseCopy(ASource);
end;

function CapitalizeCopy
  (const AValue: umlcwordstring): umlcwordstring;
begin
  Result :=
    umlcstring16s.CapitalizeCopy(ASource);
end;

function ConcatCharCopy
  (const A: umlcwordstring; const B: umlcwordchar): umlcwordstring;
begin
  Result :=
    umlcstring16s.ConcatCharCopy(A, B);
end;

function ConcatStrCopy
  (var A, B: umlcwordstring): umlcwordstring;
begin
  Result :=
    umlcstring16s.ConcatStrCopy(A, B);
end;

function StringOfChar
  (const AValue: umlcwordchar; const ACount: Byte): umlcwordstring;
begin
  Result :=
    umlcstring16s.StringOfChar(ASource, ACount);
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcwordstring;
   const ASource: umlcwordstring);
begin
  umlcstring16s.Assign
     (ADest, ASource);
end;

function Equal
  (const A, B: umlcwordstring): Boolean;
begin
  Result :=
    umlcstring16s.Equal(A, B);
end;

function Greater
  (const A, B: umlcwordstring): Boolean;
begin
  Result :=
    umlcstring16s.Greater(A, B);
end;

function Lesser
  (const A, B: umlcwordstring): Boolean;
begin
  Result :=
    umlcstring16s.Lesser(A, B);
end;

function Different
  (const A, B: umlcwordstring): Boolean;
begin
  Result :=
    umlcstring16s.Different(A, B);
end;

function GreaterEqual
  (const A, B: umlcwordstring): Boolean;
begin
  Result :=
    umlcstring16s.GreaterEqual(A, B);
end;

function LesserEqual
  (const A, B: umlcwordstring): Boolean;
begin
  Result :=
    umlcstring16s.LesserEqual(A, B);
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcwordstring);
begin
  umlcstring16s.Clear
     (ADest);
end;


end.

