(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstring8s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement pascal array style,
 ** fixed length 8 bits character based string type.
 ** Note:
 ** - The character encoding is neutral.
 ** - All characters have the same size.
 ** - Data is stored as am unsigned integer, not text.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcmemchartypes,
  umlcchar8s,
  dummy;


// ---

const

 MOD_umlcstring8s : TUMLCModule =
   ($16,$49,$98,$B9,$68,$58,$52,$48,$83,$1E,$31,$4E,$EB,$A7,$81,$FB);

// ---

(* global functions *)

function IsEmpty
 (const ASource: umlcstring_8): Boolean; overload;

function IsStringOfChar
 (const S: umlcstring_8; C: umlcchar_8): Boolean; overload;

function Length
 (const ASource: umlcstring_8): Cardinal; overload;

function SafeIndexOf
  (const AValue: Cardinal): Cardinal;

function UppercaseCopy
 (const ASource: umlcstring_8): umlcstring_8; overload;
function LowercaseCopy
 (const ASource: umlcstring_8): umlcstring_8; overload;
function TogglecaseCopy
 (const ASource: umlcstring_8): umlcstring_8; overload;
function CapitalizeCopy
 (const ASource: umlcstring_8): umlcstring_8; overload;

function ConcatCharCopy
 (const A: umlcstring_8; const B: umlcchar_8): umlcstring_8; overload;
function ConcatStrCopy
 (var A, B: umlcstring_8): umlcstring_8; overload;

function StringOfChar
 (const ASource: umlcchar_8; const ACount: Byte): umlcstring_8;

(* global operators *)

procedure Assign
 (out   ADest:   umlcstring_8;
  const ASource: umlcstring_8); overload; // operator :=

function Equal
 (const A, B: umlcstring_8): Boolean; overload; // operator =
function Greater
 (const A, B: umlcstring_8): Boolean; overload; // operator >
function Lesser
 (const A, B: umlcstring_8): Boolean; overload; // operator <
function Different
 (const A, B: umlcstring_8): Boolean; overload; // operator <>
function GreaterEqual
 (const A, B: umlcstring_8): Boolean; overload; // operator >=
function LesserEqual
 (const A, B: umlcstring_8): Boolean; overload; // operator <=

(* global procedures *)

procedure Clear
 (out ADest: umlcstring_8);

implementation

(* global functions *)

function IsEmpty
  (const ASource: umlcstring_8): Boolean;
begin
  Result :=
    umlcmemint_8(ASource[0]) = 0;
  // Goal: Returns if a string is empty.
  // Objetivo: Regresa si una cadena esta vacia.
end;

function IsStringOfChar
  (const S: umlcstring_8; C: umlcchar_8): Boolean;
var I, L: Integer; Match: Boolean;
begin
  L :=
    umlcstring8s.Length(S);

  Result := (L > 0);
  if (Result) then
  begin
    I := 1; Match := TRUE;
    while ((I <= L) and (Match)) do
    begin
      Match :=
        (S[i] = C);
      Inc(I);
    end;

    Result := Match;
  end;
  // Objetivo: Regresa si una cadena esta compuesta solo del mismo caracter.
  // Goal: Returns if a string is composed with the same character.
end;

function Length
  (const ASource: umlcstring_8): Cardinal;
begin
  Result :=
    Math.Min(umlcmemint_8(ASource[0]), 255);
end;

function SafeIndexOf
  (const AValue: umlcstring_8): Cardinal;
begin
  Result :=
    Math.Min(Math.Max(1, AValue), 255);
end;

function UppercaseCopy
  (const ASource: umlcstring_8): umlcstring_8;
var I, Last: Integer;
begin
  umlcstring8s.Clear
    (Result);

  Last :=
    umlcstring8s.Length(ASource);
  for I := 1 to Last do
  begin
    Result[i] :=
      umlcchar8s.UppercaseCopy(ASource[i]);
  end;

  Result[0] :=
    Last;
  // Goal: Returns a uppercase copy of the given string.
  // Objetivo: Regresa una copia en mayusculas de la cadena dada.
end;

function LowercaseCopy
  (const ASource: umlcstring_8): umlcstring_8;
var I, Last: Integer;
begin
  umlcstring8s.Clear
    (Result);

  Last :=
    umlcstring8s.Length(ASource);
  for I := 1 to Last do
  begin
    Result[i] :=
      umlcchar8s.UppercaseCopy(ASource[i]);
  end;

  Result[0] :=
    Last;
  // Goal: Returns a lowercase copy of the given string.
  // Objetivo: Regresa una copia en minusculas de la cadena dada.
end;

function TogglecaseCopy
  (const ASource: umlcstring_8): umlcstring_8;
var I, Last: Integer;
begin
  umlcstring8s.Clear
    (Result);

  Last :=
    umlcstring8s.Length(ASource);
  for I := 1 to Last do
  begin
    if (umlcchar8s.IsLowercase(ASource[i]))
      then Result[i] :=
        umlcchar8s.LowercaseCopy(ASource[i])
      else Result[i] :=
        umlcchar8s.UppercaseCopy(ASource[i]);
  end;

  Result[0] :=
    Last;
  // Goal: Swaps the sensitive case of each character in the given string.
  // Objetivo: Cambia el caso sensitivo de cada caracter en la cadena dada.
end;

function CapitalizeCopy
  (const ASource: umlcstring_8): umlcstring_8;
var I, Last: Integer; C: umlcchar_8; MakeUppercase: Boolean;
begin
  umlcstring8s.Clear
    (Result);

  MakeUppercase := TRUE;
  // cambiar a la primera letra en mayusculas
  // change first letter into uppercase

  Last :=
    umlcstring8s.Length(ASource);
  for I := 1 to Last do
  begin
    C := ASource[i];
    if (C <> 32) then
    begin
      if (MakeUppercase) then
      begin
        Result[i] :=
          umlcchar8s.UppercaseCopy(ASource[i]);
        MakeUppercase := FALSE;
      end else
      begin
        Result[i] :=
          umlcchar8s.LowercaseCopy(ASource[i]);
      end;
    end else MakeUppercase := TRUE;

    Result[0] :=
      Last;
  end;
  // Goal: Returns a copy with uppercase initials of the given string.
  // Objetivo: Regresa una copia con iniciales en mayusculas de la cadena dada.
end;

function ConcatCharCopy
  (const A: umlcstring_8; const B: umlcchar_8): umlcstring_8;
var SourceSize, AIndex: Cardinal;
begin
  Result[0] := 0;

  AIndex :=
    umlcstring8s.Length(A);
  SourceSize :=
    (AIndex * sizeof(umlcchar_8));

  if (AIndex < (sizeof(A) - 2)) then
  begin
    System.Move
      (A[1], Result[1], SourceSize);

    Result[AIndex] := B;
    Result[0] := umlcchar_8(AIndex + 1);
  end;
  // Objetivo: Agregar un caracter al final de la cadena dada.
  // Goal: Add a character at the end of the given string.
end;

function ConcatStrCopy
  (var A, B: umlcstring_8): umlcstring_8;
var AvailableSize, RequestedSize, AIndex: Cardinal;
begin
  Result[0] := 0;

  AvailableSize :=
    (umlcstring8s.Length(A) * sizeof(umlcchar_8));
  RequestedSize :=
    (umlcstring8s.Length(B) * sizeof(umlcchar_8));

  AIndex :=
    A[0];
  if (AvailableSize >= RequestedSize) then
  begin
    System.Move(B[1], A[1], RequestedSize);

    A[AIndex] :=
     (A[0] + B[0]);
  end;
  // Objetivo: Agregar una cadena al final de la cadena dada.
  // Goal: Add a string at the end of the given string.
end;

function StringOfChar
  (const ASource: umlcchar_8; const ACount: Byte): umlcstring_8;
var ASize: Cardinal;
begin
  umlcstring8s.Clear(Result);
  // Result := '';

  ASize :=
    (ACount * sizeof(umlcchar_8));

  System.FillByte
    (Result[1], ASize, ASource);
  // Goal: Returns a string with the same character.
  // Objetivo: Regresa una cadena con el mismo caracter.
end;

(* global operators *)

procedure Assign
  (out   ADest: umlcstring_8;
   const ASource: umlcstring_8);
var ASize, ACount: Cardinal;
begin
  ACount :=
    umlcbytechar(ASource[0]);
  ASize :=
    (ACount * sizeof(umlcbytechar));
  System.Move(ASource, ADest, ASize);

  ADest[0] :=
    umlcbytechar(ACount);
end;

function Equal
  (const A, B: umlcstring_8): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Greater
  (const A, B: umlcstring_8): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcstring_8): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function Different
  (const A, B: umlcstring_8): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function GreaterEqual
  (const A, B: umlcstring_8): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcstring_8): Boolean;
begin
  Result := false;
  // @ toxdo: ...
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcstring_8);
begin
  System.FillByte
    (ADest, sizeof(ADest), 0);
  // Goal: Clear an string.
  // Objetivo: Limpia una cadena.
end;


end.

