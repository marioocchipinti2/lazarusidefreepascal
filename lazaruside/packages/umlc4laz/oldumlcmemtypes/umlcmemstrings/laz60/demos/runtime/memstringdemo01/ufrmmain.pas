unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcdebug,
  umlcmemtypes,
  umlcmem8s,
  umlcmem8byptrs,
  umlcmem8buffers,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnTop: TPanel;
    sbMain: TStatusBar;
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    (* private declarations *)
  public
    (* public declarations *)

    DebugStr: string;

    procedure DebugWrite(const AMsg: string);
    procedure DebugWriteEoLn();
    procedure DebugWriteLn(const AMsg: string);
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure Tfrmmain.DebugWrite(const AMsg: string);
begin
  DebugStr := DebugStr + AMsg;
end;

procedure Tfrmmain.DebugWriteEoLn();
begin
  mmConsole.Lines.Add(DebugStr);
  DebugStr := '';
end;

procedure Tfrmmain.DebugWriteLn(const AMsg: string);
begin
  self.DebugWrite(AMsg);
  self.DebugWriteEoLn();
end;

procedure Tfrmmain.FormCreate(Sender: TObject);
begin
  DebugStr := '';
  umlcdebug.DebugWrite :=
    @self.DebugWrite;
  umlcdebug.DebugWriteEoLn :=
    @self.DebugWriteEoLn;
  umlcdebug.DebugWriteLn :=
    @self.DebugWriteLn;
end;

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close();
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
var ABuffer:  umlcmemintbuffer_8;
    EachItem: umlcpmemint_8;
    ACount: Cardinal; I: Byte;
begin
  mmConsole.Clear();

  ACount := 7;
  ABuffer :=
    umlcmem8buffers.BufferNew(ACount);

  EachItem :=
    umlcpmemint_8(ABuffer);
  for I := 0 to System.Pred(ACount) do
  begin
    EachItem^ := (ACount - umlcmemint_8(I));
    System.Inc(EachItem);
  end;

  EachItem :=
    umlcpmemint_8(ABuffer);
  for I := 0 to System.Pred(ACount) do
  begin
    mmConsole.Lines.Add('EachItem: [' + IntToStr(EachItem^) + ']');

    System.Inc(EachItem);
  end;

  umlcmem8buffers.BufferDrop
    (ABuffer, ACount);

  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('Done.');
end;





end.

