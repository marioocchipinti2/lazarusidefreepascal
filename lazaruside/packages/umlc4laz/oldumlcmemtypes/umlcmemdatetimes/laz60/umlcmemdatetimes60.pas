{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcmemdatetimes60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcmemdates, umlcmemdatesbyptrs, umlcmemdatetimebyptrs, umlcmemdatetimes, 
  umlcmemdatetimes_rtti, umlcmemdatetimetypes, umlcmemtimes, 
  umlcmemtimesbyptrs, umlcmemtimestamps, umlcmemtimestampsbyptrs, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('umlcmemdatetimes60', @Register);
end.
