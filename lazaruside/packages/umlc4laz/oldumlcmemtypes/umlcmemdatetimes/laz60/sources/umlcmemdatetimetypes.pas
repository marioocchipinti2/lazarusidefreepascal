(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemdatetimetypes;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement Date and Time related types.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_uumlcmemdatetimetypes : TUMLCModule =
   ($81,$1B,$C9,$A2,$83,$99,$9B,$43,$A4,$61,$CD,$A9,$16,$FB,$25,$04);

// ---

const
 ID_umlcmemdate : TUMLCType =
   ($AA,$9A,$F8,$AB,$9C,$45,$A7,$41,$96,$3B,$05,$91,$9B,$06,$9E,$D2);

 ID_umlctmemdate : TUMLCType =
   ($AA,$9A,$F8,$AB,$9C,$45,$A7,$41,$96,$3B,$05,$91,$9B,$06,$9E,$D2);
 ID_umlcpmemdate : TUMLCType =
   ($3F,$B7,$F4,$ED,$75,$21,$BD,$49,$A2,$EB,$22,$D1,$87,$83,$18,$9F);

// ---

const
 ID_umlcmemtime : TUMLCType =
   ($2B,$27,$E4,$9C,$7D,$AC,$8A,$42,$9D,$FD,$F7,$4E,$98,$E0,$0A,$92);

 ID_umlctmemtime : TUMLCType =
   ($2B,$27,$E4,$9C,$7D,$AC,$8A,$42,$9D,$FD,$F7,$4E,$98,$E0,$0A,$92);
 ID_umlcpmemtime : TUMLCType =
   ($47,$57,$9B,$D0,$36,$91,$D1,$48,$9C,$62,$50,$BE,$66,$DB,$6D,$B1);

// ---

const
 ID_umlcmemdatetime : TUMLCType =
   ($ED,$E5,$46,$77,$1A,$B0,$C1,$4D,$8E,$A3,$3B,$1C,$D8,$5E,$A7,$F7);

 ID_umlctmemdatetime : TUMLCType =
   ($ED,$E5,$46,$77,$1A,$B0,$C1,$4D,$8E,$A3,$3B,$1C,$D8,$5E,$A7,$F7);
 ID_umlcpmemdatetime : TUMLCType =
   ($DB,$33,$E4,$DA,$15,$3E,$7D,$49,$A0,$22,$E9,$88,$E7,$39,$05,$22);

// ---

 ID_umlcmemtimestamp : TUMLCType =
   ($F8,$0E,$09,$2B,$8B,$8B,$AF,$4E,$A4,$F4,$41,$D8,$0A,$EE,$47,$8E);

 ID_umlctmemtimestamp : TUMLCType =
   ($F8,$0E,$09,$2B,$8B,$8B,$AF,$4E,$A4,$F4,$41,$D8,$0A,$EE,$47,$8E);

 ID_umlcpmemtimestamp : TUMLCType =
   ($FF,$3C,$8A,$FA,$F6,$B0,$FE,$47,$84,$07,$56,$8E,$5E,$BF,$C4,$B3);

// ---

type
  umlcmemdate      = (* redefine *) type Integer;
  umlcmemtime      = (* redefine *) type Integer;
  umlcmemdatetime  = (* redefine *) type Int64;
  umlcmemtimestamp = (* redefine *) type Int64;

type
  umlctmemdate      = (* alias of *) umlcmemdate;
  umlcpmemdate      = ^umlcmemdate;

  umlctmemtime      = (* alias of *) umlcmemtime;
  umlcpmemtime      = ^umlcmemtime;

  umlctmemdatetime  = (* alias of *) umlcmemdatetime;
  umlcpmemdatetime  = ^umlcmemdatetime;

  umlctmemtimestamp = (* alias of *) umlcmemtimestamp;
  umlcpmemtimestamp = ^umlcmemtimestamp;

// ---

(* global constants *)

const
  NoDate = 0;

const
  NoTime = 0;

const
  NoDateTime = 0;

const
  NoTimeStamp = 0;

implementation




end.

