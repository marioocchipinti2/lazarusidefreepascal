unit umlcmemdatesbyptrs;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To reimplement a Date type and it's operations,
 ** by using pointers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemdatetimetypes,
  dummy;

// ---

const

 MOD_umlcmemdatesbyptrs : TUMLCModule =
   ($F1,$17,$91,$B6,$5E,$BF,$AE,$4B,$97,$64,$C9,$88,$C0,$47,$B0,$BA);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: umlcmemdate): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

function IsEmpty
 (const ASource: pointer): Boolean; overload;

(* global procedures *)

procedure Clear
 (out ADest: pointer); overload;


implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcmemdate): pointer;
var P: umlcpmemdate;
begin
  System.GetMem(P, sizeof(umlcmemdate));
  P^ := AValue;
  Result := pointer(P);
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcmemdate));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(umlcmemdate), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;



end.

