unit umlcmemtimesbyptrs;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To reimplement a Time type and it's operations,
 ** by using pointers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemdatetimetypes,
  dummy;

// ---

const

 MOD_umlcmemtimesbyptrs : TUMLCModule =
   ($A9,$BB,$1A,$86,$B7,$CC,$AD,$4C,$BB,$E5,$B2,$99,$F4,$81,$E7,$9A);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: umlcmemtime): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

function IsEmpty
 (const ASource: pointer): Boolean; overload;

(* global procedures *)

procedure Clear
 (out ADest: pointer); overload;


implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcmemtime): pointer;
var P: umlcpmemtime;
begin
  System.GetMem(P, sizeof(umlcmemtime));
  P^ := AValue;
  Result := pointer(P);
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcmemtime));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(umlcmemtime), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;



end.

