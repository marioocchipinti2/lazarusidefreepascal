unit umlcmemtimestampsbyptrs;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To reimplement a Timestamp type and it's operations,
 ** by using pointers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemdatetimetypes,
  dummy;

// ---

const

 MOD_umlcmemtimestampsbyptrs : TUMLCModule =
   ($9B,$1A,$3A,$14,$48,$5C,$62,$4C,$B8,$BE,$D5,$9B,$4C,$2B,$77,$9C);

// ---


implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcmemtimestamp): pointer;
var P: umlcpmemtimestamp;
begin
  System.GetMem(P, sizeof(umlcmemtimestamp));
  P^ := AValue;
  Result := pointer(P);
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcmemtimestamp));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(umlcmemtimestamp), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;



end.

