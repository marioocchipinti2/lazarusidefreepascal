unit umlcmemtimes;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemdatetimetypes,
  dummy;

// ---

const

 MOD_umlcmemtimes : TUMLCModule =
   ($D8,$A8,$48,$E7,$76,$5E,$C3,$40,$88,$8C,$01,$D4,$4A,$B7,$B0,$F0);

// ---

(* global functions *)

function TryEncodeTime
  (out AValue: umlcmemtime; AHour, AMin, ASec, AMSec: Word): Boolean;
function EncodeTime
  (const AHour, AMin, ASec, AMSec: Word): umlcmemtime;

implementation

(* global functions *)

function TryEncodeTime
  (out AValue: umlcmemtime; AHour, AMin, ASec, AMSec: Word): Boolean;
var Temp: umlcmemtime;
begin
  Temp := umlcmemdatetimetypes.NoTime;
  Result :=
    ((AHour < 24) and (AMin < 60) and (ASec < 60) and (AMSec < 1000));
  if (Result) then
  begin
    Temp := (AHour * 3600000 + AMin * 60000 + ASec * 1000 + AMSec) div MSecsPerDay;
  end;
  AValue := Temp;
  // Goal: Groups hours, minutes, seconds & milliseconds into
  // a complete time value.

  // Objetivo: Agrupa horas, minutos, segundos y milisegindos en
  // un valor tiempo completo.
end;

function EncodeTime
  (const AHour, AMin, ASec, AMSec: Word): umlcmemtime;
begin
  Result := umlcmemdatetimetypes.NoTime;
  if (not umlcmemtimes.TryEncodeTime(Result, AHour, AMin, ASec, AMSec)) then
  begin
    // raise error
  end;
  // Goal: Groups hours, minutes, seconds & milliseconds into
  // a complete time value.

  // Objetivo: Agrupa horas, minutos, segundos y milisegindos en
  // un valor tiempo completo.
end;




end.

