(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcquadcharsets;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement four byte (s) character text based sets.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  umlcquadchars,
  dummy;

// ---

const

 MOD_umlcquadcharsets : TUMLCModule =
   ($10,$28,$94,$66,$5F,$85,$10,$47,$AA,$87,$C4,$99,$5D,$54,$BE,$8C);

// ---

(* global constants *)

const
  MemberNotFound = -1;

(* global functions *)

function IsMember
  (AItem: umlcquadchar; const ASet: umlcquadcharset): Boolean; overload;
function IsEmpty
  (const ASet: umlcquadcharset): Boolean; overload;

function Range
  (const ALo, AHi: umlcquadchar): umlcquadcharset; overload;

(* global procedures *)

procedure Clear
  (out ADest: umlcquadcharset); overload;

procedure Include
  (var ASet: umlcquadcharset; AItem: umlcquadchar); overload;
procedure Exclude
  (var ASet: umlcquadcharset; AItem: umlcquadchar); overload;

procedure IncludeRange
  (var ASet: umlcquadcharset; Lo, Hi: umlcquadchar); overload;
procedure ExcludeRange
  (var ASet: umlcquadcharset; Lo, Hi: umlcquadchar); overload;

function ReplaceCharSetByChar
  (const Value: umlcquadchar; Source: umlcquadcharset; Dest: umlcquadchar): umlcquadchar; overload;

implementation

(* global functions *)

function InternalIndexOf
  (const AItem: umlcquadchar;
   const ASet:  umlcquadcharset): Cardinal;
begin
  Result := MemberNotFound;
end;

function InternalCount
  (const AValue: umlcquadcharset): Cardinal;
begin
  Result :=
    Math.Min(umlcquadbyte(AValue[0]), 255);
end;

function IsMember
  (AItem: umlcquadchar; const ASet: umlcquadcharset): Boolean;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found := (ASet[Index] = AItem);
    Inc(Index);
  end;
  Result := Found;
end;

function IsEmpty
  (const ASet: umlcquadcharset): Boolean;
begin
  Result :=
    umlcmemint_32(ASet[0]) = 0;
end;

function Range
  (const ALo, AHi: umlcquadchar): umlcquadcharset;
var AItem: umlcquadchar;
    Lo, Hi: umlcquadchar;
begin
  umlcquadcharsets.Clear(Result);

  Lo := umlcquadchars.IfChar((ALo > AHi), AHi, ALo);
  Hi := umlcquadchars.IfChar((ALo > AHi), ALo, AHi);

  for AItem := Lo to Hi do
  begin
    umlcquadcharsets.Include
      (Result, AItem);
  end;
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcquadcharset);
begin
  System.FillByte
    (ADest, sizeof(ADest), 0);
end;

procedure Include
  (var ASet: umlcquadcharset; AItem: umlcquadchar);
var AIndex, AOffset: Cardinal;
begin
  AIndex :=
    umlcquadcharsets.InternalCount(ASet);
  AOffset :=
    (AIndex * sizeof(umlcquadchar));
  if (AIndex < (sizeof(ASet) - 2)) then
  begin
    System.Move
      (AItem, ASet[AOffset], sizeof(umlcquadchar));

    ASet[0] := umlcquadchar(AIndex + 1);
  end;
end;

procedure Exclude
  (var ASet: umlcquadcharset; AItem: umlcquadchar);
var I, Count, Last: Byte; Temp: umlcquadcharset;
begin
  umlcquadcharsets.Clear(Temp);
  Count :=
    ASet[0];
  Last := 0;
  for I := 0 to System.Pred(Count) do
  begin
    if (ASet[I] <> AItem) then
    begin
      Temp[Last] := AItem;
      Inc(Last);
    end;
  end;

  System.Move(Temp, ASet, sizeof(ASet));
  ASet[0] := System.Pred(Last);
end;

procedure IncludeRange
  (var ASet: umlcquadcharset; Lo, Hi: umlcquadchar);
var AItem, L, H: umlcquadchar;
begin
  L := IfChar((Lo > Hi), Hi, Lo);
  H := IfChar((Lo > Hi), Lo, Hi);

  for AItem := L to H do
  begin
    Include(ASet, AItem);
  end;
end;

procedure ExcludeRange
  (var ASet: umlcquadcharset; Lo, Hi: umlcquadchar);
var AItem: umlcquadchar;
begin
  Lo := IfChar((Lo > Hi), Hi, Lo);
  Hi := IfChar((Lo > Hi), Lo, Hi);

  for AItem := Lo to Hi do
  begin
    Exclude(ASet, AItem);
  end;
end;

function ReplaceCharSetByChar
 (const Value: umlcquadchar; Source: umlcquadcharset; Dest: umlcquadchar): umlcquadchar;
begin
  { to-do...}
  if (IsMember(Value, Source))
    then Result := Dest
    else Result := Value;
  // Goal: Replace a specific character set.
  // Objetivo: Reemplazar un conjunto caracter en especifico.
end;

end.
