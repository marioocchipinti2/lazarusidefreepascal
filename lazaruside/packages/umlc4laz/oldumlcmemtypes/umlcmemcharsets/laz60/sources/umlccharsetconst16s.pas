unit umlccharsetconst16s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to implement commonly used character sets.
 ** Note:
 ** - Each elements is a 16 bits character.
 ** - The character encoding is neutral.
 ** - All characters have the same size.
 ** - Data is stored as an unsigned integer, not text.
 **************************************************************************
 **)

interface

uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  umlcchar16s,
  umlccharset16s,
  dummy;

// ---

const

 MOD_umlccharsetconst16s : TUMLCModule =
   ($8F,$14,$C9,$86,$A4,$29,$D4,$47,$BB,$D8,$4D,$83,$A6,$3C,$2A,$1E);

// ---

var
  BlanksSet, EoLnSet, NullSet: umlccharset_16;
  BoolSet: umlccharset_16;
  UpperSet, LowerSet: umlccharset_16;
  AlphaSet, FloatSet, CurrSet: umlccharset_16;
  DecDigitSet, OctDigitSet, HexDigitSet, BinDigitSet: umlccharset_16;
  IDSet, QIDSet, WildcardSet: umlccharset_16;
  UpperIDSet, LowerIDSet: umlccharset_16;


implementation

procedure UnitConstructor;
begin
  umlccharset16s.Clear(BlanksSet);
  umlccharset16s.Include(BlanksSet, umlcchar_16(32));
  umlccharset16s.Include(BlanksSet, umlcchar_16(13));
  umlccharset16s.Include(BlanksSet, umlcchar_16(10));
  umlccharset16s.Include(BlanksSet, umlcchar_16(12));

  umlccharset16s.Clear(EoLnSet);
  umlccharset16s.Include(EoLnSet, umlcchar_16(13));
  umlccharset16s.Include(EoLnSet, umlcchar_16(10));

  umlccharset16s.Clear(NullSet);
  umlccharset16s.Include(NullSet, umlcchar_16(0));
  umlccharset16s.Include(NullSet, umlcchar_16(13));
  umlccharset16s.Include(NullSet, umlcchar_16(10));
  umlccharset16s.Include(NullSet, umlcchar_16(26));

  umlccharset16s.Clear(BoolSet);
  umlccharset16s.Include(BoolSet, umlcchar_16(0));
  umlccharset16s.Include(BoolSet, umlcchar_16('f'));
  umlccharset16s.Include(BoolSet, umlcchar_16('F'));
  // set of values used for FALSE representation
  // conjunto de valores utilizados para la representacion de FALSO

  umlccharset16s.Clear(UpperSet);
  umlccharset16s.IncludeRange(UpperSet, umlcchar_16('A'), umlcchar_16('Z'));
  // UpperSet  := ['A'..'Z'];

  umlccharset16s.Clear(LowerSet);
  umlccharset16s.IncludeRange(LowerSet, umlcchar_16('a'), umlcchar_16('z'));
  // LowerSet  := ['a'..'z'];

  umlccharset16s.Clear(AlphaSet);
  umlccharset16s.IncludeRange(AlphaSet, umlcchar_16('A'), umlcchar_16('Z'));
  umlccharset16s.IncludeRange(AlphaSet, umlcchar_16('a'), umlcchar_16('z'));
  // AlphaSet  := ['A'..'Z', 'a'..'z'];

  umlccharset16s.Clear(DecDigitSet);
  umlccharset16s.IncludeRange(DecDigitSet, umlcchar_16('0'), umlcchar_16('9'));
  // DecDigitSet  := ['0'..'9'];

  umlccharset16s.Clear(OctDigitSet);
  umlccharset16s.IncludeRange(OctDigitSet, umlcchar_16('0'), umlcchar_16('7'));
  // OctDigitSet  := ['0'..'7'];

  umlccharset16s.Clear(HexDigitSet);
  umlccharset16s.IncludeRange(HexDigitSet, umlcchar_16('0'), umlcchar_16('9'));
  umlccharset16s.IncludeRange(HexDigitSet, umlcchar_16('a'), umlcchar_16('f'));
  umlccharset16s.IncludeRange(HexDigitSet, umlcchar_16('A'), umlcchar_16('F'));
  // HexDigitSet  := ['0'..'9', 'a'..'f', 'A'..'F'];

  umlccharset16s.Clear(BinDigitSet);
  umlccharset16s.Include(BinDigitSet, umlcchar_16('0'));
  umlccharset16s.Include(BinDigitSet, umlcchar_16('1'));
  // BinDigitSet  := ['0', '1'];

  umlccharset16s.Clear(FloatSet);
  umlccharset16s.IncludeRange(FloatSet, umlcchar_16('0'), umlcchar_16('9'));
  umlccharset16s.Include(FloatSet, umlcchar_16('.'));
  umlccharset16s.Include(FloatSet, umlcchar_16(','));
  umlccharset16s.Include(FloatSet, umlcchar_16('+'));
  umlccharset16s.Include(FloatSet, umlcchar_16('-'));
  umlccharset16s.Include(FloatSet, umlcchar_16('E'));
  umlccharset16s.Include(FloatSet, umlcchar_16('e'));
  // FloatSet  := ['0'..'9', '.', ',', '+', '-', 'E', 'e'];

  umlccharset16s.Clear(CurrSet);
  umlccharset16s.IncludeRange(CurrSet, umlcchar_16('0'), umlcchar_16('9'));
  umlccharset16s.Include(CurrSet, umlcchar_16('.'));
  umlccharset16s.Include(CurrSet, umlcchar_16(','));
  // CurrSet   := ['0'..'9', '.', ','];

  umlccharset16s.Clear(IDSet);
  umlccharset16s.IncludeRange(IDSet, umlcchar_16('A'), umlcchar_16('Z'));
  umlccharset16s.IncludeRange(IDSet, umlcchar_16('a'), umlcchar_16('z'));
  umlccharset16s.IncludeRange(IDSet, umlcchar_16('0'), umlcchar_16('9'));
  umlccharset16s.Include(IDSet, umlcchar_16('_'));
  // IDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9'];

  umlccharset16s.Assign(WildcardSet, IDSet);
  umlccharset16s.Include(WildcardSet, umlcchar_16('*'));
  umlccharset16s.Include(WildcardSet, umlcchar_16('?'));
  // WildcardSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '*', '?'];

  umlccharset16s.Assign(QIDSet, IDSet);
  umlccharset16s.Include(QIDSet, umlcchar_16('.'));
  // QIDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '.'];

  umlccharset16s.Clear(UpperIDSet);
  umlccharset16s.IncludeRange(UpperIDSet, umlcchar_16('A'), umlcchar_16('Z'));
  umlccharset16s.IncludeRange(UpperIDSet, umlcchar_16('0'), umlcchar_16('9'));
  umlccharset16s.Include(UpperIDSet, umlcchar_16('_'));
  // UpperIDSet := ['A'..'Z', '_', '0'..'9'];

  umlccharset16s.Clear(LowerIDSet);
  umlccharset16s.IncludeRange(LowerIDSet, umlcchar_16('A'), umlcchar_16('Z'));
  umlccharset16s.IncludeRange(LowerIDSet, umlcchar_16('a'), umlcchar_16('z'));
  umlccharset16s.IncludeRange(LowerIDSet, umlcchar_16('0'), umlcchar_16('9'));
  umlccharset16s.Include(LowerIDSet, umlcchar_16('_'));
  // LowerIDSet := ['a'..'z', '_', '0'..'9'];
end;

procedure UnitDestructor;
begin
  umlccharset16s.Clear(WildcardSet);
  umlccharset16s.Clear(IDSet);
  umlccharset16s.Clear(QIDSet);

  umlccharset16s.Clear(CurrSet);
  umlccharset16s.Clear(FloatSet);
  umlccharset16s.Clear(DecDigitSet);
  umlccharset16s.Clear(AlphaSet);

  umlccharset16s.Clear(LowerSet);
  umlccharset16s.Clear(UpperSet);

  umlccharset16s.Clear(BoolSet);

  umlccharset16s.Clear(NullSet);
  umlccharset16s.Clear(EoLnSet);
  umlccharset16s.Clear(BlanksSet);
end;

initialization
  UnitConstructor;
finalization
  UnitDestructor;
end.


