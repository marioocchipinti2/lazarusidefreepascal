(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwordchars;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement two byte (s) based characters.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  dummy;

// ---

const

 MOD_umlcwordchars : TUMLCModule =
   ($80,$00,$CA,$90,$AA,$F1,$CC,$49,$80,$1E,$89,$39,$22,$06,$D9,$14);

// ---

(* global constants *)

const
  wordnullchar = 0;

(* global standard functions *)

  function IsNull
    (const AValue: umlcwordchar): Boolean; overload;
  function IsAlpha
    (const AValue: umlcwordchar): Boolean; overload;
  function IsDigit
    (const AValue: umlcwordchar): Boolean; overload;
  function IsSpace
    (const AValue: umlcwordchar): Boolean; overload;
  function IsBlank
    (const AValue: umlcwordchar): Boolean; overload;

  function IsDecimal
    (const AValue: umlcwordchar): Boolean; overload;
  function IsHexa
    (const AValue: umlcwordchar): Boolean; overload;
  function IsOctal
    (const AValue: umlcwordchar): Boolean; overload;
  function IsBinary
    (const AValue: umlcwordchar): Boolean; overload;

  function IsUppercase
    (const AValue: umlcwordchar): Boolean; overload;
  function IsLowercase
    (const AValue: umlcwordchar): Boolean; overload;

(* global additional functions *)

  function CharToOrd
    (const AValue: umlcwordchar): Integer; overload;
  function OrdToChar
    (const AValue: Integer): umlcwordchar; overload;

  function UppercaseCopy
    (const AValue: umlcwordchar): umlcwordchar; overload;
  function LowercaseCopy
    (const AValue: umlcwordchar): umlcwordchar; overload;
  function TogglecaseCopy
    (const AValue: umlcwordchar): umlcwordchar; overload;

  function SameText
    (const A, B: umlcwordchar): Boolean; overload;

  function IfChar
    (APredicate: Boolean; A, B: umlcwordchar): umlcwordchar;

  function ReplaceChar
    (const AValue: umlcwordchar; A, B: umlcwordchar): umlcwordchar; overload;
	
(* global operators *)

  procedure Assign
    (var   ADest:   umlcwordchar;
     const ASource: umlcwordchar); overload; // operator :=

  function Equal
    (const A, B: umlcwordchar): Boolean; overload; // operator =
  function Greater
    (const A, B: umlcwordchar): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcwordchar): Boolean; overload; // operator <
  function Different
    (const A, B: umlcwordchar): Boolean; overload; // operator <>
  function GreaterEqual
    (const A, B: umlcwordchar): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcwordchar): Boolean; overload; // operator <=

(* global procedures *)

  procedure Clear
    (var AValue: umlcwordchar); overload;

implementation

(* global standard functions *)

function IsNull
  (const AValue: umlcwordchar): Boolean;
begin
  Result :=
    (AValue = 0);
end;

function IsAlpha
  (const AValue: umlcwordchar): Boolean;
begin
  Result :=
    Windows.IsCharAlphaA(ansichar(AValue));
end;

function IsDigit
  (const AValue: umlcwordchar): Boolean;
begin
  Result :=
    ((ansichar(AValue) >= '0') and (ansichar(AValue) <= '9'));
end;

function IsSpace
  (const AValue: umlcwordchar): Boolean;
begin
  Result :=
    (AValue = 32);
end;

function IsBlank
  (const AValue: umlcwordchar): Boolean;
begin
  Result :=
    (AValue = 32) or // space
    (AValue = 13) or // D.O.S. carriage-return
    (AValue = 10) or // D.O.S. line-feed
    (AValue = 12) or // page break
    (AValue = 08);   // tabulator
end;

function IsDecimal
  (const AValue: umlcwordchar): Boolean;
begin
  Result :=
    ((ansichar(AValue) >= '0') and (ansichar(AValue) <= '9'));
end;

function IsHexa
  (const AValue: umlcwordchar): Boolean;
begin
  Result :=
    (ansichar(AValue) >= '0') and (ansichar(AValue) <= '9') or
    (ansichar(AValue) >= 'A') and (ansichar(AValue) <= 'F') or
    (ansichar(AValue) >= 'a') and (ansichar(AValue) <= 'f');
end;

function IsOctal
  (const AValue: umlcwordchar): Boolean;
begin
  Result :=
    (ansichar(AValue) >= '0') and (ansichar(AValue) <= '7');
end;

function IsBinary
  (const AValue: umlcwordchar): Boolean;
begin
  Result :=
    (ansichar(AValue) = '0') or (ansichar(AValue) = '1');
end;

function IsUppercase
  (const AValue: umlcwordchar): Boolean;
begin
  Result :=
    Windows.IsCharUpperA(ansichar(AValue));
end;

function IsLowercase
  (const AValue: umlcwordchar): Boolean;
begin
  Result :=
    Windows.IsCharLowerA(ansichar(AValue));
end;

(* global additional functions *)

function CharToOrd
  (const AValue: umlcwordchar): Integer;
begin
  Result :=
    Ord(AValue);
end;

function OrdToChar
  (const AValue: Integer): umlcwordchar;
begin
  Result :=
    umlcwordchar(AValue);
end;

function UppercaseCopy
  (const AValue: umlcwordchar): umlcwordchar;
begin
  Result :=
    AValue;
  Windows.CharUpperBuffA(@Result, 1);
  // Goal: Returns a uppercase copy of the given character.
  // Objetivo: Regresa una copia en mayusculas del caracter dado.
end;

function LowercaseCopy
  (const AValue: umlcwordchar): umlcwordchar;
begin
  Result := AValue;
  Windows.CharLowerBuffA(@Result, 1);
  // Goal: Returns a lowercase copy of the given character.
  // Objetivo: Regresa una copia en minusculas del caracter dado.
end;

function TogglecaseCopy
  (const AValue: umlcwordchar): umlcwordchar;
begin
  if (IsUppercase(AValue))
    then Result := LowercaseCopy(AValue)
    else Result := UppercaseCopy(AValue);
  // Goal: Returns a switched case copy of the given character.
  // Objetivo: Regresa una copia en caso intercambiado del caracter dado.
end;

function SameText
  (const A, B: umlcwordchar): Boolean;
begin
  Result :=
    (Lowercase(ansichar(A)) = Lowercase(ansichar(B)));
  // Goal: Returns if 2 characters are equal, ignores sensitive case.
  // Objetivo: Regresa si 2 caracteres son iguales, ignorar caso sensitivo.
end;

function IfChar
  (APredicate: Boolean; A, B: umlcwordchar): umlcwordchar;
begin
  if (APredicate)
    then Result := A
    else Result := B;
  // Objetivo: Segun la condicion, regresar el caracter seleccionado.
  // Goal: Upon condition, return the select character.
end;

function ReplaceChar
  (const AValue: umlcwordchar; A, B: umlcwordchar): umlcwordchar;
begin
  if (AValue = A)
    then Result := B
    else Result := AValue;
  // Objetivo: Reemplazar un caracter en especifico.
  // Goal: Upon condition, return the select character.  
end;

(* global operators *)

procedure Assign
  (var   ADest:   umlcwordchar;
   const ASource: umlcwordchar);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcwordchar): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Greater
  (const A, B: umlcwordchar): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcwordchar): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function Different
  (const A, B: umlcwordchar): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function GreaterEqual
  (const A, B: umlcwordchar): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcwordchar): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

(* global procedures *)

procedure Clear
  (var AValue: umlcwordchar);
begin
  AValue :=
    wordnullchar;
  // Goal: Clear a character.
  // Objetivo: Limpia un caracter.
end;




end.

