(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwordcharsets;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement two byte (s) character text based sets.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  umlcwordchars,
  dummy;

// ---

const

 MOD_umlcwordcharsets : TUMLCModule =
   ($EB,$CD,$49,$EC,$97,$12,$FB,$46,$82,$9F,$BA,$AD,$6E,$F4,$DA,$EE);

// ---

(* global constants *)

const
  MemberNotFound = -1;

(* global functions *)

function IsMember
  (AItem: umlcwordchar; const ASet: umlcwordcharset): Boolean; overload;
function IsEmpty
  (const ASet: umlcwordcharset): Boolean; overload;

function Range
  (const ALo, AHi: umlcwordchar): umlcwordcharset; overload;

(* global procedures *)

procedure Clear
  (out ADest: umlcwordcharset); overload;

procedure Include
  (var ASet: umlcwordcharset; AItem: umlcwordchar); overload;
procedure Exclude
  (var ASet: umlcwordcharset; AItem: umlcwordchar); overload;

procedure IncludeRange
  (var ASet: umlcwordcharset; Lo, Hi: umlcwordchar); overload;
procedure ExcludeRange
  (var ASet: umlcwordcharset; Lo, Hi: umlcwordchar); overload;

function ReplaceCharSetByChar
  (const Value: umlcwordchar; Source: umlcwordcharset; Dest: umlcwordchar): umlcwordchar; overload;

implementation

(* global functions *)

function InternalIndexOf
  (const AItem: umlcwordchar;
   const ASet:  umlcwordcharset): Cardinal;
begin
  Result := MemberNotFound;
end;

function InternalCount
  (const AValue: umlcwordcharset): Cardinal;
begin
  Result :=
    Math.Min(umlcword(AValue[0]), 255);
end;

function IsMember
  (AItem: umlcwordchar; const ASet: umlcwordcharset): Boolean;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found := (ASet[Index] = AItem);
    Inc(Index);
  end;
  Result := Found;
end;

function IsEmpty
  (const ASet: umlcwordcharset): Boolean;
begin
  Result :=
    umlcmemint_16(ASet[0]) = 0;
end;

function Range
  (const ALo, AHi: umlcwordchar): umlcwordcharset;
var AItem: umlcwordchar;
    Lo, Hi: umlcwordchar;
begin
  umlcwordcharsets.Clear(Result);

  Lo := umlcwordchars.IfChar((Lo > Hi), Hi, Lo);
  Hi := umlcwordchars.IfChar((Lo > Hi), Lo, Hi);

  for AItem := Lo to Hi do
  begin
    umlcwordcharsets.Include
      (Result, AItem);
  end;
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcwordcharset);
begin
  System.FillByte
    (ADest, sizeof(ADest), 0);
end;

procedure Include
  (var ASet: umlcwordcharset; AItem: umlcwordchar);
var AIndex, AOffset: Cardinal;
begin
  AIndex :=
    umlcwordcharsets.InternalCount(ASet);
  AOffset :=
    (AIndex * sizeof(umlcwordchar));
  if (AIndex < (sizeof(ASet) - 2)) then
  begin
    System.Move
      (AItem, ASet[AOffset], sizeof(umlcwordchar));

    ASet[0] := umlcwordchar(AIndex + 1);
  end;
end;

procedure Exclude
  (var ASet: umlcwordcharset; AItem: umlcwordchar);
var I, Count, Last: Byte; Temp: umlcwordcharset;
begin
  umlcwordcharsets.Clear(Temp);
  Count :=
    ASet[0];
  Last := 0;
  for I := 0 to System.Pred(Count) do
  begin
    if (ASet[I] <> AItem) then
    begin
      Temp[Last] := AItem;
      Inc(Last);
    end;
  end;

  System.Move(Temp, ASet, sizeof(ASet));
  ASet[0] := System.Pred(Last);
end;

procedure IncludeRange
  (var ASet: umlcwordcharset; Lo, Hi: umlcwordchar);
var AItem, L, H: umlcwordchar;
begin
  L := IfChar((Lo > Hi), Hi, Lo);
  H := IfChar((Lo > Hi), Lo, Hi);

  for AItem := L to H do
  begin
    Include(ASet, AItem);
  end;
end;

procedure ExcludeRange
  (var ASet: umlcwordcharset; Lo, Hi: umlcwordchar);
var AItem: umlcwordchar;
begin
  Lo := IfChar((Lo > Hi), Hi, Lo);
  Hi := IfChar((Lo > Hi), Lo, Hi);

  for AItem := Lo to Hi do
  begin
    Exclude(ASet, AItem);
  end;
end;

function ReplaceCharSetByChar
 (const Value: umlcwordchar; Source: umlcwordcharset; Dest: umlcwordchar): umlcwordchar;
begin
  { to-do...}
  if (IsMember(Value, Source))
    then Result := Dest
    else Result := Value;
  // Goal: Replace a specific character set.
  // Objetivo: Reemplazar un conjunto caracter en especifico.
end;

end.
