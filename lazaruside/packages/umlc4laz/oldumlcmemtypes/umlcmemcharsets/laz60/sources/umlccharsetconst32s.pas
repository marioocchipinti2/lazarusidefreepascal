unit umlccharsetconst32s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to implement commonly used character sets.
 ** Note:
 ** - Each elements is a 32 bits character.
 ** - The character encoding is neutral.
 ** - All characters have the same size.
 ** - Data is stored as an unsigned integer, not text.
 **************************************************************************
 **)

interface

uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  umlcchar32s,
  umlccharset32s,
  dummy;

// ---

const

 MOD_umlccharsetconst32s : TUMLCModule =
   ($D1,$72,$41,$BE,$D4,$21,$78,$40,$9F,$C4,$25,$C3,$F5,$B1,$96,$22);

// ---

var
  BlanksSet, EoLnSet, NullSet: umlccharset_32;
  BoolSet: umlccharset_32;
  UpperSet, LowerSet: umlccharset_32;
  AlphaSet, FloatSet, CurrSet: umlccharset_32;
  DecDigitSet, OctDigitSet, HexDigitSet, BinDigitSet: umlccharset_32;
  IDSet, QIDSet, WildcardSet: umlccharset_32;
  UpperIDSet, LowerIDSet: umlccharset_32;

implementation

procedure UnitConstructor;
begin
  umlccharset32s.Clear(BlanksSet);
  umlccharset32s.Include(BlanksSet, umlcchar_32(32));
  umlccharset32s.Include(BlanksSet, umlcchar_32(13));
  umlccharset32s.Include(BlanksSet, umlcchar_32(10));
  umlccharset32s.Include(BlanksSet, umlcchar_32(12));

  umlccharset32s.Clear(EoLnSet);
  umlccharset32s.Include(EoLnSet, umlcchar_32(13));
  umlccharset32s.Include(EoLnSet, umlcchar_32(10));

  umlccharset32s.Clear(NullSet);
  umlccharset32s.Include(NullSet, umlcchar_32(0));
  umlccharset32s.Include(NullSet, umlcchar_32(13));
  umlccharset32s.Include(NullSet, umlcchar_32(10));
  umlccharset32s.Include(NullSet, umlcchar_32(26));

  umlccharset32s.Clear(BoolSet);
  umlccharset32s.Include(BoolSet, umlcchar_32(0));
  umlccharset32s.Include(BoolSet, umlcchar_32('f'));
  umlccharset32s.Include(BoolSet, umlcchar_32('F'));
  // set of values used for FALSE representation
  // conjunto de valores utilizados para la representacion de FALSO

  umlccharset32s.Clear(UpperSet);
  umlccharset32s.IncludeRange(UpperSet, umlcchar_32('A'), umlcchar_32('Z'));
  // UpperSet  := ['A'..'Z'];

  umlccharset32s.Clear(LowerSet);
  umlccharset32s.IncludeRange(LowerSet, umlcchar_32('a'), umlcchar_32('z'));
  // LowerSet  := ['a'..'z'];

  umlccharset32s.Clear(AlphaSet);
  umlccharset32s.IncludeRange(AlphaSet, umlcchar_32('A'), umlcchar_32('Z'));
  umlccharset32s.IncludeRange(AlphaSet, umlcchar_32('a'), umlcchar_32('z'));
  // AlphaSet  := ['A'..'Z', 'a'..'z'];

  umlccharset32s.Clear(DecDigitSet);
  umlccharset32s.IncludeRange(DecDigitSet, umlcchar_32('0'), umlcchar_32('9'));
  // DecDigitSet  := ['0'..'9'];

  umlccharset32s.Clear(OctDigitSet);
  umlccharset32s.IncludeRange(OctDigitSet, umlcchar_32('0'), umlcchar_32('7'));
  // OctDigitSet  := ['0'..'7'];

  umlccharset32s.Clear(HexDigitSet);
  umlccharset32s.IncludeRange(HexDigitSet, umlcchar_32('0'), umlcchar_32('9'));
  umlccharset32s.IncludeRange(HexDigitSet, umlcchar_32('a'), umlcchar_32('f'));
  umlccharset32s.IncludeRange(HexDigitSet, umlcchar_32('A'), umlcchar_32('F'));
  // HexDigitSet  := ['0'..'9', 'a'..'f', 'A'..'F'];

  umlccharset32s.Clear(BinDigitSet);
  umlccharset32s.Include(BinDigitSet, umlcchar_32('0'));
  umlccharset32s.Include(BinDigitSet, umlcchar_32('1'));
  // BinDigitSet  := ['0', '1'];

  umlccharset32s.Clear(FloatSet);
  umlccharset32s.IncludeRange(FloatSet, umlcchar_32('0'), umlcchar_32('9'));
  umlccharset32s.Include(FloatSet, umlcchar_32('.'));
  umlccharset32s.Include(FloatSet, umlcchar_32(','));
  umlccharset32s.Include(FloatSet, umlcchar_32('+'));
  umlccharset32s.Include(FloatSet, umlcchar_32('-'));
  umlccharset32s.Include(FloatSet, umlcchar_32('E'));
  umlccharset32s.Include(FloatSet, umlcchar_32('e'));
  // FloatSet  := ['0'..'9', '.', ',', '+', '-', 'E', 'e'];

  umlccharset32s.Clear(CurrSet);
  umlccharset32s.IncludeRange(CurrSet, umlcchar_32('0'), umlcchar_32('9'));
  umlccharset32s.Include(CurrSet, umlcchar_32('.'));
  umlccharset32s.Include(CurrSet, umlcchar_32(','));
  // CurrSet   := ['0'..'9', '.', ','];

  umlccharset32s.Clear(IDSet);
  umlccharset32s.IncludeRange(IDSet, umlcchar_32('A'), umlcchar_32('Z'));
  umlccharset32s.IncludeRange(IDSet, umlcchar_32('a'), umlcchar_32('z'));
  umlccharset32s.IncludeRange(IDSet, umlcchar_32('0'), umlcchar_32('9'));
  umlccharset32s.Include(IDSet, umlcchar_32('_'));
  // IDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9'];

  umlccharset32s.Assign(WildcardSet, IDSet);
  umlccharset32s.Include(WildcardSet, umlcchar_32('*'));
  umlccharset32s.Include(WildcardSet, umlcchar_32('?'));
  // WildcardSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '*', '?'];

  umlccharset32s.Assign(QIDSet, IDSet);
  umlccharset32s.Include(QIDSet, umlcchar_32('.'));
  // QIDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '.'];

  umlccharset32s.Clear(UpperIDSet);
  umlccharset32s.IncludeRange(UpperIDSet, umlcchar_32('A'), umlcchar_32('Z'));
  umlccharset32s.IncludeRange(UpperIDSet, umlcchar_32('0'), umlcchar_32('9'));
  umlccharset32s.Include(UpperIDSet, umlcchar_32('_'));
  // UpperIDSet := ['A'..'Z', '_', '0'..'9'];

  umlccharset32s.Clear(LowerIDSet);
  umlccharset32s.IncludeRange(LowerIDSet, umlcchar_32('a'), umlcchar_32('z'));
  umlccharset32s.IncludeRange(LowerIDSet, umlcchar_32('0'), umlcchar_32('9'));
  umlccharset32s.Include(LowerIDSet, umlcchar_32('_'));
  // LowerIDSet := ['a'..'z', '_', '0'..'9'];
end;

procedure UnitDestructor;
begin
  umlccharset32s.Clear(WildcardSet);
  umlccharset32s.Clear(IDSet);
  umlccharset32s.Clear(QIDSet);

  umlccharset32s.Clear(CurrSet);
  umlccharset32s.Clear(FloatSet);
  umlccharset32s.Clear(DecDigitSet);
  umlccharset32s.Clear(AlphaSet);

  umlccharset32s.Clear(LowerSet);
  umlccharset32s.Clear(UpperSet);

  umlccharset32s.Clear(BoolSet);

  umlccharset32s.Clear(NullSet);
  umlccharset32s.Clear(EoLnSet);
  umlccharset32s.Clear(BlanksSet);
end;

initialization
  UnitConstructor;
finalization
  UnitDestructor;
end.


