(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemchartypes;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement multiple size neutral encoded character types.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  dummy;

// ---

const

 MOD_umlcchartypes : TUMLCModule =
   ($23,$60,$8D,$75,$7E,$F6,$09,$49,$97,$53,$92,$F6,$BC,$E9,$C0,$FF);

// ---

const

// ---

  ID_umlcchar_8 : TUMLCType =
    ($87,$B5,$A1,$CA,$50,$E3,$E2,$40,$AE,$A8,$4A,$56,$0B,$1E,$BB,$95);

  ID_umlcchar_16 : TUMLCType =
    ($F3,$40,$03,$0B,$E3,$AA,$EC,$40,$81,$61,$E7,$8F,$3F,$5D,$61,$6F);

  ID_umlcchar_32 : TUMLCType =
    ($2C,$ED,$BF,$E3,$A5,$DA,$6B,$4E,$9E,$AA,$8C,$E9,$AA,$62,$DE,$D9);

  ID_umlcchar_64 : TUMLCType =
    ($DD,$0E,$98,$ED,$9F,$CE,$E3,$4B,$99,$3A,$FB,$99,$20,$46,$74,$5B);

// ---

const

// ---

  ID_TUMLCCharTypes : TUMLCType =
    ($DD,$A8,$A2,$DE,$67,$12,$6F,$48,$8C,$5A,$81,$E2,$9C,$71,$A6,$21);

const

// ---

  ID_umlctchar_8 : TUMLCType =
    ($87,$B5,$A1,$CA,$50,$E3,$E2,$40,$AE,$A8,$4A,$56,$0B,$1E,$BB,$95);

  ID_umlctchar_16 : TUMLCType =
    ($F3,$40,$03,$0B,$E3,$AA,$EC,$40,$81,$61,$E7,$8F,$3F,$5D,$61,$6F);

  ID_umlctchar_32 : TUMLCType =
    ($2C,$ED,$BF,$E3,$A5,$DA,$6B,$4E,$9E,$AA,$8C,$E9,$AA,$62,$DE,$D9);

  ID_umlctchar_64 : TUMLCType =
    ($DD,$0E,$98,$ED,$9F,$CE,$E3,$4B,$99,$3A,$FB,$99,$20,$46,$74,$5B);

// ---

const

// ---

  ID_umlcpchar_8 : TUMLCType =
    ($B2,$7D,$A6,$68,$FD,$CA,$9A,$46,$9D,$07,$86,$8D,$BA,$01,$15,$E1);

  ID_umlcpchar_16 : TUMLCType =
    ($39,$CB,$E0,$E0,$F1,$8E,$10,$41,$8F,$4E,$80,$DB,$03,$B0,$00,$DE);

  ID_umlcpchar_32 : TUMLCType =
    ($F9,$62,$04,$43,$81,$8E,$78,$4E,$A7,$5D,$C5,$2C,$B0,$F2,$3A,$22);

  ID_umlcpchar_64 : TUMLCType =
    ($FF,$7A,$98,$35,$42,$6D,$E9,$4B,$B8,$01,$F2,$07,$46,$63,$BC,$03);

// ---

const

// ---

  ID_umlccharset_8 : TUMLCType =
    ($70,$C2,$2C,$48,$EC,$2A,$8C,$47,$93,$C5,$CB,$C2,$F6,$F6,$60,$7D);

  ID_umlccharset_16 : TUMLCType =
    ($A4,$F0,$79,$9D,$4C,$45,$8F,$4A,$8C,$FE,$3D,$22,$0A,$BD,$27,$CC);

  ID_umlccharset_32 : TUMLCType =
    ($45,$67,$EA,$12,$9E,$71,$3B,$43,$AB,$F8,$10,$2C,$D3,$A9,$0C,$81);

  ID_umlccharset_64 : TUMLCType =
    ($D4,$76,$81,$F1,$E8,$90,$82,$44,$A0,$DA,$11,$F6,$F9,$BB,$18,$C3);

// ---

  ID_umlcbytecharset : TUMLCType =
    ($70,$C2,$2C,$48,$EC,$2A,$8C,$47,$93,$C5,$CB,$C2,$F6,$F6,$60,$7D);

  ID_umlcwordcharset : TUMLCType =
    ($A4,$F0,$79,$9D,$4C,$45,$8F,$4A,$8C,$FE,$3D,$22,$0A,$BD,$27,$CC);

  ID_umlcquadcharset : TUMLCType =
    ($45,$67,$EA,$12,$9E,$71,$3B,$43,$AB,$F8,$10,$2C,$D3,$A9,$0C,$81);

  ID_umlcoctacharset : TUMLCType =
    ($D4,$76,$81,$F1,$E8,$90,$82,$44,$A0,$DA,$11,$F6,$F9,$BB,$18,$C3);

// ---

(* global standard types *)

type

  umlcchar_8  =
    (* redefine *) type umlcmemint_8;  // 1 byte (s)
  umlcchar_16 =
    (* redefine *) type umlcmemint_16; // 2 byte (s)
  umlcchar_32 =
    (* redefine *) type umlcmemint_32; // 4 byte (s)
  umlcchar_64 =
    (* redefine *) type umlcmemint_64; // 8 byte (s)

(* global additional types *)

type

  TUMLCCharTypes = (* enum of *)
  (
    umlcchartypUnknown,
    umlcchartypChar_8,
    umlcchartypChar_16,
    umlcchartypChar_32,
    umlcchartypChar_64
  );

type

  umlctchar_8   = (* alias of *) umlcchar_8;
  umlctchar_16  = (* alias of *) umlcchar_16;
  umlctchar_32  = (* alias of *) umlcchar_32;
  umlctchar_64  = (* alias of *) umlcchar_64;

type

  umlcpchar_8   = (* alias of *) ^umlcchar_8;
  umlcpchar_16  = (* alias of *) ^umlcchar_16;
  umlcpchar_32  = (* alias of *) ^umlcchar_32;
  umlcpchar_64  = (* alias of *) ^umlcchar_64;

type

  umlcbytechar  = (* redefine *) type umlcmemint_8;  // byte;
  umlctbytechar = (* alias of *) umlcmemint_8;  // byte;
  umlcpbytechar = (* redefine *) type umlcpmemint_8; // pbyte;

  umlcwordchar  = (* redefine *) type umlcmemint_16;  // word;
  umlctwordchar = (* alias of *) umlcmemint_16;  // word;
  umlcpwordchar = (* redefine *) type umlcpmemint_16; // word;

  umlcquadchar  = (* redefine *) type umlcmemint_32;
  umlctquadchar = (* alias of *) umlcmemint_32;
  umlcpquadchar = (* redefine *) type umlcpmemint_32;

  umlcoctachar  = (* redefine *) type umlcmemint_64;
  umlctoctachar = (* alias of *) umlcmemint_64;
  umlcpoctachar = (* redefine *) type umlcpmemint_64;

const
  nullchar_8  = 0;
  nullchar_16 = 0;
  nullchar_32 = 0;
  nullchar_64 = 0;

(* global types *)

type

  // "Pascal" style neutral integer based encoding strings

  umlccharset_8  =
    (* redefine *) type array[0 .. 255] of umlcchar_8;  // 1 byte (s) * 256
  umlccharset_16 =
    (* redefine *) type array[0 .. 255] of umlcchar_16; // 2 byte (s) * 256
  umlccharset_32 =
    (* redefine *) type array[0 .. 255] of umlcchar_32; // 4 byte (s) * 256
  umlccharset_64 =
    (* redefine *) type array[0 .. 255] of umlcchar_64; // 8 byte (s) * 256

type
  umlcbytecharset = (* alias of *) umlccharset_8;
  umlcwordcharset = (* alias of *) umlccharset_16;
  umlcquadcharset = (* alias of *) umlccharset_32;
  umlcoctacharset = (* alias of *) umlccharset_64;

(* global functions *)

  function SizeInBytesOf
    (const ACharType: TUMLCCharTypes): Cardinal;

implementation

(* global functions *)

function SizeInBytesOf
  (const ACharType: TUMLCCharTypes): Cardinal;
begin
  //umlcmemtypUnknown:
  Result := 0;

  case ACharType of
    umlcchartypChar_8:
      Result := sizeof(umlcchar_8);
    umlcchartypChar_16:
      Result := sizeof(umlcchar_16);
    umlcchartypChar_32:
      Result := sizeof(umlcchar_32);
    umlcchartypChar_64:
      Result := sizeof(umlcchar_64);
  end;
end;

end.

