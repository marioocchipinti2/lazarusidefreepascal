unit umlccharsetconst64s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to implement commonly used character sets.
 ** Note:
 ** - Each elements is a 64 bits character.
 ** - The character encoding is neutral.
 ** - All characters have the same size.
 ** - Data is stored as an unsigned integer, not text.
 **************************************************************************
 **)

interface

uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  umlcchar64s,
  umlccharset64s,
  dummy;

// ---

const

 MOD_umlccharsetconst64s : TUMLCModule =
   ($1E,$3B,$B7,$3D,$0B,$8E,$B9,$47,$A5,$37,$45,$A7,$AB,$D5,$09,$DE);

// ---

var
  BlanksSet, EoLnSet, NullSet: umlccharset_64;
  BoolSet: umlccharset_64;
  UpperSet, LowerSet: umlccharset_64;
  AlphaSet, FloatSet, CurrSet: umlccharset_64;
  DecDigitSet, OctDigitSet, HexDigitSet, BinDigitSet: umlccharset_64;
  IDSet, QIDSet, WildcardSet: umlccharset_64;
  UpperIDSet, LowerIDSet: umlccharset_64;

implementation

procedure UnitConstructor;
begin
  umlccharset64s.Clear(BlanksSet);
  umlccharset64s.Include(BlanksSet, umlcchar_64(32));
  umlccharset64s.Include(BlanksSet, umlcchar_64(13));
  umlccharset64s.Include(BlanksSet, umlcchar_64(10));
  umlccharset64s.Include(BlanksSet, umlcchar_64(12));

  umlccharset64s.Clear(EoLnSet);
  umlccharset64s.Include(EoLnSet, umlcchar_64(13));
  umlccharset64s.Include(EoLnSet, umlcchar_64(10));

  umlccharset64s.Clear(NullSet);
  umlccharset64s.Include(NullSet, umlcchar_64(0));
  umlccharset64s.Include(NullSet, umlcchar_64(13));
  umlccharset64s.Include(NullSet, umlcchar_64(10));
  umlccharset64s.Include(NullSet, umlcchar_64(26));

  umlccharset64s.Clear(BoolSet);
  umlccharset64s.Include(BoolSet, umlcchar_64(0));
  umlccharset64s.Include(BoolSet, umlcchar_64('f'));
  umlccharset64s.Include(BoolSet, umlcchar_64('F'));
  // set of values used for FALSE representation
  // conjunto de valores utilizados para la representacion de FALSO

  umlccharset64s.Clear(UpperSet);
  umlccharset64s.IncludeRange(UpperSet, umlcchar_64('A'), umlcchar_64('Z'));
  // UpperSet  := ['A'..'Z'];

  umlccharset64s.Clear(LowerSet);
  umlccharset64s.IncludeRange(LowerSet, umlcchar_64('a'), umlcchar_64('z'));
  // LowerSet  := ['a'..'z'];

  umlccharset64s.Clear(AlphaSet);
  umlccharset64s.IncludeRange(AlphaSet, umlcchar_64('A'), umlcchar_64('Z'));
  umlccharset64s.IncludeRange(AlphaSet, umlcchar_64('a'), umlcchar_64('z'));
  // AlphaSet  := ['A'..'Z', 'a'..'z'];

  umlccharset64s.Clear(DecDigitSet);
  umlccharset64s.IncludeRange(DecDigitSet, umlcchar_64('0'), umlcchar_64('9'));
  // DecDigitSet  := ['0'..'9'];

  umlccharset64s.Clear(OctDigitSet);
  umlccharset64s.IncludeRange(OctDigitSet, umlcchar_64('0'), umlcchar_64('7'));
  // OctDigitSet  := ['0'..'7'];

  umlccharset64s.Clear(HexDigitSet);
  umlccharset64s.IncludeRange(HexDigitSet, umlcchar_64('0'), umlcchar_64('9'));
  umlccharset64s.IncludeRange(HexDigitSet, umlcchar_64('a'), umlcchar_64('f'));
  umlccharset64s.IncludeRange(HexDigitSet, umlcchar_64('A'), umlcchar_64('F'));
  // HexDigitSet  := ['0'..'9', 'a'..'f', 'A'..'F'];

  umlccharset64s.Clear(BinDigitSet);
  umlccharset64s.Include(BinDigitSet, umlcchar_64('0'));
  umlccharset64s.Include(BinDigitSet, umlcchar_64('1'));
  // BinDigitSet  := ['0', '1'];

  umlccharset64s.Clear(FloatSet);
  umlccharset64s.IncludeRange(FloatSet, umlcchar_64('0'), umlcchar_64('9'));
  umlccharset64s.Include(FloatSet, umlcchar_64('.'));
  umlccharset64s.Include(FloatSet, umlcchar_64(','));
  umlccharset64s.Include(FloatSet, umlcchar_64('+'));
  umlccharset64s.Include(FloatSet, umlcchar_64('-'));
  umlccharset64s.Include(FloatSet, umlcchar_64('E'));
  umlccharset64s.Include(FloatSet, umlcchar_64('e'));
  // FloatSet  := ['0'..'9', '.', ',', '+', '-', 'E', 'e'];

  umlccharset64s.Clear(CurrSet);
  umlccharset64s.IncludeRange(CurrSet, umlcchar_64('0'), umlcchar_64('9'));
  umlccharset64s.Include(CurrSet, umlcchar_64('.'));
  umlccharset64s.Include(CurrSet, umlcchar_64(','));
  // CurrSet   := ['0'..'9', '.', ','];

  umlccharset64s.Clear(IDSet);
  umlccharset64s.IncludeRange(IDSet, umlcchar_64('A'), umlcchar_64('Z'));
  umlccharset64s.IncludeRange(IDSet, umlcchar_64('a'), umlcchar_64('z'));
  umlccharset64s.IncludeRange(IDSet, umlcchar_64('0'), umlcchar_64('9'));
  umlccharset64s.Include(IDSet, umlcchar_64('_'));
  // IDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9'];

  umlccharset64s.Assign(WildcardSet, IDSet);
  umlccharset64s.Include(WildcardSet, umlcchar_64('*'));
  umlccharset64s.Include(WildcardSet, umlcchar_64('?'));
  // WildcardSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '*', '?'];

  umlccharset64s.Assign(QIDSet, IDSet);
  umlccharset64s.Include(QIDSet, umlcchar_64('.'));
  // QIDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '.'];

  umlccharset64s.Clear(UpperIDSet);
  umlccharset64s.IncludeRange(UpperIDSet, umlcchar_64('A'), umlcchar_64('Z'));
  umlccharset64s.IncludeRange(UpperIDSet, umlcchar_64('0'), umlcchar_64('9'));
  umlccharset64s.Include(UpperIDSet, umlcchar_64('_'));
  // UpperIDSet := ['A'..'Z', '_', '0'..'9'];

  umlccharset64s.Clear(LowerIDSet);
  umlccharset64s.IncludeRange(LowerIDSet, umlcchar_64('a'), umlcchar_64('z'));
  umlccharset64s.IncludeRange(LowerIDSet, umlcchar_64('0'), umlcchar_64('9'));
  umlccharset64s.Include(LowerIDSet, umlcchar_64('_'));
  // LowerIDSet := ['a'..'z', '_', '0'..'9'];
end;

procedure UnitDestructor;
begin
  umlccharset64s.Clear(WildcardSet);
  umlccharset64s.Clear(IDSet);
  umlccharset64s.Clear(QIDSet);

  umlccharset64s.Clear(CurrSet);
  umlccharset64s.Clear(FloatSet);
  umlccharset64s.Clear(DecDigitSet);
  umlccharset64s.Clear(AlphaSet);

  umlccharset64s.Clear(LowerSet);
  umlccharset64s.Clear(UpperSet);

  umlccharset64s.Clear(BoolSet);

  umlccharset64s.Clear(NullSet);
  umlccharset64s.Clear(EoLnSet);
  umlccharset64s.Clear(BlanksSet);
end;

initialization
  UnitConstructor;
finalization
  UnitDestructor;
end.


