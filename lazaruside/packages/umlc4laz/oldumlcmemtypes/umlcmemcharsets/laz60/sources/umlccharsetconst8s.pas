unit umlccharsetconst8s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to implement commonly used character sets.
 ** Note:
 ** - Each elements is a 8 bits character.
 ** - The character encoding is neutral.
 ** - All characters have the same size.
 ** - Data is stored as an unsigned integer, not text.
 **************************************************************************
 **)

interface

uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  umlcchar8s,
  umlccharset8s,
  dummy;

// ---

const

 MOD_umlccharsetconst8s : TUMLCModule =
   ($1C,$80,$4D,$16,$D5,$EB,$7C,$4E,$A4,$E9,$04,$FE,$4E,$08,$0A,$26);

// ---

var
  BlanksSet, EoLnSet, NullSet: umlccharset_8;
  BoolSet: umlccharset_8;
  UpperSet, LowerSet: umlccharset_8;
  AlphaSet, FloatSet, CurrSet: umlccharset_8;
  DecDigitSet, OctDigitSet, HexDigitSet, BinDigitSet: umlccharset_8;
  IDSet, QIDSet, WildcardSet: umlccharset_8;
  UpperIDSet, LowerIDSet: umlccharset_8;

implementation

procedure UnitConstructor;
begin
  umlccharset8s.Clear(BlanksSet);
  umlccharset8s.Include(BlanksSet, umlcchar_8(32));
  umlccharset8s.Include(BlanksSet, umlcchar_8(13));
  umlccharset8s.Include(BlanksSet, umlcchar_8(10));
  umlccharset8s.Include(BlanksSet, umlcchar_8(12));

  umlccharset8s.Clear(EoLnSet);
  umlccharset8s.Include(EoLnSet, umlcchar_8(13));
  umlccharset8s.Include(EoLnSet, umlcchar_8(10));

  umlccharset8s.Clear(NullSet);
  umlccharset8s.Include(NullSet, umlcchar_8(0));
  umlccharset8s.Include(NullSet, umlcchar_8(13));
  umlccharset8s.Include(NullSet, umlcchar_8(10));
  umlccharset8s.Include(NullSet, umlcchar_8(26));

  umlccharset8s.Clear(BoolSet);
  umlccharset8s.Include(BoolSet, umlcchar_8(0));
  umlccharset8s.Include(BoolSet, umlcchar_8('f'));
  umlccharset8s.Include(BoolSet, umlcchar_8('F'));
  // set of values used for FALSE representation
  // conjunto de valores utilizados para la representacion de FALSO

  umlccharset8s.Clear(UpperSet);
  umlccharset8s.IncludeRange(UpperSet, umlcchar_8('A'), umlcchar_8('Z'));
  // UpperSet  := ['A'..'Z'];

  umlccharset8s.Clear(LowerSet);
  umlccharset8s.IncludeRange(LowerSet, umlcchar_8('a'), umlcchar_8('z'));
  // LowerSet  := ['a'..'z'];

  umlccharset8s.Clear(AlphaSet);
  umlccharset8s.IncludeRange(AlphaSet, umlcchar_8('A'), umlcchar_8('Z'));
  umlccharset8s.IncludeRange(AlphaSet, umlcchar_8('a'), umlcchar_8('z'));
  // AlphaSet  := ['A'..'Z', 'a'..'z'];

  umlccharset8s.Clear(DecDigitSet);
  umlccharset8s.IncludeRange(DecDigitSet, umlcchar_8('0'), umlcchar_8('9'));
  // DecDigitSet  := ['0'..'9'];

  umlccharset8s.Clear(OctDigitSet);
  umlccharset8s.IncludeRange(OctDigitSet, umlcchar_8('0'), umlcchar_8('7'));
  // OctDigitSet  := ['0'..'7'];

  umlccharset8s.Clear(HexDigitSet);
  umlccharset8s.IncludeRange(HexDigitSet, umlcchar_8('0'), umlcchar_8('9'));
  umlccharset8s.IncludeRange(HexDigitSet, umlcchar_8('a'), umlcchar_8('f'));
  umlccharset8s.IncludeRange(HexDigitSet, umlcchar_8('A'), umlcchar_8('F'));
  // HexDigitSet  := ['0'..'9', 'a'..'f', 'A'..'F'];

  umlccharset8s.Clear(BinDigitSet);
  umlccharset8s.Include(BinDigitSet, umlcchar_8('0'));
  umlccharset8s.Include(BinDigitSet, umlcchar_8('1'));
  // BinDigitSet  := ['0', '1'];

  umlccharset8s.Clear(FloatSet);
  umlccharset8s.IncludeRange(FloatSet, umlcchar_8('0'), umlcchar_8('9'));
  umlccharset8s.Include(FloatSet, umlcchar_8('.'));
  umlccharset8s.Include(FloatSet, umlcchar_8(','));
  umlccharset8s.Include(FloatSet, umlcchar_8('+'));
  umlccharset8s.Include(FloatSet, umlcchar_8('-'));
  umlccharset8s.Include(FloatSet, umlcchar_8('E'));
  umlccharset8s.Include(FloatSet, umlcchar_8('e'));
  // FloatSet  := ['0'..'9', '.', ',', '+', '-', 'E', 'e'];

  umlccharset8s.Clear(CurrSet);
  umlccharset8s.IncludeRange(CurrSet, umlcchar_8('0'), umlcchar_8('9'));
  umlccharset8s.Include(CurrSet, umlcchar_8('.'));
  umlccharset8s.Include(CurrSet, umlcchar_8(','));
  // CurrSet   := ['0'..'9', '.', ','];

  umlccharset8s.Clear(IDSet);
  umlccharset8s.IncludeRange(IDSet, umlcchar_8('A'), umlcchar_8('Z'));
  umlccharset8s.IncludeRange(IDSet, umlcchar_8('a'), umlcchar_8('z'));
  umlccharset8s.IncludeRange(IDSet, umlcchar_8('0'), umlcchar_8('9'));
  umlccharset8s.Include(IDSet, umlcchar_8('_'));
  // IDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9'];

  umlccharset8s.Assign(WildcardSet, IDSet);
  umlccharset8s.Include(WildcardSet, umlcchar_8('*'));
  umlccharset8s.Include(WildcardSet, umlcchar_8('?'));
  // WildcardSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '*', '?'];

  umlccharset8s.Assign(QIDSet, IDSet);
  umlccharset8s.Include(QIDSet, umlcchar_8('.'));
  // QIDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '.'];

  umlccharset8s.Clear(UpperIDSet);
  umlccharset8s.IncludeRange(UpperIDSet, umlcchar_8('A'), umlcchar_8('Z'));
  umlccharset8s.IncludeRange(UpperIDSet, umlcchar_8('0'), umlcchar_8('9'));
  umlccharset8s.Include(UpperIDSet, umlcchar_8('_'));
  // UpperIDSet := ['A'..'Z', '_', '0'..'9'];

  umlccharset8s.Clear(LowerIDSet);
  umlccharset8s.IncludeRange(LowerIDSet, umlcchar_8('A'), umlcchar_8('Z'));
  umlccharset8s.IncludeRange(LowerIDSet, umlcchar_8('a'), umlcchar_8('z'));
  umlccharset8s.IncludeRange(LowerIDSet, umlcchar_8('0'), umlcchar_8('9'));
  umlccharset8s.Include(LowerIDSet, umlcchar_8('_'));
  // LowerIDSet := ['a'..'z', '_', '0'..'9'];
end;

procedure UnitDestructor;
begin
  umlccharset8s.Clear(WildcardSet);
  umlccharset8s.Clear(IDSet);
  umlccharset8s.Clear(QIDSet);

  umlccharset8s.Clear(CurrSet);
  umlccharset8s.Clear(FloatSet);
  umlccharset8s.Clear(DecDigitSet);
  umlccharset8s.Clear(AlphaSet);

  umlccharset8s.Clear(LowerSet);
  umlccharset8s.Clear(UpperSet);

  umlccharset8s.Clear(BoolSet);

  umlccharset8s.Clear(NullSet);
  umlccharset8s.Clear(EoLnSet);
  umlccharset8s.Clear(BlanksSet);
end;

initialization
  UnitConstructor;
finalization
  UnitDestructor;
end.


