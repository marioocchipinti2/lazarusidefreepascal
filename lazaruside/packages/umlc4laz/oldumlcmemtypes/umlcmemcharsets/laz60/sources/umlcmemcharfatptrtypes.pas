(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemcharfatptrtypes;

{$mode objfpc}{$H+}

interface

uses
  SysUtils, Classes, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  dummy;

// ---

const

 MOD_umlcmemcharfatptrtypes : TUMLCModule =
   ($88,$D1,$AB,$51,$AC,$A1,$8F,$40,$BC,$84,$B2,$05,$6D,$A8,$97,$AB);

// ---

type
  umlcmemcharfatptrheader_8 = record
    Ptr: umlcpchar_8;
    Idx: Word;
  end;
  umlcmemcharfatptr_8 = ^umlcmemcharfatptrheader_8;

  umlcmemcharfatptrheader_16 = record
    Ptr: umlcpchar_16;
    Idx: Word;
  end;
  umlcmemcharfatptr_16 = ^umlcmemcharfatptrheader_16;

  umlcmemcharfatptrheader_32 = record
    Ptr: umlcpchar_32;
    Idx: Word;
  end;
  umlcmemcharfatptr_32 = ^umlcmemcharfatptrheader_32;

  umlcmemcharfatptrheader_64 = record
    Ptr: umlcpchar_64;
    Idx: Word;
  end;
  umlcmemcharfatptr_64 = ^umlcmemcharfatptrheader_64;

// ---





implementation





end.

