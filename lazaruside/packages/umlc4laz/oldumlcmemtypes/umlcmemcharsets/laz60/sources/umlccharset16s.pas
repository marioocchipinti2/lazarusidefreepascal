(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccharset16s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement a "Pascal" style set of characters.
 ** Note:
 ** - Each elements is a 16 bits character.
 ** - The character encoding is neutral.
 ** - All characters have the same size.
 ** - Data is stored as an unsigned integer, not text.
 **************************************************************************
 **)

interface

uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  umlcchar16s,
  dummy;

// ---

const

  MOD_umlccharset_16s : TUMLCModule =
    ($07,$09,$66,$E4,$AB,$42,$64,$41,$B2,$58,$DA,$E2,$EF,$F2,$2C,$17);

// ---

(* global functions *)

function IsEmpty
  (const ASet: umlccharset_16): Boolean; overload;

function IsMember
  (const ASet: umlccharset_16; const AItem: umlcchar_16): Boolean; overload;

function IsSameMember
  (const ASet: umlccharset_16; const AItem: umlcchar_16): Boolean; overload;

function RangeToSetCopy
  (const ALo, AHi: umlcchar_16): umlccharset_16; overload;

(* global procedures *)

procedure Clear
  (out ASet: umlccharset_16); overload;

procedure Include
  (var ASet: umlccharset_16; const AItem: umlcchar_16); overload;
procedure Exclude
  (var ASet: umlccharset_16; const AItem: umlcchar_16); overload;

procedure IncludeRange
  (var ASet: umlccharset_16; const ALo, AHi: umlcchar_16); overload;
procedure ExcludeRange
  (var ASet: umlccharset_16; const ALo, AHi: umlcchar_16); overload;

(* global operators *)

procedure Assign
  (out   ADest:   umlccharset_16;
   const ASource: umlccharset_16); overload; // operator :=

implementation

(* global functions *)

function IsEmpty
  (const ASet: umlccharset_16): Boolean;
begin
  Result :=
    umlcmemint_16(ASet[0]) = 0;
end;

function IsMember
  (const ASet: umlccharset_16; const AItem: umlcchar_16): Boolean;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1;
    Count := System.Length(ASet);
  Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found := (ASet[Index] = AItem);
    Inc(Index);
  end;
  Result := Found;
end;

function IsSameMember
  (const ASet: umlccharset_16; const AItem: umlcchar_16): Boolean; overload;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found :=
      umlcchar16s.SameText
        (umlcchar16s.LowercaseCopy(ASet[Index]), umlcchar16s.LowercaseCopy(AItem));
    Inc(Index);
  end;
  Result := Found;
end;

function RangeToSetCopy
  (const ALo, AHi: umlcchar_16): umlccharset_16;
var AItem, L, H: umlcchar_16; ACount: Word;
begin
  umlccharset16s.Clear(Result);

  if (ALo > AHi)
    then L := AHi
    else L := ALo;
  if (ALo > AHi)
    then H := ALo
    else H := AHi;

  ACount := 0;
  for AItem := L to H do
  begin
    Result[Succ(ACount)] := AItem;
    Inc(ACount);
  end;

  Result[0] := ACount;
end;


(* global procedures *)

procedure Clear
  (out ASet: umlccharset_16);
begin
  System.FillChar(ASet, sizeof(ASet), #0);
end;

procedure Include
  (var ASet: umlccharset_16; const AItem: umlcchar_16);
var AIndex: Byte;
begin
  AIndex := ASet[0];
  ASet[Succ(AIndex)] := AItem;
  ASet[0] := Succ(AIndex);
end;

procedure Exclude
  (var ASet: umlccharset_16; const AItem: umlcchar_16);
var I, Count, Last: Byte; Temp: umlccharset_16;
begin
  umlccharset16s.Clear(Temp);
  Count :=
    ASet[0];
  Last := 0;
  for I := 0 to Pred(Count) do
  begin
    if (ASet[I] <> AItem) then
    begin
      Temp[Last] := AItem;
      Inc(Last);
    end;
  end;

  System.Move(Temp, ASet, sizeof(ASet));
  ASet[0] := Succ(Last);
end;

procedure IncludeRange
  (var ASet: umlccharset_16; const ALo, AHi: umlcchar_16);
var AItem, L, H: umlcchar_16;
begin
  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  for AItem := L to H do
  begin
    Include(ASet, AItem);
  end;
end;

procedure ExcludeRange
  (var ASet: umlccharset_16; const ALo, AHi: umlcchar_16);
var AItem, L, H: umlcchar_16;
begin
  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  for AItem := L to H do
  begin
    Exclude(ASet, AItem);
  end;
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlccharset_16;
   const ASource: umlccharset_16);
begin
  System.Move(ASource, ADest, sizeof(ADest));
end;


end.

