(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcoctacharsets;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement eight byte (s) character text based sets.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcmemchartypes,
  umlcoctachars,
  dummy;

// ---

const

 MOD_umlcoctacharsets : TUMLCModule =
   ($8B,$67,$DE,$D5,$51,$BB,$C8,$41,$84,$A3,$15,$1D,$3A,$53,$57,$86);

// ---

(* global constants *)

const
  MemberNotFound = -1;

(* global functions *)

function IsMember
  (AItem: umlcoctachar; const ASet: umlcoctacharset): Boolean; overload;
function IsEmpty
  (const ASet: umlcoctacharset): Boolean; overload;

function Range
  (const ALo, AHi: umlcoctachar): umlcoctacharset; overload;

(* global procedures *)

procedure Clear
  (out ADest: umlcoctacharset); overload;

procedure Include
  (var ASet: umlcoctacharset; AItem: umlcoctachar); overload;
procedure Exclude
  (var ASet: umlcoctacharset; AItem: umlcoctachar); overload;

procedure IncludeRange
  (var ASet: umlcoctacharset; Lo, Hi: umlcoctachar); overload;
procedure ExcludeRange
  (var ASet: umlcoctacharset; Lo, Hi: umlcoctachar); overload;

function ReplaceCharSetByChar
  (const Value: umlcoctachar; Source: umlcoctacharset; Dest: umlcoctachar): umlcoctachar; overload;

implementation

(* global functions *)

function InternalIndexOf
  (const AItem: umlcoctachar;
   const ASet:  umlcoctacharset): Cardinal;
begin
  Result := MemberNotFound;
end;

function InternalCount
  (const AValue: umlcoctacharset): Cardinal;
begin
  Result :=
    Math.Min(umlcoctabyte(AValue[0]), 255);
end;

function IsMember
  (AItem: umlcoctachar; const ASet: umlcoctacharset): Boolean;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found := (ASet[Index] = AItem);
    Inc(Index);
  end;
  Result := Found;
end;

function IsEmpty
  (const ASet: umlcoctacharset): Boolean;
begin
  Result :=
    umlcmemint_64(ASet[0]) = 0;
end;

function Range
  (const ALo, AHi: umlcoctachar): umlcoctacharset;
var AItem: umlcoctachar;
    Lo, Hi: umlcoctachar;
begin
  umlcoctacharsets.Clear(Result);

  Lo := umlcoctachars.IfChar((ALo > AHi), AHi, ALo);
  Hi := umlcoctachars.IfChar((ALo > AHi), ALo, AHi);

  AItem := Lo;
  while (AItem <= Hi) do
  begin
    umlcoctacharsets.Include
      (Result, AItem);
    Inc(AItem);
  end;
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcoctacharset);
begin
  System.FillByte
    (ADest, sizeof(ADest), 0);
end;

procedure Include
  (var ASet: umlcoctacharset; AItem: umlcoctachar);
var AIndex, AOffset: Cardinal;
begin
  AIndex :=
    umlcoctacharsets.InternalCount(ASet);
  AOffset :=
    (AIndex * sizeof(umlcoctachar));
  if (AIndex < (sizeof(ASet) - 2)) then
  begin
    System.Move
      (AItem, ASet[AOffset], sizeof(umlcoctachar));

    ASet[0] := umlcoctachar(AIndex + 1);
  end;
end;

procedure Exclude
  (var ASet: umlcoctacharset; AItem: umlcoctachar);
var I, Count, Last: Byte; Temp: umlcoctacharset;
begin
  umlcoctacharsets.Clear(Temp);
  Count :=
    ASet[0];
  Last := 0;
  for I := 0 to System.Pred(Count) do
  begin
    if (ASet[I] <> AItem) then
    begin
      Temp[Last] := AItem;
      Inc(Last);
    end;
  end;

  System.Move(Temp, ASet, sizeof(ASet));
  ASet[0] := System.Pred(Last);
end;

procedure IncludeRange
  (var ASet: umlcoctacharset; Lo, Hi: umlcoctachar);
var AItem, L, H: umlcoctachar;
begin
  L := IfChar((Lo > Hi), Hi, Lo);
  H := IfChar((Lo > Hi), Lo, Hi);

  AItem := Lo;
  while (AItem <= Hi) do
  begin
    Include(ASet, AItem);
    Inc(AItem);
  end;
end;

procedure ExcludeRange
  (var ASet: umlcoctacharset; Lo, Hi: umlcoctachar);
var AItem: umlcoctachar;
begin
  Lo := IfChar((Lo > Hi), Hi, Lo);
  Hi := IfChar((Lo > Hi), Lo, Hi);

  AItem := Lo;
  while (AItem <= Hi) do
  begin
    Exclude(ASet, AItem);
    Inc(AItem);
  end;
end;

function ReplaceCharSetByChar
 (const Value: umlcoctachar; Source: umlcoctacharset; Dest: umlcoctachar): umlcoctachar;
begin
  { to-do...}
  if (IsMember(Value, Source))
    then Result := Dest
    else Result := Value;
  // Goal: Replace a specific character set.
  // Objetivo: Reemplazar un conjunto caracter en especifico.
end;

end.
