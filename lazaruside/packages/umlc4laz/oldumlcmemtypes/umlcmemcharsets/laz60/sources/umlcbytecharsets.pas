(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbytecharsets;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement one single byte (s) character text based sets.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcmemchartypes,
  umlcbytechars,
  dummy;

// ---

const

 MOD_umlcbytecharsets : TUMLCModule =
   ($40,$38,$0F,$71,$86,$DB,$AD,$4E,$B4,$81,$E1,$8A,$30,$06,$9E,$BE);

// ---

(* global functions *)

function IsMember
  (AItem: umlcbytechar; const ASet: umlcbytecharset): Boolean; overload;
function IsEmpty
  (const ASet: umlcbytecharset): Boolean; overload;

function Range
  (const ALo, AHi: umlcbytechar): umlcbytecharset; overload;

(* global procedures *)

procedure Clear
  (out ADest: umlcbytecharset); overload;

procedure Include
  (var ASet: umlcbytecharset; AItem: umlcbytechar); overload;
procedure Exclude
  (var ASet: umlcbytecharset; AItem: umlcbytechar); overload;

procedure IncludeRange
  (var ASet: umlcbytecharset; Lo, Hi: umlcbytechar); overload;
procedure ExcludeRange
  (var ASet: umlcbytecharset; Lo, Hi: umlcbytechar); overload;

function ReplaceCharSetByChar
  (const Value: umlcbytechar; Source: umlcbytecharset; Dest: umlcbytechar): umlcbytechar; overload;

implementation

(* global functions *)

function InternalCount
  (const AValue: umlcbytecharset): Cardinal;
begin
  Result :=
    Math.Min(umlcbyte(AValue[0]), 255);
end;

function IsMember
  (AItem: umlcbytechar; const ASet: umlcbytecharset): Boolean;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found := (ASet[Index] = AItem);
    Inc(Index);
  end;
  Result := Found;
end;

function IsEmpty
  (const ASet: umlcbytecharset): Boolean;
begin
  Result :=
    umlcmemint_8(ASet[0]) = 0;
end;

function Range
  (const ALo, AHi: umlcbytechar): umlcbytecharset;
var AItem: umlcbytechar;
    Lo, Hi: umlcbytechar;
begin
  umlcbytecharsets.Clear(Result);

  Lo := umlcbytechars.IfChar((Lo > Hi), Hi, Lo);
  Hi := umlcbytechars.IfChar((Lo > Hi), Lo, Hi);

  for AItem := Lo to Hi do
  begin
    umlcbytecharsets.Include
      (Result, AItem);
  end;
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcbytecharset);
begin
  System.FillByte
    (ADest, sizeof(ADest), 0);
end;

procedure Include
  (var ASet: umlcbytecharset; AItem: umlcbytechar);
var AIndex, AOffset: Cardinal;
begin
  AIndex :=
    umlcbytecharsets.InternalCount(ASet);
  AOffset :=
    (AIndex * sizeof(umlcbytechar));
  if (AIndex < (sizeof(ASet) - 2)) then
  begin
    System.Move
      (AItem, ASet[AOffset], sizeof(umlcbytechar));

    ASet[0] := umlcbytechar(AIndex + 1);
  end;
end;

procedure Exclude
  (var ASet: umlcbytecharset; AItem: umlcbytechar);
var I, Count, Last: Byte; Temp: umlcbytecharset;
begin
  umlcbytecharsets.Clear(Temp);
  Count :=
    ASet[0];
  Last := 0;
  for I := 0 to Pred(Count) do
  begin
    if (ASet[I] <> AItem) then
    begin
      Temp[Last] := AItem;
      Inc(Last);
    end;
  end;

  System.Move(Temp, ASet, sizeof(ASet));
  ASet[0] := System.Pred(Last);
end;

procedure IncludeRange
  (var ASet: umlcbytecharset; Lo, Hi: umlcbytechar);
var AItem, L, H: umlcbytechar;
begin
  L := IfChar((Lo > Hi), Hi, Lo);
  H := IfChar((Lo > Hi), Lo, Hi);

  for AItem := L to H do
  begin
    Include(ASet, AItem);
  end;
end;

procedure ExcludeRange
  (var ASet: umlcbytecharset; Lo, Hi: umlcbytechar);
var AItem: umlcbytechar;
begin
  Lo := IfChar((Lo > Hi), Hi, Lo);
  Hi := IfChar((Lo > Hi), Lo, Hi);

  for AItem := Lo to Hi do
  begin
    Exclude(ASet, AItem);
  end;
end;

function ReplaceCharSetByChar
 (const Value: umlcbytechar; Source: umlcbytecharset; Dest: umlcbytechar): umlcbytechar;
begin
  { to-do...}
  if (IsMember(Value, Source))
    then Result := Dest
    else Result := Value;
  // Goal: Replace a specific character set.
  // Objetivo: Reemplazar un conjunto caracter en especifico.
end;

end.
