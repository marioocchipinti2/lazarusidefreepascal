{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcmemcharsets60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcbytechars, umlcbytecharsets, umlcchar8s, umlcchar16s, umlcchar32s, 
  umlcchar64s, umlccharset8s, umlccharset16s, umlccharset32s, umlccharset64s, 
  umlccharsetconst8s, umlccharsetconst16s, umlccharsetconst32s, 
  umlccharsetconst64s, umlcmemcharfatptrtypes, umlcmemcharsets_rtti, 
  umlcmemchartypes, umlcoctachars, umlcoctacharsets, umlcquadchars, 
  umlcquadcharsets, umlcwordchars, umlcwordcharsets;

implementation

end.
