unit umlcdialogsregister;

interface
uses
  Classes,
  {$IFDEF DELPHI}  
  DesignEditors,
  DesignIntf,
  {$ENDIF} 
  {$IFDEF FPC}  
  LResources,
  {$ENDIF}
  umlcaboutdlgs,
  //umlcsrchdlgs,
  umlcsrchtxtdlgs,
  umlcsrchflddlgs;

  procedure Register;

implementation

{$IFDEF DELPHI}
{$R 'umlcaboutdlgs.dcr'}
{$R 'umlcsrchtxtdlgs.dcr'}
{$R 'umlcsrchflddlgs.dcr'}
{$ENDIF} 

procedure Register;
const Palette  = 'UMLCat Dialogs';
begin
  RegisterComponents(Palette, [TUMLCAboutDialog]);

  RegisterComponents(Palette, [TUMLCSearchTextDialog]);
  RegisterComponents(Palette, [TUMLCReplaceTextDialog]);

  RegisterComponents(Palette, [TUMLCSearchFieldDialog]);
  RegisterComponents(Palette, [TUMLCReplaceFieldDialog]);
end;

initialization
{$IFDEF FPC}
{$I 'umlcaboutdlgs.lrs'}
{$I 'umlcsrchtxtdlgs.lrs'}
{$I 'umlcsrchflddlgs.lrs'}
{$ENDIF} 
end.
