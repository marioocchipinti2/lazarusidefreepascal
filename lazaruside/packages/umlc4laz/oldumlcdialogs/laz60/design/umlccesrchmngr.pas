unit umlccesrchmngr;

interface
uses
  SysUtils, Classes,
  Dialogs, Forms,
  resSrchTxtDlgs,
  umlcDlgCtrls,
  umlcSrchTxtDlgs,
  umlcSrchFldDlgs,
  frmSearchTextDialog,
  frmReplaceTextDialog,
  frmSearchFieldDialog,
  frmReplaceFieldDialog,
{$IFDEF VER140}
  DesignIntf, DesignEditors;
{$ELSE}
  DsgnIntf;
{$ENDIF}

type

{ TUMLCDialogComponentComponentEditor }

  TUMLCDialogComponentComponentEditor = class(TComponentEditor)
    function  Container: TUMLCDialogComponent; virtual; abstract;
  end;

{ TUMLCSearchTextDialogComponentEditor }

  TUMLCSearchTextDialogComponentEditor = class(TUMLCDialogComponentComponentEditor)
    function GetVerbCount: Integer; override;
    function GetVerb(Index: Integer): string; override;

    function Container: TUMLCDialogComponent; override;

    procedure ExecuteVerb(Index: Integer); override;
  end;

{ TUMLCSearchFieldDialogComponentEditor }

  TUMLCSearchFieldDialogComponentEditor = class(TUMLCDialogComponentComponentEditor)
    function GetVerbCount: Integer; override;
    function GetVerb(Index: Integer): string; override;

    function Container: TUMLCDialogComponent; override;

    procedure ExecuteVerb(Index: Integer); override;
  end;

{ TUMLCReplaceTextDialogComponentEditor }

  TUMLCReplaceTextDialogComponentEditor = class(TUMLCDialogComponentComponentEditor)
    function GetVerbCount: Integer; override;
    function GetVerb(Index: Integer): string; override;

    function Container: TUMLCDialogComponent; override;

    procedure ExecuteVerb(Index: Integer); override;
  end;

{ TUMLCReplaceFieldDialogComponentEditor }

  TUMLCReplaceFieldDialogComponentEditor = class(TUMLCDialogComponentComponentEditor)
    function GetVerbCount: Integer; override;
    function GetVerb(Index: Integer): string; override;

    function Container: TUMLCDialogComponent; override;

    procedure ExecuteVerb(Index: Integer); override;
  end;

  procedure Register;

implementation

procedure Register;
begin
  RegisterComponentEditor
    (TCustomUMLCSearchTextDialog, TUMLCSearchTextDialogComponentEditor);
  RegisterComponentEditor
    (TCustomUMLCReplaceTextDialog, TUMLCReplaceTextDialogComponentEditor);

  RegisterComponentEditor
    (TfrmSearchFieldDialog, TUMLCSearchFieldDialogComponentEditor);
  RegisterComponentEditor
    (TfrmReplaceFieldDialog, TUMLCReplaceFieldDialogComponentEditor);
end;

{ TUMLCSearchTextDialogComponentEditor }

function TUMLCSearchTextDialogComponentEditor.GetVerbCount: Integer;
begin
  Result := 1;
  { Goal: To get the number of menu options .}
  { Objetivo: Obtener el no. de opciones de menu .}
end;

function TUMLCSearchTextDialogComponentEditor.GetVerb(Index: Integer): string;
begin
  case Index of
     0: Result := resTestDialog;
  end;
  { Goal: To get the caption for a menu option .}
  { Objetivo: Obtener el titulo para una opcion de menu .}
end;

function TUMLCSearchTextDialogComponentEditor.Container: TUMLCDialogComponent;
begin
  Result := (Component as TCustomUMLCSearchTextDialog);
end;

procedure TUMLCSearchTextDialogComponentEditor.ExecuteVerb(Index: Integer);
begin
  vclfrmSearchTextDialog.Execute(Container as TCustomUMLCSearchTextDialog);
  { Goal: Execute the given option .}
  { Objetivo: Ejecutar la opcion indicada .}
end;

{ TUMLCSearchFieldDialogComponentEditor }

function TUMLCSearchFieldDialogComponentEditor.GetVerbCount: Integer;
begin
  Result := 1;
  { Goal: To get the number of menu options .}
  { Objetivo: Obtener el no. de opciones de menu .}
end;

function TUMLCSearchFieldDialogComponentEditor.GetVerb(Index: Integer): string;
begin
  case Index of
     0: Result := resTestDialog;
  end;
  { Goal: To get the caption for a menu option .}
  { Objetivo: Obtener el titulo para una opcion de menu .}
end;

function TUMLCSearchFieldDialogComponentEditor.Container: TUMLCDialogComponent;
begin
  Result := (Component as TUMLCDialogComponent);
end;

procedure TUMLCSearchFieldDialogComponentEditor.ExecuteVerb(Index: Integer);
var Form: TfrmSearchFieldDialog; AContainer: TCustomUMLCSearchFieldDialog;
begin
  AContainer := (Container as TCustomUMLCSearchFieldDialog);
  Application.CreateForm(TCustomUMLCSearchFieldDialog, Form);
     Form.Container  := AContainer;
     Form.FieldNames := AContainer.FieldNames;
     Form.FieldIndex := AContainer.FieldIndex;
     Form.ShowModal;
  Form.Release; Form := nil;

//  vclsdfrmSearchFileDialog.Execute(Container as TCustomSDSearchFileDialog);
  { Goal: Execute the given option .}
  { Objetivo: Ejecutar la opcion indicada .}
end;

{ TUMLCReplaceTextDialogComponentEditor }

function TUMLCReplaceTextDialogComponentEditor.GetVerbCount: Integer;
begin
  Result := 1;
  { Goal: To get the number of menu options .}
  { Objetivo: Obtener el no. de opciones de menu .}
end;

function TUMLCReplaceTextDialogComponentEditor.GetVerb(Index: Integer): string;
begin
  case Index of
     0: Result := resTestDialog;
  end;
  { Goal: To get the caption for a menu option .}
  { Objetivo: Obtener el titulo para una opcion de menu .}
end;

function TUMLCReplaceTextDialogComponentEditor.Container: TUMLCDialogComponent;
begin
  Result := (Component as TCustomUMLCReplaceTextDialog);
end;

procedure TUMLCReplaceTextDialogComponentEditor.ExecuteVerb(Index: Integer);
var Form: TfrmReplaceTextDialog; AContainer: TCustomUMLCReplaceTextDialog;
begin
  AContainer := (Container as TCustomUMLCReplaceTextDialog);
  Application.CreateForm(TCustomUMLCReplaceTextDialog, Form);
     Form.Container  := AContainer;
     Form.ShowModal;
  Form.Release; Form := nil;

//  vclsdfrmReplaceTextDialog.Execute(Container as TCustomUMLCReplaceTextDialog);
  { Goal: Execute the given option .}
  { Objetivo: Ejecutar la opcion indicada .}
end;

{ TUMLCReplaceFieldDialogComponentEditor }

function TUMLCReplaceFieldDialogComponentEditor.GetVerbCount: Integer;
begin
  Result := 1;
  { Goal: To get the number of menu options .}
  { Objetivo: Obtener el no. de opciones de menu .}
end;

function TUMLCReplaceFieldDialogComponentEditor.GetVerb(Index: Integer): string;
begin
  case Index of
     0: Result := resTestDialog;
  end;
  { Goal: To get the caption for a menu option .}
  { Objetivo: Obtener el titulo para una opcion de menu .}
end;

function TUMLCReplaceFieldDialogComponentEditor.Container: TUMLCDialogComponent;
begin
  Result := (Component as TCustomUMLCReplaceFieldDialog);
end;

procedure TUMLCReplaceFieldDialogComponentEditor.ExecuteVerb(Index: Integer);
var Form: TfrmReplaceFieldDialog; AContainer: TCustomUMLCReplaceFieldDialog;
begin
  AContainer := (Container as TCustomUMLCReplaceFieldDialog);
  Application.CreateForm(TCustomUMLCReplaceFieldDialog, Form);
     Form.Container  := AContainer;
     Form.FieldNames := AContainer.FieldNames;
     Form.FieldIndex := AContainer.FieldIndex;
     Form.ShowModal;
  Form.Release; Form := nil;

//  vclsdfrmReplaceFieldDialog.Execute(Container as TCustomUMLCReplaceFieldDialog);
  { Goal: Execute the given option .}
  { Objetivo: Ejecutar la opcion indicada .}
end;

end.
