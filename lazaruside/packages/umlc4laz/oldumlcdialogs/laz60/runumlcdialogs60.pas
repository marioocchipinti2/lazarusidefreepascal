{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit runumlcdialogs60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcsrchflddlgs, umlctransdlgs, umlcsrchtxtdlgs, umlcsrchdlgs, 
  umlcresinputkeytypevalue, umlcresabout, umlcoptiondlgs, 
  umlconlystringkeyvalueinputdlgs, umlcmsgdlgsmemos, umlcmsgdlgs, 
  umlcmsgdlgarrays, umlckeytypevalueinputdlgs, umlcinputdlgs, umlcdlgctrls, 
  umlcaboutdlgs, ufrmtransstrings, ufrmtransstr, ufrmsearchtextdialog, 
  ufrmsearchfielddialog, ufrmreplacetextdialog, ufrmreplacefielddialog, 
  ufrmoptiondlg, ufrmnamedescr, ufrmmsgmemo, ufrmmsgdlg, ufrminputtime, 
  ufrminputstringsplus, ufrminputstrings, ufrminputstr, ufrminputoptions, 
  ufrminputmemo, ufrminputkeyvalueoptions, ufrminputkeyvaluelist, 
  ufrminputkeyvalue, ufrminputkeytypevaluelistmany, ufrminputkeytypevaluelist, 
  ufrminputkeytypevalue, ufrminputint, ufrminputdate, ufrmabout;

implementation

end.
