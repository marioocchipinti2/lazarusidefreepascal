(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsrchdlgs;

interface
uses
  SysUtils, Classes,
  umlccomponents,
  umlcsrchtypes,
  umlcdlgctrls,
  dummy;

type

{ TCustomUMLCSearchContainer }

  TCustomUMLCSearchContainer = class(TUMLCDialogComponent)
  private
    (* Private declarations *)

    FOnHelpClick: TNotifyEvent;
  protected
    (* Protected declarations *)

    FSearchResult: TUMLCSearchResult;
    FBookmarks:    TList;
    FSearchHistory: TStrings;

    FSearchText:  string;

    FOptions: TUMLCSearchOptions;
    FStatus:  TUMLCSearchStatus;

    FDirection: TUMLCSearchDirection;
    FScope:     TUMLCSearchScope;
    FOrigin:    TUMLCSearchOrigin;
  protected
    (* Protected declarations *)

    function getBookmarks: TList; virtual;
    function getDirection: TUMLCSearchDirection; virtual;
    function getScope: TUMLCSearchScope; virtual;
    function getOrigin: TUMLCSearchOrigin; virtual;

    function getHistory: TStrings; virtual;
    function getResult: TUMLCSearchResult; virtual;
    function getText: string; virtual;

    function getShowHelp: Boolean; virtual;

    function getShowCaseSensitive: Boolean; virtual;
    function getShowWholeWordsOnly: Boolean; virtual;
    function getShowRegularExpressions: Boolean; virtual;

    function getWantCaseSensitive: Boolean; virtual;
    function getWantWholeWordsOnly: Boolean; virtual;
    function getWantRegularExpressions: Boolean; virtual;

    procedure setBookmarks(Value: TList); virtual;
    procedure setDirection(Value: TUMLCSearchDirection); virtual;
    procedure setScope(Value: TUMLCSearchScope); virtual;
    procedure setOrigin(Value: TUMLCSearchOrigin); virtual;

    procedure setHistory(const Value: TStrings); virtual;
    procedure setResult(Value: TUMLCSearchResult); virtual;
    procedure setText(Value: string); virtual;

    procedure setShowHelp(Value: Boolean); virtual;

    procedure setShowCaseSensitive(Value: Boolean); virtual;
    procedure setShowWholeWordsOnly(Value: Boolean); virtual;
    procedure setShowRegularExpressions(Value: Boolean); virtual;

    procedure setWantCaseSensitive(Value: Boolean); virtual;
    procedure setWantWholeWordsOnly(Value: Boolean); virtual;
    procedure setWantRegularExpressions(Value: Boolean); virtual;
  protected
    (* Protected declarations *)

    procedure DelegateOnHelpClick();
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
    (* Public declarations *)

    { Never Published declarations }

    property Bookmarks: TList
      read getBookmarks write setBookmarks;
    property SearchResult: TUMLCSearchResult
      read getResult write setResult;

    { UnPublished declarations }

    property SearchHistory: TStrings
      read getHistory write setHistory;
    property SearchText:  string
      read getText write setText;

    property Direction: TUMLCSearchDirection
      read getDirection write setDirection;
    property Scope: TUMLCSearchScope
      read getScope write setScope;
    property Origin: TUMLCSearchOrigin
      read getOrigin write setOrigin;

    property ShowHelp: Boolean
      read getShowHelp write setShowHelp;
    property ShowCaseSensitive: Boolean
      read getShowCaseSensitive write setShowCaseSensitive;
    property ShowWholeWordsOnly: Boolean
      read getShowWholeWordsOnly write setShowWholeWordsOnly;
    property ShowRegularExpressions: Boolean
      read getShowRegularExpressions write setShowRegularExpressions;

    property WantCaseSensitive: Boolean
      read getWantCaseSensitive write setWantCaseSensitive;
    property WantWholeWordsOnly: Boolean
      read getWantWholeWordsOnly write setWantWholeWordsOnly;
    property WantRegularExpressions: Boolean
      read getWantRegularExpressions write setWantRegularExpressions;

    property OnHelpClick: TNotifyEvent
      read FOnHelpClick write FOnHelpClick; //default nil;
  end;

{ TCustomUMLCReplaceContainer }

  TCustomUMLCReplaceContainer = class(TCustomUMLCSearchContainer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FReplaceText: string;
    FReplaceHistory: TStrings;

    function getReplaceHistory: TStrings; virtual;
    function getReplaceText: string; virtual;

    function getShowReplaceAll: Boolean; virtual;
    function getShowPromptOnReplace: Boolean; virtual;
    function getShowDeleteOnReplace: Boolean; virtual;
    function getShowKeepCapitalCase: Boolean; virtual;

    function getWantPromptOnReplace: Boolean; virtual;
    function getWantDeleteOnReplace: Boolean; virtual;
    function getWantKeepCapitalCase: Boolean; virtual;

    procedure setReplaceHistory(const Value: TStrings); virtual;
    procedure setReplaceText(Value: string); virtual;

    procedure setShowReplaceAll(Value: Boolean); virtual;
    procedure setShowPromptOnReplace(Value: Boolean); virtual;
    procedure setShowDeleteOnReplace(Value: Boolean); virtual;
    procedure setShowKeepCapitalCase(Value: Boolean); virtual;

    procedure setWantPromptOnReplace(Value: Boolean); virtual;
    procedure setWantDeleteOnReplace(Value: Boolean); virtual;
    procedure setWantKeepCapitalCase(Value: Boolean); virtual;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    { UnPublished declarations }

    property ReplaceHistory: TStrings
      read getReplaceHistory write setReplaceHistory;
    property ReplaceText: string
      read getReplaceText write setReplaceText;

    property ShowReplaceAll: Boolean
      read getShowReplaceAll write setShowReplaceAll;
    property ShowPromptOnReplace: Boolean
      read getShowPromptOnReplace write setShowPromptOnReplace;
    property ShowDeleteOnReplace: Boolean
      read getShowDeleteOnReplace write setShowDeleteOnReplace;
    property ShowKeepCapitalCase: Boolean
      read getShowKeepCapitalCase write setShowKeepCapitalCase;

    property WantPromptOnReplace: Boolean
      read getWantPromptOnReplace write setWantPromptOnReplace;
    property WantDeleteOnReplace: Boolean
      read getWantDeleteOnReplace write setWantDeleteOnReplace;
    property WantKeepCapitalCase: Boolean
      read getWantKeepCapitalCase write setWantKeepCapitalCase;
  end;

implementation

{ TCustomUMLCSearchContainer }

function TCustomUMLCSearchContainer.getBookmarks: TList;
begin
  Result := FBookmarks;
  { Goal: "Bookmarks" property get method .}
  { Objetivo: Metodo lectura propiedad "Bookmarks" .}
end;

function TCustomUMLCSearchContainer.getDirection: TUMLCSearchDirection;
begin
  Result := FDirection;
  { Goal: "Direction" property get method .}
  { Objetivo: Metodo lectura propiedad "Direction" .}
end;

function TCustomUMLCSearchContainer.getScope: TUMLCSearchScope;
begin
  Result := FScope;
  { Goal: "Scope" property get method .}
  { Objetivo: Metodo lectura propiedad "Scope" .}
end;

function TCustomUMLCSearchContainer.getOrigin: TUMLCSearchOrigin;
begin
  Result := FOrigin;
  { Goal: "Origin" property get method .}
  { Objetivo: Metodo lectura propiedad "Origin" .}
end;

function TCustomUMLCSearchContainer.getHistory: TStrings;
begin
  Result := FSearchHistory;
  { Goal: "SearchHistory" property get method .}
  { Objetivo: Metodo lectura propiedad "SearchHistory" .}
end;

function TCustomUMLCSearchContainer.getResult: TUMLCSearchResult;
begin
  Result := FSearchResult;
  { Goal: "SearchResult" property get method .}
  { Objetivo: Metodo lectura propiedad "SearchResult" .}
end;

function TCustomUMLCSearchContainer.getText: string;
begin
  Result := FSearchText;
  { Goal: "SearchText" property get method .}
  { Objetivo: Metodo lectura propiedad "SearchText" .}
end;

procedure TCustomUMLCSearchContainer.setText(Value: string);
begin
  FSearchText := Value;
  { Goal: "SearchText" property get method .}
  { Objetivo: Metodo lectura propiedad "SearchText" .}
end;


function TCustomUMLCSearchContainer.getShowHelp: Boolean;
begin
  Result := (sropShowHelp in FOptions);
  { Goal: "ShowHelp" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowHelp" .}
end;

function TCustomUMLCSearchContainer.getShowCaseSensitive: Boolean;
begin
  Result := (sropShowCaseSensitive in FOptions);
  { Goal: "ShowCaseSensitive" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowCaseSensitive" .}
end;

function TCustomUMLCSearchContainer.getShowWholeWordsOnly: Boolean;
begin
  Result := (sropShowWholeWordsOnly in FOptions);
  { Goal: "ShowWholeWordsOnly" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowWholeWordsOnly" .}
end;

function TCustomUMLCSearchContainer.getShowRegularExpressions: Boolean;
begin
  Result := (sropShowRegularExpressions in FOptions);
  { Goal: "ShowRegularExpressions" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowRegularExpressions" .}
end;

function TCustomUMLCSearchContainer.getWantCaseSensitive: Boolean;
begin
  Result := (srstWantCaseSensitive in FStatus);
  { Goal: "WantCaseSensitive" property get method .}
  { Objetivo: Metodo lectura propiedad "WantCaseSensitive" .}
end;

function TCustomUMLCSearchContainer.getWantWholeWordsOnly: Boolean;
begin
  Result := (srstWantWholeWordsOnly in FStatus);
  { Goal: "WantWholeWordsOnly" property get method .}
  { Objetivo: Metodo lectura propiedad "WantWholeWordsOnly" .}
end;

function TCustomUMLCSearchContainer.getWantRegularExpressions: Boolean;
begin
  Result := (srstWantRegularExpressions in FStatus);
  { Goal: "WantRegularExpressions" property get method .}
  { Objetivo: Metodo lectura propiedad "WantRegularExpressions" .}
end;

procedure TCustomUMLCSearchContainer.setBookmarks(Value: TList);
begin
  FBookmarks := Value;
  { Goal: "Bookmarks" property get method .}
  { Objetivo: Metodo lectura propiedad "Bookmarks" .}
end;

procedure TCustomUMLCSearchContainer.setDirection(Value: TUMLCSearchDirection);
begin
  FDirection := Value;
  { Goal: "Direction" property get method .}
  { Objetivo: Metodo lectura propiedad "Direction" .}
end;

procedure TCustomUMLCSearchContainer.setScope(Value: TUMLCSearchScope);
begin
  FScope := Value;
  { Goal: "Scope" property get method .}
  { Objetivo: Metodo lectura propiedad "Scope" .}
end;

procedure TCustomUMLCSearchContainer.setOrigin(Value: TUMLCSearchOrigin);
begin
  FOrigin := Value;
  { Goal: "Origin" property get method .}
  { Objetivo: Metodo lectura propiedad "Origin" .}
end;

procedure TCustomUMLCSearchContainer.setHistory(const Value: TStrings);
begin
  FSearchHistory := Value;
  { Goal: "SearchHistory" property get method .}
  { Objetivo: Metodo lectura propiedad "SearchHistory" .}
end;

procedure TCustomUMLCSearchContainer.setResult(Value: TUMLCSearchResult);
begin
  FSearchResult := Value;
  { Goal: "SearchResult" property get method .}
  { Objetivo: Metodo lectura propiedad "SearchResult" .}
end;

procedure TCustomUMLCSearchContainer.setShowHelp(Value: Boolean);
begin
  if (Value)
    then Include(FOptions, sropShowHelp)
    else Exclude(FOptions, sropShowHelp);
  { Goal: "ShowHelp" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowHelp" .}
end;

procedure TCustomUMLCSearchContainer.setShowCaseSensitive(Value: Boolean);
begin
  if (Value)
    then Include(FOptions, sropShowCaseSensitive)
    else Exclude(FOptions, sropShowCaseSensitive);
  { Goal: "ShowCaseSensitive" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowCaseSensitive" .}
end;

procedure TCustomUMLCSearchContainer.setShowWholeWordsOnly(Value: Boolean);
begin
  if (Value)
    then Include(FOptions, sropShowWholeWordsOnly)
    else Exclude(FOptions, sropShowWholeWordsOnly);
  { Goal: "ShowWholeWordsOnly" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowWholeWordsOnly" .}
end;

procedure TCustomUMLCSearchContainer.setShowRegularExpressions(Value: Boolean);
begin
  if (Value)
    then Include(FOptions, sropShowRegularExpressions)
    else Exclude(FOptions, sropShowRegularExpressions);
  { Goal: "ShowRegularExpressions" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowRegularExpressions" .}
end;

procedure TCustomUMLCSearchContainer.setWantCaseSensitive(Value: Boolean);
begin
  if (Value)
    then Include(FStatus, srstWantCaseSensitive)
    else Exclude(FStatus, srstWantCaseSensitive);
  { Goal: "WantCaseSensitive" property get method .}
  { Objetivo: Metodo lectura propiedad "WantCaseSensitive" .}
end;

procedure TCustomUMLCSearchContainer.setWantWholeWordsOnly(Value: Boolean);
begin
  if (Value)
    then Include(FStatus, srstWantWholeWordsOnly)
    else Exclude(FStatus, srstWantWholeWordsOnly);
  { Goal: "WantWholeWordsOnly" property get method .}
  { Objetivo: Metodo lectura propiedad "WantWholeWordsOnly" .}
end;

procedure TCustomUMLCSearchContainer.setWantRegularExpressions(Value: Boolean);
begin
  if (Value)
    then Include(FStatus, srstWantRegularExpressions)
    else Exclude(FStatus, srstWantRegularExpressions);
  { Goal: "WantRegularExpressions" property get method .}
  { Objetivo: Metodo lectura propiedad "WantRegularExpressions" .}
end;

constructor TCustomUMLCSearchContainer.Create(AOwner: TComponent);
begin
  inherited;
  // Call TComponent.Create;
  // Llamar TComponent.Create;

  FBookmarks := TList.Create;
  FSearchHistory := TStringList.Create;

  FSearchResult := srrNone;
  FSearchText := '';

  FOptions := [];
  FStatus  := [];

  Include(FOptions, sropShowHelp);
  Include(FOptions, sropShowCaseSensitive);
  Include(FOptions, sropShowWholeWordsOnly);
  Include(FOptions, sropShowRegularExpressions);

  FDirection := srdForward;
  FScope :=     srscpGlobal;
  FOrigin :=    sropFromCursor;
  // Clean the new properties at start
  // Limpiar las nuevas propiedades al inicio

  { Goal: To prepare the component .}
  { Objetivo: Preparar el componente .}
end;

destructor TCustomUMLCSearchContainer.Destroy;
begin
  FSearchHistory.Free;
  FBookmarks.Free;
  // Clean the new properties at finish
  // Limpiar las nuevas propiedades al terminar

  inherited Destroy;
  { Goal: To unprepare the component .}
  { Objetivo: Despreparar el componente .}
end;

procedure TCustomUMLCSearchContainer.DelegateOnHelpClick();
begin
  if (Assigned(FOnHelpClick))
    then FOnHelpClick(Self);
end;

{ TCustomUMLCReplaceContainer }

function TCustomUMLCReplaceContainer.getReplaceHistory: TStrings;
begin
  Result := FReplaceHistory;
  { Goal: "ReplaceHistory" property get method .}
  { Objetivo: Metodo lectura propiedad "ReplaceHistory" .}
end;

function TCustomUMLCReplaceContainer.getReplaceText: string;
begin
  Result := FReplaceText;
  { Goal: "ReplaceText" property get method .}
  { Objetivo: Metodo lectura propiedad "ReplaceText" .}
end;

function TCustomUMLCReplaceContainer.getShowReplaceAll: Boolean;
begin
  Result := (sropShowReplaceAll in FOptions);
  { Goal: "ShowReplaceAll" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowReplaceAll" .}
end;

function TCustomUMLCReplaceContainer.getShowPromptOnReplace: Boolean;
begin
  Result := (sropShowPromptOnReplace in FOptions);
  { Goal: "ShowPromptOnReplace" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowPromptOnReplace" .}
end;

function TCustomUMLCReplaceContainer.getShowDeleteOnReplace: Boolean;
begin
  Result := (sropShowDeleteOnReplace in FOptions);
  { Goal: "ShowDeleteOnReplace" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowDeleteOnReplace" .}
end;

function TCustomUMLCReplaceContainer.getShowKeepCapitalCase: Boolean;
begin
  Result := (sropShowKeepCapitalCase in FOptions);
  { Goal: "ShowKeepCapitalCase" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowKeepCapitalCase" .}
end;

function TCustomUMLCReplaceContainer.getWantPromptOnReplace: Boolean;
begin
  Result := (srstWantPromptOnReplace in FStatus);
  { Goal: "WantPromptOnReplace" property get method .}
  { Objetivo: Metodo lectura propiedad "WantPromptOnReplace" .}
end;

function TCustomUMLCReplaceContainer.getWantDeleteOnReplace: Boolean;
begin
  Result := (srstWantDeleteOnReplace in FStatus);
  { Goal: "WantDeleteOnReplace" property get method .}
  { Objetivo: Metodo lectura propiedad "WantDeleteOnReplace" .}
end;

function TCustomUMLCReplaceContainer.getWantKeepCapitalCase: Boolean;
begin
  Result := (srstWantKeepCapitalCase in FStatus);
  { Goal: "WantKeepCapitalCase" property get method .}
  { Objetivo: Metodo lectura propiedad "WantKeepCapitalCase" .}
end;

procedure TCustomUMLCReplaceContainer.setReplaceHistory(const Value: TStrings);
begin
  FReplaceHistory := Value;
  { Goal: "ReplaceHistory" property get method .}
  { Objetivo: Metodo lectura propiedad "ReplaceHistory" .}
end;

procedure TCustomUMLCReplaceContainer.setReplaceText(Value: string);
begin
  FReplaceText := Value;
  { Goal: "ReplaceText" property get method .}
  { Objetivo: Metodo lectura propiedad "ReplaceText" .}
end;

procedure TCustomUMLCReplaceContainer.setShowReplaceAll(Value: Boolean);
begin
  if (Value)
    then Include(FOptions, sropShowReplaceAll)
    else Exclude(FOptions, sropShowReplaceAll);
  { Goal: "ShowReplaceAll" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowReplaceAll" .}
end;

procedure TCustomUMLCReplaceContainer.setShowPromptOnReplace(Value: Boolean);
begin
  if (Value)
    then Include(FOptions, sropShowPromptOnReplace)
    else Exclude(FOptions, sropShowPromptOnReplace);
  { Goal: "ShowPromptOnReplace" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowPromptOnReplace" .}
end;

procedure TCustomUMLCReplaceContainer.setShowDeleteOnReplace(Value: Boolean);
begin
  if (Value)
    then Include(FOptions, sropShowDeleteOnReplace)
    else Exclude(FOptions, sropShowDeleteOnReplace);
  { Goal: "ShowDeleteOnReplace" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowDeleteOnReplace" .}
end;

procedure TCustomUMLCReplaceContainer.setShowKeepCapitalCase(Value: Boolean);
begin
  if (Value)
    then Include(FOptions, sropShowKeepCapitalCase)
    else Exclude(FOptions, sropShowKeepCapitalCase);
  { Goal: "ShowKeepCapitalCase" property get method .}
  { Objetivo: Metodo lectura propiedad "ShowKeepCapitalCase" .}
end;

procedure TCustomUMLCReplaceContainer.setWantPromptOnReplace(Value: Boolean);
begin
  if (Value)
    then Include(FStatus, srstWantPromptOnReplace)
    else Exclude(FStatus, srstWantPromptOnReplace);
  { Goal: "WantPromptOnReplace" property get method .}
  { Objetivo: Metodo lectura propiedad "WantPromptOnReplace" .}
end;

procedure TCustomUMLCReplaceContainer.setWantDeleteOnReplace(Value: Boolean);
begin
  if (Value)
    then Include(FStatus, srstWantDeleteOnReplace)
    else Exclude(FStatus, srstWantDeleteOnReplace);
  { Goal: "WantDeleteOnReplace" property get method .}
  { Objetivo: Metodo lectura propiedad "WantDeleteOnReplace" .}
end;

procedure TCustomUMLCReplaceContainer.setWantKeepCapitalCase(Value: Boolean);
begin
  if (Value)
    then Include(FStatus, srstWantKeepCapitalCase)
    else Exclude(FStatus, srstWantKeepCapitalCase);
  { Goal: "WantKeepCapitalCase" property get method .}
  { Objetivo: Metodo lectura propiedad "WantKeepCapitalCase" .}
end;

constructor TCustomUMLCReplaceContainer.Create(AOwner: TComponent);
begin
  inherited;
  // Call TComponent.Create;
  // Llamar TComponent.Create;

  Include(FOptions, sropShowReplaceAll);

  Include(FOptions, sropShowPromptOnReplace);
  Include(FOptions, sropShowDeleteOnReplace);
  Include(FOptions, sropShowKeepCapitalCase);
  
  FReplaceHistory := TStringList.Create;
  FReplaceText := '';
  // Clean the new properties at start
  // Limpiar las nuevas propiedades al inicio

  { Goal: To prepare the component .}
  { Objetivo: Preparar el componente .}
end;

destructor TCustomUMLCReplaceContainer.Destroy;
begin
  FReplaceHistory.Free;
  // Clean the new propierties at finish
  // Limpiar las nuevas propiedades al terminar

  inherited Destroy;
  { Goal: To unprepare the component .}
  { Objetivo: Despreparar el componente .}
end;

end.



