(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsrchtxtdlgs;

interface
uses
  SysUtils, Classes,
{$IFDEF MSWINDOWS}
  Graphics, Controls, Forms, Dialogs,
{$ENDIF}
  umlcsrchtypes,
  umlcdlgctrls,
  umlcsrchdlgs,
  dummy;

(**
 ** Description:
 ** This unit declares collections of treenodes,
 ** that store Simple X.M.L. parsed tokens.
 **)

type

(* TCustomUMLCSearchTextDialog *)

  TCustomUMLCSearchTextDialog = class(TCustomUMLCSearchContainer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)
  public
    (* Public declarations *)

    function CreateDialog(): TCustomForm; override;

    (* Never Published declarations *)

    (* UnPublished declarations *)
  end;

(* TCustomUMLCReplaceTextDialog *)

  TCustomUMLCReplaceTextDialog = class(TCustomUMLCReplaceContainer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)
  public
    (* Public declarations *)

    function CreateDialog(): TCustomForm; override;

    (* Never Published declarations *)

    (* UnPublished declarations *)
  end;

{ TUMLCSearchTextDialog }

  TUMLCSearchTextDialog = class(TCustomUMLCSearchTextDialog)
  published
    (* Published declarations *)

    (* TDialogComponent: *)

    property HelpContext;
    property Title;

    property BeforeExecute;
    property AfterExecute;

    { TCustomUMLCSearchContainer: }

    property SearchText;

    property Direction;
    property Scope;
    property Origin;

    property ShowHelp;
    property ShowCaseSensitive;
    property ShowWholeWordsOnly;
    property ShowRegularExpressions;

    property WantCaseSensitive;
    property WantWholeWordsOnly;
    property WantRegularExpressions;

    property OnHelpClick;
  end;

{ TUMLCReplaceTextDialog }

  TUMLCReplaceTextDialog = class(TCustomUMLCReplaceTextDialog)
  published
    (* Published declarations *)

    (* TDialogComponent: *)

    property HelpContext;
    property Title;

    property BeforeExecute;
    property AfterExecute;

    { TCustomUMLCSearchContainer: }

    property SearchHistory;    
    property SearchText;

    property Direction;
    property Scope;
    property Origin;

    property ShowHelp;
    property ShowCaseSensitive;
    property ShowWholeWordsOnly;
    property ShowRegularExpressions;

    property WantCaseSensitive;
    property WantWholeWordsOnly;
    property WantRegularExpressions;

    { TCustomUMLCReplaceContainer: }

    property ReplaceHistory;
    property ReplaceText;

    property ShowReplaceAll;
    property ShowPromptOnReplace;
    property ShowDeleteOnReplace;
    property ShowKeepCapitalCase;

    property WantPromptOnReplace;
    property WantDeleteOnReplace;
    property WantKeepCapitalCase;    

    property OnHelpClick;
  end;

implementation
{$ifdef FPC}
uses
  ufrmsearchtextdialog,
  ufrmreplacetextdialog;
{$else}
uses
  vclfrmsearchtextdialog,
  vclfrmreplacetextdialog;
{$endif}

(* TCustomUMLCSearchTextDialog *)

function TCustomUMLCSearchTextDialog.CreateDialog(): TCustomForm;
begin
  Application.CreateForm(TfrmSearchTextDialog, FForm);
  Application.ProcessMessages();

  // --> assign some initial properties to the form,
  // --> before showing
  (FForm as TfrmSearchTextDialog).Container := Self;

  // --> ready
  Result := FForm;
  { Goal: To create the dialog box.}
  { Objetivo Construir la caja dialogo .}
end;

(* TCustomUMLCReplaceTextDialog *)

function TCustomUMLCReplaceTextDialog.CreateDialog(): TCustomForm;
begin
  Application.CreateForm(TfrmReplaceTextDialog, FForm);
  Application.ProcessMessages();

  // --> assign some initial properties to the form,
  // --> before showing
  (FForm as TfrmReplaceTextDialog).Container := Self;

  // --> ready
  Result := FForm;
  { Goal: To create the dialog box.}
  { Objetivo Construir la caja dialogo .}
end;

end.
