(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsrchflddlgs;

interface
uses
  SysUtils, Classes,
{$IFDEF MSWINDOWS}
  Graphics, Controls,
{$ENDIF}
  Forms, Dialogs,
  umlcsrchtypes,
  umlcdlgctrls,
  umlcsrchdlgs,
  dummy;

type

{ TCustomUMLCSearchFieldDialog }

  TCustomUMLCSearchFieldDialog = class(TCustomUMLCSearchContainer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FFieldNames: TStrings;
    FFieldIndex: Integer;
  protected
    (* Protected declarations *)

    function getFieldNames: TStrings; virtual;
    function getFieldIndex: Integer; virtual;

    procedure setFieldNames(Value: TStrings); virtual;
    procedure setFieldIndex(Value: Integer); virtual;
  public
    (* Public declarations *)

    function CreateDialog(): TCustomForm; override;
    procedure DestroyDialog(); override;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
    (* Public declarations *)

    (* UnPublished declarations *)

    property FieldNames: TStrings
      read getFieldNames write setFieldNames;
    property FieldIndex: Integer
      read getFieldIndex write setFieldIndex;
  end;

{ TCustomUMLCReplaceFieldDialog }

  TCustomUMLCReplaceFieldDialog = class(TCustomUMLCReplaceContainer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FFieldNames: TStrings;
    FFieldIndex: Integer;
  protected
    (* Protected declarations *)

    function getFieldNames: TStrings; virtual;
    function getFieldIndex: Integer; virtual;

    procedure setFieldNames(Value: TStrings); virtual;
    procedure setFieldIndex(Value: Integer); virtual;
  public
    (* Public declarations *)

    function CreateDialog(): TCustomForm; override;
    procedure DestroyDialog(); override;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    (* Never Published declarations *)

    (* UnPublished declarations *)

    property FieldNames: TStrings
      read getFieldNames write setFieldNames;
    property FieldIndex: Integer
      read getFieldIndex write setFieldIndex;
  end;

(* TUMLCSearchFieldDialog *)

  TUMLCSearchFieldDialog = class(TCustomUMLCSearchFieldDialog)
  published
    (* Published declarations *)

    (* TDialogComponent: *)

    property HelpContext;
    property Title;

    property BeforeExecute;
    property AfterExecute;

    (* TCustomUMLCSearchContainer: *)

    property SearchText;

    property Direction;
    property Scope;
    property Origin;

    property ShowHelp;
    property ShowCaseSensitive;
    property ShowWholeWordsOnly;
    property ShowRegularExpressions;

    property WantCaseSensitive;
    property WantWholeWordsOnly;
    property WantRegularExpressions;

    property OnHelpClick;

    { TCustomUMLCSearchFieldDialog: }

    property FieldNames;
    property FieldIndex;
  end;

(* TUMLCReplaceFieldDialog *)

  TUMLCReplaceFieldDialog = class(TCustomUMLCReplaceFieldDialog)
  published
    (* Published declarations *)

    (* TDialogComponent: *)

    property HelpContext;
    property Title;

    property BeforeExecute;
    property AfterExecute;

    (* TCustomUMLCSearchContainer: *)

    property SearchHistory;    
    property SearchText;

    property Direction;
    property Scope;
    property Origin;

    property ShowHelp;
    property ShowCaseSensitive;
    property ShowWholeWordsOnly;
    property ShowRegularExpressions;

    property WantCaseSensitive;
    property WantWholeWordsOnly;
    property WantRegularExpressions;

    { TCustomUMLCReplaceContainer: }

    property ReplaceHistory;
    property ReplaceText;

    property ShowReplaceAll;
    property ShowPromptOnReplace;
    property ShowDeleteOnReplace;
    property ShowKeepCapitalCase;

    property WantPromptOnReplace;
    property WantDeleteOnReplace;
    property WantKeepCapitalCase;    

    property OnHelpClick;

    { TCustomUMLCReplaceFieldDialog: }

    property FieldNames;
    property FieldIndex;
  end;

implementation
{$ifdef FPC}
uses
  ufrmsearchfielddialog,
  ufrmreplacefielddialog;
{$else}
uses
  vclfrmsearchfielddialog,
  vclfrmreplacefielddialog;
{$endif}

{ TCustomUMLCSearchFieldDialog }

function TCustomUMLCSearchFieldDialog.getFieldNames: TStrings;
begin
  Result := FFieldNames;
  { Goal: "FieldNames" property get method .}
  { Objetivo: Metodo lectura propiedad "FieldNames" .}
end;

function TCustomUMLCSearchFieldDialog.getFieldIndex: Integer;
begin
  Result := FFieldIndex;
  { Goal: "FieldIndex" property get method .}
  { Objetivo: Metodo lectura propiedad "FieldIndex" .}
end;

procedure TCustomUMLCSearchFieldDialog.setFieldNames(Value: TStrings);
begin
  FFieldNames.Assign(Value);
  { Goal: "FieldNames" property get method .}
  { Objetivo: Metodo lectura propiedad "FieldNames" .}
end;

procedure TCustomUMLCSearchFieldDialog.setFieldIndex(Value: Integer);
begin
  FFieldIndex := Value;
  { Goal: "FieldIndex" property get method .}
  { Objetivo: Metodo lectura propiedad "FieldIndex" .}
end;

function TCustomUMLCSearchFieldDialog.CreateDialog(): TCustomForm;
var F: TfrmSearchFieldDialog;
begin
  Application.CreateForm(TfrmSearchFieldDialog, FForm);
  Application.ProcessMessages;

  // --> assign some initial properties to the form,
  // --> before showing
  F := (FForm as TfrmSearchFieldDialog);
  F.Container := Self;

  if (FFieldNames.Count > 0) and (FFieldIndex = -1)
    then FieldIndex := 0;

  Application.ProcessMessages;
  F.FieldNames := FFieldNames;
  F.FieldIndex := FieldIndex;

  // --> ready
  Result := FForm;
  { Goal: To create the dialog box.}
  { Objetivo Construir la caja dialogo .}
end;

procedure TCustomUMLCSearchFieldDialog.DestroyDialog();
begin
  // --> recover some properties from the form,
  // --> before releasing the form from memory
  FieldIndex := (FForm as TfrmSearchFieldDialog).FieldIndex;

  // --> done
  inherited DestroyDialog();
  { Goal: To destroy the dialog box.}
  { Objetivo Destruir la cajadialogo .}
end;

constructor TCustomUMLCSearchFieldDialog.Create(AOwner: TComponent);
begin
  inherited;
  // Call TComponent.Create;
  // Llamar TComponent.Create;

  FFieldNames := TStringList.Create;
  FFieldIndex := -1;  
end;

destructor TCustomUMLCSearchFieldDialog.Destroy;
begin
  FFieldNames.Free;
  // Clean the new properties at finish
  // Limpiar las nuevas propiedades al terminar

  inherited Destroy;
  { Goal: To unprepare the component .}
  { Objetivo: Despreparar el componente .}
end;

{ TCustomUMLCReplaceFieldDialog }

function TCustomUMLCReplaceFieldDialog.getFieldNames: TStrings;
begin
  Result := FFieldNames;
  { Goal: "FieldNames" property get method .}
  { Objetivo: Metodo lectura propiedad "FieldNames" .}
end;

function TCustomUMLCReplaceFieldDialog.getFieldIndex: Integer;
begin
  Result := FFieldIndex;
  { Goal: "FieldIndex" property get method .}
  { Objetivo: Metodo lectura propiedad "FieldIndex" .}
end;

procedure TCustomUMLCReplaceFieldDialog.setFieldNames(Value: TStrings);
begin
  FFieldNames.Assign(Value);
  { Goal: "FieldNames" property get method .}
  { Objetivo: Metodo lectura propiedad "FieldNames" .}
end;

procedure TCustomUMLCReplaceFieldDialog.setFieldIndex(Value: Integer);
begin
  FFieldIndex := Value;
  { Goal: "FieldIndex" property get method .}
  { Objetivo: Metodo lectura propiedad "FieldIndex" .}
end;

function TCustomUMLCReplaceFieldDialog.CreateDialog(): TCustomForm;
var F: TfrmReplaceFieldDialog;
begin
  Application.CreateForm(TfrmReplaceFieldDialog, FForm);
  Application.ProcessMessages;

  // --> assign some initial properties to the form,
  // --> before showing
  F := (FForm as TfrmReplaceFieldDialog);
  F.Container := Self;

  if (FFieldNames.Count > 0) and (FFieldIndex = -1)
    then FieldIndex := 0;

  Application.ProcessMessages;
  F.FieldNames := FFieldNames;
  F.FieldIndex := FieldIndex;

  // --> ready
  Result := FForm;
  { Goal: To create the dialog box.}
  { Objetivo Construir la caja dialogo .}
end;

procedure TCustomUMLCReplaceFieldDialog.DestroyDialog();
begin
  // --> recover some properties from the form,
  // --> before releasing the form from memory
  FieldIndex := (FForm as TfrmReplaceFieldDialog).FieldIndex;

  // --> done
  inherited DestroyDialog();
  { Goal: To destroy the dialog box.}
  { Objetivo Destruir la cajadialogo .}
end;

constructor TCustomUMLCReplaceFieldDialog.Create(AOwner: TComponent);
begin
  inherited;
  // Call TComponent.Create;
  // Llamar TComponent.Create;

  FFieldNames := TStringList.Create();
  FFieldIndex := -1;  
end;

destructor TCustomUMLCReplaceFieldDialog.Destroy();
begin
  FFieldNames.Free();
  // Clean the new properties at finish
  // Limpiar las nuevas propiedades al terminar

  inherited Destroy();
  { Goal: To unprepare the component .}
  { Objetivo: Despreparar el componente .}
end;

end.
