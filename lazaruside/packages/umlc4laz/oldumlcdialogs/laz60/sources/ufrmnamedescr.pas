(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit ufrmnamedescr;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls,
  {$IFDEF FPC}
  LResources, StdCtrls,
  {$ENDIF}
  umlcmsgdlgtypes,
  umlcmsgdlgstrs,
  umlcmsgdlgarrays,
  umlcstrings,
  dummy;

resourcestring
  reslblName_Caption  = 'Name:';
  reslblDescr_Caption = 'Description:';

type

  { Tfrmnamedescr }

  Tfrmnamedescr = class(TForm)
    btnCancel: TButton;
    btnOK: TButton;
    edName: TEdit;
    lblName: TLabel;
    lblDescr: TLabel;
    mmDescr: TMemo;
    sbStatusBar: TStatusBar;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

    Option:  TMsgDlgButton;
  end;

    function InputNameDescr
      (const ATitle: string; var AName, ADescr: string): Boolean;

var
  frmnamedescr: Tfrmnamedescr;

implementation

{$R *.lfm}

function InputNameDescr
  (const ATitle: string; var AName, ADescr: string): Boolean;
begin
  with Tfrmnamedescr.Create(Application) do
  begin
    try
      Option  := mbCancel;

      Caption := ATitle;

      edName.Text := umlcstrings.TrimCopy(AName);
      mmDescr.Text := umlcstrings.TrimCopy(ADescr);

      ShowModal();
    finally
      Result := (Option = mbOK);

      if (Result) then
      begin
        AName := umlcstrings.TrimCopy(edName.Text);
        ADescr := umlcstrings.TrimCopy(mmDescr.Text);
      end;

      Free();
    end;
  end;
end;

{ Tfrmnamedescr }

procedure Tfrmnamedescr.FormCreate(Sender: TObject);
begin
  lblName.Caption  := reslblName_Caption;
  lblDescr.Caption := reslblDescr_Caption;

  btnOK.Caption     := ButtonsCaptionsArray[mbOK];
  btnCancel.Caption := ButtonsCaptionsArray[mbCancel];
end;

procedure Tfrmnamedescr.btnOKClick(Sender: TObject);
begin
  Option := mbOK;
end;

procedure Tfrmnamedescr.btnCancelClick(Sender: TObject);
begin
  Option := mbCancel;
end;

initialization
  {$IFDEF FPC}
  {$I 'ufrmnamedescr.lrs'}
  {$ENDIF}

end.

