rem echo "Hello World"
rem @ECHO OFF

set lazarusidepath=%lazarusidepath%ide\lazaruside



cd "%lazarusidepath%\packages\umlc4laz\umlcdialogs\laz60\resources\palette\umlcaboutdlgs"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcdialogs\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcdialogs\laz60\resources\palette\umlcsrchflddlgs"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcdialogs\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcdialogs\laz60\resources\palette\umlcsrchtxtdlgs"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcdialogs\laz60\design\*.lrs"


PAUSE
