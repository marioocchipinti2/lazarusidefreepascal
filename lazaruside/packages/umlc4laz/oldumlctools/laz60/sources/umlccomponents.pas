(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the Star Developer's Component Library.          *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccomponents;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcnormobjects,
  umlcdocsres,
  dummy;

(**
 ** Description:
 ** This unit provides some control basic types,
 ** with commonly used methods.
 **
 ** A "normalized" object / class is an object that supports,
 ** partially or fully, the "Normalized Object Software Design Pattern".
 **
 ** This unit defines classes, that support:
 **
 ** . A default virtual constructor without parameters,
 ** . A default virtual destructor without parameters,
 ** . A default virtual function that returns
 **   an string like Java "ToString()" function
 ** . A non virtual method that explicitly does nothing
 **   like z80 CPU instruction "NOP"
 **
 ** The term "Normalized" is "borrowed" from Database Design.
 **
 ** * May have additional methods, fields, etc.
 **)

 const

   // ---

   ID_TUMLCUMLCNormPersistent : TUMLCType =
     ($DA,$E4,$94,$54,$2B,$5A,$22,$40,$BC,$5B,$F8,$7F,$E0,$7F,$3B,$C6);

   ID_TUMLCNormComponent : TUMLCType =
     ($53,$0C,$89,$1B,$BF,$22,$98,$43,$8D,$F9,$69,$2F,$F9,$79,$49,$27);

   ID_TUMLCExtendedNormalizedPersistent : TUMLCType =
     ($BA,$33,$CF,$CA,$03,$15,$74,$4B,$82,$6E,$5C,$A4,$25,$58,$A8,$CC);

   ID_TUMLCExtendedNormalizedComponent : TUMLCType =
     ($08,$19,$FE,$A8,$15,$2D,$C8,$46,$88,$5A,$CB,$41,$09,$EE,$BD,$55);

   // ---

type

 (* TUMLCUMLCNormPersistent *)

   TUMLCUMLCNormPersistent =
     class(TInterfacedPersistent, IUMLCNormObject)
   private
     (* private declarations *)
   protected
     (* protected declarations *)
   public
     (* public declarations *)

     // let's provide a default virtual without parameters destructor
     destructor Destroy(); override;

     // similar to Java's "ToString()"
     function AsText(): string; virtual;

     procedure DoNothing(); // nonvirtual;
   end;

 (* TUMLCNormComponent *)

   TUMLCNormComponent =
       class(TComponent, IUMLCNormObject)
   private
     (* private declarations *)
   protected
     (* protected declarations *)
   public
     (* public declarations *)

     // let's provide a default virtual without parameters destructor
     destructor Destroy(); override;

     // similar to Java's "ToString()"
     function AsText(): string; virtual;

     procedure DoNothing(); // nonvirtual;
   end;

 (* TUMLCExtendedNormalizedPersistent *)

   TUMLCExtendedNormalizedPersistent =
     class(TUMLCUMLCNormPersistent, IUMLCExtendedNormalizedObject)
   private
     (* private declarations *)
   protected
     (* protected declarations *)
   public
     (* public declarations *)

     // let's provide a default virtual without parameters constructor
     procedure DoCreate(); virtual;

     // let's provide a default virtual without parameters destructor
     procedure DoDestroy(); virtual;
   end;

   (* TUMLCExtendedNormalizedComponent *)

   TUMLCExtendedNormalizedComponent =
     class(TUMLCNormComponent, IUMLCExtendedNormalizedObject)
   private
     (* private declarations *)
   protected
     (* protected declarations *)
   public
     (* public declarations *)

     // let's provide a default virtual without parameters constructor
     procedure DoCreate(); virtual;

     // let's provide a default virtual without parameters destructor
     procedure DoDestroy(); virtual;
   end;

implementation

(* TUMLCExtendedNormalizedComponent *)

procedure TUMLCExtendedNormalizedComponent.DoCreate();
begin
  Self.DoNothing();
end;

procedure TUMLCExtendedNormalizedComponent.DoDestroy();
begin
  Self.DoNothing();
end;

(* TUMLCExtendedNormalizedPersistent *)

procedure TUMLCExtendedNormalizedPersistent.DoCreate();
begin
  Self.DoNothing();
end;

procedure TUMLCExtendedNormalizedPersistent.DoDestroy();
begin
  Self.DoNothing();
end;

(* TUMLCUMLCNormPersistent *)

destructor TUMLCUMLCNormPersistent.Destroy();
begin
  inherited Destroy();
end;

function TUMLCUMLCNormPersistent.AsText(): string;
begin
  Result := ClassName();
end;

procedure TUMLCUMLCNormPersistent.DoNothing();
begin
  // Does nothing on purpose !!!
end;

(* TUMLCNormComponent *)

destructor TUMLCNormComponent.Destroy();
begin
  inherited Destroy();
end;

function TUMLCNormComponent.AsText(): string;
begin
  Result := ClassName();
end;

procedure TUMLCNormComponent.DoNothing();
begin
  // Does nothing on purpose !!!
end;

end.

