(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcconsoles;

{$mode objfpc}{$H+}

interface
uses
  SysUtils,
  Classes,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcnormobjects,
  umlccomponents,
  umlcactcntrls,
  dummy;

const

  // ---

  ID_IUMLCConsole : TUMLCType =
    ($12,$A3,$EB,$6A,$4B,$65,$6E,$41,$B1,$E3,$B0,$C8,$E0,$EE,$93,$11);

  // ---

  ID_IUMLCDOSConsole : TUMLCType =
    ($50,$E0,$E1,$1F,$B5,$B7,$0F,$4A,$A0,$1F,$7A,$31,$50,$AF,$B7,$C4);

  ID_IUMLCDOSANSICharConsole : TUMLCType =
    ($AB,$70,$32,$A4,$FB,$C1,$69,$43,$9A,$02,$8F,$25,$27,$D0,$5C,$54);

  ID_IUMLCDOSWideCharConsole : TUMLCType =
    ($53,$C4,$CB,$F9,$17,$D3,$BB,$4F,$AE,$3F,$88,$8D,$5B,$4F,$82,$26);

  ID_UMLCDOSColors : TUMLCType =
    ($83,$4A,$1A,$AB,$92,$B9,$A5,$46,$B5,$FF,$D5,$16,$03,$DE,$3F,$06);

  // ---

type

(* IUMLCConsole *)

  IUMLCConsole =
    interface(IUMLCActivatedControl)
      ['{12A3EB6A-4B65-6E41-B1E3-B0C8E0EE9311}']

    (* Public declarations *)

    function WhereX: Cardinal; //virtual;
    function WhereY: Cardinal; //virtual;

    procedure ClrScr(); //virtual;

    procedure GotoXY
      (const AX: Cardinal;
       const AY: Cardinal); //virtual;

    procedure Home(); //virtual;
  end;

  IUMLCDOSConsole =
    interface(IUMLCConsole)
      ['{50E0E11F-B5B7-0F4A-A01F-7A3150AFB7C4}']

    (* Public declarations *)

    //function WhereX: Cardinal; //virtual;
    //function WhereY: Cardinal; //virtual;

    //procedure ClrScr(); //virtual;

    //procedure GotoXY
    //  (const AX: Cardinal;
    //   const AY: Cardinal); //virtual;

    //procedure Home(); //virtual;

    procedure WriteNewLine(); //virtual;
  end;

  IUMLCDOSANSICharConsole =
    interface(IUMLCDOSConsole)
      ['{AB7032A4-FBC1-6943-9A02-8F2527D05C54}']

    (* Public declarations *)

    //function WhereX: Cardinal; //virtual;
    //function WhereY: Cardinal; //virtual;

    //procedure ClrScr(); //virtual;

    //procedure GotoXY
    //  (const AX: Cardinal;
    //   const AY: Cardinal); //virtual;

    //procedure Home(); //virtual;

    //procedure WriteNewLine(); //virtual;

    procedure WriteChar
      (const AText: AnsiChar); //virtual;
    procedure WriteStr
      (const AText: AnsiString); //virtual;

    procedure WriteLine
      (const AText: AnsiString); //virtual;
  end;

  IUMLCDOSWideCharConsole =
    interface(IUMLCDOSConsole)
      ['{53C4CBF9-17D3-BB4F-AE3F-888D5B4F8226}']

    (* Public declarations *)

    //function WhereX: Cardinal; //virtual;
    //function WhereY: Cardinal; //virtual;

    //procedure ClrScr(); //virtual;

    //procedure GotoXY
    //  (const AX: Cardinal;
    //   const AY: Cardinal); //virtual;

    //procedure Home(); //virtual;

    //procedure WriteNewLine(); //virtual;

    procedure WriteChar
      (const AText: WideChar); //virtual;
    procedure WriteStr
      (const AText: WideString); //virtual;

    procedure WriteLine
      (const AText: WideString); //virtual;
  end;

  (* UMLCDOSColors *)

  UMLCDOSColors =
    (
      Black,
      Blue,
      Green,
      Cyan,
      Red,
      Magenta,
      Brown,
      LightGray,
      DarkGray,
      LightBlue,
      LightGreen,
      LightCyan,
      LightRed,
      LightMagenta,
      Yellow,
      White
    );

  IUMLCDOSColorANSICharConsole =
    interface(IUMLCDOSANSICharConsole)
      ['{AB7032A4-FBC1-6943-9A02-8F2527D05C54}']

    (* Public declarations *)

    //function WhereX: Cardinal; //virtual;
    //function WhereY: Cardinal; //virtual;

    //procedure ClrScr(); //virtual;

    //procedure GotoXY
    //  (const AX: Cardinal;
    //   const AY: Cardinal); //virtual;

    //procedure Home(); //virtual;

    //procedure WriteNewLine(); //virtual;

    //procedure WriteChar
    //  (const AText: AnsiChar); //virtual;
    //procedure WriteStr
    //  (const AText: AnsiString); //virtual;

    //procedure WriteLine
    //  (const AText: AnsiString); //virtual;

    procedure TextColor
      (const AColor: UMLCDOSColors); //virtual;
    procedure TextBackground
      (const AColor: UMLCDOSColors); //virtual;

    function getTextForeColor: UMLCDOSColors; //virtual;
    function getTextBackColor: UMLCDOSColors; //virtual;

    procedure setForeColor
      (const AColor: UMLCDOSColors); //virtual;
    procedure setTextBackground
      (const AColor: UMLCDOSColors); //virtual;

    (* Public declarations *)

    (* Unpublished declarations *)

    property TextForeColor: UMLCDOSColors
      read getTextForeColor write setForeColor;

    property TextForeBackground: UMLCDOSColors
      read getTextBackColor write setTextBackground;
  end;

  IUMLCDOSColorWideCharConsole =
    interface(IUMLCDOSWideCharConsole)
      ['{AB7032A4-FBC1-6943-9A02-8F2527D05C54}']

    (* Public declarations *)

    //function WhereX: Cardinal; //virtual;
    //function WhereY: Cardinal; //virtual;

    //procedure ClrScr(); //virtual;

    //procedure GotoXY
    //  (const AX: Cardinal;
    //   const AY: Cardinal); //virtual;

    //procedure Home(); //virtual;

    //procedure WriteNewLine(); //virtual;

    //procedure WriteChar
    //  (const AText: AnsiChar); //virtual;
    //procedure WriteStr
    //  (const AText: AnsiString); //virtual;

    //procedure WriteLine
    //  (const AText: AnsiString); //virtual;

    procedure TextColor
      (const AColor: UMLCDOSColors); //virtual;
    procedure TextBackground
      (const AColor: UMLCDOSColors); //virtual;

    function getTextForeColor: UMLCDOSColors; //virtual;
    function getTextBackColor: UMLCDOSColors; //virtual;

    procedure setForeColor
      (const AColor: UMLCDOSColors); //virtual;
    procedure setTextBackground
      (const AColor: UMLCDOSColors); //virtual;

    (* Public declarations *)

    (* Unpublished declarations *)

    property TextForeColor: UMLCDOSColors
      read getTextForeColor write setForeColor;

    property TextForeBackground: UMLCDOSColors
      read getTextBackColor write setTextBackground;
  end;





implementation


end.

