(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbookmngrs;

interface

uses
  SysUtils, Classes,
  umlcguids,
  umlctypes, umlcstdtypes,
  dummy;

const
  IndexNotFound = -1;

type

(* TUMLCOnBookmarkEvent *)

  TUMLCOnBookmarkEvent =
    procedure (Sender: TObject; LineNo: Integer) of object;

(* TUMLCBookmark *)

  TUMLCBookmark = class(TCollectionItem)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FLineNo: Integer;
  public
    (* public declarations *)

    constructor Create(ACollection: TCollection); override;
    destructor Destroy(); override;
  published
    (* published declarations *)

    property LineNo: Integer
      read FLineNo write FLineNo;
  end;

(* TUMLCBookmarks *)

  TUMLCBookmarks = class(TOwnedCollection)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    constructor Create(AOwner: TPersistent; AItemClass: TCollectionItemClass);
    destructor Destroy(); override;
  published
    (* published declarations *)
  end;

(* TCustomumlcBookmarkMngr *)

  TCustomumlcBookmarkMngr = class(TComponent)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FOnInsert: TUMLCOnBookmarkEvent;
    FOnRemove: TUMLCOnBookmarkEvent;

    FBookmarks: TUMLCBookmarks;

    procedure DelegateOnInsert(const Value: Integer);
    procedure DelegateOnRemove(const Value: Integer);
  public
    (* public declarations *)

    function IndexOf(const LineNo: Integer): Integer;
    function HasBookmark(const LineNo: Integer): Boolean;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Insert(const LineNo: Integer);
    procedure Remove(const LineNo: Integer);

    property Bookmarks: TUMLCBookmarks
      read FBookmarks write FBookmarks;
    property OnInsert: TUMLCOnBookmarkEvent
      read FOnInsert write FOnInsert;
    property OnRemove: TUMLCOnBookmarkEvent
      read FOnRemove write FOnRemove;
  end;

(* TUMLCBookmarkMngr *)

  TUMLCBookmarkMngr = class(TCustomumlcBookmarkMngr)
  published
    (* published declarations *)

    (* TCustomumlcBookmarkMngr: *)

    property Bookmarks;
    property OnInsert;
    property OnRemove;
  end;

implementation

(* TUMLCBookmark *)

constructor TUMLCBookmark.Create(ACollection: TCollection);
begin
  inherited Create(ACollection);
  FLineNo := -1;
end;

destructor TUMLCBookmark.Destroy();
begin
  FLineNo := -1;
  inherited Destroy();
end;

(* TUMLCBookmarks *)

constructor TUMLCBookmarks.Create(AOwner: TPersistent; AItemClass: TCollectionItemClass);
begin
  inherited Create(AOwner, AItemClass);
  // Your Code...
end;

destructor TUMLCBookmarks.Destroy();
begin
  // Your Code...
  inherited Destroy();
end;

(* TCustomumlcBookmarkMngr *)


procedure TCustomumlcBookmarkMngr.DelegateOnInsert(const Value: Integer);
begin
  if (Assigned(FOnInsert))
    then FOnInsert(Self, Value);
end;

procedure TCustomumlcBookmarkMngr.DelegateOnRemove(const Value: Integer);
begin
  if (Assigned(FOnRemove))
    then FOnRemove(Self, Value);
end;

function TCustomumlcBookmarkMngr.IndexOf(const LineNo: Integer): Integer;
var AIndex, ACount: Integer; Found: Boolean; Bookmark: TUMLCBookmark;
begin
  Result := IndexNotFound;
  AIndex := 0; ACount := FBookmarks.Count; Found := FALSE;
  while ((AIndex < ACount) and (not Found)) do
  begin
    Bookmark := (FBookmarks.Items[AIndex] as TUMLCBookmark);
    Found := (Bookmark.LineNo = LineNo);
    Inc(AIndex);
  end;
  if (Found)
    then Result := Pred(AIndex);
end;

function TCustomumlcBookmarkMngr.HasBookmark(const LineNo: Integer): Boolean;
begin
  Result := (IndexOf(LineNo) <> IndexNotFound);
end;

constructor TCustomumlcBookmarkMngr.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FBookmarks := TUMLCBookmarks.Create(Self, TUMLCBookmark);
end;

destructor TCustomumlcBookmarkMngr.Destroy;
begin
  FBookmarks.Free;
  inherited Destroy;
end;

procedure TCustomumlcBookmarkMngr.Insert(const LineNo: Integer);
begin
  if (IndexOf(LineNo) = IndexNotFound) then
  begin
    (FBookmarks.Add as TUMLCBookmark).LineNo := LineNo;
    DelegateOnInsert(LineNo);
  end;
end;

procedure TCustomumlcBookmarkMngr.Remove(const LineNo: Integer);
var AIndex: Integer;
begin
  AIndex := IndexOf(LineNo);
  if (AIndex <> IndexNotFound) then
  begin
    FBookmarks.Delete(AIndex);
    DelegateOnRemove(LineNo);
  end;
end;

end.
