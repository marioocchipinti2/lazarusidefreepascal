(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdocs;

interface
uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
  //Graphics,
  Controls, ComCtrls, Forms,
{$ENDIF}
  SysUtils, Classes, Types,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcstdevents,
  umlcstrings,
  umlccomponents,
  umlcossys,
  umlcpaths,
  umlcfilesbyname,
  umlcsrchtypes,
  umlcdocsres,
  dummy;

(**
 ** Description:
 ** This unit implements the "Document" Software Design Pattern,
 ** which allows to use a file in an application,
 ** with common features like: opening a file, saving a file, etc.
 **
 ** Examples: Bitmap editor, text or word processor editor,
 ** spreadsheet application.
 **)

type
  TOnSearchModeChangeEvent =
    (* ^ *)procedure (Sender: TObject; Value: TUMLCSearchResult) of object;
type
  TOnConfirmUserEvent =
    (* ^ *)function (Sender: TObject; const Message: string): Boolean of object;

(* TCustomUMLCDocument *)

  TCustomUMLCDocument =
    class(TUMLCNormComponent)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    (* Field declarations *)

    FAskOverwrite: Boolean;
    FHasName:      Boolean;
    FHasEncoding:  Boolean;
    FModified:     Boolean;
    FReadOnly:     Boolean;
    FIsClipboardEmpty: Boolean;

    FEncoding:  string;
    FExtNew:    string;
    FExtNewAs:  string;
    FExtOpen:   string;
    FExtSave:   string;
    FExtSaveAs: string;
    FFullPath:  string;

    FSearchMode:  TUMLCSearchResult;
    FSearchIndex: Word;
  protected
    (* Protected declarations *)

    (* Functors declarations *)

    FOnModified:          TOnBooleanChangeEvent;
    FOnPathChanged:       TOnStringChangeEvent;
    FOnSearchModeChanged: TOnSearchModeChangeEvent;
    FOnConfirmUser:       TOnConfirmUserEvent;

    FBeforeNewFile:    TConfirmEvent;
    FAfterNewFile:     TNotifyEvent;

    FBeforeNewAsFile:  TConfirmEvent;
    FAfterNewAsFile:   TNotifyEvent;

    FBeforeOpenFile:   TConfirmEvent;
    FAfterOpenFile:    TNotifyEvent;

    FBeforeSaveFile:   TConfirmEvent;
    FAfterSaveFile:    TNotifyEvent;

    FBeforeSaveAsFile: TConfirmEvent;
    FAfterSaveAsFile:  TNotifyEvent;
  protected
    (* Protected declarations *)

    (* Accesors declarations *)

    function getAskOverwrite(): Boolean;
    function getEncoding(): string;
    function getExtNew(): string;
    function getExtNewAs(): string;
    function getExtOpen(): string;
    function getExtSave(): string;
    function getExtSaveAs(): string;
    function getHasName(): Boolean;
    function getHasEncoding(): Boolean;
    function getIsClipboardEmpty(): Boolean;
    function getModified(): Boolean;
    function getReadOnly(): Boolean;
    function getFullPath(): string;
    function getSearchMode(): TUMLCSearchResult;

    procedure setAskOverwrite(const AValue: Boolean); virtual;
    procedure setEncoding(const AValue: string); virtual;
    procedure setExtNew(AValue: string); virtual;
    procedure setExtNewAs(AValue: string); virtual;
    procedure setExtOpen(const AValue: string); virtual;
    procedure setExtSave(const AValue: string); virtual;
    procedure setExtSaveAs(const AValue: string); virtual;
    procedure setHasName(const AValue: Boolean); virtual;
    procedure setHasEncoding(const AValue: Boolean); virtual;
    procedure setIsClipboardEmpty(const AValue: Boolean); virtual;
    procedure setModified(const AValue: Boolean); virtual;
    procedure setReadOnly(const AValue: Boolean); virtual;
    procedure setFullPath(const AValue: string); virtual;
    procedure setSearchMode(const AValue: TUMLCSearchResult); virtual;
  protected
    (* Protected declarations *)

    (* Delegate declarations *)

    procedure DelegateOnModified(const AValue: Boolean);
    procedure DelegateOnPathChanged(const AValue: string);
    procedure DelegateOnSearchModeChanged(const AValue: TUMLCSearchResult);

    function DelegateOnConfirmUser
      (const Message: string): Boolean;

    function DelegateBeforeNewFile(): Boolean;
    procedure DelegateAfterNewFile();

    function DelegateBeforeNewAsFile(): Boolean;
    procedure DelegateAfterNewAsFile();

    function DelegateBeforeOpenFile(): Boolean;
    procedure DelegateAfterOpenFile();

    function DelegateBeforeSaveFile(): Boolean;
    procedure DelegateAfterSaveFile();

    function DelegateBeforeSaveAsFile(): Boolean;
    procedure DelegateAfterSaveAsFile();
  protected
    (* Protected declarations *)

    function NewFileName(const Extension: string): string;

    procedure BackupPreviousFile();

    function InternalBeforeNewFile(): Boolean; virtual;
    procedure InternalNewFile();       virtual;
    procedure InternalAfterNewFile();  virtual;

    function InternalBeforeOpenFile(): Boolean; virtual;
    procedure InternalOpenFile();       virtual;
    procedure InternalAfterOpenFile();  virtual;

    function InternalBeforeNewAsFile(): Boolean; virtual;
    procedure InternalNewAsFile();       virtual;
    procedure InternalAfterNewAsFile();  virtual;

    function InternalBeforeSaveFile(): Boolean; virtual;
    procedure InternalSaveFile();       virtual;
    procedure InternalAfterSaveFile();  virtual;

    function InternalBeforeSaveAsFile(): Boolean; virtual;
    procedure InternalSaveAsFile();       virtual;
    procedure InternalAfterSaveAsFile();  virtual;
  public
    (* Public declarations *)

    function IgnoreFile(): Boolean;

    procedure NewFile();
    procedure NewAsFile();
    procedure OpenFile();
    procedure SaveFile();
    procedure SaveAsFile();
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    (* read-only properties *)

    function FileName(): string;
    function FullFileName(): string;
  public
    (* Public declarations *)

    (* Property declarations *)

    property AskOverwrite: Boolean
      read getAskOverwrite write setAskOverwrite;
    property Encoding: string
      read getEncoding write setEncoding;
    property ExtNew: string
      read getExtNew write setExtNew;
    property ExtNewAs: string
      read getExtNewAs write setExtNewAs;
    property ExtOpen: string
      read getExtOpen write setExtOpen;
    property ExtSave: string
      read getExtSave write setExtSave;
    property ExtSaveAs: string
      read getExtSaveAs write setExtSaveAs;
    property HasName: Boolean
      read getHasName write setHasName;
    property HasEncoding: Boolean
      read getHasEncoding write setHasEncoding;
    property IsClipboardEmpty: Boolean
      read getIsClipboardEmpty write setIsClipboardEmpty;
    property Modified: Boolean
      read getModified write setModified;
    property ReadOnly: Boolean
      read getReadOnly write setReadOnly;
    property FullPath: string
      read getFullPath write setFullPath;
    property SearchMode: TUMLCSearchResult
      read getSearchMode write setSearchMode;
  public
    (* Public declarations *)

    (* Event declarations *)

    property OnModified: TOnBooleanChangeEvent
      read FOnModified write FOnModified;
    property OnPathChanged: TOnStringChangeEvent
      read FOnPathChanged write FOnPathChanged;
    property OnSearchModeChanged: TOnSearchModeChangeEvent
      read FOnSearchModeChanged write FOnSearchModeChanged;
    property OnConfirmUser: TOnConfirmUserEvent
      read FOnConfirmUser write FOnConfirmUser;

    property BeforeNewFile: TConfirmEvent
      read FBeforeNewFile write FBeforeNewFile;
    property AfterNewFile: TNotifyEvent
      read FAfterNewFile write FAfterNewFile;

    property BeforeNewAsFile: TConfirmEvent
      read FBeforeNewAsFile write FBeforeNewAsFile;
    property AfterNewAsFile: TNotifyEvent
      read FAfterNewAsFile write FAfterNewAsFile;

    property BeforeOpenFile: TConfirmEvent
      read FBeforeOpenFile write FBeforeOpenFile;
    property AfterOpenFile: TNotifyEvent
      read FAfterOpenFile write FAfterOpenFile;

    property BeforeSaveFile: TConfirmEvent
      read FBeforeSaveFile write FBeforeSaveFile;
    property AfterSaveFile: TNotifyEvent
      read FAfterSaveFile write FAfterSaveFile;

    property BeforeSaveAsFile: TConfirmEvent
      read FBeforeSaveAsFile write FBeforeSaveAsFile;
    property AfterSaveAsFile: TNotifyEvent
      read FAfterSaveAsFile write FAfterSaveAsFile;
  end;

(* TCustomUMLCDelegateDocument *)

  TCustomUMLCDelegateDocument =
    class(TCustomUMLCDocument)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    (* Delegate declarations *)

    procedure DelegateOnNewFile();
    procedure DelegateOnNewAsFile();

    procedure DelegateOnOpenFile();

    procedure DelegateOnSaveFile();
    procedure DelegateOnSaveAsFile();
  protected
    (* Protected declarations *)

    procedure InternalNewFile(); override;
    procedure InternalNewAsFile(); override;
    procedure InternalOpenFile(); override;
    procedure InternalSaveFile(); override;
    procedure InternalSaveAsFile(); override;
  protected
    (* Protected declarations *)

    (* Functors declarations *)

    FOnNewFile:    TNotifyEvent;
    FOnNewAsFile:  TNotifyEvent;
    FOnOpenFile:   TNotifyEvent;
    FOnSaveFile:   TNotifyEvent;
    FOnSaveAsFile: TNotifyEvent;
  public
    (* Public declarations *)
  public
    (* Public declarations *)

    (* Event declarations *)

    property OnNewFile: TNotifyEvent
      read FOnNewFile write FOnNewFile;
    property OnNewAsFile: TNotifyEvent
      read FOnNewAsFile write FOnNewAsFile;
    property OnOpenFile: TNotifyEvent
      read FOnOpenFile write FOnOpenFile;
    property OnSaveFile: TNotifyEvent
      read FOnSaveFile write FOnSaveFile;
    property OnSaveAsFile: TNotifyEvent
      read FOnSaveAsFile write FOnSaveAsFile;
  end;

(* TCustomUMLCTextDocument *)

  TCustomUMLCTextDocument =
    class(TCustomUMLCDocument)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    (* Field declarations *)

    FIdentation:   Integer;
  protected
    (* Protected declarations *)

    (* Accesors declarations *)

    function getIdentation(): Integer;

    procedure setIdentation(const AValue: Integer);
  protected
    (* Protected declarations *)

    procedure InternalNewFile(); override;
  public
    (* Public declarations *)

    (* Property declarations *)

    property Identation: Integer
      read getIdentation write setIdentation;
  end;

(* TUMLCDocument *)

  TUMLCDocument =
    class(TCustomUMLCDocument)
  published
    (* Published declarations *)

    (* TCustomUMLCDocument: *)

    property AskOverwrite;
    property Encoding;
    property ExtNewAs;
    property ExtOpen;
    property ExtSave;
    property ExtSaveAs;
    property HasName;
    property HasEncoding;
    property IsClipboardEmpty;
    property Modified;
    property ReadOnly;
    property FullPath;
    property SearchMode;

    property OnModified;
    property OnPathChanged;
    property OnSearchModeChanged;
    property OnConfirmUser;

    property BeforeNewFile;
    property AfterNewFile;

    property BeforeNewAsFile;
    property AfterNewAsFile;

    property BeforeOpenFile;
    property AfterOpenFile;

    property BeforeSaveFile;
    property AfterSaveFile;

    property BeforeSaveAsFile;
    property AfterSaveAsFile;
  end;

(* TUMLCDelegateDocument *)

  TUMLCDelegateDocument =
    class(TCustomUMLCDelegateDocument)
  published
    (* Published declarations *)

    (* TCustomUMLCDocument: *)

    property AskOverwrite;
    property Encoding;
    property ExtNewAs;
    property ExtOpen;
    property ExtSave;
    property ExtSaveAs;
    property HasName;
    property HasEncoding;
    property IsClipboardEmpty;
    property Modified;
    property ReadOnly;
    property FullPath;
    property SearchMode;

    property OnModified;
    property OnPathChanged;
    property OnSearchModeChanged;
    property OnConfirmUser;

    property BeforeOpenFile;
    property AfterOpenFile;

    (* TCustomUMLCDelegateDocument: *)

    property OnNewFile;
    property OnNewAsFile;
    property OnOpenFile;
    property OnSaveFile;
    property OnSaveAsFile;
  end;

implementation

function TCustomUMLCDocument.getAskOverwrite(): Boolean;
begin
  Result := FAskOverwrite;
end;

function TCustomUMLCDocument.getEncoding(): string;
begin
  Result := FEncoding;
end;

function TCustomUMLCDocument.getExtNew(): string;
begin
  Result := FExtNew;
end;

function TCustomUMLCDocument.getExtNewAs(): string;
begin
  Result := FExtNewAs;
end;

function TCustomUMLCDocument.getExtOpen(): string;
begin
  Result := FExtOpen;
end;

function TCustomUMLCDocument.getExtSave(): string;
begin
  Result := FExtSave;
end;

function TCustomUMLCDocument.getExtSaveAs(): string;
begin
  Result := FExtSaveAs;
end;

function TCustomUMLCDocument.getHasName(): Boolean;
begin
  Result := FHasName;
end;

function TCustomUMLCDocument.getHasEncoding(): Boolean;
begin
  Result := FHasEncoding;
end;

function TCustomUMLCDocument.getIsClipboardEmpty(): Boolean;
begin
  Result := FIsClipboardEmpty;
end;

function TCustomUMLCDocument.getModified(): Boolean;
begin
  Result := FModified;
end;

function TCustomUMLCDocument.getReadOnly(): Boolean;
begin
  Result := FReadOnly;
end;

function TCustomUMLCDocument.getFullPath(): string;
begin
  Result := FFullPath;
end;

function TCustomUMLCDocument.getSearchMode(): TUMLCSearchResult;
begin
  Result := FSearchMode;
end;

procedure TCustomUMLCDocument.setFullPath(const AValue: string);
begin
  FFullPath := AValue;
  DelegateOnPathChanged(AValue);
end;

procedure TCustomUMLCDocument.setSearchMode(const AValue: TUMLCSearchResult);
begin
  if (FSearchMode <> AValue) then
  begin
    FSearchMode := AValue;
    DelegateOnSearchModeChanged(AValue);
  end;
end;

procedure TCustomUMLCDocument.setAskOverwrite(const AValue: Boolean);
begin
  FAskOverwrite := AValue;
end;

procedure TCustomUMLCDocument.setEncoding(const AValue: string);
begin
  FEncoding := AValue;
end;

procedure TCustomUMLCDocument.setExtNew(AValue: string);
var ANewValue: string;
begin
  // remove optional prefixes,
  // example:
  // "*.txt" becomes "txt"
  // ".txt" becomes "txt"
  ANewValue := umlcstrings.TrimPrefixCopy(AValue, '*.');
  ANewValue := umlcstrings.TrimPrefixCopy(ANewValue, '.');

  FExtNew := ANewValue;
end;

procedure TCustomUMLCDocument.setExtNewAs(AValue: string);
var ANewValue: string;
begin
  // remove optional prefixes,
  // example:
  // "*.txt" becomes "txt"
  // ".txt" becomes "txt"
  ANewValue := umlcstrings.TrimPrefixCopy(AValue, '*.');
  ANewValue := umlcstrings.TrimPrefixCopy(ANewValue, '.');

  FExtNewAs := ANewValue;
end;

procedure TCustomUMLCDocument.setExtOpen(const AValue: string);
var ANewValue: string;
begin
  // remove optional prefixes,
  // example:
  // "*.txt" becomes "txt"
  // ".txt" becomes "txt"
  ANewValue := umlcstrings.TrimPrefixCopy(AValue, '*.');
  ANewValue := umlcstrings.TrimPrefixCopy(ANewValue, '.');

  FExtOpen := ANewValue;
end;

procedure TCustomUMLCDocument.setExtSave(const AValue: string);
var ANewValue: string;
begin
  // remove optional prefixes,
  // example:
  // "*.txt" becomes "txt"
  // ".txt" becomes "txt"
  ANewValue := umlcstrings.TrimPrefixCopy(AValue, '*.');
  ANewValue := umlcstrings.TrimPrefixCopy(ANewValue, '.');

  FExtSave := AValue;
end;

procedure TCustomUMLCDocument.setExtSaveAs(const AValue: string);
var ANewValue: string;
begin
  // remove optional prefixes,
  // example:
  // "*.txt" becomes "txt"
  // ".txt" becomes "txt"
  ANewValue := umlcstrings.TrimPrefixCopy(AValue, '*.');
  ANewValue := umlcstrings.TrimPrefixCopy(ANewValue, '.');

  FExtSaveAs := AValue;
end;

procedure TCustomUMLCDocument.setHasName(const AValue: Boolean);
begin
  FHasName := AValue;
end;

procedure TCustomUMLCDocument.setHasEncoding(const AValue: Boolean);
begin
  FHasEncoding := AValue;
end;

procedure TCustomUMLCDocument.setIsClipboardEmpty(const AValue: Boolean);
begin
  FIsClipboardEmpty := AValue;

//  CloseMessagesEnabled:= Value;
//  SaveMessagesEnabled:= Value;
end;

procedure TCustomUMLCDocument.setModified(const AValue: Boolean);
begin
  FModified := AValue;
  DelegateOnModified(AValue);
end;

procedure TCustomUMLCDocument.setReadOnly(const AValue: Boolean);
begin
  FReadOnly := AValue;
end;

procedure TCustomUMLCDocument.DelegateOnModified(const AValue: Boolean);
begin
  if (FOnModified <> nil) then
  begin
    FOnModified(Self, AValue);
  end;
end;

procedure TCustomUMLCDocument.DelegateOnPathChanged(const AValue: string);
begin
  if (FOnPathChanged <> nil) then
  begin
    FOnPathChanged(Self, AValue);
  end;
end;

procedure TCustomUMLCDocument.DelegateOnSearchModeChanged
  (const AValue: TUMLCSearchResult);
begin
  if (FOnSearchModeChanged <> nil) then
  begin
    FOnSearchModeChanged(Self, AValue);
  end;
end;

function TCustomUMLCDocument.DelegateOnConfirmUser
  (const Message: string): Boolean;
begin
  Result := FALSE;
  if (FOnConfirmUser <> nil) then
  begin
    Result := FOnConfirmUser(Self, Message);
  end;
end;

function TCustomUMLCDocument.DelegateBeforeNewFile(): Boolean;
begin
  // by default, can continue
  Result := true;
  if (FBeforeNewFile <> nil) then
  begin
    Result := FBeforeNewFile(Self);
  end;
end;

procedure TCustomUMLCDocument.DelegateAfterNewFile();
begin
  if (FAfterNewFile <> nil) then
  begin
    FAfterNewFile(Self);
  end;
end;

function TCustomUMLCDocument.DelegateBeforeNewAsFile(): Boolean;
begin
  if (FBeforeNewAsFile <> nil) then
  begin
    Result := FBeforeNewAsFile(Self);
  end;
end;

procedure TCustomUMLCDocument.DelegateAfterNewAsFile();
begin
  if (FAfterNewAsFile <> nil) then
  begin
    FAfterNewAsFile(Self);
  end;
end;

function TCustomUMLCDocument.DelegateBeforeOpenFile(): Boolean;
begin
  if (FBeforeOpenFile <> nil) then
  begin
    Result := FBeforeOpenFile(Self);
  end;
end;

procedure TCustomUMLCDocument.DelegateAfterOpenFile();
begin
  if (FAfterOpenFile <> nil) then
  begin
    FAfterOpenFile(Self);
  end;
end;

function TCustomUMLCDocument.DelegateBeforeSaveFile(): Boolean;
begin
  if (FBeforeSaveFile <> nil) then
  begin
    Result := FBeforeSaveFile(Self);
  end;
end;

procedure TCustomUMLCDocument.DelegateAfterSaveFile();
begin
  if (FAfterSaveFile <> nil) then
  begin
    FAfterSaveFile(Self);
  end;
end;

function TCustomUMLCDocument.DelegateBeforeSaveAsFile(): Boolean;
begin
  if (FBeforeSaveAsFile <> nil) then
  begin
    Result := FBeforeSaveAsFile(Self);
  end;
end;

procedure TCustomUMLCDocument.DelegateAfterSaveAsFile();
begin
  if (FAfterSaveAsFile <> nil) then
  begin
    FAfterSaveAsFile(Self);
  end;
end;

function TCustomUMLCDocument.NewFileName(const Extension: string): string;
var APrevPath, APath, AFileName, ANewExt: string;
begin
  // remove potential optional prefix
  ANewExt := umlcstrings.TrimPrefixCopy(Extension, '*.');
  ANewExt := umlcstrings.TrimPrefixCopy(ANewExt, '.');

  APrevPath := umlcossys.getSystemPath();
  AFileName := resNoName; // <--
  APath     := umlcpaths.EncodePath(APrevPath, AFileName, Extension);

  Result := resNoName + '.' + ANewExt;
end;

procedure TCustomUMLCDocument.BackupPreviousFile();
var CanSave, CanOverwrite: Boolean;
begin
  if (Self.Modified) then
  begin
    CanOverwrite := not FileExists(FFullPath);
    // revisar que archivo no exista
    // check file doesn*t exists

    if ((not CanOverwrite) and (AskOverwrite)) then
    begin
      CanSave := DelegateOnConfirmUser(Format(resFileOverwrite, [FullPath]));
      AskOverwrite := not CanSave;
    end else CanSave := not AskOverwrite;
    // detecta si el archivo deberia ser
    // detect if file should be overwritten

    if (CanSave) then
    begin
      Self.SaveFile();
    end;
  end;
end;

function TCustomUMLCDocument.InternalBeforeNewFile(): Boolean;
begin
  Result := DelegateBeforeNewFile();
end;

procedure TCustomUMLCDocument.InternalNewFile();
begin
  FAskOverwrite := FALSE;
  FHasName      := FALSE;

  FullPath := NewFileName(ExtNew);
  // limpiar propiedades de interfaz
  // clear interface properties

  FSearchIndex := 1;
  FSearchMode  := umlcsrchtypes.srrSearch;
  SearchMode   := umlcsrchtypes.srrNone;
  // sera actualizado al activar ventana
  // will be updated when window is activated

  FIsClipboardEmpty := TRUE;
  FModified     := FALSE;
  FHasName      := FALSE;
  FReadOnly     := FALSE;
  FHasEncoding  := FALSE;

  FAskOverwrite := FALSE;
end;

procedure TCustomUMLCDocument.InternalAfterNewFile();
begin
  DelegateAfterNewFile();
end;

function TCustomUMLCDocument.InternalBeforeOpenFile(): Boolean;
begin
  Result := DelegateBeforeOpenFile();
end;

procedure TCustomUMLCDocument.InternalOpenFile();
begin
  HasName := TRUE;
  Modified := FALSE;
  AskOverwrite := FALSE;
  // update "modified" status
  // actualizar estado de "modificado"
end;

procedure TCustomUMLCDocument.InternalAfterOpenFile();
begin
  DelegateAfterOpenFile();
end;

function TCustomUMLCDocument.InternalBeforeNewAsFile(): Boolean;
begin
  Result := DelegateBeforeNewAsFile();
end;

procedure TCustomUMLCDocument.InternalNewAsFile();
begin
  InternalNewFile();
end;

procedure TCustomUMLCDocument.InternalAfterNewAsFile();
begin
  DelegateAfterNewAsFile();
end;

function TCustomUMLCDocument.InternalBeforeSaveFile(): Boolean;
begin
  Result := DelegateBeforeSaveFile();
end;

procedure TCustomUMLCDocument.InternalSaveFile();
begin
  HasName  := TRUE;
  Modified := FALSE;
  // update "modified" status
  // actualizar estado de "modificado"
end;

procedure TCustomUMLCDocument.InternalAfterSaveFile();
begin
  DelegateAfterSaveFile();
end;

function TCustomUMLCDocument.InternalBeforeSaveAsFile(): Boolean;
begin
  Result := DelegateBeforeSaveAsFile();
end;

procedure TCustomUMLCDocument.InternalSaveAsFile();
begin
  HasName  := TRUE;
  Modified := FALSE;
  // update "modified" status
  // actualizar estado de "modificado"
end;

procedure TCustomUMLCDocument.InternalAfterSaveAsFile();
begin
  DelegateAfterSaveAsFile();
end;

function TCustomUMLCDocument.IgnoreFile(): Boolean;
begin
  Result := not Modified;
  if (Modified) then
  begin
    Result := not DelegateOnConfirmUser(Format(resFileSave, [FullPath]));
  end;
end;

procedure TCustomUMLCDocument.NewFile();
begin
  BackupPreviousFile();

  if (InternalBeforeNewFile()) then
  begin
    InternalNewFile();
    InternalAfterNewFile();
  end;
end;

procedure TCustomUMLCDocument.NewAsFile();
begin
  BackupPreviousFile();

  if (InternalBeforeNewAsFile()) then
  begin
    InternalNewAsFile();
    InternalAfterNewAsFile();
  end;
end;

procedure TCustomUMLCDocument.OpenFile();
begin
  BackupPreviousFile();

  if (InternalBeforeOpenFile()) then
  begin
    InternalOpenFile();
    InternalAfterOpenFile();
  end;
end;

procedure TCustomUMLCDocument.SaveFile();
var CanSave, CanOverwrite: Boolean;
begin
  CanOverwrite := not FileExists(FFullPath);
  // revisar que archivo no exista
  // check file doesn*t exists

  if ((not CanOverwrite) and (AskOverwrite)) then
  begin
    CanSave := DelegateOnConfirmUser(Format(resFileOverwrite, [FullPath]));
    AskOverwrite := not CanSave;
  end else CanSave := not AskOverwrite;
  // detecta si el archivo deberia ser
  // detect if file should be overwritten

  if (CanSave) then
  begin
    if (InternalBeforeSaveFile()) then
    begin
      InternalSaveFile();
      InternalAfterSaveFile();
    end;
  end;
end;

procedure TCustomUMLCDocument.SaveAsFile();
begin
  if (InternalBeforeSaveAsFile()) then
  begin
    InternalSaveAsFile();
    InternalAfterSaveAsFile();
  end;
end;

constructor TCustomUMLCDocument.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  // --> properties that are independent of
  // --> how many times a file is opened or closed
  FExtNewAs  := '';
  FExtOpen   := '';
  FExtSave   := '';
  FExtSaveAs := '';
  // default file extensions
  // extensiones de archivo por default
end;

destructor TCustomUMLCDocument.Destroy();
begin
  inherited Destroy();
end;

function TCustomUMLCDocument.FileName: string;
begin
  Result := umlcpaths.ExtractFileName(Self.FullPath);
  // Goal: Returns filename, without extension, and without FullPath.
end;

function TCustomUMLCDocument.FullFileName(): string;
begin
  Result := umlcpaths.ExtractFileFullName(Self.FullPath);
  // Goal: Returns filename & fileext, without FullPath.
end;

(* TCustomUMLCDelegateDocument *)

procedure TCustomUMLCDelegateDocument.DelegateOnNewFile();
begin
  if (FOnNewFile <> nil) then
  begin
    FOnNewFile(Self);
  end;
end;

procedure TCustomUMLCDelegateDocument.DelegateOnNewAsFile();
begin
  if (FOnNewAsFile <> nil) then
  begin
    FOnNewAsFile(Self);
  end;
end;

procedure TCustomUMLCDelegateDocument.DelegateOnOpenFile();
begin
  if (FOnOpenFile <> nil) then
  begin
    FOnOpenFile(Self);
  end;
end;

procedure TCustomUMLCDelegateDocument.DelegateOnSaveFile();
begin
  if (FOnSaveFile <> nil) then
  begin
    FOnSaveFile(Self);
  end;
end;

procedure TCustomUMLCDelegateDocument.DelegateOnSaveAsFile();
begin
  if (FOnSaveAsFile <> nil) then
  begin
    FOnSaveAsFile(Self);
  end;
end;

procedure TCustomUMLCDelegateDocument.InternalNewFile();
begin
  inherited InternalNewFile();
  DelegateOnNewFile();
end;

procedure TCustomUMLCDelegateDocument.InternalNewAsFile();
begin
  inherited InternalNewAsFile();
  DelegateOnNewAsFile();
end;

procedure TCustomUMLCDelegateDocument.InternalOpenFile();
begin
  inherited InternalOpenFile();
  DelegateOnOpenFile();
end;

procedure TCustomUMLCDelegateDocument.InternalSaveFile();
begin
  inherited InternalSaveFile();
  DelegateOnSaveFile();
end;

procedure TCustomUMLCDelegateDocument.InternalSaveAsFile();
begin
  inherited InternalSaveFile();
  DelegateOnSaveAsFile();
end;

(* TCustomUMLCTextDocument *)

function TCustomUMLCTextDocument.getIdentation(): Integer;
begin
  Result := FIdentation;
end;

procedure TCustomUMLCTextDocument.setIdentation(const AValue: Integer);
begin
  FIdentation := AValue;
end;

procedure TCustomUMLCTextDocument.InternalNewFile();
begin
  inherited InternalNewFile();
  FIdentation := 2;
end;

end.
