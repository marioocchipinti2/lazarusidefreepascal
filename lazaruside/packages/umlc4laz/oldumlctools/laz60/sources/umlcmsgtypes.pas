(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmsgtypes;

interface
uses
  SysUtils, 
  Classes,
  umlcguids,
  umlcguidstrs,
  umlcactcntrls,
  umlclists,
  umlcreclists,
  dummy;

type

(* TUMLCMessage *)

 TUMLCMessage = TGUID;
 PUMLCMessage = ^TUMLCMessage;

// default messages
// remember that even that there is several string
// representations of a G.U.I.D. / U.U.I.D.,
// there should be considered always,
// as binary, maybe integer, data.

const
  msgNone =
    '{00000000-00000000-00000000-00000000}';

  msgServerAssign =
    '{E5267115-D7CD5F41-82D9E797-0751C401}';
  msgServerDeassign =
    '{04158D75-93451C43-B1CD62DD-3130F277}';

type

(* TUMLCMessageParamsRecord *)

(**
 ** Used to send & recieve messages.
 **)

  TUMLCMessageParamsRecord = record
    Sender:  TObject;
    Message: TUMLCMessage;
    Param:   pointer;
  end;

 PUMLCMessageParamsRecord = ^TUMLCMessageParamsRecord;

(* TUMLCMsgEventHandler *)

(**
 ** Used to delegate the message to an object pascal event (property).
 **)

  TUMLCMsgEventHandler =
    procedure
      (const AMsgRec: TUMLCMessageParamsRecord) of object;

(* Clients List Iterators *)

  IUMLCMessageClient = interface;

  TUMLCMsgClientFirstThat =
    function
      (const AItem: IUMLCMessageClient;
       const AParam: Pointer): Boolean of object;
  TUMLCMsgClientForEach =
    procedure
      (const AItem: IUMLCMessageClient;
       const AParam: pointer) of object;

(* TUMLCMsgItemRec *)

(**
 ** Interfaces cannot be directly stored in a dynamic allocated
 ** list, so a record wrapper must be used.
 **)

  TUMLCMsgItemRec = record
    Item: IUMLCMessageClient;
  end;
 PUMLCMsgItemRec = ^TUMLCMsgItemRec;

(* TUMLCMsgClientList *)

  TUMLCMsgClientList = class(TUMLCRecordList)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function getItems(AIndex: Integer): IUMLCMessageClient;

    procedure setItems(AIndex: Integer; AItem: IUMLCMessageClient);
  public
    (* Public declarations *)

    constructor Create(); override;

    function IndexOf(const AItem: IUMLCMessageClient): Integer;
    function First(): IUMLCMessageClient;
    function Last(): IUMLCMessageClient;

    function Insert(const AItem: IUMLCMessageClient): Integer;
    procedure InsertAt(const AIndex: Integer; const AItem: IUMLCMessageClient);

    function Remove(const AItem: IUMLCMessageClient): Integer;
    function Extract(const AIndex: Integer): IUMLCMessageClient;

    function FirstThat
      (const Func: TUMLCMsgClientFirstThat; AParam: Pointer): IUMLCMessageClient;

    procedure ForEach(const Proc: TUMLCMsgClientForEach; const AParam: pointer);
    procedure ForBack(const Proc: TUMLCMsgClientForEach; const AParam: pointer);

    property Items[Index: Integer]: IUMLCMessageClient
      read getItems write setItems; default;
  end;

(* TUMLCMessageHandlerRecord *)

(**
 ** Used to relate messages to a handler procedure.
 **)

  TUMLCMessageHandlerRecord = record
    Message: TUMLCMessage;
    Handler: TUMLCMsgEventHandler;
  end;

 PUMLCMessageHandlerRecord = ^TUMLCMessageHandlerRecord;

(* Handler List Iterators *)

  TUMLCMessageHandlerFirstThat =
    function (const AItem:PUMLCMessageHandlerRecord; const AParam: Pointer): Boolean of object;

(* TUMLCMessageHandlerList *)

(**
 ** Stores a list of "TUMLCMessageHandlerRecord" items,
 ** where each "Message" field should have a unique value,
 ** in the list.
 **)

  TUMLCMessageHandlerList = class(TUMLCRecordList)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function getItems(AIndex: Integer):PUMLCMessageHandlerRecord;

    procedure setItems(AIndex: Integer; AItem:PUMLCMessageHandlerRecord);
  protected
    (* Protected declarations *)

    function MatchesMessage
        (const AItem:PUMLCMessageHandlerRecord;
         const AParam: Pointer): Boolean;
  public
    (* Public declarations *)

    constructor Create(); override;

    function Insert
      (const AMsg: TUMLCMessage; AHandler: TUMLCMsgEventHandler): Integer;

    function FirstThat
      (const Func: TUMLCMessageHandlerFirstThat; AParam: Pointer):PUMLCMessageHandlerRecord;

    function HandlerOf(const AMsg: TUMLCMessage): TUMLCMsgEventHandler;
  end;

(* TUMLCCurrentMsgClientList *)

  TUMLCCurrentMsgClientList = class(TUMLCMsgClientList)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FCurrentIndex: Integer;

    function getCurrentIndex(): Integer;

    procedure setCurrentIndex(const AItem: Integer);
  public
    (* Public declarations *)

    property CurrentIndex: Integer
      read getCurrentIndex write setCurrentIndex;
  end;

(* IUMLCMessageServer *)

  IUMLCMessageServer = interface(IUnknown)
    (* Interface declarations *)

    (* Accessors declarations *)

    function getClients(): TUMLCMsgClientList;

    procedure setClients(const AValue: TUMLCMsgClientList);

    (* Public declarations *)

    procedure InsertClient(const AClient: IUMLCMessageClient);
    procedure RemoveClient(const AClient: IUMLCMessageClient);

    function AsComponent(): TComponent;

    procedure SendMessage
      (const AMsgRec: TUMLCMessageParamsRecord);
    procedure SendMessageSingle
      (const AClient: IUMLCMessageClient; const AMsgRec: TUMLCMessageParamsRecord);

    (* Read-Only properties *)

    function ClientsCount(): Integer;
    function HasClients(): Boolean;

    (* Never Published declarations *)

    property Clients: TUMLCMsgClientList
      read getClients write setClients;
  end;
  // Goal: Notifies client objects that server have change.
  // Objetivo: Notifica a objetos cliente que el servidor ha cambiado.

(* IUMLCSingleMessageServer *)

  IUMLCSingleMessageServer = interface(IUMLCMessageServer)
    (* Interface declarations *)

    function CurrentClient(): IUMLCMessageClient;

    procedure SendMessageCurrent(const AMsgRec: TUMLCMessageParamsRecord);
  end;
  // Goal: Notifies CurrentIndex client object that server have change.
  // Objetivo: Notifica al objeto cliente actual que el servidor ha cambiado.

(* IUMLCMessageClient *)

  IUMLCMessageClient = interface(IUnknown)
    (* Interface declarations *)

    procedure AnswerMessage
      (const AMsgRec: TUMLCMessageParamsRecord);

    function AsComponent(): TComponent;
  end;
  // Goal: Client object that answers to server*s changes.
  // Doesn't know anything about a server.

  // Objetivo: Objetos cliente que responde a cambios del servidor.
  // No sabe nada del servidor.

  (* IUMLCSingleServerMessageClient *)

  IUMLCSingleServerMessageClient = interface(IUMLCMessageClient)
    (* Interface declarations *)

    (* Accessors declarations *)

    function getServer(): IUMLCMessageServer;

    procedure setServer(const AValue: IUMLCMessageServer);

    (* Public declarations *)

    property Server: IUMLCMessageServer
      read getServer write setServer;
  end;
  // Goal: Client object that answers to server*s changes.
  // It intended to have a single known server.

  // Objetivo: Objetos cliente que responde a cambios del servidor.
  // Se pretende que solo haya un servidor conocido.

  (* IUMLCMultipleServerMessageClient *)

  IUMLCMultipleServerMessageClient = interface(IUMLCMessageClient)
    (* Interface declarations *)

    // @to-do: ...
  end;
  // Goal: Client object that answers to server*s changes.
  // Has several registered servers,
  // from the client side.

  // Objetivo: Objetos cliente que responde a cambios del servidor.
  // Tiene varios servidores registrados,
  // del lado del cliente.

implementation

(* TUMLCMsgClientList *)

function TUMLCMsgClientList.getItems(AIndex: Integer): IUMLCMessageClient;
var ARec:PUMLCMsgItemRec;
begin
  ARec :=PUMLCMsgItemRec(getInternalItems(AIndex));
  Result := ARec^.Item;
end;

procedure TUMLCMsgClientList.setItems(AIndex: Integer; AItem: IUMLCMessageClient);
var ARec:PUMLCMsgItemRec;
begin
  ARec :=PUMLCMsgItemRec(getInternalItems(AIndex));
  ARec^.Item := AItem;
end;

constructor TUMLCMsgClientList.Create();
begin
  inherited Create();
  RecordSize := System.SizeOf(TUMLCMsgItemRec)
end;

function TUMLCMsgClientList.IndexOf(const AItem: IUMLCMessageClient): Integer;
var CanContinue: Boolean; AMsgItemRec:PUMLCMsgItemRec;
begin
  Result := IndexNotFound;

  if ((FList <> nil) and (FCount > 0)) then
  begin
    Result := 0;
    CanContinue := false;
    repeat
      CanContinue := (Result < FCount);
      if (CanContinue) then
      begin
        AMsgItemRec :=PUMLCMsgItemRec(FList^[Result]);
        CanContinue := (AMsgItemRec^.Item <> AItem);
        if (CanContinue) then
        begin
          Inc(Result);
        end;
      end;
    until (not CanContinue);

    (*
    while ((Result < FCount) and (PumlcMsgItemRec(FList^[Result])^.Item <> AItem)) do
    begin
      Inc(Result);
    end;
    *)

    if (Result = FCount) then
    begin
      Result := IndexNotFound;
    end;
  end;
end;

function TUMLCMsgClientList.First(): IUMLCMessageClient;
var Link:PUMLCMsgItemRec;
begin
  Link :=PUMLCMsgItemRec(InternalFirst());
  Result := Link^.Item;
end;

function TUMLCMsgClientList.Last(): IUMLCMessageClient;
var Link:PUMLCMsgItemRec;
begin
  Link :=PUMLCMsgItemRec(InternalLast());
  Result := Link^.Item;
end;

function TUMLCMsgClientList.Insert(const AItem: IUMLCMessageClient): Integer;
var Link:PUMLCMsgItemRec;
begin
  Link   := InternalCreateItem();
  Link^.Item := AItem;
  Result := InternalInsert(Link);
end;

procedure TUMLCMsgClientList.InsertAt(const AIndex: Integer; const AItem: IUMLCMessageClient);
var Link:PUMLCMsgItemRec;
begin
  Link := InternalCreateItem();
  Link^.Item := AItem;
  InternalInsertAt(AIndex, Link);
end;

function TUMLCMsgClientList.Remove(const AItem: IUMLCMessageClient): Integer;
begin
  Result := IndexOf(AItem);
  
  if (Result >= 0) then
  begin
    DeleteAt(Result);
  end;
end;

function TUMLCMsgClientList.Extract(const AIndex: Integer): IUMLCMessageClient;
var Link:PUMLCMsgItemRec;
begin
  Result := nil;
  if (AIndex >= Count) then
  begin
//  Link := getInternalItems(Index);
    Link :=PUMLCMsgItemRec(InternalLast());
    Result := Link^.Item;
    FList^[AIndex] := nil;

//  Notify(Link, lnExtracted);
    DeleteAt(AIndex);
  end;
end;

function TUMLCMsgClientList.FirstThat
  (const Func: TUMLCMsgClientFirstThat; AParam: Pointer): IUMLCMessageClient;
var AIndex: Integer; Found: Boolean; AItem: IUMLCMessageClient;
begin
  AIndex := 0; Found := FALSE; AItem := nil;
  while ((AIndex < Count) and not Found) do
  begin
    AItem  := Items[AIndex];
    Found := Func(AItem, AParam);
    Inc(AIndex);
  end;

  if (Found)
    then Result := AItem
    else Result := nil;
  // Goal: Returns the index of the item that follows the given predicate.
  // Objetivo: Regresa el indice del elemento que siga el predicado dado.
end;

procedure TUMLCMsgClientList.ForEach
  (const Proc: TUMLCMsgClientForEach; const AParam: pointer);
var AIndex: Integer; AItem: IUMLCMessageClient;
begin
  for AIndex := 0 to Pred(Count) do
  begin
    AItem := Items[AIndex];
    Proc(AItem, AParam);
  end;
end;

procedure TUMLCMsgClientList.ForBack
  (const Proc: TUMLCMsgClientForEach; const AParam: pointer);
var AIndex: Integer; AItem: IUMLCMessageClient;
begin
  for AIndex := Pred(Count) downto 0 do
  begin
    AItem := Items[AIndex];
    Proc(AItem, AParam);
  end;
end;

(* TUMLCMessageHandlerList *)

function TUMLCMessageHandlerList.getItems
  (AIndex: Integer):PUMLCMessageHandlerRecord;
begin
  Result :=PUMLCMessageHandlerRecord(getInternalItems(AIndex));
end;

procedure TUMLCMessageHandlerList.setItems
  (AIndex: Integer; AItem:PUMLCMessageHandlerRecord);
var ARec: pointer;
begin
  ARec := AItem;
  setInternalItems(AIndex, ARec)
end;

function TUMLCMessageHandlerList.MatchesMessage
  (const AItem:PUMLCMessageHandlerRecord; const AParam: Pointer): Boolean;
var AMessage:PUMLCMessage;
begin
  Result := false;
  AMessage :=PUMLCMessage(AParam);
  Result := umlcguids.AreEqualGUID(AItem^.Message, AMessage^);
end;

constructor TUMLCMessageHandlerList.Create();
begin
  RecordSize := System.SizeOf(TUMLCMessageHandlerRecord)
end;

function TUMLCMessageHandlerList.Insert
  (const AMsg: TUMLCMessage; AHandler: TUMLCMsgEventHandler): Integer;
var ARec:PUMLCMessageHandlerRecord;
begin
  ARec   := InternalCreateItem();
  ARec^.Message := AMsg;
  ARec^.Handler := AHandler;
  Result := InternalInsert(ARec);
end;

function TUMLCMessageHandlerList.FirstThat
  (const Func: TUMLCMessageHandlerFirstThat; AParam: Pointer):PUMLCMessageHandlerRecord;
var AIndex: Integer; Found: Boolean; AItem:PUMLCMessageHandlerRecord;
begin
  AIndex := 0; Found := FALSE; AItem := nil;
  while ((AIndex < Count) and not Found) do
  begin
    AItem  := Items[AIndex];
    Found := Func(AItem, AParam);
    Inc(AIndex);
  end;

  if (Found)
    then Result := AItem
    else Result := nil;
  // Goal: Returns the index of the item that follows the given predicate.
  // Objetivo: Regresa el indice del elemento que siga el predicado dado.
end;

function TUMLCMessageHandlerList.HandlerOf
  (const AMsg: TUMLCMessage): TUMLCMsgEventHandler;
var ARec:PUMLCMessageHandlerRecord;
begin
  Result := nil;

  ARec := FirstThat(@MatchesMessage, @AMsg);
  if (ARec <> nil) then
  begin
    Result := ARec^.Handler;
  end;
end;

(* TUMLCCurrentMsgClientList *)

function TUMLCCurrentMsgClientList.getCurrentIndex(): Integer;
begin
  Result := FCurrentIndex;
end;

procedure TUMLCCurrentMsgClientList.setCurrentIndex(const AItem: Integer);
begin
  FCurrentIndex := AItem;
end;

end.
