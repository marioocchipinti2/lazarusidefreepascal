(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

(* "umlctools_language.incpas" *)

{$UNDEF umlctools_language_english}
{$UNDEF umlctools_language_spanisheurope}
{$UNDEF umlctools_language_spanishlatam}
{$UNDEF umlctools_language_french}
{$UNDEF umlctools_language_german}
{$UNDEF umlctools_language_portuguese}
{$UNDEF umlctools_language_italian}

{$DEFINE umlctools_language_english}
{.DEFINE umlctools_language_spanisheurope}
{.DEFINE umlctools_language_spanishlatam}
{.DEFINE umlctools_language_french}
{.DEFINE umlctools_language_german}
{.DEFINE umlctools_language_portuguese}
{.DEFINE umlctools_language_italian}
