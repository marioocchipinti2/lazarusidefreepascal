(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcactcntrls;

interface
uses
  SysUtils,
  Classes,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcnormobjects,
  umlccomponents,
  dummy;

  // Objetivo:
  // Esta unidad proporciona la interface "IUMLCActivatedControl".
  // Esta interface permite controlar algunas operaciones,
  // en controles visuales y componentes no visuales,
  // cuando la forma ha sido activada visualmente, por primera vez,
  // y se tiene que hacer alguna operacion en especifico.

  // Ejemplo:
  // procedure TMyForm.FormActivate(Sender: TObject);
  // begin
  //   if (not ActivadaPreviamente()) then
  //   begin
  //     MyControl.ActivateFirst();
  //   end;
  // end;

  // Goal:
  // This unit provides the "IUMLCActivatedControl" interface.
  // This interface allows to control some operations,
  // in visual controls, & non visual components,
  // when then form has been activated visually, for the first time,
  // and some specific operations must be perform.

  // Example:
  // procedure TMyForm.FormActivate(Sender: TObject);
  // begin
  //   if (not PreviouslyActivated()) then
  //   begin
  //     MyControl.ActivateFirst();
  //   end;

 const

   // ---

   ID_IUMLCActivatedControl : TUMLCType =
     ($2F,$D9,$52,$F1,$87,$58,$87,$48,$88,$2A,$F6,$6C,$08,$9F,$48,$9F);

   ID_TUMLCActivatedPersistent : TUMLCType =
     ($65,$1C,$F7,$03,$C3,$7B,$93,$4B,$90,$F2,$64,$B5,$F1,$FB,$C6,$BE);

   ID_TUMLCActivatedComponent : TUMLCType =
     ($C9,$0A,$5E,$BE,$FD,$9B,$45,$4E,$BA,$FA,$32,$B8,$E6,$31,$10,$50);

   ID_TUMLCNormalizedComponent : TUMLCType =
     ($41,$BA,$19,$C7,$84,$92,$7E,$45,$AD,$5B,$7A,$6C,$09,$DE,$68,$A1);

   // ---

type

(* IUMLCActivatedControl *)

  IUMLCActivatedControl = interface
    ['{2FD952F1-8758-8748-882A-F66C089F489F}']

    (* Private declarations *)

    (* Protected declarations *)

    function getActivated(): Boolean;
    procedure setActivated(const Value: Boolean);

    (* Protected declarations *)

    // after the form has been activated, for the first time,
    // what to do
    procedure ActivateFirst(); // virtual;

    // before the form is going to be deactivated,
    // for the last time, and still active, what to do
    procedure DeactivateLast(); // virtual;

    // non first activation
    procedure Activate(); //virtual;

    // non last activation
    procedure Deactivate(); //virtual;

    (* Public declarations *)

    property Activated: Boolean
      read getActivated write setActivated;
  end;
  // Goal: Returns if a visual control has been activated once.
  // Objetivo: Regresa si un control visual ha sido activado alguna vez.

(* TUMLCActivatedPersistent *)

  //TUMLCActivatedPersistent = class(TUMLCNormalizedPersistent, IUMLCActivatedControl)

  //end;

  TUMLCActivatedPersistent =
    class(TUMLCExtendedNormalizedObject, IUMLCActivatedControl)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FActivated: Boolean;

    function getActivated(): Boolean;
    procedure setActivated(const Value: Boolean);
  protected
    (* Protected declarations *)

    procedure ActivateFirst(); virtual;
    procedure DeactivateLast(); virtual;

    procedure Activate(); virtual;
    procedure Deactivate(); virtual;
  public
    (* Public declarations *)

    property Activated: Boolean
      read getActivated write setActivated;
  end;

(* TUMLCActivatedComponent *)

  TUMLCActivatedComponent =
    class(TUMLCNormComponent, IUMLCActivatedControl)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FActivated: Boolean;

    function getActivated(): Boolean;
    procedure setActivated(const Value: Boolean);
  protected
    (* Protected declarations *)

    procedure ActivateFirst(); virtual;
    procedure DeactivateLast(); virtual;

    procedure Activate(); virtual;
    procedure Deactivate(); virtual;
  public
    (* Public declarations *)

    property Activated: Boolean
      read getActivated write setActivated;
  end;

implementation

(* TUMLCActivatedPersistent *)

function TUMLCActivatedPersistent.getActivated(): Boolean;
begin
  Result :=
    Self.FActivated;
  // Goal: "Activated" property get method.
  // Objetivo: Metodo lectura para propiedad "Activated".
end;

procedure TUMLCActivatedPersistent.setActivated(const Value: Boolean);
begin
  if (FActivated <> Value) then
  begin
    FActivated := Value;
    if (Value) then
    begin
      Self.ActivateFirst()
    end else
    begin
      Self.DeActivateLast();
    end;
  end;
  // Goal: "Activated" property set method.
  // Objetivo: Metodo escritura para propiedad "Activated".
end;

procedure TUMLCActivatedPersistent.ActivateFirst();
begin
  // Goal: Performa an specific action when the control is activated
  // by the first time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // activado la primera vez.
end;

procedure TUMLCActivatedPersistent.DeactivateLast();
begin
  // Goal: Performa an specific action when the control is dectivated
  // by the last time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // deactivado por ultima vez.
end;

procedure TUMLCActivatedPersistent.Activate();
begin
  // Goal: Performa an specific action when the control is activated
  // by the first time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // activado la primera vez.
end;

procedure TUMLCActivatedPersistent.Deactivate();
begin
  // Goal: Performa an specific action when the control is dectivated
  // by the last time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // deactivado por ultima vez.
end;

(* TUMLCActivatedComponent *)

function TUMLCActivatedComponent.getActivated(): Boolean;
begin
  Result :=
    Self.FActivated;
  // Goal: "Activated" property get method.
  // Objetivo: Metodo lectura para propiedad "Activated".
end;

procedure TUMLCActivatedComponent.setActivated(const Value: Boolean);
begin
  if (Self.FActivated <> Value) then
  begin
    Self.FActivated := Value;
    if (Value) then
    begin
      Self.ActivateFirst()
    end else
    begin
      Self.DeActivateLast();
    end;
  end;
  // Goal: "Activated" property set method.
  // Objetivo: Metodo escritura para propiedad "Activated".
end;

procedure TUMLCActivatedComponent.ActivateFirst();
begin
  // Goal: Performa an specific action when the control is activated
  // by the first time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // activado la primera vez.
end;

procedure TUMLCActivatedComponent.DeactivateLast();
begin
  // Goal: Performa an specific action when the control is dectivated
  // by the last time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // deactivado por ultima vez.
end;

procedure TUMLCActivatedComponent.Activate();
begin
  // Goal: Performa an specific action when the control is activated
  // by the first time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // activado la primera vez.
end;

procedure TUMLCActivatedComponent.Deactivate();
begin
  // Goal: Performa an specific action when the control is dectivated
  // by the last time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // deactivado por ultima vez.
end;

end.
