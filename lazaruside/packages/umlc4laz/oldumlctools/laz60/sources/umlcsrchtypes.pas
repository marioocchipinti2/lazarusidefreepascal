(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsrchtypes;

interface

type

{ TUMLCSearchResult }

  TUMLCSearchResult = (srrNone, srrCancel, srrSearch, srrReplace, srrReplaceALL);

{ TUMLCSearchOptions }

  TUMLCSearchOption  = Byte;
  TUMLCSearchOptions = set of TUMLCSearchOption;

{ TUMLCSearchStatus }

  TUMLCSearchStatusItem = Byte;
  TUMLCSearchStatus = set of TUMLCSearchStatusItem;

{ TUMLCSearchDirection }

  TUMLCSearchDirection = (srdForward, srdBackward);

{ TUMLCSearchScope }

  TUMLCSearchScope = (srscpGlobal, srscpSelectedText);

{ TUMLCSearchFileScope }

  TUMLCSearchFileScope = (srscpFilename, srscpFileExt);

{ TUMLCSearchOrigin }

  TUMLCSearchOrigin = (sropFromCursor, sropEntireScope);

const

{ TUMLCSearchOptions }

  sropShowReplaceAll         = 01;
  sropShowHelp               = 02;

  sropShowCaseSensitive      = 03;
  sropShowWholeWordsOnly     = 04;
  sropShowRegularExpressions = 05;

  sropShowPromptOnReplace    = 06;
  sropShowDeleteOnReplace    = 07;
  sropShowKeepCapitalCase    = 08;

{ TUMLCSearchStatus }

  srstWantCaseSensitive      = 01;
  srstWantWholeWordsOnly     = 02;
  srstWantRegularExpressions = 03;

  srstWantPromptOnReplace    = 04;
  srstWantDeleteOnReplace    = 05;
  srstWantKeepCapitalCase    = 06;

implementation

end.
