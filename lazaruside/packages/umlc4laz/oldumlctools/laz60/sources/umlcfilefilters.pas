(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfilefilters;

interface
uses
  SysUtils, Classes,
  dummy;

const
  IndexNotFound = -1;

type

(* TUMLCFileFilter *)

  TUMLCFileFilter =
    class(TCollectionItem)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FText: string;
    FExtension: string;
  public
    (* public declarations *)

    function Wildcard(): string;
    function Filter(): string;

    constructor Create(ACollection: TCollection); override;
    destructor Destroy(); override;
  published
    (* published declarations *)

    property Text: string
      read FText write FText;
    property Extension: string
      read FExtension write FExtension;
  end;

(* TUMLCFileFilters *)

  TUMLCFileFilters =
    class(TOwnedCollection)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function Filter: string;
    function FilterByExt(const Extension: string): Integer;
    function FilterByIndex(const Index: Integer): string;
    function ExtByIndex(const Index: Integer): string;

    constructor Create(AOwner: TPersistent; AItemClass: TCollectionItemClass);
    destructor Destroy(); override;
  published
    (* published declarations *)
  end;

(* TCustomUMLCFileFiltersContainer *)

  TCustomUMLCFileFiltersContainer =
    class(TComponent)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FItems: TUMLCFileFilters;
  public
    (* public declarations *)

    function Filter(): string;

    function LoadFilter(const Ext, Text: string): TUMLCFileFilter;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;

    property Items: TUMLCFileFilters
      read FItems write FItems;
  end;

(* TUMLCFileFiltersContainer *)

  TUMLCFileFiltersContainer =
    class(TCustomUMLCFileFiltersContainer)
  published
    (* published declarations *)

    property Items;
  end;

implementation

(* TUMLCFileFilter *)

function TUMLCFileFilter.Wildcard(): string;
begin
  Result := '*.' + FExtension;
end;

function TUMLCFileFilter.Filter(): string;
begin
  Result := FText + ' (' + Wildcard + ')' + '|' + Wildcard;
end;

constructor TUMLCFileFilter.Create(ACollection: TCollection);
begin
  inherited Create(ACollection);
  FText := '';
  FExtension := '';
end;

destructor TUMLCFileFilter.Destroy();
begin
  FText := '';
  FExtension := '';
  inherited Destroy();
end;

(* TUMLCFileFilters *)

function TUMLCFileFilters.Filter: string;
var AIndex, ACount: Integer; Item: TUMLCFileFilter;
begin
  Result := '';
  ACount := Pred(Count);
  for AIndex := 0 to ACount do
  begin
    Item := (Items[AIndex] as TUMLCFileFilter);
    if (AIndex = 0)
      then Result := Item.Filter
      else Result := Result + '|' + Item.Filter;
  end;
end;

function TUMLCFileFilters.FilterByExt(const Extension: string): Integer;
var Index: Integer; Found: Boolean; EachFilter: TUMLCFileFilter;
begin
  Result := IndexNotFound;
  Index := 0; Found := FALSE;
  while ((Index < Count) and (not Found)) do
  begin
    EachFilter := (Items[Index] as TUMLCFileFilter);
    Found  := SysUtils.AnsiSameText(Extension, EachFilter.Extension);
    Inc(Index);
  end;

  if (Found)
    then Result := Pred(Index);
end;

function TUMLCFileFilters.FilterByIndex(const Index: Integer): string;
begin
  Result := (Items[Index] as TUMLCFileFilter).Filter;
end;

function TUMLCFileFilters.ExtByIndex(const Index: Integer): string;
begin
  Result := (Items[Index] as TUMLCFileFilter).Extension;
end;

constructor TUMLCFileFilters.Create(AOwner: TPersistent; AItemClass: TCollectionItemClass);
begin
  inherited Create(AOwner, AItemClass);
  // Your Code...
end;

destructor TUMLCFileFilters.Destroy();
begin
  // Your Code...
  inherited Destroy();
end;

(* TCustomUMLCFileFiltersContainer *)

function TCustomUMLCFileFiltersContainer.Filter(): string;
begin
  Result := (FItems as TUMLCFileFilters).Filter;
end;

function TCustomUMLCFileFiltersContainer.LoadFilter
  (const Ext, Text: string): TUMLCFileFilter;
var Item: TUMLCFileFilter;
begin
  Item := (Items.Add as TUMLCFileFilter);
  Item.Text := Text;
  Item.Extension := Ext;

  Result := Item;
end;

constructor TCustomUMLCFileFiltersContainer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FItems := TUMLCFileFilters.Create(Self, TUMLCFileFilter);
end;

destructor TCustomUMLCFileFiltersContainer.Destroy();
begin
  FItems.Free;
  inherited Destroy();
end;

end.


