readme.txt
==========

The "umlctools" folder contains the UMLCat set of libraries for the FreePascal &
Lazarus Programming Framework.

This package contains basic units with some non-visual components,
used by other classes, in other packages, with more advanced features.

