(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansicharbyptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcansichars,
  dummy;

// ---

const

 MOD_umlcansicharbyptrs : TUMLCModule =
   ($D9,$2F,$30,$5B,$04,$0A,$A2,$4A,$BD,$17,$5B,$8C,$EA,$6A,$A2,$34);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: ansichar): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: ansichar): pointer;
begin
  System.GetMem(Result, sizeof(ansichar));
end;

procedure DropPtr
  (var ADestPtr: pointer);
var ADestValuePtr: pansichar;
begin
  ADestValuePtr := ADestPtr;
  System.Dispose(ADestValuePtr);
end;


end.

