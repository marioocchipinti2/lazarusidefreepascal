(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansicharsets;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to emulate a character set using a string.
 ** The character encoding is A.N.S.I.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcansichars,
  dummy;

// ---

const

 MOD_umlcansicharsets : TUMLCModule =
  ($F9,$F9,$B0,$95,$51,$D6,$FE,$4E,$B3,$27,$32,$CA,$D8,$E3,$69,$32);

// ---

(* global constants *)

const
  MemberNotFound = -1;

(* global functions *)

  function IsEmpty
    (const ASet: umlcansicharset): Boolean; overload;

  function IsMember
    (const ASet: umlcansicharset; const AItem: umlcansichar): Boolean; overload;

  function IsSameMember
    (const ASet: umlcansicharset; const AItem: umlcansichar): Boolean; overload;

  function RangeToSetCopy
    (const ALo, AHi: umlcansichar): umlcansicharset; overload;

(* global procedures *)

  procedure Clear
    (out ASet: umlcansicharset); overload;

  procedure Include
    (var ASet: umlcansicharset; const AItem: umlcansichar); overload;
  procedure Exclude
    (var ASet: umlcansicharset; const AItem: umlcansichar); overload;

  procedure IncludeRange
    (var ASet: umlcansicharset; ALo, AHi: umlcansichar); overload;
  procedure ExcludeRange
    (var ASet: umlcansicharset; ALo, AHi: umlcansichar); overload;

  function ReplaceCharSetByCharCopy
    (const AHaystack: umlcansicharset;
     const ASourceNeedle: umlcansichar;
     const ADestNeedle: umlcansichar): umlcansichar;

(* global operators *)

  procedure Assign
    (out   ADest:   umlcansicharset;
     const ASource: umlcansicharset); overload; // operator :=


implementation

(* global functions *)

function IsEmpty
  (const ASet: umlcansicharset): Boolean;
begin
  Result :=
    (Ord(ASet[0]) = 0);
end;

function IsMember
  (const ASet: umlcansicharset; const AItem: umlcansichar): Boolean; overload;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found := (ASet[Index] = AItem);
    Inc(Index);
  end;
  Result := Found;
end;

function IsSameMember
  (const ASet: umlcansicharset; const AItem: umlcansichar): Boolean; overload;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found :=
      umlcansichars.SameText(umlcansichars.LowercaseCopy(ASet[Index]), umlcansichars.LowercaseCopy(AItem));
    Inc(Index);
  end;
  Result := Found;
end;

function RangeToSetCopy
  (const ALo, AHi: umlcansichar): umlcansicharset;
var AItem, L, H: umlcansichar;
begin
  Result := '';

  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  for AItem := L to H do
  begin
    umlcansicharsets.Include(Result, AItem);
  end;
end;

(* global procedures *)

procedure Clear
  (out ASet: umlcansicharset);
begin
  System.FillChar(ASet, sizeof(ASet), #0);
end;

procedure Include
  (var ASet: umlcansicharset; const AItem: umlcansichar);
var AIndex: Byte;
begin
  AIndex := Ord(ASet[0]);
  ASet[Succ(AIndex)] := AItem;
  ASet[0] := chr(Succ(AIndex));
end;

procedure Exclude
  (var ASet: umlcansicharset; const AItem: umlcansichar);
var I, Count, Last: Byte; Temp: umlcansicharset;
begin
  umlcansicharsets.Clear(Temp);
  Count :=
    Ord(ASet[0]);
  Last := 0;
  for I := 0 to Pred(Count) do
  begin
    if (ASet[I] <> AItem) then
    begin
      Temp[Last] := AItem;
      Inc(Last);
    end;
  end;

  System.Move(Temp, ASet, sizeof(ASet));
  ASet[0] := chr(Succ(Last));
end;

procedure IncludeRange
  (var ASet: umlcansicharset; ALo, AHi: umlcansichar);
var AItem, L, H: umlcansichar;
begin
  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  for AItem := L to H do
  begin
    Include(ASet, AItem);
  end;
end;

procedure ExcludeRange
  (var ASet: umlcansicharset; ALo, AHi: umlcansichar);
var AItem, L, H: umlcansichar;
begin
  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  for AItem := L to H do
  begin
    Exclude(ASet, AItem);
  end;
end;

function ReplaceCharSetByCharCopy
  (const AHaystack: umlcansicharset;
   const ASourceNeedle: umlcansichar;
   const ADestNeedle: umlcansichar): umlcansichar;
begin
  { to-do...}
  if (umlcansicharsets.IsMember(AHaystack, ASourceNeedle))
    then Result := ADestNeedle
    else Result := ASourceNeedle;
  // Goal: Replace a specific character set.
  // Objetivo: Reemplazar un conjunto caracter en especifico.
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcansicharset;
   const ASource: umlcansicharset);
begin
  System.Move(ASource, ADest, sizeof(ADest));
end;


end.
