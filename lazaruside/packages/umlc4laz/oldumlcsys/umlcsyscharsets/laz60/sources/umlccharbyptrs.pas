(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccharbyptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcchars,
  dummy;

// ---

const

 MOD_umlccharbyptrs : TUMLCModule =
   ($5C,$EF,$A1,$6E,$32,$DA,$28,$43,$A2,$D0,$45,$63,$D6,$F5,$7E,$69);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: char): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: char): pointer;
var ADestValuePtr: pchar;
begin
  System.GetMem(ADestValuePtr, sizeof(char));
  ADestValuePtr^ := AValue;
  Result :=
    ADestValuePtr;
end;

procedure DropPtr
  (var ADestPtr: pointer);
var ADestValuePtr: pchar;
begin
  ADestValuePtr := ADestPtr;
  System.Dispose(ADestValuePtr);
end;

end.

