(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwidecharsets;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to emulate a character set using a string.
 ** The character encoding is A.N.S.I.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcwidechars,
  dummy;

// ---

const

 MOD_umlcwidecharsets : TUMLCModule =
  ($B2,$F0,$A3,$C8,$F0,$DC,$AA,$46,$B1,$65,$37,$F3,$8F,$39,$F5,$1E);

// ---

(* global constants *)

const
  MemberNotFound = -1;

(* global functions *)

  function IsEmpty
    (const ASet: umlcwidecharset): Boolean; overload;

  function IsMember
    (const ASet: umlcwidecharset; const AItem: umlcwidechar): Boolean; overload;

  function IsSameMember
    (const ASet: umlcwidecharset; const AItem: umlcwidechar): Boolean; overload;

  function RangeToSetCopy
    (const ALo, AHi: umlcwidechar): umlcwidecharset; overload;

(* global procedures *)

  procedure Clear
    (out ASet: umlcwidecharset); overload;

  procedure Include
    (var ASet: umlcwidecharset; const AItem: umlcwidechar); overload;
  procedure Exclude
    (var ASet: umlcwidecharset; const AItem: umlcwidechar); overload;

  procedure IncludeRange
    (var ASet: umlcwidecharset; ALo, AHi: umlcwidechar); overload;
  procedure ExcludeRange
    (var ASet: umlcwidecharset; ALo, AHi: umlcwidechar); overload;

  function ReplaceCharSetByCharCopy
    (const AHaystack: umlcwidecharset;
     const ASourceNeedle: umlcwidechar;
     const ADestNeedle: umlcwidechar): umlcwidechar;

(* global operators *)

  procedure Assign
    (out   ADest:   umlcwidecharset;
     const ASource: umlcwidecharset); overload; // operator :=


implementation

(* global functions *)

function IsEmpty
  (const ASet: umlcwidecharset): Boolean;
begin
  Result :=
    (Ord(ASet[0]) = 0);
end;

function IsMember
  (const ASet: umlcwidecharset; const AItem: umlcwidechar): Boolean; overload;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found := (ASet[Index] = AItem);
    Inc(Index);
  end;
  Result := Found;
end;

function IsSameMember
  (const ASet: umlcwidecharset; const AItem: umlcwidechar): Boolean; overload;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found :=
      umlcwidechars.SameText(umlcwidechars.LowercaseCopy(ASet[Index]), umlcwidechars.LowercaseCopy(AItem));
    Inc(Index);
  end;
  Result := Found;
end;

function RangeToSetCopy
  (const ALo, AHi: umlcwidechar): umlcwidecharset;
var AItem, L, H: umlcwidechar;
begin
  Result := '';

  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  for AItem := L to H do
  begin
    umlcwidecharsets.Include(Result, AItem);
  end;
end;

(* global procedures *)

procedure Clear
  (out ASet: umlcwidecharset);
begin
  System.FillChar(ASet, sizeof(ASet), #0);
end;

procedure Include
  (var ASet: umlcwidecharset; const AItem: umlcwidechar);
var AIndex: Byte;
begin
  AIndex := Ord(ASet[0]);
  ASet[Succ(AIndex)] := AItem;
  ASet[0] := chr(Succ(AIndex));
end;

procedure Exclude
  (var ASet: umlcwidecharset; const AItem: umlcwidechar);
var I, Count, Last: Byte; Temp: umlcwidecharset;
begin
  umlcwidecharsets.Clear(Temp);
  Count :=
    Ord(ASet[0]);
  Last := 0;
  for I := 0 to Pred(Count) do
  begin
    if (ASet[I] <> AItem) then
    begin
      Temp[Last] := AItem;
      Inc(Last);
    end;
  end;

  System.Move(Temp, ASet, sizeof(ASet));
  ASet[0] := chr(Succ(Last));
end;

procedure IncludeRange
  (var ASet: umlcwidecharset; ALo, AHi: umlcwidechar);
var AItem, L, H: umlcwidechar;
begin
  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  for AItem := L to H do
  begin
    Include(ASet, AItem);
  end;
end;

procedure ExcludeRange
  (var ASet: umlcwidecharset; ALo, AHi: umlcwidechar);
var AItem, L, H: umlcwidechar;
begin
  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  for AItem := L to H do
  begin
    Exclude(ASet, AItem);
  end;
end;

function ReplaceCharSetByCharCopy
  (const AHaystack: umlcwidecharset;
   const ASourceNeedle: umlcwidechar;
   const ADestNeedle: umlcwidechar): umlcwidechar;
begin
  { to-do...}
  if (umlcwidecharsets.IsMember(AHaystack, ASourceNeedle))
    then Result := ADestNeedle
    else Result := ASourceNeedle;
  // Goal: Replace a specific character set.
  // Objetivo: Reemplazar un conjunto caracter en especifico.
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcwidecharset;
   const ASource: umlcwidecharset);
begin
  System.Move(ASource, ADest, sizeof(ADest));
end;


end.
