(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcchars;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to support the predefined "char" type.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
{$IFDEF MSWINDOWS}
  Windows, //Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  dummy;

// ---

const

  MOD_umlcchars : TUMLCModule =
    ($82,$6F,$92,$C4,$E4,$4D,$E7,$44,$80,$AE,$E6,$86,$74,$DA,$89,$E5);

// ---

(* global standard functions *)

  function IsNull
    (const ASource: char): Boolean; overload;
  function IsAlpha
    (const ASource: char): Boolean; overload;
  function IsDigit
    (const ASource: char): Boolean; overload;
  function IsSpace
    (const ASource: char): Boolean; overload;
  function IsBlank
    (const ASource: char): Boolean; overload;

  function IsDecimal
    (const ASource: char): Boolean; overload;
  function IsHexa
    (const ASource: char): Boolean; overload;
  function IsOctal
    (const ASource: char): Boolean; overload;
  function IsBinary
    (const ASource: char): Boolean; overload;

  function IsUppercase
    (const ASource: char): Boolean; overload;
  function IsLowercase
    (const ASource: char): Boolean; overload;

(* global additional functions *)

  function CharToOrd
    (const ASource: char): Integer; overload;
  function OrdToChar
    (const ASource: Integer): char; overload;

  function UppercaseCopy
    (const ASource: char): char; overload;
  function LowercaseCopy
    (const ASource: char): char; overload;
  function TogglecaseCopy
    (const ASource: char): char; overload;

  function SameText
    (const A, B: char): Boolean; overload;

  function IfChar
    (const APredicate: Boolean; const A, B: char): char;

  function ReplaceCopy
    (const ASource: char; const A, B: char): char; overload;

(* global operators *)

  procedure Assign
    (out   ADest: char;
     const ASource: char); overload; // operator :=

  function Greater
    (const A, B: char): Boolean; overload; // operator >
  function Lesser
    (const A, B: char): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: char): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: char): Boolean; overload; // operator <=

  function Compare
    (const A, B: char): umlctcomparison;

  function Different
    (const A, B: char): Boolean; overload; // operator <>

  function Equal
    (const A, B: char): Boolean; overload; // operator =

(* global procedures *)

  procedure Clear
    (out ADest: char); overload;

  procedure UppercaseReplace
    (out ADest: char); overload;
  procedure LowercaseReplace
    (out ADest: char); overload;
  procedure TogglecaseReplace
    (out ADest: char); overload;

implementation

(* global standard functions *)

function IsNull
  (const ASource: char): Boolean;
begin
  Result :=
    (ASource = ansinullchar);
end;

function IsAlpha
  (const ASource: char): Boolean;
begin
  Result :=
    Windows.IsCharAlphaA(ASource);
end;

function IsDigit
  (const ASource: char): Boolean;
begin
  Result :=
    ((ASource >= '0') and (ASource <= '9'));
end;

function IsSpace
  (const ASource: char): Boolean;
begin
  Result :=
    (Ord(ASource) = 32);
end;

function IsBlank
  (const ASource: char): Boolean;
begin
  Result :=
    (Ord(ASource) = 32) or // space
    (Ord(ASource) = 13) or // D.O.S. carriage-return
    (Ord(ASource) = 10) or // D.O.S. line-feed
    (Ord(ASource) = 12) or // page break
    (Ord(ASource) = 08);   // tabulator
end;

function IsDecimal
  (const ASource: char): Boolean;
begin
  Result :=
    ((ASource >= '0') and (ASource <= '9'));
end;

function IsHexa
  (const ASource: char): Boolean;
begin
  Result :=
    (ASource >= '0') and (ASource <= '9') or
    (ASource >= 'A') and (ASource <= 'F') or
    (ASource >= 'a') and (ASource <= 'f');
end;

function IsOctal
  (const ASource: char): Boolean;
begin
  Result :=
    (ASource >= '0') and (ASource <= '7');
end;

function IsBinary
  (const ASource: char): Boolean;
begin
  Result :=
    (ASource = '0') or (ASource = '1');
end;

function IsUppercase
  (const ASource: char): Boolean;
begin
  Result :=
    Windows.IsCharUpperA(ASource);
end;

function IsLowercase
  (const ASource: char): Boolean;
begin
  Result :=
    Windows.IsCharLowerA(ASource);
end;

(* global additional functions *)

function CharToOrd
  (const ASource: char): Integer;
begin
  Result :=
    Ord(ASource);
end;

function OrdToChar
  (const ASource: Integer): char;
begin
  Result :=
    Chr(ASource);
end;

function UppercaseCopy
  (const ASource: char): char;
begin
  Result :=
    ASource;
  Windows.CharUpperBuffA(@Result, 1);
  // Goal: Returns a uppercase copy of the given character.
  // Objetivo: Regresa una copia en mayusculas del caracter dado.
end;

function LowercaseCopy
  (const ASource: char): char;
begin
  Result := ASource;
  Windows.CharLowerBuffA(@Result, 1);
  // Goal: Returns a lowercase copy of the given character.
  // Objetivo: Regresa una copia en minusculas del caracter dado.
end;

function TogglecaseCopy
  (const ASource: char): char;
begin
  if (IsUppercase(ASource))
    then Result := LowercaseCopy(ASource)
    else Result := UppercaseCopy(ASource);
  // Goal: Returns a switched case copy of the given character.
  // Objetivo: Regresa una copia en caso intercambiado del caracter dado.
end;

function SameText
  (const A, B: char): Boolean;
begin
  Result :=
    SysUtils.SameText(A, B);
  // Goal: Returns if 2 characters are equal, ignores sensitive case.
  // Objetivo: Regresa si 2 caracteres son iguales, ignorar caso sensitivo.
end;

function IfChar
  (const APredicate: Boolean; const A, B: char): char;
begin
  if (APredicate)
    then Result := A
    else Result := B;
  // Objetivo: Segun la condicion, regresar el caracter seleccionado.
  // Goal: Upon condition, return the select character.
end;

function ReplaceCopy
  (const ASource: char; const A, B: char): char;
begin
  if (ASource = A)
    then Result := B
    else Result := ASource;
  // Objetivo: Reemplazar un caracter en especifico.
  // Goal: Upon condition, return the select character.
end;

(* global operators *)

procedure Assign
  (out   ADest: char;
   const ASource: char);
begin
  ADest := ASource;
end;

function Greater
  (const A, B: char): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: char): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: char): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: char): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

function Compare
  (const A, B: char): umlccomparison;
begin
  Result := cmpEqual;
  if (A < B)
    then Result := cmpLower
  else if (A > B)
    then Result := cmpHigher;
end;

function Different
  (const A, B: char): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Equal
  (const A, B: char): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

(* global procedures *)

procedure Clear
  (out ADest: char);
begin
  ADest :=
    ansinullchar;
  // Goal: Clear a character.
  // Objetivo: Limpia un caracter.
end;

procedure UppercaseReplace
  (out ADest: char);
begin
  Windows.CharUpperBuffA(@ADest, 1);
  // Goal: Changes the given character into uppercase.
  // Objetivo: Cambia el caracter dado a mayusculas.
end;

procedure LowercaseReplace
  (out ADest: char);
begin
  Windows.CharLowerBuffA(@ADest, 1);
  // Goal: Changes the given character into lowercase.
  // Objetivo: Cambia el caracter dado a minusculas.
end;

procedure TogglecaseReplace
  (out ADest: char);
begin
  if (Windows.IsCharLowerA(ADest))
    then Windows.CharUpperBuff(@ADest, 1)
    else Windows.CharLowerBuff(@ADest, 1);
  // Goal: Changes the given character into lowercase.
  // Objetivo: Cambia el caracter dado a minusculas.
end;


end.

