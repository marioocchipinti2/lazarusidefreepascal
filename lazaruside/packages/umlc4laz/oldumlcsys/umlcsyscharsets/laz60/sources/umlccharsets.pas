(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccharsets;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to emulate a character set using a string.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcchars,
  dummy;

// ---

const

 MOD_umlccharsets : TUMLCModule =
   ($CB,$6A,$14,$44,$92,$25,$24,$4A,$A1,$23,$AA,$56,$D0,$D9,$3C,$13);

 // ---

(* global constants *)

const
  MemberNotFound = -1;

(* global functions *)

   function IsEmpty
     (const ASet: charset): Boolean; overload;

   function IsMember
     (const ASet: charset; const AItem: char): Boolean; overload;

   function IsSameMember
     (const ASet: charset; const AItem: char): Boolean; overload;

   function RangeToSetCopy
     (const ALo, AHi: char): charset; overload;

(* global procedures *)

   procedure Clear
     (out ASet: charset); overload;

   procedure Include
     (var ASet: charset; const AItem: char); overload;
   procedure Exclude
     (var ASet: charset; const AItem: char); overload;

   procedure IncludeRange
     (var ASet: charset; ALo, AHi: char); overload;
   procedure ExcludeRange
     (var ASet: charset; ALo, AHi: char); overload;

   function ReplaceCharSetByCharCopy
     (const AHaystack: charset;
      const ASourceNeedle: char;
      const ADestNeedle: char): char;

(* global operators *)

  procedure Assign
    (out   ADest:   charset;
     const ASource: charset); overload; // operator :=

implementation

(* global functions *)

function IsEmpty
  (const ASet: charset): Boolean;
begin
  Result := (ASet = '');
end;

function IsMember
  (const ASet: charset; const AItem: char): Boolean;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1;
    Count := System.Length(ASet);
  Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found := (ASet[Index] = AItem);
    Inc(Index);
  end;
  Result := Found;
end;

function IsSameMember
  (const ASet: charset; const AItem: char): Boolean; overload;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found :=
      umlcchars.SameText(umlcchars.LowercaseCopy(ASet[Index]), umlcchars.LowercaseCopy(AItem));
    Inc(Index);
  end;
  Result := Found;
end;

function RangeToSetCopy
  (const ALo, AHi: char): charset;
var AItem, L, H: char;
begin
  Result := '';

  if (ALo > AHi)
    then L := AHi
    else L := ALo;
  if (ALo > AHi)
    then H := ALo
    else H := AHi;

  for AItem := L to H do
  begin
    umlccharsets.Include(Result, AItem);
  end;
end;


(* global procedures *)

procedure Clear
  (out ASet: charset);
begin
  ASet := '';
end;

procedure Include
  (var ASet: charset; const AItem: char);
begin
  ASet := ASet + AItem;
end;

procedure Exclude
  (var ASet: charset; const AItem: char);
var I, Count, Last: Word; Temp: umlcansicharset;
begin
  ASet := '';
  Count :=
    System.Length(ASet);
  Last := 0;
  for I := 0 to Pred(Count) do
  begin
    if (ASet[I] <> AItem) then
    begin
      Temp[Last] := AItem;
      Inc(Last);
    end;
  end;

  ASet := Temp;
end;

procedure IncludeRange
  (var ASet: charset; ALo, AHi: char);
var AItem, L, H: char;
begin
  if (ALo > AHi)
    then L := AHi
    else L := ALo;
  if (ALo > AHi)
    then H := ALo
    else H := AHi;

  for AItem := L to H do
  begin
    Include(ASet, AItem);
  end;
end;

procedure ExcludeRange
  (var ASet: charset; ALo, AHi: char);
var AItem, L, H: char;
begin
  if (ALo > AHi)
    then L := AHi
    else L := ALo;
  if (ALo > AHi)
    then H := ALo
    else H := AHi;

  for AItem := L to H do
  begin
    Exclude(ASet, AItem);
  end;
end;

function ReplaceCharSetByCharCopy
  (const AHaystack: charset;
   const ASourceNeedle: char;
   const ADestNeedle: char): char;
begin
  { to-do...}
  if (umlccharsets.IsMember(AHaystack, ASourceNeedle))
    then Result := ADestNeedle
    else Result := ASourceNeedle;
  // Goal: Replace a specific character set.
  // Objetivo: Reemplazar un conjunto caracter en especifico.
end;

(* global operators *)

procedure Assign
  (out   ADest:   charset;
   const ASource: charset);
begin
  System.Move(ASource, ADest, sizeof(ADest));
end;


end.

