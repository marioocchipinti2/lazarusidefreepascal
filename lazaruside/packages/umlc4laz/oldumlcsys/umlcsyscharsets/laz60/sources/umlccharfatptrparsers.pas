(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccharfatptrparsers;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Quick Text Search and Parsing operations,
 ** using composed pointers.
 ** The character encoding is undefined.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcchars,
  umlccharsets,
  umlccharsetconsts,
  umlccharfatptrtypes,
  umlccharfatptrs,
  dummy;

// ---

const

 MOD_umlcansinullparsers : TUMLCModule =
   ($8F,$04,$4A,$15,$2F,$45,$17,$40,$80,$93,$90,$2A,$4A,$98,$B2,$FA);

// ---

(* global functions *)

function TryIsEmpty
  (const AHaystackPtr: umlccharfatptr;
   out   ADestValue:   Boolean): Boolean;

function MatchesCharSet
  (const AHaystack: umlccharfatptr; const ValidChars: ansicharset): umlccharfatptr; overload;

function IsStringInSet
  (const AHaystack: umlccharfatptr; const ValidChars: ansicharset): Boolean; overload;

function TryNullStrPosOf
  (const AHaystackPtr: umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;

function TryCompare
  (const A:          umlccharfatptr;
   const B:          umlccharfatptr;
   out   ADestValue: umlccomparison): Boolean;

function TryStartsWithEqual
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr;
   out   ADestValue:   Boolean): Boolean;

function TryStartsWithSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr;
   out   ADestValue:   Boolean): Boolean;

function TryFinishesWithEqual
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr;
   out   ADestValue:   Boolean): Boolean;

function TryFinishesWithSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr;
   out   ADestValue:   Boolean): Boolean;

function TryCharPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleChar:  char;
   out   ADestPtr:     umlccharfatptr): Boolean;

function TryCharPosOfSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleChar:  char;
   out   ADestPtr:     umlccharfatptr): Boolean;

function TryCharSetPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleSet:   charset;
   out   ADestPtr:     umlccharfatptr): Boolean;

function TryCharSetPosOfSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleSet:   charset;
   out   ADestPtr:     umlccharfatptr): Boolean;

function TryStrPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;

function TryStrPosOfSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;

function Length
  (const ASource: umlccharfatptr): Word; overload;

function IsEmpty
  (const AHaystackPtr: umlccharfatptr): Boolean;

function Compare
  (const A, B: umlccharfatptr): umlccomparison;

function StartsWithEqual
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;

function StartsWithSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;

function FinishesWithEqual
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;

function DuplicateCopy
  (const ASource: umlccharfatptr): umlccharfatptr; overload;

function TrimLeftCopy
  (const ASource: umlccharfatptr): umlccharfatptr; overload;
function TrimRightCopy
  (const ASource: umlccharfatptr): umlccharfatptr; overload;
function TrimCopy
  (const ASource: umlccharfatptr): umlccharfatptr; overload;

(* global procedures *)

procedure NullStrPosOf
  (const AHaystackPtr: umlccharfatptr;
   out   ADestPtr:     umlccharfatptr);

procedure CharPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleChar:  char;
   out   ADestPtr:     umlccharfatptr);

procedure CharPosOfSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleChar:  char;
   out   ADestPtr:     umlccharfatptr);

procedure CharSetPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleSet:   charset;
   out   ADestPtr:     umlccharfatptr);

procedure CharSetPosOfSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleSet:   charset;
   out   ADestPtr:     umlccharfatptr);

procedure StrPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr);

(* global operators *)

function Equal
  (const A, B: umlccharfatptr): Boolean; // operator =

function Different
  (const A, B: umlccharfatptr): Boolean; // operator <>



implementation

(* global functions *)

function TryIsEmpty
  (const AHaystackPtr: umlccharfatptr;
   out   ADestValue:   Boolean): Boolean;
begin
  ADestValue := false;
  Result :=
    umlccharfatptrs.Assigned(AHaystackPtr);
  if (Result) then
  begin
    ADestValue :=
      (AHaystackPtr^.Ptr = #0);
  end;
end;

function TryLength
  (const AHaystackPtr: umlccharfatptr;
   out   ADestValue:   Word): Boolean;
var P: pchar; ACount: Word;
begin
  ADestValue := 0;
  Result :=
    umlccharfatptrs.Assigned(AHaystackPtr);
  if (Result) then
  begin
    P := AHaystackPtr^.Ptr;
    ACount := 0;
    while (P^ <> #0) and (ACount < sizeof(Word)) do
    begin
      System.Inc(P);
      System.Inc(ACount);
    end;

    ADestValue :=
      System.Pred(ACount);
  end;
end;

function MatchesCharSet
  (const AHaystack: umlccharfatptr; const ValidChars: ansicharset): umlccharfatptr;
var CanContinue: Boolean;
    ASource: umlccharfatptr;
begin
  Result := nil;

  if (System.Assigned(AHaystack)) then
  begin
    ASource :=
      AHaystack;

    CanContinue := TRUE;
    while (not umlccharfatptrparsers.IsEmpty(ASource) and CanContinue) do
    begin
      CanContinue :=
        umlccharsets.IsMember(ValidChars, ASource^.Ptr^);
      Inc(ASource);
    end;

    if (not CanContinue) then
    begin
      Dec(ASource);
    end;

    Result := ASource;
  end;
  // Goal: Moves a null termnated string pointer,
  // as long as each character, matches the valid set.

  // Objetivo: Mueve un apuntador cadena terminada en nulo,
  // mientras cada caracter, coincide con el conjunto valido.
end;

function IsStringInSet
  (const AHaystack: umlccharfatptr; const ValidChars: ansicharset): Boolean;
begin
  Result :=
    (umlccharfatptrparsers.MatchesCharSet(AHaystack, ValidChars) <> AHaystack);
  // Goal: Returns if all the characters, a string are valid,
  // by been part of a set.
  // Objetivo: Regresa si todos los caracteres en una cadena son validos,
  // siendo parte de un conjunto.
end;

function TryNullStrPosOf
  (const AHaystackPtr: umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;
var P: pchar; ACount: Word; Match: Boolean;
begin
  Result :=
    umlccharfatptrs.Assigned(AHaystackPtr);
  if (Result) then
  begin
    ACount := 0;
    P := AHaystackPtr^.Ptr;
    Match := false;

    while ((not Match) and (ACount < sizeof(Word))) do
    begin
      Match :=
        (P^ = #0);

      System.Inc(P);
      System.Inc(ACount);
    end;

    Result :=
      Match;
    if (Match) then
    begin
      System.Dec(P);
      System.Dec(ACount);

     umlccharfatptrs.EncodeFatPtr
      (ADestPtr, P, ACount);
    end;
  end;
end;

function TryCompare
  (const A:          umlccharfatptr;
   const B:          umlccharfatptr;
   out   ADestValue: umlccomparison): Boolean;
var P, Q: pchar; ACount: Word;
begin
  Result :=
    (umlccharfatptrs.Assigned(A) and umlccharfatptrs.Assigned(B));
  if (Result) then
  begin
    ACount := 0;
    ADestValue := cmpEqual;
    P := A^.Ptr;
    Q := B^.Ptr;

    while
      ((ADestValue = cmpEqual) and
       (ACount < sizeof(Word)) and
       (P <> #0) and (Q <> #0)) do
    begin
      ADestValue :=
        umlcchars.Compare(P^, Q^);

      System.Inc(P);
      System.Inc(Q);
      System.Inc(ACount);
    end;
  end;
end;

function TryStartsWithEqual
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr;
   out   ADestValue:   Boolean): Boolean;
var H, N: pchar; ACount: Word;
begin
  Result :=
    (umlccharfatptrs.Assigned(AHaystackPtr) and
     umlccharfatptrs.Assigned(ANeedlePtr));
  if (Result) then
  begin
    ACount := 0;
    ADestValue := true;
    H := AHaystackPtr^.Ptr;
    N := ANeedlePtr^.Ptr;

    while
      ((ADestValue) and
       (ACount < sizeof(Word)) and
       (H <> #0) and (N <> #0)) do
    begin
      ADestValue :=
        (H^ = N^);

      System.Inc(H);
      System.Inc(N);
      System.Inc(ACount);
    end;

    if (ADestValue) then
    begin
      umlccharfatptrs.EncodeFatPtr
        (ADestPtr, H, ACount);
    end;
  end;
end;

function TryStartsWithSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr;
   out   ADestValue:   Boolean): Boolean;
var H, N: pchar; ACount: Word;
begin
  Result :=
    (umlccharfatptrs.Assigned(AHaystackPtr) and
     umlccharfatptrs.Assigned(ANeedlePtr));
  if (Result) then
  begin
    ACount := 0;
    ADestValue := true;
    H := AHaystackPtr^.Ptr;
    N := ANeedlePtr^.Ptr;

    while
      ((ADestValue) and
       (ACount < sizeof(Word)) and
       (H <> #0) and (N <> #0)) do
    begin
      ADestValue :=
        umlcchars.SameText(H^, N^);

      System.Inc(H);
      System.Inc(N);
      System.Inc(ACount);
    end;

    if (ADestValue) then
    begin
      umlccharfatptrs.EncodeFatPtr
        (ADestPtr, H, ACount);
    end;
  end;
end;

function TryFinishesWithEqual
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr;
   out   ADestValue:   Boolean): Boolean;
var H, N: umlccharfatptr; Match: Boolean; ACount: Word;
begin
  Result :=
    (umlccharfatptrs.Assigned(AHaystackPtr) and
     umlccharfatptrs.Assigned(ANeedlePtr));
  if (Result) then
  begin
    Result :=
      umlccharfatptrparsers.TryNullStrPosOf(AHaystackPtr, H);
    if (Result) then
    begin
      Result :=
        umlccharfatptrparsers.TryNullStrPosOf(ANeedlePtr, N);
      if (Result) then
      begin
        Match := true; ACount := sizeof(word);
        while (Match and (ACount > 0) and (AHaystackPtr^.Idx > 0) and (ANeedlePtr^.Idx > 0)) do
        begin
          Match :=
            (AHaystackPtr^.Ptr^ = ANeedlePtr^.Ptr^);

          umlccharfatptrs.Dec(H);
          umlccharfatptrs.Dec(N);
          System.Dec(ACount);
        end;

        ADestValue :=
          Match;
        if (Match) then
        begin
          ADestPtr :=
            umlccharfatptrs.DuplicateCopy(AHaystackPtr);
          umlccharfatptrs.Inc(ADestPtr);
        end;
      end;
    end;
  end;
end;

function TryFinishesWithSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr;
   out   ADestValue:   Boolean): Boolean;
var H, N: umlccharfatptr; Match: Boolean; ACount: Word;
begin
  Result :=
    (umlccharfatptrs.Assigned(AHaystackPtr) and
     umlccharfatptrs.Assigned(ANeedlePtr));
  if (Result) then
  begin
    Result :=
      umlccharfatptrparsers.TryNullStrPosOf(AHaystackPtr, H);
    if (Result) then
    begin
      Result :=
        umlccharfatptrparsers.TryNullStrPosOf(ANeedlePtr, N);
      if (Result) then
      begin
        Match := true; ACount := sizeof(word);
        while (Match and (ACount > 0) and (AHaystackPtr^.Idx > 0) and (ANeedlePtr^.Idx > 0)) do
        begin
          Match :=
            umlcchars.SameText(AHaystackPtr^.Ptr^, ANeedlePtr^.Ptr^);

          umlccharfatptrs.Dec(H);
          umlccharfatptrs.Dec(N);
          System.Dec(ACount);
        end;

        ADestValue :=
          Match;
        if (Match) then
        begin
          ADestPtr :=
            umlccharfatptrs.DuplicateCopy(AHaystackPtr);
          umlccharfatptrs.Inc(ADestPtr);
        end;
      end;
    end;
  end;
end;

function TryCharPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleChar:  char;
   out   ADestPtr:     umlccharfatptr): Boolean;
var Match: Boolean;
begin
  Result :=
    (umlccharfatptrs.Assigned(AHaystackPtr));
  if (Result) then
  begin
    Match :=
      (AHaystackPtr^.Ptr^ = ANeedleChar);
    if (Match) then
    begin
      ADestPtr :=
        umlccharfatptrs.DuplicateCopy(AHaystackPtr);
    end;
  end;
end;

function TryCharPosOfSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleChar:  char;
   out   ADestPtr:     umlccharfatptr): Boolean;
var Match: Boolean;
begin
  Result :=
    (umlccharfatptrs.Assigned(AHaystackPtr));
  if (Result) then
  begin
    Match :=
      umlcchars.SameText(AHaystackPtr^.Ptr^, ANeedleChar);
    if (Match) then
    begin
      ADestPtr :=
        umlccharfatptrs.DuplicateCopy(AHaystackPtr);
    end;
  end;
end;

function TryCharSetPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleSet:   charset;
   out   ADestPtr:     umlccharfatptr): Boolean;
var Match: Boolean;
begin
  Result :=
    (umlccharfatptrs.Assigned(AHaystackPtr));
  if (Result) then
  begin
    Match :=
      umlccharsets.IsMember(ANeedleSet, AHaystackPtr^.Ptr^);
    if (Match) then
    begin
      ADestPtr :=
        umlccharfatptrs.DuplicateCopy(AHaystackPtr);
    end;
  end;
end;

function TryCharSetPosOfSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleSet:   charset;
   out   ADestPtr:     umlccharfatptr): Boolean;
var Match: Boolean;
begin
  Result :=
    (umlccharfatptrs.Assigned(AHaystackPtr));
  if (Result) then
  begin
    Match :=
      umlccharsets.IsSameMember(ANeedleSet, AHaystackPtr^.Ptr^);
    if (Match) then
    begin
      ADestPtr :=
        umlccharfatptrs.DuplicateCopy(AHaystackPtr);
    end;
  end;
end;

function TryStrPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;
var Match: Boolean;
begin
  Result :=
    (umlccharfatptrs.Assigned(AHaystackPtr) and
     umlccharfatptrs.Assigned(ANeedlePtr));
  if (Result) then
  begin
    Match :=
      (AHaystackPtr^.Ptr^ = ANeedlePtr^.Ptr^);




    if (Match) then
    begin
      ADestPtr :=
        umlccharfatptrs.DuplicateCopy(AHaystackPtr);
    end;
  end;
end;

function TryStrPosOfSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;
var Match: Boolean;
begin
  Result :=
    (umlccharfatptrs.Assigned(AHaystackPtr) and
     umlccharfatptrs.Assigned(ANeedlePtr));
  if (Result) then
  begin
    Match :=
      (AHaystackPtr^.Ptr^ = ANeedlePtr^.Ptr^);




    if (Match) then
    begin
      ADestPtr :=
        umlccharfatptrs.DuplicateCopy(AHaystackPtr);
    end;
  end;
end;

function Length
  (const ASource: umlccharfatptr): Word;
var CanContinue: Boolean;
begin
  CanContinue :=
    umlccharfatptrparsers.TryLength(ASource, Result);
  if (not CanContinue) then
  begin
    // raise error
  end;
  // Goal: Returns the length of the given string.
  // Objetivo: Regresa la longuitud de la cadena dada.
end;

function IsEmpty
  (const AHaystackPtr: umlccharfatptr): Boolean;
begin
  Result :=
    umlccharfatptrparsers.TryIsEmpty(AHaystackPtr, Result);
  if (not Result) then
  begin
    // raise error
  end;
end;

function Compare
  (const A, B: umlccharfatptr): umlccomparison;
var CanContinue: Boolean;
begin
  Result :=
    cmpEqual;
  CanContinue :=
    umlccharfatptrparsers.TryCompare(A, B, Result);
  if (not CanContinue) then
  begin
    // raise error
  end;
end;

function StartsWithEqual
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;
begin
  Result := false;
  if (not umlccharfatptrparsers.TryStartsWithEqual
    (AHaystackPtr, ANeedlePtr, ADestPtr, Result)) then
  begin
    // raise error
  end;
end;

function StartsWithSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;
begin
  Result := false;
  if (not umlccharfatptrparsers.TryStartsWithSame
    (AHaystackPtr, ANeedlePtr, ADestPtr, Result)) then
  begin
    // raise error
  end;
end;

function FinishesWithEqual
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr): Boolean;
begin
  Result := false;
  if (not umlccharfatptrparsers.TryFinishesWithEqual
    (AHaystackPtr, ANeedlePtr, ADestPtr, Result)) then
  begin
    // raise error
  end;
end;

function DuplicateCopy
  (const ASource: umlccharfatptr): umlccharfatptr;
var AItemCount, AByteCount: Word; P: pchar;
begin
  Result := nil;
  if (umlccharfatptrs.Assigned(ASource)) then
  begin
    AItemCount :=
      umlccharfatptrparsers.Length(ASource);
    System.GetMem(P, System.Pred(AItemCount));
    AByteCount :=
      (AItemCount * sizeof(char));
    System.Move(ASource^.Ptr^, P, AByteCount);

    umlccharfatptrs.EncodeFatPtr(Result, P, AItemCount);
  end;
  // Goal: Returns a copy of the given.
  // Objetivo: Regresa una copia de la cadena dada.
end;

function TrimLeftCopy
  (const ASource: umlccharfatptr): umlccharfatptr;
var ABlanksCount, AItemCount, AllCharCount, AByteCount: Word;
    P, Start, Temp: pchar;
begin
  Result := nil;
  if (umlccharfatptrs.Assigned(ASource)) then
  begin
    // skip non space leading items
    P := ASource^.Ptr;
    ABlanksCount := 0;
    while (
      (umlccharsets.IsMember(umlccharsetconsts.BlanksSet, P^)) and
      (P^ <> #0) and
      (ABlanksCount < sizeof(Word))
      ) do
    begin
      System.Inc(P);
      System.Inc(ABlanksCount);
    end;

    if ((P^ <> #0) or (ABlanksCount < sizeof(Word))) then
    begin
      // get count of remainning characters
      Start := P;
      AItemCount := 0;
      AllCharCount :=
        ABlanksCount;
      while (
        (P^ <> #0) and
        (AllCharCount < sizeof(Word))
        ) do
      begin
        System.Inc(P);
        System.Inc(AItemCount);
        System.Inc(AllCharCount);
      end;

      if (AItemCount > 0) then
      begin
        // obtain memory
        AByteCount :=
          System.Succ(AItemCount * sizeof(char));
        System.GetMem(Temp, AByteCount);

        // perform copy
        System.Move(Start^, Temp, AByteCount);
        P := Start;
        System.Inc(P, AItemCount);
        // add null char marker
        P^ := #0;

        // generate result
        umlccharfatptrs.EncodeFatPtr(Result, Start, AItemCount);
      end;
    end;
  end;
  // Goal: Returns a copy of the given string without leading spaces.
  // Objetivo: Regresa una copia de la cadena dada sin espacios iniciales.
end;

function TrimRightCopy
  (const ASource: umlccharfatptr): umlccharfatptr;
begin
  Result := nil;
  if (umlccharfatptrs.Assigned(ASource)) then
  begin


  end;
    //SysUtils.TrimRight(ASource);
  // Goal: Returns a copy of the given string without trailing spaces.
  // Objetivo: Regresa una copia de la cadena dada sin espacios finales.
end;

function TrimCopy
  (const ASource: umlccharfatptr): umlccharfatptr;
begin
  Result := nil;
  if (umlccharfatptrs.Assigned(ASource)) then
  begin


  end;
    //SysUtils.Trim(ASource);
  // Goal: Returns a copy of the given string }
  // without leading & trailing spaces.

  // Objetivo: Regresa una copia de la cadena dada
  // sin espacios iniciales y finales.
end;

(* global procedures *)

procedure NullStrPosOf
  (const AHaystackPtr: umlccharfatptr;
   out   ADestPtr:     umlccharfatptr);
begin
  if (not umlccharfatptrparsers.TryNullStrPosOf(AHaystackPtr, ADestPtr)) then
  begin
    // raise error
  end;
end;

procedure CharPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleChar:  char;
   out   ADestPtr:     umlccharfatptr);
begin
  ADestPtr := nil;
  if (not umlccharfatptrparsers.TryCharPosOf
    (AHaystackPtr, ANeedleChar, ADestPtr)) then
  begin
    //
  end;
end;

procedure CharPosOfSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleChar:  char;
   out   ADestPtr:     umlccharfatptr);
begin
  ADestPtr := nil;
  if (not umlccharfatptrparsers.TryCharPosOfSame
    (AHaystackPtr, ANeedleChar, ADestPtr)) then
  begin
    //
  end;
end;

procedure CharSetPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleSet:   charset;
   out   ADestPtr:     umlccharfatptr);
begin
  ADestPtr := nil;
  if (not umlccharfatptrparsers.TryCharSetPosOf
    (AHaystackPtr, ANeedleSet, ADestPtr)) then
  begin
    //
  end;
end;

procedure CharSetPosOfSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedleSet:   charset;
   out   ADestPtr:     umlccharfatptr);
begin
  ADestPtr := nil;
  if (not umlccharfatptrparsers.TryCharSetPosOfSame
    (AHaystackPtr, ANeedleSet, ADestPtr)) then
  begin
    //
  end;
end;

procedure StrPosOf
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr);
begin
  ADestPtr := nil;
  if (not umlccharfatptrparsers.TryStrPosOf
    (AHaystackPtr, ANeedlePtr, ADestPtr)) then
  begin
    //
  end;
end;

procedure StrPosOfSame
  (const AHaystackPtr: umlccharfatptr;
   const ANeedlePtr:   umlccharfatptr;
   out   ADestPtr:     umlccharfatptr);
begin
  ADestPtr := nil;
  if (not umlccharfatptrparsers.TryStrPosOfSame
    (AHaystackPtr, ANeedlePtr, ADestPtr)) then
  begin
    //
  end;
end;


(* global operators *)

function Equal
  (const A, B: umlccharfatptr): Boolean;
var ADestValue: umlccomparison;
begin
  Result := false;

  if (umlccharfatptrparsers.TryCompare(A, B, ADestValue)) then
  begin
    Result :=
      (ADestValue = cmpEqual);
  end else
  begin
    // raise error
  end;
end;

function Different
  (const A, B: umlccharfatptr): Boolean;
var ADestValue: umlccomparison;
begin
  Result := false;

  if (umlccharfatptrparsers.TryCompare(A, B, ADestValue)) then
  begin
    Result :=
      (ADestValue <> cmpEqual);
  end else
  begin
    // raise error
  end;
end;



end.

