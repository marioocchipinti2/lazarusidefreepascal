(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansichars;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to support the predefined "char" type.
 ** The character encoding is A.N.S.I.
 **************************************************************************
 **)

interface
uses
{$IFDEF MSWINDOWS}
  Windows, //Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  dummy;

// ---

const

 MOD_umlcansichars : TUMLCModule =
   ($4E,$09,$BE,$6D,$3F,$19,$55,$4D,$9C,$16,$A8,$1C,$44,$EE,$86,$88);

// ---

(* global standard functions *)

  function IsNull
    (const ASource: ansichar): Boolean; overload;
  function IsAlpha
    (const ASource: ansichar): Boolean; overload;
  function IsDigit
    (const ASource: ansichar): Boolean; overload;
  function IsSpace
    (const ASource: ansichar): Boolean; overload;
  function IsBlank
    (const ASource: ansichar): Boolean; overload;

  function IsDecimal
    (const ASource: ansichar): Boolean; overload;
  function IsHexa
    (const ASource: ansichar): Boolean; overload;
  function IsOctal
    (const ASource: ansichar): Boolean; overload;
  function IsBinary
    (const ASource: ansichar): Boolean; overload;

  function IsUppercase
    (const ASource: ansichar): Boolean; overload;
  function IsLowercase
    (const ASource: ansichar): Boolean; overload;

(* global additional functions *)

  function CharToOrd
    (const ASource: ansichar): Integer; overload;
  function OrdToChar
    (const ASource: Integer): ansichar; overload;

  function UppercaseCopy
    (const ASource: ansichar): ansichar; overload;
  function LowercaseCopy
    (const ASource: ansichar): ansichar; overload;
  function TogglecaseCopy
    (const ASource: ansichar): ansichar; overload;

  function SameText
    (const A, B: ansichar): Boolean; overload;

  function IfChar
    (const APredicate: Boolean; const A, B: ansichar): ansichar;

  function ReplaceCopy
    (const ASource: ansichar; const A, B: ansichar): ansichar; overload;

(* global operators *)

  procedure Assign
    (out   ADest:   ansichar;
     const ASource: ansichar); overload; // operator :=

  function Compare
    (const A, B: ansichar): umlctcomparison;

  function Different
    (const A, B: ansichar): Boolean; overload; // operator <>
  function Equal
    (const A, B: ansichar): Boolean; overload; // operator =

  function Greater
    (const A, B: ansichar): Boolean; overload; // operator >
  function Lesser
    (const A, B: ansichar): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: ansichar): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: ansichar): Boolean; overload; // operator <=

(* global procedures *)

  procedure Clear
    (out ADest: ansichar); overload;

  procedure ReplaceUppercase
    (out ADest: ansichar); overload;
  procedure ReplaceLowercase
    (out ADest: ansichar); overload;
  procedure ReplaceTogglecase
    (out ADest: ansichar); overload;

implementation

(* global standard functions *)

function IsNull
  (const ASource: ansichar): Boolean;
begin
  Result :=
    (ASource = ansinullchar);
end;

function IsAlpha
  (const ASource: ansichar): Boolean;
begin
  Result :=
    Windows.IsCharAlphaA(ASource);
end;

function IsDigit
  (const ASource: ansichar): Boolean;
begin
  Result :=
    ((ASource >= '0') and (ASource <= '9'));
end;

function IsSpace
  (const ASource: ansichar): Boolean;
begin
  Result :=
    (Ord(ASource) = 32);
end;

function IsBlank
  (const ASource: ansichar): Boolean;
begin
  Result :=
    (Ord(ASource) = 32) or // space
    (Ord(ASource) = 13) or // D.O.S. carriage-return
    (Ord(ASource) = 10) or // D.O.S. line-feed
    (Ord(ASource) = 12) or // page break
    (Ord(ASource) = 08);   // tabulator
end;

function IsDecimal
  (const ASource: ansichar): Boolean;
begin
  Result :=
    ((ASource >= '0') and (ASource <= '9'));
end;

function IsHexa
  (const ASource: ansichar): Boolean;
begin
  Result :=
    (ASource >= '0') and (ASource <= '9') or
    (ASource >= 'A') and (ASource <= 'F') or
    (ASource >= 'a') and (ASource <= 'f');
end;

function IsOctal
  (const ASource: ansichar): Boolean;
begin
  Result :=
    (ASource >= '0') and (ASource <= '7');
end;

function IsBinary
  (const ASource: ansichar): Boolean;
begin
  Result :=
    (ASource = '0') or (ASource = '1');
end;

function IsUppercase
  (const ASource: ansichar): Boolean;
begin
  Result :=
    Windows.IsCharUpperA(ASource);
end;

function IsLowercase
  (const ASource: ansichar): Boolean;
begin
  Result :=
    Windows.IsCharLowerA(ASource);
end;

(* global additional functions *)

function CharToOrd
  (const ASource: ansichar): Integer;
begin
  Result :=
    Ord(ASource);
end;

function OrdToChar
  (const ASource: Integer): ansichar;
begin
  Result :=
    Chr(ASource);
end;

function UppercaseCopy
  (const ASource: ansichar): ansichar;
begin
  Result :=
    ASource;
  Windows.CharUpperBuffA(@Result, 1);
  // Goal: Returns a uppercase copy of the given character.
  // Objetivo: Regresa una copia en mayusculas del caracter dado.
end;

function LowercaseCopy
  (const ASource: ansichar): ansichar;
begin
  Result := ASource;
  Windows.CharLowerBuffA(@Result, 1);
  // Goal: Returns a lowercase copy of the given character.
  // Objetivo: Regresa una copia en minusculas del caracter dado.
end;

function TogglecaseCopy
  (const ASource: ansichar): ansichar;
begin
  if (IsUppercase(ASource))
    then Result := LowercaseCopy(ASource)
    else Result := UppercaseCopy(ASource);
  // Goal: Returns a switched case copy of the given character.
  // Objetivo: Regresa una copia en caso intercambiado del caracter dado.
end;

function SameText
  (const A, B: ansichar): Boolean;
begin
  Result :=
    SysUtils.SameText(A, B);
  // Goal: Returns if 2 characters are equal, ignores sensitive case.
  // Objetivo: Regresa si 2 caracteres son iguales, ignorar caso sensitivo.
end;

function IfChar
  (const APredicate: Boolean; const A, B: ansichar): ansichar;
begin
  if (APredicate)
    then Result := A
    else Result := B;
  // Objetivo: Segun la condicion, regresar el caracter seleccionado.
  // Goal: Upon condition, return the select character.
end;

function ReplaceCopy
  (const ASource: ansichar; const A, B: ansichar): ansichar;
begin
  if (ASource = A)
    then Result := B
    else Result := ASource;
  // Objetivo: Reemplazar un caracter en especifico.
  // Goal: Upon condition, return the select character.  
end;

(* global operators *)

procedure Assign
  (out   ADest:   ansichar;
   const ASource: ansichar);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: ansichar): umlccomparison;
begin
  Result := cmpEqual;
  if (A < B)
    then Result := cmpLower
  else if (A > B)
    then Result := cmpHigher;
end;

function Different
  (const A, B: ansichar): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Equal
  (const A, B: ansichar): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Greater
  (const A, B: ansichar): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: ansichar): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: ansichar): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: ansichar): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

(* global procedures *)

procedure Clear
  (out ADest: ansichar);
begin
  ADest :=
    ansinullchar;
  // Goal: Clear a character.
  // Objetivo: Limpia un caracter.
end;

procedure ReplaceUppercase
  (out ADest: ansichar);
begin
  Windows.CharUpperBuffA(@ADest, 1);
  // Goal: Changes the given character into uppercase.
  // Objetivo: Cambia el caracter dado a mayusculas.
end;

procedure ReplaceLowercase
  (out ADest: ansichar);
begin
  Windows.CharLowerBuffA(@ADest, 1);
  // Goal: Changes the given character into lowercase.
  // Objetivo: Cambia el caracter dado a minusculas.
end;

procedure ReplaceTogglecase
  (out ADest: ansichar);
begin
  if (Windows.IsCharLowerA(ADest))
    then Windows.CharUpperBuff(@ADest, 1)
    else Windows.CharLowerBuff(@ADest, 1);
  // Goal: Changes the given character into lowercase.
  // Objetivo: Cambia el caracter dado a minusculas.
end;


end.
