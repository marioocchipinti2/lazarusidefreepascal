(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwidecharbyptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcwidechars,
  dummy;

// ---

const

 MOD_umlcwidecharbyptrs : TUMLCModule =
   ($19,$0C,$02,$0B,$82,$D0,$D2,$41,$A4,$60,$68,$A2,$3B,$DC,$E1,$3C);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: widechar): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: widechar): pointer;
begin
  System.GetMem(Result, sizeof(widechar));
end;

procedure DropPtr
  (var ADestPtr: pointer);
var ADestValuePtr: pwidechar;
begin
  ADestValuePtr := ADestPtr;
  System.Dispose(ADestValuePtr);
end;

end.

