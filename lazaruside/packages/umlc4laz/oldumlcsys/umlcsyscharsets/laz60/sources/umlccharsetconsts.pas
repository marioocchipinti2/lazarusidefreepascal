(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccharsetconsts;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to implement commonly used character sets.
 ** The character encoding is undefined.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcchars,
  umlccharsets,
  dummy;

// ---

const

 MOD_umlccharsetconsts : TUMLCModule =
   ($1A,$A7,$4C,$F9,$4A,$01,$C6,$48,$96,$F1,$75,$5E,$40,$4C,$65,$F6);

// ---

var
  BlanksSet, EoLnSet, NullSet: umlccharset;
  BoolSet: umlccharset;
  UpperSet, LowerSet: umlccharset;
  AlphaSet, FloatSet, CurrSet: umlccharset;
  DecDigitSet, OctDigitSet, HexDigitSet, BinDigitSet: umlccharset;
  IDSet, QIDSet, WildcardSet: umlccharset;
  UpperIDSet, LowerIDSet: umlccharset;

implementation

procedure UnitConstructor;
begin
  BlanksSet := #32#13#10#12;
  // BlanksSet := [#32, #13, #10, #12];

  EoLnSet := #13#10;
  // EoLnSet := [#13, #10];

  NullSet := #0#13#10#26;
  // NullSet := [#0, #13, #10, #26];

  BoolSet := '0fF';
  // BoolSet := ['0', 'f', 'F'];

  // set of values used for FALSE representation
  // conjunto de valores utilizados para la representacion de FALSO

  umlccharsets.Clear(UpperSet);
  umlccharsets.IncludeRange(UpperSet, 'A', 'Z');
  //umlccharsets.Include(UpperSet, chr(ord('Ñ')));
  // UpperSet := ['A'..'Z', 'Ñ'];

  umlccharsets.Clear(LowerSet);
  umlccharsets.IncludeRange(LowerSet, 'a', 'z');
  //umlccharsets.Include(LowerSet, 'ñ');
  // LowerSet := ['a'..'z', 'ñ'];

  umlccharsets.Clear(AlphaSet);
  umlccharsets.IncludeRange(AlphaSet, 'A', 'Z');
  umlccharsets.IncludeRange(AlphaSet, 'a', 'z');
  // AlphaSet  := ['A'..'Z', 'a'..'z'];

  umlccharsets.Clear(DecDigitSet);
  umlccharsets.IncludeRange(DecDigitSet, '0', '9');
  // DecDigitSet  := ['0'..'9'];

  umlccharsets.Clear(OctDigitSet);
  umlccharsets.IncludeRange(OctDigitSet, '0', '7');
  // OctDigitSet  := ['0'..'7'];

  umlccharsets.Clear(HexDigitSet);
  umlccharsets.IncludeRange(HexDigitSet, '0', '9');
  umlccharsets.IncludeRange(HexDigitSet, 'a', 'f');
  umlccharsets.IncludeRange(HexDigitSet, 'A', 'F');
  // HexDigitSet  := ['0'..'9', 'a'..'f', 'A'..'F'];

  umlccharsets.Clear(BinDigitSet);
  umlccharsets.Include(BinDigitSet, '0');
  umlccharsets.Include(BinDigitSet, '1');
  // BinDigitSet  := ['0', '1'];

  umlccharsets.Clear(FloatSet);
  umlccharsets.IncludeRange(FloatSet, '0', '9');
  umlccharsets.Include(FloatSet, '.');
  umlccharsets.Include(FloatSet, ',');
  umlccharsets.Include(FloatSet, '+');
  umlccharsets.Include(FloatSet, '-');
  umlccharsets.Include(FloatSet, 'E');
  umlccharsets.Include(FloatSet, 'e');
  // FloatSet  := ['0'..'9', '.', ',', '+', '-', 'E', 'e'];

  umlccharsets.Clear(CurrSet);
  umlccharsets.IncludeRange(CurrSet, '0', '9');
  umlccharsets.Include(CurrSet, '.');
  umlccharsets.Include(CurrSet, ',');
  // CurrSet   := ['0'..'9', '.', ','];

  umlccharsets.Clear(IDSet);
  umlccharsets.IncludeRange(IDSet, 'A', 'Z');
  umlccharsets.IncludeRange(IDSet, 'a', 'z');
  umlccharsets.IncludeRange(IDSet, '0', '9');
  umlccharsets.Include(IDSet, '_');
  // IDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9'];

  WildcardSet := IDSet;
  umlccharsets.Include(WildcardSet, '*');
  umlccharsets.Include(WildcardSet, '?');
  // WildcardSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '*', '?'];

  QIDSet := IDSet;
  umlccharsets.Include(QIDSet, '.');
  // QIDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '.'];

  umlccharsets.Clear(UpperIDSet);
  umlccharsets.IncludeRange(UpperIDSet, 'A', 'Z');
  umlccharsets.IncludeRange(UpperIDSet, '0', '9');
  umlccharsets.Include(UpperIDSet, '_');
  // UpperIDSet := ['A'..'Z', '_', '0'..'9'];

  umlccharsets.Clear(LowerIDSet);
  umlccharsets.IncludeRange(LowerIDSet, 'A', 'Z');
  umlccharsets.IncludeRange(LowerIDSet, 'a', 'z');
  umlccharsets.IncludeRange(LowerIDSet, '0', '9');
  umlccharsets.Include(LowerIDSet, '_');
  // LowerIDSet := ['a'..'z', '_', '0'..'9'];
end;

procedure UnitDestructor;
begin
  WildcardSet := '';
  IDSet     := '';
  QIDSet    := '';

  CurrSet   := '';
  FloatSet  := '';
  DecDigitSet  := '';
  AlphaSet  := '';

  LowerSet  := '';
  UpperSet  := '';

  BoolSet   := '';

  NullSet   := '';
  EoLnSet   := '';
  BlanksSet := '';
end;

initialization
  UnitConstructor;
finalization
  UnitDestructor;
end.

