(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwidecharsetconsts;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to implement commonly used character sets.
 ** The character encoding is A.N.S.I.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcwidechars, umlcwidecharsets,
  dummy;

// ---

const

 MOD_umlcwidecharsetconsts : TUMLCModule =
   ($50,$A2,$44,$08,$66,$E3,$86,$45,$B0,$75,$C7,$2E,$5D,$9D,$7F,$7F);

// ---

var
  BlanksSet, EoLnSet, NullSet: umlcwidecharset;
  BoolSet: umlcwidecharset;
  UpperSet, LowerSet: umlcwidecharset;
  AlphaSet, FloatSet, CurrSet: umlcwidecharset;
  DecDigitSet, OctDigitSet, HexDigitSet, BinDigitSet: umlcwidecharset;
  IDSet, QIDSet, WildcardSet: umlcwidecharset;
  UpperIDSet, LowerIDSet: umlcwidecharset;

implementation

procedure UnitConstructor;
begin
  BlanksSet := #32#13#10#12;
  // BlanksSet := [#32, #13, #10, #12];

  EoLnSet := #13#10;
  // EoLnSet := [#13, #10];

  NullSet := #0#13#10#26;
  // NullSet := [#0, #13, #10, #26];

  BoolSet := '0fF';
  // BoolSet := ['0', 'f', 'F'];

  // set of values used for FALSE representation
  // conjunto de valores utilizados para la representacion de FALSO

  umlcwidecharsets.Clear(UpperSet);
  umlcwidecharsets.IncludeRange(UpperSet, umlcwidechar('A'), umlcwidechar('Z'));
  //umlcwidecharsets.Include(UpperSet, chr(ord('Ñ')));
  // UpperSet := ['A'..'Z', 'Ñ'];

  umlcwidecharsets.Clear(LowerSet);
  umlcwidecharsets.IncludeRange(LowerSet, umlcwidechar('a'), umlcwidechar('z'));
  //umlcwidecharsets.Include(LowerSet, 'ñ');
  // LowerSet := ['a'..'z', 'ñ'];

  umlcwidecharsets.Clear(AlphaSet);
  umlcwidecharsets.IncludeRange(AlphaSet, umlcwidechar('A'), umlcwidechar('Z'));
  umlcwidecharsets.IncludeRange(AlphaSet, umlcwidechar('a'), umlcwidechar('z'));
  // AlphaSet  := ['A'..'Z', 'a'..'z'];

  umlcwidecharsets.Clear(DecDigitSet);
  umlcwidecharsets.IncludeRange(DecDigitSet, umlcwidechar('0'), umlcwidechar('9'));
  // DecDigitSet  := ['0'..'9'];

  umlcwidecharsets.Clear(OctDigitSet);
  umlcwidecharsets.IncludeRange(OctDigitSet, umlcwidechar('0'), umlcwidechar('7'));
  // OctDigitSet  := ['0'..'7'];

  umlcwidecharsets.Clear(HexDigitSet);
  umlcwidecharsets.IncludeRange(HexDigitSet, umlcwidechar('0'), umlcwidechar('9'));
  umlcwidecharsets.IncludeRange(HexDigitSet, umlcwidechar('a'), umlcwidechar('f'));
  umlcwidecharsets.IncludeRange(HexDigitSet, umlcwidechar('A'), umlcwidechar('F'));
  // HexDigitSet  := ['0'..'9', 'a'..'f', 'A'..'F'];

  umlcwidecharsets.Clear(BinDigitSet);
  umlcwidecharsets.Include(BinDigitSet, umlcwidechar('0'));
  umlcwidecharsets.Include(BinDigitSet, umlcwidechar('1'));
  // BinDigitSet  := ['0', '1'];

  umlcwidecharsets.Clear(FloatSet);
  umlcwidecharsets.IncludeRange(FloatSet, umlcwidechar('0'), umlcwidechar('9'));
  umlcwidecharsets.Include(FloatSet, umlcwidechar('.'));
  umlcwidecharsets.Include(FloatSet, umlcwidechar(','));
  umlcwidecharsets.Include(FloatSet, umlcwidechar('+'));
  umlcwidecharsets.Include(FloatSet, umlcwidechar('-'));
  umlcwidecharsets.Include(FloatSet, umlcwidechar('E'));
  umlcwidecharsets.Include(FloatSet, umlcwidechar('e'));
  // FloatSet  := ['0'..'9', '.', ',', '+', '-', 'E', 'e'];

  umlcwidecharsets.Clear(CurrSet);
  umlcwidecharsets.IncludeRange(CurrSet, umlcwidechar('0'), umlcwidechar('9'));
  umlcwidecharsets.Include(CurrSet, umlcwidechar('.'));
  umlcwidecharsets.Include(CurrSet, umlcwidechar(','));
  // CurrSet   := ['0'..'9', '.', ','];

  umlcwidecharsets.Clear(IDSet);
  umlcwidecharsets.IncludeRange(IDSet, umlcwidechar('A'), umlcwidechar('Z'));
  umlcwidecharsets.IncludeRange(IDSet, umlcwidechar('a'), umlcwidechar('z'));
  umlcwidecharsets.IncludeRange(IDSet, umlcwidechar('0'), umlcwidechar('9'));
  umlcwidecharsets.Include(IDSet, '_');
  // IDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9'];

  WildcardSet := IDSet;
  umlcwidecharsets.Include(WildcardSet, umlcwidechar('*'));
  umlcwidecharsets.Include(WildcardSet, umlcwidechar('?'));
  // WildcardSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '*', '?'];

  QIDSet := IDSet;
  umlcwidecharsets.Include(QIDSet, umlcwidechar('.'));
  // QIDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '.'];

  umlcwidecharsets.Clear(UpperIDSet);
  umlcwidecharsets.IncludeRange(UpperIDSet, umlcwidechar('A'), umlcwidechar('Z'));
  umlcwidecharsets.IncludeRange(UpperIDSet, umlcwidechar('0'), umlcwidechar('9'));
  umlcwidecharsets.Include(UpperIDSet, umlcwidechar('_'));
  // UpperIDSet := ['A'..'Z', '_', '0'..'9'];

  umlcwidecharsets.Clear(LowerIDSet);
  umlcwidecharsets.IncludeRange(LowerIDSet, umlcwidechar('a'), umlcwidechar('z'));
  umlcwidecharsets.IncludeRange(LowerIDSet, umlcwidechar('0'), umlcwidechar('9'));
  umlcwidecharsets.Include(LowerIDSet, umlcwidechar('_'));
  // LowerIDSet := ['a'..'z', '_', '0'..'9'];
end;

procedure UnitDestructor;
begin
  WildcardSet := '';
  IDSet     := '';
  QIDSet    := '';

  CurrSet   := '';
  FloatSet  := '';
  DecDigitSet  := '';
  AlphaSet  := '';

  LowerSet  := '';
  UpperSet  := '';

  BoolSet   := '';

  NullSet   := '';
  EoLnSet   := '';
  BlanksSet := '';
end;

initialization
  UnitConstructor;
finalization
  UnitDestructor;
end.
