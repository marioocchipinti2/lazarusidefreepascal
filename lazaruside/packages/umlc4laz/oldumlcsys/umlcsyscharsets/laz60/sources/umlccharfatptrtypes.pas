(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccharfatptrtypes;

{$mode objfpc}{$H+}

interface
uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdstrtypes,
  dummy;

// ---

const

  MOD_umlccharfatptrtypes : TUMLCModule =
    ($A9,$5B,$99,$42,$51,$B7,$CF,$44,$B9,$09,$01,$FC,$76,$8D,$18,$D8);

// ---

const

// ---

  ID_umlccharfatptr : TUMLCType =
    ($89,$78,$8E,$EF,$C7,$63,$93,$4D,$AA,$E9,$BA,$48,$85,$D7,$64,$84);

  ID_umlcansicharfatptr : TUMLCType =
    ($78,$A3,$1D,$25,$40,$3A,$9C,$48,$A8,$02,$12,$CB,$DC,$F4,$3F,$3F);

  ID_umlcwidecharfatptr : TUMLCType =
    ($C9,$6F,$1F,$34,$F7,$9B,$7A,$49,$81,$BE,$A7,$AD,$2F,$D7,$C3,$0A);

// ---

type
  umlccharfatptrheader = record
    Ptr: PChar;
    Idx: Word;
  end;
  umlccharfatptr = ^umlccharfatptrheader;

type
  umlcansicharfatptrheader = record
    Ptr: PAnsiChar;
    Idx: Word;
  end;
  umlcansicharfatptr = ^umlcansicharfatptrheader;

type
  umlcwidecharfatptrheader = record
    Ptr: PWideChar;
    Idx: Word;
  end;
  umlcwidecharfatptr = ^umlcwidecharfatptrheader;

implementation





end.

