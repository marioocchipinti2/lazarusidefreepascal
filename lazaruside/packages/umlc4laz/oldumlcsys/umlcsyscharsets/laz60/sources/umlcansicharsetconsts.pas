(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansicharsetconsts;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to implement commonly used character sets.
 ** The character encoding is A.N.S.I.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcansichars, umlcansicharsets,
  dummy;

// ---

const

 MOD_umlcansicharsetconsts : TUMLCModule =
   ($29,$9F,$A1,$A4,$83,$DF,$01,$4F,$84,$AC,$EE,$45,$09,$26,$09,$E3);

// ---

var
  BlanksSet, EoLnSet, NullSet: umlcansicharset;
  BoolSet: umlcansicharset;
  UpperSet, LowerSet: umlcansicharset;
  AlphaSet, FloatSet, CurrSet: umlcansicharset;
  DecDigitSet, OctDigitSet, HexDigitSet, BinDigitSet: umlcansicharset;
  IDSet, QIDSet, WildcardSet: umlcansicharset;
  UpperIDSet, LowerIDSet: umlcansicharset;

implementation

procedure UnitConstructor;
begin
  BlanksSet := #32#13#10#12;
  // BlanksSet := [#32, #13, #10, #12];

  EoLnSet := #13#10;
  // EoLnSet := [#13, #10];

  NullSet := #0#13#10#26;
  // NullSet := [#0, #13, #10, #26];

  BoolSet := '0fF';
  // BoolSet := ['0', 'f', 'F'];

  // set of values used for FALSE representation
  // conjunto de valores utilizados para la representacion de FALSO

  umlcansicharsets.Clear(UpperSet);
  umlcansicharsets.IncludeRange(UpperSet, 'A', 'Z');
  //umlcansicharsets.Include(UpperSet, chr(ord('Ñ')));
  // UpperSet := ['A'..'Z', 'Ñ'];

  umlcansicharsets.Clear(LowerSet);
  umlcansicharsets.IncludeRange(LowerSet, 'a', 'z');
  //umlcansicharsets.Include(LowerSet, 'ñ');
  // LowerSet := ['a'..'z', 'ñ'];

  umlcansicharsets.Clear(AlphaSet);
  umlcansicharsets.IncludeRange(AlphaSet, 'A', 'Z');
  umlcansicharsets.IncludeRange(AlphaSet, 'a', 'z');
  // AlphaSet  := ['A'..'Z', 'a'..'z'];

  umlcansicharsets.Clear(DecDigitSet);
  umlcansicharsets.IncludeRange(DecDigitSet, '0', '9');
  // DecDigitSet  := ['0'..'9'];

  umlcansicharsets.Clear(OctDigitSet);
  umlcansicharsets.IncludeRange(OctDigitSet, '0', '7');
  // OctDigitSet  := ['0'..'7'];

  umlcansicharsets.Clear(HexDigitSet);
  umlcansicharsets.IncludeRange(HexDigitSet, '0', '9');
  umlcansicharsets.IncludeRange(HexDigitSet, 'a', 'f');
  umlcansicharsets.IncludeRange(HexDigitSet, 'A', 'F');
  // HexDigitSet  := ['0'..'9', 'a'..'f', 'A'..'F'];

  umlcansicharsets.Clear(BinDigitSet);
  umlcansicharsets.Include(BinDigitSet, '0');
  umlcansicharsets.Include(BinDigitSet, '1');
  // BinDigitSet  := ['0', '1'];

  umlcansicharsets.Clear(FloatSet);
  umlcansicharsets.IncludeRange(FloatSet, '0', '9');
  umlcansicharsets.Include(FloatSet, '.');
  umlcansicharsets.Include(FloatSet, ',');
  umlcansicharsets.Include(FloatSet, '+');
  umlcansicharsets.Include(FloatSet, '-');
  umlcansicharsets.Include(FloatSet, 'E');
  umlcansicharsets.Include(FloatSet, 'e');
  // FloatSet  := ['0'..'9', '.', ',', '+', '-', 'E', 'e'];

  umlcansicharsets.Clear(CurrSet);
  umlcansicharsets.IncludeRange(CurrSet, '0', '9');
  umlcansicharsets.Include(CurrSet, '.');
  umlcansicharsets.Include(CurrSet, ',');
  // CurrSet   := ['0'..'9', '.', ','];

  umlcansicharsets.Clear(IDSet);
  umlcansicharsets.IncludeRange(IDSet, 'A', 'Z');
  umlcansicharsets.IncludeRange(IDSet, 'a', 'z');
  umlcansicharsets.IncludeRange(IDSet, '0', '9');
  umlcansicharsets.Include(IDSet, '_');
  // IDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9'];

  WildcardSet := IDSet;
  umlcansicharsets.Include(WildcardSet, '*');
  umlcansicharsets.Include(WildcardSet, '?');
  // WildcardSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '*', '?'];

  QIDSet := IDSet;
  umlcansicharsets.Include(QIDSet, '.');
  // QIDSet := ['A'..'Z', 'a'..'z', '_', '0'..'9', '.'];

  umlcansicharsets.Clear(UpperIDSet);
  umlcansicharsets.IncludeRange(UpperIDSet, 'A', 'Z');
  umlcansicharsets.IncludeRange(UpperIDSet, '0', '9');
  umlcansicharsets.Include(UpperIDSet, '_');
  // UpperIDSet := ['A'..'Z', '_', '0'..'9'];

  umlcansicharsets.Clear(LowerIDSet);
  umlcansicharsets.IncludeRange(LowerIDSet, 'A', 'Z');
  umlcansicharsets.IncludeRange(LowerIDSet, 'a', 'z');
  umlcansicharsets.IncludeRange(LowerIDSet, '0', '9');
  umlcansicharsets.Include(LowerIDSet, '_');
  // LowerIDSet := ['a'..'z', '_', '0'..'9'];
end;

procedure UnitDestructor;
begin
  WildcardSet := '';
  IDSet     := '';
  QIDSet    := '';

  CurrSet   := '';
  FloatSet  := '';
  DecDigitSet  := '';
  AlphaSet  := '';

  LowerSet  := '';
  UpperSet  := '';

  BoolSet   := '';

  NullSet   := '';
  EoLnSet   := '';
  BlanksSet := '';
end;

initialization
  UnitConstructor;
finalization
  UnitDestructor;
end.
