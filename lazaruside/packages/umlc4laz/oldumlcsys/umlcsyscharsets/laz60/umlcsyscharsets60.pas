{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcsyscharsets60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcansicharbyptrs, umlcansichars, umlcansicharsetconsts, umlcansicharsets, 
  umlccharsets_rtti, umlccharbyptrs, umlcchars, umlccharsetconsts, 
  umlccharsets, umlcwidecharbyptrs, umlcwidechars, umlcwidecharsetconsts, 
  umlcwidecharsets, umlccharfatptrparsers, umlccharfatptrs, umlccharfatptrtypes;

implementation

end.
