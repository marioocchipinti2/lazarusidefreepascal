{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcsysconv60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcansicharbyptrconv, umlcansinullstrstextconv, umlcansishortstrbyptrconv, 
  umlcbins, umlcbooltostrbyptrconv, umlcbooltostrconv, umlccharbyptrconv, 
  umlcconfigstextconv, umlcconvtypes, umlcdatetostrconv, umlcdaystextconv, 
  umlcdecs, umlcdoscolorstextconv, umlcfloatcurrencytostrconv, 
  umlcfloatextendedtostrconv, umlcfloatrealtostrconv, 
  umlcfloatsdoubletostrconv, umlcfloatsingletostrconv, umlchexas, 
  umlclangstextconv, umlcmonthstextconv, umlcoctals, umlcptrbyptrtostrconv, 
  umlcresbooleans, umlcresconfigs, umlcresdays, umlcresdoscolors, 
  umlcreslangs, umlcresmonths, umlcreswin16colors, umlcrgbtextconv, 
  umlcshortstrbyptrconv, umlcsintbyptrbinconv, umlcsintbyptrdecconv, 
  umlcsintbyptrhexconv, umlcsintbyptroctconv, umlcstddatetimetostrbyptrconv, 
  umlcstddatetimetostrconv, umlcstddatetostrbyptrconv, umlcstddatetostrconv, 
  umlcstdtimestamptostrbyptrconv, umlcstdtimetostrbyptrconv, 
  umlcstdtimetostrconv, umlcsysconv_rtti, umlctimestamptostrconv, 
  umlcuintbyptrbinconv, umlcuintbyptrdecconv, umlcuintbyptrhexconv, 
  umlcuintbyptroctconv, umlcuintbyptrtobinstrconv, umlcuintbyptrtodecstrconv, 
  umlcuintbyptrtohexstrconv, umlcuintbyptrtooctstrconv, umlcuinttobinstrconv, 
  umlcuinttodecstrconv, umlcuinttohexstrconv, umlcuinttooctstrconv, 
  umlcuuidtostrbyptrconv, umlcuuidtostrconv, umlcwidecharbyptrconv, 
  umlcwin16colorstextconv, umlcxnumtextconv;

implementation

end.
