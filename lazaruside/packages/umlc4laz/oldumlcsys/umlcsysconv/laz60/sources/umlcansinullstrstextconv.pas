(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansinullstrstextconv;

{$mode objfpc}{$H+}

interface
uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  umlccomparisons,
  umlcstddatetimetypes,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcstdnullstrtypes,
  umlcstdfloattypes,
  //umlcbinfloattypes,
  //umlcdecfloattypes,
  umlcansimemos,
  umlcansishortstrings,
  umlcansichars,
  umlcansicharsets,
  umlctextconsts,
  umlcansicharsetconsts,
  umlcansinullstrs,
  dummy;


  function NullStrToChar
    (const AValue: ansinullstring): char;
  function NullStrToStr
    (const AValue: ansinullstring): string;
  function NullStrToDate
    (const AValue: ansinullstring): TDateTime;
  function NullStrToTime
    (const AValue: ansinullstring): TDateTime;
  function NullStrToInt
    (const AValue: ansinullstring): Integer;
  function NullStrToFloat
    (const AValue: ansinullstring): TDBFloat;
  function NullStrToCurr
    (const AValue: ansinullstring): Currency;

  function AssignStr
    (var Dest: ansinullstring; const AValue: string): ansinullstring;
  function AssignDate
    (var Dest: ansinullstring; const AValue: TDateTime): ansinullstring;
  function AssignTime
    (var Dest: ansinullstring; const AValue: TDateTime): ansinullstring;
  function AssignInt
    (var Dest: ansinullstring; const AValue: Integer): ansinullstring;
  function AssignFloat
    (var Dest: ansinullstring; const AValue: TDBFloat): ansinullstring;
  function AssignCurr
    (var Dest: ansinullstring; const AValue: Currency): ansinullstring;


implementation


function NullStrToChar
  (const AValue: ansinullstring): char;
begin
  Result := AValue^;
  // Objetivo: Regresa un caracter a partir de una cadena nula.
  // Goal: Returns a character from a null string.
end;

function NullStrToStr
  (const AValue: ansinullstring): string;
begin
  System.SetLength(Result, StrLen(AValue));
  Result := AValue;
  // Objetivo: Regresa una cadena pascal de copia de una cadena nula.
  // Goal: Returns a pascal string copy of a null string.
end;

function NullStrToDate
  (const AValue: ansinullstring): TDateTime;
begin
  Result := NoDateTime;
    //StrToDate(NullStrToStr(AValue));
  // Objetivo: Regresa una fecha a partir de una cadena terminada en nulo.
  // Goal: Returns a date from a null terminated string.
end;

function NullStrToTime
  (const AValue: ansinullstring): TDateTime;
begin
  Result := NoTime;
  //StrToTime(NullStrToStr(AValue));
  // Objetivo: Regresa una hora a partir de una cadena terminada en nulo.
  // Goal: Returns a time from a null terminated string.
end;

function NullStrToInt
  (const AValue: ansinullstring): Integer;
begin
  Result := 0;
  //StrToIntDef(NullStrToStr(AValue), 0);
  // Objetivo: Regresa un entero a partir de una cadena terminada en nulo.
  // Goal: Returns an integer from a null terminated string.
end;

function NullStrToFloat
  (const AValue: ansinullstring): TDBFloat;
begin
  Result := 0;
  //StrToFloat(NullStrToStr(AValue));
  // Objetivo: Regresa un numero de punto flotante
  // a partir de una cadena terminada en nulo.

  // Goal: Returns a floating decimal number
  // from a null terminated string.
end;

function NullStrToCurr
  (const AValue: ansinullstring): Currency;
begin
  Result := 0;
  //StrToCurr(NullStrToStr(AValue));
  // Objetivo: Regresa un valor moneda
  // a partir de una cadena terminada en nulo.
  // Goal: Returns a currency AValue from a null terminated string.
end;

function AssignStr
  (var Dest: ansinullstring; const AValue: string): ansinullstring;
begin
//if Assigned(Dest)
//  then FreeNullStr(Dest);
  Dest := StrToNullStr(AValue);
  Result := Dest;
  // Objetivo: Reemplazar el contenido de una cadena terminada en nulo por
  // una cadena pascal.
  // Goal: Replace a null terminated string for a pascal string.
end;

function AssignDate
  (var Dest: ansinullstring; const AValue: TDateTime): ansinullstring;
begin
  Result := nil;

  //if (Assigned(Dest))
  //  then FreeNullStr(Dest);
  //Dest := StrToNullStr(DateToStr(AValue));
  //Result := Dest;
  // Objetivo: Reemplazar el contenido de una cadena terminada en nulo por
  // una fecha.
  // Goal: Replace a null terminated string for a date.
end;

function AssignTime
  (var Dest: ansinullstring; const AValue: TDateTime): ansinullstring;
begin {TimeToNull}
  Result := nil;

  //if (Assigned(Dest))
  //  then FreeNullStr(Dest);
  //Dest := StrToNullStr(TimeToStr(AValue));
  //Result := Dest;
  // Objetivo: Reemplazar el contenido de una cadena terminada en nulo por
  // una fecha.
  // Goal: Replace a null terminated string for a date.
end;

function AssignInt
  (var Dest: ansinullstring; const AValue: Integer): ansinullstring;
begin {IntToNull}
  Result := nil;

  //if (Assigned(Dest))
  //  then FreeNullStr(Dest);
  //Dest := StrToNullStr(IntToStr(AValue));
  //Result := Dest;
  // Objetivo: Reemplazar el contenido de una cadena terminada en nulo por
  // un entero.
  // Goal: Replace a null terminated string for an integer.
end;

function AssignFloat
  (var Dest: ansinullstring; const AValue: TDBFloat): ansinullstring;
begin {FloatToNull}
  Result := nil;

  //if (Assigned(Dest))
  //  then FreeNullStr(Dest);
  //Dest := StrToNullStr(FloatToStr(AValue));
  //Result := Dest;
  // Objetivo: Reemplazar el contenido de una cadena terminada en nulo por
  // un numero de punto flotante.
  // Goal: Replace a null terminated string for an floating decimal number.
end;

function AssignCurr
  (var Dest: ansinullstring; const AValue: Currency): ansinullstring;
begin {CurrToNull}
  Result := nil;

  //if (Assigned(Dest))
  //  then FreeNullStr(Dest);
  //Dest := StrToNullStr(CurrToStr(AValue));
  //Result := Dest;
  // Objetivo: Reemplazar el contenido de una cadena terminada en nulo por
  // un numero de punto flotante.
  // Goal: Replace a null terminated string for an floating decimal number.
end;


end.

