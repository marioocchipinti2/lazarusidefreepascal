(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfloatcurrencytostrconv;

{$mode objfpc}{$H+}

interface
uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes,
  umlcfloatcurrencies,
  umlcansishortstrings,
  umlcansimemos,
  umlctextconsts,
  umlcansicharsets,
  umlcansicharsetconsts,
  dummy;

// ---

const

 MOD_umlcfloatcurrencytostrconv : TUMLCModule =
   ($E3,$61,$EA,$B0,$A1,$2E,$2C,$46,$85,$2E,$6D,$74,$F3,$26,$95,$11);

// ---

(* global functions *)

  function StrToCurrDef
    (const AValue: string; const ADefValue: umlcfloat_currency): umlcfloat_currency;

  function AnsiMemoToCurrDef
    (const AValue: ansimemo; const ADefValue: umlcfloat_currency): umlcfloat_currency; overload;

implementation

(* global functions *)

function StrToCurrDef
  (const AValue: string; const ADefValue: umlcfloat_currency): umlcfloat_currency;
var i, l: byte; AnyError: Boolean;
begin
  I := 1;
  L := System.Length(AValue);
  AnyError := L < 1;
  // Revisar longuitud primero

  if (L > 1) then
  while ((i<=L) and not (AnyError)) do
  begin
    AnyError :=
      not IsMember(CurrSet, AValue[i]);
    Inc(i);
  end;
  // Under construction

  if (AnyError)
    then Result := ADefValue
    else Result := SysUtils.StrToCurr(AValue);
  // Goal: Returns the currency value of "Value" with type protection.
  // Objetivo: Regresa el valor moneda de "Value" con proteccion a errores.
end;


function ansimemoToCurrDef
  (const AValue: ansimemo; const ADefValue: Currency): Currency;
begin
  Result :=
    StrToFloatDef(umlcansimemos.MemoToStr(AValue), ADefValue);
  // Goal: Returns the currency value of "Value" with type protection.
  // Objetivo: Regresa el valor moneda de "Value" con proteccion a errores.
end;


end.

