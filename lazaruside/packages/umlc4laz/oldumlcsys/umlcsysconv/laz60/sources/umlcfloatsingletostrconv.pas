(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfloatsingletostrconv;

{$mode objfpc}{$H+}

interface
uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes,
  umlcfloatsingles,
  umlcansishortstrings,
  umlcansimemos,
  umlctextconsts,
  umlcansicharsets,
  umlcansicharsetconsts,
  dummy;

// ---

const

 MOD_umlcfloatsingletostrconv : TUMLCModule =
   ($C9,$15,$90,$F9,$7F,$51,$45,$48,$95,$85,$EE,$56,$65,$A2,$9D,$D7);

// ---


(* global functions *)

  function SingleToStr
    (const AValue: umlcfloat_single): string;
  function StrToSingle
    (const AValue: string): umlcfloat_single;

  function StrToSingleDef
    (const AValue: string; const ADefValue: umlcfloat_single): umlcfloat_single;

  function ansimemoToSingleDef
    (const AValue: ansimemo; const ADefValue: umlcfloat_single): umlcfloat_single; overload;



implementation

(* global functions *)

function SingleToStr
  (const AValue: umlcfloat_single): string;
begin
  Result := SysUtils.FloatToStr(AValue);
end;

function StrToSingle
  (const AValue: string): umlcfloat_single;
begin
  Result := SysUtils.StrToFloat(AValue);
end;

function StrToSingleDef
  (const AValue: string; const ADefValue: umlcfloat_single): umlcfloat_single;
var i, l: byte; AnyError: Boolean;
begin
  I := 1;
  L := System.Length(AValue);
  AnyError := L < 1;
  // Revisar longuitud primero

  if (L > 1)
    then
  while ((i<=L) and not (AnyError)) do
  begin
    AnyError :=
      not IsMember(FloatSet, AValue[i]);
    Inc(i);
  end;
  // Under construction

  if (AnyError)
    then Result := ADefValue
    else Result := SysUtils.StrToFloat(AValue);
  // Goal: Returns the float value of "Value" with error protection.
  // Objetivo: Regresa el valor flotante de "Value" con proteccion a errores.
end;

function AnsiMemoToSingleDef
  (const AValue: ansimemo; const ADefValue: umlcfloat_single): umlcfloat_single;
begin
  Result :=
    SysUtils.StrToFloatDef(umlcansimemos.MemoToStr(AValue), ADefValue);
  // Goal: Returns the float value of "Value" with error protection.
  // Objetivo: Regresa el valor flotante de "Value" con proteccion a errores.
end;


end.

