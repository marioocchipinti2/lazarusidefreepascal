
(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfloatsdoubletostrconv;

{$mode objfpc}{$H+}

interface
uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes,
  umlcfloatdoubles,
  umlcansishortstrings,
  umlcansimemos,
  umlctextconsts,
  umlcansicharsets,
  umlcansicharsetconsts,
  dummy;

// ---

const

 MOD_umlcfloatsdoubletostrconv : TUMLCModule =
   ($53,$D0,$AD,$35,$69,$89,$35,$4B,$8E,$2C,$31,$E0,$7F,$EB,$10,$38);

// ---

(* global functions *)

  function DoubleToStr
    (const AValue: umlcfloat_double): string;
  function StrToDouble
    (const AValue: string): umlcfloat_double;

  function FormatDoubleToStr
    (const AFormat: string;
     const AValue:  umlcfloat_double): string;
  function FormatStrToDouble
    (const AFormat: string;
     const AValue: string): umlcfloat_double;


implementation

(* global functions *)

function DoubleToStr
  (const AValue: umlcfloat_double): string;
begin
  Result := SysUtils.FloatToStr(AValue);
end;

function StrToDouble
  (const AValue: string): umlcfloat_double;
begin
  Result := SysUtils.StrToFloat(AValue);
end;

function FormatDoubleToStr
  (const AFormat: string;
   const AValue:  umlcfloat_double): string;
begin
  Result := '';
  // Goal: Returns a string representation of the given double value.
  // Objetivo: Regresa una representacion alfanumerica del valor doble dado.
end;

function FormatStrToDouble
  (const AFormat: string;
   const AValue: string): umlcfloat_double;
begin
  Result := 0;
  // Goal: Returns a double value from the given string.
  // Objetivo: Regresa un valor doble a partir de la cadena dada.
end;



end.

