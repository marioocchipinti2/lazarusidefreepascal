unit umlcfloatextendedtostrconv;

{$mode objfpc}{$H+}

interface
uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes,
  umlcfloatsingles,
  umlcansishortstrings,
  umlcansimemos,
  umlctextconsts,
  umlcansicharsets,
  umlcansicharsetconsts,
  dummy;

// ---

const

 MOD_umlcfloatextendedtostrconv : TUMLCModule =
   ($20,$58,$95,$D6,$A9,$9A,$C5,$4B,$98,$A4,$AB,$A8,$22,$EC,$0E,$D0);

// ---

implementation

end.

