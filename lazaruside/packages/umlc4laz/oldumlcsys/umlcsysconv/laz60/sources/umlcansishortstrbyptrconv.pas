(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansishortstrbyptrconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcstdstrtypes,
  umlcstrings,
  umlcmaskarrays,
  umlcansishortstrings,
  umlcshortstrbyptrs,
  dummy;

// ---

const

  MOD_umlcshortstrbyptrconv : TUMLCModule =
    ($D8,$E3,$21,$94,$1D,$4F,$82,$45,$B4,$9F,$6A,$0E,$9E,$B3,$A8,$8C);

// ---

(* global functions *)



(* global procedures *)

procedure ShortStrPtrToStr
  (out ADest: string; const ASource: pointer);


implementation

(* global functions *)



(* global procedures *)

procedure ShortStrPtrToStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpansishortstring;
    ACount, ARealCount, I: Byte;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpansishortstring(ASource);

  ACount :=
    umlcansishortstrings.Length(ASourcePtr^);
  ARealCount :=
    Math.Min(sizeof(umlcansishortstring), ACount);

  I := 0;
  while (I <= ARealCount) do
  begin
    umlcstrings.ConcatStr(ADest, ASourcePtr^[I]);

    System.Inc(I);
  end;
end;

end.

