(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuinttohexstrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for memory based unsigned integers,
 ** using hexadecimal base representation numbers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlchexas,
  dummy;

// ---

const

  MOD_umlcInttohexstrconv : TUMLCModule =
    ($D8,$A0,$67,$55,$18,$79,$58,$49,$BB,$A1,$1C,$F6,$C5,$8F,$D9,$8B);

// ---

(* global functions *)

function TryParseUInt8ToHexStr
  (out ADest: string; const ASource: umlcuint_8): Boolean;

function TryParseUInt16ToHexStr
  (out ADest: string; const ASource: umlcuint_16): Boolean;

function TryParseUInt32ToHexStr
  (out ADest: string; const ASource: umlcuint_32): Boolean;

function TryParseUInt64ToHexStr
  (out ADest: string; const ASource: umlcuint_64): Boolean;

function TryParseUInt128ToHexStr
  (out ADest: string; const ASource: umlcuint_128): Boolean;

function TryParseUInt256ToHexStr
  (out ADest: string; const ASource: umlcuint_256): Boolean;

function TryParseUInt512ToHexStr
  (out ADest: string; const ASource: umlcuint_512): Boolean;

// ---

function TryConcatUInt8ToHexStr
  (var ADest: string; const ASource: umlcuint_8): Boolean;

function TryConcatUInt16ToHexStr
  (var ADest: string; const ASource: umlcuint_16): Boolean;

function TryConcatUInt32ToHexStr
  (var ADest: string; const ASource: umlcuint_32): Boolean;

function TryConcatUInt64ToHexStr
  (var ADest: string; const ASource: umlcuint_64): Boolean;

function TryConcatUInt128ToHexStr
  (var ADest: string; const ASource: umlcuint_128): Boolean;

function TryConcatUInt256ToHexStr
  (var ADest: string; const ASource: umlcuint_256): Boolean;

function TryConcatUInt512ToHexStr
  (var ADest: string; const ASource: umlcuint_512): Boolean;

(* global procedures *)

procedure ParseUInt8ToHexStr
  (out ADest: string; const ASource: umlcuint_8);

procedure ParseUInt16ToHexStr
  (out ADest: string; const ASource: umlcuint_16);

procedure ParseUInt32ToHexStr
  (out ADest: string; const ASource: umlcuint_32);

procedure ParseUInt64ToHexStr
  (out ADest: string; const ASource: umlcuint_64);

procedure ParseUInt128ToHexStr
  (out ADest: string; const ASource: umlcuint_128);

procedure ParseUInt256ToHexStr
  (out ADest: string; const ASource: umlcuint_256);

procedure ParseUInt512ToHexStr
  (out ADest: string; const ASource: umlcuint_512);

// ---

procedure ConcatUInt8ToHexStr
  (var ADest: string; const ASource: umlcuint_8);

procedure ConcatUInt16ToHexStr
  (var ADest: string; const ASource: umlcuint_16);

procedure ConcatUInt32ToHexStr
  (var ADest: string; const ASource: umlcuint_32);

procedure ConcatUInt64ToHexStr
  (var ADest: string; const ASource: umlcuint_64);

procedure ConcatUInt128ToHexStr
  (var ADest: string; const ASource: umlcuint_128);

procedure ConcatUInt256ToHexStr
  (var ADest: string; const ASource: umlcuint_256);

procedure ConcatUInt512ToHexStr
  (var ADest: string; const ASource: umlcuint_512);

implementation

(* global functions *)

function TryParseUInt8ToHexStr
  (out ADest: string; const ASource: umlcuint_8): Boolean;
var Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result := false;
  ADest := '';
  umlcstrings.Clear(ADest);

  Count  := 0;
  Number := ASource;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('TryParseUInt8ToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := chr(Remainder + 48);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatChar(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

function TryParseUInt16ToHexStr
  (out ADest: string; const ASource: umlcuint_16): Boolean;
var Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result := false;
  ADest := '';
  umlcstrings.Clear(ADest);

  Count  := 0;
  Number := ASource;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('TryParseUInt16ToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := chr(Remainder + 48);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatChar(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

function TryParseUInt32ToHexStr
  (out ADest: string; const ASource: umlcuint_32): Boolean;
var Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result := false;
  ADest := '';
  umlcstrings.Clear(ADest);

  Count  := 0;
  Number := ASource;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('TryParseUInt8ToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := chr(Remainder + 48);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatChar(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

function TryParseUInt64ToHexStr
  (out ADest: string; const ASource: umlcuint_64): Boolean;
var Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result := false;
  ADest := '';
  umlcstrings.Clear(ADest);

  Count  := 0;
  Number := ASource;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('TryParseUInt8ToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := chr(Remainder + 48);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatChar(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

function TryParseUInt128ToHexStr
  (out ADest: string; const ASource: umlcuint_128): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin
  Result := false;
end;

function TryParseUInt256ToHexStr
  (out ADest: string; const ASource: umlcuint_256): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin
  Result := false;
end;

function TryParseUInt512ToHexStr
  (out ADest: string; const ASource: umlcuint_512): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin
  Result := false;
end;

// ---

function TryConcatUInt8ToHexStr
  (var ADest: string; const ASource: umlcuint_8): Boolean;
var Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result := true;

  Count  := 0;
  Number := ASource;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatUInt8PtrToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

function TryConcatUInt16ToHexStr
  (var ADest: string; const ASource: umlcuint_16): Boolean;
var Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result := true;

  Count  := 0;
  Number := ASource;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatUInt16PtrToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

function TryConcatUInt32ToHexStr
  (var ADest: string; const ASource: umlcuint_32): Boolean;
var Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result := true;

  Count  := 0;
  Number := ASource;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatUInt32PtrToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  //if ((Count mod 2) <> 0) then
  //begin
  //  umlcstrings.ConcatChar(ADest, '0');
  //end;

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

function TryConcatUInt64ToHexStr
  (var ADest: string; const ASource: umlcuint_64): Boolean;
var Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result := true;

  Count  := 0;
  Number := ASource;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatUInt64PtrToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

function TryConcatUInt128ToHexStr
  (var ADest: string; const ASource: umlcuint_128): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result := true;

  ASourcePtr := pbyte(@ASource);

  //umlcdebug.DebugWriteLn('ConcatUInt128PtrToHexStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcuint_128);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlchexas.ByteToHexStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

end;

function TryConcatUInt256ToHexStr
  (var ADest: string; const ASource: umlcuint_256): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result := true;

  ASourcePtr := pbyte(@ASource);

  //umlcdebug.DebugWriteLn('ConcatUInt256PtrToHexStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcuint_256);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlchexas.ByteToHexStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

end;

function TryConcatUInt512ToHexStr
  (var ADest: string; const ASource: umlcuint_512): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result := true;

  ASourcePtr := pbyte(@ASource);

  //umlcdebug.DebugWriteLn('ConcatUInt512PtrToHexStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcuint_512);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlchexas.ByteToHexStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

(* global procedures *)

procedure ParseUInt8ToHexStr
  (out ADest: string; const ASource: umlcuint_8);
begin
  if (not TryParseUInt8ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt16ToHexStr
  (out ADest: string; const ASource: umlcuint_16);
begin
  if (not TryParseUInt16ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt32ToHexStr
  (out ADest: string; const ASource: umlcuint_32);
begin
  if (not TryParseUInt32ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt64ToHexStr
  (out ADest: string; const ASource: umlcuint_64);
begin
  if (not TryParseUInt64ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt128ToHexStr
  (out ADest: string; const ASource: umlcuint_128);
begin
  if (not TryParseUInt128ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt256ToHexStr
  (out ADest: string; const ASource: umlcuint_256);
begin
  if (not TryParseUInt256ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt512ToHexStr
  (out ADest: string; const ASource: umlcuint_512);
begin
  if (not TryParseUInt512ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

// ---


procedure ConcatUInt8ToHexStr
  (var ADest: string; const ASource: umlcuint_8);
begin
  if (not TryConcatUInt8ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt16ToHexStr
  (var ADest: string; const ASource: umlcuint_16);
begin
  if (not TryConcatUInt16ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt32ToHexStr
  (var ADest: string; const ASource: umlcuint_32);
begin
  if (not TryConcatUInt32ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt64ToHexStr
  (var ADest: string; const ASource: umlcuint_64);
begin
  if (not TryConcatUInt64ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt128ToHexStr
  (var ADest: string; const ASource: umlcuint_128);
begin
  if (not TryConcatUInt128ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt256ToHexStr
  (var ADest: string; const ASource: umlcuint_256);
begin
  if (not TryConcatUInt256ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt512ToHexStr
  (var ADest: string; const ASource: umlcuint_512);
begin
  if (not TryConcatUInt512ToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;


end.

