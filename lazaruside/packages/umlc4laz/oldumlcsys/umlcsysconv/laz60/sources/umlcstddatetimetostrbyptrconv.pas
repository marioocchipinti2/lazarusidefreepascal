(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstddatetimetostrbyptrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for pointers to Date and Time type variables.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcstddatetimetypes,
  umlcstddatetimes,
  umlcstrings,
  dummy;

// ---

const

  MOD_umlcstddatetimetostrbyconv : TUMLCModule =
    ($1D,$8F,$77,$D0,$C3,$F3,$58,$48,$99,$F0,$DA,$88,$DB,$7F,$EA,$CA);

// ---

(* global functions *)

function TryParseDateTimePtrToStr
  (out ADest: string; const ASource: pointer): Boolean;

// ---

function TryConcatDateTimePtrToStr
  (var ADest: string; const ASource: pointer): Boolean;

(* global procedures *)

procedure ParseDateTimePtrToStr
  (out ADest: string; const ASource: pointer);

// ---

procedure ConcatDateTimePtrToStr
  (var ADest: string; const ASource: pointer);



implementation

(* global functions *)

function TryParseDateTimePtrToStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpstddatetime;
  AHour, AMin, ASec, AMSec: Word;
  IsEvening: Boolean;
  AYear, AMonth, ADay: Word;
begin
  ADest := '';
  umlcstrings.Clear(ADest);

  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpstddatetime(ASource);
    umlcstddatetimes.DecodeDateTime
      (ASourcePtr^,
       AYear, AMonth, ADay,
       AHour, AMin, ASec, AMSec);

      IsEvening :=
        (AHour > 12);
      if (IsEvening) then
      begin
        System.Dec(AHour, 12);
      end;

      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(AYear));
      umlcstrings.ConcatChar
        (ADest, '/');
      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(AMonth));
      umlcstrings.ConcatChar
        (ADest, '/');
      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(ADay));

      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(AHour));
      umlcstrings.ConcatChar
        (ADest, ':');
      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(AMin));
      umlcstrings.ConcatChar
        (ADest, ':');
      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(ASec));
      umlcstrings.ConcatChar
        (ADest, ':');
      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(AMSec));

      if (IsEvening) then
      begin
        umlcstrings.ConcatStr
          (ADest, 'pm');
      end else
      begin
        umlcstrings.ConcatStr
          (ADest, 'am');
      end;
  end;
end;

// ---

function TryConcatDateTimePtrToStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpstddatetime;
  AHour, AMin, ASec, AMSec: Word;
  IsEvening: Boolean;
  AYear, AMonth, ADay: Word;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpstddatetime(ASource);
    umlcstddatetimes.DecodeDateTime
      (ASourcePtr^,
       AYear, AMonth, ADay,
       AHour, AMin, ASec, AMSec);

      IsEvening :=
        (AHour > 12);
      if (IsEvening) then
      begin
        System.Dec(AHour, 12);
      end;

      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(AYear));
      umlcstrings.ConcatChar
        (ADest, '/');
      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(AMonth));
      umlcstrings.ConcatChar
        (ADest, '/');
      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(ADay));
      umlcstrings.ConcatChar
        (ADest, ' ');
      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(AHour));
      umlcstrings.ConcatChar
        (ADest, ':');
      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(AMin));
      umlcstrings.ConcatChar
        (ADest, ':');
      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(ASec));
      umlcstrings.ConcatChar
        (ADest, ':');
      umlcstrings.ConcatStr
        (ADest, SysUtils.IntToStr(AMSec));

      if (IsEvening) then
      begin
        umlcstrings.ConcatStr
          (ADest, 'pm');
      end else
      begin
        umlcstrings.ConcatStr
          (ADest, 'am');
      end;
  end;
end;

(* global procedures *)

procedure ParseDateTimePtrToStr
  (out ADest: string; const ASource: pointer);
begin
  if (not TryParseDateTimePtrToStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

// ---

procedure ConcatDateTimePtrToStr
  (var ADest: string; const ASource: pointer);
begin
  if (not TryConcatDateTimePtrToStr
    (ADest, ASource)) then
  begin
    //
  end;
end;



end.

