(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbooltostrbyptrconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcbooleans,
  umlcbooltostrconv,
  dummy;

// ---

const

  MOD_umlcbooltostrbyptrconv : TUMLCModule =
    ($EA,$8B,$4F,$AF,$9C,$21,$64,$4F,$9E,$BA,$A2,$77,$EE,$AD,$0D,$1D);

// ---

(* global functions *)



(* global procedures *)

procedure BoolPtrToCharStr
  (out ADest: string; const ASource: pointer);

procedure BoolPtrToNumberStr
  (out ADest: string; const ASource: pointer);

procedure BoolPtrToShortStr
  (out ADest: string; const ASource: pointer);

procedure BoolPtrToLongStr
  (out ADest: string; const ASource: pointer);


implementation

(* global functions *)



(* global procedures *)

procedure BoolPtrToCharStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pboolean; AChar: Char;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := pboolean(ASource);
  AChar :=
    umlcbooltostrconv.BoolToChar(ASourcePtr^);

  umlcstrings.ConcatChar
    (ADest, AChar);
end;

procedure BoolPtrToNumberStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pboolean; AChar: Char;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := pboolean(ASource);
  if (ASourcePtr^) then
  begin
    AChar := '1';
  end else
  begin
    AChar := '0';
  end;

  umlcstrings.ConcatChar(ADest, AChar);
end;

procedure BoolPtrToShortStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pboolean; AStr: string;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := pboolean(ASource);
  AStr :=
    umlcbooltostrconv.BoolToShortText(ASourcePtr^);

  umlcstrings.ConcatStr
    (ADest, AStr);
end;

procedure BoolPtrToLongStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pboolean; AStr: string;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := pboolean(ASource);
  AStr :=
    umlcbooltostrconv.BoolToLongText(ASourcePtr^);

  umlcstrings.ConcatStr
    (ADest, AStr);
end;

end.

