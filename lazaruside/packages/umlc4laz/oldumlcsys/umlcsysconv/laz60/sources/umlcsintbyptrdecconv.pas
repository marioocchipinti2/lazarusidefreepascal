unit umlcsintbyptrdecconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcsinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcdecs,
  dummy;

// ---

const

  MOD_umlcsintbyptrdecconv : TUMLCModule =
    ($89,$A4,$2F,$03,$C4,$7A,$FE,$46,$8E,$60,$49,$FA,$B3,$37,$B7,$D2);

// ---

(* global functions *)



(* global procedures *)

procedure UIntPtrToDecStr
  (out ADest: string; const ASource: pointer);

// ---

procedure ConcatSInt8PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt16PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt32PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt64PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt128PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt256PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt512PtrToDecStr
  (var ADest: string; const ASource: pointer);


implementation

(* global functions *)



(* global procedures *)

procedure UInt8ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_8;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_8(ASource);

    // ...
end;

procedure UInt16ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_16;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_16(ASource);

  // ...
end;

procedure UInt32ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_32;
begin
  //umlcdebug.DebugWriteLn('UInt32ToDecStr(...)');
  //umlcdebug.DebugWriteEoLn();

  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_32(ASource);

  ADest :=
    SysUtils.IntToStr(ASourcePtr^);
end;

procedure UInt64ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_64;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_64(ASource);

  // ...
end;

procedure UInt128ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_128;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_128(ASource);

  // ...
end;

procedure UInt256ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_256;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_256(ASource);

  // ...
end;

procedure UInt512ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_512;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_512(ASource);

  // ...
end;

procedure UIntPtrToDecStr
  (out ADest: string; const ASource: pointer);
begin
  umlcstrings.Clear(ADest);

  if (sizeof(Cardinal) = sizeof(umlcsint_32))
    then UInt32ToDecStr(ADest, ASource)

  //if (sizeof(Cardinal) = sizeof(umlcsint_8))
  //  then UInt8ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_16))
  //  then UInt16ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_32))
  //  then UInt32ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_64))
  //  then UInt64ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_128))
  //  then UInt128ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_256))
  //  then UInt256ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_512))
  //  then UInt512ToDecStr(ADest, ASource);
end;

// ---

procedure ConcatSInt8PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_8;
    Temp: string;
    ASrcInteger: Integer;
begin
  ASourcePtr := umlcpsint_8(ASource);

    ASrcInteger := ASourcePtr^;

    Temp := '';
    umlcdecs.ConcatIntegerToDecStr
      (Temp, ASrcInteger);

    //Temp :=
    //  IntToStr(ASourcePtr^);
  umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatSInt16PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_16;
    Temp: string;
    ASrcInteger: Integer;
begin
  ASourcePtr := umlcpsint_16(ASource);

  ASrcInteger := ASourcePtr^;

  Temp := '';
  umlcdecs.ConcatIntegerToDecStr
    (Temp, ASrcInteger);

  //Temp :=
  //  IntToStr(ASourcePtr^);
  umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatSInt32PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_32;
    Temp: string;
    ASrcInteger: Integer;
begin
  ASourcePtr := umlcpsint_32(ASource);

  ASrcInteger := ASourcePtr^;

  Temp := '';
  umlcdecs.ConcatIntegerToDecStr
    (Temp, ASrcInteger);

  //Temp :=
  //  IntToStr(ASourcePtr^);
  umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatSInt64PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_64;
  Temp: string;
begin
  ASourcePtr := umlcpsint_64(ASource);

  Temp :=
    IntToStr(ASourcePtr^);
  umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatSInt128PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_128;
  Temp: string;
begin
  ASourcePtr := umlcpsint_128(ASource);

  //Temp :=
  //  IntToStr(ASourcePtr^);
  //umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatSInt256PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_256;
  Temp: string;
begin
  ASourcePtr := umlcpsint_256(ASource);

  //Temp :=
  //  IntToStr(ASourcePtr^);
  //umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatSInt512PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_512;
  Temp: string;
begin
  ASourcePtr := umlcpsint_512(ASource);

  //Temp :=
  //  IntToStr(ASourcePtr^);
  //umlcstrings.ConcatStr(ADest, Temp);
end;





end.

