(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbins;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for binary base representation numbers.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlcstdtypes, umlcconvtypes, umlcuinttypes,
  umlcmaskarrays,
  dummy;

// ---

const

 MOD_umlcbins : TUMLCModule =
   ($89,$3F,$4D,$7C,$C4,$F7,$FB,$4B,$95,$92,$EF,$8D,$C4,$81,$89,$6D);

// ---

resourcestring
  resBinaryOverflow = 'Binary digit overflow';
(*
resource
  resBinaryOverflow: string = '';
*)

function DecToBinChar
  (const AValue: umlcdecimal): umlcbinarychar;

function IsBinaryDigit
  (const AValue: ansichar): Boolean;

function IntToBinStrByCount
  (const AValue: uint64; const ADigitCount: Byte): ansistring;

function ByteToBinStr
  (const AValue: Byte): umlcbinarystring;

function WordToBinStr
  (const AValue: Word): umlcbinarystring;

function LongWordToBinStr
  (const AValue: LongWord): umlcbinarystring;

function Int64ToBinStr
  (const AValue: Int64): umlcbinarystring;

  function TryBinStrToUInt8
    (var ADest: umlcuint_8; const ASource: umlcbinarystring): Boolean;
  function TryBinStrToUInt16
    (var ADest: umlcuint_16; const ASource: umlcbinarystring): Boolean;
  function TryBinStrToUInt32
    (var ADest: umlcuint_32; const ASource: umlcbinarystring): Boolean;
  function TryBinStrToUInt64
    (var ADest: umlcuint_64; const ASource: umlcbinarystring): Boolean;

  function TryBinStrToUInt128
    (var ADest: umlcuint_128; const ASource: umlcbinarystring): Boolean;
  function TryBinStrToUInt256
    (var ADest: umlcuint_256; const ASource: umlcbinarystring): Boolean;
  function TryBinStrToUInt512
    (var ADest: umlcuint_512; const ASource: umlcbinarystring): Boolean;

  procedure UInt8ToBinStr
    (var ADest: umlcbinarystring; const ASource: umlcuint_8);
  procedure UInt16ToBinStr
    (var ADest: umlcbinarystring; const ASource: umlcuint_16);
  procedure UInt32ToBinStr
    (var ADest: umlcbinarystring; const ASource: umlcuint_32);
  procedure UInt64ToBinStr
    (var ADest: umlcbinarystring; const ASource: umlcuint_64);

  procedure UInt128ToBinStr
    (var ADest: umlcbinarystring; const ASource: umlcuint_128);
  procedure UInt256ToBinStr
    (var ADest: umlcbinarystring; const ASource: umlcuint_256);
  procedure UInt512ToBinStr
    (var ADest: umlcbinarystring; const ASource: umlcuint_512);

implementation

const BinaryChars: array[0..1] of umlcbinarychar =
   ('0', '1');

function DecToBinChar
  (const AValue: umlcdecimal): umlcbinarychar;
var ASourceValue: umlcdecimal;
begin
  ASourceValue :=
    Math.Min(AValue, 1);
  Result := BinaryChars[ASourceValue];
end;

function IsBinaryDigit
  (const AValue: ansichar): Boolean;
begin
  Result :=
    ((AValue >= '0') or (AValue <= '1'));
end;

function IntToBinStrByCount
  (const AValue: uint64; const ADigitCount: Byte): ansistring;
var p_nb_int: uint64; p_nb_digits: Byte;
begin
  Result := '';

  p_nb_int    := AValue;
  p_nb_digits :=
    Math.Min(ADigitCount, 64);

  System.SetLength(Result, p_nb_digits);
  while (p_nb_digits > 0) do
  begin
    if odd(p_nb_int) then
      Result[p_nb_digits] := '1'
    else
      Result[p_nb_digits] := '0';
    p_nb_int := p_nb_int shr 1;

    System.Dec(p_nb_digits);
  end;
end;

function ByteToBinStr
  (const AValue: Byte): umlcbinarystring;
var
  i: Integer;
  pStr: PChar;
begin
  Result := '';
  System.SetLength(Result, 8);
  pStr  :=
    PChar(Pointer(Result));
  for i := 7 downto 0 do
  begin
    pStr[i] :=
      (Char(Ord('0') + ((AValue shr (7 - i)) and 1)));
  end;
end;

function WordToBinStr
  (const AValue: Word): umlcbinarystring;
begin
  Result :=
    umlcbins.IntToBinStrByCount
      (AValue, 16);
end;

function LongWordToBinStr
  (const AValue: LongWord): umlcbinarystring;
begin
  Result :=
    umlcbins.IntToBinStrByCount
      (AValue, 32);
end;

function Int64ToBinStr
  (const AValue: Int64): umlcbinarystring;
begin
  Result :=
    umlcbins.IntToBinStrByCount
      (AValue, 64);
end;

function TryBinStrToUInt8
(var ADest: umlcuint_8; const ASource: umlcbinarystring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryBinStrToUInt16
(var ADest: umlcuint_16; const ASource: umlcbinarystring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryBinStrToUInt32
(var ADest: umlcuint_32; const ASource: umlcbinarystring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryBinStrToUInt64
(var ADest: umlcuint_64; const ASource: umlcbinarystring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryBinStrToUInt128
(var ADest: umlcuint_128; const ASource: umlcbinarystring): Boolean;
begin
  Result := false;
  //ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryBinStrToUInt256
(var ADest: umlcuint_256; const ASource: umlcbinarystring): Boolean;
begin
  Result := false;
  //ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryBinStrToUInt512
(var ADest: umlcuint_512; const ASource: umlcbinarystring): Boolean;
begin
  Result := false;
  //  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

procedure UInt8ToBinStr
  (var ADest: umlcbinarystring; const ASource: umlcuint_8);
begin
  ADest := '';
end;

procedure UInt16ToBinStr
  (var ADest: umlcbinarystring; const ASource: umlcuint_16);
begin
  ADest := '';
end;

procedure UInt32ToBinStr
  (var ADest: umlcbinarystring; const ASource: umlcuint_32);
begin
  ADest := '';
end;

procedure UInt64ToBinStr
  (var ADest: umlcbinarystring; const ASource: umlcuint_64);
begin
  ADest := '';
end;

procedure UInt128ToBinStr
  (var ADest: umlcbinarystring; const ASource: umlcuint_128);
begin
  ADest := '';
end;

procedure UInt256ToBinStr
  (var ADest: umlcbinarystring; const ASource: umlcuint_256);
begin
  ADest := '';
end;

procedure UInt512ToBinStr
  (var ADest: umlcbinarystring; const ASource: umlcuint_512);
begin
  ADest := '';
end;






end.

