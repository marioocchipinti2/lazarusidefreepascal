(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbooltostrconv;

{$mode objfpc}{$H+}

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlcresBooleans,
  umlctextconsts,
  umlcansicharsets,
  umlcansicharsetconsts,
  dummy;

// ---

const

 MOD_umlcbooltostrconv : TUMLCModule =
   ($72,$15,$53,$85,$32,$96,$46,$47,$81,$26,$E1,$B3,$BC,$7A,$7E,$85);

// ---

  function BoolToChar
     (const ASource: Boolean): char;
  function CharToBool
     (const ASource: char): Boolean;

  function BoolToStr
     (const ASource: Boolean): string;
  function BoolToShortText
     (const ASource: Boolean): string;
  function BoolToLongText
     (const ASource: Boolean): string;

  function StrToBool
     (const ASource: string): Boolean;
  function ShortTextToBool
     (const ASource: string): Boolean;
  function LongTextToBool
     (const ASource: string): Boolean;

implementation

const
  resBoolFalse = 'FALSE';
  resBoolTrue  = 'TRUE';

const
   BoolToStrArray: array[Boolean] of string =
     (resBoolFalse, resBoolTrue);
   BoolToShortTextArray: array[Boolean] of string =
     (resshortFalse, resshortTrue);
   BoolToLongTextArray: array[Boolean] of string =
     (reslongFalse, reslongTrue);

 function BoolToChar
    (const ASource: Boolean): char;
 begin
   if (ASource)
     then Result := 'T'
     else Result := 'F';
   // Goal: To cast a "Boolean" value to a "char" value.
   // Objetivo: Convertir un valor "Boolean" a un valor "char".
 end;

 function CharToBool
  (const ASource: char): Boolean;
 begin
   Result :=
     not IsMember(BoolSet, ASource);
   // Goal: To cast a "char" value to a "Boolean" value.
   // Objetivo: Convertir un valor "char" a un valor "Boolean".
 end;

 function BoolToStr
    (const ASource: Boolean): string;
 begin
   if (ASource)
     then Result := 'TRUE'
     else Result := 'FALSE';

 (*
   Result := BoolToStrArray[Value];
   if (Value)
     then Result := BoolToStrArray[TRUE]
     else Result := BoolToStrArray[FALSE];
 *)
   // Goal: To cast a "boolean" value to a "string" value.
   // Objetivo: Convertir un valor "Boolean" a un valor "string".
 end;

 function BoolToShortText
    (const ASource: Boolean): string;
 begin
   Result := BoolToShortTextArray[ASource];
   // Goal: To cast a "boolean" value to a "string" value
   // (short description).

   // Objetivo: Convertir un valor "Boolean" a un valor "string"
   // (descripcion corta).
 end;

 function BoolToLongText
    (const ASource: Boolean): string;
 begin
   Result := BoolToLongTextArray[ASource];
   // Goal: To cast a "boolean" value to a "string" value
   // (short description).

   // Objetivo: Convertir un valor "Boolean" a un valor "string"
   // (descripcion corta).
 end;

 function StrToBool
    (const ASource: string): Boolean;
 var Source: string;
 begin
   Source := UpperCase(ASource);
   // avoid case incompatibility
   // evitar incompatibilidad por caso

   Result :=
     (Source = BoolToStrArray[TRUE]);
   // Goal: To cast a "string" value to a "Boolean" value.
   // Objetivo: Convertir un valor "string" a un valor "Boolean".
 end;

 function ShortTextToBool
    (const ASource: string): Boolean;
 var Source: string;
 begin
   Source := UpperCase(ASource);
   // avoid case incompatibility
   // evitar incompatibilidad por caso

   Result := (Source = BoolToShortTextArray[TRUE]);
   // Goal: To cast a "string" value to a "Boolean" value (short description).

   // Objetivo: Convertir un valor "string" a un valor "Boolean" }
   // (descripcion corta).
 end;

 function LongTextToBool
    (const ASource: string): Boolean;
 var Source: string;
 begin
   Source := UpperCase(ASource);
   // avoid case incompatibility
   // evitar incompatibilidad por caso

   Result := (Source = BoolToLongTextArray[TRUE]);
   // Goal: To cast a "string" value to a "Boolean" value (short description).

   // Objetivo: Convertir un valor "string" a un valor "Boolean"
   // (descripcion corta).
 end;



end.

