(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

(* "umlcsysconv_language.incpas" *)

{$UNDEF umlcsysconv_language_english}
{$UNDEF umlcsysconv_language_spanisheurope}
{$UNDEF umlcsysconv_language_spanishlatam}
{$UNDEF umlcsysconv_language_french}
{$UNDEF umlcsysconv_language_german}
{$UNDEF umlcsysconv_language_portuguese}
{$UNDEF umlcsysconv_language_italian}

{$DEFINE umlcsysconv_language_english}
{.DEFINE umlcsysconv_language_spanisheurope}
{.DEFINE umlcsysconv_language_spanishlatam}
{.DEFINE umlcsysconv_language_french}
{.DEFINE umlcsysconv_language_german}
{.DEFINE umlcsysconv_language_portuguese}
{.DEFINE umlcsysconv_language_italian}

{.WARNING "umlcsysconv_language.incpas"}

{.FATAL "HELLO MACRO"}
