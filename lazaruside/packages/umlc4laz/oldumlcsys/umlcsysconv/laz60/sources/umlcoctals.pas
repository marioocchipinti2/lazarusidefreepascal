unit umlcoctals;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for octal base representation numbers.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlcstdtypes, umlcconvtypes, umlcuinttypes,
  umlcmaskarrays,
  dummy;

// ---

const

 MOD_umlcoctas : TUMLCModule =
   ($80,$2E,$72,$1E,$46,$3F,$59,$45,$9B,$FF,$5C,$F2,$5D,$36,$16,$8A);

// ---

resourcestring
  resOctalOverflow = 'Octal digit overflow';
(*
resource
  resOctalOverflow: string = '';
*)

function DecToOctalChar
  (const AValue: umlcdecimal): umlcoctalchar;

function IsOctalDigit
  (const AValue: ansichar): Boolean;

function ByteToOctalStr
  (const AValue: Byte): umlcoctalstring;

function WordToOctalStr
  (const AValue: Word): umlcoctalstring;

function LongWordToOctalStr
  (const AValue: LongWord): umlcoctalstring;

  function TryOctalStrToUInt8
    (var ADest: umlcuint_8; const ASource: umlcoctalstring): Boolean;
  function TryOctalStrToUInt16
    (var ADest: umlcuint_16; const ASource: umlcoctalstring): Boolean;
  function TryOctalStrToUInt32
    (var ADest: umlcuint_32; const ASource: umlcoctalstring): Boolean;
  function TryOctalStrToUInt64
    (var ADest: umlcuint_64; const ASource: umlcoctalstring): Boolean;

  function TryOctalStrToUInt128
    (var ADest: umlcuint_128; const ASource: umlcoctalstring): Boolean;
  function TryOctalStrToUInt256
    (var ADest: umlcuint_256; const ASource: umlcoctalstring): Boolean;
  function TryOctalStrToUInt512
    (var ADest: umlcuint_512; const ASource: umlcoctalstring): Boolean;

  procedure UInt8ToOctalStr
    (var ADest: umlcoctalstring; const ASource: umlcuint_8);
  procedure UInt16ToOctalStr
    (var ADest: umlcoctalstring; const ASource: umlcuint_16);
  procedure UInt32ToOctalStr
    (var ADest: umlcoctalstring; const ASource: umlcuint_32);
  procedure UInt64ToOctalStr
    (var ADest: umlcoctalstring; const ASource: umlcuint_64);

  procedure UInt128ToOctalStr
    (var ADest: umlcoctalstring; const ASource: umlcuint_128);
  procedure UInt256ToOctalStr
    (var ADest: umlcoctalstring; const ASource: umlcuint_256);
  procedure UInt512ToOctalStr
    (var ADest: umlcoctalstring; const ASource: umlcuint_512);


implementation

const OctalChars: array[0..7] of umlcoctalchar =
   ('0', '1', '2', '3', '4', '5', '6', '7');

function DecToOctalChar
  (const AValue: umlcdecimal): umlcoctalchar;
var ASourceValue: umlcdecimal;
begin
  ASourceValue :=
    Math.Min(AValue, 7);
  Result :=
    umlcoctals.OctalChars[ASourceValue];
end;

function IsOctalDigit
  (const AValue: ansichar): Boolean;
begin
  Result :=
    ((AValue >= '0') or (AValue <= '7'));
end;

function IntToOctalStr
  (const AValue: Int64): umlcoctalstring;
var Digit: umlcdecimal; ASourceValue: Int64;
begin
  Result := '';

  ASourceValue :=
    AValue;
  while (ASourceValue > 0) do
  begin
    Digit :=
      (ASourceValue AND 7);
    // This is a bitwize AND, meaning:
    //      001001010011010 (Some number)
    //  AND 000000000000111 ( 7 )
    //      ---------------
    //      000000000000010 ( 2 )

    (* convert this digit to Octal *)
    ASourceValue :=
      (ASourceValue shr 3);
    // this "shifts" the number right 3 bits:
    //         001001010011010 (Some number)
    // becomes 000001001010011 (Some number DIV 8)
    Result :=
      Result + umlcoctals.OctalChars[Digit];
  end;
end;

function ByteToOctalStr
  (const AValue: Byte): umlcoctalstring;
begin
  Result :=
    umlcoctals.OctalChars[AValue and 7];
end;

function WordToOctalStr
  (const AValue: Word): umlcoctalstring;
begin
  Result :=
    umlcoctals.IntToOctalStr(AValue);
end;

function LongWordToOctalStr
  (const AValue: LongWord): umlcoctalstring;
begin
  Result :=
    umlcoctals.IntToOctalStr(AValue);
end;

function TryOctalStrToUInt8
(var ADest: umlcuint_8; const ASource: umlcoctalstring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryOctalStrToUInt16
(var ADest: umlcuint_16; const ASource: umlcoctalstring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryOctalStrToUInt32
(var ADest: umlcuint_32; const ASource: umlcoctalstring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryOctalStrToUInt64
(var ADest: umlcuint_64; const ASource: umlcoctalstring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryOctalStrToUInt128
(var ADest: umlcuint_128; const ASource: umlcoctalstring): Boolean;
begin
  Result := false;
  //ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryOctalStrToUInt256
(var ADest: umlcuint_256; const ASource: umlcoctalstring): Boolean;
begin
  Result := false;
  //  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryOctalStrToUInt512
(var ADest: umlcuint_512; const ASource: umlcoctalstring): Boolean;
begin
  Result := false;
  //ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

procedure UInt8ToOctalStr
  (var ADest: umlcoctalstring; const ASource: umlcuint_8);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt16ToOctalStr
  (var ADest: umlcoctalstring; const ASource: umlcuint_16);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt32ToOctalStr
  (var ADest: umlcoctalstring; const ASource: umlcuint_32);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt64ToOctalStr
  (var ADest: umlcoctalstring; const ASource: umlcuint_64);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt128ToOctalStr
  (var ADest: umlcoctalstring; const ASource: umlcuint_128);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt256ToOctalStr
  (var ADest: umlcoctalstring; const ASource: umlcuint_256);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt512ToOctalStr
  (var ADest: umlcoctalstring; const ASource: umlcuint_512);
begin
  ADest := '';
  // @toxdo: ...
end;


end.

