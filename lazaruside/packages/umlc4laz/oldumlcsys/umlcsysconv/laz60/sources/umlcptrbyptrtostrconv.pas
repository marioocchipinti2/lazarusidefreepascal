(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcptrbyptrtostrconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcuinttohexstrconv,
  dummy;

// ---

const

  MOD_umlcptrbyptrconv : TUMLCModule =
    ($90,$CA,$DF,$3E,$F8,$76,$46,$44,$AB,$81,$82,$F1,$F2,$19,$8B,$CF);

// ---

(* global functions *)

function TryConcatPtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;

(* global procedures *)

procedure ConcatPtrToHexStr
  (var ADest: string; const ASource: pointer);

procedure ParsePtrToHexStr
  (out ADest: string; const ASource: pointer);

implementation

(* global functions *)

function TryConcatPtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;
var AByte:  umlcuint_8;
    AWord:  umlcuint_16;
    ADWord: umlcuint_32;
    AQWord: umlcuint_64;
begin
  //umlcdebug.DebugWriteLn('TryConcatPtrToHexStr');
  //umlcdebug.DebugWriteEoLn();
  //
  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  Result := true;
  AByte  := 0;
  AWord  := 0;
  ADWord := 0;
  AQWord := 0;

  if (ASource = nil) then
  begin
    Result := true;
    umlcstrings.ConcatStr(ADest, 'nil');
  end
  else if (sizeof(pointer) = sizeof(umlcuint_8)) then
  begin
    //umlcdebug.DebugWriteLn('8');
    //umlcdebug.DebugWriteEoLn();

    AByte := umlcuint_8(ASource);
    Result :=
      umlcuinttohexstrconv.TryConcatUInt8ToHexStr
        (ADest, AByte);
  end
  else if (sizeof(pointer) = sizeof(umlcuint_16)) then
  begin
    //umlcdebug.DebugWriteLn('16');
    //umlcdebug.DebugWriteEoLn();

    AWord := umlcuint_16(ASource);
    Result :=
      umlcuinttohexstrconv.TryConcatUInt16ToHexStr
        (ADest, AWord);
  end
  else if (sizeof(pointer) = sizeof(umlcuint_32)) then
  begin
    //umlcdebug.DebugWriteLn('32');
    //umlcdebug.DebugWriteEoLn();

    ADWord := umlcuint_32(ASource);
    Result :=
      umlcuinttohexstrconv.TryConcatUInt32ToHexStr
        (ADest, ADWord);
  end
  else if (sizeof(pointer) = sizeof(umlcuint_64)) then
  begin
    //umlcdebug.DebugWriteLn('64');
    //umlcdebug.DebugWriteEoLn();

    AQWord := umlcuint_64(ASource);
    Result :=
      umlcuinttohexstrconv.TryConcatUInt64ToHexStr
        (ADest, AQWord);
  end;
end;

function TryParsePtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;
begin
  Result := false;
  ADest := '';
end;

(* global procedures *)

procedure ConcatPtrToHexStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcptrbyptrtostrconv.TryConcatPtrToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParsePtrToHexStr
  (out ADest: string; const ASource: pointer);
begin
  if (not umlcptrbyptrtostrconv.TryParsePtrToHexStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;


end.

