(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwin16colorstextconv;

{$mode objfpc}{$H+}

interface
uses
{$IFDEF MSWINDOWS}
  Graphics,
{$ENDIF}
{$IFDEF LINUX}
  QGraphics,
{$ENDIF}
  SysUtils,
  umlcmodules, umlctypes,
  umlcwin16colors,
  umlcreswin16colors,
  dummy;

// ---

const

  MOD_umlcwin16colorstextconv : TUMLCModule =
   ($C3,$1D,$CD,$D1,$A1,$0F,$75,$41,$88,$E5,$4B,$BE,$93,$B6,$82,$B5);

// ---

const
  strwin16clBlack   = 'win16clBlack';
  strwin16clMaroon  = 'win16clMaroon';
  strwin16clGreen   = 'win16clGreen';
  strwin16clOlive   = 'win16clOlive';
  strwin16clNavy    = 'win16clNavy';
  strwin16clPurple  = 'win16clPurple';
  strwin16clTeal    = 'win16clTeal';
  strwin16clGray    = 'win16clGray';
  strwin16clSilver  = 'win16clSilver';
  strwin16clRed     = 'win16clRed';
  strwin16clLime    = 'win16clLime';
  strwin16clYellow  = 'win16clYellow';
  strwin16clBlue    = 'win16clBlue';
  strwin16clFuchsia = 'win16clFucsia';
  strwin16clAqua    = 'win16clAqua';
  strwin16clWhite   = 'win16clWhite';

  function Win16colorToStr(const Value: win16color): string;
  function Win16colorToText(const Value: win16color): string;

  function StrToWin16Color(const Value: string): win16color;
  function TextToWin16Color(const Value: string): win16color;

  function ColorToStr(const Value: TColor): string;
  function StrToColor(const Value: string): TColor;


implementation

type
  Twin16colorNames = array[win16color] of string;

const
  win16colorToStrArray: Twin16colorNames =
   ( strwin16clBlack, strwin16clMaroon,  strwin16clGreen,
     strwin16clOlive, strwin16clNavy,    strwin16clPurple,
     strwin16clTeal,  strwin16clGray,    strwin16clSilver,
     strwin16clRed,   strwin16clLime,    strwin16clYellow,
     strwin16clBlue,  strwin16clFuchsia, strwin16clAqua,
     strwin16clWhite );

const
  win16colorToTextArray: Twin16colorNames =
   ( textwin16clBlack, textwin16clMaroon,  textwin16clGreen,
     textwin16clOlive, textwin16clNavy,    textwin16clPurple,
     textwin16clTeal,  textwin16clGray,    textwin16clSilver,
     textwin16clRed,   textwin16clLime,    textwin16clYellow,
     textwin16clBlue,  textwin16clFuchsia, textwin16clAqua,
     textwin16clWhite );


function Win16colorToStr(const Value: win16color): string;
begin
  Result := win16colorToStrArray[Value];
  // Goal: To cast a "win16color" value to a string.
  // Objetivo: Convertir un valor "win16color" a un valor "string".
end;

function Win16colorToText(const Value: win16color): string;
begin
  Result := win16colorToTextArray[Value];
  // Goal: To cast a "win16color" value to a readable text.
  // Objetivo: Convertir un valor "win16color" a un texto legible.
end;

function MatchWin16Color
  (const Value: string; const win16colorNames: Twin16colorNames): Twin16color;
var i: win16color; Found: Boolean;
begin
  i := Low(win16color); Found := FALSE;
  while ((i <= High(Twin16color)) and (not Found)) do
  begin
    Found := SameText(win16colorNames[i], Value);
    Inc(i);
  end;

  if Found
    then Result := Pred(i)
    else Result := Low(win16color);
  // Goal: Locates a win16color by its name in a given array.
end;

function StrToWin16Color(const Value: string): win16color;
begin
  Result := MatchWin16Color(Value, Win16ColorToStrArray);
  // Goal: To cast a "string" value to a "win16color" value.
  // Objetivo: Convertir un valor "string" a un valor "win16color".
end;

function TextToWin16Color(const Value: string): win16color;
begin
  Result := MatchWin16Color(Value, Win16ColorToTextArray);
  // Goal: To cast a readable text to a "win16color" value.
  // Objetivo: Convertir un texto legible a un valor "win16color".
end;

function ColorToStr(const Value: TColor): string;
begin
  Result := Win16ColorToStr( ColorToWin16Color(Value) );
  // Goal: To cast a "TColor" value to a string.
  // Objetivo: Convertir un valor "TColor" a un valor "string".
end;

function StrToColor(const Value: string): TColor;
begin
  Result := Win16ColorToColor( StrToWin16Color(Value) );
  // Goal: To cast a "string" value to a "TColor" value.
  // Objetivo: Convertir un valor "string" a un valor "TColor".
end;

end.

