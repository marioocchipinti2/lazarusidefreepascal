(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcxnumtextconv;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcstdtypes,
  umlcxnums,
  dummy;

// ---

const

  MOD_umlcxnumtextconv : TUMLCModule =
    ($22,$43,$EB,$50,$75,$FE,$D0,$43,$BB,$71,$91,$CF,$DD,$BA,$1D,$0F);

// ---

  procedure IntCplxToStr
  (const ASource: TUMLCIntegerComplexNumber;
   out   ADest:   string);

  procedure DblCplxToStr
  (const ASource: TUMLCDoubleComplexNumber;
   out   ADest:   string);

  procedure ExtCplxToStr
  (const ASource: TUMLCExtendedComplexNumber;
   out   ADest:   string);

  procedure StrToIntCplxDef
  (const ASource:   string;
   out   ADest:     TUMLCIntegerComplexNumber;
   const ADefValue: TUMLCIntegerComplexNumber);

  procedure StrToDblCplxDef
  (const ASource:   string;
   out   ADest:     TUMLCDoubleComplexNumber;
   const ADefValue: TUMLCDoubleComplexNumber);

  procedure StrToExtCplxDef
  (const ASource:   string;
   out   ADest:     TUMLCExtendedComplexNumber;
   const ADefValue: TUMLCExtendedComplexNumber);

  procedure StrToIntCplx
  (const ASource: string;
   out   ADest:   TUMLCIntegerComplexNumber);

  procedure StrToDblCplx
  (const ASource: string;
   out   ADest:   TUMLCDoubleComplexNumber);

  procedure StrToExtCplx
  (const ASource: string;
   out   ADest:   TUMLCExtendedComplexNumber);

implementation

procedure IntCplxToStr
(const ASource: TUMLCIntegerComplexNumber;
 out   ADest:   string);
begin
  ADest := '(';
  ADest := ADest + IntToStr(ASource.Real) + ' + ';
  ADest := ADest + IntToStr(ASource.Imaginary) + 'i';
  ADest := ADest + ')';
end;

procedure DblCplxToStr
(const ASource: TUMLCDoubleComplexNumber;
 out   ADest:   string);
begin
  ADest := '(';
  ADest := ADest + FloatToStr(ASource.Real) + ' + ';
  ADest := ADest + FloatToStr(ASource.Imaginary) + 'i';
  ADest := ADest + ')';
end;

procedure ExtCplxToStr
(const ASource: TUMLCExtendedComplexNumber;
 out   ADest:   string);
begin
  ADest := '(';
  ADest := ADest + FloatToStr(ASource.Real) + ' + ';
  ADest := ADest + FloatToStr(ASource.Imaginary) + 'i';
  ADest := ADest + ')';
end;

procedure StrToIntCplxDef
(const ASource:   string;
 out   ADest:     TUMLCIntegerComplexNumber;
 const ADefValue: TUMLCIntegerComplexNumber);
begin
  IntCplxEncode
    (ADest, ADefValue.Real, ADefValue.Imaginary);
end;

procedure StrToDblCplxDef
(const ASource:   string;
 out   ADest:     TUMLCDoubleComplexNumber;
 const ADefValue: TUMLCDoubleComplexNumber);
begin
  DblCplxEncode
    (ADest, ADefValue.Real, ADefValue.Imaginary);
end;

procedure StrToExtCplxDef
(const ASource:   string;
 out   ADest:     TUMLCExtendedComplexNumber;
 const ADefValue: TUMLCExtendedComplexNumber);
begin
  ExtCplxEncode
    (ADest, ADefValue.Real, ADefValue.Imaginary);
end;

procedure StrToIntCplx
(const ASource: string;
 out   ADest:   TUMLCIntegerComplexNumber);
begin
  IntCplxEncode
    (ADest, 0, 0);
end;

procedure StrToDblCplx
(const ASource: string;
 out   ADest:   TUMLCDoubleComplexNumber);
begin
  //DblCplxEncode
  //  (ADest, 0.0L, 0.0L);
end;

procedure StrToExtCplx
(const ASource: string;
 out   ADest:   TUMLCExtendedComplexNumber);
begin
  //ExtCplxEncode
  //  (ADest, 0.0, 0.0);
end;


end.

