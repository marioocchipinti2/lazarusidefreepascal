(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdtimestamptostrbyptrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for pointers to TimeStamp type variables.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcstddatetimetypes,
  umlctimestamps,
  dummy;

// ---

const

  MOD_umlcstdtimestamptostrbyptrconv : TUMLCModule =
    ($A6,$67,$AE,$49,$DC,$7E,$DB,$4A,$B8,$E9,$4F,$DA,$D5,$D0,$8E,$E4);


// ---

(* global functions *)

function TryParseTimeStampPtrToStr
  (out ADest: string; const ASource: pointer): Boolean;

// ---

function TryConcatTimeStampPtrToStr
  (var ADest: string; const ASource: pointer): Boolean;

(* global procedures *)

procedure ParseTimeStampPtrToStr
  (out ADest: string; const ASource: pointer);

// ---

procedure ConcatTimeStampPtrToStr
  (var ADest: string; const ASource: pointer);

implementation

(* global functions *)

function TryParseTimeStampPtrToStr
  (out ADest: string; const ASource: pointer): Boolean;
begin
  ADest := '';
  Result := false;
end;

// ---

function TryConcatTimeStampPtrToStr
  (var ADest: string; const ASource: pointer): Boolean;
begin
  ADest := '';
  Result := false;
end;

(* global procedures *)

procedure ParseTimeStampPtrToStr
  (out ADest: string; const ASource: pointer);
begin
  if (not TryParseTimeStampPtrToStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

// ---

procedure ConcatTimeStampPtrToStr
  (var ADest: string; const ASource: pointer);
begin
  if (not TryConcatTimeStampPtrToStr
    (ADest, ASource)) then
  begin
    //
  end;
end;



end.

