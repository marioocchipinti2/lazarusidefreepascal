(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstddatetimetostrconv;

{$mode objfpc}{$H+}

interface
uses
  SysUtils,
  umlcmodules, umlctypes,
  umlcstdtypes,
  umlcstddatetimetypes,
  umlccomparisons,
  umlcstdtimes,
  umlcstddates,
  umlcstddatetimes,
  umlcstdtimetostrconv,
  umlcstddatetostrconv,
  dummy;

// ---

const

 MOD_umlcstddatetimetostrconv : TUMLCModule =
   ($F7,$5D,$A1,$41,$6E,$AE,$C1,$46,$8A,$3C,$5C,$0C,$D5,$4F,$55,$72);

// ---

(* global functions *)

  function StrToDateTime
    (const AValue: string): umlcstddatetime;
  function DateTimeToStr
    (const AValue: umlcstddatetime): string;

  function FormatDateTimeToStr
    (const AFormat: string; AValue: umlcstddatetime): string;
  function FormatStrToDateTime
    (const AFormat: string; AValue: string): umlcstddatetime;

implementation

(* global functions *)


function StrToDateTime
  (const AValue: string): umlcstddatetime;
begin
  Result := SysUtils.StrToDateTime(AValue);
  // Goal: Returns a datetime value of the given string description.
  // Objetivo: Regresa un valor fechatiempo de la descripcion alfanumerica dada.
end;

function DateTimeToStr
  (const AValue: umlcstddatetime): string;
begin
  Result := SysUtils.DateTimeToStr(AValue);
  // Goal: Returns a string description of the given datetime value.
  // Objetivo: Regresa una descripcion alfanumerica del valor fechatiempo dado.
end;

function FormatDateTimeToStr
  (const AFormat: string; AValue: umlcstddatetime): string;
begin
  Result := SysUtils.FormatDateTime(AFormat, AValue);
  // Goal: Returns a string representation of the given datetime value.
  // Objetivo: Regresa una representacion alfanumerica del valor fechahora dado.
end;

function FormatStrToDateTime
  (const AFormat: string; AValue: string): umlcstddatetime;
begin
  Result := SysUtils.StrToDateTime(AValue);
  // Goal: Returns a datetime value from the given string.
  // Objetivo: Regresa un valor fechahora a partir de la cadena dada.
end;

end.

