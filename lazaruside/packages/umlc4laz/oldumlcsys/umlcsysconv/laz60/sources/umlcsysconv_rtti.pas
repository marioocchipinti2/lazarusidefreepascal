(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsysconv_rtti;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules,
  dummy;


// ---

const

  MOD_umlcsysconv_rtti : TUMLCModule =
    ($31,$D8,$45,$C7,$6F,$62,$91,$48,$BD,$A5,$77,$64,$86,$06,$B2,$1C);

// ---


implementation

end.

