(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlchexas;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for hexadecimal base representation numbers.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlcstdtypes, umlcconvtypes, umlcuinttypes,
  umlcmaskarrays,
  dummy;

// ---

const

 MOD_umlchexas : TUMLCModule =
    ($12,$22,$F4,$C7,$BB,$9E,$15,$4A,$B7,$CA,$1F,$D2,$83,$E1,$C2,$A5);

// ---

resourcestring
  resHexOverflow = 'hexadecimal digit overflow';
(*
resource
  resHexaOverflow: string = '';
*)

type
  THexArray    = array [0 .. 65535] of char;
  PHexArray    = ^THexArray;

  EHexOverflow = exception;

  function TryHexToByte
    (const ASource: umlchexastring; var ADest: Byte): Boolean;
  function TryHexToWord
    (const ASource: umlchexastring; var ADest: Word): Boolean;
  function TryHexToLongWord
    (const ASource: umlchexastring; var ADest: LongWord): Boolean;

  function HexToDec
    (const AValue: umlchexachar): umlcdecimal;

  function HexToInt
    (const AValue: umlchexastring): Integer;

  function HexToByte
    (const AValue: umlchexastring): Byte;
  function HexToWord
    (const AValue: umlchexastring): Word;
  function HexToLongWord
    (const AValue: umlchexastring): LongWord;

  function DecToHexChar
    (const AValue: umlcdecimal): umlchexachar;

  function IntToHexStr
    ({in} AValue: Integer): umlchexastring;

function ByteToHexStr
  (const AValue: Byte): umlchexastring;
function WordToHexStr
  (const AValue: Word): umlchexastring;
function LongWordToHexStr
  (const AValue: LongWord): umlchexastring;

  procedure FillZeroOpenStr
    (var ADest: string; ASize: Integer);
  procedure FillZeroAnsiStr
    (var ADest: ansistring; ASize: Integer);
  procedure FillZeroShortStr
    (var ADest: shortstring; ASize: Integer);
  procedure FillZeroCharPtr
    (var ADest: PChar; ASize: Integer);

  function IsHexDigit
    (const AValue: ansichar): Boolean;

  function IndexNonHexShortStr
    (const ADest: shortstring; ASize: Integer): Integer;
  function IndexNonHexAnsiCharPtr
    (const ADest: PChar; ASize: Integer): Integer;

  function IsHexShortStr
    (const ADest: shortstring; ASize: Integer): Boolean;
  function IsHexAnsiCharPtr
    (const ADest: PChar; ASize: Integer): Boolean;

  function IndexNonHexaStrByMask
    (const ASource: shortstring; AMask: PMaskArray; ASize: Integer): Integer;
  function IndexNonHexaAnsiCharPtrByMask
    (const ASource: PChar; AMask: PMaskArray; ASize: Integer): Integer;

  function TryHexaStrToUInt8
    (var ADest: umlcuint_8; const ASource: umlchexastring): Boolean;
  function TryHexaStrToUInt16
    (var ADest: umlcuint_16; const ASource: umlchexastring): Boolean;
  function TryHexaStrToUInt32
    (var ADest: umlcuint_32; const ASource: umlchexastring): Boolean;
  function TryHexaStrToUInt64
    (var ADest: umlcuint_64; const ASource: umlchexastring): Boolean;

  function TryHexaStrToUInt128
    (var ADest: umlcuint_128; const ASource: umlchexastring): Boolean;
  function TryHexaStrToUInt256
    (var ADest: umlcuint_256; const ASource: umlchexastring): Boolean;
  function TryHexaStrToUInt512
    (var ADest: umlcuint_512; const ASource: umlchexastring): Boolean;

  procedure UInt8ToHexaStr
    (var ADest: umlchexastring; const ASource: umlcuint_8);
  procedure UInt16ToHexaStr
    (var ADest: umlchexastring; const ASource: umlcuint_16);
  procedure UInt32ToHexaStr
    (var ADest: umlchexastring; const ASource: umlcuint_32);
  procedure UInt64ToHexaStr
    (var ADest: umlchexastring; const ASource: umlcuint_64);

  procedure UInt128ToHexaStr
    (var ADest: umlchexastring; const ASource: umlcuint_128);
  procedure UInt256ToHexaStr
    (var ADest: umlchexastring; const ASource: umlcuint_256);
  procedure UInt512ToHexaStr
    (var ADest: umlchexastring; const ASource: umlcuint_512);


implementation

const HexChars: array[0..15] of umlchexachar =
   ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    'A', 'B', 'C', 'D', 'E', 'F');

function HexToDec
  (const AValue: umlchexachar): umlcdecimal;
var I: Integer; Found: Boolean; ASource: umlchexachar;
begin
  Result := 0;
  ASource := UpCase(AValue);

  I := 0; Found := FALSE;
  while (not Found) and (I <= 15) do
  begin
    Found := (ASource = umlchexas.HexChars[i]);
    Inc(I);
  end;

  if (Found) then
  begin
    Result := Pred(I);
  end;
end;

function HexToInt
  (const AValue: umlchexastring): Integer;
var I, L, J, D: Integer; C: char;
begin
  Result := 0;
  L :=
    System.Length(AValue);
  J := 0;
  for I := L downto 1 do
  begin
    C := AValue[i];
    D := HexToDec(C);
    D := Trunc(D * IntPower(16, J));
    Result := Result + D;
    Inc(J);
  end;
end;

function TryHexToByte
  (const ASource: umlchexastring; var ADest: Byte): Boolean;
var L: Integer;
begin
  L := System.Length(ASource); ADest := 0;
  Result := (L > 0) and (L < 3);

  if (Result) then
  begin
    ADest := HexToInt(ASource);
  end;
end;

function TryHexToWord
  (const ASource: umlchexastring; var ADest: Word): Boolean;
var L: Integer;
begin
  L := System.Length(ASource); ADest := 0;
  Result := (L > 0) and (L < 5);

  if (Result) then
  begin
    ADest := HexToInt(ASource);
  end;
end;

function TryHexToLongWord
  (const ASource: umlchexastring; var ADest: LongWord): Boolean;
var L: Integer;
begin
  L := System.Length(ASource); ADest := 0;
  Result := (L > 0) and (L < 9);

  if (Result) then
  begin
    ADest := HexToInt(ASource);
  end;
end;

function HexToByte
  (const AValue: umlchexastring): Byte;
begin
  Result := 0;
  if (not TryHexToByte(AValue, Result)) then
  begin
    raise EHexOverflow.Create(resHexOverflow);
  end;
end;

function HexToWord
  (const AValue: umlchexastring): Word;
begin
  Result := 0;
  if (not TryHexToWord(AValue, Result)) then
  begin
    raise EHexOverflow.Create(resHexOverflow);
  end;
end;

function HexToLongWord
  (const AValue: umlchexastring): LongWord;
begin
  Result := 0;
  if (not TryHexToLongWord(AValue, Result)) then
  begin
    raise EHexOverflow.Create(resHexOverflow);
  end;
end;

function DecToHexChar
  (const AValue: umlcdecimal): umlchexachar;
var ASourceValue: umlcdecimal;
begin
  ASourceValue :=
    Math.Min(AValue, 15);
  Result := HexChars[ASourceValue];
end;

function IntToHexStr
  ({in} AValue: Integer): umlchexastring;
var I: Integer;
begin
  Result := '';
  repeat
    I := AValue mod 16;
    AValue := AValue div 16;
    Result := Concat(HexChars[I], Result);
  until (AValue < 1);
end;

function ByteToHexStr
  (const AValue: Byte): umlchexastring;
begin
  Result := '';
  Result := Result + umlchexas.HexChars[AValue shr 4];
  Result := Result + umlchexas.HexChars[AValue and 15];
end;

function WordToHexStr
  (const AValue: Word): umlchexastring;
begin
  Result := umlchexas.IntToHexStr(AValue);
end;

function LongWordToHexStr
  (const AValue: LongWord): umlchexastring;
begin
  Result := umlchexas.IntToHexStr(AValue);
end;

procedure FillZeroOpenStr
  (var ADest: string; ASize: Integer);
var I: Integer;
begin
  System.SetLength(ADest, ASize);
  for I := 1 to ASize do
  begin
    ADest[I] := '0';
  end;
end;

procedure FillZeroAnsiStr
  (var ADest: ansistring; ASize: Integer);
begin
  System.SetLength(ADest, ASize);
  System.FillChar(ADest[1], ASize, '0');
  // Goal: fills with zero a pascal string.
end;

procedure FillZeroShortStr
  (var ADest: shortstring; ASize: Integer);
begin
  System.FillChar(ADest, ASize, '0');
  ADest[0] := chr(ASize);
  // Goal: fills with zero a pascal string.
end;

procedure FillZeroCharPtr
  (var ADest: PChar; ASize: Integer);
begin
  System.FillChar(ADest^, ASize, '0');
  // Goal: fills with zero an array of ansichar.
end;

function IsHexDigit
  (const AValue: ansichar): Boolean;
var ThisValue: ansichar;
begin
  Result := false;
  ThisValue :=
    System.UpCase(AValue);
  Result :=
    (
      ((ThisValue >= '0') or (ThisValue <= '9')) or
      ((ThisValue >= 'A') or (ThisValue <= 'F'))
    );
end;

function IndexNonHexShortStr
  (const ADest: shortstring; ASize: Integer): Integer;
var I: Integer; CanContinue: Boolean;
begin
  Result := -1;

  if (ASize > 0) then
  begin
    CanContinue := true;
    I := 1;
    while ((I <= ASize) and CanContinue) do
    begin
      CanContinue :=
        umlchexas.IsHexDigit(ADest[I]);
      Inc(I);
    end;

    if (not CanContinue) then
    begin
      Result := (I - 1);
    end;
  end;
  // Goal: Returns the index of a non hexadecimal character,
  // in the given parameter. If all characters are umlchexachar digits,
  // then, "-1" is returned.
end;

function IndexNonHexAnsiCharPtr
  (const ADest: PChar; ASize: Integer): Integer;
var I: Integer; CanContinue: Boolean; P: PChar;
begin
  Result := -1;

  if (ASize > 0) then
  begin
    P := @ADest;
    CanContinue := true;
    I := 1;
    while ((I <= ASize) and CanContinue) do
    begin
      CanContinue :=
        umlchexas.IsHexDigit(P^);
      Inc(P);
      Inc(I);
    end;

    if (not CanContinue) then
    begin
      Result := (I - 1);
    end;
  end;
  // Goal: Returns if there is a non hexadecimal character,
  // in the given parameter.
end;

function IsHexShortStr
  (const ADest: shortstring; ASize: Integer): Boolean;
begin
  Result :=
    (IndexNonHexShortStr(ADest, ASize) = -1);
  // Goal: Returns if there is a non hexadecimal character,
  // in the given parameter.
end;

function IsHexAnsiCharPtr
  (const ADest: PChar; ASize: Integer): Boolean;
begin
  Result :=
    (IndexNonHexAnsiCharPtr(ADest, ASize) = -1);
  // Goal: Returns if there is a non hexadecimal character,
  // in the given parameter.
end;

function IndexNonHexaStrByMask
  (const ASource: shortstring; AMask: PMaskArray; ASize: Integer): Integer;
var ALen, AIndex: Integer;
    CanValidate, CanContinue: Boolean;
begin
  Result := -1;

  AIndex := 0;
  CanContinue := true;
  while ((AIndex < ASize) and CanContinue) do
  begin
    CanValidate :=
      MaskArrayGetAt(AMask, ASize, AIndex);
    if (CanValidate) then
    begin
      CanContinue :=
        umlchexas.IsHexDigit(ASource[AIndex + 1]);
    end;
    Inc(AIndex);
  end;

  if (not CanContinue) then
  begin
    Result := AIndex;
  end;
  // Goal: Receives an array of char ("ADest"),
  // and and array of booleans ("AMask"), of the same length,
  // and checks only the characters indicated by its matching
  // boolean values, are hexadecimal digits.
  // other characters, are ignored.
  //
  // Warning: if the string is empty, it returns as if not error found.
end;

function IndexNonHexaAnsiCharPtrByMask
  (const ASource: PChar; AMask: PMaskArray; ASize: Integer): Integer;
begin
  Result := -1;
  // Goal: Receives an array of char ("ADest"),
  // and and array of booleans ("AMask"), of the same length,
  // and checks only the characters indicated by its matching
  // boolean values, are hexadecimal digits.
  // other characters, are ignored.
  //
  // Warning: if the string is empty, it returns as if not error found.
end;

function TryHexaStrToUInt8
(var ADest: umlcuint_8; const ASource: umlchexastring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryHexaStrToUInt16
(var ADest: umlcuint_16; const ASource: umlchexastring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryHexaStrToUInt32
(var ADest: umlcuint_32; const ASource: umlchexastring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryHexaStrToUInt64
(var ADest: umlcuint_64; const ASource: umlchexastring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryHexaStrToUInt128
(var ADest: umlcuint_128; const ASource: umlchexastring): Boolean;
begin
  Result := false;
  //ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryHexaStrToUInt256
(var ADest: umlcuint_256; const ASource: umlchexastring): Boolean;
begin
  Result := false;
  //  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryHexaStrToUInt512
(var ADest: umlcuint_512; const ASource: umlchexastring): Boolean;
begin
  Result := false;
  //ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

procedure UInt8ToHexaStr
  (var ADest: umlchexastring; const ASource: umlcuint_8);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt16ToHexaStr
  (var ADest: umlchexastring; const ASource: umlcuint_16);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt32ToHexaStr
  (var ADest: umlchexastring; const ASource: umlcuint_32);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt64ToHexaStr
  (var ADest: umlchexastring; const ASource: umlcuint_64);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt128ToHexaStr
  (var ADest: umlchexastring; const ASource: umlcuint_128);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt256ToHexaStr
  (var ADest: umlchexastring; const ASource: umlcuint_256);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt512ToHexaStr
  (var ADest: umlchexastring; const ASource: umlcuint_512);
begin
  ADest := '';
  // @toxdo: ...
end;

end.
