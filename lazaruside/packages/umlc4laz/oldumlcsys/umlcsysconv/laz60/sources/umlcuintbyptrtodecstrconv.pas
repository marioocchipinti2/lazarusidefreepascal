unit umlcuintbyptrtodecstrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for pointers to memory based unsigned integers,
 ** using decimal base representation numbers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcdecs,
  umlcuint64byptrs,
  umlcuint128byptrs,
  umlcuint256byptrs,
  umlcuint512byptrs,
  dummy;

// ---

const

  MOD_umlcIntptrdecconv : TUMLCModule =
    ($C6,$C3,$FF,$32,$81,$DF,$48,$4F,$AA,$36,$E3,$3A,$93,$F2,$0D,$24);

// ---

(* global functions *)



(* global procedures *)

procedure IntPtrToDecStr
  (out ADest: string; const ASource: pointer);

procedure Int8ToDecStr
  (out ADest: string; const ASource: pointer);

procedure Int16ToDecStr
  (out ADest: string; const ASource: pointer);

procedure Int32ToDecStr
  (out ADest: string; const ASource: pointer);

procedure Int64ToDecStr
  (out ADest: string; const ASource: pointer);

procedure Int128ToDecStr
  (out ADest: string; const ASource: pointer);

procedure Int256ToDecStr
  (out ADest: string; const ASource: pointer);

procedure Int512ToDecStr
  (out ADest: string; const ASource: pointer);

// ---

function TryConcatInt8PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt16PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt32PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt64PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt128PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt256PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt512PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;

// ---

procedure ConcatInt8PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt16PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt32PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt64PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt128PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt256PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt512PtrToDecStr
  (var ADest: string; const ASource: pointer);



implementation

(* global functions *)



(* global procedures *)

procedure Int8ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_8;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_8(ASource);

    // ...
end;

procedure Int16ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_16;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_16(ASource);

  // ...
end;

procedure Int32ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_32;
begin
  //umlcdebug.DebugWriteLn('Int32ToDecStr(...)');
  //umlcdebug.DebugWriteEoLn();

  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_32(ASource);

  ADest :=
    SysUtils.IntToStr(ASourcePtr^);
end;

procedure Int64ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_64;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_64(ASource);

  // ...
end;

procedure Int128ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_128;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_128(ASource);

  // ...
end;

procedure Int256ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_256;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_256(ASource);

  // ...
end;

procedure Int512ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_512;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_512(ASource);

  // ...
end;

procedure IntPtrToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: PInteger;
begin
  umlcstrings.Clear(ADest);

  if (sizeof(Integer) = sizeof(umlcuint_32))
    then Int32ToDecStr(ADest, ASource)

  //if (sizeof(Integer) = sizeof(umlcuint_8))
  //  then Int8ToDecStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_16))
  //  then Int16ToDecStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_32))
  //  then Int32ToDecStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_64))
  //  then Int64ToDecStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_128))
  //  then Int128ToDecStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_256))
  //  then Int256ToDecStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_512))
  //  then Int512ToDecStr(ADest, ASource);
end;

// ---

function TryConcatInt8PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_8;
    Temp: string;
    ASrcCardinal: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    //umlcdebug.DebugWriteLn('ConcatInt8PtrToDecStr');
    //umlcdebug.DebugWriteEoLn();

    ASourcePtr := umlcpuint_8(ASource);

    ASrcCardinal := ASourcePtr^;

    Temp := '';
    umlcdecs.ConcatCardinalToDecStr
      (Temp, ASrcCardinal);

    //Temp :=
    //  IntToStr(ASourcePtr^);

    umlcstrings.ConcatStr(ADest, Temp);
  end;
end;

function TryConcatInt16PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_16;
    Temp: string;
    ASrcCardinal: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_16(ASource);

    ASrcCardinal := ASourcePtr^;

    Temp := '';
    umlcdecs.ConcatCardinalToDecStr
      (Temp, ASrcCardinal);

    //Temp :=
    //  IntToStr(ASourcePtr^);
    umlcstrings.ConcatStr(ADest, Temp);
  end;
end;

function TryConcatInt32PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_32;
    Temp: string;
    ASrcCardinal: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_32(ASource);

    ASrcCardinal := ASourcePtr^;

    Temp := '';
    umlcdecs.ConcatCardinalToDecStr
      (Temp, ASrcCardinal);

    //Temp :=
    //  IntToStr(ASourcePtr^);
    umlcstrings.ConcatStr(ADest, Temp);
  end;
end;

function TryConcatInt64PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_64;
    number: Byte;
    Count: Byte;
    i, j: Byte;
    Temp1, Temp2: string;

    EachBit: Byte;
    ByteCount: Word;
    Base: LongWord;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    //umlcdebug.DebugWriteLn('ConcatInt64PtrToDecStr : [' + IntToStr(ASrcCardinal) + ']');
    //umlcdebug.DebugWriteEoLn();

    ASourcePtr := umlcpuint_64(ASource);

    umlcstrings.Clear(Temp1);
    if (not umlcuint64byptrs.IsEmpty(ASourcePtr)) then
    begin
      //ByteCount :=
      //  sizeof(umlcpuint_64);
      //Base := 0;
      //while (ByteCount > 0) do
      //begin
      //  for EachBit := 0 to 7 do
      //  begin
      //
      //  end;
      //
      //  System.Inc(Base);
      //  System.Dec(ByteCount);
      //end;


    //  number :=
    //    ASrcCardinal;
    //  while (number > 0) do
    //  begin
    //    j := ((number mod 10) + 48);
    //
    //    //umlcdebug.DebugWriteLn('letter : [' + chr(j) + ']');
    //    //umlcdebug.DebugWriteEoLn();
    //
    //    temp1 := temp1 + chr(j);
    //
    //    //umlcdebug.DebugWriteLn('temp1 : [' + temp1 + ']');
    //    //umlcdebug.DebugWriteEoLn();
    //
    //    number := number div 10;
    //  end;
    //
    //  umlcdebug.DebugWriteLn('temp1 : [' + temp1 + ']');
    //  umlcdebug.DebugWriteEoLn();
    //
    //  umlcstrings.Clear(Temp2);
    //  Count :=
    //    System.Length(temp1);
    //  for i := 1 to Count do
    //  begin
    //    Temp2 := Temp2 + temp1[Count - i + 1];
    //
    //    umlcdebug.DebugWriteLn('Temp2 : [' + Temp2 + ']');
    //    umlcdebug.DebugWriteEoLn();
    //  end;
    //  Temp1 := Temp2;
    end else
    begin
      Temp1 := '0';
    end;

    //Temp :=
    //  IntToStr(ASourcePtr^);

    umlcstrings.ConcatStr(ADest, Temp1);
  end;
end;

function TryConcatInt128PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_128;
    Temp: string;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_128(ASource);

    //Temp :=
    //  IntToStr(ASourcePtr^);
    //umlcstrings.ConcatStr(ADest, Temp);
  end;
end;

function TryConcatInt256PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_256;
    Temp: string;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_256(ASource);

    //Temp :=
    //  IntToStr(ASourcePtr^);
    //umlcstrings.ConcatStr(ADest, Temp);
  end;
end;

function TryConcatInt512PtrToDecStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_512;
    Temp: string;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_512(ASource);

    //Temp :=
    //  IntToStr(ASourcePtr^);
    //umlcstrings.ConcatStr(ADest, Temp);
  end;
end;

// ---

procedure ConcatInt8PtrToDecStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtodecstrconv.TryConcatInt8PtrToDecStr
    (ADest, ASource)) then
  begin
    // ...
  end;
end;

procedure ConcatInt16PtrToDecStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtodecstrconv.TryConcatInt16PtrToDecStr
    (ADest, ASource)) then
  begin
    // ...
  end;
end;

procedure ConcatInt32PtrToDecStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtodecstrconv.TryConcatInt32PtrToDecStr
    (ADest, ASource)) then
  begin
    // ...
  end;
end;

procedure ConcatInt64PtrToDecStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtodecstrconv.TryConcatInt64PtrToDecStr
    (ADest, ASource)) then
  begin
    // ...
  end;
end;

procedure ConcatInt128PtrToDecStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtodecstrconv.TryConcatInt128PtrToDecStr
    (ADest, ASource)) then
  begin
    // ...
  end;
end;

procedure ConcatInt256PtrToDecStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtodecstrconv.TryConcatInt256PtrToDecStr
    (ADest, ASource)) then
  begin
    // ...
  end;
end;

procedure ConcatInt512PtrToDecStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtodecstrconv.TryConcatInt512PtrToDecStr
    (ADest, ASource)) then
  begin
    // ...
  end;
end;


end.

