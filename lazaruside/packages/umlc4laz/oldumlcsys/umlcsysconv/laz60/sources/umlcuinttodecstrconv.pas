(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuinttodecstrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for memory based unsigned integers,
 ** using decimal base representation numbers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcdecs,
  dummy;

// ---

const

  MOD_umlcInttodecstrconv : TUMLCModule =
    ($88,$6D,$2A,$F2,$40,$5F,$44,$4D,$BA,$CF,$7A,$B8,$52,$B7,$CC,$7F);

// ---

(* global functions *)

function TryParseUInt8ToDecStr
  (out ADest: string; const ASource: umlcuint_8): Boolean;

function TryParseUInt16ToDecStr
  (out ADest: string; const ASource: umlcuint_16): Boolean;

function TryParseUInt32ToDecStr
  (out ADest: string; const ASource: umlcuint_32): Boolean;

function TryParseUInt64ToDecStr
  (out ADest: string; const ASource: umlcuint_64): Boolean;

function TryParseUInt128ToDecStr
  (out ADest: string; const ASource: umlcuint_128): Boolean;

function TryParseUInt256ToDecStr
  (out ADest: string; const ASource: umlcuint_256): Boolean;

function TryParseUInt512ToDecStr
  (out ADest: string; const ASource: umlcuint_512): Boolean;

// ---

function TryConcatUInt8ToDecStr
  (var ADest: string; const ASource: umlcuint_8): Boolean;

function TryConcatUInt16ToDecStr
  (var ADest: string; const ASource: umlcuint_16): Boolean;

function TryConcatUInt32ToDecStr
  (var ADest: string; const ASource: umlcuint_32): Boolean;

function TryConcatUInt64ToDecStr
  (var ADest: string; const ASource: umlcuint_64): Boolean;

function TryConcatUInt128ToDecStr
  (var ADest: string; const ASource: umlcuint_128): Boolean;

function TryConcatUInt256ToDecStr
  (var ADest: string; const ASource: umlcuint_256): Boolean;

function TryConcatUInt512ToDecStr
  (var ADest: string; const ASource: umlcuint_512): Boolean;

(* global procedures *)

procedure ParseUInt8ToDecStr
  (out ADest: string; const ASource: umlcuint_8);

procedure ParseUInt16ToDecStr
  (out ADest: string; const ASource: umlcuint_16);

procedure ParseUInt32ToDecStr
  (out ADest: string; const ASource: umlcuint_32);

procedure ParseUInt64ToDecStr
  (out ADest: string; const ASource: umlcuint_64);

procedure ParseUInt128ToDecStr
  (out ADest: string; const ASource: umlcuint_128);

procedure ParseUInt256ToDecStr
  (out ADest: string; const ASource: umlcuint_256);

procedure ParseUInt512ToDecStr
  (out ADest: string; const ASource: umlcuint_512);

// ---

procedure ConcatUInt8ToDecStr
  (var ADest: string; const ASource: umlcuint_8);

procedure ConcatUInt16ToDecStr
  (var ADest: string; const ASource: umlcuint_16);

procedure ConcatUInt32ToDecStr
  (var ADest: string; const ASource: umlcuint_32);

procedure ConcatUInt64ToDecStr
  (var ADest: string; const ASource: umlcuint_64);

procedure ConcatUInt128ToDecStr
  (var ADest: string; const ASource: umlcuint_128);

procedure ConcatUInt256ToDecStr
  (var ADest: string; const ASource: umlcuint_256);

procedure ConcatUInt512ToDecStr
  (var ADest: string; const ASource: umlcuint_512);

implementation

(* global functions *)

function TryParseUInt8ToDecStr
  (out ADest: string; const ASource: umlcuint_8): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt16ToDecStr
  (out ADest: string; const ASource: umlcuint_16): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt32ToDecStr
  (out ADest: string; const ASource: umlcuint_32): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt64ToDecStr
  (out ADest: string; const ASource: umlcuint_64): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt128ToDecStr
  (out ADest: string; const ASource: umlcuint_128): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt256ToDecStr
  (out ADest: string; const ASource: umlcuint_256): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt512ToDecStr
  (out ADest: string; const ASource: umlcuint_512): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

// ---

function TryConcatUInt8ToDecStr
  (var ADest: string; const ASource: umlcuint_8): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt16ToDecStr
  (var ADest: string; const ASource: umlcuint_16): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt32ToDecStr
  (var ADest: string; const ASource: umlcuint_32): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt64ToDecStr
  (var ADest: string; const ASource: umlcuint_64): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt128ToDecStr
  (var ADest: string; const ASource: umlcuint_128): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt256ToDecStr
  (var ADest: string; const ASource: umlcuint_256): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt512ToDecStr
  (var ADest: string; const ASource: umlcuint_512): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

(* global procedures *)

procedure ParseUInt8ToDecStr
  (out ADest: string; const ASource: umlcuint_8);
begin
  if (not TryParseUInt8ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt16ToDecStr
  (out ADest: string; const ASource: umlcuint_16);
begin
  if (not TryParseUInt16ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt32ToDecStr
  (out ADest: string; const ASource: umlcuint_32);
begin
  if (not TryParseUInt32ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt64ToDecStr
  (out ADest: string; const ASource: umlcuint_64);
begin
  if (not TryParseUInt64ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt128ToDecStr
  (out ADest: string; const ASource: umlcuint_128);
begin
  if (not TryParseUInt128ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt256ToDecStr
  (out ADest: string; const ASource: umlcuint_256);
begin
  if (not TryParseUInt256ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt512ToDecStr
  (out ADest: string; const ASource: umlcuint_512);
begin
  if (not TryParseUInt512ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

// ---


procedure ConcatUInt8ToDecStr
  (var ADest: string; const ASource: umlcuint_8);
begin
  if (not TryConcatUInt8ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt16ToDecStr
  (var ADest: string; const ASource: umlcuint_16);
begin
  if (not TryConcatUInt16ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt32ToDecStr
  (var ADest: string; const ASource: umlcuint_32);
begin
  if (not TryConcatUInt32ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt64ToDecStr
  (var ADest: string; const ASource: umlcuint_64);
begin
  if (not TryConcatUInt64ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt128ToDecStr
  (var ADest: string; const ASource: umlcuint_128);
begin
  if (not TryConcatUInt128ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt256ToDecStr
  (var ADest: string; const ASource: umlcuint_256);
begin
  if (not TryConcatUInt256ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt512ToDecStr
  (var ADest: string; const ASource: umlcuint_512);
begin
  if (not TryConcatUInt512ToDecStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;


end.

