(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsintbyptrhexconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcsinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlchexas,
  dummy;

// ---

const

  MOD_umlcsintbyptrhexconv : TUMLCModule =
    ($B9,$20,$72,$31,$58,$F4,$52,$4A,$A4,$10,$5A,$A8,$A0,$07,$C8,$46);

// ---

(* global functions *)



(* global procedures *)

procedure UIntPtrToHexStr
  (out ADest: string; const ASource: pointer);

// ---

procedure ConcatSInt8PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt16PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt32PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt64PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt128PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt256PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt512PtrToHexStr
  (out ADest: string; const ASource: pointer);

implementation

(* global functions *)



(* global procedures *)

procedure UIntPtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_8;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_8(ASource);

    // ...
end;

procedure UInt16ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_16;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_16(ASource);

  // ...
end;

procedure UInt32ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_32;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_32(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('MemInt32ToOctStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := chr(Remainder + 48);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatChar(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure UInt64ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_64;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_64(ASource);

  // ...
end;

procedure UInt128ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_128;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_128(ASource);

  // ...
end;

procedure UInt256ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_256;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_256(ASource);

  // ...
end;

procedure UInt512ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_512;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_512(ASource);

  // ...
end;

procedure UIntToHexStr
  (out ADest: string; const ASource: pointer);
begin
  umlcstrings.Clear(ADest);

  if (sizeof(Cardinal) = sizeof(umlcsint_32))
    then UInt32ToHexStr(ADest, ASource)

  //if (sizeof(Cardinal) = sizeof(umlcsint_8))
  //  then UInt8ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_16))
  //  then UInt16ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_32))
  //  then UInt32ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_64))
  //  then UInt64ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_128))
  //  then UInt128ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_256))
  //  then UInt256ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_512))
  //  then UInt512ToHexStr(ADest, ASource);
end;


// ---

procedure ConcatSInt8PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_8;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_8(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  umlcdebug.DebugWriteLn('ConcatSInt8PtrToHexStr');
  umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

procedure ConcatSInt16PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_16;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_16(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatSInt16PtrToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

procedure ConcatSInt32PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_32;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_32(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatSInt32PtrToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

procedure ConcatSInt64PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_64;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_64(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatSInt64PtrToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

procedure ConcatSInt128PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatSInt128PtrToHexStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcsint_128);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlchexas.ByteToHexStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt256PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  umlcdebug.DebugWriteLn('ConcatSInt256PtrToHexStr');
  umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcsint_256);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlchexas.ByteToHexStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt512PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatSInt512PtrToHexStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcsint_512);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlchexas.ByteToHexStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;


end.

