unit umlcuintbyptrdecconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcdecs,
  dummy;

// ---

const

  MOD_umlcuintbyptrdecconv : TUMLCModule =
    ($E5,$1E,$87,$C6,$90,$84,$C8,$41,$B6,$79,$A5,$39,$79,$3F,$4F,$02);

// ---

(* global functions *)



(* global procedures *)

procedure UIntPtrToDecStr
  (out ADest: string; const ASource: pointer);

// ---

procedure ConcatUInt8PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt16PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt32PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt64PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt128PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt256PtrToDecStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt512PtrToDecStr
  (var ADest: string; const ASource: pointer);



implementation

(* global functions *)



(* global procedures *)

procedure UInt8ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_8;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_8(ASource);

    // ...
end;

procedure UInt16ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_16;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_16(ASource);

  // ...
end;

procedure UInt32ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_32;
begin
  //umlcdebug.DebugWriteLn('UInt32ToDecStr(...)');
  //umlcdebug.DebugWriteEoLn();

  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_32(ASource);

  ADest :=
    SysUtils.IntToStr(ASourcePtr^);
end;

procedure UInt64ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_64;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_64(ASource);

  // ...
end;

procedure UInt128ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_128;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_128(ASource);

  // ...
end;

procedure UInt256ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_256;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_256(ASource);

  // ...
end;

procedure UInt512ToDecStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_512;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_512(ASource);

  // ...
end;

procedure UIntPtrToDecStr
  (out ADest: string; const ASource: pointer);
begin
  umlcstrings.Clear(ADest);

  if (sizeof(Cardinal) = sizeof(umlcuint_32))
    then UInt32ToDecStr(ADest, ASource)

  //if (sizeof(Cardinal) = sizeof(umlcuint_8))
  //  then UInt8ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_16))
  //  then UInt16ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_32))
  //  then UInt32ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_64))
  //  then UInt64ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_128))
  //  then UInt128ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_256))
  //  then UInt256ToDecStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_512))
  //  then UInt512ToDecStr(ADest, ASource);
end;

// ---

procedure ConcatUInt8PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_8;
    Temp: string;
    ASrcCardinal: Cardinal;
begin
  ASourcePtr := umlcpuint_8(ASource);

  ASrcCardinal := ASourcePtr^;

  Temp := '';
  umlcdecs.ConcatCardinalToDecStr
    (Temp, ASrcCardinal);

  //Temp :=
  //  IntToStr(ASourcePtr^);
  umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatUInt16PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_16;
    Temp: string;
    ASrcCardinal: Cardinal;
begin
  ASourcePtr := umlcpuint_16(ASource);
  ASrcCardinal := ASourcePtr^;

  Temp := '';
  umlcdecs.ConcatCardinalToDecStr
    (Temp, ASrcCardinal);

  //Temp :=
  //  IntToStr(ASourcePtr^);
  umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatUInt32PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_32;
    Temp: string;
    ASrcCardinal: Cardinal;
begin
  ASourcePtr := umlcpuint_32(ASource);
  ASrcCardinal := ASourcePtr^;

  Temp := '';
  umlcdecs.ConcatCardinalToDecStr
    (Temp, ASrcCardinal);

  //Temp :=
  //  IntToStr(ASourcePtr^);
  umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatUInt64PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_64;
    Temp: string;
begin
  ASourcePtr := umlcpuint_64(ASource);

  Temp :=
    IntToStr(ASourcePtr^);
  umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatUInt128PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_128;
    Temp: string;
begin
  ASourcePtr := umlcpuint_128(ASource);

  //Temp :=
  //  IntToStr(ASourcePtr^);
  //umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatUInt256PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_256;
    Temp: string;
begin
  ASourcePtr := umlcpuint_256(ASource);

  //Temp :=
  //  IntToStr(ASourcePtr^);
  //umlcstrings.ConcatStr(ADest, Temp);
end;

procedure ConcatUInt512PtrToDecStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_512;
    Temp: string;
begin
  ASourcePtr := umlcpuint_512(ASource);

  //Temp :=
  //  IntToStr(ASourcePtr^);
  //umlcstrings.ConcatStr(ADest, Temp);
end;



end.

