unit umlcuintbyptrtobinstrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for pointers to memory based unsigned integers,
 ** using binary base representation numbers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcbins,
  dummy;

// ---

const

  MOD_umlcmemoctbyptrbinconv : TUMLCModule =
    ($07,$A7,$E0,$6E,$46,$C5,$6E,$40,$86,$38,$C9,$2C,$C6,$F3,$B2,$AF);

// ---

(* global functions *)



(* global procedures *)

procedure IntPtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure Int8PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure Int16PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure Int32PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure Int64PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure Int128PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure Int256PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure Int512PtrToBinStr
  (out ADest: string; const ASource: pointer);

// ---

function TryConcatUInt8PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt16PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt32PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt64PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt128PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt256PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt512PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;

// ---

procedure ConcatUInt8PtrToBinStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt16PtrToBinStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt32PtrToBinStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt64PtrToBinStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt128PtrToBinStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt256PtrToBinStr
  (var ADest: string; const ASource: pointer);

procedure ConcatUInt512PtrToBinStr
  (var ADest: string; const ASource: pointer);


implementation

(* global functions *)



(* global procedures *)

procedure Int8PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := pbyte(ASource);

    // ...
end;

procedure Int16PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pword;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := pword(ASource);

  // ...
end;

procedure Int32PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: plongword;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := plongword(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('Int32ToOctStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 2;
      Remainder := Number mod 2;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := chr(Remainder + 48);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatChar(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure Int64PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    ByteCount: Word; EachBit: Byte;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := pbyte(ASource);

  ByteCount :=
    sizeof(pqword);
  while (ByteCount > 0) do
  begin
    for EachBit := 0 to 7 do
    begin
      if ((EachBit and ASourcePtr^) <> 0) then
      begin
        umlcstrings.ConcatChar(ADest, '0');
      end else
      begin
        umlcstrings.ConcatChar(ADest, '1');
      end;
    end;

    System.Dec(ByteCount);
  end;
end;

procedure Int128PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    ByteCount: Word; EachBit: Byte;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := pbyte(ASource);

  ByteCount :=
    sizeof(umlcuint_128);
  while (ByteCount > 0) do
  begin
    for EachBit := 0 to 7 do
    begin
      if ((EachBit and ASourcePtr^) <> 0) then
      begin
        umlcstrings.ConcatChar(ADest, '0');
      end else
      begin
        umlcstrings.ConcatChar(ADest, '1');
      end;
    end;

    System.Dec(ByteCount);
  end;
end;

procedure Int256PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    ByteCount: Word; EachBit: Byte;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := pbyte(ASource);

  ByteCount :=
    sizeof(umlcuint_256);
  while (ByteCount > 0) do
  begin
    for EachBit := 0 to 7 do
    begin
      if ((EachBit and ASourcePtr^) <> 0) then
      begin
        umlcstrings.ConcatChar(ADest, '0');
      end else
      begin
        umlcstrings.ConcatChar(ADest, '1');
      end;
    end;

    System.Dec(ByteCount);
  end;
end;

procedure Int512PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    ByteCount: Word; EachBit: Byte;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := pbyte(ASource);

  ByteCount :=
    sizeof(umlcuint_512);
  while (ByteCount > 0) do
  begin
    for EachBit := 0 to 7 do
    begin
      if ((EachBit and ASourcePtr^) <> 0) then
      begin
        umlcstrings.ConcatChar(ADest, '0');
      end else
      begin
        umlcstrings.ConcatChar(ADest, '1');
      end;
    end;

    System.Dec(ByteCount);
  end;
end;

procedure IntPtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: PInteger;
begin
  umlcstrings.Clear(ADest);

  if (sizeof(Integer) = sizeof(umlcuint_32))
    then Int32PtrToBinStr(ADest, ASource)

  //if (sizeof(Integer) = sizeof(umlcuint_8))
  //  then Int8ToBinStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_16))
  //  then Int16ToBinStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_32))
  //  then Int32ToBinStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(pqword))
  //  then Int64ToBinStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_128))
  //  then Int128ToBinStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_256))
  //  then Int256ToBinStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_512))
  //  then Int512ToBinStr(ADest, ASource);
end;

// ---

function TryConcatUInt8PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: pbyte;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := pbyte(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('ConcatUInt8PtrToBinStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 2;
        Remainder := Number mod 2;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlcbins.DecToBinChar(Remainder);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt16PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: pword;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := pword(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('ConcatUInt16PtrToBinStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 2;
        Remainder := Number mod 2;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlcbins.DecToBinChar(Remainder);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt32PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: plongword;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := plongword(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('ConcatUInt32PtrToBinStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 2;
        Remainder := Number mod 2;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlcbins.DecToBinChar(Remainder);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt64PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_64;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_64(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('ConcatUInt64PtrToBinStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 2;
        Remainder := Number mod 2;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlcbins.DecToBinChar(Remainder);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt128PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := pbyte(ASource);

    //umlcdebug.DebugWriteLn('ConcatUInt128PtrToBinStr');
    //umlcdebug.DebugWriteEoLn();

    Count :=
      sizeof(umlcuint_128);

    //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
    //umlcdebug.DebugWriteEoLn();

    I := 0;
    for I := 0 to System.Pred(Count) do
    begin
      B :=
        umlcbins.ByteToBinStr(ASourcePtr^);
      umlcstrings.ConcatStrBack(ADest, B);

      //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(ASourcePtr);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt256PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := pbyte(ASource);

    //umlcdebug.DebugWriteLn('ConcatUInt256PtrToBinStr');
    //umlcdebug.DebugWriteEoLn();

    Count :=
      sizeof(umlcuint_256);

    //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
    //umlcdebug.DebugWriteEoLn();

    I := 0;
    for I := 0 to System.Pred(Count) do
    begin
      B :=
        umlcbins.ByteToBinStr(ASourcePtr^);
      umlcstrings.ConcatStrBack(ADest, B);

      //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(ASourcePtr);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt512PtrToBinStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := pbyte(ASource);

    //umlcdebug.DebugWriteLn('ConcatUInt512PtrToBinStr');
    //umlcdebug.DebugWriteEoLn();

    Count :=
      sizeof(umlcuint_512);

    //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
    //umlcdebug.DebugWriteEoLn();

    I := 0;
    for I := 0 to System.Pred(Count) do
    begin
      B :=
        umlcbins.ByteToBinStr(ASourcePtr^);
      umlcstrings.ConcatStrBack(ADest, B);

      //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(ASourcePtr);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

// ---

procedure ConcatUInt8PtrToBinStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtobinstrconv.TryConcatUInt8PtrToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt16PtrToBinStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtobinstrconv.TryConcatUInt16PtrToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt32PtrToBinStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtobinstrconv.TryConcatUInt32PtrToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt64PtrToBinStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtobinstrconv.TryConcatUInt64PtrToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt128PtrToBinStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtobinstrconv.TryConcatUInt128PtrToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt256PtrToBinStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtobinstrconv.TryConcatUInt128PtrToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt512PtrToBinStr
  (var ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtobinstrconv.TryConcatUInt512PtrToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;


end.

