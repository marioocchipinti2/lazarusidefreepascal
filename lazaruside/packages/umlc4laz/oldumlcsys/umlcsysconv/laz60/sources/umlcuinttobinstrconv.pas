(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuinttobinstrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for memory based unsigned integers,
 ** using binary base representation numbers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcbins,
  dummy;

// ---

const

  MOD_umlcInttobinstrconv: TUMLCModule =
    ($5C,$05,$98,$CC,$26,$E5,$E9,$4B,$BA,$E1,$6D,$F0,$75,$04,$97,$4F);

// ---

(* global functions *)

function TryParseUInt8ToBinStr
  (out ADest: string; const ASource: umlcuint_8): Boolean;

function TryParseUInt16ToBinStr
  (out ADest: string; const ASource: umlcuint_16): Boolean;

function TryParseUInt32ToBinStr
  (out ADest: string; const ASource: umlcuint_32): Boolean;

function TryParseUInt64ToBinStr
  (out ADest: string; const ASource: umlcuint_64): Boolean;

function TryParseUInt128ToBinStr
  (out ADest: string; const ASource: umlcuint_128): Boolean;

function TryParseUInt256ToBinStr
  (out ADest: string; const ASource: umlcuint_256): Boolean;

function TryParseUInt512ToBinStr
  (out ADest: string; const ASource: umlcuint_512): Boolean;

// ---

function TryConcatUInt8ToBinStr
  (var ADest: string; const ASource: umlcuint_8): Boolean;

function TryConcatUInt16ToBinStr
  (var ADest: string; const ASource: umlcuint_16): Boolean;

function TryConcatUInt32ToBinStr
  (var ADest: string; const ASource: umlcuint_32): Boolean;

function TryConcatUInt64ToBinStr
  (var ADest: string; const ASource: umlcuint_64): Boolean;

function TryConcatUInt128ToBinStr
  (var ADest: string; const ASource: umlcuint_128): Boolean;

function TryConcatUInt256ToBinStr
  (var ADest: string; const ASource: umlcuint_256): Boolean;

function TryConcatUInt512ToBinStr
  (var ADest: string; const ASource: umlcuint_512): Boolean;

(* global procedures *)

procedure ParseUInt8ToBinStr
  (out ADest: string; const ASource: umlcuint_8);

procedure ParseUInt16ToBinStr
  (out ADest: string; const ASource: umlcuint_16);

procedure ParseUInt32ToBinStr
  (out ADest: string; const ASource: umlcuint_32);

procedure ParseUInt64ToBinStr
  (out ADest: string; const ASource: umlcuint_64);

procedure ParseUInt128ToBinStr
  (out ADest: string; const ASource: umlcuint_128);

procedure ParseUInt256ToBinStr
  (out ADest: string; const ASource: umlcuint_256);

procedure ParseUInt512ToBinStr
  (out ADest: string; const ASource: umlcuint_512);

// ---

procedure ConcatUInt8ToBinStr
  (var ADest: string; const ASource: umlcuint_8);

procedure ConcatUInt16ToBinStr
  (var ADest: string; const ASource: umlcuint_16);

procedure ConcatUInt32ToBinStr
  (var ADest: string; const ASource: umlcuint_32);

procedure ConcatUInt64ToBinStr
  (var ADest: string; const ASource: umlcuint_64);

procedure ConcatUInt128ToBinStr
  (var ADest: string; const ASource: umlcuint_128);

procedure ConcatUInt256ToBinStr
  (var ADest: string; const ASource: umlcuint_256);

procedure ConcatUInt512ToBinStr
  (var ADest: string; const ASource: umlcuint_512);

implementation

(* global functions *)

function TryParseUInt8ToBinStr
  (out ADest: string; const ASource: umlcuint_8): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt16ToBinStr
  (out ADest: string; const ASource: umlcuint_16): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt32ToBinStr
  (out ADest: string; const ASource: umlcuint_32): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt64ToBinStr
  (out ADest: string; const ASource: umlcuint_64): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt128ToBinStr
  (out ADest: string; const ASource: umlcuint_128): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt256ToBinStr
  (out ADest: string; const ASource: umlcuint_256): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt512ToBinStr
  (out ADest: string; const ASource: umlcuint_512): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

// ---

function TryConcatUInt8ToBinStr
  (var ADest: string; const ASource: umlcuint_8): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt16ToBinStr
  (var ADest: string; const ASource: umlcuint_16): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt32ToBinStr
  (var ADest: string; const ASource: umlcuint_32): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt64ToBinStr
  (var ADest: string; const ASource: umlcuint_64): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt128ToBinStr
  (var ADest: string; const ASource: umlcuint_128): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt256ToBinStr
  (var ADest: string; const ASource: umlcuint_256): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt512ToBinStr
  (var ADest: string; const ASource: umlcuint_512): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

(* global procedures *)

procedure ParseUInt8ToBinStr
  (out ADest: string; const ASource: umlcuint_8);
begin
  if (not TryParseUInt8ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt16ToBinStr
  (out ADest: string; const ASource: umlcuint_16);
begin
  if (not TryParseUInt16ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt32ToBinStr
  (out ADest: string; const ASource: umlcuint_32);
begin
  if (not TryParseUInt32ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt64ToBinStr
  (out ADest: string; const ASource: umlcuint_64);
begin
  if (not TryParseUInt64ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt128ToBinStr
  (out ADest: string; const ASource: umlcuint_128);
begin
  if (not TryParseUInt128ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt256ToBinStr
  (out ADest: string; const ASource: umlcuint_256);
begin
  if (not TryParseUInt256ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt512ToBinStr
  (out ADest: string; const ASource: umlcuint_512);
begin
  if (not TryParseUInt512ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

// ---


procedure ConcatUInt8ToBinStr
  (var ADest: string; const ASource: umlcuint_8);
begin
  if (not TryConcatUInt8ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt16ToBinStr
  (var ADest: string; const ASource: umlcuint_16);
begin
  if (not TryConcatUInt16ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt32ToBinStr
  (var ADest: string; const ASource: umlcuint_32);
begin
  if (not TryConcatUInt32ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt64ToBinStr
  (var ADest: string; const ASource: umlcuint_64);
begin
  if (not TryConcatUInt64ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt128ToBinStr
  (var ADest: string; const ASource: umlcuint_128);
begin
  if (not TryConcatUInt128ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt256ToBinStr
  (var ADest: string; const ASource: umlcuint_256);
begin
  if (not TryConcatUInt256ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt512ToBinStr
  (var ADest: string; const ASource: umlcuint_512);
begin
  if (not TryConcatUInt512ToBinStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;


end.

