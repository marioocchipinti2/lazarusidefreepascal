unit umlcuintbyptroctconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcoctals,
  dummy;

// ---

const

  MOD_umlcuintbyptroctconv : TUMLCModule =
    ($BA,$75,$70,$7D,$AB,$EA,$D0,$48,$A8,$5C,$F5,$71,$AF,$DA,$28,$96);

// ---

(* global functions *)



(* global procedures *)

procedure UIntPtrToOctStr
  (out ADest: string; const ASource: pointer);

// ---

procedure ConcatUInt8PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt16PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt32PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt64PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt128PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt256PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt512PtrToOctStr
  (out ADest: string; const ASource: pointer);

implementation

(* global functions *)



(* global procedures *)

procedure UInt8ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_8;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_8(ASource);

    // ...
end;

procedure UInt16ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_16;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_16(ASource);

  // ...
end;

procedure UInt32ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_32;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_32(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('UInt32ToOctStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 8;
      Remainder := Number mod 8;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := chr(Remainder + 48);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatChar(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure UInt64ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_64;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_64(ASource);

  // ...
end;

procedure UInt128ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_128;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_128(ASource);

  // ...
end;

procedure UInt256ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_256;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_256(ASource);

  // ...
end;

procedure UInt512ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_512;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_512(ASource);

  // ...
end;

procedure UIntPtrToOctStr
  (out ADest: string; const ASource: pointer);
begin
  umlcstrings.Clear(ADest);

  if (sizeof(Cardinal) = sizeof(umlcuint_32))
    then UInt32ToOctStr(ADest, ASource)

  //if (sizeof(Cardinal) = sizeof(umlcuint_8))
  //  then UInt8ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_16))
  //  then UInt16ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_32))
  //  then UInt32ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_64))
  //  then UInt64ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_128))
  //  then UInt128ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_256))
  //  then UInt256ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_512))
  //  then UInt512ToOctStr(ADest, ASource);
end;

// ---

procedure ConcatUInt8PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_8;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpuint_8(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;
//
//  umlcdebug.DebugWriteLn('ConcatUInt8PtrToOctStr');
//  umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
//  umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 8;
      Remainder := Number mod 8;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcoctals.DecToOctalChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatUInt16PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_16;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpuint_16(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatUInt16PtrToOctStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 8;
      Remainder := Number mod 8;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcOctals.DecToOctalChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  umlcdebug.DebugWriteEoLn();
end;

procedure ConcatUInt32PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_32;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpuint_32(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  umlcdebug.DebugWriteLn('ConcatUInt32PtrToOctStr');
  umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 8;
      Remainder := Number mod 8;
      Number    := Division;

      umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcOctals.DecToOctalChar(Remainder);

      umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  umlcdebug.DebugWriteEoLn();
end;

procedure ConcatUInt64PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_64;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpuint_64(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatUInt64PtrToOctStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 8;
      Remainder := Number mod 8;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcOctals.DecToOctalChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatUInt128PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatUInt128PtrToOctStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcuint_128);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlcoctals.ByteToOctalStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatUInt256PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatUInt256PtrToOctStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcuint_256);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlcoctals.ByteToOctalStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatUInt512PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatUInt512PtrToOctStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcuint_512);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlcoctals.ByteToOctalStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;


end.

