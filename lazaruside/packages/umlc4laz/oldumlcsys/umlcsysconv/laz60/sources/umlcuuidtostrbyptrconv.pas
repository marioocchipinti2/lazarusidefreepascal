unit umlcuuidtostrbyptrconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  //umlcuinttypes,
  //umlcmaskarrays,
  umlcstrings,
  umlcuuids,
  umlcuuidtostrconv,
  dummy;

// ---

const

  MOD_umlcuuidbyptrconv : TUMLCModule =
    ($7A,$8A,$0C,$1A,$97,$D7,$0D,$4C,$85,$32,$24,$AD,$C4,$1B,$13,$8E);

// ---

(* global functions *)



(* global procedures *)

procedure UUIDPtrToSingleStr
  (out ADest: string; const ASource: pointer);

procedure UUIDPtrToDoubleStr
  (out ADest: string; const ASource: pointer);

procedure UUIDPtrToLongStr
  (out ADest: string; const ASource: pointer);

procedure UUIDPtrToPascalArrayStr
  (out ADest: string; const ASource: pointer);

procedure UUIDPtrToPlainCArrayStr
  (out ADest: string; const ASource: pointer);



implementation

(* global functions *)



(* global procedures *)

procedure UUIDPtrToSingleStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: puuid; Temp: TSingleUUIDStr;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := puuid(ASource);

  umlcuuidtostrconv.UUIDToSingleStr
    (ASourcePtr^, Temp);

  ADest := Temp;
end;

procedure UUIDPtrToDoubleStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: puuid; Temp: TDoubleUUIDStr;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := puuid(ASource);

  umlcuuidtostrconv.UUIDToDoubleStr
    (ASourcePtr^, Temp);

  ADest := Temp;
end;

procedure UUIDPtrToLongStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: puuid; Temp: TLongUUIDStr;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := puuid(ASource);

  umlcuuidtostrconv.UUIDToLongStr
    (ASourcePtr^, Temp);

  ADest := Temp;
end;

procedure UUIDPtrToPascalArrayStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: puuid; Temp: ansistring;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := puuid(ASource);

  umlcuuidtostrconv.UUIDToPascalByteArray
    (ASourcePtr^, Temp);

  ADest := Temp;
end;

procedure UUIDPtrToPlainCArrayStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: puuid; Temp: ansistring;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := puuid(ASource);

  umlcuuidtostrconv.UUIDToPlainCByteArray
    (ASourcePtr^, Temp);

  ADest := Temp;
end;



end.

