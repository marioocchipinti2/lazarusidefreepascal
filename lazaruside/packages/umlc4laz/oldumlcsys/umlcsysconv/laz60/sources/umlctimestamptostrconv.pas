(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctimestamptostrconv;

{$mode objfpc}{$H+}

interface
uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysUtils,
  umlcmodules, umlctypes,
  umlcstddatetimetypes,
  umlcdates, umlctimes,
  umlctimestamps,
  dummy;

// ---

const

  MOD_umlctimestamptostrconv : TUMLCModule =
    ($BA,$F6,$9C,$F2,$64,$84,$7D,$49,$85,$63,$AF,$97,$A6,$EA,$BA,$9E);

// ---

(* global functions *)

  function StrToTimeStamp
    (const AValue: string): TTimeStamp;
  function TimeStampToStr
    (const AValue: TTimeStamp): string;


implementation

(* global functions *)

function StrToTimeStamp(const AValue: string): TTimeStamp;
begin
  Result := NoTimeStamp;
  // Goal: Returns if 2 timestamp values are equal.
  // Objetivo: Regresa si 2 valores tiempoestampa son iguales.
end;

function TimeStampToStr(const AValue: TTimeStamp): string;
begin
  Result := '';
  // Goal: Returns if 2 timestamp values are equal.
  // Objetivo: Regresa si 2 valores tiempoestampa son iguales.
end;


end.

