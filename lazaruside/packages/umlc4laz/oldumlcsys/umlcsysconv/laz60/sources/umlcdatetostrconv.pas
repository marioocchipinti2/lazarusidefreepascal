(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdatetostrconv;

{$mode objfpc}{$H+}

interface
uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcdates,
  dummy;

// ---

const

MOD_umlcdatetostrconv : TUMLCModule =
  ($99,$4A,$0F,$94,$48,$78,$AC,$4E,$AA,$6A,$99,$A3,$AD,$81,$7E,$18);

// ---

(* global functions *)

  function FormatDateToStr
    (const Format: string; Value: TDate): string;
  function FormatStrToDate
    (const Format: string; Value: string): TDate;

  function StrToDateDef
    (const S: string; Value: TDate): TDate;
  function StrToDate
    (const Value: string): TDate;
  function DateToStr
    (const Value: TDate): string;

implementation

(* global functions *)

function FormatDateToStr
  (const Format: string; Value: TDate): string;
begin
  Result := SysUtils.FormatDateTime(Format, DateToDateTime(Value));
  // Goal: Returns a string representation of the given date value.
  // Objetivo: Regresa una representacion alfanumerica del valor fecha dado.
end;

function FormatStrToDate
  (const Format: string; Value: string): TDate;
begin
  Result :=
    umlcdatetostrconv.StrToDate(Value);
  // Goal: Returns a date value from the given string.
  // Objetivo: Regresa un valor fecha a partir de la cadena dada.
end;

function StrToDateDef
  (const S: string; Value: TDate): TDate;
begin
  try
    Result := StrToDate(S);
  except
    Result := Value;
  end;
  // Goal: Returns a date value from a string.
  // Objetivo: Regresa un valor fecha de una cadena.
end;

function StrToDate
  (const Value: string): TDate;
begin
  Result :=
    umlcdates.DateTimeToDate(SysUtils.StrToDate(Value));
  // Goal: Returns a date value from a string.
  // Objetivo: Regresa un valor fecha a partir de una cadena.
end;

function DateToStr
  (const Value: TDate): string;
begin
  Result :=
    SysUtils.DateToStr(umlcdates.DateToDateTime(Value));
  // Goal: Returns a string from a time value.
  // Objetivo: Regresa una cadena a partir de un valor tiempo.
end;


end.

