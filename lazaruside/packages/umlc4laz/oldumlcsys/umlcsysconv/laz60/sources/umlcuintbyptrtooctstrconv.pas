(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuintbyptrtooctstrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for pointers to memory based unsigned integers,
 ** using octal base representation numbers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcoctals,
  dummy;

// ---

const

  MOD_umlcmemoctbyptroctconv : TUMLCModule =
    ($FB,$04,$C6,$E0,$29,$6B,$E6,$4A,$92,$47,$03,$BC,$AC,$7A,$F4,$52);

// ---

(* global functions *)



(* global procedures *)

procedure IntPtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure Int8PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure Int16PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure Int64PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure Int256PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_256;

procedure Int512PtrToOctStr
  (out ADest: string; const ASource: pointer);

// ---

function TryConcatUInt8PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt16PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt32PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt64PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt128PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt256PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryConcatUInt512PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;

// ---

procedure ConcatUInt8PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt16PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt32PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt64PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt128PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt256PtrToOctStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt512PtrToOctStr
  (out ADest: string; const ASource: pointer);


implementation

(* global functions *)



(* global procedures *)

procedure Int8PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_8;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_8(ASource);

    // ...
end;

procedure Int16PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_16;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_16(ASource);

  // ...
end;

procedure Int32PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_32;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_32(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('Int32ToOctStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 8;
      Remainder := Number mod 8;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := chr(Remainder + 48);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatChar(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure Int64PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_64;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_64(ASource);

  // ...
end;

procedure Int128PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_128;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_128(ASource);

  // ...
end;

procedure Int256PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_256;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_256(ASource);

  // ...
end;

procedure Int512PtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_512;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_512(ASource);

  // ...
end;

procedure IntPtrToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: PInteger;
begin
  umlcstrings.Clear(ADest);

  if (sizeof(Integer) = sizeof(umlcuint_32))
    then Int32PtrToOctStr(ADest, ASource)

  //if (sizeof(Integer) = sizeof(umlcuint_8))
  //  then Int8ToOctStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_16))
  //  then Int16ToOctStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_32))
  //  then Int32ToOctStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_64))
  //  then Int64ToOctStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_128))
  //  then Int128ToOctStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_256))
  //  then Int256ToOctStr(ADest, ASource)
  //else if (sizeof(Integer) = sizeof(umlcuint_512))
  //  then Int512ToOctStr(ADest, ASource);
end;

// ---

function TryConcatUInt8PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_8;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_8(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;
  //
  //  umlcdebug.DebugWriteLn('ConcatUInt8PtrToOctStr');
  //  umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //  umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 8;
        Remainder := Number mod 8;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlcoctals.DecToOctalChar(Remainder);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt16PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_16;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_16(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('ConcatUInt16PtrToOctStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 8;
        Remainder := Number mod 8;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlcOctals.DecToOctalChar(Remainder);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt32PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_32;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_32(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    umlcdebug.DebugWriteLn('ConcatUInt32PtrToOctStr');
    umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 8;
        Remainder := Number mod 8;
        Number    := Division;

        umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlcOctals.DecToOctalChar(Remainder);

        umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt64PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_64;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_64(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    umlcdebug.DebugWriteLn('ConcatUInt64PtrToOctStr');
    umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 8;
        Remainder := Number mod 8;
        Number    := Division;

        umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlcOctals.DecToOctalChar(Remainder);

        umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt128PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := pbyte(ASource);

    //umlcdebug.DebugWriteLn('ConcatUInt128PtrToOctStr');
    //umlcdebug.DebugWriteEoLn();

    Count :=
      sizeof(umlcuint_128);

    //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
    //umlcdebug.DebugWriteEoLn();

    I := 0;
    for I := 0 to System.Pred(Count) do
    begin
      B :=
        umlcoctals.ByteToOctalStr(ASourcePtr^);
      umlcstrings.ConcatStrBack(ADest, B);

      //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(ASourcePtr);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt256PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := pbyte(ASource);

    //umlcdebug.DebugWriteLn('ConcatUInt256PtrToOctStr');
    //umlcdebug.DebugWriteEoLn();

    Count :=
      sizeof(umlcuint_256);

    //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
    //umlcdebug.DebugWriteEoLn();

    I := 0;
    for I := 0 to System.Pred(Count) do
    begin
      B :=
        umlcoctals.ByteToOctalStr(ASourcePtr^);
      umlcstrings.ConcatStrBack(ADest, B);

      //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(ASourcePtr);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatUInt512PtrToOctStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := pbyte(ASource);

    //umlcdebug.DebugWriteLn('ConcatUInt512PtrToOctStr');
    //umlcdebug.DebugWriteEoLn();

    Count :=
      sizeof(umlcuint_512);

    //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
    //umlcdebug.DebugWriteEoLn();

    I := 0;
    for I := 0 to System.Pred(Count) do
    begin
      B :=
        umlcoctals.ByteToOctalStr(ASourcePtr^);
      umlcstrings.ConcatStrBack(ADest, B);

      //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(ASourcePtr);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

// ---

procedure ConcatUInt8PtrToOctStr
  (out ADest: string; const ASource: pointer);
begin
  if (not TryConcatUInt8PtrToOctStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt16PtrToOctStr
  (out ADest: string; const ASource: pointer);
begin
  if (not TryConcatUInt16PtrToOctStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt32PtrToOctStr
  (out ADest: string; const ASource: pointer);
begin
  if (not TryConcatUInt32PtrToOctStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt64PtrToOctStr
  (out ADest: string; const ASource: pointer);
begin
  if (not TryConcatUInt64PtrToOctStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt128PtrToOctStr
  (out ADest: string; const ASource: pointer);
begin
  if (not TryConcatUInt128PtrToOctStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt256PtrToOctStr
  (out ADest: string; const ASource: pointer);
begin
  if (not TryConcatUInt256PtrToOctStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt512PtrToOctStr
  (out ADest: string; const ASource: pointer);
begin
  if (not TryConcatUInt512PtrToOctStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;



end.

