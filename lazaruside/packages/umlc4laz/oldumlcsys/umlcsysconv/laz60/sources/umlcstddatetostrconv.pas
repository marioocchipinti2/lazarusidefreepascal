(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstddatetostrconv;

{$mode objfpc}{$H+}

interface
uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  Math,
  SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdtypes,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcstdnullstrtypes,
  umlcstddatetimetypes,
  umlcdates,
  umlcansicharsets,
  umlcansinullstrs, //umlcansinullparsers,
  umlcstrings,
  dummy;

// ---

const

  MOD_umlcstddatetostrconv : TUMLCModule =
    ($E2,$E6,$BA,$4B,$E2,$19,$10,$4D,$93,$2D,$4A,$63,$A9,$CB,$86,$F2);

// ---

(* global constants *)

const DefaultDateFormatSingleton: string = 'MM/DD/YYYY';

(* global properties *)

function getCurrentDateFormat(): string;

procedure setCurrentDateFormat
  (const AValue: string);

(* global functions *)



(* global variables *)

var
  DayFormat, MonthFormat, YearFormat, EraFormat: ansicharset;

implementation

(* global variables *)

var FCurrentDateFormat: string;

(* global properties *)

function getCurrentDateFormat(): string;
begin
  Result := FCurrentDateFormat;
end;

procedure setCurrentDateFormat
  (const AValue: string);
begin
  FCurrentDateFormat := AValue;
end;

(* global functions *)

procedure UnitConstructor;
begin
  YearFormat := 'yY';
  // YearFormat := ['y', 'Y'];

  MonthFormat := 'mM';
  // MonthFormat  := ['m', 'M'];

  DayFormat := 'dD';
  // DayFormat  := ['d', 'D'];

  EraFormat := 'eE';
  // EraFormat := ['e', 'E'];

  FCurrentDateFormat :=
    DefaultDateFormatSingleton;
end;

procedure UnitDestructor;
begin
  YearFormat  := '';
  MonthFormat := '';
  DayFormat   := '';
  EraFormat   := '';
end;

initialization
  UnitConstructor;
finalization
  UnitDestructor;
end.

