(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlclangstextconv;

{$mode objfpc}{$H+}

interface
uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcreslangs,
  umlclangs,
  dummy;

// ---

const

 MOD_umlclangstextconv : TUMLCModule =
   ($04,$2B,$55,$A0,$55,$CD,$27,$44,$9F,$4B,$51,$53,$FD,$F2,$72,$AF);

// ---

  function LangToStr
    (const AValue: TUMLCLanguage): string;
  function LangToText
    (const AValue: TUMLCLanguage): string;

  function StrToLang
    (const AValue: string): TUMLCLanguage;
  function TextToLang
    (const AValue: string): TUMLCLanguage;

implementation

type
  TLanguageNames = array[TUMLCLanguage] of string;

const
  LangToStrArray: TLanguageNames =
  (
    strlgNeutral,
//    strlgNone,

    strlgAfar,
    strlgAbkhazian,
    strlgAvestan,
    strlgAfrikaans,
    strlgAkan,
    strlgAmharic,
    strlgAragonese,
    strlgArabic,
    strlgAssamese,
    strlgAvaric,
    strlgAymara,
    strlgAzerbaijani,
    strlgBashkir,
    strlgBelarusian,
    strlgBulgarian,
    strlgBihari,
    strlgBislama,
    strlgBambara,
    strlgBengali,
    strlgTibetan,
    strlgBreton,
    strlgBosnian,
    strlgCatalan,
    strlgChechen ,
    strlgChamorro,
    strlgCorsican,
    strlgCree,
    strlgCzech,
    strlgChuvash,
    strlgWelsh,
    strlgDanish,
    strlgGerman,
    strlgDivehi,
    strlgDzongkha,
    strlgEwe,
    strlgGreek,
    strlgEnglish,
    strlgEnglishAmerican,
    strlgEnglishBritish,
    strlgEnglishAustralian,
    strlgEsperanto,
    strlgSpanish,
    strlgEstonian,
    strlgBasque,
    strlgPersian,
    strlgFulah,
    strlgFinnish,
    strlgFijian,
    strlgFaroese,
    strlgFrench,
    strlgWalloonBelgique,
    strlgFrisian,
    strlgIrish,
    strlgGaelic,
    strlgGallegan,
    strlgGuarani,
    strlgGujarati,
    strlgManx,
    strlgHausa,
    strlgHebrew,
    strlgHindi,
    strlgHiriMotu,
    strlgCroatian,
    strlgHaitian,
    strlgHungarian,
    strlgArmenian,
    strlgHerero,
    strlgInterlingua,
    strlgIndonesian,
    strlgInterlingue,
    strlgIgbo,
    strlgSichuanYi,
    strlgInupiaq,
    strlgIdo,
    strlgIcelandic,
    strlgItalian,
    strlgInumlcitut,
    strlgJapanese,
    strlgJavanese,
    strlgGeorgian,
    strlgKongo,
    strlgKikuyu,
    strlgKuanyama,
    strlgKazakh,
    strlgGreenlandic,
    strlgKhmer,
    strlgKannada,
    strlgKorean,
    strlgKanuri,
    strlgKashmiri,
    strlgKurdish,
    strlgCornish,
    strlgKomi,
    strlgKirghiz,
    strlgLatin,
    strlgLuxembourgish,
    strlgGanda,
    strlgLimburgan,
    strlgLingala,
    strlgLao,
    strlgLithuanian,
    strlgLubaKatanga,
    strlgLatvian,
    strlgMalagasy,
    strlgMarshallese,
    strlgMaori,
    strlgMacedonian,
    strlgMalayalam,
    strlgMongolian,
    strlgMoldavian,
    strlgMarathi,
    strlgMalay,
    strlgMaltese,
    strlgBurmese,
    strlgNauru,
    strlgNorwegianBokmaal,
    strlgNdebeleNorth,
    strlgNepali,
    strlgNdonga,
    strlgDutch,
    strlgFlemish,
    strlgNorwegianNynorsk,
    strlgNorwegian,
    strlgNdebeleSouth,
    strlgNavajo,
    strlgChichewa,
    strlgOccitan,
    strlgOjibwa,
    strlgOromo,
    strlgOriya,
    strlgOssetian,
    strlgPanjabi,
    strlgPali,
    strlgPolish ,
    strlgPushto,
    strlgPortuguese,
    strlgQuechua,
    strlgRaetoRomance,
    strlgRundi,
    strlgRomanian,
    strlgRussian,
    strlgKinyarwanda,
    strlgSanskrit,
    strlgSardinian,
    strlgSindhi,
    strlgNorthernSami,
    strlgSango,
    strlgSinhalese,
    strlgSlovak,
    strlgSlovenian,
    strlgSamoan,
    strlgShona,
    strlgSomali,
    strlgAlbanian,
    strlgSerbian,
    strlgSwati,
    strlgSothoSouthern,
    strlgSundanese,
    strlgSwedish,
    strlgSwahili,
    strlgTamil,
    strlgTelugu,
    strlgTajik,
    strlgThai,
    strlgTigrinya,
    strlgTurkmen,
    strlgTagalog,
    strlgTswana,
    strlgTonga,
    strlgTurkish,
    strlgTsonga,
    strlgTatar,
    strlgTwi,
    strlgTahitian,
    strlgUighur,
    strlgUkrainian,
    strlgUrdu,
    strlgUzbek,
    strlgVenda,
    strlgVietnamese,
    strlgVolapuk,
    strlgWalloon,
    strlgWolof,
    strlgXhosa,
    strlgYiddish,
    strlgYoruba,
    strlgZhuang,
    strlgChinese,
    strlgZulu,

    strlgSpanishEurope,
    strlgSpanishAmerica,
    strlgPortugueseEurope,
    strlgPortugueseBrasil,
    strlgJapaneseAynu
  );

const
  LangToTextArray: TLanguageNames =
  (
    textlgNeutral,
//    textlgNone,

    textlgAfar,
    textlgAbkhazian,
    textlgAvestan,
    textlgAfrikaans,
    textlgAkan,
    textlgAmharic,
    textlgAragonese,
    textlgArabic,
    textlgAssamese,
    textlgAvaric,
    textlgAymara,
    textlgAzerbaijani,
    textlgBashkir,
    textlgBelarusian,
    textlgBulgarian,
    textlgBihari,
    textlgBislama,
    textlgBambara,
    textlgBengali,
    textlgTibetan,
    textlgBreton,
    textlgBosnian,
    textlgCatalan,
    textlgChechen ,
    textlgChamorro,
    textlgCorsican,
    textlgCree,
    textlgCzech,
    textlgChuvash,
    textlgWelsh,
    textlgDanish,
    textlgGerman,
    textlgDivehi,
    textlgDzongkha,
    textlgEwe,
    textlgGreek,
    textlgEnglish,
    textlgEnglishAmerican,
    textlgEnglishBritish,
    textlgEnglishAustralian,
    textlgEsperanto,
    textlgSpanish,
    textlgEstonian,
    textlgBasque,
    textlgPersian,
    textlgFulah,
    textlgFinnish,
    textlgFijian,
    textlgFaroese,
    textlgFrench,
    textlgWalloonBelgique,
    textlgFrisian,
    textlgIrish,
    textlgGaelic,
    textlgGallegan,
    textlgGuarani,
    textlgGujarati,
    textlgManx,
    textlgHausa,
    textlgHebrew,
    textlgHindi,
    textlgHiriMotu,
    textlgCroatian,
    textlgHaitian,
    textlgHungarian,
    textlgArmenian,
    textlgHerero,
    textlgInterlingua,
    textlgIndonesian,
    textlgInterlingue,
    textlgIgbo,
    textlgSichuanYi,
    textlgInupiaq,
    textlgIdo,
    textlgIcelandic,
    textlgItalian,
    textlgInumlcitut,
    textlgJapanese,
    textlgJavanese,
    textlgGeorgian,
    textlgKongo,
    textlgKikuyu,
    textlgKuanyama,
    textlgKazakh,
    textlgGreenlandic,
    textlgKhmer,
    textlgKannada,
    textlgKorean,
    textlgKanuri,
    textlgKashmiri,
    textlgKurdish,
    textlgCornish,
    textlgKomi,
    textlgKirghiz,
    textlgLatin,
    textlgLuxembourgish,
    textlgGanda,
    textlgLimburgan,
    textlgLingala,
    textlgLao,
    textlgLithuanian,
    textlgLubaKatanga,
    textlgLatvian,
    textlgMalagasy,
    textlgMarshallese,
    textlgMaori,
    textlgMacedonian,
    textlgMalayalam,
    textlgMongolian,
    textlgMoldavian,
    textlgMarathi,
    textlgMalay,
    textlgMaltese,
    textlgBurmese,
    textlgNauru,
    textlgNorwegianBokmaal,
    textlgNdebeleNorth,
    textlgNepali,
    textlgNdonga,
    textlgDutch,
    textlgFlemish,
    textlgNorwegianNynorsk,
    textlgNorwegian,
    textlgNdebeleSouth,
    textlgNavajo,
    textlgChichewa,
    textlgOccitan,
    textlgOjibwa,
    textlgOromo,
    textlgOriya,
    textlgOssetian,
    textlgPanjabi,
    textlgPali,
    textlgPolish ,
    textlgPushto,
    textlgPortuguese,
    textlgQuechua,
    textlgRaetoRomance,
    textlgRundi,
    textlgRomanian,
    textlgRussian,
    textlgKinyarwanda,
    textlgSanskrit,
    textlgSardinian,
    textlgSindhi,
    textlgNorthernSami,
    textlgSango,
    textlgSinhalese,
    textlgSlovak,
    textlgSlovenian,
    textlgSamoan,
    textlgShona,
    textlgSomali,
    textlgAlbanian,
    textlgSerbian,
    textlgSwati,
    textlgSothoSouthern,
    textlgSundanese,
    textlgSwedish,
    textlgSwahili,
    textlgTamil,
    textlgTelugu,
    textlgTajik,
    textlgThai,
    textlgTigrinya,
    textlgTurkmen,
    textlgTagalog,
    textlgTswana,
    textlgTonga,
    textlgTurkish,
    textlgTsonga,
    textlgTatar,
    textlgTwi,
    textlgTahitian,
    textlgUighur,
    textlgUkrainian,
    textlgUrdu,
    textlgUzbek,
    textlgVenda,
    textlgVietnamese,
    textlgVolapuk,
    textlgWalloon,
    textlgWolof,
    textlgXhosa,
    textlgYiddish,
    textlgYoruba,
    textlgZhuang,
    textlgChinese,
    textlgZulu,

    textlgSpanishEurope,
    textlgSpanishAmerica,
    textlgPortugueseEurope,
    textlgPortugueseBrasil,
    textlgJapaneseAynu
  );


function LangToStr
  (const AValue: TUMLCLanguage): string;
begin
  Result := LangToStrArray[AValue];
  // Objetivo: Convertir un valor "language" a un valor "string".
  // Goal: To cast a "language" value to a "string" value.
end;

function LangToText
  (const AValue: TUMLCLanguage): string;
begin
  Result := LangToTextArray[AValue];
  // Objetivo: Convertir un valor "language" a texto legible.
  // Goal: To cast a "language" value to a readable text.
end;

function MatchLanguage
  (const AValue: string; const LanguageNames: TLanguageNames): TUMLCLanguage;
var i: TUMLCLanguage; Found: Boolean;
begin
  i := Low(TUMLCLanguage); Found := FALSE;
  while ((i <= High(TUMLCLanguage)) and (not Found)) do
  begin
    Found := SameText(LanguageNames[i], AValue);
    Inc(i);
  end;

  if (Found)
    then Result := Pred(i)
    else Result := Low(TUMLCLanguage);
  // Goal: Locates a language by its name in a given array.
end;

function StrToLang
  (const AValue: string): TUMLCLanguage;
begin
  Result := MatchLanguage(AValue, LangToStrArray);
  // Goal: To cast a "string" value to a "Language" value.
  // Objetivo: Convertir un valor "string" a un valor "Language".
end;

function TextToLang
  (const AValue: string): TUMLCLanguage;
begin
  Result := MatchLanguage(AValue, LangToTextArray);
  // Goal: To cast a "string" value to a "Language" value.
  // Objetivo: Convertir un valor "string" a un valor "Language".
end;


end.

