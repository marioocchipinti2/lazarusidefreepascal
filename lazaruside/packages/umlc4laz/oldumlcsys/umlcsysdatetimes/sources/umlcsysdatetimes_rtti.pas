(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsysdatetimes_rtti;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules,
  dummy;


// ---

const

  MOD_umlcsysdatetimes_rtti : TUMLCModule =
    ($12,$16,$F9,$DE,$7E,$01,$DA,$4B,$9C,$5D,$D2,$C0,$F2,$2C,$F7,$A2);

// ---


implementation

end.

