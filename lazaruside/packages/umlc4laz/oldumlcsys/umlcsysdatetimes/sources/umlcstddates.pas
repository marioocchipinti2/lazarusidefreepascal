(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstddates;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlccomparisons,
  umlcstddatetimetypes,
  dummy;

// ---

const

 MOD_umlcstddates : TUMLCModule =
   ($CC,$51,$36,$AD,$E0,$38,$45,$4E,$97,$04,$E7,$CF,$A7,$82,$A6,$5B);

// ---

(* global functions *)

function DateToDateTime
  (const AValue: umlcstddate): TDateTime;
function DateTimeToDate
  (const AValue: TDateTime): umlcstddate;

function TryEncodeDate
  (out AValue: umlcstddate; AYear, AMonth, ADay: Word): Boolean;

(* global procedures *)

procedure DecodeDate
  (const ASourceValue: umlcstddate;
   out   AYear, AMonth, ADay: Word);

procedure EncodeDate
  (out   ADestValue: umlcstddate;
   const AYear, AMonth, ADay: Word);

implementation

(* global functions *)

function DateToDateTime
  (const AValue: umlcstddate): TDateTime;
//var Temp: SysUtils.TTimeStamp;
begin
  System.FillByte(Result, sizeof(Result), 0);
  //Temp.Date := AValue;
  //Temp.Time := 0;
  //Result := SysUtils.TimeStampToDateTime(Temp);
  // Goal: Used internally to return a datetime AValue from a time AValue.
  // Objetivo: Usado internamente para regresar un valor fechatiempo de un valor tiempo.
end;

function DateTimeToDate
  (const AValue: TDateTime): umlcstddate;
begin
  Result := SysUtils.DateTimeToTimeStamp(AValue).Date;
  // Goal: Used internally to return a date value from a datetime AValue.
  // Objetivo: Usado internamente para regresar un valor fecha de un valor fechatiempo.
end;

function TryEncodeDate
  (out AValue: umlcstddate; AYear, AMonth, ADay: Word): Boolean;
begin
  Result := true;
  AValue :=
    SysUtils.EncodeDate(AYear, AMonth, ADay);
  // Goal: Groups years, months & days into a complete time value.
  // Objetivo: Agrupa años, meses y dias en un valor tiempo completo.
end;

(* global procedures *)

procedure DecodeDate
  (const ASourceValue: umlcstddate;
   out   AYear, AMonth, ADay: Word);
begin
  if (ASourceValue = NoDate) then
  begin
    AYear  := 0;
    AMonth := 0;
    ADay   := 0;
  end else
  SysUtils.DecodeDate(ASourceValue, AYear, AMonth, ADay);
  // Objetivo: El procedimiento "DecodeDate" separa el valor especificado }
  // como "AValue" en los valores "AYear", "AMonth" y "ADay".

  // Goal: The "DecodeDate" procedure breaks the value specified as }
  // "AValue" into "AYear", "AMonth", and "ADay" values.
end;

procedure EncodeDate
  (out   ADestValue: umlcstddate;
   const AYear, AMonth, ADay: Word);
begin
  ADestValue :=
    SysUtils.EncodeDate(AYear, AMonth, ADay);
  // Objetivo: La funcion "EncodeDate" regresa un valor "umlcstddate"
  // a partir de los valores especificados como "AYear", "AMonth" y "ADay".

  // Goal: The "EncodeDate" function returns a "umlcstddate" value
  // from the values specified as "AYear", "AMonth", and "ADay".
end;


end.

