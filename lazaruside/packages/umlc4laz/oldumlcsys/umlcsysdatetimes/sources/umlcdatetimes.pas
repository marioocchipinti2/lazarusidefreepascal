(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdatetimes;

interface
uses
  SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstddatetimetypes,
  dummy;

// ---

const

 MOD_umlcstddatetimetypes : TUMLCModule =
   ($D3,$A0,$92,$F7,$F9,$8E,$8F,$44,$BF,$FC,$C8,$71,$F5,$C1,$67,$66);

// ---

(* global functions *)

  function PredMonth(const Value: UMLCTDateTime): UMLCTDateTime;
  function SuccMonth(const Value: UMLCTDateTime): UMLCTDateTime;

  function EncodeDateTime(const Date, Time: UMLCTDateTime): UMLCTDateTime;
  procedure DecodeDateTime(const Value: UMLCTDateTime; var Date, Time: UMLCTDateTime);

  // operator =(const A, B: UMLCTDateTime): Boolean;
  function Equal(const A, B: UMLCTDateTime): Boolean;
  function Compare(const A, B: UMLCTDateTime): TComparison;

implementation

(* global functions *)

function PredMonth(const Value: UMLCTDateTime): UMLCTDateTime;
var Year, Month, Day: Word;
begin
  SysUtils.DecodeDate(Value, Year, Month, Day);
  Dec(Month);
  Result := SysUtils.EncodeDate(Year, Month, Day);
end;

function SuccMonth(const Value: UMLCTDateTime): UMLCTDateTime;
var Year, Month, Day: Word;
begin
  SysUtils.DecodeDate(Value, Year, Month, Day);
  Inc(Month);
  Result := SysUtils.EncodeDate(Year, Month, Day);
end;

function EncodeDateTime(const Date, Time: UMLCTDateTime): UMLCTDateTime;
begin
  Result := Date + Time;
  // Goal: Join a date & time value.
  // Objetivo: Juntar un valor fecha con un valor hora.
end;

procedure DecodeDateTime(const Value: UMLCTDateTime; var Date, Time: UMLCTDateTime);
var Year, Month, Day: Word; Hour, Min, Sec, MSec: Word;
begin
  SysUtils.DecodeDate(Value, Year, Month, Day);
  // obtain year, month & day from datetime value
  // obtener a�o, mes y dia de valor fechahora

  Date := SysUtils.EncodeDate(Year, Month, Day);
  // obtain the date without the time
  // obtener la fecha sin la hora

  SysUtils.DecodeTime(Value, Hour, Min, Sec, MSec);
  // obtain hour, minutes, seconds, & milliseconds from datetime value
  // obtener hora, minutos, segundos y milisegundos de valor fechahora

  Time := SysUtils.EncodeTime(Hour, Min, Sec, MSec);
  // obtain the time without the date
  // obtener la hora sin la fecha

  // Goal: Splits a date & time value.
  // Objetivo: Separa un valor fecha con un valor hora.
end;

function Equal(const A, B: UMLCTDateTime): Boolean;
begin
  Result := (A = B);
end;

function Compare(const A, B: UMLCTDateTime): TComparison;
begin
  if (A = B)
    then Result := cmpEqual
  else if (A < B)
    then Result := cmpLower
  else Result := cmpHigher;
end;

end.
