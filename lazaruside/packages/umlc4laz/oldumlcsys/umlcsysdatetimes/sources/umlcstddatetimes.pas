unit umlcstddatetimes;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, DateUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlccomparisons,
  umlcstddatetimetypes,
  dummy;

// ---

const

 MOD_umlcstddatetimes : TUMLCModule =
   ($0A,$58,$04,$35,$58,$E9,$A6,$4F,$BA,$86,$3F,$F8,$B0,$2E,$6F,$06);

// ---


(* global functions *)

function TryEncodeDateTime
  (out   ADestValue: umlcstddatetime;
   const AYear, AMonth, ADay: Word;
   const AHour, AMin, ASec, AMSec: Word): Boolean;

function TryDecodeDateTime
  (const ASourceValue: umlcstddatetime;
   out   AYear, AMonth, ADay: Word;
   out   AHour, AMin, ASec, AMSec: Word): Boolean;

(* global procedures *)

procedure EncodeDateTime
  (out   ADestValue: umlcstddatetime;
   const AYear, AMonth, ADay: Word;
   const AHour, AMin, ASec, AMSec: Word);

procedure DecodeDateTime
  (const ASourceValue: umlcstddatetime;
   out   AYear, AMonth, ADay: Word;
   out   AHour, AMin, ASec, AMSec: Word);

implementation

(* global functions *)

function TryEncodeDateTime
  (out   ADestValue: umlcstddatetime;
   const AYear, AMonth, ADay: Word;
   const AHour, AMin, ASec, AMSec: Word): Boolean;
begin
  Result :=
    DateUtils.TryEncodeDateTime
      (AYear, AMonth, ADay, AHour, AMin, ASec, AMSec, ADestValue);
end;

function TryDecodeDateTime
  (const ASourceValue: umlcstddatetime;
   out   AYear, AMonth, ADay: Word;
   out   AHour, AMin, ASec, AMSec: Word): Boolean;
var ADate: umlcstddate; ATime: umlcstdtime;
begin
  ADate := DateUtils.DateOf(ASourceValue);
  ATime := DateUtils.TimeOf(ASourceValue);

  SysUtils.DecodeDate
    (ADate, AYear, AMonth, ADay);
  SysUtils.DecodeTime
    (ATime, AHour, AMin, ASec, AMSec);
end;

(* global procedures *)

procedure EncodeDateTime
  (out   ADestValue: umlcstddatetime;
   const AYear, AMonth, ADay: Word;
   const AHour, AMin, ASec, AMSec: Word);
begin
  if (not umlcstddatetimes.TryEncodeDateTime
    (ADestValue,AYear, AMonth, ADay, AHour, AMin, ASec, AMSec)) then
  begin
    // raise error
  end;
end;

procedure DecodeDateTime
  (const ASourceValue: umlcstddatetime;
   out   AYear, AMonth, ADay: Word;
   out   AHour, AMin, ASec, AMSec: Word);
begin
  if (not umlcstddatetimes.TryDecodeDateTime
    (ASourceValue, AYear, AMonth, ADay, AHour, AMin, ASec, AMSec)) then
  begin
    // raise error
  end;
end;



end.

