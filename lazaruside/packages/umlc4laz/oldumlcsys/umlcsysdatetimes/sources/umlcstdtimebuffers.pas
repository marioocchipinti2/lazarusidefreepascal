(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdtimebuffers;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 1D 64 bits segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstddatetimetypes, umlcstdtimes,
  dummy;

// ---

const

  MOD_umlcstdtimebuffers : TUMLCModule =
    ($2B,$50,$5B,$EB,$FF,$5F,$65,$45,$81,$2F,$F0,$C7,$64,$07,$98,$16);

// ---

(* global types *)

type
  umlcstdtimebuffer = (* alias of *) pointer;

// ---

(* global functions *)

  function BufferIsEmpty
    (var   ABuffer: Pointer;
     const ACount:  Cardinal): Boolean; overload;

  function BufferSize
    (const ACount: Cardinal): Cardinal; overload;

  function BufferNew
    (const ACount: Cardinal): Pointer; overload;

  function BufferAt
    (var   ABuffer: Pointer;
     const ACount:  Cardinal;
     const AIndex:  Cardinal): Boolean; overload;

(* global procedures *)

  procedure BufferClear
    (var   ABuffer: Pointer;
     const ACount:  Cardinal); overload;

  procedure BufferFill
    (var   ABuffer: Pointer;
     const ACount:  Cardinal;
     const AValue:  umlcstdtime); overload;

  procedure BufferDrop
    (var   ABuffer: Pointer;
     const ACount: Cardinal); overload;

(* global properties *)

function BufferGetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal): pointer; overload;

procedure BufferSetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   const AValue:  pointer); overload;

implementation

(* global functions *)

function BufferIsEmpty
  (var   ABuffer: Pointer;
   const ACount:  Cardinal): Boolean;
var I: Cardinal; Match: Boolean; Item: umlcpstdtime;
begin
  Result := false;
  if (Assigned(ABuffer) and (ACount > 0)) then
  begin
    Item := umlcpstdtime(ABuffer);
    Match := false;
    I := 0;
    while ((I <= Pred(ACount)) and (not Match)) do
    begin
      Match :=
        (Item^ <> 0);

      Inc(I);
    end;

    Result := not Match;
  end;
end;

function BufferSize
  (const ACount: Cardinal): Cardinal;
begin
  Result :=
    sizeof(umlcstdtime) * ACount;
end;

function BufferNew
  (const ACount: Cardinal): Pointer;
var ASize: Cardinal;
begin
  Result := nil;
  ASize := sizeof(umlcstdtime) * ACount;
  System.GetMem(Result, ASize);
end;


(* global procedures *)

procedure BufferClear
  (var   ABuffer: Pointer;
   const ACount:  Cardinal);
var ASize: Cardinal;
begin
  if (Assigned(ABuffer)) then
  begin
    ASize := sizeof(umlcstdtime) * ACount;
    System.FillByte(ABuffer^, ASize, 0);
  end;
end;

procedure BufferFill
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AValue:  umlcstdtime);
//var ASize: Cardinal;
begin
  if (Assigned(ABuffer)) then
  begin
    //ASize := sizeof(umlcstdtime) * ACount;
    //System.FillByte(ABuffer^, ASize, AValue);
  end;
end;

procedure BufferDrop
  (var   ABuffer: Pointer;
   const ACount:  Cardinal);
var ASize: Cardinal;
begin
  if (Assigned(ABuffer)) then
  begin
    ASize := sizeof(umlcstdtime) * ACount;
    System.FreeMem(ABuffer, ASize);
  end;
end;

function BufferAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal): Boolean;
begin
  Result := false;
  if (Assigned(ABuffer)) then
  begin
    //ASize := sizeof(umlcstdtime) * ACount;
    //System.FreeMem(ABuffer, ASize);
  end;
end;

(* global properties *)

function BufferGetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal): pointer;
begin
  Result := nil;
  if (Assigned(ABuffer) and (ACount > 0)) then
  begin
    //ASize := sizeof(umlcstdtime) * ACount;
    //System.FreeMem(ABuffer, ASize);
  end;
end;

procedure BufferSetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   const AValue:  pointer);
begin
  if (Assigned(ABuffer) and (ACount > 0) and Assigned(AValue)) then
  begin
    //ASize := sizeof(umlcstdtime) * ACount;
    //System.FreeMem(ABuffer, ASize);
  end;
end;


end.

