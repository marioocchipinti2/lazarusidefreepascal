(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctimestamps;

interface
uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstddatetimetypes,
  umlcdates, umlctimes,
  dummy;

// ---

const

  MOD_umlctimestamps : TUMLCModule =
    ($8C,$6D,$CD,$2E,$CB,$E4,$CC,$4A,$97,$C8,$DE,$BA,$87,$E9,$20,$D2);

// ---

(* global properties *)

  function getSystemTimeStamp: umlcstdtimestamp;
  procedure setSystemTimeStamp
    (const Value: umlcstdtimestamp);

(* global functions *)

  { = } function Equal
    (const A, B: umlcstdtimestamp): Boolean;
  { < } function Lesser
    (const A, B: umlcstdtimestamp): Boolean;
  { > } function Greater
    (const A, B: umlcstdtimestamp): Boolean;

  function Compare
    (const A, B: umlcstdtimestamp): Integer;

implementation

(* global properties *)

function getSystemTimeStamp: umlcstdtimestamp;
var SystemTime: TSystemTime;
begin
  Windows.GetLocalTime(SystemTime);
  with SystemTime do
  begin
    //Result.Year   := wYear;
    //Result.Month  := wMonth;
    //Result.Day    := wDay;
    //Result.Hour   := wHour;
    //Result.Minute := wMinute;
    //Result.Second := wSecond;
    //Result.Milliseconds := wMilliseconds;
  end;
end;

procedure setSystemTimeStamp
  (const Value: umlcstdtimestamp);
var SystemTime: TSystemTime;
begin
  //with SystemTime do
  //begin
  //  wYear   := Value.Year;
  //  wMonth  := Value.Month;
  //  wDay    := Value.Day;
  //  wHour   := Value.Hour;
  //  wMinute := Value.Minute;
  //  wSecond := Value.Second;
  //  wMilliseconds := Value.Milliseconds;
  //end;
  //Windows.SetLocalTime(SystemTime);
end;

(* global functions *)

function Equal
  (const A, B: umlcstdtimestamp): Boolean;
begin
//  Result := (Int64(A) = Int64(B));
  Result := FALSE;
  // Goal: Returns if 2 timestamp values are equal.
  // Objetivo: Regresa si 2 valores tiempoestampa son iguales.
end;

function Lesser
  (const A, B: umlcstdtimestamp): Boolean;
begin
//  Result := (Int64(A) < Int64(B));
  Result := FALSE;
  // Goal: Returns if 2 timestamp values are equal.
  // Objetivo: Regresa si 2 valores tiempoestampa son iguales.
end;

function Greater
  (const A, B: umlcstdtimestamp): Boolean;
begin
//  Result := (Int64(A) < Int64(B));
  Result := FALSE;
  // Goal: Returns if 2 timestamp values are equal.
  // Objetivo: Regresa si 2 valores tiempoestampa son iguales.
end;

function Compare
  (const A, B: umlcstdtimestamp): Integer;
begin
  if (Equal(A, B))
    then Result := 0
  else if (Lesser(A, B))
    then Result := -1
  else Result := 0;
  // Goal: Returns if 2 timestamp values are equal.
  // Objetivo: Regresa si 2 valores tiempoestampa son iguales.
end;

end.
