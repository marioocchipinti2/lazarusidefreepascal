unit umlcstddatebyptrs;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement a Date type and it's operations,
 ** by using pointers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcstddatetimetypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcstddatebyptrs : TUMLCModule =
   ($4C,$07,$52,$FD,$51,$E0,$E1,$42,$89,$94,$C4,$FE,$5D,$E9,$39,$43);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: umlcstddate): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

function IsEmpty
 (const ASource: pointer): Boolean; overload;

(* global procedures *)

procedure Clear
 (out ADest: pointer); overload;


implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcstddate): pointer;
var P: umlcpstddate;
begin
  System.GetMem(P, sizeof(umlcstddate));
  P^ := AValue;
  Result := pointer(P);
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcstddate));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(umlcstddate), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;



end.

