unit umlcstdtimebyptrs;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement a Date type and it's operations,
 ** by using pointers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcstddatetimetypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcstdtimebyptrs : TUMLCModule =
   ($B3,$8F,$BE,$B7,$01,$71,$E3,$4E,$80,$F3,$C9,$42,$5E,$BF,$E2,$B7);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: umlcstdtime): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

function IsEmpty
 (const ASource: pointer): Boolean; overload;

(* global procedures *)

procedure Clear
 (out ADest: pointer); overload;



implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcstdtime): pointer;
var P: umlcpstdtime;
begin
  System.GetMem(P, sizeof(umlcstdtime));
  P^ := AValue;
  Result := pointer(P);
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcstdtime));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(umlcstdtime), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;


end.

