(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdates;

{ Espanol }

  // Objetivo: Provee constantes para manejo de valores tipo fecha.

{ English }

  // Goal: Provides constants for date type values management.

interface
uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstddatetimetypes,
  dummy;

// ---

const

MOD_umlcstddatetimetypes : TUMLCModule =
  ($6F,$58,$BE,$AF,$FF,$55,$C1,$4C,$83,$9A,$F3,$C3,$5E,$D6,$ED,$28);

// ---

(* global properties *)

  function getSystemDate(): umlcstddate;
  procedure setSystemDate
    (const AValue: umlcstddate);

(* global functions *)

  function IsLeapYear
    (AYear: Word): Boolean;

  function MonthDaysOf
    (const AMonthNo:    Byte;
     const AIsLeapYear: Boolean): Word;

  function DateToDateTime
    (const AValue: umlcstddate): TDateTime;
  function DateTimeToDate
    (const AValue: TDateTime): umlcstddate;

  function TryEncodeDate
    (out AValue: umlcstddate; AYear, AMonth, ADay: Word): Boolean;

  function EncodeDate
    (const AYear, AMonth, ADay: Word): umlcstddate;
  procedure DecodeDate
    (const AValue: umlcstddate; out AYear, AMonth, ADay: Word);

  function VarToDate
    (const AValue: Variant): umlcstddate;
  function DateToVar
    (const AValue: umlcstddate): Variant;

  function PredWeek
    (const AValue: umlcstddate; ACount: Integer): umlcstddate;
  function PredMonth
    (const AValue: umlcstddate; ACount: Integer): umlcstddate;
  function PredYear
    (const AValue: umlcstddate; ACount: Integer): umlcstddate;
  function PredCentury(
   const AValue: umlcstddate; ACount: Integer): umlcstddate;

  function SuccWeek
    (const AValue: umlcstddate; ACount: Integer): umlcstddate;
  function SuccMonth
    (const AValue: umlcstddate; ACount: Integer): umlcstddate;
  function SuccYear
    (const AValue: umlcstddate; ACount: Integer): umlcstddate;
  function SuccCentury
    (const AValue: umlcstddate; ACount: Integer): umlcstddate;

  { = } function Equal
    (const A, B: umlcstddate): Boolean;

  function Compare
    (const A, B: umlcstddate): TComparison;

implementation

{ The MonthDays array can be used to quickly find the number of
  days in a AMonth:  MonthDays[IsLeapYear(Y), M]      }

type
  PMonthDayCountTable = ^TMonthDayCountTable;
  TMonthDayCountTable = array[1..12] of Word;

const
  MonthDays: array [Boolean] of TMonthDayCountTable =
    ((31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31),
     (31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31));

(* global properties *)

function getSystemDate: umlcstddate;
var SystemTime: TSystemTime;
begin
  Windows.GetLocalTime(SystemTime);
  with SystemTime do
    Result := umlcdates.EncodeDate(wYear, wMonth, wDay);
end;

procedure setSystemDate(const AValue: umlcstddate);
var SystemTime: TSystemTime;
begin
  with SystemTime do
    umlcDates.DecodeDate(AValue, wYear, wMonth, wDay);
  Windows.SetLocalTime(SystemTime);
end;

(* global functions *)

function IsLeapYear
  (AYear: Word): Boolean;
begin
  Result := (AYear mod 4 = 0) and ((AYear mod 100 <> 0) or (AYear mod 400 = 0));
  // Goal: "IsLeapYear" determines whether the given AYear is a leap AYear.
  // Objetivo: "IsLeapYear" determina cuando el a�o dado es a�o bisiesto.
end;

function MonthDaysOf
  (const AMonthNo:    Byte;
   const AIsLeapYear: Boolean): Word;
begin
  Result :=
    MonthDays[AIsLeapYear, AMonthNo];
end;

function DateToDateTime
  (const AValue: umlcstddate): TDateTime;
//var Temp: SysUtils.TTimeStamp;
begin
  //Temp.Date := AValue;
  //Temp.Time := 0;
  //Result := SysUtils.TimeStampToDateTime(Temp);
  // Goal: Used internally to return a datetime AValue from a time AValue.
  // Objetivo: Usado internamente para regresar un valor fechatiempo de un valor tiempo.
end;

function DateTimeToDate
  (const AValue: TDateTime): umlcstddate;
begin
  Result := SysUtils.DateTimeToTimeStamp(AValue).Date;
  // Goal: Used internally to return a date AValue from a datetime AValue.
  // Objetivo: Usado internamente para regresar un valor fecha de un valor fechatiempo.
end;

function TryEncodeDate
  (out AValue: umlcstddate; AYear, AMonth, ADay: Word): Boolean;
begin
  Result := true;
  AValue :=
    SysUtils.EncodeDate(AYear, AMonth, ADay);
  // Goal: Groups years, months & days into a complete time AValue.
  // Objetivo: Agrupa a�os, meses y dias en un valor tiempo completo.
end;

function EncodeDate
  (const AYear, AMonth, ADay: Word): umlcstddate;
begin
  Result := umlcdates.DateTimeToDate(SysUtils.EncodeDate(AYear, AMonth, ADay));
  // Objetivo: La funcion "EncodeDate" regresa un valor "umlcstddate"
  // a partir de los valores especificados como "AYear", "AMonth" y "ADay".

  // Goal: The "EncodeDate" function returns a "umlcstddate" AValue
  // from the values specified as "AYear", "AMonth", and "ADay".
end;

procedure DecodeDate
  (const AValue: umlcstddate; out AYear, AMonth, ADay: Word);
begin
  if (AValue = NoDate) then
  begin
    AYear  := 0;
    AMonth := 0;
    ADay   := 0;
  end else
  SysUtils.DecodeDate(DateToDateTime(AValue), AYear, AMonth, ADay);
  // Objetivo: El procedimiento "DecodeDate" separa el valor especificado }
  // como "AValue" en los valores "AYear", "AMonth" y "ADay".

  // Goal: The "DecodeDate" procedure breaks the AValue specified as }
  // "AValue" into "AYear", "AMonth", and "ADay" values.
end;

function VarToDate(const AValue: Variant): umlcstddate;
begin
  Result := umlcstddate(LongInt(AValue));
  // Goal: Returns a date AValue stored in a variant.
  // Objetivo: Regresa un valor fecha almacenado en un variante.
end;

function DateToVar(const AValue: umlcstddate): Variant;
begin
  Result := Variant(AValue);
  // Goal: Stores a date AValue into a variant.
  // Objetivo: Almacena un valor fecha dentro de un variante.
end;

function PredWeek(const AValue: umlcstddate; ACount: Integer): umlcstddate;
begin
  Result := (AValue - 7);
  // Goal: Decrements the given value by "ACount" weeks.
  // Objetivo: Decrementa el valor dado en "ACount" semanas.
end;

function PredMonth(const AValue: umlcstddate; ACount: Integer): umlcstddate;
begin
  Result := DateTimeToDate(SysUtils.IncMonth(DateToDateTime(AValue), - ACount));
  // Goal: Decrements the given value by "ACount" months.
  // Objetivo: Decrementa el valor dado en "ACount" meses.
end;

function PredYear
  (const AValue: umlcstddate; ACount: Integer): umlcstddate;
var AYear, AMonth, ADay: Word;
begin
  DecodeDate(AValue, AYear, AMonth, ADay);
  Result := EncodeDate(Succ(AYear), AMonth, ADay);
  // Goal: Decrements the given value by "ACount" years.
  // Objetivo: Decrementa el valor dado en "ACount" a�os.
end;

function PredCentury
  (const AValue: umlcstddate; ACount: Integer): umlcstddate;
var AYear, AMonth, ADay: Word;
begin
  DecodeDate(AValue, AYear, AMonth, ADay);
  Result := EncodeDate(AYear - 100, AMonth, ADay);
  // Goal: Decrements the given value by "ACount" centuries.
  // Objetivo: Decrementa el valor dado en "ACount" siglos.
end;

function SuccWeek
  (const AValue: umlcstddate; ACount: Integer): umlcstddate;
begin
  Result := (AValue + 7);
  // Goal: Increments the given value by "ACount" weeks.
  // Objetivo: Incrementa el valor dado en "ACount" semanas.
end;

function SuccMonth
  (const AValue: umlcstddate; ACount: Integer): umlcstddate;
begin
  Result := DateTimeToDate(SysUtils.IncMonth(DateToDateTime(AValue), + ACount));
  // Goal: Increments the given value by "ACount" months.
  // Objetivo: Incrementa el valor dado en "ACount" meses.
end;

function SuccYear
  (const AValue: umlcstddate; ACount: Integer): umlcstddate;
var AYear, AMonth, ADay: Word;
begin
  DecodeDate(AValue, AYear, AMonth, ADay);
  Result := EncodeDate(Succ(AYear), AMonth, ADay);
  // Goal: Increments the given value by "ACount" years.
  // Objetivo: Incrementa el valor dado en "ACount" a�os.
end;

function SuccCentury
  (const AValue: umlcstddate; ACount: Integer): umlcstddate;
var AYear, AMonth, ADay: Word;
begin
  DecodeDate(AValue, AYear, AMonth, ADay);
  Result := EncodeDate(AYear + 100, AMonth, ADay);
  // Goal: Increments the given value by "ACount" centuries.
  // Objetivo: Incrementa el valor dado en "ACount" siglos.
end;

function Equal
  (const A, B: umlcstddate): Boolean;
begin
  Result := (A = B);
  // Goal: Returns if 2 date values are equal.
  // Objetivo: Regresa si 2 valores fecha son iguales.
end;

function Compare
  (const A, B: umlcstddate): TComparison;
begin
  if (A = B)
    then Result := cmpEqual
  else if (A < B)
    then Result := cmpLower
  else Result := cmpHigher;
  // Goal: Returns if 2 date values are equal.
  // Objetivo: Regresa si 2 valores fecha son iguales.
end;

end.
