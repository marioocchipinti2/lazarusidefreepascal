{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcsysdatetimes60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcdates, umlcdatetimes, umlcstddates, umlcstddatebyptrs, umlcstddatetimes, 
  umlcstddatetimesbyptrs, umlcstdtimes, umlcstdtimebyptrs, umlcstdtimestamps, 
  umlcstdtimestampsbyptrs, umlcsysdatetimes_rtti, umlctimes, umlctimestamps, 
  umlcstdtimebuffers;

implementation

end.
