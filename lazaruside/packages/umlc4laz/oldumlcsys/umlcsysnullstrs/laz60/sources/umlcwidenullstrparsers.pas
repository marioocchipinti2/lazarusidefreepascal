unit umlcwidenullstrparsers;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcstdnullstrtypes,
  umlccomparisons,
  umlcwidenullstrs,
  dummy;

function StartsWithEqualStr
  (const AHaystack: widenullstring; const ANeedleStr: widenullstring): Boolean;

function FinishesWithEqualStr
  (const AHaystack: widenullstring; const ANeedleStr: widenullstring): Boolean;

implementation

function StartsWithEqualStr
(const AHaystack: widenullstring; const ANeedleStr: widenullstring): Boolean;
begin
Result := false;
end;

function FinishesWithEqualStr
(const AHaystack: widenullstring; const ANeedleStr: widenullstring): Boolean;
begin
Result := false;
end;


end.

