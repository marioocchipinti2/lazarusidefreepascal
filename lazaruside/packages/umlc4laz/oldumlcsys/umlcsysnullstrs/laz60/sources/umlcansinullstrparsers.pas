unit umlcansinullstrparsers;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcstdnullstrtypes,
  umlccomparisons,
  umlcansinullstrs,
  dummy;

  function StartsWithEqualStr
    (const AHaystack: ansinullstring; const ANeedleStr: ansinullstring): Boolean;

  function FinishesWithEqualStr
    (const AHaystack: ansinullstring; const ANeedleStr: ansinullstring): Boolean;

implementation

function StartsWithEqualStr
  (const AHaystack: ansinullstring; const ANeedleStr: ansinullstring): Boolean;
begin
  Result := false;
end;

function FinishesWithEqualStr
  (const AHaystack: ansinullstring; const ANeedleStr: ansinullstring): Boolean;
begin
  Result := false;
end;




end.

