{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcsysnullstrs60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcansinullstrs, umlcsysnullstrs_rtti, umlcwidenullstrs, 
  umlcansinullstrparsers, umlcwidenullstrparsers;

implementation

end.
