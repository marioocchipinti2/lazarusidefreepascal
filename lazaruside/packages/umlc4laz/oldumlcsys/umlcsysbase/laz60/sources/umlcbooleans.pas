(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbooleans;

(* Espanol *)

  // Objetivo: Provee constantes para manejo de valores booleanos.

(* English *)

  // Goal: Provides constants for boolean values management.

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlcstdbooltypes,
  dummy;

// ---

const

 MOD_umlcbooleans : TUMLCModule =
   ($B9,$ED,$2F,$FE,$DF,$BD,$CC,$49,$B6,$1A,$CB,$95,$D3,$A7,$81,$B2);

// ---

function BoolToInt
  (const AValue: Boolean): Integer;
function IntToBool
  (const AValue: Integer): Boolean;

implementation

function BoolToInt
  (const AValue: Boolean): Integer;
begin
  if AValue
    then Result := 1
    else Result := 0;
//    then Result := -1
  // Goal: To cast a "boolean" AValue to a "integer" value.
  // Objetivo: Convertir un valor "boolean" a un valor "integer".
end;

function IntToBool
  (const AValue: Integer): Boolean;
begin
  Result := (AValue <> 0);
  // Goal: To cast a "integer" AValue to a "boolean" value.
  // Objetivo: Convertir un valor "integer" a un valor "boolean".
end;


end.
