(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmonths;

(* Espanol *)

  // Objetivo: Provee constantes para manejo de meses.

(* English *)

  // Goal: Provides constants for months management.

interface
uses
  SysUtils,
  dummy;

type

  TMonth =
  ( moNone,
    moJan, moFeb, moMar, moApr, moMay, moJun,
    moJul, moAug, moSep, moOct, moNov, moDec);

  function ValidateMonth(Value: Integer): Integer;
  function SafeMonth(const AValue: TMonth): TMonth;

implementation

function ValidateMonth(Value: Integer): Integer;
begin
  if (Value < 1)
    then Result := 1
  else if (Value > 12)
    then Result := 12
  else Result := Value
  // Goal: Checks that a month number is between 1 and 12.
  // Objetivo: Revisa que el numero de mes este entre 1 y 12.
end;

function SafeMonth(const AValue: TMonth): TMonth;
begin
  Result := AValue;
  if (Result < Low(TMonth))
    then Result := Low(TMonth);
  if (Result > High(TMonth))
    then Result := High(TMonth);
  // Goal: Checks that a month number is between 1 and 12.
  // Objetivo: Revisa que el numero de mes este entre 1 y 12.
end;


end.
