unit umlcdebug;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  dummy;

(* global types *)

type
  DebugWriteFunctor =
    procedure (* ^ *) (const AMsg: string) of object;

type
  DebugWriteEoLnFunctor =
    procedure (* ^ *) ( (* noparams *) ) of object;

type
  DebugWriteLnFunctor =
    procedure (* ^ *) (const AMsg: string) of object;

type
  TUMLCDebugClass = class
  public
  (* public declarations *)

    procedure DebugWrite(const AMsg: string);
    procedure DebugWriteEoLn();
    procedure DebugWriteLn(const AMsg: string);
  end;

(* global variables *)

var
  DebugObject:    TUMLCDebugClass;
  DebugWrite:     DebugWriteFunctor;
  DebugWriteEoLn: DebugWriteEoLnFunctor;
  DebugWriteLn:   DebugWriteLnFunctor;

implementation

procedure TUMLCDebugClass.DebugWrite
  (const AMsg: string);
begin
  System.Write(AMsg);
end;

procedure TUMLCDebugClass.DebugWriteEoLn();
begin
  self.DebugWrite(#13#10);
end;

procedure TUMLCDebugClass.DebugWriteLn
  (const AMsg: string);
begin
  self.DebugWrite(AMsg);
  self.DebugWriteEoLn();
end;

procedure UnitConstructor;
begin
  umlcdebug.DebugObject.Create();

  umlcdebug.DebugWrite :=
    @umlcdebug.DebugObject.DebugWrite;
  umlcdebug.DebugWriteEoLn :=
    @umlcdebug.DebugObject.DebugWriteEoLn;
  umlcdebug.DebugWriteLn :=
    @umlcdebug.DebugObject.DebugWriteLn;
end;

procedure UnitDestructor;
begin
  umlcdebug.DebugObject.Free();
end;

initialization
  UnitConstructor;
finalization
  UnitDestructor;
end.

