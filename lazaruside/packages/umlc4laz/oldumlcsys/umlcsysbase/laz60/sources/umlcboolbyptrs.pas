unit umlcboolbyptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcboolbyptrs : TUMLCModule =
   ($42,$51,$99,$E3,$0D,$CE,$26,$40,$82,$E7,$69,$C0,$04,$0A,$1B,$81);

 // ---


 (* global functions *)

 function ConstToPtr
   (const AValue: boolean): pointer;

 procedure DropPtr
   (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: boolean): pointer;
var P: pboolean;
begin
  System.GetMem(P, sizeof(boolean));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(boolean));
end;


end.

