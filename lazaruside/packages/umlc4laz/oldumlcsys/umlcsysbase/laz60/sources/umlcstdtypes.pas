(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdtypes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcuuids,
  dummy;

// ---

const

 MOD_umlcstdtypes : TUMLCModule =
   ($76,$87,$81,$18,$69,$A9,$CC,$46,$8A,$BE,$81,$72,$4A,$D2,$F9,$DF);

// ---

const

  // ---

  ID_Pointer : TUMLCType =
    ($4D,$E2,$51,$BD,$F6,$FA,$14,$49,$86,$1B,$B9,$8F,$E0,$2A,$ED,$DC);
  ID_PPointer : TUMLCType =
    ($8C,$E8,$D1,$36,$BE,$0E,$16,$44,$A3,$B8,$E9,$23,$2E,$FA,$04,$D1);

  // ---

  ID_AnsiChar : TUMLCType =
    ($EB,$19,$C8,$70,$A8,$F3,$80,$4A,$93,$9C,$B4,$EE,$57,$D7,$10,$24);

  ID_PAnsiChar : TUMLCType =
    ($54,$5B,$D5,$93,$92,$29,$51,$4C,$AE,$DE,$17,$27,$05,$C2,$FF,$C4);

  ID_AnsiString : TUMLCType =
    ($60,$43,$F7,$99,$41,$C6,$33,$43,$9E,$63,$27,$F3,$A1,$3A,$F5,$65);

  ID_PAnsiString : TUMLCType =
    ($60,$43,$F7,$99,$41,$C6,$33,$43,$9E,$63,$27,$F3,$A1,$3A,$F5,$65);

  ID_WideChar : TUMLCType =
    ($68,$1B,$17,$F7,$D4,$C7,$CC,$4D,$87,$93,$47,$17,$60,$02,$3F,$14);

  ID_PWideChar : TUMLCType =
    ($AE,$20,$D7,$38,$58,$48,$EF,$41,$81,$B9,$0B,$89,$93,$37,$F7,$79);


  ID_WideString : TUMLCType =
    ($89,$19,$F5,$B4,$51,$C5,$3A,$4A,$84,$AD,$11,$57,$33,$BC,$74,$E9);

  ID_PWideString : TUMLCType =
    ($A6,$07,$88,$3C,$B3,$B5,$C0,$45,$BF,$F0,$BC,$1E,$8F,$00,$96,$83);


  ID_Char : TUMLCType =
    ($11,$79,$EE,$88,$FE,$B4,$F0,$47,$BB,$F4,$A7,$CC,$4A,$4F,$20,$31);

  ID_PChar : TUMLCType =
    ($F3,$CA,$D1,$A3,$CF,$FB,$5B,$48,$B1,$34,$BB,$80,$BE,$E3,$DB,$1D);

  ID_String : TUMLCType =
    ($EF,$12,$86,$F0,$BE,$FF,$B7,$45,$83,$4B,$3B,$7C,$A3,$81,$9B,$9B);

  ID_PString : TUMLCType =
    ($6D,$CF,$88,$FF,$44,$1D,$03,$4F,$82,$E9,$6C,$3D,$05,$B9,$1C,$14);


  ID_ShortString : TUMLCType =
    ($E1,$32,$61,$05,$E6,$2C,$82,$4E,$A8,$81,$BD,$19,$A4,$58,$1A,$32);

  ID_PShortString : TUMLCType =
    ($1D,$18,$53,$95,$78,$46,$30,$4F,$80,$34,$8A,$A0,$0C,$E4,$6A,$1D);

  ID_ANSIMemo : TUMLCType =
    ($EB,$B5,$C2,$06,$AD,$50,$50,$4E,$93,$71,$17,$17,$B4,$2C,$17,$DE);

  ID_PANSIMemo : TUMLCType =
    ($F3,$78,$F7,$40,$B8,$CB,$97,$47,$96,$E5,$A6,$5C,$DD,$90,$5C,$BD);

  ID_WideMemo : TUMLCType =
    ($2C,$D4,$B0,$11,$93,$31,$C5,$45,$96,$69,$9C,$38,$F8,$81,$13,$20);

  ID_PWideMemo : TUMLCType =
    ($35,$BA,$42,$F2,$5C,$C9,$A2,$44,$96,$CB,$60,$63,$D6,$FE,$EB,$5A);

  // ---

  ID_TAnsiNullString : TUMLCType =
    ($88,$9D,$91,$D7,$3E,$8D,$04,$4E,$AB,$81,$15,$B3,$B7,$B4,$CD,$C6);

  ID_TWideNullString : TUMLCType =
    ($2B,$D1,$C4,$DF,$82,$7D,$54,$4D,$82,$E8,$3F,$D2,$AE,$13,$35,$BC);


  ID_PAnsiNullString : TUMLCType =
    ($57,$17,$48,$89,$47,$A5,$5D,$42,$A3,$32,$E1,$68,$5B,$BE,$60,$A3);

  ID_PWideNullString : TUMLCType =
    ($FD,$EF,$00,$79,$DE,$31,$86,$42,$90,$6B,$BD,$3A,$AE,$4C,$02,$87);

  // ---

  ID_AnsiCharSet : TUMLCType =
    ($4B,$5E,$65,$7D,$58,$F9,$E6,$4F,$A4,$76,$E4,$BF,$F3,$48,$65,$BA);

  ID_WideCharSet : TUMLCType =
    ($31,$6B,$0A,$1B,$DC,$64,$F7,$40,$A9,$36,$3F,$32,$CB,$50,$90,$54);


  ID_PAnsiCharSet : TUMLCType =
    ($9F,$BE,$A8,$79,$44,$C2,$E0,$40,$AC,$4A,$C4,$CB,$44,$C2,$18,$28);

  ID_PWideCharSet : TUMLCType =
    ($E2,$78,$95,$8A,$45,$39,$B8,$4F,$B9,$6F,$20,$F0,$AE,$A5,$F8,$4C);

  // ---

  ID_TComparison : TUMLCType =
    ($3A,$44,$71,$A8,$61,$64,$ED,$41,$AB,$7E,$09,$06,$39,$42,$83,$82);

  ID_PComparison : TUMLCType =
    ($D6,$D7,$17,$C3,$86,$3F,$10,$4B,$A5,$CE,$23,$B5,$FE,$5A,$BF,$65);

  // ---

  // 8 bits
  ID_Boolean : TUMLCType =
    ($32,$52,$92,$D8,$07,$00,$00,$4F,$B5,$37,$F3,$28,$FA,$4F,$D7,$F5);

  ID_TBoolean : TUMLCType =
    ($32,$52,$92,$D8,$07,$00,$00,$4F,$B5,$37,$F3,$28,$FA,$4F,$D7,$F5);

  // 8 bits

  ID_PBoolean : TUMLCType =
    ($3E,$63,$52,$F6,$2A,$56,$03,$48,$9B,$E4,$AF,$10,$AD,$8F,$7A,$F0);


  // 16 bits
  ID_WordBool : TUMLCType =
    ($C7,$24,$90,$7C,$82,$CC,$21,$43,$A7,$2D,$78,$37,$51,$5F,$76,$16);

  ID_TWordBool : TUMLCType =
    ($C7,$24,$90,$7C,$82,$CC,$21,$43,$A7,$2D,$78,$37,$51,$5F,$76,$16);

  // 16 bits

  ID_PWordBool : TUMLCType =
    ($19,$07,$65,$37,$C0,$BA,$6C,$46,$AA,$14,$A7,$08,$B8,$F4,$1C,$45);

  // ---

  ID_Integer : TUMLCType =
    ($03,$BF,$95,$F5,$77,$DF,$58,$41,$9B,$3F,$40,$C4,$2D,$3B,$DC,$A4);

  ID_TInteger : TUMLCType =
    ($03,$BF,$95,$F5,$77,$DF,$58,$41,$9B,$3F,$40,$C4,$2D,$3B,$DC,$A4);

  ID_PInteger : TUMLCType =
    ($2F,$9F,$CD,$CD,$F8,$61,$B6,$40,$84,$72,$9F,$48,$9C,$37,$75,$8A);

  // ---

  // { 8
  ID_ShortInt : TUMLCType =
    ($63,$4E,$9A,$75,$37,$67,$BD,$4E,$B2,$DE,$2C,$E1,$CC,$76,$60,$23);

  ID_Int8 : TUMLCType =
    ($63,$4E,$9A,$75,$37,$67,$BD,$4E,$B2,$DE,$2C,$E1,$CC,$76,$60,$23);

  ID_PShortInt : TUMLCType =
    ($EA,$67,$66,$84,$A2,$82,$EB,$43,$B3,$97,$96,$D5,$35,$67,$03,$6B);

  ID_PInt8 : TUMLCType =
    ($EA,$67,$66,$84,$A2,$82,$EB,$43,$B3,$97,$96,$D5,$35,$67,$03,$6B);
  // } 8

  // { 16
  ID_SmallInt : TUMLCType =
    ($5B,$F0,$91,$6F,$A4,$E7,$0A,$49,$9A,$5E,$1A,$9F,$9E,$B2,$EC,$E7);

  ID_Int16 : TUMLCType =
    ($5B,$F0,$91,$6F,$A4,$E7,$0A,$49,$9A,$5E,$1A,$9F,$9E,$B2,$EC,$E7);

  ID_PSmallInt : TUMLCType =
    ($B0,$9C,$45,$E8,$F2,$79,$96,$40,$BA,$47,$AB,$F4,$6E,$03,$BC,$7B);

  ID_PInt16 : TUMLCType =
    ($B0,$9C,$45,$E8,$F2,$79,$96,$40,$BA,$47,$AB,$F4,$6E,$03,$BC,$7B);
  // } 16

  // { 32
  ID_LongInt : TUMLCType =
    ($A4,$30,$88,$FC,$61,$FE,$20,$41,$BA,$57,$A1,$BA,$DA,$76,$00,$07);

  ID_Int32 : TUMLCType =
    ($A4,$30,$88,$FC,$61,$FE,$20,$41,$BA,$57,$A1,$BA,$DA,$76,$00,$07);

  ID_PLongInt : TUMLCType =
    ($95,$8D,$4F,$5F,$07,$80,$73,$42,$A9,$E8,$85,$9C,$79,$B8,$A0,$BD);

  ID_PInt32 : TUMLCType =
    ($95,$8D,$4F,$5F,$07,$80,$73,$42,$A9,$E8,$85,$9C,$79,$B8,$A0,$BD);
  // } 32

  ID_Int64 : TUMLCType =
    ($4F,$E3,$F2,$CB,$90,$FB,$D7,$4D,$BF,$3F,$92,$58,$86,$3D,$EE,$5A);

  ID_PInt64 : TUMLCType =
    ($F6,$53,$7E,$C1,$A9,$EE,$ED,$48,$A4,$40,$A7,$7A,$1B,$A8,$48,$0C);
  // ---

  ID_Cardinal : TUMLCType =
    ($A0,$3E,$38,$01,$43,$1F,$E0,$4D,$93,$64,$81,$5E,$12,$00,$C9,$BD);

  ID_PCardinal : TUMLCType =
    ($83,$E2,$73,$FF,$1E,$DA,$8F,$4A,$86,$EF,$AA,$EA,$53,$44,$93,$B2);


  ID_Byte : TUMLCType =
    ($52,$EF,$74,$90,$EF,$2D,$0E,$4B,$8E,$AC,$02,$DE,$D6,$1A,$5E,$3A);

  ID_Word : TUMLCType =
    ($89,$57,$34,$FB,$A5,$3B,$1D,$43,$82,$98,$92,$4E,$CB,$FA,$9A,$AB);

  ID_LongWord : TUMLCType =
    ($9C,$E5,$D1,$9C,$97,$07,$13,$4D,$9E,$F7,$C4,$5F,$95,$6F,$2A,$CE);


  ID_PByte : TUMLCType =
    ($89,$ED,$C5,$17,$F4,$5C,$2E,$4D,$BE,$01,$7A,$1C,$EC,$09,$9E,$2D);

  ID_PWord : TUMLCType =
    ($12,$D7,$7F,$99,$54,$5C,$E9,$48,$AE,$F7,$53,$18,$6F,$6E,$EA,$95);

  ID_PLongWord : TUMLCType =
    ($46,$78,$68,$49,$E0,$83,$2E,$42,$93,$4D,$89,$F6,$57,$24,$8A,$7A);

  // ---

  ID_Single : TUMLCType =
    ($75,$A0,$7F,$42,$31,$04,$57,$43,$A8,$2D,$B6,$0A,$57,$DF,$7A,$F7);

  ID_Double : TUMLCType =
    ($53,$DE,$21,$0C,$1E,$13,$10,$48,$B5,$F2,$F9,$1D,$FC,$FB,$F4,$81);

  ID_Extended : TUMLCType =
    ($1D,$D7,$4B,$C7,$18,$FB,$59,$46,$82,$CF,$F7,$33,$FF,$CD,$29,$7F);

  ID_Comp : TUMLCType =
    ($4D,$3B,$F2,$54,$DF,$68,$E9,$48,$A0,$70,$E9,$EB,$F6,$E0,$AA,$73);


  ID_PSingle : TUMLCType =
    ($13,$74,$BD,$63,$A2,$B0,$E3,$4E,$B0,$CE,$AB,$7C,$08,$C0,$35,$06);

  ID_PDouble : TUMLCType =
    ($E7,$4A,$7B,$8A,$F5,$48,$A5,$4E,$A4,$3C,$42,$7F,$B4,$3E,$09,$B9);

  ID_PExtended : TUMLCType =
    ($0A,$A9,$CC,$B0,$48,$AF,$E7,$42,$9A,$0D,$7B,$D8,$16,$75,$C2,$8E);

  ID_PComp : TUMLCType =
    ($99,$2C,$FC,$21,$3D,$09,$3D,$4C,$81,$FF,$B7,$21,$21,$00,$B2,$0E);

  // ---

  ID_Real : TUMLCType =
    ($BA,$90,$07,$11,$58,$85,$0C,$4D,$B2,$05,$AE,$D6,$5E,$0D,$05,$50);

  ID_Real48 : TUMLCType =
    ($36,$11,$48,$FE,$AA,$21,$8B,$4D,$B4,$ED,$C1,$96,$F1,$27,$95,$5D);


  ID_PReal : TUMLCType =
    ($0A,$8B,$55,$97,$FF,$57,$16,$47,$AA,$F5,$47,$59,$CE,$2A,$F8,$FE);

  ID_PReal48 : TUMLCType =
    ($17,$E7,$17,$70,$AB,$25,$19,$44,$89,$74,$E3,$E4,$6D,$25,$C5,$8F);

  // ---

  ID_Currency : TUMLCType =
    ($A6,$56,$28,$78,$D5,$19,$EC,$4C,$81,$6C,$85,$9E,$2F,$AE,$54,$6F);

  ID_PCurrency : TUMLCType =
    ($C3,$A6,$AE,$CC,$EA,$2C,$7B,$47,$97,$6E,$59,$F4,$98,$0F,$BD,$6A);

  // ---

  ID_TDateTime : TUMLCType =
    ($C0,$0F,$07,$C2,$72,$AC,$5B,$45,$BB,$EB,$34,$CC,$05,$2B,$BA,$BF);
  ID_PDateTime : TUMLCType =
    ($7D,$19,$3E,$5F,$EA,$55,$D8,$4B,$A5,$44,$3C,$7A,$08,$9E,$49,$7E);

  ID_PUMLCDate : TUMLCType =
    ($1E,$B7,$4B,$11,$41,$BF,$59,$4E,$87,$FE,$59,$19,$31,$B6,$25,$26);
  ID_PUMLCTime : TUMLCType =
    ($1E,$DF,$DB,$EE,$96,$F4,$6E,$4D,$9D,$D2,$4F,$14,$49,$CC,$46,$D4);
  ID_PUMLCDateTime : TUMLCType =
    ($49,$25,$EA,$B0,$3A,$68,$A3,$47,$89,$D9,$32,$60,$E3,$F7,$34,$F9);

  // ---

  ID_File : TUMLCType =
    ($77,$62,$C6,$6F,$78,$A4,$04,$49,$86,$CD,$71,$78,$BE,$0A,$AF,$16);

  ID_Text : TUMLCType =
    ($E6,$6F,$E9,$4B,$95,$8A,$85,$45,$AE,$7A,$D7,$FB,$35,$6F,$7A,$E6);


  ID_PFile : TUMLCType =
    ($03,$3D,$25,$DB,$EF,$3E,$69,$44,$86,$24,$D1,$89,$D9,$59,$34,$83);

  ID_PText : TUMLCType =
    ($A6,$75,$09,$03,$FF,$20,$3F,$42,$B9,$81,$98,$15,$E5,$73,$CC,$40);

  // ---


  ID_GUID_ : TUMLCType =
    ($E9,$6F,$6A,$9A,$48,$56,$6E,$44,$BD,$F3,$66,$E7,$1C,$67,$2B,$4E);

  ID_Variant : TUMLCType =
    ($25,$6A,$9F,$9C,$DB,$E9,$73,$4B,$B1,$DD,$59,$4B,$DF,$BA,$4A,$B2);

  // ---

  ID_TPoint : TUMLCType =
    ($E6,$CE,$62,$FB,$63,$D4,$E0,$4E,$9A,$12,$F6,$A8,$D8,$B2,$E1,$F2);

  ID_TRect : TUMLCType =
    ($CF,$48,$AA,$4E,$EF,$88,$84,$4B,$9B,$EF,$E0,$40,$CD,$27,$B3,$90);


  ID_PPoint : TUMLCType =
    ($D1,$1D,$BD,$91,$A1,$FA,$89,$4C,$BA,$2E,$47,$A9,$BC,$0A,$82,$D6);

  ID_PRect : TUMLCType =
    ($CC,$80,$7F,$E7,$31,$E1,$11,$4A,$A7,$9E,$CD,$D2,$66,$47,$23,$35);

  // ---

  ID_TObject : TUMLCType =
    ($F9,$49,$10,$30,$F8,$58,$6E,$44,$A7,$5A,$1F,$25,$FA,$4C,$D3,$CE);

  ID_PObject : TUMLCType =
    ($81,$9F,$D2,$AC,$DE,$BC,$9E,$43,$9F,$66,$42,$24,$BA,$7A,$8C,$FA);

implementation

end.

