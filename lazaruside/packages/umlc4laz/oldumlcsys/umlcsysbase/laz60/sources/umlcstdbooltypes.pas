unit umlcstdbooltypes;

{$mode objfpc}{$H+}

interface

uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  dummy;

// ---

const

 MOD_umlcstdbooltypes : TUMLCModule =
   ($E6,$D1,$72,$F9,$85,$01,$15,$47,$AA,$E2,$B7,$39,$42,$34,$45,$0E);

// ---

const

 ID_TBoolean : TUMLCType =
   ($8D,$F1,$09,$DC,$19,$88,$39,$42,$AB,$1C,$15,$7A,$2E,$33,$47,$0E);

 ID_PBoolean : TUMLCType =
   ($CA,$15,$DD,$BA,$E0,$F1,$3A,$4E,$9A,$09,$3F,$F4,$98,$01,$0E,$6F);

 ID_TByteBool : TUMLCType =
    ($07,$13,$53,$21,$D0,$40,$84,$4C,$89,$E4,$3D,$7C,$1C,$BD,$DE,$28);

 ID_PByteBool : TUMLCType =
    ($E1,$7F,$BC,$AF,$BE,$89,$3B,$4C,$9C,$DC,$10,$65,$F6,$18,$37,$C4);

 ID_TWordBool : TUMLCType =
   ($DC,$7A,$19,$71,$42,$1A,$2D,$4B,$8A,$2F,$54,$F0,$E8,$BD,$0B,$5B);

 ID_PWordBool : TUMLCType =
   ($CB,$51,$B6,$3A,$68,$FD,$91,$44,$84,$70,$FB,$31,$7D,$87,$41,$0C);

 ID_TLongBool : TUMLCType =
    ($FE,$62,$B7,$70,$84,$22,$DB,$4C,$84,$7C,$A6,$99,$25,$16,$84,$98);

 ID_PLongBool : TUMLCType =
    ($FC,$A1,$0B,$3D,$CC,$E8,$9A,$41,$92,$D2,$15,$6D,$B1,$12,$46,$E1);

// ---

const

  ID_umlcboolean : TUMLCType =
   ($B9,$7B,$45,$8F,$CF,$D4,$20,$44,$87,$C7,$46,$74,$D0,$A0,$66,$35);

  ID_umlcbytebool : TUMLCType =
   ($52,$16,$C8,$99,$E0,$10,$F0,$4B,$B8,$4D,$CF,$39,$F8,$8F,$F3,$53);

  ID_umlcwordbool : TUMLCType =
   ($5E,$CC,$A0,$0C,$5D,$65,$1D,$45,$B4,$CF,$AD,$0F,$0A,$4B,$27,$61);

  ID_umlclongbool : TUMLCType =
   ($C9,$82,$E4,$BB,$68,$B3,$30,$4D,$83,$FC,$C3,$08,$CE,$CD,$BC,$4A);

  ID_umlcdwordbool : TUMLCType =
   ($C9,$82,$E4,$BB,$68,$B3,$30,$4D,$83,$FC,$C3,$08,$CE,$CD,$BC,$4A);

// ---

const

  ID_umltcboolean : TUMLCType =
   ($B9,$7B,$45,$8F,$CF,$D4,$20,$44,$87,$C7,$46,$74,$D0,$A0,$66,$35);

  ID_umlctbytebool : TUMLCType =
   ($52,$16,$C8,$99,$E0,$10,$F0,$4B,$B8,$4D,$CF,$39,$F8,$8F,$F3,$53);

  ID_umlctwordbool : TUMLCType =
   ($5E,$CC,$A0,$0C,$5D,$65,$1D,$45,$B4,$CF,$AD,$0F,$0A,$4B,$27,$61);

  ID_umlctlongbool : TUMLCType =
   ($C9,$82,$E4,$BB,$68,$B3,$30,$4D,$83,$FC,$C3,$08,$CE,$CD,$BC,$4A);

  ID_umlctdwordbool : TUMLCType =
   ($C9,$82,$E4,$BB,$68,$B3,$30,$4D,$83,$FC,$C3,$08,$CE,$CD,$BC,$4A);

// ---

const

  ID_umlcpboolean : TUMLCType =
   ($CA,$15,$DD,$BA,$E0,$F1,$3A,$4E,$9A,$09,$3F,$F4,$98,$01,$0E,$6F);

  ID_umlcpbytebool : TUMLCType =
    ($E1,$7F,$BC,$AF,$BE,$89,$3B,$4C,$9C,$DC,$10,$65,$F6,$18,$37,$C4);

  ID_umlcpwordbool : TUMLCType =
   ($CB,$51,$B6,$3A,$68,$FD,$91,$44,$84,$70,$FB,$31,$7D,$87,$41,$0C);

  ID_umlcplonglool : TUMLCType =
    ($FC,$A1,$0B,$3D,$CC,$E8,$9A,$41,$92,$D2,$15,$6D,$B1,$12,$46,$E1);

// ---

type
  TBoolean = (* alias of *) Boolean;
  PBoolean = ^TBoolean;

  TByteBoolean = (* alias of *) ByteBool;
  PByteBoolean = ^TByteBoolean;

  TWordBool = (* alias of *) WordBool;
  PWordBool = ^TWordBool;

  TLongBool = (* alias of *) LongBool;
  PLongBool = ^TLongBool;

type
  umlcboolean      = (* alias of *) Boolean;

  umlcbytebool     = (* alias of *) ByteBool;
  umlcwordbool     = (* alias of *) WordBool;

  umlclongwordbool = (* alias of *) LongBool;
  umlcdwordbool    = (* alias of *) LongBool;

type
  umlctboolean      = (* alias of *) umlcboolean;

  umlctbytebool     = (* alias of *) umlcbytebool;
  umlctwordbool     = (* alias of *) umlcwordbool;

  umlctlongwordbool = (* alias of *) umlclongwordbool;
  umlctdwordbool    = (* alias of *) umlcdwordbool;

type
  umlcpboolean      = ^umlctboolean;

  umlcpbytebool     = ^umlctbytebool;
  umlcpwordbool     = ^umlctwordbool;

  umlcplongwordbool = ^umlctlongwordbool;
  umlcpdwordbool    = ^umlctdwordbool;

implementation




end.

