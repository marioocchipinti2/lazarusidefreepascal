(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemory;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement low level memory management operations.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcmemory : TUMLCModule =
   ($D2,$73,$5C,$4A,$19,$E2,$F1,$41,$93,$22,$6A,$D1,$92,$A5,$6C,$8A);

// ---

(* global functions *)

function IsEmpty
  (const ASource: pointer; const ASizeinBytes: Cardinal): Boolean;

function TryCopy
  (var   ADest: pointer;
   const ASource: pointer;
   const ASizeinBytes: Cardinal): Boolean;

function TryCompare
  (out   AComparison: umlccomparison;
   const A: pointer;
   const B: pointer;
   const ASizeinBytes: Cardinal): Boolean;

function Compare
  (const A: pointer;
   const B: pointer;
   const ASizeinBytes: Cardinal): umlccomparison;

(* global procedures *)

procedure Copy
  (var ADest: pointer; const ASource: pointer; const ASizeinBytes: Cardinal);

procedure Clear
  (out ADest: pointer; const ASizeinBytes: Cardinal);

procedure Fill
  (out ADest: pointer; const ASizeinBytes: Cardinal; const AValue: Byte);

implementation

(* global functions *)

function IsEmpty
  (const ASource: pointer; const ASizeinBytes: Cardinal): Boolean;
var P: PByte; Match: Boolean; ACount: Cardinal;
begin
  Result :=
    ((ASource < nil) and (ASizeinBytes > 0));
  if (Result) then
  begin
    P := PByte(ASource);
    ACount := ASizeinBytes;
    Match := true;
    while (Match and (ACount > 0)) do
    begin
      Match :=
        (P^ = 0);

      System.Inc(P);
      System.Dec(ACount);
    end;
  end;
end;

function TryCopy
  (var   ADest: pointer;
   const ASource: pointer;
   const ASizeinBytes: Cardinal): Boolean;
begin
  Result :=
    ((ADest <> nil) and (ASource <> nil) and (ASizeinBytes <> 0));
  if (Result) then
  begin
    System.Move(ASource^, ADest^, ASizeinBytes);
  end;
end;

function ByteCompare
  (const A: Byte;
   const B: Byte): umlccomparison;
begin
  if (A > B)
    then Result := cmpHigher
  else if (A < B)
    then Result := cmpLower
  else Result := cmpEqual;
end;

function TryCompare
  (out   AComparison: umlccomparison;
   const A: pointer;
   const B: pointer;
   const ASizeinBytes: Cardinal): Boolean;
var P, Q: PByte; ACount: Cardinal;
begin
  AComparison := cmpEqual;
  Result :=
    ((A <> nil) and (B <> nil) and (ASizeinBytes > 0));
  if (Result) then
  begin
    P := PByte(A);
    Q := PByte(B);
    ACount := ASizeinBytes;
    repeat
      AComparison :=
        umlcmemory.ByteCompare(P^, Q^);

      System.Inc(P);
      System.Dec(ACount);
    until ((AComparison <> cmpEqual) or (ACount < 1));
  end;
end;

function Compare
  (const A: pointer;
   const B: pointer;
   const ASizeinBytes: Cardinal): umlccomparison;
begin
  Result := cmpError;
  if (not umlcmemory.TryCompare(Result, A, B, ASizeinBytes)) then
  begin
    // raise error
  end;
end;

(* global procedures *)

procedure Copy
  (var ADest: pointer; const ASource: pointer; const ASizeinBytes: Cardinal);
begin
  if (not umlcmemory.TryCopy(ADest, ASource, ASizeinBytes)) then
  begin
    // raise error
  end;
end;

procedure Clear
  (out ADest: pointer; const ASizeinBytes: Cardinal);
begin
  System.FillByte(ADest^, ASizeinBytes, 0);
end;

procedure Fill
  (out ADest: pointer; const ASizeinBytes: Cardinal; const AValue: Byte);
begin
  System.FillByte(ADest^, ASizeinBytes, AValue);
end;

end.

