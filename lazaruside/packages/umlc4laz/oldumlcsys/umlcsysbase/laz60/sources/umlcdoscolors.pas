(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdoscolors;

(* Espanol *)

  // Objetivo: Provee constantes para manejo de colores.

(* English *)

  // Goal: Provides constants for colors management.

interface
uses
{$IFDEF MSWINDOWS}
  Graphics,
{$ENDIF}
{$IFDEF LINUX}
  QGraphics,
{$ENDIF}
  SysUtils, Math,
  umlcmodules, umlctypes,
dummy;

// ---

const

  MOD_umlcdoscolors : TUMLCModule =
   ($7A,$74,$A4,$D0,$CC,$C9,$A2,$43,$87,$0B,$FD,$FB,$45,$4A,$97,$2A);

// ---

type
  doscolor =
  (
  dosclBlack,
  dosclBlue,
  dosclGreen,
  dosclCyan,
  dosclRed,
  dosclMagenta,
  dosclBrown,
  dosclLightGray,
  dosclDarkGray,
  dosclLightBlue,
  dosclLightGreen,
  dosclLightCyan,
  dosclLightRed,
  dosclLightMagenta,
  dosclYellow,
  dosclWhite
  );
  tdoscolor = doscolor;
  pdoscolor = ^tdoscolor;

  doscolorset = set of doscolor;

const
  dosclLow  = dosclBlack;
  dosclHigh = dosclWhite;

  function SafeDOSColor(const AValue: doscolor): doscolor;
  function RandomDOSColor(): doscolor;
  function RandomColor(): TColor;

  function HighDOSColor(const AValue: doscolor): doscolor;
  function LowDOSColor(const AValue: doscolor): doscolor;

  function ColorToDOSColor(const AValue: TColor): doscolor;
  function DOSColorToColor(const AValue: doscolor): TColor;

implementation

const
  DOSColorToColorArray: array[doscolor] of TColor =
   ( clBlack, clBlue,    clGreen,
     clTeal,  clMaroon,  clPurple,
     clOlive, clSilver,  clGray,
     clBlue,  clLime,    clAqua,
     clRed,   clFuchsia, clYellow,
     clWhite );

  {clOlive = clBrown}

function SafeDOSColor(const AValue: doscolor): doscolor;
begin
  Result := AValue;
  if (Result < Low(doscolor))
    then Result := Low(doscolor);
  if (Result > High(doscolor))
    then Result := High(doscolor);
  // Goal: Checks that a color number is valid.
  // Objetivo: Revisa que el numero de color es valido.
end;

function RandomDOSColor(): doscolor;
begin
  Randomize();
  Result := doscolor(Random(Ord(dosclHigh)));
end;

function RandomColor(): TColor;
begin
  Randomize();
  Result := DOSColorToColor(doscolor(Random(Ord(dosclHigh))));
end;

function HighDOSColor(const AValue: doscolor): doscolor;
var I: Integer;
begin
  I := Ord(AValue);
  I := Math.Max(I, I + 8);
  Result := doscolor(I);
end;

function LowDOSColor(const AValue: doscolor): doscolor;
var I: Integer;
begin
  I := Ord(AValue);
  I := Math.Min(I, I - 8);
  I := Math.Max(0, I);
  Result := doscolor(I);
end;

function ColorToDOSColor(const AValue: TColor): doscolor;
var i: doscolor; Found: Boolean;
begin
  i := Low(doscolor); Found := FALSE;
  while ((i <= High(doscolor)) and (not Found)) do
  begin
    Found := (AValue = DOSColorToColorArray[i]);
    Inc(I);
  end;

  if (Found)
    then Result := Pred(i)
    else Result := Low(doscolor);
  // Goal: To cast a "TColor" AValue to a "doscolor" AValue.
  // Objetivo: Convertir un valor "TColor" a un valor "doscolor".
end;

function DOSColorToColor(const AValue: doscolor): TColor;
begin
  Result := DOSColorToColorArray[AValue];
  // Goal: To cast a "doscolor" AValue to a "TColor" AValue.
  // Objetivo: Convertir un valor "doscolor" a un valor "TColor".
end;


end.

