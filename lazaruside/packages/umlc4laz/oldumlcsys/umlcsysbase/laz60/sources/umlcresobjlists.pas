(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcresobjlists;

interface
uses
  dummy;

{$INCLUDE 'umlcsys_language.inc'}

{$IFDEF umlcsys_language_english}
resourcestring
  err_ListIndexError = 'List index out of bounds (%d)';
{$ENDIF}

{$IFDEF umlcsys_language_spanisheurope}
resourcestring
  err_ListIndexError = 'Indice de Lista fuera de rango (%d)';
{$ENDIF}

{$IFDEF umlcsys_language_spanishlatam}
resourcestring
  err_ListIndexError = 'Indice de Lista fuera de rango (%d)';
{$ENDIF}

implementation

end.
