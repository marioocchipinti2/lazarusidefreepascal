(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcreclists;

interface
uses 
  Classes,
  umlcmodules, umlctypes,
  umlcobjlists,
  dummy;

// ---

const

  MOD_umlcreclists : TUMLCModule =
    ($4F,$7A,$00,$6A,$5F,$05,$F9,$43,$97,$1A,$92,$2E,$B3,$80,$3E,$39);

// ---

const

  // ---

  ID_TUMLCRecordList : TUMLCType =
    ($36,$7F,$B9,$E1,$86,$28,$10,$40,$81,$EC,$16,$26,$07,$BC,$7B,$11);

  // ---

type

{ TUMLCRecordList }

  TUMLCRecordList = class(TUMLCOwnerList)
  private
    { Private declarations }

    FRecordSize: Integer;
  protected
    { Protected declarations }

    function getItems
      (Index: Integer): pointer;
    function getRecordSize(): Integer;

    procedure setItems
      (Index: Integer; Item: pointer);
    procedure setRecordSize
      (const Value: Integer);
  public
    { Public declarations }

    constructor Create(); override;
  public
    { Public declarations }

    function InternalCreateItem(): pointer; virtual;
    procedure InternalDestroyItem
      (const Item: pointer); virtual;

    function IndexOf
      (const Item: pointer): Integer;

    function First(): pointer;
    function Last(): pointer;

    function InsertFirst(): pointer;
    function InsertLast(): pointer;

    function Extract
      (const Index: Integer): pointer;
    function Remove
      (const Item: pointer): Integer;

    function FirstThat
      (const Func: TUMLCPointerListFirstThatFunc; const Param: pointer): pointer;

    procedure ForEach
      (const Proc: TUMLCPointerListForEachProc; const Param: pointer);
    procedure ForBack
      (const Proc: TUMLCPointerListForEachProc; const Param: pointer);
  public
    { Public declarations }

    property Items[Index: Integer]: pointer
      read getItems write setItems; default;
    property RecordSize: Integer
      read getRecordSize write setRecordSize;
  end;

implementation

{ TUMLCRecordList }

function TUMLCRecordList.getItems
  (Index: Integer): pointer;
begin
  Result := getInternalItems(Index);
end;

function TUMLCRecordList.getRecordSize(): Integer;
begin
  Result := FRecordSize;
end;

procedure TUMLCRecordList.setItems
  (Index: Integer; Item: pointer);
begin
  setInternalItems(Index, Item);
end;

procedure TUMLCRecordList.setRecordSize
  (const Value: Integer);
begin
  FRecordSize := Value;
end;

constructor TUMLCRecordList.Create();
begin
  inherited Create();
  FRecordSize := 0;
end;

function TUMLCRecordList.InternalCreateItem(): pointer;
begin
  System.GetMem(Result, FRecordSize);
  System.FillChar(Result^, FRecordSize, 0);
  // Goal: To create a new record of the previous defined type.
  // Objetivo: Crear un nuevo registro del tipo definido previamente.
end;

procedure TUMLCRecordList.InternalDestroyItem
  (const Item: pointer);
begin
  System.FreeMem(Item, FRecordSize);
  // Goal: To destroy a given record of the previous defined type.
  // Objetivo: Destruir un registro dado del tipo definido previamente.
end;

function TUMLCRecordList.IndexOf
  (const Item: Pointer): Integer;
begin
  Result := InternalIndexOf(Item);
end;

function TUMLCRecordList.First(): Pointer;
begin
  Result := InternalFirst;
end;

function TUMLCRecordList.Last(): Pointer;
begin
  Result := InternalLast;
end;

function TUMLCRecordList.InsertFirst(): pointer;
begin
  Result :=
    InternalCreateItem();
  InternalInsertFirst(Result);
end;

function TUMLCRecordList.InsertLast(): pointer;
begin
  Result :=
    InternalCreateItem();
  InternalInsertLast(Result);
end;

function TUMLCRecordList.Extract
(const Index: Integer): pointer;
begin
  Result := Items[Index];
  DeleteAt(Index);
  // Goal: Removes & returns an item of the list without destroying it.
  // Objetivo: Remueve y regresa un elemento de la lista sin destruirlo.
end;

function TUMLCRecordList.Remove(const Item: pointer): Integer;
begin
  Result := IndexOf(Item);
  if (Result <> IndexNotFound)
    then DeleteAt(Result);
  // Goal: Destroys of the list.
  // Objetivo: Destruye un elemento de la lista.
end;

function TUMLCRecordList.FirstThat(const Func: TUMLCPointerListFirstThatFunc;
  const Param: pointer): pointer;
var Found: Boolean; Index, LastIndex: Integer; Item: pointer;
begin
  Result := nil;

  Found := false;
  Index := 0;
  LastIndex  := Count;
  while (not Found and (Index < LastIndex)) do
  begin
    Item := getInternalItems(Index);
    Found := Func(Item, Param);
    Inc(Index);
  end;

  if (Found) then
  begin
    Result := Item;
  end;
end;

procedure TUMLCRecordList.ForEach(const Proc: TUMLCPointerListForEachProc; const Param: pointer);
var Index: Integer; Item: pointer;
begin
  for Index := 0 to Pred(Count) do
  begin
    Item := getInternalItems(Index);
    Proc(Item, Param);
  end;
end;

procedure TUMLCRecordList.ForBack(const Proc: TUMLCPointerListForEachProc; const Param: pointer);
var Index: Integer; Item: pointer;
begin
  for Index := Pred(Count) downto 0 do
  begin
    Item := getInternalItems(Index);
    Proc(Item, Param);
  end;
end;

end.
