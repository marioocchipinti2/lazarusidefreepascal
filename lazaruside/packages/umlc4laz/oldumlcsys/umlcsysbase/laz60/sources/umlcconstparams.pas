unit umlcconstparams;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcconstparams : TUMLCModule =
   ($4E,$E9,$A4,$40,$7E,$B8,$E0,$4C,$AE,$AF,$99,$A8,$88,$FA,$01,$5F);

// ---

(* global types *)

type
  umlcconstantparameters = (* redefine *) type pointer;

type
  umlctconstantparametersarray = array[0..65535] of pointer;
  umlcpconstantparametersarray = ^umlctconstantparametersarray;

type
  umlcconstantparametersheader = record
    // how many items can be stored
    ItemsMaxCount:     Cardinal;

    // how many items are currently stored
    ItemsCurrCount: Cardinal;

    // ...
  end;
  umlctconstantparametersheader = (* alias of *) umlcconstantparametersheader;
  umlcpconstantparametersheader = ^umlcconstantparametersheader;

type
  umlcconstantparametersrecord = record
    Header: umlctconstantparametersheader;
    Items:  umlcpconstantparametersarray;
  end;
  umlctconstantparametersrecord = (* alias of *) umlcconstantparametersrecord;
  umlcpconstantparametersrecord = ^umlcconstantparametersrecord;

(* global functions *)

function paramsreadmaxcount
  (const AParams: umlcconstantparameters): Cardinal;

function paramsreadcurrcount
  (const AParams: umlcconstantparameters): Cardinal;

function paramsreadparamat
  (const AParams: umlcconstantparameters;
   const AIndex:  Cardinal): pointer;

function paramscreate
  (const AMaxCount: Cardinal): umlcconstantparameters;

(* global procedures *)

procedure addparam
  (var   AParams: umlcconstantparameters;
   const AParam:  pointer);

procedure paramsdrop
  (var AParams: umlcconstantparameters);

implementation

(* global functions *)

function paramsreadmaxcount
  (const AParams: umlcconstantparameters): Cardinal;
var AParamsRecord: umlcpconstantparametersrecord;
begin
  AParamsRecord :=
    umlcpconstantparametersrecord(AParams);

  Result :=
    AParamsRecord^.Header.ItemsMaxCount;
end;

function paramsreadcurrcount
  (const AParams: umlcconstantparameters): Cardinal;
var AParamsRecord: umlcpconstantparametersrecord;
begin
  AParamsRecord :=
    umlcpconstantparametersrecord(AParams);

  Result :=
    AParamsRecord^.Header.ItemsCurrCount;
end;

function paramsreadparamat
  (const AParams: umlcconstantparameters;
   const AIndex:  Cardinal): pointer;
var AParamsRecord: umlcpconstantparametersrecord;
    CanContinue: Boolean;
begin
  AParamsRecord :=
    umlcpconstantparametersrecord(AParams);

  CanContinue :=
    (AIndex < AParamsRecord^.Header.ItemsMaxCount);
  if (CanContinue) then
  begin
    Result :=
      AParamsRecord^.Items^[AIndex];
  end;
end;

function paramscreate
  (const AMaxCount: Cardinal): umlcconstantparameters;
var AParamsRecord: umlcpconstantparametersrecord;
    AItemsSize:    Cardinal;
begin
  Result := nil;

  // allocate full record
  System.GetMem
    (AParamsRecord, sizeof(umlctconstantparametersrecord));

  AItemsSize :=
    (AMaxCount * sizeof(pointer));

  // add final count
  AParamsRecord^.Header.ItemsMaxCount :=
    AMaxCount;

  AParamsRecord^.Header.ItemsCurrCount :=
    0;

  // allocate items
  System.GetMem
    (AParamsRecord^.Items, AItemsSize);

  // clear items
  System.FillByte
    (AParamsRecord^.Items^, AItemsSize, 0);

  Result :=
    AParamsRecord;
end;

(* global procedures *)

procedure addparam
  (var   AParams: umlcconstantparameters;
   const AParam:  pointer);
var AParamsRecord: umlcpconstantparametersrecord;
    ACurrCount:    Cardinal;
begin
  AParamsRecord :=
    umlcpconstantparametersrecord(AParams);

  ACurrCount :=
    AParamsRecord^.Header.ItemsCurrCount;
  if (ACurrCount < AParamsRecord^.Header.ItemsMaxCount) then
  begin
    AParamsRecord^.Items^[ACurrCount] :=
      AParam;
    AParamsRecord^.Header.ItemsCurrCount :=
      Succ(ACurrCount);
  end;
end;

procedure paramsdrop
  (var AParams: umlcconstantparameters);
var AParamsRecord: umlcpconstantparametersrecord;
    AItemsSize:    Cardinal;
begin
  if (AParams <> nil) then
  begin
    AParamsRecord :=
      umlcpconstantparametersrecord(AParams);

    // Items are removed separatly

    AItemsSize :=
      (AParamsRecord^.Header.ItemsMaxCount * sizeof(pointer));

    // deallocate items
    System.FreeMem
      (AParamsRecord^.Items, AItemsSize);

    // deallocate full record
    System.FreeMem
      (AParamsRecord, sizeof(umlctconstantparametersrecord));
  end;
end;


end.

