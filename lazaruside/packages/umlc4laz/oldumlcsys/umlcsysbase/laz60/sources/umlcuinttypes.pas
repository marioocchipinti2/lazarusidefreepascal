(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuinttypes;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement math oriented unsigned integer types.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcuinttypes : TUMLCModule =
   ($D6,$3F,$05,$74,$85,$38,$3B,$4F,$84,$B3,$79,$8B,$F0,$EE,$D1,$C1);

// ---

const

// ---

  ID_umlcuint_8 : TUMLCType =
    ($C6,$D3,$6B,$5E,$E6,$C1,$B5,$4B,$95,$8E,$65,$17,$2E,$CB,$A7,$7B);

  ID_umlcuint_16 : TUMLCType =
    ($A7,$4A,$C9,$1C,$9E,$C8,$7D,$43,$B3,$D3,$CF,$46,$61,$59,$C2,$BE);

  ID_umlcuint_32 : TUMLCType =
    ($09,$40,$CD,$5D,$65,$BB,$1A,$4C,$B4,$41,$2C,$C4,$F6,$22,$73,$63);

  ID_umlcuint_64 : TUMLCType =
    ($5A,$D3,$CE,$AF,$7D,$EF,$B1,$49,$BB,$91,$B3,$E5,$93,$3E,$D7,$A5);

  ID_umlcuint_128 : TUMLCType =
    ($7D,$BC,$90,$EF,$85,$17,$BC,$4E,$AF,$55,$BF,$60,$20,$8B,$CC,$F2);

  ID_umlcuint_256 : TUMLCType =
    ($70,$9F,$C8,$49,$8A,$8A,$89,$41,$8E,$3C,$BF,$09,$84,$77,$B0,$4F);

  ID_umlcuint_512 : TUMLCType =
    ($2E,$C7,$F1,$96,$49,$52,$F6,$41,$B1,$72,$C4,$E9,$10,$95,$80,$11);

// ---

const

// ---

  ID_TUMLCUnsignedIntegerTypes : TUMLCType =
    ($FE,$EE,$37,$A3,$16,$13,$6F,$49,$87,$45,$AB,$71,$38,$16,$47,$8C);

// ---

const

// ---

  ID_umlctuint_8 : TUMLCType =
    ($C6,$D3,$6B,$5E,$E6,$C1,$B5,$4B,$95,$8E,$65,$17,$2E,$CB,$A7,$7B);

  ID_umlctuint_16 : TUMLCType =
    ($A7,$4A,$C9,$1C,$9E,$C8,$7D,$43,$B3,$D3,$CF,$46,$61,$59,$C2,$BE);

  ID_umlctuint_32 : TUMLCType =
    ($09,$40,$CD,$5D,$65,$BB,$1A,$4C,$B4,$41,$2C,$C4,$F6,$22,$73,$63);

  ID_umlctuint_64 : TUMLCType =
    ($5A,$D3,$CE,$AF,$7D,$EF,$B1,$49,$BB,$91,$B3,$E5,$93,$3E,$D7,$A5);

  ID_umlctuint_128 : TUMLCType =
    ($7D,$BC,$90,$EF,$85,$17,$BC,$4E,$AF,$55,$BF,$60,$20,$8B,$CC,$F2);

  ID_umlctuint_256 : TUMLCType =
    ($70,$9F,$C8,$49,$8A,$8A,$89,$41,$8E,$3C,$BF,$09,$84,$77,$B0,$4F);

  ID_umlctuint_512 : TUMLCType =
    ($2E,$C7,$F1,$96,$49,$52,$F6,$41,$B1,$72,$C4,$E9,$10,$95,$80,$11);

// ---

const

 ID_umlcpuint_8 : TUMLCType =
   ($38,$FC,$A1,$76,$0D,$B2,$D2,$43,$A1,$2A,$00,$8E,$51,$BB,$FE,$ED);

 ID_umlcpuint_16 : TUMLCType =
   ($FF,$C8,$40,$64,$90,$57,$C6,$48,$9A,$B0,$89,$A6,$85,$1C,$FA,$69);

 ID_umlcpuint_32 : TUMLCType =
   ($11,$02,$93,$CB,$1A,$2E,$8C,$4B,$82,$A8,$B8,$29,$92,$DA,$4B,$4C);

 ID_umlcpuint_64 : TUMLCType =
   ($C0,$9D,$C8,$DE,$37,$91,$80,$4B,$B2,$D7,$95,$DD,$D1,$14,$FC,$6B);

 ID_umlcpuint_128 : TUMLCType =
   ($F3,$22,$00,$AB,$AA,$6B,$85,$4A,$B9,$AD,$6C,$08,$1F,$21,$20,$7F);

 ID_umlcpuint_256 : TUMLCType =
   ($15,$06,$FE,$FA,$8F,$CA,$30,$4A,$8A,$CA,$F5,$C5,$74,$24,$91,$77);

 ID_umlcpuint_512 : TUMLCType =
   ($2F,$86,$6C,$7D,$16,$88,$41,$44,$BD,$3B,$0D,$C8,$B2,$8F,$25,$AA);

// ---

(* global standard types *)

type

 umlcuint_8    = (* alias of *) byte;
 umlcuint_16   = (* alias of *) word; // array[0..1] of byte;
 umlcuint_32   = (* alias of *) longword; // array[0..3] of byte;
 umlcuint_64   = (* alias of *) qword; // packed record Value: array[0..7] of byte; end;

 umlcuint_128  = (* alias of *) packed record Value: array[0..15] of byte; end;
 umlcuint_256  = (* alias of *) packed record Value: array[0..31] of byte; end;
 umlcuint_512  = (* alias of *) packed record Value: array[0..63] of byte; end;

(* global additional types *)

type

  TUMLCUnsignedIntegerTypes = (* enum of *)
  (
    umlctypuint_Unknown,
    umlctypuint_8,
    umlctypuint_16,
    umlctypuint_32,
    umlctypuint_64,
    umlctypuint_128,
    umlctypuint_256,
    umlctypuint_512
  );

type

  umlctuint_8    = (* alias of *) umlcuint_8;
  umlctuint_16   = (* alias of *) umlcuint_16;
  umlctuint_32   = (* alias of *) umlcuint_32;
  umlctuint_64   = (* alias of *) umlcuint_64;
  umlctuint_128  = (* alias of *) umlcuint_128;
  umlctuint_256  = (* alias of *) umlcuint_256;
  umlctuint_512  = (* alias of *) umlcuint_512;

type

  umlcpuint_8    = ^umlcuint_8;
  umlcpuint_16   = ^umlcuint_16;
  umlcpuint_32   = ^umlcuint_32;
  umlcpuint_64   = ^umlcuint_64;
  umlcpuint_128  = ^umlcuint_128;
  umlcpuint_256  = ^umlcuint_256;
  umlcpuint_512  = ^umlcuint_512;

(* global functions *)

function SizeInBytesOf
  (const AUnsignedIntegerType: TUMLCUnsignedIntegerTypes): Cardinal;

implementation

(* global functions *)

function SizeInBytesOf
  (const AUnsignedIntegerType: TUMLCUnsignedIntegerTypes): Cardinal;
begin
  //umlctypuint_Unknown:
  Result := 0;

  case AUnsignedIntegerType of
    umlctypuint_8:
      Result := sizeof(umlcuint_8);
    umlctypuint_16:
      Result := sizeof(umlcuint_16);
    umlctypuint_32:
      Result := sizeof(umlcuint_32);
    umlctypuint_64:
      Result := sizeof(umlcuint_64);
    umlctypuint_128:
      Result := sizeof(umlcuint_128);
    umlctypuint_256:
      Result := sizeof(umlcuint_256);
    umlctypuint_512:
      Result := sizeof(umlcuint_512);
  end;
end;



end.

