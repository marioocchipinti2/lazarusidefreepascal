(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcrgbs;

interface
uses
  Graphics,
  umlcmodules, umlctypes,
  umlcstdtypes,
  dummy;

// ---

const

 MOD_umlcrgbs : TUMLCModule =
   ($84,$A2,$5C,$EF,$81,$A4,$22,$4F,$AE,$CA,$1A,$85,$0F,$4E,$0F,$49);

// ---

const

  // ---

  ID_TUMLCARGBRecord : TUMLCType =
    ($EB,$F8,$A7,$44,$77,$51,$AA,$45,$A7,$DB,$71,$3D,$71,$16,$8B,$6F);
  ID_PUMLCARGBRecord : TUMLCType =
    ($62,$9E,$EF,$64,$30,$50,$F8,$4D,$B1,$07,$8F,$94,$AA,$FF,$21,$BE);

  // ---

  ID_TUMLCRGBRecord : TUMLCType =
    ($9F,$95,$B3,$58,$37,$CA,$C7,$48,$AB,$5D,$F3,$F9,$D7,$D3,$66,$93);
  ID_PUMLCRGBRecord : TUMLCType =
    ($19,$95,$54,$DF,$CF,$33,$CE,$4B,$98,$C0,$AA,$C7,$42,$49,$71,$15);

  // ---

  ID_TUMLCARGB : TUMLCType =
    ($B7,$26,$7C,$D7,$A2,$0E,$B8,$4D,$BA,$78,$D5,$94,$E9,$22,$B8,$11);
  ID_PUMLCARGB : TUMLCType =
    ($36,$08,$2A,$41,$83,$00,$3C,$43,$A5,$55,$88,$36,$B0,$FB,$8A,$0F);

  // ---

  ID_TUMLCRGB : TUMLCType =
    ($FF,$93,$34,$17,$22,$80,$55,$49,$93,$05,$DF,$31,$CD,$9B,$8C,$85);
  ID_PUMLCRGB : TUMLCType =
    ($5B,$B0,$C2,$3C,$0A,$39,$6F,$4B,$B1,$7F,$AF,$2C,$9A,$5A,$A1,$F8);

  // ---

type

  /// <summary>
  /// Clase para manejo de funciones para colores,
  /// en el formato Alpha (transparencia) + RGB.
  /// </summary>
  TUMLCARGBRecord = record
    Alpha: Byte;
    Red:   Byte;
    Green: Byte;
    Blue:  Byte;
  end;
  PUMLCARGBRecord = ^TUMLCARGBRecord;

  TUMLCRGBRecord = record
    Red:   Byte;
    Green: Byte;
    Blue:  Byte;
  end;
  PUMLCRGBRecord = ^TUMLCARGBRecord;

  TUMLCRGB  = LongInt;
  PUMLCRGB  = ^TUMLCRGB;

  TUMLCARGB = LongInt;
  PUMLCARGB = ^TUMLCARGB;

  procedure EncodeARGBRec
    (var ADest: TUMLCARGBRecord; const A, R, G, B: Byte);

  procedure DecodeARGBRec
    (const ADest: TUMLCARGBRecord; var A, R, G, B: Byte);

  procedure EncodeRGBRec
    (var ADest: TUMLCRGBRecord; const R, G, B: Byte);

  procedure DecodeRGBRec
    (const ADest: TUMLCRGBRecord; var R, G, B: Byte);

  function ColorToRGB(const AColor: Graphics.TColor): TUMLCRGB;

implementation

procedure EncodeARGBRec
  (var ADest: TUMLCARGBRecord; const A, R, G, B: Byte);
begin
  ADest.Alpha := A;
  ADest.Red   := R;
  ADest.Green := G;
  ADest.Blue  := B;
end;

procedure DecodeARGBRec
  (const ADest: TUMLCARGBRecord; var A, R, G, B: Byte);
begin
  A := ADest.Alpha;
  R := ADest.Red;
  G := ADest.Green;
  B := ADest.Blue;
end;

procedure EncodeRGBRec
  (var ADest: TUMLCRGBRecord; const R, G, B: Byte);
begin
  ADest.Red   := R;
  ADest.Green := G;
  ADest.Blue  := B;
end;

procedure DecodeRGBRec
  (const ADest: TUMLCRGBRecord; var R, G, B: Byte);
begin
  R := ADest.Red;
  G := ADest.Green;
  B := ADest.Blue;
end;

function ColorToRGB(const AColor: Graphics.TColor): TUMLCRGB;
begin
  Result := Graphics.ColorToRGB(AColor);
end;

function RGBToColor(const ARGB: TUMLCRGB): TColor;
begin
  //@ to-do:
  Result := ARGB;
end;

function EncodeRGB(const R, G, B: Byte): TUMLCRGB;
begin
  Result :=0;
end;

//function RGBToColor(R, G, B: Byte): TColor;

end.
