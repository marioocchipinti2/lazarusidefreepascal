unit umlctreesregister;

interface
uses
  Classes,
{$IFDEF FPC}  
  LResources,
{$ENDIF}
  umlctreecntrs,
  umlcmsgtreecntrs,
  dummy;

  procedure Register;

implementation

{$IFDEF DELPHI}
{$R 'umlctreecntrs.dcr'}
{$R 'umlcmsgtreecntrs.dcr'}
{$ENDIF} 

procedure Register;
const
  TreePalette = 'UMLCat Trees';
begin
  RegisterComponents(TreePalette, [TUMLCTreeContainer]);
  RegisterComponents(TreePalette, [TUMLCMsgTreeContainer]);
end;

initialization
{$IFDEF FPC} 
{$I 'umlctreecntrs.lrs'}
{$I 'umlcmsgtreecntrs.lrs'}
{$ENDIF} 
end.



