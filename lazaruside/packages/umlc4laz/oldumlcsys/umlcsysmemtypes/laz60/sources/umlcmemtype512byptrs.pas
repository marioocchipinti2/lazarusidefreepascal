(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemtype512byptrs;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 512 bits segments' pointers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmeminttypes,
  dummy;

// ---

const

 MOD_umlcmemtype512byptrs : TUMLCModule =
    ($AD,$27,$F1,$C3,$0C,$D0,$88,$40,$A1,$AD,$71,$DF,$78,$AF,$FF,$52);

 // ---

(* global functions *)

function ConstToPtr
  (const AValue: umlcmemint_512): pointer;

procedure DropPtr
  (var ADestPtr: pointer);



 (* global procedures *)




 (* global operators *)

   procedure Assign
     (out   ADest:   pointer;
      const ASource: pointer); overload; // operator :=

   function Compare
     (const A, B: pointer): umlctcomparison;

   function Equal
     (const A, B: pointer): Boolean; overload; // operator =
   function Different
     (const A, B: pointer): Boolean; overload; // operator <>
   function Greater
     (const A, B: pointer): Boolean; overload; // operator >
   function Lesser
     (const A, B: pointer): Boolean; overload; // operator <
   function GreaterEqual
     (const A, B: pointer): Boolean; overload; // operator >=
   function LesserEqual
     (const A, B: pointer): Boolean; overload; // operator <=

implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcmemint_512): pointer;
var P: umlcpmemint_512;
begin
  System.GetMem(P, sizeof(umlcmemint_512));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcmemint_512));
end;



(* global procedures *)





(* global operators *)

procedure Assign
  (out   ADest:   pointer;
   const ASource: pointer);
begin
  System.Move(ASource^, ADest^, sizeof(umlcmemint_512));
end;

function Compare
  (const A, B: pointer): umlccomparison;
var C, D: umlcpmemint_512;
begin
  C := umlcpmemint_512(A);
  D := umlcpmemint_512(B);
  //if (C^ = D^)
  //  then Result := cmpEqual
  //else if (C^ < D^)
  //  then Result := cmpLower
  //else Result := cmpHigher;
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemint_512));
end;

function Equal
  (const A, B: pointer): Boolean;
var C, D: umlcpmemint_512;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemint_512(A);
  D := umlcpmemint_512(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemint_512));
  Result :=
    (ErrorCode = cmpEqual);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Different
  (const A, B: pointer): Boolean;
var C, D: umlcpmemint_512;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemint_512(A);
  D := umlcpmemint_512(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemint_512));
  Result :=
    (ErrorCode <> cmpEqual);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Greater
  (const A, B: pointer): Boolean;
var C, D: umlcpmemint_512;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemint_512(A);
  D := umlcpmemint_512(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemint_512));
  Result :=
    (ErrorCode <> cmpHigher);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: pointer): Boolean;
var C, D: umlcpmemint_512;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemint_512(A);
  D := umlcpmemint_512(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemint_512));
  Result :=
    (ErrorCode <> cmpLower);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: pointer): Boolean;
var C, D: umlcpmemint_512;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemint_512(A);
  D := umlcpmemint_512(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemint_512));
  Result :=
  (ErrorCode = cmpHigher) or (ErrorCode = cmpEqual);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: pointer): Boolean;
var C, D: umlcpmemint_512;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemint_512(A);
  D := umlcpmemint_512(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemint_512));
  Result :=
    (ErrorCode = cmpLower) or (ErrorCode = cmpEqual);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;



end.

