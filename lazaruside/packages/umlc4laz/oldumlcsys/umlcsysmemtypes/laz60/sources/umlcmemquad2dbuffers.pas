(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemquad2Dbuffers;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 2D quad segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  umlcmemquads,
  dummy;
  
// ---

const

  MOD_umlcmemquad2Dbuffers : TUMLCModule =
    ($1D,$08,$25,$61,$2B,$C6,$84,$45,$B1,$29,$00,$C1,$66,$2C,$F9,$CB);

// ---

implementation

end.

