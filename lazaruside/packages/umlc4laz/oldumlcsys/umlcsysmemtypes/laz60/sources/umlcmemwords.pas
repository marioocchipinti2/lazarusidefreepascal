(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemwords;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in words segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  umlcmemtype16s,
  dummy;
  
// ---

const

  MOD_umlcmemwords : TUMLCModule =
   ($3E,$F4,$C8,$DD,$25,$3A,$7D,$46,$B1,$C8,$43,$9D,$F4,$04,$B8,$D3);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcmemword): Boolean; overload;

(* global procedures *)

  procedure Clear
    (out ADest: umlcmemword); overload;




implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcmemword): Boolean;
begin
  Result :=
    umlcmemtype16s.IsEmpty(ASource);
end;



(* global procedures *)

procedure Clear
  (out ADest: umlcmemword);
begin
  umlcmemtype16s.Clear(ADest)
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;







end.

