(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemtype16byptrs;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 16 bits segments' pointers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  dummy;

// ---

const

 MOD_umlcmemtype16byptrs : TUMLCModule =
  ($F6,$EA,$57,$AA,$58,$F1,$40,$48,$82,$89,$D0,$0B,$C0,$64,$BC,$7D);

 // ---

(* global functions *)

function ConstToPtr
  (const AValue: umlcmemtype_16): pointer;

procedure DropPtr
  (var ADestPtr: pointer);


  function IsEmpty
   (const ASource: pointer): Boolean; overload;

(* global procedures *)

  procedure Clear
   (out ADest: pointer); overload;





(* global operators *)

  procedure Assign
    (out   ADest:   pointer;
     const ASource: pointer); overload; // operator :=

  function Compare
    (const A, B: pointer): umlctcomparison;

  function Equal
    (const A, B: pointer): Boolean; overload; // operator =
  function Different
    (const A, B: pointer): Boolean; overload; // operator <>

  function Greater
    (const A, B: pointer): Boolean; overload; // operator >
  function Lesser
    (const A, B: pointer): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: pointer): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: pointer): Boolean; overload; // operator <=







implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcmemtype_16): pointer;
var P: umlcpmemtype_16;
begin
  System.GetMem(P, sizeof(umlcmemtype_16));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcmemtype_16));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var C: umlcpmemtype_16;
begin
  C := umlcpmemtype_16(ASource);
  Result :=
    (C^ = 0);
end;


(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(pointer), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;


(* global operators *)

procedure Assign
  (out   ADest:   pointer;
   const ASource: pointer);
begin
  System.Move(ASource^, ADest^, sizeof(umlcmemtype_16));
end;

function Compare
  (const A, B: pointer): umlccomparison;
var C, D: umlcpmemtype_16;
begin
  C := umlcpmemtype_16(A);
  D := umlcpmemtype_16(B);
  if (C^ = D^)
    then Result := cmpEqual
  else if (C^ < D^)
    then Result := cmpLower
  else Result := cmpHigher;
end;

function Equal
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_16;
begin
  C := umlcpmemtype_16(A);
  D := umlcpmemtype_16(B);
  Result :=
    (C^ = D^);
end;

function Different
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_16;
begin
  C := umlcpmemtype_16(A);
  D := umlcpmemtype_16(B);
  Result :=
    (C^ <> D^);
end;

function Greater
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_16;
begin
  C := umlcpmemtype_16(A);
  D := umlcpmemtype_16(B);
  Result :=
    (C^ > D^);
end;

function Lesser
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_16;
begin
  C := umlcpmemtype_16(A);
  D := umlcpmemtype_16(B);
  Result :=
    (C^ < D^);
end;

function GreaterEqual
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_16;
begin
  C := umlcpmemtype_16(A);
  D := umlcpmemtype_16(B);
  Result :=
    (C^ >= D^);
end;

function LesserEqual
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_16;
begin
  C := umlcpmemtype_16(A);
  D := umlcpmemtype_16(B);
  Result :=
    (C^ <= D^);
end;




end.

