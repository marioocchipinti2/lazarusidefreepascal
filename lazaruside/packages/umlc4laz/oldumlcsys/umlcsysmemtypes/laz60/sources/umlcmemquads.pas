(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemquads;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in quads segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  umlcmemtype32s,
  dummy;
  
// ---

const

  MOD_umlcmemquads : TUMLCModule =
   ($45,$4D,$83,$03,$E0,$02,$85,$44,$AE,$3A,$71,$E7,$B9,$FE,$00,$8A);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcmemquad): Boolean; overload;

(* global procedures *)

  procedure Clear
    (out ADest: umlcmemquad); overload;



implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcmemquad): Boolean;
begin
  Result :=
    umlcmemtype32s.IsEmpty(ASource);
end;



(* global procedures *)

procedure Clear
  (out ADest: umlcmemquad);
begin
  umlcmemtype32s.Clear(ADest)
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;




end.

