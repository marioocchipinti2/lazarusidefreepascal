(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemtype128s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 128 bits segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcdebug,
  dummy;

// ---

const

 MOD_umlcmemtype128s : TUMLCModule =
  ($EB,$62,$15,$D2,$63,$C6,$04,$42,$96,$47,$6C,$7B,$6B,$3F,$15,$9A);

// ---

(* "namespaced" redefinitions *)

type

 umlcmemtype128s__umlcmemtype_128 = (* alias of *) umlcmemtype_128;

(* global functions *)

function IsEmpty
  (const ASource: umlcmemtype_128): Boolean; overload;

procedure Min
  (out   ADest: umlcmemtype_128;
   const A:     umlcmemtype_128;
   const B:     umlcmemtype_128); overload;

procedure Max
  (out   ADest: umlcmemtype_128;
   const A:     umlcmemtype_128;
   const B:     umlcmemtype_128); overload;

(* global procedures *)

procedure Clear
  (out ADest: umlcmemtype_128); overload;

procedure Encode64To128
  (out   ADest: umlcmemtype_128;
   const ALo:   umlcmemtype_64;
   const AHi:   umlcmemtype_64);

procedure Decode128To64
  (out   ALo:     umlcmemtype_64;
   out   AHi:     umlcmemtype_64;
   const ASource: umlcmemtype_128);

procedure AssignMem8
  (out   ADest:   umlcmemtype_128;
   const ASource: umlcmemtype_8);

procedure AssignMem16
  (out   ADest:   umlcmemtype_128;
   const ASource: umlcmemtype_16);

procedure AssignMem32
  (out   ADest:   umlcmemtype_128;
   const ASource: umlcmemtype_32);

procedure AssignMem64
  (out   ADest:   umlcmemtype_128;
   const ASource: umlcmemtype_64);

(* global operators *)

procedure Assign
  (out   ADest:   umlcmemtype_128;
   const ASource: umlcmemtype_128); overload; // operator :=

function Equal
  (const A, B: umlcmemtype_128): Boolean; overload; // operator =
function Different
  (const A, B: umlcmemtype_128): Boolean; overload; // operator <>

function NotOp
  (const AValue: umlcmemtype_128): umlcmemtype_128; overload; // operator not
function NegTwoOp
  (const AValue: umlcmemtype_128): umlcmemtype_128; overload; // operator negtwo

function ShlOp
  (const AValue: umlcmemtype_128;
   const AShift: umlcmemtype_128): umlcmemtype_128; overload; // operator xor
function ShrOp
  (const AValue: umlcmemtype_128;
   const AShift: umlcmemtype_128): umlcmemtype_128; overload; // operator xor

function AndOp
  (const A, B: umlcmemtype_128): umlcmemtype_128; overload; // operator and
function OrOp
  (const A, B: umlcmemtype_128): umlcmemtype_128; overload; // operator or
function XorOp
  (const A, B: umlcmemtype_128): umlcmemtype_128; overload; // operator xor

procedure Substract
  (out   ADest: umlcmemtype_128;
   const A, B:  umlcmemtype_128); overload; // operator -


implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcmemtype_128): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(@ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

procedure Min
  (out   ADest: umlcmemtype_128;
   const A:     umlcmemtype_128;
   const B:     umlcmemtype_128);
begin
  //ADest := 0;
  // @toxdo: ...
end;

procedure Max
  (out   ADest: umlcmemtype_128;
   const A:     umlcmemtype_128;
   const B:     umlcmemtype_128);
begin
  //ADest := 0;
  // @toxdo: ...
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcmemtype_128);
begin
  System.FillByte(ADest, sizeof(umlcmemtype_128), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

procedure Encode64To128
  (out   ADest: umlcmemtype_128;
   const ALo:   umlcmemtype_64;
   const AHi:   umlcmemtype_64);
begin
  umlcmemtype128s.Clear(ADest);
  System.Move(ALo, ADest.Value[0], sizeof(umlcmemtype_64));
  System.Move(AHi, ADest.Value[1], sizeof(umlcmemtype_64));
end;

procedure Decode128To64
  (out   ALo:     umlcmemtype_64;
   out   AHi:     umlcmemtype_64;
   const ASource: umlcmemtype_128);
begin
  ALo := 0;
  AHi := 0;
  System.Move(ASource.Value[0], ALo, sizeof(umlcmemtype_64));
  System.Move(ASource.Value[1], AHi, sizeof(umlcmemtype_64));
end;

procedure AssignMem8
  (out   ADest:   umlcmemtype_128;
   const ASource: umlcmemtype_8);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem16
  (out   ADest:   umlcmemtype_128;
   const ASource: umlcmemtype_16);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem32
  (out   ADest:   umlcmemtype_128;
   const ASource: umlcmemtype_32);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem64
  (out   ADest:   umlcmemtype_128;
   const ASource: umlcmemtype_64);
begin
  Clear(ADest);
  // @toxdo: ...
end;



(* global operators *)

procedure Assign
  (out   ADest:   umlcmemtype_128;
   const ASource: umlcmemtype_128);
begin
  System.Move(ASource, ADest, sizeof(umlcmemtype_128));
end;

function Equal
  (const A, B: umlcmemtype_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcmemtype_128));
  Result :=
    (ErrorCode = cmpEqual);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Different
  (const A, B: umlcmemtype_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcmemtype_128));
  Result :=
    (ErrorCode <> cmpEqual);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function NotOp
  (const AValue: umlcmemtype_128): umlcmemtype_128;
begin
  Clear(Result);
  // @toxdo: ...
end;

function NegTwoOp
  (const AValue: umlcmemtype_128): umlcmemtype_128;
begin
  Clear(Result);
  // @toxdo: ...
end;

function ShlOp
  (const AValue: umlcmemtype_128;
   const AShift: umlcmemtype_128): umlcmemtype_128;
begin
  Clear(Result);
  // @toxdo: ...
end;

function ShrOp
  (const AValue: umlcmemtype_128;
   const AShift: umlcmemtype_128): umlcmemtype_128;
begin
  Clear(Result);
  // @toxdo: ...
end;

function AndOp
  (const A, B: umlcmemtype_128): umlcmemtype_128;
begin
  Clear(Result);
  // @toxdo: ...
end;

function OrOp
  (const A, B: umlcmemtype_128): umlcmemtype_128;
begin
  Clear(Result);
  // @toxdo: ...
end;

function XorOp
  (const A, B: umlcmemtype_128): umlcmemtype_128;
begin
  Clear(Result);
  // @toxdo: ...
end;

procedure Substract
  (out   ADest: umlcmemtype_128;
   const A, B:  umlcmemtype_128);
begin
  Clear(ADest);
  // @toxdo: ...
end;

end.

