(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemtypes;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement (unsigned) memory oriented integer types.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcmemtypes : TUMLCModule =
   ($B3,$71,$BC,$48,$E6,$14,$14,$4F,$8D,$27,$5B,$4E,$67,$93,$40,$8E);

// ---

const

// ---

  ID_umlcmemtype_8 : TUMLCType =
    ($FC,$F7,$71,$77,$B1,$E4,$FB,$4D,$9B,$23,$4D,$03,$BF,$D7,$1A,$CF);

  ID_umlcmemtype_16 : TUMLCType =
    ($63,$EB,$FC,$24,$21,$66,$45,$42,$8F,$B8,$A9,$3B,$D5,$1B,$B7,$B6);

  ID_umlcmemtype_32 : TUMLCType =
    ($3E,$D0,$56,$86,$10,$E5,$16,$40,$9F,$F4,$83,$93,$3F,$13,$43,$2E);

  ID_umlcmemtype_64 : TUMLCType =
    ($67,$53,$7F,$8D,$9F,$4D,$FC,$4F,$87,$63,$47,$CA,$71,$AF,$69,$F6);

  ID_umlcmemtype_128 : TUMLCType =
    ($9F,$A7,$73,$3E,$09,$61,$FF,$41,$AF,$61,$46,$DD,$39,$09,$76,$F2);

  ID_umlcmemtype_256 : TUMLCType =
    ($DC,$1F,$45,$6C,$0A,$57,$53,$42,$B6,$42,$F8,$A2,$97,$3C,$C2,$69);

  ID_umlcmemtype_512 : TUMLCType =
    ($78,$D1,$8B,$33,$C5,$41,$E3,$40,$9A,$79,$1D,$A9,$62,$C4,$14,$23);

// ---

const

// ---

  ID_umlctmemtype_8 : TUMLCType =
    ($FC,$F7,$71,$77,$B1,$E4,$FB,$4D,$9B,$23,$4D,$03,$BF,$D7,$1A,$CF);

  ID_umlctmemtype_16 : TUMLCType =
    ($63,$EB,$FC,$24,$21,$66,$45,$42,$8F,$B8,$A9,$3B,$D5,$1B,$B7,$B6);

  ID_umlctmemtype_32 : TUMLCType =
    ($3E,$D0,$56,$86,$10,$E5,$16,$40,$9F,$F4,$83,$93,$3F,$13,$43,$2E);

  ID_umlctmemtype_64 : TUMLCType =
    ($67,$53,$7F,$8D,$9F,$4D,$FC,$4F,$87,$63,$47,$CA,$71,$AF,$69,$F6);

  ID_umlctmemtype_128 : TUMLCType =
    ($9F,$A7,$73,$3E,$09,$61,$FF,$41,$AF,$61,$46,$DD,$39,$09,$76,$F2);

  ID_umlctmemtype_256 : TUMLCType =
    ($DC,$1F,$45,$6C,$0A,$57,$53,$42,$B6,$42,$F8,$A2,$97,$3C,$C2,$69);

  ID_umlctmemtype_512 : TUMLCType =
    ($78,$D1,$8B,$33,$C5,$41,$E3,$40,$9A,$79,$1D,$A9,$62,$C4,$14,$23);

// ---

const

// ---

  ID_umlcpmemtype_8 : TUMLCType =
    ($4D,$F5,$2F,$46,$E7,$A4,$51,$4F,$84,$F8,$66,$61,$DE,$28,$33,$23);

  ID_umlcpmemtype_16 : TUMLCType =
    ($8B,$11,$07,$C5,$A7,$B9,$CE,$45,$B2,$A4,$DC,$6E,$2C,$87,$1D,$50);

  ID_umlcpmemtype_32 : TUMLCType =
    ($D3,$D8,$8A,$60,$18,$F2,$9F,$43,$A4,$62,$CB,$AD,$99,$8E,$7B,$9A);

  ID_umlcpmemtype_64 : TUMLCType =
    ($06,$0D,$2D,$F9,$05,$DC,$42,$4D,$91,$9B,$92,$A7,$A9,$3E,$CA,$B1);

  ID_umlcpmemtype_128 : TUMLCType =
    ($84,$96,$00,$80,$4C,$21,$E8,$43,$8C,$86,$37,$ED,$C8,$D7,$20,$A2);

  ID_umlcpmemtype_256 : TUMLCType =
    ($2F,$92,$6D,$14,$E7,$19,$08,$48,$AD,$22,$87,$00,$19,$D3,$D7,$9B);

  ID_umlcpmemtype_512 : TUMLCType =
    ($5D,$F8,$04,$2B,$C6,$A4,$27,$48,$8D,$69,$06,$35,$3C,$B9,$BA,$EA);

// ---

const

// ---

  ID_TUMLCMemoryTypes : TUMLCType =
    ($DC,$3C,$1E,$0D,$E7,$0C,$5B,$4E,$AD,$9C,$94,$9E,$E7,$93,$30,$29);

// ---

const

// ---

  ID_umlcmembyte : TUMLCType =
    ($4D,$F5,$2F,$46,$E7,$A4,$51,$4F,$84,$F8,$66,$61,$DE,$28,$33,$23);

  ID_umlcmemword : TUMLCType =
    ($8B,$11,$07,$C5,$A7,$B9,$CE,$45,$B2,$A4,$DC,$6E,$2C,$87,$1D,$50);

  ID_umlcmemdoublebyte : TUMLCType =
    ($8B,$11,$07,$C5,$A7,$B9,$CE,$45,$B2,$A4,$DC,$6E,$2C,$87,$1D,$50);

  ID_umlcmemlongword : TUMLCType =
    ($D3,$D8,$8A,$60,$18,$F2,$9F,$43,$A4,$62,$CB,$AD,$99,$8E,$7B,$9A);

  ID_umlcmemdoubleword : TUMLCType =
    ($D3,$D8,$8A,$60,$18,$F2,$9F,$43,$A4,$62,$CB,$AD,$99,$8E,$7B,$9A);

  ID_umlcmemquad: TUMLCType =
    ($D3,$D8,$8A,$60,$18,$F2,$9F,$43,$A4,$62,$CB,$AD,$99,$8E,$7B,$9A);

  ID_umlcmemocta: TUMLCType =
    ($D1,$8A,$BB,$57,$FD,$7E,$09,$4B,$AA,$AB,$8D,$2C,$BE,$E3,$D1,$FF);

// ---

type

  umlcmemtype_8    = (* alias of *) umlcuinttypes.umlcuint_8;
  umlcmemtype_16   = (* alias of *) umlcuinttypes.umlcuint_16;
  umlcmemtype_32   = (* alias of *) umlcuinttypes.umlcuint_32;
  umlcmemtype_64   = (* alias of *) umlcuinttypes.umlcuint_64;

  umlcmemtype_128  = (* alias of *) umlcuinttypes.umlcuint_128;
  umlcmemtype_256  = (* alias of *) umlcuinttypes.umlcuint_256;
  umlcmemtype_512  = (* alias of *) umlcuinttypes.umlcuint_512;

(* global additional types *)

type

  TUMLCMemoryTypes = (* enum of *)
  (
    umlcmemtypmemtype_Unknown,
    umlcmemtypmemtype_8,
    umlcmemtypmemtype_16,
    umlcmemtypmemtype_32,
    umlcmemtypmemtype_64,
    umlcmemtypmemtype_128,
    umlcmemtypmemtype_256,
    umlcmemtypmemtype_512
  );

type

  umlctmemtype_8    = (* alias of *) umlcmemtype_8;
  umlctmemtype_16   = (* alias of *) umlcmemtype_16;
  umlctmemtype_32   = (* alias of *) umlcmemtype_32;
  umlctmemtype_64   = (* alias of *) umlcmemtype_64;
  umlctmemtype_128  = (* alias of *) umlcmemtype_128;
  umlctmemtype_256  = (* alias of *) umlcmemtype_256;
  umlctmemtype_512  = (* alias of *) umlcmemtype_512;

type

  umlcpmemtype_8    = ^umlcmemtype_8;
  umlcpmemtype_16   = ^umlcmemtype_16;
  umlcpmemtype_32   = ^umlcmemtype_32;
  umlcpmemtype_64   = ^umlcmemtype_64;
  umlcpmemtype_128  = ^umlcmemtype_128;
  umlcpmemtype_256  = ^umlcmemtype_256;
  umlcpmemtype_512  = ^umlcmemtype_512;

type

  umlcmembyte       = (* alias of *) umlcmemtype_8;

  umlcmemword       = (* alias of *) umlcmemtype_16;
  umlcmemdoublebyte = (* alias of *) umlcmemtype_16;

  umlcmemlongword   = (* alias of *) umlcmemtype_32;
  umlcmemdoubleword = (* alias of *) umlcmemtype_32;
  umlcmemquad       = (* alias of *) umlcmemtype_32;

  umlcmemocta       = (* alias of *) umlcmemtype_64;

(* "namespaced" redefinitions *)

type

  umlcmemtypes__umlcmemtype_8    = (* alias of *) umlcmemtype_8;
  umlcmemtypes__umlcmemtype_16   = (* alias of *) umlcmemtype_16;
  umlcmemtypes__umlcmemtype_32   = (* alias of *) umlcmemtype_32;
  umlcmemtypes__umlcmemtype_64   = (* alias of *) umlcmemtype_64;
  umlcmemtypes__umlcmemtype_128  = (* alias of *) umlcmemtype_128;
  umlcmemtypes__umlcmemtype_256  = (* alias of *) umlcmemtype_256;
  umlcmemtypes__umlcmemtype_512  = (* alias of *) umlcmemtype_512;

type

  umlcmemtypes__umlctmemtype_8    = (* alias of *) umlctmemtype_8;
  umlcmemtypes__umlctmemtype_16   = (* alias of *) umlctmemtype_16;
  umlcmemtypes__umlctmemtype_32   = (* alias of *) umlctmemtype_32;
  umlcmemtypes__umlctmemtype_64   = (* alias of *) umlctmemtype_64;
  umlcmemtypes__umlctmemtype_128  = (* alias of *) umlctmemtype_128;
  umlcmemtypes__umlctmemtype_256  = (* alias of *) umlctmemtype_256;
  umlcmemtypes__umlctmemtype_512  = (* alias of *) umlctmemtype_512;

type

  umlcmemtypes__umlcpmemtype_8    = (* alias of *) umlcpmemtype_8;
  umlcmemtypes__umlcpmemtype_16   = (* alias of *) umlcpmemtype_16;
  umlcmemtypes__umlcpmemtype_32   = (* alias of *) umlcpmemtype_32;
  umlcmemtypes__umlcpmemtype_64   = (* alias of *) umlcpmemtype_64;
  umlcmemtypes__umlcpmemtype_128  = (* alias of *) umlcpmemtype_128;
  umlcmemtypes__umlcpmemtype_256  = (* alias of *) umlcpmemtype_256;
  umlcmemtypes__umlcpmemtype_512  = (* alias of *) umlcpmemtype_512;

(* global constants *)

const
  umlcmemtypezero_8:  umlcmemtype_8  = 0;
  umlcmemtypezero_16: umlcmemtype_16 = 0;
  umlcmemtypezero_32: umlcmemtype_32 = 0;
  umlcmemtypezero_64: umlcmemtype_64 = 0;

(* global functions *)

function SizeInBytesOf
  (const AMemoryType: TUMLCMemoryTypes): Cardinal;

implementation

(* global functions *)

function SizeInBytesOf
  (const AMemoryType: TUMLCMemoryTypes): Cardinal;
begin
  //umlcmemtypmemtype_Unknown:
  Result := 0;

  case AMemoryType of
    umlcmemtypmemtype_8:
      Result := sizeof(umlcmemtype_8);
    umlcmemtypmemtype_16:
      Result := sizeof(umlcmemtype_16);
    umlcmemtypmemtype_32:
      Result := sizeof(umlcmemtype_32);
    umlcmemtypmemtype_64:
      Result := sizeof(umlcmemtype_64);
    umlcmemtypmemtype_128:
      Result := sizeof(umlcmemtype_128);
    umlcmemtypmemtype_256:
      Result := sizeof(umlcmemtype_256);
    umlcmemtypmemtype_512:
      Result := sizeof(umlcmemtype_512);
  end;
end;


end.

