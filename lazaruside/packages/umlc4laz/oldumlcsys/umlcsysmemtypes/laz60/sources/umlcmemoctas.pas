(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemoctas;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in octas segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  umlcmemtype64s,
  dummy;
  
// ---

const

  MOD_umlcmemoctas : TUMLCModule =
    ($F2,$3D,$FE,$1B,$33,$02,$36,$44,$A1,$1B,$A2,$D6,$3F,$B9,$E4,$AF);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcmemocta): Boolean; overload;

(* global procedures *)

  procedure Clear
    (out ADest: umlcmemocta); overload;




implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcmemocta): Boolean;
begin
  Result :=
    umlcmemtype64s.IsEmpty(ASource);
end;



(* global procedures *)

procedure Clear
  (out ADest: umlcmemocta);
begin
  umlcmemtype64s.Clear(ADest)
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;








end.

