(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmembytes;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in bytes segments.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  umlcmemtype8s,
  dummy;
  
// ---

const

  MOD_umlcmembytes : TUMLCModule =
    ($69,$FE,$E2,$A5,$F9,$83,$EF,$46,$8A,$F5,$AF,$99,$8E,$22,$33,$3B);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcmembyte): Boolean; overload;

(* global standard procedures *)

  procedure Clear
    (out ADest: umlcmembyte); overload;

implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcmembyte): Boolean;
begin
  Result :=
    umlcmemtype8s.IsEmpty(ASource);
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcmembyte);
begin
  umlcmemtype8s.Clear(ADest)
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;





end.

