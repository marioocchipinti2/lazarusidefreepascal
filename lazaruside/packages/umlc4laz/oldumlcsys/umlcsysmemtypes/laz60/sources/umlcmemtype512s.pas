(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemtype512s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 512 bits segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  umlcmemtype64s,
  umlcmemtype128s,
  umlcmemtype256s,
  dummy;

// ---

const

 MOD_umlcmemtype512s : TUMLCModule =
  ($EA,$14,$43,$90,$9E,$70,$43,$47,$9C,$EC,$16,$A0,$B4,$1D,$08,$34);

// ---

(* "namespaced" redefinitions *)

type

 umlcmemtype512s__umlcmemtype_512 = (* alias of *) umlcmemtype_512;

(* global functions *)

function IsEmpty
  (const ASource: umlcmemtype_512): Boolean; overload;

procedure Min
  (out   ADest: umlcmemtype_512;
   const A:     umlcmemtype_512;
   const B:     umlcmemtype_512); overload;

procedure Max
  (out   ADest: umlcmemtype_512;
   const A:     umlcmemtype_512;
   const B:     umlcmemtype_512); overload;

(* global procedures *)

procedure Clear
  (out ADest: umlcmemtype_512); overload;

procedure Encode128To512
  (out   ADest: umlcmemtype_512;
   const AA:    umlcmemtype_128;
   const AB:    umlcmemtype_128;
   const AC:    umlcmemtype_128;
   const AD:    umlcmemtype_128);

procedure Encode256To512
  (out   ADest: umlcmemtype_512;
   const AA:    umlcmemtype_256;
   const AB:    umlcmemtype_256);

procedure Decode512To128
  (out   AA:      umlcmemtype_128;
   out   AB:      umlcmemtype_128;
   out   AC:      umlcmemtype_128;
   out   AD:      umlcmemtype_128;
   const ASource: umlcmemtype_512);

procedure Decode512To256
  (out   AA:      umlcmemtype_256;
   out   AB:      umlcmemtype_256;
   const ASource: umlcmemtype_512);

procedure AssignMem8
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_8);

procedure AssignMem16
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_16);

procedure AssignMem32
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_32);

procedure AssignMem64
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_64);

procedure AssignMem128
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_128);

procedure AssignMem256
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_256);

(* global operators *)

procedure Assign
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_512); overload; // operator :=

function Equal
  (const A, B: umlcmemtype_512): Boolean; overload; // operator =
function Different
  (const A, B: umlcmemtype_512): Boolean; overload; // operator <>

function NotOp
  (const AValue: umlcmemtype_512): umlcmemtype_512; overload; // operator not
function NegTwoOp
  (const AValue: umlcmemtype_512): umlcmemtype_512; overload; // operator negtwo

function ShlOp
  (const AValue: umlcmemtype_512;
   const AShift: umlcmemtype_512): umlcmemtype_512; overload; // operator xor
function ShrOp
  (const AValue: umlcmemtype_512;
   const AShift: umlcmemtype_512): umlcmemtype_512; overload; // operator xor

function AndOp
  (const A, B: umlcmemtype_512): umlcmemtype_512; overload; // operator and
function OrOp
  (const A, B: umlcmemtype_512): umlcmemtype_512; overload; // operator or
function XorOp
  (const A, B: umlcmemtype_512): umlcmemtype_512; overload; // operator xor

procedure Substract
  (out   ADest: umlcmemtype_512;
   const A, B:  umlcmemtype_512);

implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcmemtype_512): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(@ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

procedure Min
  (out   ADest: umlcmemtype_512;
   const A:     umlcmemtype_512;
   const B:     umlcmemtype_512);
begin
  //ADest := 0;
  // @toxdo: ...
end;

procedure Max
  (out   ADest: umlcmemtype_512;
   const A:     umlcmemtype_512;
   const B:     umlcmemtype_512);
begin
  //ADest := 0;
  // @toxdo: ...
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcmemtype_512);
begin
  System.FillByte(ADest, sizeof(umlcmemtype_512), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

procedure Encode128To512
  (out   ADest: umlcmemtype_512;
   const AA:    umlcmemtype_128;
   const AB:    umlcmemtype_128;
   const AC:    umlcmemtype_128;
   const AD:    umlcmemtype_128);
begin
  umlcmem512s.Clear(ADest);
  System.Move(AA, ADest.Value[0], sizeof(umlcmemtype_128));
  System.Move(AB, ADest.Value[2], sizeof(umlcmemtype_128));
  System.Move(AC, ADest.Value[4], sizeof(umlcmemtype_128));
  System.Move(AD, ADest.Value[6], sizeof(umlcmemtype_128));
end;

procedure Encode256To512
  (out   ADest: umlcmemtype_512;
   const AA:    umlcmemtype_256;
   const AB:    umlcmemtype_256);
begin
  umlcmem512s.Clear(ADest);
  System.Move(AA, ADest.Value[0], sizeof(umlcmemtype_256));
  System.Move(AB, ADest.Value[1], sizeof(umlcmemtype_256));
end;

procedure Decode512To128
  (out   AA:      umlcmemtype_128;
   out   AB:      umlcmemtype_128;
   out   AC:      umlcmemtype_128;
   out   AD:      umlcmemtype_128;
   const ASource: umlcmemtype_512);
begin
  umlcmem128s.Clear(AA);
  umlcmem128s.Clear(AB);
  umlcmem128s.Clear(AC);
  umlcmem128s.Clear(AD);
  System.Move(ASource.Value[0], AA, sizeof(umlcmemtype_128));
  System.Move(ASource.Value[2], AB, sizeof(umlcmemtype_128));
  System.Move(ASource.Value[4], AC, sizeof(umlcmemtype_128));
  System.Move(ASource.Value[6], AD, sizeof(umlcmemtype_128));
end;

procedure Decode512To256
  (out   AA:      umlcmemtype_256;
   out   AB:      umlcmemtype_256;
   const ASource: umlcmemtype_512);
begin
  umlcmem256s.Clear(AA);
  umlcmem256s.Clear(AB);
  System.Move(ASource.Value[0], AA, sizeof(umlcmemtype_256));
  System.Move(ASource.Value[1], AB, sizeof(umlcmemtype_256));
end;

procedure AssignMem8
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_8);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem16
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_16);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem32
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_32);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem64
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_64);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem128
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_128);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem256
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_256);
begin
  Clear(ADest);
  // @toxdo: ...
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcmemtype_512;
   const ASource: umlcmemtype_512);
begin
  System.Move(ASource, ADest, sizeof(umlcmemtype_512));
end;

function Equal
  (const A, B: umlcmemtype_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcmemtype_512));
  Result :=
    (ErrorCode = cmpEqual);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Different
  (const A, B: umlcmemtype_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcmemtype_512));
  Result :=
    (ErrorCode <> cmpEqual);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function NotOp
  (const AValue: umlcmemtype_512): umlcmemtype_512;
begin
  Clear(Result);
  // @toxdo: ...
end;

function NegTwoOp
  (const AValue: umlcmemtype_512): umlcmemtype_512;
begin
  Clear(Result);
  // @toxdo: ...
end;

function ShlOp
  (const AValue: umlcmemtype_512;
   const AShift: umlcmemtype_512): umlcmemtype_512;
begin
  Clear(Result);
  // @toxdo: ...
end;

function ShrOp
  (const AValue: umlcmemtype_512;
   const AShift: umlcmemtype_512): umlcmemtype_512;
begin
  Clear(Result);
  // @toxdo: ...
end;

function AndOp
  (const A, B: umlcmemtype_512): umlcmemtype_512;
begin
  Clear(Result);
  // @toxdo: ...
end;

function OrOp
  (const A, B: umlcmemtype_512): umlcmemtype_512;
begin
  Clear(Result);
  // @toxdo: ...
end;

function XorOp
  (const A, B: umlcmemtype_512): umlcmemtype_512;
begin
  Clear(Result);
  // @toxdo: ...
end;

procedure Substract
  (out   ADest: umlcmemtype_512;
   const A, B:  umlcmemtype_512);
begin
  Clear(ADest);
  // @toxdo: ...
end;


end.

