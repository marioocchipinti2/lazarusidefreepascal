(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmembytebuffers;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 1D 8 bits segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  umlcmembytes,
  dummy;
  
// ---

const

  MOD_umlcmembytebuffers : TUMLCModule =
    ($B2,$1D,$91,$E0,$53,$C8,$05,$43,$AD,$75,$A9,$DB,$42,$7D,$84,$BF);

// ---

(* global functions *)

  function BufferIsEmpty
    (var   ABuffer: Pointer;
     const ACount:  Cardinal): Boolean; overload;

  function BufferSize
    (const ACount: Cardinal): Cardinal; overload;

  function NewBuffer
    (const ACount: Cardinal): Pointer; overload;

  function BufferAt
    (var   ABuffer: Pointer;
     const ACount:  Cardinal;
     const AIndex:  Cardinal): Boolean; overload;

(* global procedures *)

  procedure ClearBuffer
    (var   ABuffer: Pointer;
     const ACount:  Cardinal); overload;

  procedure FillBuffer
    (var   ABuffer: Pointer;
     const ACount:  Cardinal;
     const AValue:  Byte); overload;

  procedure DropBuffer
    (var   ABuffer: Pointer;
     const ACount: Cardinal); overload;

(* global properties *)

function BufferGetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal): PByte; overload;

procedure BufferSetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   const AValue:  PByte); overload;

implementation

(* global functions *)

function BufferIsEmpty
  (var   ABuffer: Pointer;
   const ACount:  Cardinal): Boolean;
var I: Cardinal; Match: Boolean; Item: PByte;
begin
  Result := false;
  if (Assigned(ABuffer) and (ACount > 0)) then
  begin
    Item := PByte(ABuffer);
    Match := false;
    I := 0;
    while ((I <= Pred(ACount)) and (not Match)) do
    begin
      Match :=
        (Item^ <> 0);

      Inc(I);
    end;

    Result := not Match;
  end;
end;

function BufferSize
  (const ACount: Cardinal): Cardinal;
begin
  Result :=
    sizeof(umlcmembyte) * ACount;
end;

function NewBuffer
  (const ACount: Cardinal): Pointer;
var ASize: Cardinal;
begin
  Result := nil;
  ASize := sizeof(umlcmembyte) * ACount;
  System.GetMem(Result, ASize);
end;


(* global procedures *)

procedure ClearBuffer
  (var   ABuffer: Pointer;
   const ACount:  Cardinal);
var ASize: Cardinal;
begin
  if (Assigned(ABuffer)) then
  begin
    ASize := sizeof(umlcmembyte) * ACount;
    System.FillByte(ABuffer^, ASize, 0);
  end;
end;

procedure FillBuffer
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AValue:  Byte);
var ASize: Cardinal;
begin
  if (Assigned(ABuffer)) then
  begin
    ASize := sizeof(umlcmembyte) * ACount;
    System.FillByte(ABuffer^, ASize, AValue);
  end;
end;

procedure DropBuffer
  (var   ABuffer: Pointer;
   const ACount:  Cardinal);
var ASize: Cardinal;
begin
  if (Assigned(ABuffer)) then
  begin
    ASize := sizeof(umlcmembyte) * ACount;
    System.FreeMem(ABuffer, ASize);
  end;
end;

function BufferAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal): Boolean;
begin
  Result := false;
  if (Assigned(ABuffer)) then
  begin
    //ASize := sizeof(umlcmembyte) * ACount;
    //System.FreeMem(ABuffer, ASize);
  end;
end;

(* global properties *)

function BufferGetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal): PByte;
begin
  Result := nil;
  if (Assigned(ABuffer) and (ACount > 0)) then
  begin
    //ASize := sizeof(umlcmembyte) * ACount;
    //System.FreeMem(ABuffer, ASize);
  end;
end;

procedure BufferSetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   const AValue:  PByte);
begin
  if (Assigned(ABuffer) and (ACount > 0) and Assigned(AValue)) then
  begin
    //ASize := sizeof(umlcmembyte) * ACount;
    //System.FreeMem(ABuffer, ASize);
  end;
end;


end.

