(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemword2dbuffers;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 2D word segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  umlcmemquads,
  dummy;
  
// ---

const

  MOD_umlcmemword2dbuffers : TUMLCModule =
    ($62,$44,$62,$71,$B3,$75,$92,$4E,$A6,$08,$83,$20,$99,$13,$57,$24);

// ---



implementation





end.

