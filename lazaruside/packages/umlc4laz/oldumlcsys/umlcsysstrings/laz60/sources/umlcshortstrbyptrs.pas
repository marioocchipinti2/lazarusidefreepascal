unit umlcshortstrbyptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcstdstrtypes,
  dummy;

// ---

const

 MOD_umlcshortstrbyptrs : TUMLCModule =
   ($03,$A7,$38,$30,$67,$98,$D1,$4F,$92,$EF,$92,$6B,$D2,$28,$3F,$C9);

// ---


(* global functions *)

function ConstToPtr
 (const AValue: shortstring): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: shortstring): pointer;
var P: umlcpansishortstring;
    ACount, ARealCount, I: Byte;
begin
  System.GetMem(P, sizeof(umlcansishortstring));

  ACount :=
    System.Length(AValue);
  ARealCount :=
    Math.Min(sizeof(umlcansishortstring), ACount);

  I := 0;
  while (I <= ARealCount) do
  begin
    P^[I] := AValue[I];
    System.Inc(I);
  end;

  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcansishortstring));
end;


end.

