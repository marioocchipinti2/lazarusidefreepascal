(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstrings;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to support the predefined non pascal Delphi "string" type.
 ** The character encoding is neutral.
 **************************************************************************
 **)

  // note: doesn't check if characters are ANSI or Unicode
  // nota: no he checado si los caracteres son ANSI o Unicode

interface
uses
{$IFDEF MSWINDOWS}
  Windows, //Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdstrtypes,
  umlcchars,
  dummy;

// ---

const

   MOD_umlcstrings : TUMLCModule =
     ($D0,$2B,$6B,$9A,$00,$7C,$5A,$41,$9E,$72,$2A,$78,$EB,$26,$C9,$A5);

// ---

(* global functions *)

  function IsEmpty
    (const ASource: string): Boolean;

  function Length
    (const ASource: string): Word; overload;

  function IsStringOfChar
    (const AHaystack: string; ANeedle: Char): Boolean;

  function UppercaseCopy
    (const ASource: string): string; overload;
  function LowercaseCopy
    (const ASource: string): string; overload;
  function TogglecaseCopy
    (const ASource: string): string; overload;
  function CapitalizeCopy
    (const ASource: string): string; overload;

  function StringOfCharCopy
    (const ASource: ansichar; const ACount: Byte): string;

  function ConcatCharCopy
    (const A: string; B: ansichar): string; overload;
  function ConcatStrCopy
    (const A, B: string): string; overload;

  function LeftCopy
    (const ASource: string; const ACount: Word): string;
  function RightCopy
    (const ASource: string; const ACount: Word): string;

  function UnLeftCopy
    (const ASource: string; const ACount: Word): string;
  function UnRightCopy
    (const ASource: string; const ACount: Word): string;

  function TrimLeftCopy
    (const ASource: string): string; overload;
  function TrimRightCopy
    (const ASource: string): string; overload;
  function TrimCopy
    (const ASource: string): string; overload;

  function TrimPrefixCopy
    (const ASource: string; APrefix: string): string; overload;
  function TrimPosfixCopy
    (const ASource: string; APosfix: string): string; overload;

  function UnTrimLeftCopy
    (const ASource: string; ACount: Word): string; overload;
  function UnTrimRightCopy
    (const ASource: string; ACount: Word): string; overload;
  function UnTrimCopy
    (const ASource: string; ACount: Word): string; overload;

  function PadPrefixCopy
    (const ASource: string; APrefix: string): string; overload;
  function PadPosfixCopy
    (const ASource: string; APrefix: string): string; overload;

  function AlignLeftCopy
   (const ASource: string;
    const ACount:  Word;
    const AValue:  Char): string;
  function AlignRightCopy
   (const ASource: string;
    const ACount:  Word;
    const AValue:  Char): string;

  function CARCopy
    (const ASource: string): string; overload;
  function CDRCopy
    (const ASource: string): string; overload;

  function LastChar
    (const ASource: string): Char;

  function LastCharPtr
    (var ASource: string): PChar;

  function RangeToStrCopy
    (const Min, Max: Char): string;

  function ReverseCopy
    (const ASource: string): string;

  function TryStrToChar
    (const ASource: string; var ADest: ansichar): Boolean;
  function StrToCharDef
    (const ASource: string; const ADefValue: ansichar): ansichar;
  function StrToChar
    (const ASource: string): ansichar;

  function PtrToStr
    (const ASource: pointer): string;
  function StrToPtr
    (const ASource: string): pointer;

  function IsSubStrAt
    (const AHaystack: string;
     const ANeedle: string): Boolean;

type
  TCompareStrAt =
    (* ^ *)function (const AHaystack, ANeedle: string; var AIndex: Word): Boolean;

  function SameStrAt
    (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;
  function EqualStrAt
    (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;

  function SameStrAtIn
    (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;
  function EqualStrAtIn
    (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;

  function SameStr
    (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;
  function EqualStr
    (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;

  function Search
    (const AHaystack: string;
     const ANeedle: string;
     var AIndex: Word;
     const CompareStrAt: TCompareStrAt): Boolean;

(* global operators *)

  procedure Assign
    (out   ADest:   string;
     const ASource: string); overload; // operator :=

  procedure ConcatChar
    (var   ADest:   string;
     const ASource: Char); overload; // operator +
  procedure ConcatCharBack
    (var   ADest: string;
     const ASource: Char); overload; // operator +

  procedure ConcatStr
    (var ADest: string; const ASource: string); overload; // operator +
  procedure ConcatStrBack
    (var ADest: string; const ASource: string); overload; // operator +

  function Different
    (const A, B: string): Boolean; overload; // operator <>
  function Greater
    (const A, B: string): Boolean; overload; // operator >
  function Lesser
    (const A, B: string): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: string): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: string): Boolean; overload; // operator <=

(* equality operators *)

  function Equal
    (const A, B: string): Boolean; // operator :=

  function SameText
    (const A, B: string): Boolean;

  function Compare
    (const A, B: string): TComparison;

  function EqualByOptions
    (const A, B: string; Options: TStringOptions): Boolean;
  function CompareByOptions
    (const A, B: string; Options: TStringOptions): TComparison;

(* global procedures *)

  procedure AssignLength
    (var ADest: string; ACount: Word); overload;

  procedure Clear
    (out ADest: string);

  function TryAssignCopyAt
    (out   ADest:        string;
     const ASource:      string;
     const ASourceIndex: word): Boolean;
  function TryAssignCopyAtCount
    (out   ADest:        string;
     const ASource:      string;
     const ASourceIndex: word;
     const ASourceCount: word): Boolean;
  function TryAssignCopyCount
    (out   ADest:        string;
     const ASource:      string;
     const ASourceCount: word): Boolean;

  procedure AssignCopyAt
    (out   ADest:        string;
     const ASource:      string;
     const ASourceIndex: word);
  procedure AssignCopyAtCount
    (out   ADest:        string;
     const ASource:      string;
     const ASourceIndex: word;
     const ASourceCount: word);
  procedure AssignCopyCount
    (out   ADest:        string;
     const ASource:      string;
     const ASourceCount: word);

  procedure AssignAt
    (var ADest: string; const ASource: string; AIndex, ACount: Word);
  procedure AssignLeft
    (var ADest: string; const ASource: string; const ACount: Word);
  procedure AssignRight
    (var ADest: string; const ASource: string; const ACount: Word);

procedure LastCharCountPtr
  (out   ADestPtr: PChar;
   out   ACount:   Word;
   const ASource:  string);

procedure AssignReverse
  (out ADest: string; const ASource: string);

(* global random string functions *)

  function RandomAlphaNumString
    (const ACount: Word): string;
  function RandomAlphaString
    (const ACount: Word): string;
  function RandomNumString
    (const ACount: Word): string;

implementation

(* global functions *)

function IsEmpty
  (const ASource: string): Boolean;
begin
  Result :=
    Length(ASource) = 0;
  // Goal: Returns if a string is empty.
  // Objetivo: Regresa si una cadena esta vacia.
end;

function Length
  (const ASource: string): Word;
begin
  Result :=
    System.Length(ASource);
  // Goal: Returns the length of the given string.
  // Objetivo: Regresa la longuitud de la cadena dada.
end;

function IsStringOfChar
  (const AHaystack: string; ANeedle: Char): Boolean;
var I, L: Integer; Match: Boolean;
begin
  L :=
    umlcstrings.Length(AHaystack);

  Result := (L > 0);
  if (Result) then
  begin
    I := 1; Match := TRUE;
    while ((I <= L) and (Match)) do
    begin
      Match :=
        (AHaystack[i] = ANeedle);
      Inc(I);
    end;

    Result :=
      Match;
  end;
  // Objetivo: Regresa si una cadena esta compuesta solo del mismo caracter.
  // Goal: Returns if a string is composed with the same character.
end;

function UppercaseCopy
  (const ASource: string): string;
begin
  Result :=
    SysUtils.ANSIUpperCase(ASource);
  // Goal: Returns a uppercase copy of the given string.
  // Objetivo: Regresa una copia en mayusculas de la cadena dada.
end;

function LowercaseCopy
  (const ASource: string): string;
begin
  Result :=
    SysUtils.ANSILowerCase(ASource);
  // Goal: Returns a lowercase copy of the given string.
  // Objetivo: Regresa una copia en minusculas de la cadena dada.
end;

function TogglecaseCopy
  (const ASource: string): string;
var I, Last: Integer; C: char;
begin
  umlcstrings.Clear(Result);

  Last :=
    umlcstrings.Length(ASource);

  for I := 1 to Last do
  begin
    C := ASource[i];
    if (IsCharLower(C))
      then CharUpperBuff(@C, 1)
      else CharLowerBuff(@C, 1);
    Result := Result + C;
  end;
  // Goal: Swaps the sensitive case of each character, the given ansiarray.
  // Objetivo: Cambia el caso sensitivo de cada caracter en la cadena.
end;

function CapitalizeCopy
  (const ASource: string): string;
var I, Last: Integer; C: char; MakeUppercase: Boolean;
begin
  umlcstrings.Clear(Result);

  MakeUppercase := TRUE;
  Last :=
    umlcstrings.Length(ASource);
  for I := 1 to Last do
  begin
    C := ASource[i];
    if (C <> #32) then
    begin
      if (MakeUppercase) then
      begin
        CharUpperBuff(@C, 1);
        MakeUppercase := FALSE;
      end else
      begin
        CharLowerBuff(@C, 1);
      end;
    end else
    begin
      MakeUppercase := TRUE;
    end;

    umlcstrings.ConcatChar(Result, C);
  end;
  // Goal: Returns a copy with uppercase initials of the given ansiarray.
  // Objetivo: Regresa una copia con iniciales en mayusculas de la cadena dada.
end;

function StringOfCharCopy
  (const ASource: ansichar; const ACount: Byte): string;
begin
  umlcstrings.Clear(Result);
  umlcstrings.AssignLength(Result, ACount);
  System.FillChar(Result[1], ACount, ASource);
  // Goal: Returns a string of the same ansichar.
  // Objetivo: Regresa una cadena del mismo caracter.
end;

function ConcatCharCopy
  (const A: string; B: ansichar): string;
begin
  Result := A + B;
  // Objetivo: Agregar  un caracter al final de la cadena dada.
  // Goal: Add a character at the end of the given string.
end;

function ConcatStrCopy
  (const A, B: string): string;
begin
  Result := A + B;
end;

function LeftCopy
  (const ASource: string; const ACount: Word): string;
var ThisCount: Word;
begin
  umlcstrings.Clear(Result);

  ThisCount :=
    umlcstrings.Length(ASource);
  if (ThisCount > ACount) then
  begin
    ThisCount := ACount;
  end;

  umlcstrings.AssignLeft
    (Result, ASource, ThisCount);
  // Goal: Returns the leftmost characters of "AValue".
  // Objetivo: Regresa los caracteres mas a la izquierda de "AValue".
end;

function RightCopy
  (const ASource: string; const ACount: Word): string;
var ThisCount: Word;
begin
  umlcstrings.Clear(Result);

  ThisCount :=
    umlcstrings.Length(ASource);
  if (ThisCount > ACount) then
  begin
    ThisCount := ACount;
  end;

  umlcstrings.AssignRight(Result, ASource, ThisCount);
  // Goal: Returns the rightmost characters of "AValue".
  // Objetivo: Regresa los caracteres mas a la derecha de "AValue".
end;

function UnLeftCopy
  (const ASource: string; const ACount: Word): string;
begin
  umlcstrings.Clear(Result);

  umlcstrings.AssignAt
    (Result, ASource, ACount, umlcstrings.Length(ASource));
  // Goal: Removes the leftmost characters of "ASource".
  // Objetivo: Remueve los caracteres mas a la izquierda de "ASource".
end;

function UnRightCopy
  (const ASource: string; const ACount: Word): string;
var AIndex: Word;
begin
  umlcstrings.Clear(Result);

  AIndex :=
    Succ(umlcstrings.Length(ASource)-ACount);
  umlcstrings.AssignAt(Result, ASource, 1, AIndex);
  // Goal: Removes the rightmost characters of "ASource".
  // Objetivo: Remueve los caracteres mas a la derecha de "ASource".
end;

function TrimLeftCopy
  (const ASource: string): string;
begin
  Result :=
    SysUtils.TrimLeft(ASource);
  // Goal: Returns a copy of the given string without leading spaces.
  // Objetivo: Regresa una copia de la cadena dada sin espacios iniciales.
end;

function TrimRightCopy
  (const ASource: string): string;
begin
  Result :=
    SysUtils.TrimRight(ASource);
  // Goal: Returns a copy of the given string without trailing spaces.
  // Objetivo: Regresa una copia de la cadena dada sin espacios finales.
end;

function TrimCopy
  (const ASource: string): string;
begin
  Result :=
    SysUtils.Trim(ASource);
  // Goal: Returns a copy of the given string }
  // without leading & trailing spaces.

  // Objetivo: Regresa una copia de la cadena dada
  // sin espacios iniciales y finales.
end;

function TrimPrefixCopy
  (const ASource: string; APrefix: string): string;
begin
  Result := '';
    //SysUtils.Trim(ASource);
end;

function TrimPosfixCopy
  (const ASource: string; APosfix: string): string;
begin
  Result := '';
    //SysUtils.Trim(ASource);
end;

function UnTrimLeftCopy
  (const ASource: string; ACount: Word): string;
var L: Integer;
begin
  L :=
    umlcstrings.Length(ASource);
  Result :=
    umlcstrings.StringOfCharCopy(#32, ACount-L) + ASource;
  // Goal: Returns a copy of the given string plus leading spaces.
  // Objetivo: Regresa una copia de la cadena dada mas espacios iniciales.
end;

function UnTrimRightCopy
  (const ASource: string; ACount: Word): string;
var L : Integer;
begin
  L :=
    umlcstrings.Length(ASource);
  Result :=
    ASource + umlcstrings.StringOfCharCopy(#32, ACount-L);
  // Goal: Returns a copy of the given string plus trailing spaces.
  // Objetivo: Regresa una copia de la cadena dada mas espacios finales.
end;

function UnTrimCopy
  (const ASource: string; ACount: Word): string;
var L : Integer;
begin
  L :=
    umlcstrings.Length(ASource);
  Result :=
    umlcstrings.StringOfCharCopy(#32, ACount-L) + ASource + umlcstrings.StringOfCharCopy(#32, ACount-L);
  // Goal: Returns a copy of the given string plus trailing spaces.
  // Objetivo: Regresa una copia de la cadena dada mas espacios finales.
end;

function PadPrefixCopy
  (const ASource: string; APrefix: string): string;
begin
  Result := '';end;

function PadPosfixCopy
  (const ASource: string; APrefix: string): string;
begin
  Result := '';
end;

function AlignLeftCopy
 (const ASource: string;
  const ACount:  Word;
  const AValue:  Char): string;
var Difference: Word;
begin
  Difference :=
    ACount - umlcstrings.Length(ASource);
  umlcstrings.Clear
    (Result);
  umlcstrings.ConcatStr
    (Result, ASource);
  umlcstrings.ConcatStr
    (Result, umlcstrings.StringOfCharCopy(AValue, Difference));
  // Goal: Add characters to the right of a string.
  // Objetivo: Agregar caracteres a la derecha o final de una cadena.
end;

function AlignRightCopy
 (const ASource: string;
  const ACount:  Word;
  const AValue: Char): string;
var Difference: Word;
begin
  Difference :=
    ACount - umlcstrings.Length(ASource);
  umlcstrings.Clear(Result);
  umlcstrings.ConcatStr
    (Result, umlcstrings.StringOfCharCopy(AValue, Difference));
  umlcstrings.ConcatStr
    (Result, ASource);
  // Goal: Add characters to the left of a string.
  // Objetivo: Agregar caracteres a la izquierda o inicio de una cadena.
end;

function CARCopy
  (const ASource: string): string;
begin
  Result := ASource[1];
  // Objetivo: Regresa el primer caracter de una cadena.
  // Goal: Returns the first character of a string.
end;

function CDRCopy
  (const ASource: string): string;
begin
  Result :=
    System.Copy(ASource, 2, Pred(umlcstrings.Length(ASource)));
  // Objetivo: Regresa el resto de una cadena.
  // Goal: Returns the rest of the string.
end;

function LastChar
  (const ASource: string): Char;
var L: Word;
begin
  L := umlcstrings.Length(ASource);
  if (L < 1)
    then Result := #0
    else Result := ASource[ L ];
  // Goal: To return the last Char of a string.
  // Objetivo: Regresar el ultimo caracter de una cadena.
end;

function LastCharPtr
  (var ASource: string): PChar;
var ACount: Cardinal;
begin
  Result := nil;
  if (ASource <> '') then
  begin
    ACount := System.Length(ASource);
    Result :=
      @(ASource[ACount]);
  end;
end;

function RangeToStrCopy
  (const Min, Max: Char): string;
var EachChar, L, H: Char; { s := 'a' .. 'z'; }
begin
  umlcstrings.Clear(Result);

  L := IfChar(Max < Min, Max, Min);
  H := IfChar(Min > Max, Min, Max);

  for EachChar := L to H do
  begin
    umlcstrings.ConcatChar(Result, EachChar);
  end;
  // Goal: Transform a range of characters into a "string".
  // Objetivo: Transformar un rango de caracteres en un "string".

  // Warning: "Min" must be lesser than "Max".
  // Advertencia: "Min" debera ser menor que "Max".
end;

function ReverseCopy
  (const ASource: string): string;
var I, C: Word;
begin
  Result := '';
  C :=
    umlcstrings.Length(ASource);
  for I := C downto 1 do
  begin
    Result := Result + ASource[i];
  end;
  // Objetivo: Regresa una copia de la cadena dada al reves.
  // Goal: Returns a copy of the given string backwards.
end;

function TryStrToChar
  (const ASource: string; var ADest: ansichar): Boolean;
begin
  Result :=
    (umlcstrings.Length(ASource) > 0);
  if (Result)
    then ADest := ASource[1];
  // Goal: To cast a "string", to a "ansichar".
  // Objetivo: Convertir un "string" en un "ansichar".
end;

function StrToCharDef
  (const ASource: string; const ADefValue: ansichar): ansichar;
begin
  Result := #0;
  if (not umlcstrings.TryStrToChar(ASource, Result))
    then Result := ADefValue;
  // Goal: To cast a "string", to a "char".
  // Objetivo: Convertir un "string" en un "char".
end;

function StrToChar
  (const ASource: string): ansichar;
begin
  Result := #0;
  if (not umlcstrings.TryStrToChar(ASource, Result)) then
  begin
    // raise error
  end;
  // Goal: To cast a "string", to a "ansichar".
  // Objetivo: Convertir un "string" en un "ansichar".
end;

function PtrToStr
  (const ASource: pointer): string;
begin
  Result := Pstring(ASource)^;
  // Goal: Copy an string into a dynamic string.
  // Objetivo: Copiar una cadena a una cadena dinamica.
end;

function StrToPtr
  (const ASource: string): pointer;
begin
  Result := @ASource;
  // Goal: Copy an string into a dynamic string.
  // Objetivo: Copiar una cadena a una cadena dinamica.
end;

function IsSubStrAt
  (const AHaystack: string;
   const ANeedle: string): Boolean;
var AIndex: Word;
begin
  Result := false;

  //AIndex :=
  //  umlcstrings.StrPosOf(AHaystack, ANeedle);
  //Result :=
  //  (AIndex > 0);
  // Goal: Returns if a string is contained by other string.
  // Objetivo: Regresa si una cadena esta contenida en otra.
end;

function SameStrAt
  (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;
var Match: Boolean; LastStr, LastSubStr, I, J: Integer;
begin
  LastStr := umlcstrings.Length(AHaystack);
  LastSubStr := umlcstrings.Length(ANeedle);
  I := 1;
  J := AIndex;

  Match := LastSubStr <= Succ(LastStr - AIndex);
//  Match := Succ(LastStr - AIndex) >= LastSubStr;
  // verificar que las subcadena sea de menor longuitud que la cadena destino

  if (Match) then
  repeat
    Match := umlcchars.SameText(ANeedle[i], AHaystack[j]);

    Inc(I);
    Inc(J);
  until not Match or (I > LastSubStr);
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // y que inicie a partir de la posicion indicada,
  // ignorando el caso sensitivo.
end;

function EqualStrAt
  (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;
var Match: Boolean; LastStr, LastSubStr, I, J: Integer;
begin
  LastStr := umlcstrings.Length(AHaystack);
  LastSubStr := umlcstrings.Length(ANeedle);
  I := 1;
  J := AIndex;

  Match := LastSubStr <= Succ(LastStr - AIndex);
//  Match := Succ(LastStr - AIndex) >= LastSubStr;
  // verificar que las subcadena sea de menor longuitud que la cadena destino

  if (Match) then
  repeat
    Match := (ANeedle[i] = AHaystack[j]);

    Inc(I);
    Inc(J);
  until not Match or (I > LastSubStr);
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // y que inicie a partir de la posicion indicada,
  // tomando en cuenta el caso sensitivo.
end;

function SameStrAtIn
  (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;
begin
  Result := false;
  AIndex := 1;
  //Result := Search(ANeedle, AHaystack, AIndex, {@}SameStrAt);
  Result := Search(ANeedle, AHaystack, AIndex, @SameStrAt);
  // Objetivo: Buscar una subcadena en otra cadena,
  // sin importar la posicion,
  // ignorando el caso sensitivo.
end;

function EqualStrAtIn
  (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;
begin
  Result := false;
  AIndex := 1;
  //Result := Search(ANeedle, AHaystack, AIndex, {@}EqualStrAt);
  Result :=
    Search(ANeedle, AHaystack, AIndex, @EqualStrAt);
  // Objetivo: Buscar una subcadena en otra cadena,
  // sin importar la posicion,
  // tomando en cuenta el caso sensitivo.
end;

function SameStr
  (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;
var Match: Boolean; Last1, Last2: Integer;
begin
  Last1 := umlcstrings.Length(AHaystack);
  Last2 := umlcstrings.Length(ANeedle);

  Match := (Last1 = Last2);
  // verificar que las cadenas sean de la misma longuitud

  if (Match)
    then Match := umlcstrings.SameText(ANeedle, AHaystack);
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // que sean de la misma longuitud,
  // ignorando el caso sensitivo.
end;

function EqualStr
  (const AHaystack: string; const ANeedle: string; var AIndex: Word): Boolean;
var Match: Boolean; Last1, Last2: Integer;
begin
  Last1 := umlcstrings.Length(AHaystack);
  Last2 := umlcstrings.Length(ANeedle);

  Match := (Last1 = Last2);
  // verificar que las cadenas sean de la misma longuitud

  if (Match)
    then Match := (ANeedle = AHaystack);
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // que sean de la misma longuitud,
  // tomando en cuenta el caso sensitivo.
end;

function Search
  (const AHaystack: string; const ANeedle: string; var AIndex: Word;
   const CompareStrAt: TCompareStrAt): Boolean;
var Len: Word; Found: Boolean;
begin
  Len :=
    umlcstrings.Length(AHaystack);
  Found  := FALSE;

  if (CompareStrAt <> nil) then
  while (not Found and (AIndex <= Len)) do
  begin
    if (CompareStrAt(ANeedle, AHaystack, AIndex))
      then Found := TRUE
      else Inc(AIndex);
  end;

  Result := Found;
  // Goal: Searches for a substring inside other string
  // beginning at the "AIndex" char and returns the next character,
  // and using the given comparison functor.
end;

(* global operators *)

procedure Assign
  (out   ADest: string;
   const ASource: string);
begin
  ADest := ASource;
end;
procedure ConcatChar
  (var ADest: string; const ASource: Char);
begin
  ADest := ADest + ASource;
  // Objetivo: Agregar  un caracter al final de la cadena dada.
  // Goal: Add a character at the end of the given string.
end;

procedure ConcatCharBack
  (var   ADest:   string;
   const ASource: Char);
begin
  ADest := ASource + ADest;
  // Objetivo: Agregar  un caracter al final de la cadena dada.
  // Goal: Add a character at the end of the given string.
end;

procedure ConcatStr
  (var   ADest:   string;
   const ASource: string);
begin
  ADest := ADest + ASource;
end;

procedure ConcatStrBack
  (var ADest: string; const ASource: string);
begin
  ADest := ASource + ADest;
end;

function Different
  (const A, B: string): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Greater
  (const A, B: string): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: string): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: string): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: string): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

(* equality operators *)

function Equal
  (const A, B: string): Boolean;
begin
  Result := (A = B);
  // Goal: Returns if 2 strings are equal.
  // Objetivo: Regresa si 2 cadenas son iguales.
end;

function SameText
  (const A, B: string): Boolean;
begin
  Result :=
    SysUtils.SameText(A, B);
  // Goal: Returns if 2 strings are equal, ignores sensitive case.
  // Objetivo: Regresa si 2 cadenas son iguales, ignorar caso sensitivo.
end;

function Compare
  (const A, B: string): TComparison;
begin
  if (A = B)
    then Result := cmpEqual
  else if (A < B)
    then Result := cmpLower
  else Result := cmpHigher
  // Goal: Returns the comparison between 2 strings.
  // Objetivo: Regresa la comparacion de 2 cadenas.
end;

function EqualByOptions
  (const A, B: string; Options: TStringOptions): Boolean;
begin
  case (Options) of
    soExactMatch:
      Result := (A = B);
    soForceMatch:
      Result := (umlcstrings.UppercaseCopy(A) = B);
    soForceFirst:
      Result := (umlcstrings.UppercaseCopy(A) = umlcstrings.UppercaseCopy(B));
    soForceLast:
      Result := (A = umlcstrings.UppercaseCopy(B));
    else
      Result := FALSE;
  end;
  // Goal: Returns if 2 strings are equals uppon the given options.
  // Objetivo: Regresa si 2 cadenas son iguales basado en las opciones dadas.
end;

function CompareByOptions
  (const A, B: string; Options: TStringOptions): TComparison;
begin
  case Options of
    soExactMatch:
      Result :=
        umlcstrings.Compare(A, B);
    soForceMatch:
      Result :=
        umlcstrings.Compare
          (umlcstrings.UppercaseCopy(A), umlcstrings.UppercaseCopy(B));
    soForceFirst:
      Result :=
        umlcstrings.Compare
          (umlcstrings.UppercaseCopy(A), B);
    soForceLast:
      Result :=
        umlcstrings.Compare
          (A, umlcstrings.UppercaseCopy(B));
    else
      Result := cmpHigher;
  end;
  // Goal: Returns if 2 strings are equals upon the given options.
  // Objetivo: Regresa si 2 cadenas son iguales basado en las opciones dadas.
end;

(* global procedures *)

procedure AssignLength
  (var ADest: string; ACount: Word);
begin
  System.SetLength(ADest, ACount);
end;

procedure Clear
  (out ADest: string);
begin
  ADest := EmptyStr;
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;

function TryAssignCopyAt
  (out   ADest:        string;
   const ASource:      string;
   const ASourceIndex: word): Boolean;
var ASourceLen: word;
begin
  ASourceLen :=
    umlcstrings.Length(ASource);
  Result :=
   ((ASourceIndex > 0) and (ASourceLen > 0));
  if (Result) then
  begin
    umlcstrings.Clear(ADest);

    System.Move(ASource[ASourceIndex], ADest[1], ASourceLen);

    umlcstrings.AssignLength(ADest, ASourceLen);
  end;
  // Goal: To copy a substring at the given index.
end;

function TryAssignCopyAtCount
  (out   ADest:        string;
   const ASource:      string;
   const ASourceIndex: word;
   const ASourceCount: word): Boolean;
var ASourceLen, ARealCount: word;
begin
  ASourceLen :=
    umlcstrings.Length(ASource);
  Result :=
   ((ASourceIndex > 0) and(ASourceCount > 0) and (ASourceLen > 0));
  if (Result) then
  begin
    umlcstrings.Clear(ADest);

    ARealCount :=
      Math.Min((ASourceLen - ASourceIndex), ASourceLen);

    System.Move(ASource[ASourceIndex], ADest[1], ARealCount);
    umlcstrings.AssignLength(ADest, ARealCount);
  end;
end;

function TryAssignCopyCount
  (out   ADest:        string;
   const ASource:      string;
   const ASourceCount: word): Boolean;
var ASourceLen, ARealCount: word;
begin
  ASourceLen :=
    umlcstrings.Length(ASource);
  Result :=
   ((ASourceCount > 0) and (ASourceLen > 0));
  if (Result) then
  begin
    umlcstrings.Clear(ADest);

    ARealCount :=
      Math.Min(ASourceLen, ASourceCount);

    System.Move(ASource[1], ADest[1], ARealCount);
    umlcstrings.AssignLength(ADest, ARealCount);
  end;
  // Objetivo: Copiar el contenido de una subcadena a un nueva cadena.
  // Goal: Copy the contents of a substring into a new string.
end;
procedure AssignCopyAt
  (out   ADest:        string;
   const ASource:      string;
   const ASourceIndex: word);
begin
  if (not TryAssignCopyAt(ADest, ASource, ASourceIndex)) then
  begin
    // raise error
  end;
end;

procedure AssignCopyAtCount
  (out   ADest:        string;
   const ASource:      string;
   const ASourceIndex: word;
   const ASourceCount: word);
begin
  if (not TryAssignCopyAtCount(ADest, ASource, ASourceIndex, ASourceCount)) then
  begin
    // raise error
  end;
end;

procedure AssignCopyCount
  (out   ADest:        string;
   const ASource:      string;
   const ASourceCount: word);
begin
  if (not TryAssignCopyCount(ADest, ASource, ASourceCount)) then
  begin
    // raise error
  end;
end;

procedure AssignAt
  (var ADest: string; const ASource: string; AIndex, ACount: Word);
begin
  umlcstrings.Clear(ADest);
  umlcstrings.AssignLength(ADest, ACount);
  System.Move(ASource[AIndex], ADest[1], ACount);
  // Objetivo: Copiar el contenido de una cadena a un nueva cadena.
  // Goal: Copy the contents of a string into a new string.
end;

procedure AssignLeft
  (var ADest: string; const ASource: string; const ACount: Word);
begin
  umlcstrings.AssignAt
    (ADest, ASource, 1, ACount);
  // Goal: Returns the leftmost characters of "ADest".
  // Objetivo: Regresa los caracteres mas a la izquierda de "ADest".
end;

procedure AssignRight
  (var ADest: string; const ASource: string; const ACount: Word);
var AIndex: Word;
begin
  AIndex :=
    Succ(Length(ASource)-ACount);
  umlcstrings.AssignAt
    (ADest, ASource, AIndex, ACount);
  // Goal: Returns the rightmost characters of "ADest".
  // Objetivo: Regresa los caracteres mas a la derecha de "ADest".
end;

procedure LastCharCountPtr
  (out   ADestPtr: PChar;
   out   ACount:   Word;
   const ASource:  string);
begin
  ADestPtr := nil;
  ACount   := 0;
  if (ASource <> '') then
  begin
    ACount := umlcstrings.Length(ASource);
    ADestPtr :=
      @(ASource[ACount]);
  end;
end;

procedure AssignReverse
  (out ADest: string; const ASource: string);
var CanContinue: Boolean; P: pchar; ACount: Word;
begin
  umlcstrings.Clear(ADest);
  CanContinue :=
    (not umlcstrings.IsEmpty(ASource));
  if (CanContinue) then
  begin
    umlcstrings.LastCharCountPtr
      (P, ACount, ASource);
    while ((P <> nil) and (ACount > 0)) do
    begin
      umlcstrings.ConcatChar(ADest, P^);

      System.Dec(P);
      System.Dec(ACount);
    end;
  end;
end;

(* global random string functions *)

function Internal_RandomRange
  (const AMin, AMax: Byte ): Byte;
begin
  Result :=
    System.Random(AMax-AMin)+AMin;
  // Objetivo: Obtener un numero aleatorio en el rango entero indicado.
  // Goal: Obtain a random number in the indicated integer range.
end;

function RandomAlphaNumString
  (const ACount: Word): string;
var i: Integer;
begin
  Result :=
    umlcstrings.StringOfCharCopy( #32, ACount );

  // keep first character as letter
  Result[1] :=
    Chr(umlcstrings.Internal_RandomRange(65, 90));

  for i := 2 to ACount do
  begin
    Result[i] :=
       Chr(umlcstrings.Internal_RandomRange(48, 57));
  end;
  // Objetivo: Obtener una cadena de caracteres alfanumericos aleatorios,
  // con la longuitud indicada.
  // Goal: Obtain a string of alphanumeric random characters,
  // with the given length.
end;

function RandomAlphaString
  (const ACount: Word): string;
var i: Integer;
begin
  Result :=
    umlcstrings.StringOfCharCopy( #32, ACount );

  for i := 1 to ACount do
  begin
    Result[i] :=
       Chr(umlcstrings.Internal_RandomRange(65, 90));
  end;
  // Objetivo: Obtener una cadena de caracteres alfabeticos aleatorios,
  // con la longuitud indicada.
  // Goal: Obtain a string of alphabetic random characters,
  // with the given length.
end;

function RandomNumString
  (const ACount: Word): string;
var i: Word;
begin
  Result :=
    umlcstrings.StringOfCharCopy( #32, ACount );
  for i := 1 to ACount do
  begin
    Result[i] := Chr(umlcstrings.Internal_RandomRange(48, 57));
  end;
  // Objetivo: Obtener una cadena de caracteres numericos aleatorios,
  // con la longuitud indicada.
  // Goal: Obtain a string of random numerical characters,
  // with the given string.
end;


end.

