(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwidestrparsers;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Quick Text Search and Parsing operations.
 ** The character encoding is A.N.S.I.
 ** The quantity of items is NOT restricted to 255 characters.
 ** The string is internally managed as memory referenced.
 **************************************************************************
 **)

interface

uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdstrtypes,
  umlcwidechars,
  umlcwidecharsets,
  umlcwidecharsetconsts,
  umlcwidestrings,
  dummy;

(* global functions *)



implementation

(* global functions *)




end.

