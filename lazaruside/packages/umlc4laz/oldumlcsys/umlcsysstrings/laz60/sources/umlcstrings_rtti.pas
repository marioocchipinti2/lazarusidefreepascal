(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstrings_rtti;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules,
  dummy;


// ---

const

  MOD_umlcstrings_rtti : TUMLCModule =
    ($A7,$7A,$AA,$97,$E3,$D2,$5E,$47,$85,$A8,$E5,$54,$97,$1B,$DB,$EC);

// ---


implementation

end.

