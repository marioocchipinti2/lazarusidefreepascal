(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansimemos;

(* English *)

(**
 ** "ansimemo" is a primitive type that allows the use of
 ** variable length strings, without the use of references.

 ** Replaces the "infamous" delphi string type
 ** that allocs & release memory arbitrary.

 ** Its used, similar to the standard pascal string,
 ** using a character array,
 ** with a reserved field for the current quantity,
 ** but, supports more than 256 bytes.
 *)

(* Espanol *)

(**
 ** "ansimemo" es un tipo primitivo que permite el uso de
 ** cadenas de longuitud variable sin el uso de referencias.

 ** su uso es similar al string standard de pascal,
 ** se maneja como un arreglo de caracteres,
 ** con un campo reservado para la cantidad actual,
 ** pero soporta una cantidad mayor que los 256 bytes.

 ** Reemplaza al "infame" tipo string de delphi
 ** que reserva y libera memoria arbitrariamente.
 *)

interface
uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysUtils, Math,
  umlccomparisons,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcansichars,
  umlcansicharsets, umlcansicharsetconsts,
  dummy;

type

(* global types *)

   ansimemo = type pointer;

   TansimemoArray = array[0 .. 65535] of ansichar;
   PansimemoArray = ^TansimemoArray;
   // Used for access individual characters
   // Utilizado para accesar caracteres individuales

   TansimemoHeader = record
     Len:    Word;
     MaxLen: Word;
   end;
   // ansimemo's fixed type (header)
   // Tipo fijo del ansimemo (encabezado)

   TansimemoRec = record
     Header: TansimemoHeader;
     Items:  TansimemoArray;
   end;
   PansimemoRec = ^TansimemoRec;
   // ansimemo's variable length type
   // Tipo de longuitud variable del ansimemo

(* global functions *)

  function QuotedUppercaseCopy
   (const ASource: ansimemo;
    const LeftQuote, RightQuote: ansichar): ansimemo; overload;
  function QuotedLowercaseCopy
   (const ASource: ansimemo;
    const LeftQuote, RightQuote: ansichar): ansimemo; overload;

  function getTrimLeft(const ASource: ansimemo): ansimemo; overload;
  function getTrimRight(const ASource: ansimemo): ansimemo; overload;
  function getTrim(const ASource: ansimemo): ansimemo; overload;

  function getUnTrimLeft
   (const ASource: ansimemo; ACount: Integer): ansimemo; overload;
  function getUnTrimRight
   (const ASource: ansimemo; ACount: Integer): ansimemo; overload;

  procedure ReplaceQuotedUppercase
   (const ASource: ansimemo; const LeftQuote, RightQuote: ansichar); overload;
  procedure ReplaceQuotedLowercase
   (const ASource: ansimemo; const LeftQuote, RightQuote: ansichar); overload;

  procedure setTrimLeft(var ADest: ansimemo); overload;
  procedure setTrimRight(var ADest: ansimemo); overload;
  procedure setTrim(var ADest: ansimemo); overload;

  procedure setUnTrimLeft(var ADest: ansimemo; ACount: Integer); overload;
  procedure setUnTrimRight(var ADest: ansimemo; ACount: Integer); overload;

(* global functions *)

  function Length(const ASource: ansimemo): Word;
  function MaxLength(const ASource: ansimemo): Word;

  function SameText(const A, B: ansimemo): Boolean;

  function UppercaseCopy(const ASource: ansimemo): ansimemo; overload;
  function LowercaseCopy(const ASource: ansimemo): ansimemo; overload;
  function TogglecaseCopy(const ASource: ansimemo): ansimemo; overload;
  function CapitalizeCopy(const ASource: ansimemo): ansimemo; overload;

  function IsEmpty(const ASource: ansimemo): Boolean; overload;
  function IsStringOfChar(const S: ansimemo; C: ansichar): Boolean;

  function SameChar(const ASource: ansichar; const ACount: Word): ansimemo;

  function CharPos
    (const AHayStack: ansimemo; const ANeedle: ansichar): Word; overload;
  function CharPosReverse
    (const AHayStack: ansimemo; const ANeedle: ansichar): Word; overload;

  function Pos
    (const AHayStack: ansimemo; const ANeedle: ansistring): Word; overload;
  function Pos
    (const AHayStack: ansimemo; const ANeedle: ansimemo): Word; overload;

  function StartsWith
    (const SubStr: ansimemo; const S: ansimemo): Boolean; overload;
  function FinishesWith
    (const SubStr: ansimemo; const S: ansimemo): Boolean; overload;

  function StartsWith
    (const SubStr: ansistring; const S: ansimemo): Boolean; overload;
  function FinishesWith
    (const SubStr: ansistring; const S: ansimemo): Boolean; overload;

  function LeftCopy(const ASource: ansimemo; const ACount: Word): ansimemo;
  function RightCopy(const ASource: ansimemo; const ACount: Word): ansimemo;

  procedure Clear(var ADest: ansimemo); overload;

  procedure Copy
     (const ASource: ansimemo; AIndex, ACount: Word; var ADest: ansistring); overload;
  procedure Copy
     (const ASource: ansimemo; AIndex, ACount: Word; var ADest: ansimemo); overload;

  function IsSubStr
    (const AHaystack, ANeedle: ansimemo): Boolean; overload;
  function IsIdentifier
    (const ASource: ansimemo): Boolean;
  function RangeToStr
    (const Min, Max: ansichar): ansimemo;

  function ReplaceAllChars
    (const ASource: ansimemo; A, B: ansichar): ansimemo; overload;
//  function ReplaceChars
//    (const ASource: ansimemo; Source: ansicharset; Dest: ansichar): ansimemo; overload;

  function ReplaceStrALL
    (var AHaystack: ansimemo; const ASourceNeedle, ADestNeedle: ansistring): Integer;
  function ReplaceStr
    (var AHaystack: ansimemo; const ASourceNeedle, ADestNeedle: ansistring): Integer;

  function ReplaceCopy(const FullStr, SubStr, AValue: ansistring): ansimemo;
  function DeleteCopy(const FullStr, SubStr: ansistring): ansimemo;

  procedure ReplaceUppercase(var ADest: ansimemo); overload;
  procedure ReplaceLowercase(var ADest: ansimemo); overload;
  procedure ReplaceTogglecase(var ADest: ansimemo); overload;
  procedure ReplaceCapitalize(var ADest: ansimemo); overload;

  function StrToMemo(const ASource: ansistring): ansimemo;
  function MemoToStr(const ASource: ansimemo): ansistring;

  procedure AssignStr(var ADest: ansimemo; const ASource: ansistring);
  procedure AssignMemo(var ADest: ansimemo; const ASource: ansimemo);

  function getAt(const List: ansimemo; const AIndex: Word): ansichar;
  procedure setAt(const List: ansimemo; const AIndex: Word; AValue: ansichar);

  function PtrOfMemo(const ASource: ansimemo): pointer;

  procedure Append(var ADest: ansimemo; const ASource: ansichar);
  procedure InsertAt(var ADest: ansimemo; AIndex: Word; AValue: ansichar);
  procedure DeleteAt(var ADest: ansimemo; AIndex: Word);

  procedure Concat(var ADest: ansimemo; const ASource: ansistring); overload;
  procedure Concat(var ADest: ansimemo; const ASource: ansimemo); overload;

  procedure Insert
    (const ASource: ansistring; var S: ansimemo; AIndex: Word); overload;
  procedure Insert
    (const ASource: ansimemo; var S: ansimemo; AIndex: Word); overload;
  procedure Delete(var S: ansimemo; AIndex, ACount: Word);

  procedure AllocMemo(var ADest: ansimemo; const ACount: Word);
  procedure FreeMemo(var ADest: ansimemo);

  function DuplicateMemo(const ASource: ansimemo): ansimemo;
  procedure ReAllocMemo(var ADest: ansimemo; const ACount: Word);

(* global operators *)

  { = } function Equal(const A, B: ansimemo): Boolean;
  { > } function Greater(const A, B: ansimemo): Boolean;
  { < } function Lesser(const A, B: ansimemo): Boolean;

  function Compare(const A, B: ansimemo): TComparison;

implementation

(* global functions *)

function QuotedUppercaseCopy
  (const ASource: ansimemo; const LeftQuote, RightQuote: ansichar): ansimemo;
begin
  Result := umlcansimemos.DuplicateMemo(ASource);
  umlcansimemos.ReplaceQuotedUppercase(Result, LeftQuote, RightQuote);
  // Goal: Returns a uppercase copy of the given memo,
  // without modifying delimited subansimemos.

  // Objetivo: Regresa una copia en mayusculas del memo dado,
  // sin modificar a los submemos delimitados.
end;

function QuotedLowercaseCopy
  (const ASource: ansimemo; const LeftQuote, RightQuote: ansichar): ansimemo;
begin
  Result := umlcansimemos.DuplicateMemo(ASource);
 umlcansimemos.ReplaceQuotedLowercase(Result, LeftQuote, RightQuote);
  // Goal: Returns a lowercase copy of the given memo,
  // without modifying delimited substrings.

  // Objetivo: Regresa una copia en minusculas del memo dado,
  // sin modificar a los submemos delimitados.
end;

function getTrimLeft(const ASource: ansimemo): ansimemo;
begin
  Result := nil;
  // Goal: Returns a copy of the given memo without leading spaces.
  // Objetivo: Regresa una copia de la cadena dada sin espacios iniciales.
end;

function getTrimRight(const ASource: ansimemo): ansimemo;
begin
  Result := nil;
  // Goal: Returns a copy of the given memo without trailing spaces.
  // Objetivo: Regresa una copia de la cadena dada sin espacios finales.
end;

function getTrim(const ASource: ansimemo): ansimemo;
begin
  Result := nil;
  // Goal: Returns a copy of the given memo
  // without leading & trailing spaces.

  // Objetivo: Regresa una copia de la cadena dada
  // sin espacios iniciales y finales.
end;

function getUnTrimLeft(const ASource: ansimemo; ACount: Integer): ansimemo;
begin
  Result := nil;
  // Goal: Returns a copy of the given memo plus leading spaces.
  // Objetivo: Regresa una copia de la cadena dada mas espacios iniciales.
end;

function getUnTrimRight(const ASource: ansimemo; ACount: Integer): ansimemo;
begin
  Result := nil;
  // Goal: Returns a copy of the given memo plus trailing spaces.
  // Objetivo: Regresa una copia de la cadena dada mas espacios finales.
end;

procedure ReplaceQuotedUppercase
  (const ASource: ansimemo; const LeftQuote, RightQuote: ansichar);
var InsideString: Boolean; P: pansichar;
begin
  InsideString := FALSE; P := PtrOfMemo(ASource);
  while not (P^ = #0) do
  begin
    if (InsideString)
      then InsideString := not (P^= RightQuote)
      else InsideString := (P^= LeftQuote);
    // initial or final delimiter found ?
    // delimitador inicial o final encontrado ?

    if (not InsideString)
      then umlcansichars.ReplaceUppercase(P^);
    // replace characters
    // reemplazar caracteres

    Inc(P);
  end;
  // Goal: Changes the given memo into uppercase,
  // without modifying delimited subansimemos.

  // Objetivo: Cambia el memo dado a mayusculas.
  // sin modificar a los subansimemos delimitados.
end;

procedure ReplaceQuotedLowercase
  (const ASource: ansimemo; const LeftQuote, RightQuote: ansichar); overload;
var InsideString: Boolean; P: pansichar;
begin
  InsideString := FALSE; P := PtrOfMemo(ASource);
  while not (P^ = #0) do
  begin
    if (InsideString)
      then InsideString := not (P^= RightQuote)
      else InsideString := (P^= LeftQuote);
    // initial or final delimiter found ?
    // delimitador inicial o final encontrado ?

    if (not InsideString)
      then umlcansichars.ReplaceLowercase(P^);
    // replace characters
    // reemplazar caracteres

    Inc(P);
  end;
  // Goal: Changes the given memo into lowercase,
  // without modifying delimited subansimemos.

  // Objetivo: Cambia el memo dado a minusculas.
  // sin modificar a los subansimemos delimitados.
end;

procedure setTrimLeft(var ADest: ansimemo);
begin
//  AValue := SysUtils.TrimLeft(AValue);
  // Goal: Returns the given memo without leading spaces.
  // Objetivo: Regresa la cadena dada sin espacios iniciales.
end;

procedure setTrimRight(var ADest: ansimemo);
begin
//  AValue := SysUtils.TrimRight(AValue);
  // Goal: Returns the given memo without trailing spaces.
  // Objetivo: Regresa la cadena dada sin espacios finales.
end;

procedure setTrim(var ADest: ansimemo);
begin
//  AValue := SysUtils.Trim(AValue);
  // Goal: Returns the given memo without leading & trailing spaces.
  // Objetivo: Regresa la cadena dada sin espacios iniciales y finales.
end;

procedure setUnTrimLeft(var ADest: ansimemo; ACount: Integer);
var SourceRec: PansimemoRec absolute ADest;
begin
  // Goal: Returns the given memo plus leading spaces.
  // Objetivo: Regresa la cadena dada mas espacios iniciales.
end;

procedure setUnTrimRight(var ADest: ansimemo; ACount: Integer);
var ADestRec: PansimemoRec absolute ADest; L, C: Integer;
begin
  if (Assigned(ADestRec)) then
  begin
    if (ACount <= ADestRec^.Header.MaxLen) then
    begin
      L := ADestRec^.Header.Len;
      C := (ACount- L);

      if (C > 0) then
      begin
        System.FillChar(ADestRec^.Items[L], C, #32);
        ADestRec^.Header.Len := ACount;
      end;
    end;
  end;
  // Goal: Returns the given ansistring plus trailing spaces.
  // Objetivo: Regresa la cadena dada mas espacios finales.
end;

(* global functions *)

function Length(const ASource: ansimemo): Word;
begin
  Result := 0;
  if (Assigned(ASource))
    then Result := PansimemoRec(ASource)^.Header.Len;
  // Objetivo: Regresa el numero utilizado de caracteres.
  // Goal: Returns the used number of characters.
end;

function MaxLength(const ASource: ansimemo): Word;
begin
  Result := 0;
  if (Assigned(ASource))
    then Result := PansimemoRec(ASource)^.Header.MaxLen;
  // Objetivo: Regresa el numero maximo de caracteres.
  // Goal: Returns the max number of characters.
end;

function SameText(const A, B: ansimemo): Boolean;
var AIndex, Len: Integer; C, D: ansichar;
begin
  Result := TRUE;
  AIndex := 0; Len :=umlcansimemos.Length(A);
  while ((AIndex < Len) and Result) do
  begin
    C := getAt(A, AIndex);
    D := getAt(B, AIndex);
    Result := umlcansichars.SameText(C, D);
    Inc(AIndex);
  end;
//  Result := SysUtils.SameText(A, B);
  // Goal: Returns if 2 memos are equal, ignores sensitive case.
  // Objetivo: Regresa si 2 memos son iguales, ignora caso sensitivo.
end;

function UppercaseCopy(const ASource: ansimemo): ansimemo;
begin
  Result := umlcansimemos.DuplicateMemo(ASource);
  ReplaceUppercase(Result);
  // Goal: Returns a uppercase copy of the given memo.
  // Objetivo: Regresa una copia en mayusculas del memo dado.
end;

function LowercaseCopy(const ASource: ansimemo): ansimemo;
begin
  Result := umlcansimemos.DuplicateMemo(ASource);
  ReplaceLowercase(Result);
  // Goal: Returns a lowercase copy of the given memo.
  // Objetivo: Regresa una copia en minusculas del memo dado.
end;

function TogglecaseCopy(const ASource: ansimemo): ansimemo;
begin
  Result := umlcansimemos.DuplicateMemo(ASource);
  ReplaceTogglecase(Result);
  // Goal: Returns a copy of the given string.
  // Objetivo: Regresa una copia de la cadena dada.
end;

function CapitalizeCopy(const ASource: ansimemo): ansimemo;
begin
  Result := umlcansimemos.DuplicateMemo(ASource);
  ReplaceCapitalize(Result);
  // Goal: Returns a copy with uppercase initials of the given ansistring.
  // Objetivo: Regresa una copia con iniciales en mayusculas de la cadena dada.
end;

function IsEmpty(const ASource: ansimemo): Boolean;
var ansimemoRec: PansimemoRec absolute ASource;
begin
  Result := (ansimemoRec^.Header.Len = 0);
  // Goal: Returns if a ansimemo is empty.
  // Objetivo: Regresa si un ansimemo esta vacio.
end;

function IsStringOfChar(const S: ansimemo; C: ansichar): Boolean;
var I, L: Integer; Match: Boolean;
begin
  L :=umlcansimemos.Length(S);

  Result := (L > 0);
  if (Result) then
  begin
    I := 1; Match := TRUE;
    while ((I <= L) and (Match)) do
    begin
      Match := (getAt(S ,i) = C);
      Inc(I);
    end;

    Result := Match;
  end;
  // Objetivo: Regresa si una cadena esta compuesta solo del mismo caracter.
  // Goal: Returns if a string is composed with the same character.
end;

function SameChar(const ASource: ansichar; const ACount: Word): ansimemo;
var ansimemoRec: PansimemoRec absolute ASource;
begin
  AllocMemo(Result, ACount);
  System.FillChar(ansimemoRec^.Items, ACount, ASource);
  // Goal: Returns a ansimemo of the same ansichar.
  // Objetivo: Regresa una cadena del mismo caractaer.
end;

function CharPos
  (const AHayStack: ansimemo; const ANeedle: ansichar): Word;
var ansimemoRec: PansimemoRec absolute AHayStack; Len: Word; Found: Boolean;
begin
  Len := Pred(umlcansimemos.Length(AHayStack));
  Result := 0; Found := FALSE;

  while (not Found) and (Result < Len) do
  begin
    Found := (ansimemoRec^.Items[Result] = ANeedle);
    Inc(Result);
  end;

  if (Found)
    then Dec(Result)
    else Result := 0;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the AIndex of the first ocurrence of "Substr".
end;

function CharPosReverse
  (const AHayStack: ansimemo; const ANeedle: ansichar): Word;
var ansimemoRec: PansimemoRec absolute AHayStack; Found: Boolean;
begin
  Result := umlcansimemos.Length(AHayStack);
  Found  := FALSE;

  while (not Found) and (Result > 0) do
  begin
    Found := (ansimemoRec^.Items[Result] = ANeedle);
    Dec(Result);
  end;

  if (Found)
    then Inc(Result)
    else Result := 0;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the AIndex of the first ocurrence of "Substr".
end;

function Pos(const AHayStack: ansimemo; const ANeedle: ansistring): Word;
begin
  Result := 0;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the AIndex of the first ocurrence of "Substr".
end;

function Pos(const AHayStack: ansimemo; const ANeedle: ansimemo): Word;
begin
  Result := 0;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the AIndex of the first ocurrence of "Substr".
end;

function StartsWith(const SubStr: ansimemo; const S: ansimemo): Boolean;
begin
  // @to-do:
  Result := false;
end;

function FinishesWith(const SubStr: ansimemo; const S: ansimemo): Boolean;
begin
  // @to-do:
  Result := false;
end;

function StartsWith(const SubStr: ansistring; const S: ansimemo): Boolean;
begin
  // @to-do:
  Result := false;
end;

function FinishesWith(const SubStr: ansistring; const S: ansimemo): Boolean;
begin
  // @to-do:
  Result := false;
end;

function LeftCopy(const ASource: ansimemo; const ACount: Word): ansimemo;
var ansimemoRec: PansimemoRec absolute ASource; Source, Dest: pansichar;
begin
  AllocMemo(Result, ACount);
  Source := PtrOfMemo(ASource);
  Dest   := PtrOfMemo(Result);
  // obtain address of source & destination characters
  // obtener diorecccion de caracteres fuente y destino

  System.Move(Source^, Dest^, ACount);
  // trasnfer characters
  // transferir caracteres

  ansimemoRec^.Header.Len := ACount;
  // update length
  // actualizar longuitud

  // Goal: Returns the leftmost characters of "AValue".
  // Objetivo: Regresa los caracteres mas a la izquierda de "AValue".
end;

function RightCopy(const ASource: ansimemo; const ACount: Word): ansimemo;
var SourceRec, DestRec: PansimemoRec;
    Source, Dest: pansichar; AIndex: Word;
begin
  AllocMemo(Result, ACount);
  SourceRec := Result;
  Source    := @(SourceRec^.Items[0]);
  // obtain address of source characters
  // obtener direcccion de caracteres fuente

  AIndex  := umlcansimemos.Pos
    (ASource, ansinullchar) - ACount;
  DestRec := ASource;
  Dest    := @(DestRec^.Items[AIndex]);
  // obtain address of destination characters
  // obtener direcccion de caracteres destino

  System.Move(Source^, Dest^, ACount);
  Result := DestRec;
  // Goal: Returns the rightmost characters of "AValue".
  // Objetivo: Regresa los caracteres mas a la derecha de "AValue".
end;

procedure Clear(var ADest: ansimemo);
begin
  //Clear(AValue);
  // Goal: Clear a ansimemo.
  // Objetivo: Limpia un ansimemo.
end;

procedure Copy(const ASource: ansimemo; AIndex, ACount: Word; var ADest: ansistring);
var SourceRec: PansimemoRec absolute ASource;
begin
  if (Assigned(SourceRec)) then
  begin
    System.SetLength(ADest, ACount);
    System.Move(SourceRec^.Items[AIndex], ADest[1], ACount);
  end;
  // Objetivo: Copiar el contenido de un memo a un nuevo string.
  // Goal: Copy the contents of a amemo into a new string.
end;

procedure Copy(const ASource: ansimemo; AIndex, ACount: Word; var ADest: ansimemo);
var SourceRec: PansimemoRec absolute ASource;
    DestRec:   PansimemoRec absolute ADest;
begin
  if (Assigned(ADest))
    then FreeMemo(ADest);
  // limpiar valores previos
  // clear previous AValues

  if (Assigned(ASource)) then
  begin
    AllocMemo(ADest, ACount);
    System.Move(SourceRec^.Items[AIndex], DestRec^.Items[0], ACount);
  end;
  // Objetivo: Copiar el contenido de un ansimemo a otro nuevo ansimemo.
  // Goal: Copy the contents of a ansimemo into a new ansimemo.
end;

function IsSubStr
  (const AHaystack, ANeedle: ansimemo): Boolean;
var AIndex: Integer;
begin
  AIndex := Pos(AHaystack, ANeedle);
  Result := (AIndex > 0);
  // Goal: Returns if a ansimemo is contained by other ansimemo.
  // Objetivo: Regresa si una cadena esta contenida en otra.
end;

function IsIdentifier
  (const ASource: ansimemo): Boolean;
var ansimemoRec: PansimemoRec absolute ASource;
    I, L: Integer;
begin
  Result := FALSE;
  L :=umlcansimemos.Length(ASource);
  
  if ((L = 0) or not IsMember(AlphaSet, ansimemoRec^.Items[0]))
    then Exit;

  for I := 1 to L do
    if (not IsMember(IDSet, ansimemoRec^.Items[I]))
      then Exit;

  Result := TRUE;
  // Goal: To return if a ansimemo is a valid identifier.
  // Objetivo: Regresar si una cadena es un identificador valido.
end;

function RangeToStr
  (const Min, Max: ansichar): ansimemo;
var ansimemoRec: PansimemoRec absolute Result;
    AIndex: Word; C: ansichar; ACount: Byte; { s := 'a' .. 'z'; }
begin
  ACount := Succ(Ord(Max) - Ord(Min));
  AllocMemo(Result, ACount);

  AIndex := 0;
  for C := Min to Max do
  begin
    ansimemoRec^.Items[AIndex] := C;
    Inc(AIndex);
  end;
  // Goal: Transform a range of characters into a "ansimemo".
  // Objetivo: Transformar un rango de caracteres en un "ansimemo".

  // Warning: "Min" must be lesser than "Max".
  // Advertencia: "Min" debera ser menor que "Max".
end;

function ReplaceAllChars
  (const ASource: ansimemo; A, B: ansichar): ansimemo;
var ansimemoRec: PansimemoRec absolute ASource;
    AIndex, Len: Integer; P: pansichar;
begin
  Result := umlcansimemos.DuplicateMemo(ansimemoRec);
  Len    := Pred(Length(ansimemoRec));
  P      := PtrOfMemo(ansimemoRec);
  for AIndex := 0 to Len do
    if (P^ = A)
      then P^ := B;
  // Goal: Replace a specific character from a ansimemo.
  // Objetivo: Reemplazar un caracter en especifico de un ansimemo.
end;

(*
function ReplaceChars
 (const ASource: ansimemo; Source: ansicharset; Dest: ansichar): ansimemo;
var ansimemoRec: PansimemoRec absolute ADest;
    AIndex, Len: Integer; P: pansichar;
begin
  Result := umlcansimemos.DuplicateMemo(ansimemoRec);
  Len    := Pred(Length(ansimemoRec));
  P      := PtrOfMemo(ansimemoRec);
  for AIndex := 0 to Len do
    if (P^ in Source)
      then P^ := Dest;
  // Goal: Replace a specific character set from a ansimemo.
  // Objetivo: Reemplazar un conjunto caracter en especifico de un ansimemo.
end;
*)

function ReplaceStrALL
  (var AHaystack: ansimemo; const ASourceNeedle, ADestNeedle: ansistring): Integer;
var Finished: Boolean;
begin
  Result := 0;
  repeat
    Finished := (ReplaceStr(AHaystack, ASourceNeedle, ADestNeedle) = 0);
    if (not Finished)
      then Inc(Result);
  until Finished;
  // Goal: Replaces all the "SubStr" found in "FullStr" with "AValue".
  // Objetivo: Reemplaza todas las "SubStr" encontradas en "FullStr" por "AValue".}
end;

function ReplaceStr
  (var AHaystack: ansimemo; const ASourceNeedle, ADestNeedle: ansistring): Integer;
var B, ACount: Integer; Temp: ansimemo;
begin
  Result := Pos(AHaystack, ASourceNeedle);
  if (Result > 0) then
  begin
    B := Result + System.Length(ASourceNeedle);
    ACount := umlcansimemos.Length(AHaystack) - System.Length(ASourceNeedle);

    if (Result <> 0) then
    begin
     umlcansimemos.Copy(AHaystack, 0, Pred(Result), AHaystack);
     umlcansimemos.Concat(AHaystack, ADestNeedle);
     umlcansimemos.Copy(AHaystack, B, ACount, Temp);
     umlcansimemos.Concat(AHaystack, Temp);
    end;
  end;
  // Goal: Replaces the first "SubStr" found in "FullStr" with "AValue".
  // Objetivo: Reemplaza la primera "SubStr" encontrada en "FullStr" por "AValue".}
end;

function ReplaceCopy
  (const FullStr, SubStr, AValue: ansistring): ansimemo;
//var A, B, ACount: Integer;
begin
  Result := nil;
(*
  A := System.Pos(SubStr, FullStr);
  if (A > 0) then
  begin
    B := A + System.Length(SubStr);
    ACount := System.Length(FullStr) - System.Length(SubStr);

    if (A = 0)
      then Result := FullStr
      else
    Result := Copy(FullStr, 0, Pred(A)) + AValue + Copy(FullStr, B, ACount);
  end;
*)  
  // Goal: Returns a copy of the ansistring with the given SubStr replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function DeleteCopy(const FullStr, SubStr: ansistring): ansimemo;
begin
  Result := ReplaceCopy(FullStr, SubStr, '');
  // Goal: Returns a copy of the ansistring with the given SubStr deleted.
  // Objetivo: Regresa una copia de la cadena con la subcadena eliminada.
end;

procedure ReplaceUppercase(var ADest: ansimemo);
var Len: Integer; P: pansichar;
begin
  Len := umlcansimemos.Length(ADest);
  P   := PtrOfMemo(ADest);
{$IFDEF MSWINDOWS}
  Windows.CharUpperBuffA(P, Len);
{$ENDIF}
{$IFDEF LINUX}
//
{$ENDIF}
  // Goal: Changes the given memo into uppercase.
  // Objetivo: Cambia el memo dado a mayusculas.
end;

procedure ReplaceLowercase(var ADest: ansimemo);
var Len: Integer; P: pansichar;
begin
  Len := umlcansimemos.Length(ADest);
  P   := PtrOfMemo(ADest);
{$IFDEF MSWINDOWS}
  Windows.CharLowerBuffA(P, Len)
{$ENDIF}
{$IFDEF LINUX}
//
{$ENDIF}
  // Goal: Changes the given memo into lowercase.
  // Objetivo: Cambia el memo dado a minusculas.
end;

procedure ReplaceTogglecase(var ADest: ansimemo);
var P: pansichar;
begin
  P := PtrOfMemo(ADest);
  while ((P <> nil) and (P^ <> #0)) do
  begin
{$IFDEF MSWINDOWS}
    if (IsCharLowerA(P^))
      then CharUpperBuffA(P, 1)
      else CharLowerBuffA(P, 1);
{$ENDIF}
{$IFDEF LINUX}
//
{$ENDIF}
    Inc(P);
  end;
  // Goal: Swaps the sensitive case of each character in the given ansiarray.
  // Objetivo: Cambia el caso sensitivo de cada caracter en la cadena.
end;

procedure ReplaceCapitalize(var ADest: ansimemo);
var P: pansichar; MakeUppercase: Boolean; Q: ansichar;
begin
  P := PtrOfMemo(ADest);
  MakeUppercase := TRUE;
  while ((P <> nil) and (P^ <> #0)) do
  begin
    Q := P^;
    if (Q <> #32) then
    begin
      if (MakeUppercase) then
      begin
        {$IFDEF MSWINDOWS}
        CharUpperBuffA(@Q, 1);
        {$ENDIF}
        {$IFDEF LINUX}
        {$ENDIF}
        MakeUppercase := FALSE;
        {$IFDEF MSWINDOWS}
      end else CharLowerBuffA(@Q, 1);
        {$ENDIF}
        {$IFDEF LINUX}
      end else ;
        {$ENDIF}
    end else MakeUppercase := TRUE;
    P^ := Q;
    Inc(P);
  end;
  // Goal: Returns a copy with uppercase initials of the given ansiarray.
  // Objetivo: Regresa una copia con iniciales en mayusculas de la cadena dada.
end;

function StrToMemo(const ASource: ansistring): ansimemo;
var ansimemoRec: PansimemoRec; ACount: Word;
begin
  ACount := System.Length(ASource);
  // check if there's enough space
  // checar si hay suficiente espacio

  AllocMemo(ansimemo(ansimemoRec), ACount);
  System.Move(ASource[1], ansimemoRec^.Items[0], ACount);
  // transfer characters
  // transferir caracteres

  ansimemoRec^.Header.Len    := ACount;
  ansimemoRec^.Header.MaxLen := ACount;
  // update length
  // actualizar longuitud

  Result := ansimemoRec;
  // Goal: Assign a regular ansistring to a ansimemo ansistring.
  // Objetivo: Asigna una cadena regular a una cadena ansimemo.

  // Warning: "Dest" must be assigned.
  // Advertencia: "Dest" debera estar asignado.
end;

function MemoToStr(const ASource: ansimemo): ansistring;
var ansimemoRec: PansimemoRec absolute ASource; ACount: Word;
begin
  ACount := ansimemoRec^.Header.Len;
  System.SetLength(Result, ACount);
  System.Move(ansimemoRec^.Items[0], Result[1], ACount);
  // Goal: Returns the ansistring equivalent of a ansimemo.
  // Objetivo: Regresa el equivalente cadena de un ansimemo.
end;

procedure AssignStr(var ADest: ansimemo; const ASource: ansistring);
var ansimemoRec: PansimemoRec absolute ADest; ACount: Word;
begin
  ACount := System.Length(ASource);
  // check if there's enough space
  // checar si hay suficiente espacio

  if (ACount <= MaxLength(ADest)) then
  begin
    System.Move(ASource[1], ansimemoRec^.Items[0], ACount);
    ansimemoRec^.Header.Len := ACount;
  end;
  // Goal: Assign a ansistring to a ansimemo.
  // Objetivo: Asignar una cadena a un ansimemo.
end;

procedure AssignMemo(var ADest: ansimemo; const ASource: ansimemo);
var ADestRec: PansimemoRec absolute ADest;
    ASourceRec: PansimemoRec absolute ASource;
    ACount: Word;
begin
  ACount := umlcansimemos.Length(ASource);
  // check if there's enough space
  // checar si hay suficiente espacio

  FreeMemo(ADest);
  AllocMemo(ADest, ACount);
  // get a new ansimemo
  // obtener un nuevo ansimemo

  System.Move(ASourceRec^.Items, ADestRec^.Items, ACount);
  // copy characters
  // copiar caracteres

  // Goal: Assign a null ansistring to a ansimemo.
  // Objetivo: Asignar una cadena nula a un ansimemo.
end;

function getAt(const List: ansimemo; const AIndex: Word): ansichar;
var ansimemoRec: PansimemoRec absolute List; Items: PansimemoArray;
begin
  Result := #0;
  if (Assigned(ansimemoRec) and (AIndex < ansimemoRec^.Header.Len)) then
  begin
    Items  := @ansimemoRec^.Items;
    Result := Items^[AIndex];
  end;
  // Goal: Returns the indicated character.
  // Objetivo: Regresa el caracter indicado.
end;

procedure setAt(const List: ansimemo; const AIndex: Word; AValue: ansichar);
var ansimemoRec: PansimemoRec absolute List; Items: PansimemoArray;
begin
  if (Assigned(ansimemoRec) and (AIndex < ansimemoRec^.Header.Len)) then
  begin
    Items := @ansimemoRec^.Items;
    Items^[AIndex] := AValue;
  end;
  // Goal: Changes the indicated character.
  // Objetivo: Cambia el caracter indicado.
end;

function PtrOfMemo(const ASource: ansimemo): pointer;
var ansimemoRec: PansimemoRec absolute ASource;
begin
  Result := @(ansimemoRec^.Items[0]);
  // Goal: Returns the address of the first item.
  // Objetivo: Regresa la direccion del primer elemento.
end;

procedure Append(var ADest: ansimemo; const ASource: ansichar);
var ADestRec: PansimemoRec absolute ADest;
    OldLen, MaxLen, NewLen: Word;
begin
  if (Assigned(ADest)) then
  begin
    OldLen := umlcansimemos.Length(ADest);
    MaxLen := umlcansimemos.MaxLength(ADest);
    NewLen := Succ(OldLen);

    if (NewLen <= MaxLen) then
    begin
      ADestRec^.Items[NewLen] := ASource;
      ADestRec^.Header.Len := NewLen;
    end;
  end;
  // Objetivo: Agregar un valor al final del ansimemo.
  // Goal: Add a AValue at the end of the ansimemo.
end;

procedure InsertAt(var ADest: ansimemo; AIndex: Word; AValue: ansichar);
var ADestRec: PansimemoRec absolute ADest;
    Len, I: Word; MoveAValue: ansichar;
begin
  if (Assigned(ADest)) then
  begin
    Len :=umlcansimemos.Length(ADest);
    if (AIndex <= Len) then
    begin
      for I := Len downto AIndex do
      begin
        MoveAValue := getAt(ADest, I);
        // obtain previous AValue
        // obtener valor previo

        setAt(ADest, Succ(I), MoveAValue);
        // replace previous AValue
        // reemplazar valor anterior
      end;
      // move AValues
      // mover valores

      setAt(ADest, AIndex, AValue);
      // erase last AValue due to shift
      // borrar ultimo valor debido a recorrido

      ADestRec^.Header.Len := Succ(Len);
      // update length
      // actualizar longuitud
    end;
  end;
  // Objetivo: Insertar un valor en la posicion indicada del ansimemo.
  // Goal: Insert a AValue in the given position of the ansimemo.
end;

procedure DeleteAt(var ADest: ansimemo; AIndex: Word);
var ADestRec: PansimemoRec absolute ADest;
    Len, I: Word; MoveAValue: ansichar;
begin
  if (Assigned(ADest)) then
  begin
    Len := umlcansimemos.Length(ADest);
    if (AIndex <= Len) then
    begin
      for I := AIndex to Pred(Len) do
      begin
        MoveAValue := getAt(ADest, Succ(I));
        // obtain next AValue
        // obtener valor siguiente

        setAt(ADest, I, MoveAValue);
        // replace previous AValue
        // reemplazar valor anterior
      end;
      // move AValues
      // mover valores

      setAt(ADest, Len, ansinullchar);
      // erase last AValue due to shift
      // borrar ultimo valor debido a recorrido

      ADestRec^.Header.Len := Pred(Len);
      // update length
      // actualizar longuituf
    end;
  end;
  // Objetivo: Remover un valor en la posicion indicada del ansimemo.
  // Goal: Remove a AValue in the given position of the ansimemo.
end;

procedure Concat(var ADest: ansimemo; const ASource: ansistring);
var ADestRec: PansimemoRec absolute ADest;
    SourceLen, DestLen, MaxLen: Word;
begin
  if (Assigned(ADest)) then
  begin
    SourceLen := System.Length(ASource);
    DestLen := Succ(umlcansimemos.Length(ADest));
    MaxLen := SourceLen + DestLen;
    if (MaxLen >umlcansimemos.MaxLength(ADest))
      then ReAllocMemo(ADest, MaxLen);
    // update length
    // actualizar longuitud

    System.Move(ASource[1], ADestRec^.Items[DestLen], SourceLen);
    ADestRec^.Header.Len := Pred(MaxLen);
    // transfer characters
    // transferir caracteres
  end;
  // Objetivo: Concatenar 2 cadenas.
  // Goal: To concat 2 strings.
end;

procedure Concat(var ADest: ansimemo; const ASource: ansimemo);
var ADestRec: PansimemoRec absolute ADest;
    ASourceRec: PansimemoRec absolute ASource;
    SourceLen, DestLen, MaxLen: Word;
begin
  if (Assigned(ADest)) then
  begin
    SourceLen := umlcansimemos.Length(ASource);
    DestLen := Succ(umlcansimemos.Length(ADest));
    MaxLen := SourceLen + DestLen;
    if (MaxLen > umlcansimemos.MaxLength(ADest))
      then ReAllocMemo(ADest, MaxLen);
    // update length
    // actualizar longuitud

    System.Move(ASourceRec^.Items[0], ADestRec^.Items[DestLen], SourceLen);
    ADestRec^.Header.Len := Pred(MaxLen);
    // transfer characters
    // transferir caracteres
  end;
  // Objetivo: Concatenar 2 cadenas.
  // Goal: To concat 2 strings.
end;

procedure Insert(const ASource: ansistring; var S: ansimemo; AIndex: Word);
begin
//
end;

procedure Insert(const ASource: ansimemo; var S: ansimemo; AIndex: Word);
begin
//
end;

procedure Delete(var S: ansimemo; AIndex, ACount: Word);
begin
//
end;

procedure AllocMemo(var ADest: ansimemo; const ACount: Word);
var ansimemoRec: PansimemoRec absolute ADest;
    ItemsSizeInBytes, SizeInBytes: Word;
begin
  ItemsSizeInBytes := (ACount * SizeOf(ansichar));
  // obtain size in bytes of characters
  // obtener tama�o en bytes de caracteres

  SizeInBytes := SizeOf(TansimemoHeader) + ItemsSizeInBytes;
  // obtain size of header plus size of characters
  // obtener tama�o de encabezado mas tama�o de caracteres

  System.GetMem(ansimemoRec, SizeInBytes);
  System.FillChar(ansimemoRec^, SizeInBytes, ansinullchar);
  // obtain memory
  // obtener memoria

  ansimemoRec^.Header.Len := 0;
  ansimemoRec^.Header.MaxLen := ACount;
  // update length
  // actualizar longuitud

  // Objetivo: Crear un memo ANSI con la longuitud maxima expecificada.
  // Goal: Create an ANSI memo with the given specified maximun umlcansimemos.Length.
end;

procedure FreeMemo(var ADest: ansimemo);
var ansimemoRec: PansimemoRec absolute ADest;
    ItemsSizeInBytes, SizeInBytes, ACount: Word;
begin
  ACount := ansimemoRec^.Header.MaxLen;
  ItemsSizeInBytes := (ACount * SizeOf(ansichar));
  // obtain size in bytes of characters
  // obtener tama�o en bytes de caracteres

  SizeInBytes := SizeOf(TansimemoHeader) + ItemsSizeInBytes;
  // obtain size of header plus size of characters
  // obtener tama�o de encabezado mas tama�o de caracteres

  System.FreeMem(ADest, SizeInBytes);
  ADest := nil;
  // Objetivo: Liberar un memo ANSI de memoria.
  // Goal: Release an ANSI memo from memory.
end;

function DuplicateMemo(const ASource: ansimemo): ansimemo;
var SourceRec: PansimemoRec absolute ASource;
    DestRec: PansimemoRec absolute Result;
    ACount, SizeInBytes: Word;
begin
  Result := nil;

  ACount := umlcansimemos.Length(ASource);
  SizeInBytes := (ACount * SizeOf(ansichar));
  // obtain size in bytes of characters
  // obtener tama�o en bytes de caracteres

  AllocMemo(Result, ACount);
  System.Move(SourceRec^.Items[0], DestRec^.Items[0], SizeInBytes);
  // transfer characters
  // transferir caracteres

  DestRec^.Header.Len := SourceRec^.Header.Len;
  DestRec^.Header.MaxLen := SourceRec^.Header.MaxLen;
  // update length
  // actualizar longuitud

  // Goal: Returns a copy of the given memo.
  // Objetivo: Regresa una copia del memo dado.
end;

procedure ReAllocMemo(var ADest: ansimemo; const ACount: Word);
var ADestRec: PansimemoRec absolute ADest;
    Backup: pointer;
    OldCount, SizeInBytes: Word;
begin
  OldCount := umlcansimemos.Length(ADestRec);
  // check if there's enough space
  // checar si hay suficiente espacio

  SizeInBytes := (OldCount * SizeOf(ansichar));
  // obtain size in bytes of characters
  // obtener tama�o en bytes de caracteres

  System.GetMem(Backup, OldCount);
  System.Move(ADestRec^.Items[0], Backup^, SizeInBytes);
  // backup ansimemo's data
  // respaldar los datos del ansimemo

  FreeMemo(ADest);
  AllocMemo(ADest, ACount);
  // get a new ansimemo
  // obtener un nuevo ansimemo

  System.Move(Backup^, ADestRec^.Items[0], ACount);
  ADestRec^.Header.Len := ACount;
  // copy characters
  // copiar caracteres

  FreeMem(Backup, ACount);
  // release backup
  // liberar respaldo

  // Goal: Change a ansimemos* max length.
  // Objetivo: Cambiar la maxima longuitud de un ansimemo.
end;

(* global operators *)

function Equal(const A, B: ansimemo): Boolean;
begin
  Result := FALSE; // to-do
  // Goal: Returns if 2 memos are equal.
  // Objetivo: Regresa si 2 memos son iguales.
end;

function Greater(const A, B: ansimemo): Boolean;
begin
  Result := FALSE; // to-do
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser(const A, B: ansimemo): Boolean;
begin
  Result := FALSE; // to-do
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function Compare(const A, B: ansimemo): TComparison;
begin
  if (Equal(A, B))
    then Result := cmpEqual
  else if (Greater(A, B))
    then Result := cmpLower
  else Result := cmpHigher
  // Goal: Returns the comparison between 2 ansimemos.
  // Objetivo: Regresa la comparacion de 2 ansimemos.
end;

end.
