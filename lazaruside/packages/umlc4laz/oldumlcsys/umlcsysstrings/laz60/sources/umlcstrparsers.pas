(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstrparsers;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement search & parse text operations.
 ** Note:
 ** - The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
  {$IFDEF MSWINDOWS}
    Windows, //Messages,
  {$ENDIF}
  {$IFDEF LINUX}
    Types, Libc,
  {$ENDIF}
  SysUtils, Math,
  umlcdebug,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcstdnullstrtypes,
  umlcchars,
  umlccharsets,
  umlcstrings,
  umlctextconsts,
  umlccharsetconsts,
  dummy;

// ---

const

 MOD_umlcstrparsers : TUMLCModule =
  ($74,$9A,$51,$6D,$DD,$71,$B7,$4F,$85,$B2,$CE,$DA,$FE,$C5,$A1,$38);

// ---

(* global functions *)

  function MatchesCharSet
    (const AHaystack: string; const ValidChars: ansicharset): string; overload;

  function IsStringInSet
    (const AHaystack: string; const ValidChars: ansicharset): Boolean; overload;

  function IsWildcard
     (const ASource: string): Boolean;

  function IsIdentifier
    (const ASource: string): Boolean;

  function TryStrAtPosOf
    (const AHaystack:    string;
     const ANeedle:      string;
     const ASourceIndex: Word;
     out   ADestIndex:   Word): Boolean; overload;

  function StrAtPosOf
    (const AHaystack: string;
     const ANeedle:   string;
     const AIndex:    Word): Word; overload;

  function CharPosOf
    (const AHaystack: string; const ANeedle: char): Word;
  function CharPosOfReverse
    (const AHaystack: string; const ANeedle: char): Word;

  function CharSetPosOf
    (const AHaystack: string; ANeedleSet: charset): Word;
  function CharSetPosOfReverse
    (const AHaystack: string; ANeedleSet: charset): Word;

  function TryStrPosOf
    (const AHaystack:  string;
     const ANeedleStr: string;
     out   AIndex:    Word): Boolean;
  function TryStrPosOfSame
    (const AHaystack:  string;
     const ANeedleStr: string;
     out   AIndex:     Word): Boolean;

  function StrPosOf
    (const AHaystack:  string;
     const ANeedleStr: string): Word;
  function StrPosOfSame
    (const AHaystack:  string;
     const ANeedleStr: string): Word;

  function StartsWithEqualChar
    (const AHaystack: string; const ANeedle: char): Boolean;

  function StartsWithSameChar
    (const AHaystack: string; const ANeedle: char): Boolean;

  function StartsWithEqualStr
    (const AHaystack: string; const ANeedleStr: string): Boolean;

  function StartsWithSameStr
    (const AHaystack: string; const ANeedleStr: string): Boolean;

  function StartsWithEqualCharSet
    (const AHaystack: string; const ANeedleSet: charset): Boolean;

  function StartsWithSameCharSet
    (const AHaystack: string; const ANeedleSet: charset): Boolean;

  function FinishesWithEqualChar
    (const AHaystack: string; const ANeedleChar: string): Boolean;

  function FinishesWithSameChar
    (const AHaystack: string; const ANeedleChar: string): Boolean;

  function FinishesWithEqualCharSet
   (const AHaystack: string; const ANeedleSet: charset): Boolean;

  function FinishesWithSameCharSet
   (const AHaystack: string; const ANeedleSet: charset): Boolean;

  function FinishesWithEqualStr
    (const AHaystack: string; const ANeedleStr: string): Boolean;

  function FinishesWithSameStr
    (const AHaystack: string; const ANeedleStr: string): Boolean;

  function NonQuotedUppercaseCopy
    (const ASource: string; const A, B: Char): string; overload;
  function NonQuotedLowercaseCopy
    (const ASource: string; const A, B: Char): string; overload;
  function NonQuotedTogglecaseCopy
    (const ASource: string; const A, B: Char): string; overload;
  function NonQuotedCapitalizeCopy
    (const ASource: string; const A, B: Char): string; overload;

  function TrimPrefixCopy
    (const AFullStr: string; const Prefix: string) : string;
  function TrimPosfixCopy
    (const AFullStr: string; const Posfix: string): string;
  function TrimDelimitersCopy
    (const AFullStr: string;
     const Prefix: string; const Posfix: string): string;

  function PadPrefixCopy
    (const AFullStr: string; const Prefix: string) : string;
  function PadPosfixCopy
    (const AFullStr: string; const Posfix: string) : string;
  function PadDelimitersCopy
    (const AFullStr: string;
     const Prefix: string; const Posfix: string): string;

  function CharCountOf
    (const AHaystack: string; ANeedleChar: char): Cardinal;
  function CharSetCountOf
    (const AHaystack: string; ANeedleSet: charset): Cardinal;

  function RemoveCharCopy
    (const AHaystack: string; ANeedle: Char): string; overload;
  function RemoveCharSetCopy
    (const AHaystack: string; const ANeedle: charset): string; overload;

  function ReplaceCharByCharCopy
    (const AHaystack: string; A, B: Char): string;
  function ReplaceCharByStrCopy
    (const AHaystack: string; A: Char; B: string): string;

  function ReplaceCharSetByCharCopy
    (const AHaystack: string;
     const ASource: charset; ADest: Char): string; overload;

  function ReplaceStrByStrCopy
    (const AHaystack, ASource, ADest: string): string;
  function ReplaceStrByStrSameCopy
    (const AHaystack, ASource, ADest: string): string;

  function ReplaceAllCharToStrCopy
    (const AHaystack: string;
     const ASource: Char; const ADest: string): string;
  function ReplaceAllStrByStrCopy
    (const AHaystack: string; const ASource, ADest: string): string;

  function ScanEqual
   (const AHaystack: string;
    const ANeedle:   string;
    const AIndex:    Word): Word; overload;
  function ScanSame
   (const AHaystack: string;
    const ANeedle:   string;
    const AIndex:    Word): Word; overload;

  function SkipChar
   (const AHaystack: string; const AIndex: Word; const ValidChar: Char): Word;
  function SkipChars
   (const AHaystack: string; const AIndex: Word; const ValidChars: ansicharset): Word;
  function SkipCharWhile
   (const AHaystack: string; const AIndex: Word; const ValidChar: Char): Word;
  function SkipCharUntil
   (const AHaystack: string; const AIndex: Word; const BreakChar: Char): Word;
  function SkipWhile
   (const AHaystack: string; const AIndex: Word; const ValidChars: ansicharset): Word;
  function SkipUntil
   (const AHaystack: string; const AIndex: Word; const BreakChars: ansicharset): Word;
  function SkipToken
    (const AHaystack: string; var Start, Finish: Word): string;
  function SkipDigit
    (const AHaystack: string; var Start, Finish: Word): string;
  function SkipLetter
    (const AHaystack: string; var Start, Finish: Word): string;

  function SkipEmpty
    (const Start, Finish: Word): Boolean;

  function CopyFromTo
    (const ASource: string; const Start, Finish: Word): string;
  function CopyFrom
    (const ASource: string; AIndex: Word): string;
  function ParseFrom
    (const ASource: string; var Start, Finish: Word): string;

  function ExtractCharUntil
   (const AHaystack: string;
    const AIndex: Word;
    const BreakChar: Char): string;
  function ExtractCharUntilBack
   (const AHaystack: string;
    const AIndex: Word;
    const BreakChar: Char): string;

  function ExtractWhile
   (const AHaystack: string;
    const AIndex: Word;
    const SkipChars: ansicharset): string;
  function ExtractUntil
   (const AHaystack: string;
    const AIndex: Word;
    const BreakChars: ansicharset): string;
  function ExtractAt
    (const AHaystack: string;
     const AIndex: Word): string;

  function ExtractUntilIndex
   (const AHaystack: string;
    var   AIndex: Word;
    const BreakChars: ansicharset): string;

  function ParseCharSingle
    (const ASource: string; var AIndex: Word; ValidChar: ansichar): Boolean;
  function ParseSingle
    (const ASource: string; var AIndex: Word; ValidChars: ansicharset): Boolean;

  function ParseCharWhile
    (const ASource: string; var AIndex: Word; ValidChar: ansichar): Boolean;
  function ParseCharUntil
    (const ASource: string; var AIndex: Word; BreakChar: ansichar): Boolean;

  function ParseWhile
   (const AHaystack: string; var AIndex: Word; ValidChars: ansicharset): Boolean;
  function ParseUntil
   (const AHaystack: string; var AIndex: Word; BreakChars: ansicharset): Boolean;

  function ParseQuotedStr
    (const ASource: string; var AIndex: Word): Boolean;
  function ParseUnsigned
    (const ASource: string; var AIndex: Word): Boolean;
  function ParseIdentifier(
    const ASource: string; var AIndex: Word): Boolean;
  function ParseQualified
    (const ASource: string; var AIndex: Word): Boolean;
  function ParseCount
    (const ASource: string; var AIndex: Word; Count: Integer): Boolean;

  function IsQuotedStr
    (const ASource: string): Boolean;
  function IsUnsigned
    (const ASource: string): Boolean;

  function ExtractQuotedStr
    (const ASource: string; var ADest: string; var AIndex: Word): Boolean;
  function ExtractUnsigned
    (const ASource: string; var ADest: string; var AIndex: Word): Boolean;
  function ExtractIdentifier
    (const ASource: string; var ADest: string; var AIndex: Word): Boolean;
  function ExtractQualified
    (const ASource: string; var ADest: string; var AIndex: Word): Boolean;
  function ExtractCount
    (const ASource: string; var ADest: string; var AIndex: Word; Count: Word): Boolean;

(* global procedures *)

  procedure ReplaceQuotedUppercase
    (var ADest: string; const A, B: Char); overload;
  procedure ReplaceQuotedLowercase
    (var ADest: string; const A, B: Char); overload;
  procedure ReplaceQuotedTogglecase
    (var ADest: string; const A, B: Char); overload;
  procedure ReplaceQuotedCapitalize
    (var ADest: string; const A, B: Char); overload;

  procedure ReplaceUppercase
    (var ADest: string); overload;
  procedure ReplaceLowercase
    (var ADest: string); overload;
  procedure ReplaceTogglecase
    (var ADest: string); overload;
  procedure ReplaceCapitalize
    (var ADest: string); overload;

  procedure ReplaceTrimLeft
    (var ADest: string); overload;
  procedure ReplaceTrimRight
    (var ADest: string); overload;
  procedure ReplaceTrim
    (var ADest: string); overload;

  procedure ReplaceUnTrimLeft
    (var ADest: string; ACount: Word); overload;
  procedure ReplaceUnTrimRight
    (var ADest: string; ACount: Word); overload;

  procedure ReplaceAllCharByChar
    (var ADest: string; A, B: Char); overload;
  procedure ReplaceAllCharByString
    (var ADest: string; A: Char; B: string); overload;

  procedure ReplaceCharSetByChar
    (var AHaystack: string; const ASource: charset; ADest: Char); overload;

  procedure ReplaceAllStrByStr
    (var AHaystack: string; const ASource, ADest: string); overload;

  procedure ReplaceStrByStrAt
    (var AHaystack: string;
     var AIndex: Word; const ASource, ADest: string);
  procedure ReplaceStrByStrAtSame
    (var AHaystack: string;
     var AIndex: Word; const ASource, ADest: string);

  procedure ReplaceStrByStr
    (var AHaystack: string; const ASource, ADest: string);

  procedure ReplaceCharByStr
    (var AHaystack: string; const ASource: Char; const ADest: string);

  procedure RemoveChar
    (var AHaystack: string; IgnoreChar: Char);
  procedure RemoveCharSet
    (var AHaystack: string; IgnoreChars: charset);

implementation

(* global functions *)

function MatchesCharSet
  (const AHaystack: string; const ValidChars: ansicharset): string;
var CanContinue: Boolean; AIndex, ALen: Word; Temp: string;
begin
  umlcstrings.Clear(Result);

  if (not umlcstrings.IsEmpty(AHaystack)) then
  begin
    umlcstrings.Clear(Temp);

    AIndex := 1;
    ALen   := umlcstrings.Length(AHaystack);

    CanContinue := TRUE;
    while ((AIndex < ALen) and CanContinue) do
    begin
      CanContinue :=
        umlccharsets.IsMember(ValidChars, AHaystack[AIndex]);

      if (CanContinue) then
      begin
        Temp := Temp + AHaystack[AIndex];
      end;

      Inc(AIndex);
    end;

    if (CanContinue) then
    begin
      Result := Temp;
    end;
  end;
  // Goal: Moves a null terminated string pointer,
  // as long as each character, matches the valid set.

  // Objetivo: Mueve un apuntador cadena terminada en nulo,
  // mientras cada caracter, coincide con el conjunto valido.
end;

function IsStringInSet
  (const AHaystack: string; const ValidChars: ansicharset): Boolean;
begin
  Result :=
    (umlcstrparsers.MatchesCharSet(AHaystack, ValidChars) <> AHaystack);
  // Goal: Returns if all the characters, a string are valid.
  // Objetivo: Regresa si todos los caracteres en una cadena son validos.
end;

function IsWildcard
   (const ASource: string): Boolean;
var P: ansinullstring;
begin
  Result :=
    umlcstrparsers.IsStringInSet(ASource, WildcardSet);
  // Goal: Returns if a string is a wildcard.
  // Objetivo: Regresa si una cadena es un comodin.
end;

function IsIdentifier
  (const ASource: string): Boolean;
var I: Integer;
begin
  Result := FALSE;
  if (umlcstrings.Length(ASource) = 0) or (not umlccharsets.IsMember(AlphaSet, ASource[1]))
    then Exit;

  for I := 2 to umlcstrings.Length(ASource) do
  begin
    if (not umlccharsets.IsMember(IDSet, ASource[I]))
      then Exit;
  end;

  Result := TRUE;
  // Goal: To return if a string is a valid identifier.
  // Objetivo: Regresar si una cadena es un identificador valido.
end;

function TryStrAtPosOf
  (const AHaystack:    string;
   const ANeedle:      string;
   const ASourceIndex: Word;
   out   ADestIndex:   Word): Boolean;
var ANewIndex, Len: Word; Found: Boolean;
begin
  Result := false;
  ADestIndex := 0;

  Len :=
    umlcstrings.Length(AHaystack);
  ANewIndex :=
    ASourceIndex;
  Found  := FALSE;

  while (not Found and (ANewIndex <= Len)) do
  begin
    if (umlcstrings.EqualStrAt(AHaystack, ANeedle, ANewIndex))
      then Found := TRUE
      else Inc(ANewIndex);
  end;

  if (Found) then
  begin
    ADestIndex := ANewIndex;
  end;

  Result := Found;
  // Goal: Searches for a substring inside other string
  // beginning at the "Index" character and returns the next character.

  // Objetivo: Busca una subcadena adentro de otra cadena
  // comenzando en el caracter numero "indice" y regresa el siguiente caracter.
end;

function StrAtPosOf
 (const AHaystack: string;
  const ANeedle:   string;
  const AIndex:    Word): Word;
var ANewIndex: Word;
begin
  Result := 0;
  if (umlcstrparsers.TryStrAtPosOf
    (AHaystack, ANeedle, AIndex, ANewIndex)) then
  begin
    Result := ANewIndex;
  end;
  // Goal: Searches for a substring inside other string
  // beginning at the "Index" character and returns the next character.

  // Objetivo: Busca una subcadena adentro de otra cadena
  // comenzando en el caracter numero "indice" y regresa el siguiente caracter.
end;

function CharPosOf
 (const AHaystack: string; const ANeedle: char): Word;
var AIndex: Word; Found: Boolean;
begin
  Found := FALSE; AIndex := umlcstrings.Length(AHaystack);
  while ((AIndex > 0) and (not Found)) do
  begin
    Found := (AHaystack[AIndex] = ANeedle);
    Inc(AIndex);
  end;

  if (Found)
    then Result := Pred(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena.

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function CharPosOfReverse
  (const AHaystack: string; const ANeedle: char): Word;
var AIndex: Word; Found: Boolean;
begin
  Found := FALSE; AIndex := umlcstrings.Length(AHaystack);
  while ((AIndex > 0) and (not Found)) do
  begin
    Found := (AHaystack[AIndex] = ANeedle);
    Dec(AIndex);
  end;

  if (Found)
    then Result := Succ(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el final de la cadena.

  // Goal: Returns the first location of the given character,
  // from the finish of the string.
end;

function CharSetPosOf
  (const AHaystack: string; ANeedleSet: charset): Word;
var AIndex, Len: Word; Found: Boolean;
begin
  Found := FALSE;
  Len   := umlcstrings.Length(AHaystack);
  AIndex := 1;

  while ((AIndex <= Len) and (not Found)) do
  begin
    Found :=
      umlccharsets.IsMember(ANeedleSet, AHaystack[AIndex]);
    Inc(AIndex);
  end;

  if (Found)
    then Result := Pred(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion de alguno de los caracters dados,
  // desde el inicio de la cadena.

  // Goal: Returns the first location of any of the given characters,
  // from the start of the string.
end;

function CharSetPosOfReverse
  (const AHaystack: string; ANeedleSet: charset): Word;
var AIndex: Word; Found: Boolean;
begin
  Found  := FALSE;
  AIndex :=
    umlcstrings.Length(AHaystack);

  while ((AIndex > 0) and (not Found)) do
  begin
    Found :=
      umlccharsets.IsMember(ANeedleSet, AHaystack[AIndex]);
    Dec(AIndex);
  end;

  if (Found)
    then Result := Succ(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion de alguno de los caracteres dados,
  // desde el inicio de la cadena}

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function TryStrPosOf
  (const AHaystack:  string;
   const ANeedleStr: string;
   out   AIndex:     Word): Boolean;
var Len, EachIndex: Word; Found: Boolean;
begin
  Result := false;
  AIndex := 0;

  Len :=
    umlcstrings.Length(AHaystack);
  // obtain length of full string
  // obtener longuitud de cadena completa

  EachIndex := 1;

  Found := FALSE;
  while ((not Found) and (EachIndex < Len)) do
  begin
    Found :=
      umlcstrparsers.StartsWithEqualStr
        (ANeedleStr, System.Copy(AHaystack, EachIndex, Len));

    Inc(AIndex);
  end;

  if (Found)
    then AIndex := Pred(EachIndex)
    else AIndex := 0;
  Result := Found;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function TryStrPosOfSame
  (const AHaystack:  string;
   const ANeedleStr: string;
   out   AIndex:     Word): Boolean;
var Len, EachIndex: Word; Found: Boolean;
begin
  Result := false;
  AIndex := 0;

  Len :=
    umlcstrings.Length(AHaystack);
  // obtain length of full string
  // obtener longuitud de cadena completa

  EachIndex := 1;

  Found := FALSE;
  while ((not Found) and (EachIndex < Len)) do
  begin
    Found :=
      umlcstrparsers.StartsWithSameStr
        (ANeedleStr, System.Copy(AHaystack, EachIndex, Len));

    Inc(EachIndex);
  end;

  if (Found)
    then AIndex := Pred(EachIndex)
    else AIndex := 0;
  Result := Found;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the AIndex of the first ocurrence of "Substr".
end;

function StrPosOf
  (const AHaystack:  string;
   const ANeedleStr: string): Word;
begin
  Result := 0;
  umlcstrparsers.TryStrPosOf(AHaystack, ANeedleStr, Result);
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function StrPosOfSame
  (const AHaystack:  string;
   const ANeedleStr: string): Word;
begin
  Result := 0;
  umlcstrparsers.TryStrPosOfSame(AHaystack, ANeedleStr, Result);
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the AIndex of the first ocurrence of "Substr".
end;

function StartsWithEqualChar
  (const AHaystack: string; const ANeedle: Char): Boolean;
begin
  Result :=
    (umlcstrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      umlcchars.Equal(AHaystack[1], ANeedle);
  end;
end;

function StartsWithSameChar
  (const AHaystack: string; const ANeedle: Char): Boolean;
begin
  Result :=
    (umlcstrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      umlcchars.SameText(AHaystack[1], ANeedle);
  end;
end;


function StartsWithEqualStr
  (const AHaystack: string; const ANeedleStr: string): Boolean;
var Shorter, Len: Word; LeadStr: string;
begin
  Shorter :=
    umlcstrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcstrings.Length(AHaystack);
  // obtain length of full string
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full string
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    LeadStr :=
      System.Copy(AHaystack, 1, Shorter);
    Result  :=
      umlcstrings.Equal(ANeedleStr, LeadStr);
  end;
  // Objetivo: Regresa si una subcadena es igual o esta al inicio de
  // otra cadena.

  // Goal: Returns if a substring is equal o is the start of another string.
end;

function StartsWithSameStr
  (const AHaystack: string; const ANeedleStr: string): Boolean;
var Shorter, Len: Word; LeadStr: string;
begin
  Shorter :=
    umlcstrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcstrings.Length(AHaystack);
  // obtain length of full string
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full string
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    LeadStr := System.Copy(AHaystack, 1, Shorter);
    Result  :=
      umlcstrings.SameText(ANeedleStr, LeadStr);
  end;
  // Objetivo: Regresa si una subcadena es igual o esta al inicio de
  // otra cadena.

  // Goal: Returns if a substring is equal o is the start of another string.
end;

function StartsWithEqualCharSet
  (const AHaystack: string; const ANeedleSet: charset): Boolean;
begin
  Result :=
    (umlcstrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      (umlccharsets.IsMember(ANeedleSet, AHaystack[1]));
  end;
end;

function StartsWithSameCharSet
  (const AHaystack: string; const ANeedleSet: charset): Boolean;
begin
  Result :=
    (umlcstrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      (umlccharsets.IsSameMember(ANeedleSet, AHaystack[1]));
  end;
end;

function FinishesWithEqualChar
  (const AHaystack: string; const ANeedleChar: string): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcstrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (AHaystack[Pred(ACount)] = ANeedleChar);
  end;
end;

function FinishesWithSameChar
  (const AHaystack: string; const ANeedleChar: string): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcstrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (AHaystack[Pred(ACount)] = ANeedleChar);
  end;
end;

function FinishesWithEqualCharSet
 (const AHaystack: string; const ANeedleSet: charset): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcstrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (umlccharsets.IsMember(ANeedleSet, AHaystack[Pred(ACount)]));
  end;
end;

function FinishesWithSameCharSet
 (const AHaystack: string; const ANeedleSet: charset): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcstrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (umlccharsets.IsSameMember(ANeedleSet, AHaystack[Pred(ACount)]));
  end;
end;

function FinishesWithEqualStr
  (const AHaystack: string; const ANeedleStr: string): Boolean;
var Shorter, Len, AIndex: Word; TrailingStr: string;
begin
  Shorter :=
    umlcstrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcstrings.Length(AHaystack);
  // obtain length of full string
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full string
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    AIndex      :=
      (Len - Shorter + 1);
    TrailingStr :=
      System.Copy(AHaystack, AIndex, Shorter);
    Result      :=
      umlcstrings.Equal(ANeedleStr, TrailingStr);
  end;
end;

function FinishesWithSameStr
  (const AHaystack: string; const ANeedleStr: string): Boolean;
var Shorter, Len, AIndex: Word; TrailingStr: string;
begin
  Shorter :=
    umlcstrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcstrings.Length(AHaystack);
  // obtain length of full string
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full string
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    AIndex      :=
      (Len - Shorter + 1);
    TrailingStr :=
      System.Copy(AHaystack, AIndex, Shorter);
    Result      :=
      umlcstrings.SameText(ANeedleStr, TrailingStr);
  end;
end;

function NonQuotedUppercaseCopy
  (const ASource: string; const A, B: Char): string;
begin
  Result := ASource;
  umlcstrparsers.ReplaceQuotedUppercase(Result, A, B);
  // Goal: Returns a uppercase copy of the given string,
  // without modifying delimited substrings.

  // Objetivo: Regresa una copia en mayusculas de la cadena dada,
  // sin modificar a las subcadenas delimitadas.
end;

function NonQuotedLowercaseCopy
  (const ASource: string; const A, B: Char): string;
begin
  Result := ASource;
  umlcstrparsers.ReplaceQuotedLowercase(Result, A, B);
  // Goal: Returns a lowercase copy of the given nullstring,
  // without modifying delimited substrings.

  // Objetivo: Regresa una copia en minusculas de la cadena terminada en nulo,
  // dada sin modificar a las subcadenas delimitadas.
end;

function NonQuotedTogglecaseCopy
  (const ASource: string; const A, B: Char): string;
begin
  Result := ASource;
  umlcstrparsers.ReplaceQuotedTogglecase(Result, A, B);
  // Goal: Returns a lowercase copy of the given nullstring,
  // without modifying delimited substrings.

  // Objetivo: Regresa una copia en minusculas de la cadena terminada en nulo,
  // dada sin modificar a las subcadenas delimitadas.
end;

function NonQuotedCapitalizeCopy
  (const ASource: string; const A, B: Char): string;
begin
  Result := ASource;
  umlcstrparsers.ReplaceQuotedCapitalize(Result, A, B);
  // Goal: Returns a lowercase copy of the given nullstring,
  // without modifying delimited substrings.

  // Objetivo: Regresa una copia en minusculas de la cadena terminada en nulo,
  // dada sin modificar a las subcadenas delimitadas.
end;

/// <summary>
/// If founds a string Prefix in the given text,
/// returns a copy without the Prefix,
/// otherwise, returns the full string.
/// </summary>
/// <param name="Prefix">Prefix substring</param>
/// <param name="str">Text with an optional Prefix</param>
/// <returns>Text without a Prefix</returns>
function TrimPrefixCopy
  (const AFullStr: string; const Prefix: string) : string;
var ALen: Integer;
begin
  Result := '';

  if (umlcstrparsers.StartsWithEqualStr(AFullStr, Prefix)) then
  begin
    ALen   := (umlcstrings.Length(AFullStr) - umlcstrings.Length(Prefix));
    Result := RightCopy(AFullStr, ALen);
  end else
  begin
    Result := AFullStr;
  end;
end;

/// <summary>
/// If founds a string Prefix in the given text,
/// returns a copy without the Posfix,
/// otherwise, returns the full string.
/// </summary>
/// <param name="Prefix">Prefix substring</param>
/// <param name="str">Text with an optional Posfix</param>
/// <returns>Text without a Posfix</returns>
function TrimPosfixCopy
  (const AFullStr: string; const Posfix: string): string;
var ALen : Integer;
begin
  Result := '';

  if (umlcstrparsers.FinishesWithEqualStr(AFullStr, Posfix)) then
  begin
    ALen   := (umlcstrings.Length(AFullStr) - 1);
    Result := umlcstrings.LeftCopy(AFullStr, ALen);
  end else
  begin
    Result := AFullStr;
  end;
end;

/// <summary>
/// Returns a copy of the <code>str</code> string,
/// where the given Prefix &  Posfix substrings, are removed.
/// </summary>
/// <param name="Prefix"></param>
/// <param name="Posfix"></param>
/// <param name="str"></param>
/// <returns></returns>
function TrimDelimitersCopy
  (const AFullStr: string;
   const Prefix: string; const Posfix: string): string;
begin
  Result := '';
  Result := umlcstrparsers.TrimPrefixCopy(AFullStr, Prefix);
  Result := umlcstrparsers.TrimPosfixCopy(Result, Posfix);
end;

function PadPrefixCopy
  (const AFullStr: string; const Prefix: string): string;
begin
  Result := '';

  if (not umlcstrparsers.StartsWithEqualStr(AFullStr, Prefix)) then
  begin
    Result := (Prefix + AFullStr);
  end else
  begin
    Result := AFullStr;
  end;
  // if a string doesn't have some Prefix,
  // add its.
end;

function PadPosfixCopy
  (const AFullStr: string; const Posfix: string): string;
begin
  Result := '';

  if (not umlcstrparsers.FinishesWithEqualStr(AFullStr, Posfix)) then
  begin
    Result := (AFullStr + Posfix);
  end else
  begin
    Result := AFullStr;
  end;
  // if a string doesn't have some Posfix,
  // add its.
end;

function PadDelimitersCopy
  (const AFullStr: string;
   const Prefix: string;
   const Posfix: string): string;
begin
  Result := '';
  Result :=
    umlcstrparsers.PadPrefixCopy(AFullStr, Prefix);
  Result :=
    umlcstrparsers.PadPosfixCopy(Result, Posfix);
end;

function CharCountOf
  (const AHaystack: string; ANeedleChar: char): Cardinal;
var I, ALen: Cardinal;
begin
  Result := 0;
  if (not umlcstrings.IsEmpty(AHaystack) and (not umlcchars.IsNull(ANeedleChar))) then
  begin
    ALen :=
       umlcstrings.Length(AHaystack);
    for I := 1 to ALen do
    begin
      if (AHaystack[I] = ANeedleChar) then
      begin
        System.Inc(Result);
      end;
    end;
  end;
end;

function CharSetCountOf
  (const AHaystack: string; ANeedleSet: charset): Cardinal;
var I, ALen: Cardinal;
begin
  Result := 0;
  if (not umlcstrings.IsEmpty(AHaystack) and (not umlccharsets.IsEmpty(ANeedleSet))) then
  begin
    ALen :=
       umlcstrings.Length(AHaystack);
    for I := 1 to ALen do
    begin
      if (umlccharsets.IsMember(ANeedleSet, AHaystack[I])) then
      begin
        System.Inc(Result);
      end;
    end;
  end;
end;

function RemoveCharCopy
  (const AHaystack: string; ANeedle: Char): string;
var I, L: Word;
begin
  Result := '';
  L := umlcstrings.Length(AHaystack);
  for i := 1 to L do
  begin
    if (Result[i] <> ANeedle)
      then Result := Result + ANeedle;
  end;
  // Goal: Replace a specific character from a string.
  // Objetivo: Reemplazar un caracter en especifico de una cadena.
end;

function RemoveCharSetCopy
(const AHaystack: string; const ANeedle: charset): string;
var I: Word;
begin
  umlcstrings.Clear(Result);
  for i := 1 to Length(AHaystack) do
  begin
    if (not umlccharsets.IsMember(ANeedle, AHaystack[i]))
      then umlcstrings.ConcatStr(Result, AHaystack[i]);
  end;
  // Goal: Replace a specific character set from a string.
  // Objetivo: Reemplazar un conjunto caracter en especifico de una cadena.
end;

function ReplaceCharByCharCopy
  (const AHaystack: string; A, B: Char): string;
var I, L: Word;
begin
  Result := AHaystack;
  L :=
    umlcstrings.Length(AHaystack);
  for i := 1 to L do
  begin
    if (Result[i] = A) then
    begin
      Result[i] := B;
    end;
  end;
  // Goal: Replace a specific character from a string.
  // Objetivo: Reemplazar un caracter en especifico de una cadena.
end;

function ReplaceCharByStrCopy
  (const AHaystack: string; A: Char; B: string): string;
var I, L: Word;
begin
  Result := '';
    L := umlcstrings.Length(AHaystack);
  for i := 1 to L do
  begin
    if (AHaystack[i] = A) then
    begin
      Result := Result + B;
    end else
    begin
      Result := Result + AHaystack[i];
    end;
  end;
  // Goal: Replace a specific character from a string.
  // Objetivo: Reemplazar un caracter en especifico de una cadena.
end;

function ReplaceCharSetByCharCopy
 (const AHaystack: string;
  const ASource: charset; ADest: Char): string;
var I: Integer;
begin
  umlcstrings.Clear(Result);
  for i := 1 to Length(AHaystack) do
  begin
    if (IsMember(ASource, AHaystack[i]))
      then umlcstrings.ConcatStr(Result, ADest)
      else umlcstrings.ConcatChar(Result, AHaystack[i]);
  end;
  // Goal: Replace a specific character set from a string.
  // Objetivo: Reemplazar un conjunto caracter en especifico de una cadena.
end;

function ReplaceStrByStrCopy
  (const AHaystack, ASource, ADest: string): string;
var AIndex: Word;
begin
  Result := AHaystack;
  umlcstrparsers.ReplaceStrByStrAt(Result, AIndex, ASource, ADest);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceStrByStrSameCopy
  (const AHaystack, ASource, ADest: string): string;
var AIndex: Word;
begin
  AIndex := 1;
  Result := AHaystack;
    umlcstrparsers.ReplaceStrByStrAt(Result, AIndex, ASource, ADest);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceAllCharToStrCopy
  (const AHaystack: string;
   const ASource: Char; const ADest: string): string;
var I: Word;
begin
  umlcstrings.Clear(Result);
  for i := 1 to Length(AHaystack) do
    if (AHaystack[i] <> ASource)
      then umlcstrings.ConcatChar(Result, AHaystack[i])
      else umlcstrings.ConcatStr(Result, ADest)
  // Goal: Replaces all the "Source" found, "AValue" with "Dest".
  // Objetivo: Reemplaza todos los "Source" encontrada en "AValue" por "Dest".}
end;

function ReplaceAllStrByStrCopy
  (const AHaystack: string; const ASource, ADest: string): string;
var Finished: Boolean; AIndex: Word; Temp: ansistring;
begin
  AIndex := 1;
  Result := AHaystack;
  repeat
    Temp := Result;
    umlcstrparsers.ReplaceStrByStrAt(Temp, AIndex, ASource, ADest);
    Result := Temp;
    Finished := (AIndex = 0);
  until Finished;
  // Goal: Replaces all the "Source" found, "AValue" with "Dest".
  // Objetivo: Reemplaza todas las "Source" encontradas en "AValue" por "Dest".}
end;

function ScanEqual
 (const AHaystack: string;
  const ANeedle:   string;
  const AIndex:    Word): Word;
var Len, EachIndex: Word; Found: Boolean;
begin
  // strings starts with "1"
  Result := 0;

  EachIndex := AIndex;
  Len := umlcstrings.Length(AHaystack);
  Found  := FALSE;

  while (not Found and (AIndex <= Len)) do
  begin
    Found :=
      umlcstrings.EqualStrAt(AHaystack, ANeedle, EachIndex);
    if (not Found) then
    begin
      Inc(EachIndex);
    end;
  end;

  if (Found)
    then Result := EachIndex
    else Result := 0;
  // Goal: Searches for a substring inside other string
  // beginning at the "AIndex" char and returns the next character,
  // is case sensitive.

  // Objetivo: Busca una subcadena adentro de otra cadena
  // comenzando en el caracter numero "indice" y regresa el siguiente caracter,
  // es sensible al caso.
end;

function ScanSame
 (const AHaystack: string;
  const ANeedle:   string;
  const AIndex:    Word): Word;
var EachIndex, Len: Word; Found: Boolean;
begin
  // strings starts with "1"
  Result := 0;

  Len :=
    umlcstrings.Length(AHaystack);
  EachIndex := AIndex;
  Found  := FALSE;

  while (not Found and (EachIndex <= Len)) do
  begin
    Found :=
      umlcstrings.SameStrAt(AHaystack, ANeedle, EachIndex);
    if (not Found) then
    begin
      Inc(EachIndex);
    end;
  end;

  if (Found)
    then Result := EachIndex
    else Result := 0;
  // Goal: Searches for a substring inside other string
  // beginning at the "AIndex" char and returns the next character,
  // is case sensitive.

  // Objetivo: Busca una subcadena adentro de otra cadena
  // comenzando en el caracter numero "indice" y regresa el siguiente caracter,
  // NO es sensible al caso.
end;

function SkipChar
  (const AHaystack: string; const AIndex: Word; const ValidChar: Char): Word;
var L: Word;
begin
  Result := AIndex; L := Length(AHaystack);
  if (Result <= L) and (AHaystack[Result] = ValidChar) then
  begin
    Inc(Result);
  end;
  // Goal: Returns a single character.
  // Objetivo: Regresa un solo caracter.
end;

function SkipChars
  (const AHaystack: string; const AIndex: Word; const ValidChars: ansicharset): Word;
var L: Word;
begin
  Result := AIndex; L :=
    umlcstrings.Length(AHaystack);
  if (Result <= L) and umlccharsets.IsMember(ValidChars, AHaystack[Result]) then
  begin
    Inc(Result);
  end;
  // Goal: Returns a single character.
  // Objetivo: Regresa un solo caracter.
end;

function SkipCharWhile
  (const AHaystack: string; const AIndex: Word; const ValidChar: Char): Word;
var L: Word;
begin
  Result := AIndex; L := umlcstrings.Length(AHaystack);
  while ((Result <= L) and (AHaystack[Result] = ValidChar)) do
  begin
   Inc(Result);
  end;
  // Goal: Returns a group of characters.
  // Objetivo: Regresa un grupo de caracteres.
end;

function SkipCharUntil
  (const AHaystack: string; const AIndex: Word; const BreakChar: Char): Word;
var L: Word;
begin
  Result := AIndex; L := umlcstrings.Length(AHaystack);
  while ((Result <= L) and (AHaystack[Result] <> BreakChar)) do
  begin
   Inc(Result);
  end;
  // Goal: Returns a group of characters.
  // Objetivo: Regresa un grupo de caracteres.
end;

function SkipWhile
  (const AHaystack: string; const AIndex: Word; const ValidChars: ansicharset): Word;
var L: Word;
begin
  Result := AIndex; L := umlcstrings.Length(AHaystack);
  while ((Result <= L) and umlccharsets.IsMember(ValidChars, AHaystack[Result])) do
  begin
   Inc(Result);
  end;
  // Goal: Returns a group of non-space characters.
  // Objetivo: Regresa un grupo de caracteres que no son espacios.
end;

function SkipUntil
  (const AHaystack: string; const AIndex: Word; const BreakChars: ansicharset): Word;
var L: Word;
begin
  Result := AIndex;
  L :=
    umlcstrings.Length(AHaystack);
  while ((Result <= L) and not umlccharsets.IsMember(BreakChars, AHaystack[Result])) do
  begin
    Inc(Result);
  end;
  // Goal: Returns a group of non-space characters.
  // Objetivo: Regresa un grupo de caracteres que no son espacios.
end;

function SkipToken
  (const AHaystack: string; var Start, Finish: Word): string;
var StartIndex, FinishIndex: Word;
begin
  StartIndex  := Start;

  StartIndex  :=
    umlcstrparsers.SkipCharWhile(AHaystack, StartIndex, #32);
  FinishIndex :=
    umlcstrparsers.SkipCharUntil(AHaystack, StartIndex, #32);
  Result      :=
    umlcstrparsers.CopyFromTo(AHaystack, StartIndex, FinishIndex);

  Start  := StartIndex;
  Finish := FinishIndex;
end;

function SkipDigit
  (const AHaystack: string; var Start, Finish: Word): string;
begin
  Start  :=
    umlcstrparsers.SkipCharWhile(AHaystack, Start, #32);
  Finish :=
    umlcstrparsers.SkipWhile(AHaystack, Start, DecDigitSet);
  Result :=
    umlcstrparsers.CopyFromTo(AHaystack, Start, Finish);
  Start  := Finish;
end;

function SkipLetter
  (const AHaystack: string; var Start, Finish: Word): string;
begin
  Start  :=
    umlcstrparsers.SkipCharWhile(AHaystack, Start, #32);
  Finish :=
    umlcstrparsers.SkipWhile(AHaystack, Start, AlphaSet);
  Result :=
    umlcstrparsers.CopyFromTo(AHaystack, Start, Finish);
  Start  := Finish;
end;

function SkipEmpty
  (const Start, Finish: Word): Boolean;
begin
  Result :=
    ((Finish - Start) = 0);
  // Goal: Returns if there is a SubStr between the given indices is empty.
  // Objetivo: Regresa si hay una subcadena entre los indices dados esta vacia.
end;

function CopyFromTo
  (const ASource: string; const Start, Finish: Word): string;
var I, ACount: Word;
begin
  ACount := Succ(Finish - Start);

  Result := '';
  for I := 1 to ACount do
  begin
     Result := Result + ASource[1];
  end;
//  umlcstrings_new.Move(Result[1], Source[1], Start, ACount);
  // Goal: Returns a substring from "start" to "finish".
  // Objetivo: Regresa una subcadena desde "start" hasta "finish".
end;

function CopyFrom
  (const ASource: string; AIndex: Word): string;
var I, ACount: Word;
begin
  ACount :=
    umlcstrings.Length(ASource);
  ACount :=
    Succ(ACount - AIndex);

  Result := '';
  for I := 1 to ACount do
  begin
    Result := Result + ASource[1];
  end;
//  umlcstrings_new.Move(Result[1], Source[1], Start, ACount);
  // Goal: Returns a substring from "start" to "finish".
  // Objetivo: Regresa una subcadena desde "start" hasta "finish".
end;

function ParseFrom
  (const ASource: string; var Start, Finish: Word): string;
var I, ACount: Integer;
begin
  ACount := (Finish - Start);

  Result := '';
  for I := 1 to ACount do
  begin
     Result := Result + ASource[(Start + i - 1)];
  end;

  Start  := Finish;
  // Goal: Returns a substring from "start" to "finish" & update "start".

  // Objetivo: Regresa una subcadena desde "start" hasta "finish"
  // y actualiza "start.
end;

function ExtractCharUntil
 (const AHaystack: string;
  const AIndex: Word;
  const BreakChar: Char): string;
var I, L: Word;
begin
  umlcstrings.Clear(Result);

  I := AIndex;
  L := umlcstrings.Length(AHaystack);

  while ((I <= L) and (AHaystack[I] = #32)) do
  begin
    Inc(I);
    // Jump leading spaces
    // Brincar espacios al inicio
  end;

  while ((I <= L) and (AHaystack[I] <> BreakChar)) do
  begin
    umlcstrings.ConcatChar(Result, AHaystack[I]);
    Inc(I);
  end;
  // Goal: Returns a list of characters until a special marker is found.

  // Objetivo: Regresa una lista de caracteres hasta
  // que un marcador especial es encontrado.
end;

function ExtractCharUntilBack
 (const AHaystack: string;
  const AIndex: Word;
  const BreakChar: Char): string;
var I, L: Integer;
begin
  umlcstrings.Clear(Result);

  L := umlcstrings.Length(AHaystack);
  I := L;

  while ((I > AIndex) and (AHaystack[I] <> BreakChar)) do
  begin
    umlcstrings.ConcatCharBack(Result, AHaystack[I]);
    Dec(I);
  end;
  // Goal: Returns a list of characters until a special marker is found.

  // Objetivo: Regresa una lista de caracteres hasta
  // que un marcador especial es encontrado.
end;

function ExtractWhile
 (const AHaystack: string;
  const AIndex: Word;
  const SkipChars: ansicharset): string;
var I, L: Word;
begin
  umlcstrings.Clear(Result);

  I := AIndex;
  L :=
    umlcstrings.Length(AHaystack);

  //umlcdebug.DebugWriteLn('AIndex : [' + IntToStr(AIndex) + ']');
  //umlcdebug.DebugWriteEoLn();
  //
  //umlcdebug.DebugWriteLn('AHaystack[I] : [' + AHaystack[I] + ']');
  //umlcdebug.DebugWriteEoLn();

  while ((I <= L) and (umlccharsets.IsMember(BlanksSet, AHaystack[I]))) do
  begin
    Inc(I);
    // Jump leading spaces
    // Brincar espacios al inicio
  end;

  //umlcdebug.DebugWriteLn('I : [' + IntToStr(I) + ']');
  //umlcdebug.DebugWriteEoLn();
  //
  //umlcdebug.DebugWriteLn('L : [' + IntToStr(L) + ']');
  //umlcdebug.DebugWriteEoLn();
  //
  //umlcdebug.DebugWriteLn('AHaystack[I] : [' + AHaystack[I] + ']');
  //umlcdebug.DebugWriteEoLn();
  //
  //umlcdebug.DebugWriteLn('SkipChars : [' + SkipChars + ']');
  //umlcdebug.DebugWriteEoLn();

  while ((I <= L) and (umlccharsets.IsMember(SkipChars, AHaystack[I]))) do
  begin
    umlcstrings.ConcatChar(Result, AHaystack[I]);

    //umlcdebug.DebugWriteLn('AHaystack[I] : [' + AHaystack[I] + ']');
    //umlcdebug.DebugWriteEoLn();
    //
    //umlcdebug.DebugWriteLn('Result : [' + Result + ']');
    //umlcdebug.DebugWriteEoLn();

    Inc(I);
  end;
  // Goal: Returns a group of non-space selected characters.
  // Objetivo: Regresa un grupo de caracteres seleccionados que no son espacios.
end;

function ExtractUntil
 (const AHaystack: string;
  const AIndex: Word;
  const BreakChars: ansicharset): string;
var I, L: Word;
begin
  umlcstrings.Clear(Result);

  I := AIndex;
  L :=
    umlcstrings.Length(AHaystack);
  while ((I <= L) and (umlccharsets.IsMember(BlanksSet, AHaystack[I]))) do
  begin
    Inc(I);
    // Jump leading spaces
    // Brincar espacios al inicio
  end;

  //umlcdebug.DebugWriteLn('I : [' + IntToStr(I) + ']');
  //umlcdebug.DebugWriteEoLn();
  //
  //umlcdebug.DebugWriteLn('L : [' + IntToStr(L) + ']');
  //umlcdebug.DebugWriteEoLn();
  //
  //umlcdebug.DebugWriteLn('AHaystack[I] : [' + AHaystack[I] + ']');
  //umlcdebug.DebugWriteEoLn();
  //
  //umlcdebug.DebugWriteLn('BreakChars : [' + BreakChars + ']');
  //umlcdebug.DebugWriteEoLn();

  while ((I <= L) and (not umlccharsets.IsMember(BreakChars, AHaystack[I]))) do
  begin
    umlcstrings.ConcatChar(Result, AHaystack[I]);

    //umlcdebug.DebugWriteLn('AHaystack[I] : [' + AHaystack[I] + ']');
    //umlcdebug.DebugWriteEoLn();
    //
    //umlcdebug.DebugWriteLn('Result : [' + Result + ']');
    //umlcdebug.DebugWriteEoLn();

    Inc(I);
  end;
  // Goal: Returns a group of non-space characters.
  // Objetivo: Regresa un grupo de caracteres que no son espacios.
end;

function ExtractAt
  (const AHaystack: string;
   const AIndex: Word): string;
var I, L: Word;
begin
  umlcstrings.Clear(Result);

  I := AIndex;
  L := umlcstrings.Length(AHaystack);
  while ((I <= L) and (AHaystack[I] = #32)) do
  begin
    Inc(I);
  // Jump leading spaces
  // Brincar espacios al inicio
  end;

  while ((I <= L) and (AHaystack[I] <> #32)) do
  begin
    umlcstrings.ConcatChar(Result, AHaystack[I]);
    Inc(I);
  end;
  // Goal: Returns a group of non-space characters.
  // Objetivo: Regresa un grupo de caracteres que no son espacios.
end;

function ExtractUntilIndex
 (const AHaystack: string;
  var   AIndex: Word;
  const BreakChars: ansicharset): string;
var I, L: Integer;
begin
  umlcstrings.Clear(Result);

  I := AIndex;
  L := umlcstrings.Length(AHaystack);
  while ((I <= L) and (AHaystack[I] = #32)) do
  begin
    Inc(I);
    // Jump leading spaces
    // Brincar espacios al inicio
  end;

  while ((I <= L) and not umlccharsets.IsMember(BreakChars, AHaystack[I])) do
  begin
    umlcstrings.ConcatChar(Result, AHaystack[I]);
    Inc(I);
  end;

  AIndex := I;
  // Goal: Returns a group of non-space characters.
  // Objetivo: Regresa un grupo de caracteres que no son espacios.
end;

function ParseCharSingle
  (const ASource: string; var AIndex: Word; ValidChar: ansichar): Boolean;
var Backup: Integer; L: Word;
begin
  Backup := AIndex;
  // backup pointer
  // respaldar apuntador

  L := umlcstrings.Length(ASource);
  if ((AIndex <= L) and (ASource[AIndex] = ValidChar))
    then Inc(AIndex);
  // try parsing
  // intenta parsear

  Result := (AIndex > Backup);
  // verify any changes
  // revisa si hubo cambios

  if (not Result)
    then AIndex := Backup;
  // restore pointer
  // restaurar apuntador

  // Goal: Returns a single character.
  // Objetivo: Regresa un solo caracter.
end;

function ParseSingle
  (const ASource: string; var AIndex: Word; ValidChars: ansicharset): Boolean;
var Backup: Integer; L: Integer;
begin
  Backup := AIndex;
  // backup pointer
  // respaldar apuntador

  L := Length(ASource);
  if ((AIndex <= L) and umlccharsets.IsMember(ValidChars, ASource[AIndex]))
    then Inc(AIndex);
  // try parsing
  // intenta parsear

  Result := (AIndex > Backup);
  // verify any changes
  // revisa si hubo cambios

  if (not Result)
    then AIndex := Backup;
  // restore pointer
  // restaurar apuntador

  // Goal: Returns a single character.
  // Objetivo: Regresa un solo caracter.
end;

function ParseCharWhile
  (const ASource: string; var AIndex: Word; ValidChar: ansichar): Boolean;
var Backup: Integer; L: Word;
begin
  Backup := AIndex;
  // backup pointer
  // respaldar apuntador

  L := umlcstrings.Length(ASource);
  while ((AIndex <= L) and (ASource[AIndex] = ValidChar)) do
   Inc(AIndex);
  // try parsing
  // intenta parsear

  Result := (AIndex > Backup);
  // verify any changes
  // revisa si hubo cambios

  if (not Result)
    then AIndex := Backup;
  // restore pointer
  // restaurar apuntador

  // Goal: Returns a group of characters.
  // Objetivo: Regresa un grupo de caracteres.
end;

function ParseCharUntil
  (const ASource: string; var AIndex: Word; BreakChar: ansichar): Boolean;
var Backup: Word; L: Word;
begin
  Backup := AIndex;
  // backup pointer
  // respaldar apuntador

  L := umlcstrings.Length(ASource);
  while (AIndex <= L) and (ASource[AIndex] <> BreakChar) do
   Inc(AIndex);
  // try parsing
  // intenta parsear

  Result := (AIndex > Backup);
  // verify any changes
  // revisa si hubo cambios

  if (not Result)
    then AIndex := Backup;
  // restore pointer
  // restaurar apuntador

  // Goal: Returns a group of ansicharacters.
  // Objetivo: Regresa un grupo de caracteres.
end;

function ParseWhile
 (const AHaystack: string; var AIndex: Word; ValidChars: ansicharset): Boolean;
var Backup: Word; L: Word;
begin
  Backup := AIndex;
  // backup pointer
  // respaldar apuntador

  L := umlcstrings.Length(AHaystack);
  while (AIndex <= L) and umlccharsets.IsMember(ValidChars, AHaystack[AIndex]) do
   Inc(AIndex);
  // try parsing
  // intenta parsear

  Result := (AIndex > Backup);
  // verify any changes
  // revisa si hubo cambios

  if (not Result)
    then AIndex := Backup;
  // restore pointer
  // restaurar apuntador

  // Goal: Returns a group of non-space ansicharacters.
  // Objetivo: Regresa un grupo de caracteres que no son espacios.
end;

function ParseUntil
 (const AHaystack: string; var AIndex: Word; BreakChars: ansicharset): Boolean;
var Backup: Word; L: Word;
begin
  Backup := AIndex;
  // backup pointer
  // respaldar apuntador

  L := umlcstrings.Length(AHaystack);
  while (AIndex <= L) and not IsMember(BreakChars, AHaystack[AIndex]) do
   Inc(AIndex);
  // try parsing
  // intenta parsear

  Result := (AIndex > Backup);
  // verify any changes
  // revisa si hubo cambios

  if (not Result)
    then AIndex := Backup;
  // restore pointer
  // restaurar apuntador

  // Goal: Returns a group of non-space ansicharacters.
  // Objetivo: Regresa un grupo de caracteres que no son espacios.
end;

function ParseQuotedStr
  (const ASource: string; var AIndex: Word): Boolean;
var Backup: Word;
begin
  Backup := AIndex;
  // backup pointer
  // respaldar apuntador

  Result := ParseCharSingle(ASource, AIndex, ansiSingleQuote);
  if (Result) then
  begin
    Result := ParseCharUntil(ASource, AIndex, ansiSingleQuote);
    if (Result) then
    begin
      Result := ParseCharSingle(ASource, AIndex, ansiSingleQuote);
    end;
  end;
  // try parsing
  // intenta parsear

  Result := Result and (AIndex > Backup);
  // verify any changes
  // revisa si hubo cambios

  if (not Result)
    then AIndex := Backup;
  // restore pointer
  // restaurar apuntador

  // Goal: Parse a quoted string.
  // Objetivo: Parsear una cadena entre comillas.
end;

function ParseUnsigned
  (const ASource: string; var AIndex: Word): Boolean;
var Backup: Word;
begin
  Backup := AIndex;
  // backup pointer
  // respaldar apuntador

  Result := ParseWhile(ASource, AIndex, DecDigitSet);
  // try parsing
  // intenta parsear

  Result := Result and (AIndex > Backup);
  // verify any changes
  // revisa si hubo cambios

  if (not Result)
    then AIndex := Backup;
  // restore pointer
  // restaurar apuntador

  // Goal: Parse a unsigned integer.
  // Objetivo: Parsear un entero sin signo.
end;

function ParseIdentifier
  (const ASource: string; var AIndex: Word): Boolean;
var Backup: Integer;
begin
  Backup := AIndex;
  // backup pointer
  // respaldar apuntador

  Result := ParseSingle(ASource, AIndex, AlphaSet);
  if (Result) then
  begin
    Result := ParseWhile(ASource, AIndex, IDSet);
  end;
  // try parsing
  // intenta parsear

  Result := Result and (AIndex > Backup);
  // verify any changes
  // revisa si hubo cambios

  if (not Result)
    then AIndex := Backup;
  // restore pointer
  // restaurar apuntador

  // Goal: Parse an identifier.
  // Objetivo: Parsear un identificador.
end;

function ParseQualified(const ASource: string; var AIndex: Word): Boolean;
var Backup: Integer;
begin
  Backup := AIndex;
  // backup pointer
  // respaldar apuntador

  Result := ParseSingle(ASource, AIndex, AlphaSet);
  if (Result) then
  begin
    Result := ParseWhile(ASource, AIndex, QIDSet);
  end;
  // try parsing
  // intenta parsear

  Result := Result and (AIndex > Backup);
  // verify any changes
  // revisa si hubo cambios

  if (not Result)
    then AIndex := Backup;
  // restore pointer
  // restaurar apuntador

  // Goal: Parse an identifier.
  // Objetivo: Parsear un identificador.
end;

function ParseCount
  (const ASource: string; var AIndex: Word; Count: Integer): Boolean;
var Backup: Integer;
begin
  Backup := AIndex;
  // backup pointer

  Result := (Pred(AIndex + Count) <= System.Length(ASource));
  if (Result)
    then Inc(AIndex, Count);

  if (not Result)
    then AIndex := Backup;
  // restore pointer

  // Goal: Parse "Count" ansicharacters.
end;

function IsQuotedStr(const ASource: string): Boolean;
var AIndex: Word;
begin
  AIndex := 1;
  Result := ParseQuotedStr(ASource, AIndex);
end;

function IsUnsigned(const ASource: string): Boolean;
var AIndex: Word;
begin
  AIndex := 1;
  Result := ParseUnsigned(ASource, AIndex);
end;

function ExtractQuotedStr
  (const ASource: string; var ADest: string; var AIndex: Word): Boolean;
//var Start: Integer;
begin
  Result := false;

  //Start := Index;
  //Result := ParseQuotedStr(ASource, Index);
  //if (Result)
  //  then Dest := ParseFrom(ASource, Start, Index);
  // Goal: Extract a quoted string.
  // Objetivo: Extractar una cadena entre comillas.
end;

function ExtractUnsigned
  (const ASource: string; var ADest: string; var AIndex: Word): Boolean;
//var Start: Integer;
begin
  Result := false;

  //Start := Index;
  //Result := ParseUnsigned(ASource, Index);
  //if (Result)
  //  then Dest := ParseFrom(ASource, Start, Index);
  // Goal: Extract a unsigned integer.
  // Objetivo: Extractar un entero sin signo.
end;

function ExtractIdentifier
  (const ASource: string; var ADest: string; var AIndex: Word): Boolean;
//var Start: Integer;
begin
  Result := false;

  //Start := Index;
  //Result := ParseIdentifier(ASource, Index);
  //if (Result)
  //  then Dest := ParseFrom(ASource, Start, Index);
  // Goal: Extract an identifier.
  // Objetivo: Extractar un identificador.
end;

function ExtractQualified
  (const ASource: string; var ADest: string; var AIndex: Word): Boolean;
//var Start: Integer;
begin
  Result := false;

  //Start := Index;
  //Result := ParseQualified(ASource, Index);
  //if (Result)
  //  then Dest := ParseFrom(ASource, Start, Index);
  // Goal: Extract an identifier.
  // Objetivo: Extractar un identificador.
end;

function ExtractCount
  (const ASource: string; var ADest: string; var AIndex: Word; Count: Word): Boolean;
//var Start: Integer;
begin
  Result := false;

  //Start := Index;
  //Result := ParseCount(Source, Index, Count);
  //if (Result)
  //  then Dest := umlcstrparsers.(Source, Start, Index);
  // Goal: Extract "Count" ansicharacters.
  // Objetivo: Extraer "Count" caracteres.
end;


(* global procedures *)

procedure ReplaceQuotedUppercase
  (var ADest: string; const A, B: Char);
var InsideString: Boolean; AIndex: Integer; C: Char;
begin
  InsideString := FALSE;
  for AIndex := 1 to Length(ADest) do
  begin
    C := ADest[AIndex];
    if (InsideString)
      then InsideString := not (C = B)
      else InsideString := (C = A);
    // initial or final delimiter found ?
    // delimitador inicial o final encontrado ?

    if (not InsideString and umlcchars.IsLowercase(C))
      then CharLowerBuff(@C, 1);
    ADest[AIndex] := C;
    // replace characters
    // reemplazar caracteres
  end;
  // Goal: Changes the given string into uppercase,
  // without modifying delimited substrings.

  // Objetivo: Cambia la cadena dada a mayusculas.
  // sin modificar a las subcadenas delimitadas.
end;

procedure ReplaceQuotedLowercase
  (var ADest: string; const A, B: Char);
var InsideString: Boolean; AIndex: Integer; C: Char;
begin
  InsideString := FALSE;
  for AIndex := 1 to Length(ADest) do
  begin
    C := ADest[AIndex];
    if (InsideString)
      then InsideString := not (C = B)
      else InsideString := (C = A);
    // initial or final delimiter found ?
    // delimitador inicial o final encontrado ?

    if (not InsideString and umlccharsets.IsMember(UpperSet, C))
      then CharUpperBuff(@C, 1);
    ADest[AIndex] := C;
    // replace characters
    // reemplazar caracteres
  end;
  // Goal: Changes the given string into lowercase,
  // without modifying delimited substrings.

  // Objetivo: Cambia la cadena dada a minusculas.
  // sin modificar a las subcadenas delimitadas.
end;

procedure ReplaceQuotedTogglecase
  (var ADest: string; const A, B: Char);
var InsideString: Boolean; AIndex: Integer; C: Char;
begin
  InsideString := FALSE;
  for AIndex := 1 to Length(ADest) do
  begin
    C := ADest[AIndex];
    if (InsideString)
      then InsideString := not (C = B)
      else InsideString := (C = A);
    // initial or final delimiter found ?
    // delimitador inicial o final encontrado ?

    if (not InsideString and umlccharsets.IsMember(UpperSet, C))
      then ADest[AIndex] := Chr(Ord(C) + 32)
    // replace characters
    // reemplazar caracteres
  end;
  // Goal: Changes the given string into Togglecase,
  // without modifying delimited substrings.

  // Objetivo: Cambia la cadena dada a minusculas.
  // sin modificar a las subcadenas delimitadas.
end;

procedure ReplaceQuotedCapitalize
  (var ADest: string; const A, B: Char);
var InsideString: Boolean; AIndex: Integer; C: Char;
begin
  InsideString := FALSE;
  for AIndex := 1 to Length(ADest) do
  begin
    C := ADest[AIndex];
    if (InsideString)
      then InsideString := not (C = B)
      else InsideString := (C = A);
    // initial or final delimiter found ?
    // delimitador inicial o final encontrado ?

    if (not InsideString and umlccharsets.IsMember(UpperSet, C))
      then ADest[AIndex] := Chr(Ord(C) + 32)
    // replace characters
    // reemplazar caracteres
  end;
  // Goal: Changes the given string into capitalize,
  // without modifying delimited substrings.

  // Objetivo: Cambia la cadena dada a minusculas.
  // sin modificar a las subcadenas delimitadas.
end;

procedure ReplaceUppercase
  (var ADest: string);
begin
  ADest :=
    umlcstrings.UpperCaseCopy(ADest);
  // Goal: Changes the given string into uppercase.
  // Objetivo: Cambia la cadena dada a mayusculas.
end;

procedure ReplaceLowercase
  (var ADest: string);
begin
  ADest :=
    umlcstrings.LowerCaseCopy(ADest);
  // Goal: Changes the given string into lowercase.
  // Objetivo: Cambia la cadena dada a minusculas.
end;

procedure ReplaceTogglecase
  (var ADest: string);
begin
  ADest :=
    umlcstrings.TogglecaseCopy(ADest);
  // Goal: Changes the given string into Togglecase.
  // Objetivo: Cambia la cadena dada a minusculas.
end;

procedure ReplaceCapitalize
  (var ADest: string);
begin
  ADest :=
    umlcstrings.CapitalizeCopy(ADest);
  // Goal: Changes the given string into capitalize.
  // Objetivo: Cambia la cadena dada a minusculas.
end;

procedure ReplaceTrimLeft
  (var ADest: string);
begin
  ADest := SysUtils.TrimLeft(ADest);
  // Goal: Returns the given string without leading spaces.
  // Objetivo: Regresa la cadena dada sin espacios iniciales.
end;

procedure ReplaceTrimRight
  (var ADest: string);
begin
  ADest := SysUtils.TrimRight(ADest);
  // Goal: Returns the given string without trailing spaces.
  // Objetivo: Regresa la cadena dada sin espacios finales.
end;

procedure ReplaceTrim
  (var ADest: string);
begin
  ADest := SysUtils.Trim(ADest);
  // Goal: Returns the given string without leading & trailing spaces.
  // Objetivo: Regresa la cadena dada sin espacios iniciales y finales.
end;

procedure ReplaceUnTrimLeft
  (var ADest: string; ACount: Word);
begin
  ADest :=
    umlcstrings.UnTrimLeftCopy(ADest, ACount);
  // Goal: Returns the given string plus leading spaces.
  // Objetivo: Regresa la cadena dada mas espacios iniciales.
end;

procedure ReplaceUnTrimRight
  (var ADest: string; ACount: Word);
begin
  ADest :=
    umlcstrings.UnTrimRightCopy(ADest, ACount);
  // Goal: Returns the given string plus trailing spaces.
  // Objetivo: Regresa la cadena dada mas espacios finales.
end;

procedure ReplaceAllCharByChar
  (var ADest: string; A, B: Char);
var I, L: Integer;
begin
  L := Length(ADest);
  for i := 1 to L do
  begin
    if (ADest[i] = A) then
    begin
      ADest[i] := B;
    end;
  end;
  // Goal: Replace a specific character from a string.
  // Objetivo: Reemplazar un caracter en especifico de una cadena.
end;

procedure ReplaceAllCharByString
  (var ADest: string; A: Char; B: string);
begin
  ADest :=
    umlcstrparsers.ReplaceAllCharToStrCopy
      (ADest, A, B);
end;

procedure ReplaceCharSetByChar
 (var AHaystack: string; const ASource: charset; ADest: Char);
begin
  AHaystack :=
    umlcstrparsers.ReplaceCharSetByCharCopy(AHaystack, ASource, ADest);
  // Goal: Replace a specific character set from a string.
  // Objetivo: Reemplazar un conjunto caracter en especifico de una cadena.
end;

procedure ReplaceAllStrByStr
  (var AHaystack: string; const ASource, ADest: string);
var Finished: Boolean; AIndex: Word; Temp: ansistring;
begin
  AIndex := 1;
  repeat
    Temp := AHaystack;
    umlcstrparsers.ReplaceStrByStrAt(Temp, AIndex, ASource, ADest);
    AHaystack := Temp;
    Finished := (AIndex = 0);
  until Finished;
  // Goal: Replaces all the "Source" found, "AValue" with "Dest".
  // Objetivo: Reemplaza todas las "Source" encontradas en "AValue" por "Dest".}
end;

procedure ReplaceStrByStrAt
  (var AHaystack: string;
   var AIndex: Word; const ASource, ADest: string);
var BeforeCount, AfterCount, S, V: Word;
    BeforeString, AfterString: string;
begin
  AIndex :=
    umlcstrparsers.StrPosOf(AHaystack, ASource);
  // localizar en que posicion inicia la cadena que se va a reemplazar
  // locate, which location starts the string to be replaced

  if (AIndex > 0) then
  begin
    S := umlcstrings.Length(ASource);
    V := umlcstrings.Length(AHaystack);

    AfterCount := (V - Pred(AIndex + S));
    // obtener la long. de la cadena despues de la parte que se va a reemplazar
    // obtain the length of the string after the section to be replaced

    BeforeCount := Pred(AIndex);
    // obtener la long. de la cadena antes de la parte que se va a reemplazar
    // obtain the length of the string before the section to be replaced

    if (AIndex <> 0) then
    begin
      umlcstrings.AssignLeft(BeforeString, AHaystack, BeforeCount);
      umlcstrings.AssignRight(AfterString, AHaystack, AfterCount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted string

      umlcstrings.Clear(AHaystack);
      umlcstrings.ConcatStr(AHaystack, BeforeString);
      umlcstrings.ConcatStr(AHaystack, ADest);
      umlcstrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces the first "Source" found, "AValue" with "Dest".
  // Objetivo: Reemplaza la primera "Source" encontrada en "AValue" por "Dest".}
end;

procedure ReplaceStrByStrAtSame
  (var AHaystack: string;
   var AIndex: Word; const ASource, ADest: string);
var BeforeCount, AfterCount, S, V: Integer;
    BeforeString, AfterString: string;
begin
  AIndex :=
    umlcstrparsers.StrPosOfSame(AHaystack, ASource);
  // localizar en que posicion inicia la cadena que se va a reemplazar
  // locate, which location starts the string to be replaced

  if (AIndex > 0) then
  begin
    S := umlcstrings.Length(ASource);
    V := umlcstrings.Length(AHaystack);

    AfterCount := (V - Pred(AIndex + S));
    // obtener la long. de la cadena despues de la parte que se va a reemplazar
    // obtain the length of the string after the section to be replaced

    BeforeCount := Pred(AIndex);
    // obtener la long. de la cadena antes de la parte que se va a reemplazar
    // obtain the length of the string before the section to be replaced

    if (AIndex <> 0) then
    begin
      umlcstrings.AssignLeft(BeforeString, AHaystack, BeforeCount);
      umlcstrings.AssignRight(AfterString, AHaystack, AfterCount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted string

      umlcstrings.Clear(AHaystack);
      umlcstrings.ConcatStr(AHaystack, BeforeString);
      umlcstrings.ConcatStr(AHaystack, ADest);
      umlcstrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces the first "Source" found, "AValue" with "Dest".
  // Objetivo: Reemplaza la primera "Source" encontrada en "AValue" por "Dest".}
end;

procedure ReplaceStrByStr
  (var AHaystack: string; const ASource, ADest: string);
begin
  AHaystack :=
    umlcstrparsers.ReplaceStrByStrCopy(AHaystack, ASource, ADest);
  // Goal: Replaces the first "Source" found, "AValue" with "Dest".
  // Objetivo: Reemplaza la primera "Source" encontrada en "AValue" por "Dest".}
end;

procedure ReplaceCharByStr
  (var AHaystack: string; const ASource: Char; const ADest: string);
begin
  AHaystack :=
    umlcstrparsers.ReplaceCharByStrCopy(AHaystack, ASource, ADest);
  // Goal: Replaces the first "Source" found, "AHaystack" with "Dest".
  // Objetivo: Reemplaza la primera "Source" encontrada en "AHaystack" por "Dest".}
end;

procedure RemoveChar
  (var AHaystack: string; IgnoreChar: Char);
begin
  AHaystack :=
    umlcstrparsers.RemoveCharCopy(AHaystack, IgnoreChar);
  // Goal: Delete a specific character from a string.
  // Objetivo: Eliminar un caracter en especifico de una cadena.
end;

procedure RemoveCharSet
  (var AHaystack: string; IgnoreChars: charset);
begin
  AHaystack :=
    umlcstrparsers.RemoveCharSetCopy(AHaystack, IgnoreChars);
  // Goal: Delete a specific character set from a string.
  // Objetivo: Eliminar un conjunto caracter en especifico de una cadena.
end;


end.

