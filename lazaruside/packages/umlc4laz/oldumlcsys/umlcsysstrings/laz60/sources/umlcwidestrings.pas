(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwidestrings;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to support the predefined non pascal Delphi "string" type.
 ** The character encoding is Wide.
 ** The quantity of items is NOT restricted to 255 characters.
 ** The string is internally managed as memory referenced.
 **************************************************************************
 **)

interface
uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdstrtypes,
  umlcwidechars,
  umlcwidecharsets,
  umlcwidecharsetconsts,
  dummy;

// ---

const

  MOD_umlcwidestrings : TUMLCModule =
    ($76,$60,$90,$45,$50,$A2,$A3,$4A,$81,$A7,$30,$38,$E3,$4E,$68,$F2);

// ---

(* global functions *)

  function IsEmpty
    (const ASource: widestring): Boolean; overload;

  function Length
    (const ASource: widestring): Word; overload;


function ConcatCharCopy
  (const A: widestring;
   const B: widechar): widestring; overload; // operator +
function ConcatStrCopy
  (const A: widestring;
   const B: widestring): widestring; overload; // operator +



(* global operators *)



(* global procedures *)

procedure Reset
  (out ADest: widestring); overload;

procedure AssignLength
  (out   ADest:      widestring;
   const ANewLength: Word); overload;

procedure Clear
  (out ADest: widestring); overload;



procedure LastCharCountPtr
  (out   ADestPtr: pwidechar;
   out   ACount:   Byte;
   const ASource:  widestring);

procedure AssignReverse
  (out ADest: widestring; const ASource: widestring);


procedure ConcatChar
  (var   ADest:   widestring;
   const ASource: widechar); overload;
procedure ConcatStr
  (var   ADest:   widestring;
   const ASource: widestring); overload;

implementation

(* global functions *)

function IsEmpty
  (const ASource: widestring): Boolean;
begin
  Result :=
    System.Length(ASource) = 0;
  // Goal: Returns if a string is empty.
  // Objetivo: Regresa si una cadena esta vacia.
end;

function Length
  (const ASource: widestring): Word;
begin
  Result :=
    System.Length(ASource);
  // Goal: Returns the length of the given string.
  // Objetivo: Regresa la longuitud de la cadena dada.
end;


function ConcatCharCopy
  (const A: widestring;
   const B: widechar): widestring;
begin
  Result :=
    A + B;
  //Result :=
  //  umlcansistrings.ConcatCharCopy(A, B);
  // Objetivo: Agregar un caracter al final de la cadena dada.
  // Goal: Add a character at the end of the given string.
end;

function ConcatStrCopy
  (const A: widestring;
   const B: widestring): widestring; overload; // operator +
begin
  Result :=
    A + B;
  //Result :=
  //  umlcansistrings.ConcatStrCopy(A, B);
  // Objetivo: Agregar una cadena al final de la cadena dada.
  // Goal: Add a string at the end of the given string.
end;

(* global operators *)



(* global procedures *)

procedure Reset
  (out ADest: widestring);
begin
  ADest := '';
  System.setLength(ADest, 0);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;

procedure AssignLength
  (out   ADest:      widestring;
   const ANewLength: Word);
begin
  ADest := '';
  System.setLength(ADest, ANewLength);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;

procedure Clear
  (out ADest: widestring);
begin
  ADest := '';
  //System.FillByte(ADest, sizeof(ADest), 0);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;




procedure LastCharCountPtr
  (out   ADestPtr: pwidechar;
   out   ACount:   Byte;
   const ASource:  widestring);
begin
  ADestPtr := nil;
  ACount   := 0;
  if (ASource <> '') then
  begin
    ACount := umlcwidestrings.Length(ASource);
    ADestPtr :=
      @(ASource[ACount]);
  end;
end;

procedure AssignReverse
  (out ADest: widestring; const ASource: widestring);
var CanContinue: Boolean; P: pwidechar; ACount: Byte;
begin
  umlcwidestrings.Clear(ADest);
  CanContinue :=
    (not umlcwidestrings.IsEmpty(ASource));
  if (CanContinue) then
  begin
    umlcwidestrings.LastCharCountPtr
      (P, ACount, ASource);
    while ((P <> nil) and (ACount > 0)) do
    begin
      umlcwidestrings.ConcatChar(ADest, P^);

      System.Dec(P);
      System.Dec(ACount);
    end;
  end;
end;

procedure ConcatChar
  (var ADest: widestring; const ASource: widechar);
begin
  ADest := ADest + ASource;
  // Objetivo: Agregar un caracter al final de la cadena dada.
  // Goal: Add a character at the end of the given string.
end;

procedure ConcatStr
  (var ADest: widestring; const ASource: widestring);
begin
  ADest := ADest + ASource;
  // Objetivo: Agregar una cadena al final de la cadena dada.
  // Goal: Add a string at the end of the given string.
end;


end.

