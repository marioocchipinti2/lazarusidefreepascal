(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwideshortstrparsers;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Quick Text Search and Parsing operations,
 ** for FreePascal's Default pascal 256 items array strings.
 ** The character encoding is Wide.
 **************************************************************************
 **)

interface

uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdstrtypes,
  umlcansichars,
  umlcansicharsets,
  umlcansicharsetconsts,
  umlcansishortstrings,
dummy;

// ---

const

  MOD_umlcwideshortstrparsers : TUMLCModule =
    ($6F,$C3,$F7,$C5,$9B,$BC,$67,$4B,$BF,$02,$F2,$0B,$0C,$54,$BC,$79);

// ---

(* global functions *)


implementation

(* global functions *)



end.

