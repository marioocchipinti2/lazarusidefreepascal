(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansishortstrings;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to support the predefined pascal "string" type.
 ** The character encoding is A.N.S.I.
 ** The quantity of items is restricted to 255 characters.
 **************************************************************************
 **)

interface
uses
{$IFDEF MSWINDOWS}
  Windows, 
  //Messages, 
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdstrtypes,
  umlcansichars,
  umlcansicharsets,
  umlcansicharsetconsts,
  dummy;

// ---

const

 MOD_umlcansishortstrings : TUMLCModule =
   ($1D,$D9,$E7,$EE,$2D,$3E,$2E,$49,$B6,$99,$AE,$F8,$F9,$10,$AC,$BE);

// ---

(* global functions *)

function IsEmpty
  (const ASource: umlcansishortstring): Boolean; overload;

function Length
  (const ASource: umlcansishortstring): Word; overload;

  function IsStringOfChar
    (const AHaystack: umlcansishortstring; ANeedle: ansichar): Boolean; overload;

  function UppercaseCopy
    (const ASource: umlcansishortstring): umlcansishortstring; overload;
  function LowercaseCopy
    (const ASource: umlcansishortstring): umlcansishortstring; overload;
  function TogglecaseCopy
    (const ASource: umlcansishortstring): umlcansishortstring; overload;
  function CapitalizeCopy
    (const ASource: umlcansishortstring): umlcansishortstring; overload;

  function StringOfCharCopy
    (const ASource: ansichar; const ACount: Byte): umlcansishortstring;

  function LeftCopy
    (const ASource: umlcansishortstring; const ACount: Byte): umlcansishortstring;
  function RightCopy
    (const ASource: umlcansishortstring; const ACount: Byte): umlcansishortstring;

  function CAR
    (const ASource: umlcansishortstring): umlcansishortstring; overload;
  function CDR
    (const ASource: umlcansishortstring): umlcansishortstring; overload;

  function TryStrToChar
    (const ASource: umlcansishortstring; var ADest: ansichar): Boolean;
  function StrToCharDef
    (const ASource: umlcansishortstring; const ADefSource: ansichar): ansichar;
  function StrToChar
    (const ASource: umlcansishortstring): ansichar;

(* global procedures *)

procedure Reset
  (out ADest: umlcansishortstring); overload;

procedure AssignLength
  (out   ADest:      umlcansishortstring;
   const ANewLength: Byte); overload;

procedure Clear
  (out ADest: umlcansishortstring); overload;

  function TryAssignCopyAt
    (out   ADest:        umlcansishortstring;
     const ASource:      umlcansishortstring;
     const ASourceIndex: Byte): Boolean;
  function TryAssignCopyAtCount
    (out   ADest:        umlcansishortstring;
     const ASource:      umlcansishortstring;
     const ASourceIndex: Byte;
     const ASourceCount: Byte): Boolean;
  function TryAssignCopyCount
    (out   ADest:        umlcansishortstring;
     const ASource:      umlcansishortstring;
     const ASourceCount: Byte): Boolean;

  procedure AssignCopyAt
    (out   ADest:        umlcansishortstring;
     const ASource:      umlcansishortstring;
     const ASourceIndex: Byte);
  procedure AssignCopyAtCount
    (out   ADest:        umlcansishortstring;
     const ASource:      umlcansishortstring;
     const ASourceIndex: Byte;
     const ASourceCount: Byte);
  procedure AssignCopyCount
    (out   ADest:        umlcansishortstring;
     const ASource:      umlcansishortstring;
     const ASourceCount: Byte);

  procedure AssignLeft
    (var   ADest:   umlcansishortstring;
     const ASource: umlcansishortstring;
     const ACount:  Byte);
  procedure AssignRight
    (var   ADest:   umlcansishortstring;
     const ASource: umlcansishortstring;
     const ACount:  Byte);

  procedure ReplaceLeft
    (out   ADest:   umlcansishortstring;
     const ASource: umlcansishortstring;
     const ACount:  Byte);
  procedure ReplaceRight
    (out   ADest:   umlcansishortstring;
     const ASource: umlcansishortstring;
     const ACount:  Byte);

  procedure ReplaceUppercase
    (var ADest: umlcansishortstring); overload;
  procedure ReplaceLowercase
    (var ADest: umlcansishortstring); overload;
  procedure ReplaceTogglecase
    (var ADest: umlcansishortstring); overload;
  procedure ReplaceCapitalize
    (var ADest: umlcansishortstring); overload;

procedure LastCharCountPtr
  (out   ADestPtr: pansichar;
   out   ACount:   Byte;
   const ASource:  umlcansishortstring);

procedure AssignReverse
  (out ADest: umlcansishortstring; const ASource: umlcansishortstring);

procedure ConcatChar
  (var   ADest:   umlcansishortstring;
   const ASource: ansichar); overload;
procedure ConcatStr
  (var   ADest:   umlcansishortstring;
   const ASource: umlcansishortstring); overload;

  function StartsWith
    (const AHaystack: umlcansishortstring; const ANeedle: umlcansishortstring): Boolean;
  function FinishesWith
    (const AHaystack: umlcansishortstring; const ANeedle: umlcansishortstring): Boolean;

(* global operators *)

  function ConcatCharCopy
    (const A: umlcansishortstring;
     const B: ansichar): umlcansishortstring; overload; // operator +
  function ConcatStrCopy
    (const A: umlcansishortstring;
     const B: umlcansishortstring): umlcansishortstring; overload; // operator +

  procedure AssignStr
    (var   ADest:   umlcansishortstring;
     const ASource: umlcansishortstring); overload; // operator :=
  procedure AssignChar
    (var   ADest:   umlcansishortstring;
     const ASource: ansichar); overload; // operator :=

type
  TIsEqualANSIStr =
    {^}function (const A, B: umlcansishortstring): Boolean;

type
  TIsEqualANSIStrAt =
    {^}function (const AHaystack, ANeedle: umlcansishortstring; const AIndex: Word): Boolean;

  function SameStrAt
    (const AHaystack, ANeedle: umlcansishortstring; const AIndex: Word): Boolean;
  function EqualStrAt
    (const AHaystack, ANeedle: umlcansishortstring; const AIndex: Word): Boolean;

  function SameStrIn
    (const AHaystack, ANeedle: umlcansishortstring; var AIndex: Word): Boolean;
  function EqualStrIn
    (const AHaystack, ANeedle: umlcansishortstring; var AIndex: Word): Boolean;

  function SameStr
    (const AHaystack, ANeedle: umlcansishortstring; var AIndex: Word): Boolean;
  function EqualStr
    (const AHaystack, ANeedle: umlcansishortstring; var AIndex: Word): Boolean;

  function Search
    (const AHaystack, ANeedle: umlcansishortstring; var AIndex: Word;
     CompareStrAt: TIsEqualANSIStrAt): Boolean;

  function Equal
    (const A, B: umlcansishortstring): Boolean; // operator :=

  function SameText
    (const A, B: umlcansishortstring): Boolean;

  function Compare
    (const A, B: umlcansishortstring): TComparison;

  function EqualByOptions
    (const A, B: umlcansishortstring; Options: TStringOptions): Boolean;
  function CompareByOptions
    (const A, B: umlcansishortstring; Options: TStringOptions): TComparison;

implementation

(* global functions *)

function IsEmpty
  (const ASource: umlcansishortstring): Boolean;
begin
  Result :=
    System.Length(ASource) = 0;
  // Goal: Returns if a string is empty.
  // Objetivo: Regresa si una cadena esta vacia.
end;

function Length
  (const ASource: umlcansishortstring): Word;
begin
  Result :=
    System.Length(ASource);
  // Goal: Returns the length of the given string.
  // Objetivo: Regresa la longuitud de la cadena dada.
end;

function IsStringOfChar
  (const AHaystack: umlcansishortstring; ANeedle: ansichar): Boolean;
var I, L: Byte; Match: Boolean;
begin
  L :=
    umlcansishortstrings.Length(AHaystack);

  Result := (L > 0);
  if (Result) then
  begin
    I := 1; Match := TRUE;
    while ((I <= L) and (Match)) do
    begin
      Match :=
        (AHaystack[i] = ANeedle);
      Inc(I);
    end;

    Result := Match;
  end;
  // Objetivo: Regresa si una cadena esta compuesta solo del mismo caracter.
  // Goal: Returns if a string is composed with the same character.
end;

function UppercaseCopy
  (const ASource: umlcansishortstring): umlcansishortstring;
var I, Last: Byte; C: ansichar;
begin
  umlcansishortstrings.Clear(Result);

  Last :=
    umlcansishortstrings.Length(ASource);

  for I := 1 to Last do
  begin
    C :=
      umlcansichars.UppercaseCopy(ASource[i]);
    umlcansishortstrings.ConcatChar(Result, C);
  end;
  // Goal: Returns a uppercase copy of the given string.
  // Objetivo: Regresa una copia en mayusculas de la cadena dada.
end;

function LowercaseCopy
  (const ASource: umlcansishortstring): umlcansishortstring;
var I, Last: Byte; C: ansichar;
begin
  umlcansishortstrings.Clear(Result);

  Last :=
    umlcansishortstrings.Length(ASource);

  for I := 1 to Last do
  begin
    C :=
      umlcansichars.LowercaseCopy(ASource[i]);
    umlcansishortstrings.ConcatChar(Result, C);
  end;
  // Goal: Returns a lowercase copy of the given string.
  // Objetivo: Regresa una copia en minusculas de la cadena dada.
end;

function TogglecaseCopy
  (const ASource: umlcansishortstring): umlcansishortstring;
var I, Last: Byte; C: ansichar;
begin
  umlcansishortstrings.Clear(Result);

  Last :=
    umlcansishortstrings.Length(ASource);

  for I := 1 to Last do
  begin
    C := ASource[i];
    if (Windows.IsCharLowerA(C))
      then CharUpperBuff(@C, 1)
      else CharLowerBuff(@C, 1);
    umlcansishortstrings.ConcatChar(Result, C);
  end;
  // Goal: Swaps the sensitive case of each character in the given string.
  // Objetivo: Cambia el caso sensitivo de cada caracter en la cadena dada.
end;

function CapitalizeCopy
  (const ASource: umlcansishortstring): umlcansishortstring;
var I, Last: Byte; C: ansichar; MakeUppercase: Boolean;
begin
  Result := '';
  MakeUppercase := TRUE;
  // cambiar a la primera letra en mayusculas
  // change first letter into uppercase

  Last := System.Length(ASource);
  for I := 1 to Last do
  begin
    C := ASource[i];
    if (C <> #32) then
    begin
      if (MakeUppercase) then
      begin
        CharUpperBuff(@C, 1);
        MakeUppercase := FALSE;
      end else CharLowerBuff(@C, 1);
    end else MakeUppercase := TRUE;
    Result := Result + C;
  end;
  // Goal: Returns a copy with uppercase initials of the given string.
  // Objetivo: Regresa una copia con iniciales en mayusculas de la cadena dada.
end;

function StringOfCharCopy
  (const ASource: ansichar; const ACount: Byte): umlcansishortstring;
begin
  umlcansishortstrings.Reset(Result);

  umlcansishortstrings.AssignLength(Result, ACount);

  System.FillChar(Result[1], ACount, ASource);
  // Goal: Returns a string of the same character.
  // Objetivo: Regresa una cadena del mismo caracter.
end;

function LeftCopy
  (const ASource: umlcansishortstring; const ACount: Byte): umlcansishortstring;
begin
  umlcansishortstrings.ReplaceLeft
    (Result, ASource, ACount);
  // Goal: Returns the leftmost characters of "ASource".
  // Objetivo: Regresa los caracteres mas a la izquierda de "ASource".
end;

function RightCopy
  (const ASource: umlcansishortstring; const ACount: Byte): umlcansishortstring;
begin
  umlcansishortstrings.ReplaceRight
    (Result, ASource, ACount);
  // Goal: Returns the rightmost characters of "ASource".
  // Objetivo: Regresa los caracteres mas a la derecha de "ASource".
end;

function CAR
  (const ASource: umlcansishortstring): umlcansishortstring;
begin
  Result := ASource[1];
  // Objetivo: Regresa el primer caracter de una cadena (como en LISP).
  // Goal: Returns the first character of a string (as in LISP).
end;

function CDR
  (const ASource: umlcansishortstring): umlcansishortstring;
begin
  Result := System.Copy(ASource, 2, Pred(System.Length(ASource)));
  // Objetivo: Regresa el resto de una cadena (como en LISP).
  // Goal: Returns the rest of the string (as in LISP).
end;

function TryStrToChar
  (const ASource: umlcansishortstring; var ADest: ansichar): Boolean;
begin
  Result := (Length(ASource) > 0);
  if (Result)
    then ADest := ASource[1];
  // Goal: To cast a string in to a character.
  // Objetivo: Convertir una cadena en un caracter.
end;

function StrToCharDef
  (const ASource: umlcansishortstring; const ADefSource: ansichar): ansichar;
begin
  if (not TryStrToChar(ASource, Result))
    then Result := ADefSource;
  // Goal: To cast a string in to a character.
  // Objetivo: Convertir una cadena en un caracter.
end;

function StrToChar(const ASource: umlcansishortstring): ansichar;
begin
  if (not TryStrToChar(ASource, Result))
    then EInvalidCast.Create(SInvalidCast);
  // Goal: To cast a string in to a character.
  // Objetivo: Convertir una cadena en un caracter.
end;

(* global procedures *)

procedure Reset
  (out ADest: umlcansishortstring);
begin
  ADest[0] := chr(0);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;

procedure AssignLength
  (out   ADest:      umlcansishortstring;
   const ANewLength: Byte);
begin
  ADest[0] := chr(ANewLength);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;

procedure Clear
  (out ADest: umlcansishortstring);
begin
  System.FillChar(ADest, sizeof(ADest), #0);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;

function TryAssignCopyAt
  (out   ADest:        umlcansishortstring;
   const ASource:      umlcansishortstring;
   const ASourceIndex: Byte): Boolean;
var ASourceLen: Byte;
begin
  ASourceLen :=
    umlcansishortstrings.Length(ASource);
  Result :=
   ((ASourceIndex > 0) and (ASourceLen > 0));
  if (Result) then
  begin
    umlcansishortstrings.Reset(ADest);

    System.Move(ASource[ASourceIndex], ADest[1], ASourceLen);

    umlcansishortstrings.AssignLength(ADest, ASourceLen);
  end;
  // Goal: To copy a substring at the given index.
end;

function TryAssignCopyAtCount
  (out   ADest:        umlcansishortstring;
   const ASource:      umlcansishortstring;
   const ASourceIndex: Byte;
   const ASourceCount: Byte): Boolean;
var ASourceLen, ARealCount: Byte;
begin
  ASourceLen :=
    umlcansishortstrings.Length(ASource);
  Result :=
   ((ASourceIndex > 0) and(ASourceCount > 0) and (ASourceLen > 0));
  if (Result) then
  begin
    umlcansishortstrings.Reset(ADest);

    ARealCount :=
      Math.Min((ASourceLen - ASourceIndex), ASourceLen);

    System.Move(ASource[ASourceIndex], ADest[1], ARealCount);
    umlcansishortstrings.AssignLength(ADest, ARealCount);
  end;
end;

function TryAssignCopyCount
  (out   ADest:        umlcansishortstring;
   const ASource:      umlcansishortstring;
   const ASourceCount: Byte): Boolean;
var ASourceLen, ARealCount: Byte;
begin
  ASourceLen :=
    umlcansishortstrings.Length(ASource);
  Result :=
   ((ASourceCount > 0) and (ASourceLen > 0));
  if (Result) then
  begin
    umlcansishortstrings.Reset(ADest);

    ARealCount :=
      Math.Min(ASourceLen, ASourceCount);

    System.Move(ASource[1], ADest[1], ARealCount);
    umlcansishortstrings.AssignLength(ADest, ARealCount);
  end;
  // Objetivo: Copiar el contenido de una subcadena a un nueva cadena.
  // Goal: Copy the contents of a substring into a new string.
end;

procedure AssignCopyAt
  (out   ADest:        umlcansishortstring;
   const ASource:      umlcansishortstring;
   const ASourceIndex: Byte);
begin
  if (not TryAssignCopyAt(ADest, ASource, ASourceIndex)) then
  begin
    // raise error
  end;
end;

procedure AssignCopyAtCount
  (out   ADest:        umlcansishortstring;
   const ASource:      umlcansishortstring;
   const ASourceIndex: Byte;
   const ASourceCount: Byte);
begin
  if (not TryAssignCopyAtCount(ADest, ASource, ASourceIndex, ASourceCount)) then
  begin
    // raise error
  end;
end;

procedure AssignCopyCount
  (out   ADest:        umlcansishortstring;
   const ASource:      umlcansishortstring;
   const ASourceCount: Byte);
begin
  if (not TryAssignCopyCount(ADest, ASource, ASourceCount)) then
  begin
    // raise error
  end;
end;

procedure AssignLeft
  (var   ADest:   umlcansishortstring;
   const ASource: umlcansishortstring;
   const ACount:  Byte);
begin
  umlcansishortstrings.AssignCopyCount
    (ADest, ASource, ACount);
  // Goal: Returns the leftmost characters of "ADest".
  // Objetivo: Regresa los caracteres mas a la izquierda de "ADest".
end;

procedure AssignRight
  (var   ADest:   umlcansishortstring;
   const ASource: umlcansishortstring;
   const ACount:  Byte);
var AIndex: Byte;
begin
  AIndex :=
    Succ(Length(ASource) - ACount);
  umlcansishortstrings.AssignCopyAtCount
    (ADest, ASource, AIndex, ACount);
  // Goal: Returns the rightmost characters of "ADest".
  // Objetivo: Regresa los caracteres mas a la derecha de "ADest".
end;

procedure ReplaceLeft
  (out   ADest:   umlcansishortstring;
   const ASource: umlcansishortstring;
   const ACount:  Byte);
begin
  umlcansishortstrings.Reset(ADest);

  System.Move(ASource[1], ADest[1], ACount);

  umlcansishortstrings.AssignLength(ADest, ACount);
  // Goal: Returns the leftmost characters of "ASource".
  // Objetivo: Regresa los caracteres mas a la izquierda de "ASource".
end;

procedure ReplaceRight
  (out   ADest:   umlcansishortstring;
   const ASource: umlcansishortstring;
   const ACount:  Byte);
var AIndex: Byte;
begin
  umlcansishortstrings.Reset(ADest);

  AIndex :=
    Succ(umlcansishortstrings.Length(ASource)-ACount);

  System.Move(ASource[AIndex], ADest[1], ACount);

  umlcansishortstrings.AssignLength(ADest, ACount);
  // Goal: Returns the rightmost characters of "ASource".
  // Objetivo: Regresa los caracteres mas a la derecha de "ASource".
end;

procedure ReplaceUppercase
  (var ADest: umlcansishortstring);
var I, Last: Byte; C: ansichar;
begin
  umlcansishortstrings.Clear(ADest);

  Last :=
    umlcansishortstrings.Length(ADest);

  for I := 1 to Last do
  begin
    C :=
      umlcansichars.UppercaseCopy(ADest[i]);
    umlcansishortstrings.ConcatChar
      (ADest, C);
  end;
  // Goal: Changes the given string into uppercase.
  // Objetivo: Cambia la cadena dada a mayusculas.
end;

procedure ReplaceLowercase
  (var ADest: umlcansishortstring);
var I, Last: Byte; C: ansichar;
begin
  umlcansishortstrings.Clear(ADest);

  Last :=
    umlcansishortstrings.Length(ADest);

  for I := 1 to Last do
  begin
    C :=
      umlcansichars.LowercaseCopy(ADest[i]);
    umlcansishortstrings.ConcatChar
      (ADest, C);
  end;
  // Goal: Changes the given string into lowercase.
  // Objetivo: Cambia la cadena dada a minusculas.
end;

procedure ReplaceTogglecase
  (var ADest: umlcansishortstring);
var I, Last: Byte; C: ansichar;
begin
  umlcansishortstrings.Clear(ADest);

  Last :=
    umlcansishortstrings.Length(ADest);

  for I := 1 to Last do
  begin
    C :=
      umlcansichars.TogglecaseCopy(ADest[i]);
    umlcansishortstrings.ConcatChar
      (ADest, C);
  end;
  // Goal: Changes the given string into the opposite case.
  // Objetivo: Cambia la cadena dada al caso opuesto.
end;

procedure ReplaceCapitalize
  (var ADest: umlcansishortstring);
var I, Last: Byte; C: ansichar; MakeUppercase: Boolean;
begin
  umlcansishortstrings.Reset(ADest);

  MakeUppercase := TRUE;
  Last :=
    umlcansishortstrings.Length(ADest);
  for I := 1 to Last do
  begin
    C := ADest[i];
    if (C <> #32) then
    begin
      if (MakeUppercase) then
      begin
        C :=
          umlcansichars.UppercaseCopy(C);
        MakeUppercase := FALSE;
      end else
      begin
        C :=
          umlcansichars.LowercaseCopy(C);
      end;
    end else
    begin
      MakeUppercase := TRUE;
    end;

    umlcansishortstrings.ConcatChar
      (ADest, C);
  end;
  // Goal: Changes the given string into capitalize.
  // Objetivo: Cambia la cadena dada a capitalizar.
end;

procedure LastCharCountPtr
  (out   ADestPtr: pansichar;
   out   ACount:   Byte;
   const ASource:  umlcansishortstring);
begin
  ADestPtr := nil;
  ACount   := 0;
  if (ASource <> '') then
  begin
    ACount := umlcansishortstrings.Length(ASource);
    ADestPtr :=
      @(ASource[ACount]);
  end;
end;

procedure AssignReverse
  (out ADest: umlcansishortstring; const ASource: umlcansishortstring);
var CanContinue: Boolean; P: pansichar; ACount: Byte;
begin
  umlcansishortstrings.Clear(ADest);
  CanContinue :=
    (not umlcansishortstrings.IsEmpty(ASource));
  if (CanContinue) then
  begin
    umlcansishortstrings.LastCharCountPtr
      (P, ACount, ASource);
    while ((P <> nil) and (ACount > 0)) do
    begin
      umlcansishortstrings.ConcatChar(ADest, P^);

      System.Dec(P);
      System.Dec(ACount);
    end;
  end;
end;

procedure ConcatChar
  (var   ADest:   umlcansishortstring;
   const ASource: ansichar);
begin
  ADest := ADest + ASource;
  // Objetivo: Agregar un caracter al final de la cadena dada.
  // Goal: Add a character at the end of the given string.
end;

procedure ConcatStr
  (var   ADest:   umlcansishortstring;
   const ASource: umlcansishortstring);
begin
  ADest := ADest + ASource;
  // Objetivo: Agregar una cadena al final de la cadena dada.
  // Goal: Add a string at the end of the given string.
end;


function StartsWith
  (const AHaystack: umlcansishortstring; const ANeedle: umlcansishortstring): Boolean;
begin
  Result := false;
end;

function FinishesWith
  (const AHaystack: umlcansishortstring; const ANeedle: umlcansishortstring): Boolean;
begin
  Result := false;
end;

(* global operators *)

function ConcatCharCopy
  (const A: umlcansishortstring; const B: ansichar): umlcansishortstring;
begin
  Result :=
    umlcansishortstrings.ConcatCharCopy(A, B);
  // Objetivo: Agregar un caracter al final de la cadena dada.
  // Goal: Add a character at the end of the given string.
end;

function ConcatStrCopy
  (const A: umlcansishortstring;
   const B: umlcansishortstring): umlcansishortstring; overload; // operator +
begin
  Result :=
    umlcansishortstrings.ConcatStrCopy(A, B);
  // Objetivo: Agregar una cadena al final de la cadena dada.
  // Goal: Add a string at the end of the given string.
end;

procedure AssignStr
  (var   ADest:   umlcansishortstring;
   const ASource: umlcansishortstring);
var ACount: Byte;
begin
  ACount :=
    umlcansishortstrings.Length(ASource);
  System.Move(ASource[1], ADest[1], ACount);
  ADest[0] := chr(ACount);
end;

procedure AssignChar
  (var   ADest:   umlcansishortstring;
   const ASource: ansichar);
begin
  ADest[0] := chr(1);
  ADest[1] := ASource;
end;

function SameStrAt
  (const AHaystack, ANeedle: umlcansishortstring; const AIndex: Word): Boolean;
var Match: Boolean; Last1, Last2, I, J: Byte;
begin
  Last1 := umlcansishortstrings.Length(AHaystack);
  Last2 := umlcansishortstrings.Length(ANeedle);
  I := 1;
  J := AIndex;

  Match := Last2 <= Succ(Last1 - AIndex);
//  Match := Succ(Last1 - AIndex) >= Last2;
  // verificar que las subcadena sea de menor longuitud que la cadena destino

  if (Match) then
  repeat
    Match := umlcansichars.SameText(AHaystack[i], ANeedle[j]);

    Inc(I);
    Inc(J);
  until (not Match or (I > Last2));
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // y que inicie a partir de la posicion indicada,
  // ignorando el caso sensitivo.
end;

function EqualStrAt
  (const AHaystack, ANeedle: umlcansishortstring; const AIndex: Word): Boolean;
var Match: Boolean; Last1, Last2, I, J: Byte;
begin
  Last1 := System.Length(AHaystack);
  Last2 := System.Length(ANeedle);
  I := 1;
  J := AIndex;

  Match := Last2 <= Succ(Last1 - AIndex);
//  Match := Succ(Last1 - AIndex) >= Last2;
  // verificar que las subcadena sea de menor longuitud que la cadena destino

  if (Match) then
  repeat
    Match := (AHaystack[i] = ANeedle[j]);

    Inc(I);
    Inc(J);
  until (not Match or (I > Last2));
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // y que inicie a partir de la posicion indicada,
  // tomando en cuenta el caso sensitivo.
end;

function SameStrIn
  (const AHaystack, ANeedle: umlcansishortstring; var AIndex: Word): Boolean;
begin
  AIndex := 1;
  {$ifdef Delphi}
  Result := Search(AHaystack, ANeedle, AIndex, (*@*)SameStrAt);
  {$else}
  Result := Search(AHaystack, ANeedle, AIndex, @SameStrAt);
  {$endif}
  // Objetivo: Buscar una subcadena en otra cadena,
  // sin importar la posicion,
  // ignorando el caso sensitivo.
end;

function EqualStrIn
  (const AHaystack, ANeedle: umlcansishortstring; var AIndex: Word): Boolean;
begin
  AIndex := 1;
  {$ifdef Delphi}
  Result := Search(AHaystack, ANeedle, AIndex, (*@*)EqualStrAt);
  {$else}
  Result := Search(AHaystack, ANeedle, AIndex, @EqualStrAt);
  {$endif}
  // Objetivo: Buscar una subcadena en otra cadena,
  // sin importar la posicion,
  // tomando en cuenta el caso sensitivo.
end;

function SameStr
  (const AHaystack, ANeedle: umlcansishortstring; var AIndex: Word): Boolean;
var Match: Boolean; Last1, Last2: Byte;
begin
  // @to-do: corregir "AIndex"
  AIndex := 0;

  Last1 := umlcansishortstrings.Length(AHaystack);
  Last2 := umlcansishortstrings.Length(ANeedle);

  Match := (Last1 = Last2);
  // verificar que las cadenas sean de la misma longuitud

  if (Match)
    then Match := umlcansishortstrings.SameText(AHaystack, ANeedle);
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // que sean de la misma longuitud,
  // ignorando el caso sensitivo.
end;

function EqualStr
  (const AHaystack, ANeedle: umlcansishortstring; var AIndex: Word): Boolean;
var Match: Boolean; Last1, Last2: Byte;
begin
  // Not used "AIndex"

  Last1 := System.Length(AHaystack);
  Last2 := System.Length(ANeedle);

  Match := (Last1 = Last2);
  // verificar que las cadenas sean de la misma longuitud

  if (Match)
    then Match := (AHaystack = ANeedle);
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // que sean de la misma longuitud,
  // tomando en cuenta el caso sensitivo.
end;

function Search
  (const AHaystack, ANeedle: umlcansishortstring; var AIndex: Word;
   CompareStrAt: TIsEqualANSIStrAt): Boolean;
var Len: Word; Found: Boolean;
begin
  Len := System.Length(AHaystack);
  Found  := FALSE;

  if (Assigned(CompareStrAt)) then
  while (not Found and (AIndex <= Len)) do
  begin
    if (CompareStrAt(AHaystack, ANeedle, AIndex))
      then Found := TRUE
      else Inc(AIndex);
  end;

  Result := Found;
  // Goal: Searches for a substring inside other string
  // beginning at the "AIndex" char and returns the next character.
end;

function Equal
  (const A, B: umlcansishortstring): Boolean;
begin
  Result := (A = B);
  // Goal: Returns if 2 strings are equal.
  // Objetivo: Regresa si 2 cadenas son iguales.
end;

function SameText
  (const A, B: umlcansishortstring): Boolean;
begin
  Result :=
    SysUtils.SameText(A, B);
  // Goal: Returns if 2 strings are equal, ignores sensitive case.
  // Objetivo: Regresa si 2 cadenas son iguales, ignorar caso sensitivo.
end;

function Compare
  (const A, B: umlcansishortstring): TComparison;
begin
  if (A = B)
    then Result := cmpEqual
  else if (A < B)
    then Result := cmpLower
  else Result := cmpHigher
  // Goal: Returns the comparison between 2 strings.
  // Objetivo: Regresa la comparacion de 2 cadenas.
end;

function EqualByOptions
  (const A, B: umlcansishortstring; Options: TStringOptions): Boolean;
begin
  case (Options) of
    soExactMatch:
      Result := (A = B);
    soForceMatch:
      Result :=
        (umlcansishortstrings.UppercaseCopy(A) = B);
    soForceFirst:
      Result :=
        (umlcansishortstrings.UppercaseCopy(A) = umlcansishortstrings.UppercaseCopy(B));
    soForceLast:
      Result :=
        (A = umlcansishortstrings.UppercaseCopy(B));
    else
      Result := FALSE;
  end;
  // Goal: Returns if 2 strings are equals uppon the given options.
  // Objetivo: Regresa si 2 cadenas son iguales basado en las opciones dadas.
end;

function CompareByOptions
  (const A, B: umlcansishortstring; Options: TStringOptions): TComparison;
begin
  Result := cmpEqual;

  case Options of
    soExactMatch:
      Result :=
        umlcansishortstrings.Compare(A, B);
    soForceMatch:
      Result :=
        umlcansishortstrings.Compare
          (umlcansishortstrings.UppercaseCopy(A), umlcansishortstrings.UppercaseCopy(B));
    soForceFirst:
      Result :=
        umlcansishortstrings.Compare
          (umlcansishortstrings.UppercaseCopy(A), B);
    soForceLast:
      Result :=
        umlcansishortstrings.Compare
          (A, umlcansishortstrings.UppercaseCopy(B));
    else
      Result := cmpHigher;
  end;
  // Goal: Returns if 2 strings are equals upon the given options.
  // Objetivo: Regresa si 2 cadenas son iguales basado en las opciones dadas.
end;


end.
