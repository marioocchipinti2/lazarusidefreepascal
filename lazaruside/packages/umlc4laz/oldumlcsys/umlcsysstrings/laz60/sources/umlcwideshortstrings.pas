(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwideshortstrings;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to support the predefined pascal "string" type.
 ** The character encoding is Wide.
 ** The quantity of items is restricted to 255 characters.
 **************************************************************************
 **)

interface
uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdstrtypes,
  umlcwidechars,
  umlcwidecharsets,
  umlcwidecharsetconsts,
dummy;

// ---

const

  MOD_umlcwideshortstrings : TUMLCModule =
    ($5E,$02,$0A,$E9,$45,$11,$A9,$4B,$BF,$EC,$8D,$32,$45,$62,$70,$D3);

// ---

(* global functions *)

function IsEmpty
  (const ASource: umlcwideshortstring): Boolean; overload;

function Length
  (const ASource: umlcwideshortstring): Word; overload;

(* global procedures *)

procedure Reset
  (out ADest: umlcwideshortstring); overload;

procedure AssignLength
  (out   ADest:      umlcwideshortstring;
   const ANewLength: Byte); overload;

procedure Clear
  (out ADest: umlcwideshortstring); overload;


implementation
(* global functions *)

function IsEmpty
  (const ASource: umlcwideshortstring): Boolean;
begin
  Result :=
    System.Length(ASource) = 0;
  // Goal: Returns if a string is empty.
  // Objetivo: Regresa si una cadena esta vacia.
end;

function Length
  (const ASource: umlcwideshortstring): Word;
begin
  Result :=
    System.Length(ASource);
  // Goal: Returns the length of the given string.
  // Objetivo: Regresa la longuitud de la cadena dada.
end;

(* global procedures *)

procedure Reset
  (out ADest: umlcwideshortstring);
begin
  ADest[0] := chr(0);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;

procedure AssignLength
  (out   ADest:      umlcwideshortstring;
   const ANewLength: Byte);
begin
  ADest[0] := chr(ANewLength);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;

procedure Clear
  (out ADest: umlcwideshortstring);
begin
  System.FillChar(ADest, sizeof(ADest), #0);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;


end.

