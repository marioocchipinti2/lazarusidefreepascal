unit umlcansishortstrbyptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcstdstrtypes,
  dummy;

// ---

const

 MOD_umlcshortstrbyptrs : TUMLCModule =
  ($6A,$E6,$FB,$C0,$EE,$1F,$97,$4E,$83,$EF,$40,$D5,$E4,$13,$ED,$B1);

// ---


(* global functions *)

function ConstToPtr
 (const AValue: umlcansishortstring): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcansishortstring): pointer;
var P: umlcpansishortstring;
    ACount, ARealCount, I: Byte;
begin
  System.GetMem(P, sizeof(umlcansishortstring));

  ACount :=
    System.Length(AValue);
  ARealCount :=
    Math.Min(sizeof(umlcansishortstring), ACount);

  I := 0;
  while (I <= ARealCount) do
  begin
    P^[I] := AValue[I];
    System.Inc(I);
  end;

  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcansishortstring));
end;


end.

