(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctextconsts;

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  dummy;

// ---

const

 MOD_umlctextconsts : TUMLCModule =
   ($74,$9A,$51,$6D,$DD,$71,$B7,$4F,$85,$B2,$CE,$DA,$FE,$C5,$A1,$38);

// ---

const
  DOSLineBreak  = #13#10;
  UnixLineBreak = #13;
  ClassicLineBreak = #10;
  CarbonLineBreak  = #10;

  DOSPageBreak  = #12;
  DOSEoFchar    = #26;

  WinBreak   = UnixLineBreak;
  MacBreak   = UnixLineBreak;
  LinuxBreak = UnixLineBreak;

  ansinullchar : ansichar = #0;
  widenullchar : widechar = #0;  

  ansiEoFchar  : ansichar = #26;
  wideEoFchar  : ansichar = #26;  

  ansiSingleQuote : ansichar = #39;
  wideSingleQuote : ansichar = #39;

  ansiDoubleQuote : ansichar = #34;
  wideDoubleQuote : ansichar = #34;

  ansiSpaceChar   : ansichar = #32;
  wideSpaceChar   : ansichar = #32;

  ansiSlash       : ansichar = '/';
  wideSlash       : ansichar = '/';

  ansiBackSlash   : ansichar = '\';
  wideBackSlash   : ansichar = '\';

implementation

end.

