(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansistrings;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations to support the predefined non pascal Delphi "string" type.
 ** The character encoding is A.N.S.I.
 ** The quantity of items is NOT restricted to 255 characters.
 ** The string is internally managed as memory referenced.
 **************************************************************************
 **)

interface
uses
{$IFDEF MSWINDOWS}
  Windows, 
  //Messages, 
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcansichars,
  umlcansicharsets,
  umlcansicharsetconsts,
  dummy;

// ---

const

 MOD_umlcansistrings : TUMLCModule =
   ($99,$7D,$3C,$02,$A4,$08,$E9,$48,$B8,$58,$F8,$D9,$A4,$EE,$D0,$D5);

// ---

(* global functions *)

  function IsEmpty
    (const ASource: ansistring): Boolean; overload;

  function IsStringOfChar
    (const AHaystack: ansistring; ANeedle: ansichar): Boolean; overload;

  function Length
    (const ASource: ansistring): Word; overload;

  function UppercaseCopy
    (const ASource: ansistring): ansistring; overload;
  function LowercaseCopy
    (const ASource: ansistring): ansistring; overload;
  function TogglecaseCopy
    (const ASource: ansistring): ansistring; overload;
  function CapitalizeCopy
    (const ASource: ansistring): ansistring; overload;

  function StringOfCharCopy
    (const ASource: ansichar; const ACount: Byte): ansistring;

  function LeftCopy
    (const ASource: ansistring; const ACount: Byte): ansistring;
  function RightCopy
    (const ASource: ansistring; const ACount: Byte): ansistring;

  function CAR
    (const ASource: ansistring): ansistring; overload;
  function CDR
    (const ASource: ansistring): ansistring; overload;

  function CharPosEqual
    (const S: ansistring; const SubStr: ansichar): Word;
  function CharPosEqualReverse
    (const S: ansistring; const SubStr: ansichar): Word;

  function CharPosSame
    (const S: ansistring; const SubStr: ansichar): Word;
  function CharPosSameReverse
    (const S: ansistring; const SubStr: ansichar): Word;

  function CharSetPosEqual
    (const S: ansistring; const Items: ansicharset): Word;
  function CharSetPosReverseEqual
    (const S: ansistring; const Items: ansicharset): Word;

  function CharSetPosSame
    (const S: ansistring; const Items: ansicharset): Word;
  function CharSetPosReverseSame
    (const S: ansistring; const Items: ansicharset): Word;

  function PosForward
    (const SubStr, FullStr: ansistring): Word; overload;
  function PosSameForward
    (const S: ansistring; const SubStr: ansistring): Word; overload;

  function PosBackward
    (const SubStr, FullStr: ansistring): Word; overload;
  function PosSameBackward
    (const SubStr, FullStr: ansistring): Word; overload;

  function StartsEqualWith
    (const S: ansistring; const SubStr: ansistring): Boolean;

  function StartsSameWith
    (const S: ansistring; const SubStr: ansistring): Boolean;

  function FinishesEqualWith
    (const S: ansistring; const SubStr: ansistring): Boolean;

  function FinishesSameWith
    (const S: ansistring; const SubStr: ansistring): Boolean;

  function TryStrToChar
    (const ASource: ansistring; var ADest: ansichar): Boolean;
  function StrToCharDef
    (const ASource: ansistring; const ADefSource: ansichar): ansichar;
  function StrToChar
    (const ASource: ansistring): ansichar;

  function CppEscToStr
    (const ASource: ansistring): ansistring;
  function StrToCppEsc
    (const ASource: ansistring): ansistring;

(* global procedures *)

procedure Reset
  (out ADest: ansistring); overload;

procedure AssignLength
  (out   ADest:      ansistring;
   const ANewLength: Word); overload;

procedure Clear
  (out ADest: ansistring); overload;

  function TryAssignCopyAt
    (out   ADest:        ansistring;
     const ASource:      ansistring;
     const ASourceIndex: Word): Boolean;
  function TryAssignCopyAtCount
    (out   ADest:        ansistring;
     const ASource:      ansistring;
     const ASourceIndex: Word;
     const ASourceCount: Word): Boolean;
  function TryAssignCopyCount
    (out   ADest:        ansistring;
     const ASource:      ansistring;
     const ASourceCount: Word): Boolean;

  procedure AssignCopyAt
    (out   ADest:        ansistring;
     const ASource:      ansistring;
     const ASourceIndex: word);
  procedure AssignCopyAtCount
    (out   ADest:        ansistring;
     const ASource:      ansistring;
     const ASourceIndex: word;
     const ASourceCount: word);
  procedure AssignCopyCount
    (out   ADest:        ansistring;
     const ASource:      ansistring;
     const ASourceCount: Word);

  procedure AssignLeft
    (var   ADest:   ansistring;
     const ASource: ansistring;
     const ACount:  Word);
  procedure AssignRight
    (var   ADest:   ansistring;
     const ASource: ansistring;
     const ACount:  Word);

  procedure ReplaceLeft
    (out   ADest:   ansistring;
     const ASource: ansistring;
     const ACount:  Word);
  procedure ReplaceRight
    (out   ADest:   ansistring;
     const ASource: ansistring;
     const ACount:  Word);

  procedure ReplaceUppercase
    (var ADest: ansistring); overload;
  procedure ReplaceLowercase
    (var ADest: ansistring); overload;
  procedure ReplaceTogglecase
    (var ADest: ansistring); overload;
  procedure ReplaceCapitalize
    (var ADest: ansistring); overload;

procedure ConcatChar
  (var ADest: ansistring; const ASource: ansichar); overload;
procedure ConcatStr
  (var ADest: ansistring; const ASource: ansistring); overload;

  function ReplaceForwardCopy
    (const AHaystack, Source, Dest: ansistring): ansistring;
  function ReplaceForwardCopySame
    (const AHaystack, Source, Dest: ansistring): ansistring;

  function ReplaceBackwardCopy
    (const AHaystack, Source, Dest: ansistring): ansistring;
  function ReplaceBackwardCopySame
    (const AHaystack, Source, Dest: ansistring): ansistring;

  function ReplaceForwardALLCopy
    (const AHaystack, ASourceNeedle, ADestNeedle: ansistring): ansistring;
  function ReplaceBackwardALLCopy
    (const AHaystack, ASourceNeedle, ADestNeedle: ansistring): ansistring;

  procedure ReplaceForwardPosCopy
    (var AHaystack: ansistring;
     var AIndex: Integer; const ASourceNeedle, ADestNeedle: ansistring);
  procedure ReplaceForwardPosCopySame
    (var AHaystack: ansistring;
     var AIndex: Integer; const ASourceNeedle, ADestNeedle: ansistring);

  procedure ReplaceBackwardPosCopy
    (var AHaystack: ansistring;
     var AIndex: Integer; const ASourceNeedle, ADestNeedle: ansistring);
  procedure ReplaceBackwardPosCopySame
    (var AHaystack: ansistring;
     var AIndex: Integer; const ASourceNeedle, ADestNeedle: ansistring);

  function DeleteForwardCopy
    (const AHaystack, ANeedle: ansistring): ansistring;
  function DeleteBackwardCopy
    (const AHaystack, ANeedle: ansistring): ansistring;

  function DeleteALLCopy
    (const AHaystack, ANeedle: ansistring): ansistring;

procedure LastCharCountPtr
  (out ADestPtr: pansichar; out ACount: Word; const ASource: ansistring);

procedure AssignReverse
  (out ADest: ansistring; const ASource: ansistring);



(* global operators *)

function ConcatCharCopy
  (const A: ansistring;
   const B: ansichar): ansistring; overload; // operator +
function ConcatStrCopy
  (const A: ansistring;
   const B: ansistring): ansistring; overload; // operator +

  procedure Assign
    (var   ADest:   ansistring;
     const ASource: ansistring); overload; // operator :=
  procedure Assign
    (var   ADest:   ansistring;
     const ASource: ansichar); overload; // operator :=

type
  TIsEqualANSIStr =
    {^}function (const A, B: ansistring): Boolean;

type
  TIsEqualANSIStrAt =
    {^}function (const AHaystack, ANeedle: ansistring; const AIndex: Word): Boolean;

  function SameStrAt
    (const SubStr, Str: ansistring; const AIndex: Word): Boolean;
  function EqualStrAt
    (const SubStr, Str: ansistring; const AIndex: Word): Boolean;

  function SameStrIn
    (const SubStr, Str: ansistring; var AIndex: Word): Boolean;
  function EqualStrIn
    (const SubStr, Str: ansistring; var AIndex: Word): Boolean;

  function SameStr
    (const SubStr, Str: ansistring; var AIndex: Word): Boolean;
  function EqualStr
    (const SubStr, Str: ansistring; var AIndex: Word): Boolean;

  function Search
    (const SubStr, Str: ansistring; var AIndex: Word;
     CompareStrAt: TIsEqualANSIStrAt): Boolean;

  function Equal
    (const A, B: ansistring): Boolean; // operator :=

  function SameText
    (const A, B: ansistring): Boolean;

  function Compare
    (const A, B: ansistring): TComparison;

  function EqualByOptions
    (const A, B: ansistring; Options: TStringOptions): Boolean;
  function CompareByOptions
    (const A, B: ansistring; Options: TStringOptions): TComparison;

implementation

(* global functions *)

function IsEmpty
  (const ASource: ansistring): Boolean;
begin
  Result :=
    System.Length(ASource) = 0;
  // Goal: Returns if a string is empty.
  // Objetivo: Regresa si una cadena esta vacia.
end;

function IsStringOfChar
  (const AHaystack: ansistring; ANeedle: ansichar): Boolean;
var I, L: Integer; Match: Boolean;
begin
  L :=
    umlcansistrings.Length(AHaystack);

  Result := (L > 0);
  if (Result) then
  begin
    I := 1; Match := TRUE;
    while ((I <= L) and (Match)) do
    begin
      Match :=
        (AHaystack[i] = ANeedle);
      Inc(I);
    end;

    Result := Match;
  end;
  // Objetivo: Regresa si una cadena esta compuesta solo del mismo caracter.
  // Goal: Returns if a string is composed with the same character.
end;

function Length
  (const ASource: ansistring): Word;
begin
  Result :=
    System.Length(ASource);
  // Goal: Returns the length of the given string.
  // Objetivo: Regresa la longuitud de la cadena dada.
end;

function UppercaseCopy
  (const ASource: ansistring): ansistring;
var I, Last: Integer; C: ansichar;
begin
  umlcansistrings.Clear(Result);

  Last :=
    umlcansistrings.Length(ASource);

  for I := 1 to Last do
  begin
    C :=
      umlcansichars.UppercaseCopy(ASource[i]);
    umlcansistrings.ConcatChar(Result, C);
  end;
  // Goal: Returns a uppercase copy of the given string.
  // Objetivo: Regresa una copia en mayusculas de la cadena dada.
end;

function LowercaseCopy
  (const ASource: ansistring): ansistring;
var I, Last: Integer; C: ansichar;
begin
  umlcansistrings.Clear(Result);

  Last :=
    umlcansistrings.Length(ASource);

  for I := 1 to Last do
  begin
    C :=
      umlcansichars.LowercaseCopy(ASource[i]);
    umlcansistrings.ConcatChar(Result, C);
  end;
  // Goal: Returns a lowercase copy of the given string.
  // Objetivo: Regresa una copia en minusculas de la cadena dada.
end;

function TogglecaseCopy
  (const ASource: ansistring): ansistring;
var I, Last: Integer; C: ansichar;
begin
  umlcansistrings.Clear(Result);

  Last :=
    umlcansistrings.Length(ASource);

  for I := 1 to Last do
  begin
    C := ASource[i];
    if (Windows.IsCharLowerA(C))
      then CharUpperBuff(@C, 1)
      else CharLowerBuff(@C, 1);
    umlcansistrings.ConcatChar(Result, C);
  end;
  // Goal: Swaps the sensitive case of each character in the given string.
  // Objetivo: Cambia el caso sensitivo de cada caracter en la cadena dada.
end;

function CapitalizeCopy
  (const ASource: ansistring): ansistring;
var I, Last: Integer; C: ansichar; MakeUppercase: Boolean;
begin
  Result := '';
  MakeUppercase := TRUE;
  // cambiar a la primera letra en mayusculas
  // change first letter into uppercase

  Last := System.Length(ASource);
  for I := 1 to Last do
  begin
    C := ASource[i];
    if (C <> #32) then
    begin
      if (MakeUppercase) then
      begin
        CharUpperBuff(@C, 1);
        MakeUppercase := FALSE;
      end else CharLowerBuff(@C, 1);
    end else MakeUppercase := TRUE;
    Result := Result + C;
  end;
  // Goal: Returns a copy with uppercase initials of the given string.
  // Objetivo: Regresa una copia con iniciales en mayusculas de la cadena dada.
end;

function StringOfCharCopy
  (const ASource: ansichar; const ACount: Byte): ansistring;
begin
  umlcansistrings.Reset(Result);

  umlcansistrings.AssignLength(Result, ACount);

  System.FillChar(Result[1], ACount, ASource);
  // Goal: Returns a string of the same character.
  // Objetivo: Regresa una cadena del mismo caracter.
end;

function LeftCopy
  (const ASource: ansistring; const ACount: Byte): ansistring;
begin
  umlcansistrings.ReplaceLeft
    (Result, ASource, ACount);
  // Goal: Returns the leftmost characters of "ASource".
  // Objetivo: Regresa los caracteres mas a la izquierda de "ASource".
end;

function RightCopy
  (const ASource: ansistring; const ACount: Byte): ansistring;
begin
  umlcansistrings.ReplaceRight
    (Result, ASource, ACount);
  // Goal: Returns the rightmost characters of "ASource".
  // Objetivo: Regresa los caracteres mas a la derecha de "ASource".
end;

function CAR
  (const ASource: ansistring): ansistring;
begin
  Result := ASource[1];
  // Objetivo: Regresa el primer caracter de una cadena (como en LISP).
  // Goal: Returns the first character of a string (as in LISP).
end;

function CDR
  (const ASource: ansistring): ansistring;
begin
  Result := System.Copy(ASource, 2, Pred(System.Length(ASource)));
  // Objetivo: Regresa el resto de una cadena (como en LISP).
  // Goal: Returns the rest of the string (as in LISP).
end;

function CharPosEqual
  (const S: ansistring; const SubStr: ansichar): Word;
var Len: Word; Found: Boolean;
begin
  Len :=
    Pred(umlcansistrings.Length(S));
  Result := 0; Found := FALSE;

  while ((not Found) and (Result < Len)) do
  begin
    Found :=
      umlcansichars.Equal(S[Result], SubStr);
    Inc(Result);
  end;

  if (Found)
    then Dec(Result)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena.

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function CharPosEqualReverse
  (const S: ansistring; const SubStr: ansichar): Word;
var Found: Boolean;
begin
  Result :=
    umlcansistrings.Length(S);
  Found  := FALSE;

  while ((not Found) and (Result > 0)) do
  begin
    Found :=
      umlcansichars.Equal(S[Result], SubStr);
    Dec(Result);
  end;

  if (Found)
    then Inc(Result)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el final de la cadena.

  // Goal: Returns the first location of the given character,
  // from the finish of the string.
end;

function CharPosSame
  (const S: ansistring; const SubStr: ansichar): Word;
var Len: Word; Found: Boolean;
begin
  Len :=
    Pred(umlcansistrings.Length(S));
  Result := 0; Found := FALSE;

  while ((not Found) and (Result < Len)) do
  begin
    Found :=
      umlcansichars.SameText(S[Result], SubStr);
    Inc(Result);
  end;

  if (Found)
    then Dec(Result)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena.

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function CharPosSameReverse
  (const S: ansistring; const SubStr: ansichar): Word;
var Found: Boolean;
begin
  Result :=
    umlcansistrings.Length(S);
  Found  := FALSE;

  while ((not Found) and (Result > 0)) do
  begin
    Found :=
      umlcansichars.SameText(S[Result], SubStr);
    Dec(Result);
  end;

  if (Found)
    then Inc(Result)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el final de la cadena.

  // Goal: Returns the first location of the given character,
  // from the finish of the string.
end;

function CharSetPosEqual
  (const S: ansistring; const Items: ansicharset): Word;
var AIndex, Len: Word; Found: Boolean;
begin
  Found := FALSE;
  Len   := System.Length(S);
  AIndex := 1;

  while ((AIndex <= Len) and (not Found)) do
  begin
    Found :=
      umlcansicharsets.IsMember(Items, S[AIndex]);
    Inc(AIndex);
  end;

  if (Found)
    then Result := Pred(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena}

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function CharSetPosReverseEqual
  (const S: ansistring; const Items: ansicharset): Word;
var AIndex: Word; Found: Boolean;
begin
  Found := FALSE;
  AIndex := System.Length(S);

  while ((AIndex > 0) and (not Found)) do
  begin
    Found :=
      umlcansicharsets.IsMember(Items, S[AIndex]);
    Dec(AIndex);
  end;

  if (Found)
    then Result := Succ(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena}

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;
function CharSetPosSame
  (const S: ansistring; const Items: ansicharset): Word;
var AIndex, Len: Word; Found: Boolean;
begin
  Found := FALSE;
  Len   := System.Length(S);
  AIndex := 1;

  while ((AIndex <= Len) and (not Found)) do
  begin
    Found :=
      umlcansicharsets.IsSameMember(Items, S[AIndex]);
    Inc(AIndex);
  end;

  if (Found)
    then Result := Pred(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena}

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function CharSetPosReverseSame
  (const S: ansistring; const Items: ansicharset): Word;
var AIndex: Word; Found: Boolean;
begin
  Found := FALSE;
  AIndex := System.Length(S);

  while ((AIndex > 0) and (not Found)) do
  begin
    Found :=
      umlcansicharsets.IsSameMember(Items, S[AIndex]);
    Dec(AIndex);
  end;

  if (Found)
    then Result := Succ(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena}

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function PosForward
  (const SubStr, FullStr: ansistring): Word;
var Len, AIndex: Integer; Found: Boolean;
begin
  Len := System.Length(FullStr);
  // obtain length of full string
  // obtener longuitud de cadena completa

  AIndex := 1;
  // "AIndex" indicates from which character in full string starts analysis
  // "AIndex" indica de cual caracter eb la cadena completa comienza el analisis

  Found := FALSE;
  while ((not Found) and (AIndex < Len)) do
  begin
    Found :=
      umlcansistrings.StartsEqualWith
        (System.Copy(FullStr, AIndex, Len), SubStr);

    Inc(AIndex);
  end;

  if (Found)
    then Result := Pred(AIndex)
    else Result := 0;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function PosSameForward
  (const S: ansistring; const SubStr: ansistring): Word;
var Len, AIndex: Integer; Found: Boolean;
begin
  Len := System.Length(S);
  // obtain length of full string
  // obtener longuitud de cadena completa

  AIndex := 1;
  // "AIndex" indicates from which character in full string starts analysis
  // "AIndex" indica de cual caracter en la cadena completa comienza el analisis

  Found := FALSE;
  while ((not Found) and (AIndex < Len)) do
  begin
    Found :=
      umlcansistrings.StartsSameWith
        (System.Copy(S, AIndex, Len), SubStr);

    Inc(AIndex);
  end;

  if (Found)
    then Result := Pred(AIndex)
    else Result := 0;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function PosBackward
  (const SubStr, FullStr: ansistring): Word;
var Len, AIndex: Integer; Found: Boolean;
begin
  Len := System.Length(FullStr);
  // obtain length of full string
  // obtener longuitud de cadena completa

  AIndex := Len;
  // "AIndex" indicates from which character in full string starts analysis
  // "AIndex" indica de cual caracter eb la cadena completa comienza el analisis

  Found := FALSE;
  while ((not Found) and (AIndex > 0)) do
  begin
    Found :=
      umlcansistrings.StartsEqualWith
        (System.Copy(FullStr, AIndex, Len), SubStr);

    Dec(AIndex);
  end;

  if (Found)
    then Result := Succ(AIndex)
    else Result := 0;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function PosSameBackward
  (const SubStr, FullStr: ansistring): Word;
var Len, AIndex: Integer; Found: Boolean;
begin
  Len := System.Length(FullStr);
  // obtain length of full string
  // obtener longuitud de cadena completa

  AIndex := Len;
  // "AIndex" indicates from which character in full string starts analysis
  // "AIndex" indica de cual caracter eb la cadena completa comienza el analisis

  Found := FALSE;
  while ((not Found) and (AIndex > 0)) do
  begin
    Found :=
      umlcansistrings.StartsSameWith
        (System.Copy(FullStr, AIndex, Len), SubStr);

    Dec(AIndex);
  end;

  if (Found)
    then Result := Succ(AIndex)
    else Result := 0;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function StartsEqualWith
  (const S: ansistring; const SubStr: ansistring): Boolean;
var Shorter, Len: Integer; LeadStr: ansistring;
begin
  Shorter :=
    System.Length(SubStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    System.Length(S);
  // obtain length of full string
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full string
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    LeadStr := System.Copy(S, 1, Shorter);
    Result  := (SubStr = LeadStr);
  end;
  // Objetivo: Regresa si una subcadena es igual o esta al inicio de }
  { otra cadena.}

  // Goal: Returns if a substring is equal o is the start of another string.
end;

function StartsSameWith
  (const S: ansistring; const SubStr: ansistring): Boolean;
var Shorter, Len: Integer; LeadStr: ansistring;
begin
  Shorter := System.Length(SubStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len := System.Length(S);
  // obtain length of full string
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full string
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    LeadStr := System.Copy(S, 1, Shorter);
    Result  := ANSISameText(SubStr, LeadStr);
  end;
  // Objetivo: Regresa si una subcadena es igual o esta al inicio de }
  { otra cadena.}

  // Goal: Returns if a substring is equal o is the start of another string.
end;

function FinishesEqualWith
  (const S: ansistring; const SubStr: ansistring): Boolean;
begin
  // @to-do:
  Result := false;
end;

function FinishesSameWith
  (const S: ansistring; const SubStr: ansistring): Boolean;
begin
  // @to-do:
  Result := false;
end;

function TryStrToChar
  (const ASource: ansistring; var ADest: ansichar): Boolean;
begin
  Result := (Length(ASource) > 0);
  if (Result)
    then ADest := ASource[1];
  // Goal: To cast a string in to a character.
  // Objetivo: Convertir una cadena en un caracter.
end;

function StrToCharDef
  (const ASource: ansistring; const ADefSource: ansichar): ansichar;
begin
  if (not TryStrToChar(ASource, Result))
    then Result := ADefSource;
  // Goal: To cast a string in to a character.
  // Objetivo: Convertir una cadena en un caracter.
end;

function StrToChar(const ASource: ansistring): ansichar;
begin
  if (not TryStrToChar(ASource, Result))
    then EInvalidCast.Create(SInvalidCast);
  // Goal: To cast a string in to a character.
  // Objetivo: Convertir una cadena en un caracter.
end;

function CppEscToStr
  (const ASource: ansistring): ansistring;
begin
  Result := ReplaceForwardALLCopy(ASource, '\n', #13#10);
  // replace escape characters by line break characters
  // reemplazar caracteres de escape por caracteres de salto de linea

  Result := ReplaceForwardALLCopy(Result, '\'#39, #39);
  // replace escape characters by single quote characters
  // reemplazar caracteres de escape por caracteres de comilla simple

  Result := ReplaceForwardALLCopy(Result, '\'#34, #34);
  // replace escape characters by double quote characters
  // reemplazar caracteres de escape por caracteres de comilla doble

  Result := ReplaceForwardALLCopy(Result, '\\', '\');
  // replace escape characters by slash characters
  // reemplazar caracteres de escape por caracteres de diagonal

  // Objetivo: Convertir una cadena con caracteres de escape que comienzan con
  // el caracter "\" y otro caracter a una cadena sin caracteres de escape.

  // Goal: To convert a string with escape characters & another character into
  // a string without escape characters.
end;

function StrToCppEsc
  (const ASource: ansistring): ansistring;
begin
  Result := ReplaceForwardALLCopy(ASource, #13#10, '\n');
  // replace line break characters by escape characters
  // reemplazar caracteres de salto de linea por caracteres de escape

  Result := ReplaceForwardALLCopy(Result, #39, '$$');
  Result := ReplaceForwardALLCopy(Result, '$$', '\'#39);
  // replace single quote characters by escape characters
  // reemplazar caracteres de comilla simple por caracteres de escape

  Result := ReplaceForwardALLCopy(Result, #34, '$$');
  Result := ReplaceForwardALLCopy(Result, '$$', '\'#34);
  // replace double quote characters by escape characters
  // reemplazar caracteres de comilla doble por caracteres de escape

  Result := ReplaceForwardALLCopy(Result, '\', '$$');
  Result := ReplaceForwardALLCopy(Result, '$$', '\\');
  // replace double quote characters by escape characters
  // reemplazar caracteres de comilla doble por caracteres de escape

  // Objetivo: Convertir una cadena sin caracteres de escape a cadena con
  // caracteres de escape que comienzan con el caracter "\" y otro caracter.

  // Goal: To convert a string without escape characters into
  // a string with escape characters & another character.
end;

(* global procedures *)

procedure Reset
  (out ADest: ansistring);
begin
  ADest := '';
  System.setLength(ADest, 0);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;

procedure AssignLength
  (out   ADest:      ansistring;
   const ANewLength: Word);
begin
  ADest := '';
  System.setLength(ADest, ANewLength);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;

procedure Clear
  (out ADest: ansistring);
begin
  ADest := '';
  //System.FillChar(ADest, sizeof(ADest), #0);
  // Goal: Clear a string.
  // Objetivo: Limpia una cadena.
end;
function TryAssignCopyAt
  (out   ADest:        ansistring;
   const ASource:      ansistring;
   const ASourceIndex: Word): Boolean;
var ASourceLen: Word;
begin
  ASourceLen :=
    umlcansistrings.Length(ASource);
  Result :=
   ((ASourceIndex > 0) and (ASourceLen > 0));
  if (Result) then
  begin
    umlcansistrings.Reset(ADest);

    System.Move(ASource[ASourceIndex], ADest[1], ASourceLen);

    umlcansistrings.AssignLength(ADest, ASourceLen);
  end;
  // Goal: To copy a substring at the given index.
end;

function TryAssignCopyAtCount
  (out   ADest:        ansistring;
   const ASource:      ansistring;
   const ASourceIndex: Word;
   const ASourceCount: Word): Boolean;
var ASourceLen, ARealCount: Word;
begin
  ASourceLen :=
    umlcansistrings.Length(ASource);
  Result :=
   ((ASourceIndex > 0) and(ASourceCount > 0) and (ASourceLen > 0));
  if (Result) then
  begin
    umlcansistrings.Reset(ADest);

    ARealCount :=
      Math.Min((ASourceLen - ASourceIndex), ASourceLen);

    System.Move(ASource[ASourceIndex], ADest[1], ARealCount);
    umlcansistrings.AssignLength(ADest, ARealCount);
  end;
end;

function TryAssignCopyCount
  (out   ADest:        ansistring;
   const ASource:      ansistring;
   const ASourceCount: Word): Boolean;
var ASourceLen, ARealCount: Word;
begin
  ASourceLen :=
    umlcansistrings.Length(ASource);
  Result :=
   ((ASourceCount > 0) and (ASourceLen > 0));
  if (Result) then
  begin
    umlcansistrings.Reset(ADest);

    ARealCount :=
      Math.Min(ASourceLen, ASourceCount);

    System.Move(ASource[1], ADest[1], ARealCount);
    umlcansistrings.AssignLength(ADest, ARealCount);
  end;
  // Objetivo: Copiar el contenido de una subcadena a un nueva cadena.
  // Goal: Copy the contents of a substring into a new string.
end;

procedure AssignCopyAt
  (out   ADest:        ansistring;
   const ASource:      ansistring;
   const ASourceIndex: Word);
begin
  if (not TryAssignCopyAt(ADest, ASource, ASourceIndex)) then
  begin
    // raise error
  end;
end;

procedure AssignCopyAtCount
  (out   ADest:        ansistring;
   const ASource:      ansistring;
   const ASourceIndex: Word;
   const ASourceCount: Word);
begin
  if (not TryAssignCopyAtCount(ADest, ASource, ASourceIndex, ASourceCount)) then
  begin
    // raise error
  end;
end;

procedure AssignCopyCount
  (out   ADest:        ansistring;
   const ASource:      ansistring;
   const ASourceCount: Word);
begin
  if (not TryAssignCopyCount(ADest, ASource, ASourceCount)) then
  begin
    // raise error
  end;
end;

procedure AssignLeft
  (var   ADest:   ansistring;
   const ASource: ansistring;
   const ACount:  Word);
begin
  umlcansistrings.AssignCopyCount
    (ADest, ASource, ACount);
  // Goal: Returns the leftmost characters of "ADest".
  // Objetivo: Regresa los caracteres mas a la izquierda de "ADest".
end;

procedure AssignRight
  (var   ADest:   ansistring;
   const ASource: ansistring;
   const ACount:  Word);
var AIndex: Word;
begin
  AIndex :=
    Succ(umlcansistrings.Length(ASource) - ACount);
  umlcansistrings.AssignCopyAtCount
    (ADest, ASource, AIndex, ACount);
  // Goal: Returns the rightmost characters of "ADest".
  // Objetivo: Regresa los caracteres mas a la derecha de "ADest".
end;

procedure ReplaceLeft
  (out   ADest:   ansistring;
   const ASource: ansistring;
   const ACount:  Word);
begin
  umlcansistrings.Reset(ADest);

  System.Move(ASource[1], ADest[1], ACount);

  umlcansistrings.AssignLength(ADest, ACount);
  // Goal: Returns the leftmost characters of "ASource".
  // Objetivo: Regresa los caracteres mas a la izquierda de "ASource".
end;

procedure ReplaceRight
  (out   ADest:   ansistring;
   const ASource: ansistring;
   const ACount:  Word);
var AIndex: Integer;
begin
  umlcansistrings.Reset(ADest);

  AIndex :=
    Succ(umlcansistrings.Length(ASource)-ACount);

  System.Move(ASource[AIndex], ADest[1], ACount);

  umlcansistrings.AssignLength(ADest, ACount);
  // Goal: Returns the rightmost characters of "ASource".
  // Objetivo: Regresa los caracteres mas a la derecha de "ASource".
end;

procedure ReplaceUppercase
  (var ADest: ansistring);
var I, Last: Word; C: ansichar;
begin
  umlcansistrings.Clear(ADest);

  Last :=
    umlcansistrings.Length(ADest);

  for I := 1 to Last do
  begin
    C :=
      umlcansichars.UppercaseCopy(ADest[i]);
    umlcansistrings.ConcatChar
      (ADest, C);
  end;
  // Goal: Changes the given string into uppercase.
  // Objetivo: Cambia la cadena dada a mayusculas.
end;

procedure ReplaceLowercase
  (var ADest: ansistring);
var I, Last: Word; C: ansichar;
begin
  umlcansistrings.Clear(ADest);

  Last :=
    umlcansistrings.Length(ADest);

  for I := 1 to Last do
  begin
    C :=
      umlcansichars.LowercaseCopy(ADest[i]);
    umlcansistrings.ConcatChar
      (ADest, C);
  end;
  // Goal: Changes the given string into lowercase.
  // Objetivo: Cambia la cadena dada a minusculas.
end;

procedure ReplaceTogglecase
  (var ADest: ansistring);
var I, Last: Word; C: ansichar;
begin
  umlcansistrings.Clear(ADest);

  Last :=
    umlcansistrings.Length(ADest);

  for I := 1 to Last do
  begin
    C :=
      umlcansichars.TogglecaseCopy(ADest[i]);
    umlcansistrings.ConcatChar
      (ADest, C);
  end;
  // Goal: Changes the given string into the opposite case.
  // Objetivo: Cambia la cadena dada al caso opuesto.
end;

procedure ReplaceCapitalize
  (var ADest: ansistring);
var I, Last: Word; C: ansichar; MakeUppercase: Boolean;
begin
  umlcansistrings.Reset(ADest);

  MakeUppercase := TRUE;
  Last :=
    umlcansistrings.Length(ADest);
  for I := 1 to Last do
  begin
    C := ADest[i];
    if (C <> #32) then
    begin
      if (MakeUppercase) then
      begin
        C :=
          umlcansichars.UppercaseCopy(C);
        MakeUppercase := FALSE;
      end else
      begin
        C :=
          umlcansichars.LowercaseCopy(C);
      end;
    end else
    begin
      MakeUppercase := TRUE;
    end;

    umlcansistrings.ConcatChar
      (ADest, C);
  end;
  // Goal: Changes the given string into capitalize.
  // Objetivo: Cambia la cadena dada a capitalizar.
end;

procedure ConcatChar
  (var ADest: ansistring; const ASource: ansichar);
begin
  ADest := ADest + ASource;
  // Objetivo: Agregar un caracter al final de la cadena dada.
  // Goal: Add a character at the end of the given string.
end;

procedure ConcatStr
  (var ADest: ansistring; const ASource: ansistring);
begin
  ADest := ADest + ASource;
  // Objetivo: Agregar una cadena al final de la cadena dada.
  // Goal: Add a string at the end of the given string.
end;

function ReplaceForwardCopy
  (const AHaystack, Source, Dest: ansistring): ansistring;
var AIndex: Integer;
begin
  Result := AHaystack;
  AIndex := 0;
  ReplaceForwardPosCopy
    (Result, AIndex, Source, Dest);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceForwardCopySame
  (const AHaystack, Source, Dest: ansistring): ansistring;
var AIndex: Integer;
begin
  Result := AHaystack;
  AIndex := 0;
  ReplaceForwardPosCopySame
    (Result, AIndex, Source, Dest);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceBackwardCopy
  (const AHaystack, Source, Dest: ansistring): ansistring;
var AIndex: Integer;
begin
  Result := AHaystack;
  AIndex := 0;
  ReplaceBackwardPosCopy
    (Result, AIndex, Source, Dest);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceBackwardCopySame
  (const AHaystack, Source, Dest: ansistring): ansistring;
var AIndex: Integer;
begin
  Result := AHaystack;
  AIndex := 0;
  ReplaceBackwardPosCopySame
    (Result, AIndex, Source, Dest);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceForwardALLCopy
  (const AHaystack, ASourceNeedle, ADestNeedle: ansistring): ansistring;
var AIndex: Integer;
begin
  Result := AHaystack;
  AIndex := 0;
  repeat
    ReplaceForwardPosCopy
      (Result, AIndex, ASourceNeedle, ADestNeedle);
  until (AIndex = 0);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceBackwardALLCopy
  (const AHaystack, ASourceNeedle, ADestNeedle: ansistring): ansistring;
var AIndex: Integer;
begin
  Result := AHaystack;
  AIndex := 0;
  repeat
    ReplaceBackwardPosCopy(Result, AIndex, ASourceNeedle, ADestNeedle);
  until (AIndex = 0);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

procedure ReplaceForwardPosCopy
  (var AHaystack: ansistring;
   var AIndex: Integer; const ASourceNeedle, ADestNeedle: ansistring);
var BeforeACount, AfterACount, S, V: Integer;
    BeforeString, AfterString: ansistring;
begin
  AIndex :=
    PosForward(ASourceNeedle, AHaystack);
  // localizar en que posicion inicia la cadena que se va a reemplazar
  // locate, which location starts the string to be replaced

  if (AIndex > 0) then
  begin
    S := umlcansistrings.Length(ASourceNeedle);
    V := umlcansistrings.Length(AHaystack);

    AfterACount := (V - Pred(AIndex + S));
    // obtener la long. de la cadena despues de la parte que se va a reemplazar
    // obtain the length of the string after the section to be replaced

    BeforeACount := Pred(AIndex);
    // obtener la long. de la cadena antes de la parte que se va a reemplazar
    // obtain the length of the string before the section to be replaced

    if (AIndex <> 0) then
    begin
      umlcansistrings.ReplaceLeft(BeforeString, AHaystack, BeforeACount);
      umlcansistrings.ReplaceRight(AfterString, AHaystack, AfterACount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted string

      umlcansistrings.Reset(AHaystack);
      umlcansistrings.ConcatStr(AHaystack, BeforeString);
      umlcansistrings.ConcatStr(AHaystack, ADestNeedle);
      umlcansistrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces in "AHaystack", the first "ASourceNeedle" found with "ADestNeedle".
  // Objetivo: Reemplaza en "AHaystack", la primera "ASourceNeedle" encontrada por "ADestNeedle".}
end;

procedure ReplaceForwardPosCopySame
  (var AHaystack: ansistring;
   var AIndex: Integer; const ASourceNeedle, ADestNeedle: ansistring);
var BeforeACount, AfterACount, S, V: Integer;
    BeforeString, AfterString: ansistring;
begin
  BeforeString := '';
  AfterString  := '';
  AIndex := PosSameForward(ASourceNeedle, AHaystack);
  // localizar en que posicion inicia la cadena que se va a reemplazar
  // locate, which location starts the string to be replaced

  if (AIndex > 0) then
  begin
    S := umlcansistrings.Length(ASourceNeedle);
    V := umlcansistrings.Length(AHaystack);

    AfterACount := (V - Pred(AIndex + S));
    // obtener la long. de la cadena despues de la parte que se va a reemplazar
    // obtain the length of the string after the section to be replaced

    BeforeACount := Pred(AIndex);
    // obtener la long. de la cadena antes de la parte que se va a reemplazar
    // obtain the length of the string before the section to be replaced

    if (AIndex <> 0) then
    begin
      umlcansistrings.ReplaceLeft(BeforeString, AHaystack, BeforeACount);
      umlcansistrings.ReplaceRight(AfterString, AHaystack, AfterACount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted ansistring

      umlcansistrings.Reset(AHaystack);
      umlcansistrings.ConcatStr(AHaystack, BeforeString);
      umlcansistrings.ConcatStr(AHaystack, ADestNeedle);
      umlcansistrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces in "AHaystack", the first "ASourceNeedle" found with "ADestNeedle".
  // Objetivo: Reemplaza en "AHaystack", la primera "ASourceNeedle" encontrada por "ADestNeedle".}
end;

procedure ReplaceBackwardPosCopy
  (var AHaystack: ansistring;
   var AIndex: Integer; const ASourceNeedle, ADestNeedle: ansistring);
var BeforeACount, AfterACount, S, V: Integer;
    BeforeString, AfterString: ansistring;
begin
  AIndex := PosBackward(ASourceNeedle, AHaystack);
  // localizar en que posicion inicia la cadena que se va a reemplazar
  // locate, which location starts the string to be replaced

  if (AIndex > 0) then
  begin
    S := umlcansistrings.Length(ASourceNeedle);
    V := umlcansistrings.Length(AHaystack);

    AfterACount := (V - Pred(AIndex + S));
    // obtener la long. de la cadena despues de la parte que se va a reemplazar
    // obtain the length of the string after the section to be replaced

    BeforeACount := Pred(AIndex);
    // obtener la long. de la cadena antes de la parte que se va a reemplazar
    // obtain the length of the string before the section to be replaced

    if (AIndex <> 0) then
    begin
      umlcansistrings.ReplaceLeft(BeforeString, AHaystack, BeforeACount);
      umlcansistrings.ReplaceRight(AfterString, AHaystack, AfterACount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted ansistring

      umlcansistrings.Reset(AHaystack);
      umlcansistrings.ConcatStr(AHaystack, BeforeString);
      umlcansistrings.ConcatStr(AHaystack, ADestNeedle);
      umlcansistrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces in "AHaystack", the first "ASourceNeedle" found with "ADestNeedle".
  // Objetivo: Reemplaza en "AHaystack" , la primera "ASourceNeedle" encontrada por "ADestNeedle".}
end;

procedure ReplaceBackwardPosCopySame
  (var AHaystack: ansistring;
   var AIndex: Integer; const ASourceNeedle, ADestNeedle: ansistring);
var BeforeACount, AfterACount, S, V: Integer;
    BeforeString, AfterString: ansistring;
begin
  BeforeString := '';
  AfterString  := '';
  AIndex := PosSameBackward(ASourceNeedle, AHaystack);
  // localizar en que posicion inicia la cadena que se va a reemplazar
  // locate, which location starts the string to be replaced

  if (AIndex > 0) then
  begin
    S := umlcansistrings.Length(ADestNeedle);
    V := umlcansistrings.Length(AHaystack);

    AfterACount := (V - Pred(AIndex + S));
    // obtener la long. de la cadena despues de la parte que se va a reemplazar
    // obtain the length of the string after the section to be replaced

    BeforeACount := Pred(AIndex);
    // obtener la long. de la cadena antes de la parte que se va a reemplazar
    // obtain the length of the string before the section to be replaced

    if (AIndex <> 0) then
    begin
      umlcansistrings.ReplaceLeft(BeforeString, AHaystack, BeforeACount);
      umlcansistrings.ReplaceRight(AfterString, AHaystack, AfterACount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted ansistring

      umlcansistrings.Reset(AHaystack);
      umlcansistrings.ConcatStr(AHaystack, BeforeString);
      umlcansistrings.ConcatStr(AHaystack, ADestNeedle);
      umlcansistrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces in "AHaystack", the first "ASourceNeedle" found with "ADestNeedle".
  // Objetivo: Reemplaza en "AHaystack", la primera "ASourceNeedle" encontrada por "ADestNeedle".}
end;

function DeleteForwardCopy
  (const AHaystack, ANeedle: ansistring): ansistring;
begin
  Result :=
    ReplaceForwardCopy(AHaystack, ANeedle, '');
  // Goal: Returns a copy of the string with the given substring deleted.
  // Objetivo: Regresa una copia de la cadena con la subcadena eliminada.
end;

function DeleteBackwardCopy
  (const AHaystack, ANeedle: ansistring): ansistring;
begin
  Result :=
    ReplaceBackwardCopy(AHaystack, ANeedle, '');
  // Goal: Returns a copy of the string with the given substring deleted.
  // Objetivo: Regresa una copia de la cadena con la subcadena eliminada.
end;

function DeleteALLCopy
  (const AHaystack, ANeedle: ansistring): ansistring;
begin
  Result :=
    ReplaceForwardALLCopy(AHaystack, ANeedle, '');
  // Goal: Returns a copy of the string with the given substring deleted.
  // Objetivo: Regresa una copia de la cadena con la subcadena eliminada.
end;

procedure LastCharCountPtr
  (out   ADestPtr: pansichar;
   out   ACount:   Word;
   const ASource:  ansistring);
begin
  ADestPtr := nil;
  ACount   := 0;
  if (ASource <> '') then
  begin
    ACount := umlcansistrings.Length(ASource);
    ADestPtr :=
      @(ASource[ACount]);
  end;
end;

procedure AssignReverse
  (out ADest: ansistring; const ASource: ansistring);
var CanContinue: Boolean; P: pansichar; ACount: Word;
begin
  umlcansistrings.Clear(ADest);
  CanContinue :=
    (not umlcansistrings.IsEmpty(ASource));
  if (CanContinue) then
  begin
    umlcansistrings.LastCharCountPtr
      (P, ACount, ASource);
    while ((P <> nil) and (ACount > 0)) do
    begin
      umlcansistrings.ConcatChar(ADest, P^);

      System.Dec(P);
      System.Dec(ACount);
    end;
  end;
end;

(* global operators *)

function ConcatCharCopy
  (const A: ansistring;
   const B: ansichar): ansistring;
begin
  Result :=
    umlcansistrings.ConcatCharCopy(A, B);
  // Objetivo: Agregar un caracter al final de la cadena dada.
  // Goal: Add a character at the end of the given string.
end;

function ConcatStrCopy
  (const A: ansistring;
   const B: ansistring): ansistring; overload; // operator +
begin
  Result :=
    umlcansistrings.ConcatStrCopy(A, B);
  // Objetivo: Agregar una cadena al final de la cadena dada.
  // Goal: Add a string at the end of the given string.
end;

procedure Assign
  (var   ADest:   ansistring;
   const ASource: ansistring);
var ACount: Integer;
begin
  ACount := umlcansistrings.Length(ASource);
  System.SetLength(ADest, ACount);
  System.Move(ASource[1], ADest[1], ACount);
end;

procedure Assign
  (var   ADest:   ansistring;
   const ASource: ansichar);
begin
  System.SetLength(ADest, 1);
  ADest[1] := ASource;
end;

function SameStrAt
  (const SubStr, Str: ansistring; const AIndex: Word): Boolean;
var Match: Boolean; Last1, Last2, I, J: Integer;
begin
  Last1 := umlcansistrings.Length(Str);
  Last2 := umlcansistrings.Length(SubStr);
  I := 1;
  J := AIndex;

  Match := Last2 <= Succ(Last1 - AIndex);
//  Match := Succ(Last1 - AIndex) >= Last2;
  // verificar que las subcadena sea de menor longuitud que la cadena destino

  if (Match) then
  repeat
    Match := umlcansichars.SameText(SubStr[i], Str[j]);

    Inc(I);
    Inc(J);
  until (not Match or (I > Last2));
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // y que inicie a partir de la posicion indicada,
  // ignorando el caso sensitivo.
end;

function EqualStrAt
  (const SubStr, Str: ansistring; const AIndex: Word): Boolean;
var Match: Boolean; Last1, Last2, I, J: Integer;
begin
  Last1 := System.Length(Str);
  Last2 := System.Length(SubStr);
  I := 1;
  J := AIndex;

  Match := Last2 <= Succ(Last1 - AIndex);
//  Match := Succ(Last1 - AIndex) >= Last2;
  // verificar que las subcadena sea de menor longuitud que la cadena destino

  if (Match) then
  repeat
    Match := (SubStr[i] = Str[j]);

    Inc(I);
    Inc(J);
  until (not Match or (I > Last2));
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // y que inicie a partir de la posicion indicada,
  // tomando en cuenta el caso sensitivo.
end;

function SameStrIn
  (const SubStr, Str: ansistring; var AIndex: Word): Boolean;
begin
  AIndex := 1;
  {$ifdef Delphi}
  Result := Search(SubStr, Str, AIndex, (*@*)SameStrAt);
  {$else}
  Result := Search(SubStr, Str, AIndex, @SameStrAt);
  {$endif}
  // Objetivo: Buscar una subcadena en otra cadena,
  // sin importar la posicion,
  // ignorando el caso sensitivo.
end;

function EqualStrIn
  (const SubStr, Str: ansistring; var AIndex: Word): Boolean;
begin
  AIndex := 1;
  {$ifdef Delphi}
  Result := Search(SubStr, Str, AIndex, (*@*)EqualStrAt);
  {$else}
  Result := Search(SubStr, Str, AIndex, @EqualStrAt);
  {$endif}
  // Objetivo: Buscar una subcadena en otra cadena,
  // sin importar la posicion,
  // tomando en cuenta el caso sensitivo.
end;

function SameStr
  (const SubStr, Str: ansistring; var AIndex: Word): Boolean;
var Match: Boolean; Last1, Last2: Integer;
begin
  // @to-do: corregir "AIndex"
  AIndex := 0;

  Last1 := umlcansistrings.Length(Str);
  Last2 := umlcansistrings.Length(SubStr);

  Match := (Last1 = Last2);
  // verificar que las cadenas sean de la misma longuitud

  if (Match)
    then Match := umlcansistrings.SameText(SubStr, Str);
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // que sean de la misma longuitud,
  // ignorando el caso sensitivo.
end;

function EqualStr
  (const SubStr, Str: ansistring; var AIndex: Word): Boolean;
var Match: Boolean; Last1, Last2: Integer;
begin
  // Not used "AIndex"

  Last1 := System.Length(Str);
  Last2 := System.Length(SubStr);

  Match := (Last1 = Last2);
  // verificar que las cadenas sean de la misma longuitud

  if (Match)
    then Match := (SubStr = Str);
  Result := Match;
  // Objetivo: Buscar una subcadena en otra cadena,
  // que sean de la misma longuitud,
  // tomando en cuenta el caso sensitivo.
end;

function Search
  (const SubStr, Str: ansistring; var AIndex: Word;
   CompareStrAt: TIsEqualANSIStrAt): Boolean;
var Len: Word; Found: Boolean;
begin
  Len := System.Length(Str);
  Found  := FALSE;

  if (Assigned(CompareStrAt)) then
  while (not Found and (AIndex <= Len)) do
  begin
    if (CompareStrAt(SubStr, Str, AIndex))
      then Found := TRUE
      else Inc(AIndex);
  end;

  Result := Found;
  // Goal: Searches for a substring inside other string
  // beginning at the "AIndex" char and returns the next character.
end;

function Equal
  (const A, B: ansistring): Boolean;
begin
  Result := (A = B);
  // Goal: Returns if 2 strings are equal.
  // Objetivo: Regresa si 2 cadenas son iguales.
end;

function SameText
  (const A, B: ansistring): Boolean;
begin
  Result :=
    SysUtils.SameText(A, B);
  // Goal: Returns if 2 strings are equal, ignores sensitive case.
  // Objetivo: Regresa si 2 cadenas son iguales, ignorar caso sensitivo.
end;

function Compare
  (const A, B: ansistring): TComparison;
begin
  if (A = B)
    then Result := cmpEqual
  else if (A < B)
    then Result := cmpLower
  else Result := cmpHigher
  // Goal: Returns the comparison between 2 strings.
  // Objetivo: Regresa la comparacion de 2 cadenas.
end;

function EqualByOptions
  (const A, B: ansistring; Options: TStringOptions): Boolean;
begin
  case (Options) of
    soExactMatch:
      Result := (A = B);
    soForceMatch:
      Result :=
        (umlcansistrings.UppercaseCopy(A) = B);
    soForceFirst:
      Result :=
        (umlcansistrings.UppercaseCopy(A) = umlcansistrings.UppercaseCopy(B));
    soForceLast:
      Result :=
        (A = umlcansistrings.UppercaseCopy(B));
    else
      Result := FALSE;
  end;
  // Goal: Returns if 2 strings are equals uppon the given options.
  // Objetivo: Regresa si 2 cadenas son iguales basado en las opciones dadas.
end;

function CompareByOptions
  (const A, B: ansistring; Options: TStringOptions): TComparison;
begin
  Result := cmpEqual;

  case Options of
    soExactMatch:
      Result :=
        umlcansistrings.Compare(A, B);
    soForceMatch:
      Result :=
        umlcansistrings.Compare
          (umlcansistrings.UppercaseCopy(A), umlcansistrings.UppercaseCopy(B));
    soForceFirst:
      Result :=
        umlcansistrings.Compare
          (umlcansistrings.UppercaseCopy(A), B);
    soForceLast:
      Result :=
        umlcansistrings.Compare
          (A, umlcansistrings.UppercaseCopy(B));
    else
      Result := cmpHigher;
  end;
  // Goal: Returns if 2 strings are equals upon the given options.
  // Objetivo: Regresa si 2 cadenas son iguales basado en las opciones dadas.
end;


end.
