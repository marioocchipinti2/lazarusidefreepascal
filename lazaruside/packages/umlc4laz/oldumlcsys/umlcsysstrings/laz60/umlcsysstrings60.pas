{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcsysstrings60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcansimemos, umlcansishortstrbyptrs, umlcansishortstrings, 
  umlcansishortstrparsers, umlcansistrings, umlcansistrparsers, 
  umlcasciitoutf32, umlcshortstrbyptrs, umlcstrbyptrs, umlcstrings, 
  umlcstrings_rtti, umlcstrparsers, umlctextconsts, umlctextmarkers, 
  umlcwideshortstrings, umlcwideshortstrparsers, umlcwidestrings, 
  umlcwidestrparsers;

implementation

end.
