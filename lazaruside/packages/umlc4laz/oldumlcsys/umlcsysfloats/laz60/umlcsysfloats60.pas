{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcsysfloats60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcfloatextendedbyptrs, umlcdbcurrencies, umlcdbfloats, 
  umlcfloatcurrencies, umlcfloatcurrencybyptrs, umlcfloatdoublebyptrs, 
  umlcfloatdoubles, umlcfloatextendeds, umlcfloatrealbyptrs, umlcfloatreals, 
  umlcfloatsinglebuffers, umlcfloatsinglebyptrs, umlcfloatsingles, 
  umlcsysfloats_rtti;

implementation

end.
