(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfloatextendedbyptrs;

{$mode objfpc}{$H+}

interface
uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes,
  dummy;

// ---

const

 MOD_umlcfloat_extendedbyptrs : TUMLCModule =
   ($8A,$3D,$24,$63,$F4,$0D,$95,$43,$96,$42,$E9,$94,$62,$8E,$F0,$44);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: umlcfloat_extended): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

function IsEmpty
 (const ASource: pointer): Boolean; overload;

(* global procedures *)

procedure Clear
 (out ADest: pointer); overload;


implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcfloat_extended): pointer;
var P: umlcpfloat_extended;
begin
  System.GetMem(P, sizeof(umlcfloat_extended));
  P^ := AValue;
  Result := pointer(P);
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcfloat_extended));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(umlcfloat_extended), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;




end.

