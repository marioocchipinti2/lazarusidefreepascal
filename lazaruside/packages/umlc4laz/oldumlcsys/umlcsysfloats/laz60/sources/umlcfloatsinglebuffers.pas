(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfloatsinglebuffers;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle sintory in 1D 8 bits segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes, umlcfloatsingles,
  dummy;

// ---

const

  MOD_umlcsinglefloatbuffers: TUMLCModule =
    ($90,$60,$DE,$99,$96,$74,$F1,$4F,$B4,$0C,$5C,$0D,$61,$93,$0A,$1E);

// ---

(* global types *)

type
  umlcsinglebuffer_float = (* alias of *) pointer;

// ---

(* global functions *)

function BufferIsEmpty
  (var   ABuffer: Pointer;
   const ACount:  Cardinal): Boolean; overload;

function BufferSize
  (const ACount: Cardinal): Cardinal; overload;

function BufferNew
  (const ACount: Cardinal): Pointer; overload;

function BufferIsValidIndex
  (const ACount:  Cardinal;
   const AIndex:  Cardinal): Boolean; overload;

function BufferAt
  (const ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal): Pointer;  overload;

(* global procedures *)

procedure BufferClear
  (var   ABuffer: Pointer;
   const ACount:  Cardinal); overload;

procedure BufferFill
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AValue:  umlcfloat_single); overload;

procedure BufferDrop
  (var   ABuffer: Pointer;
   const ACount: Cardinal); overload;

(* global properties *)

procedure BufferGetAt
  (const ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   var   ADest:   umlcpfloat_single); overload;

procedure BufferSetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   const ASource: umlcpfloat_single); overload;

implementation

(* global functions *)

function BufferIsEmpty
  (var   ABuffer: Pointer;
   const ACount:  Cardinal): Boolean;
var I: Cardinal; Match: Boolean; Item: umlcpfloat_single;
begin
  Result := false;
  if (Assigned(ABuffer) and (ACount > 0)) then
  begin
    Item := umlcpfloat_single(ABuffer);
    Match := false;
    I := 0;
    while ((I <= System.Pred(ACount)) and (not Match)) do
    begin
      Match :=
        (Item^ <> 0);

      System.Inc(I);
    end;

    Result := not Match;
  end;
end;

function BufferSize
  (const ACount: Cardinal): Cardinal;
begin
  Result :=
    (sizeof(umlcfloat_single) * ACount);
end;

function BufferNew
  (const ACount: Cardinal): Pointer;
var ASize: Cardinal;
begin
  Result := nil;
  ASize  :=
    (sizeof(umlcfloat_single) * ACount);
  System.GetMem(Result, ASize);
end;

function BufferIsValidIndex
  (const ACount:  Cardinal;
   const AIndex:  Cardinal): Boolean;
begin
  Result :=
    (AIndex < ACount);
end;

(* global procedures *)

procedure BufferClear
  (var   ABuffer: Pointer;
   const ACount:  Cardinal);
var ASize: Cardinal;
begin
  if (Assigned(ABuffer)) then
  begin
    ASize :=
      (sizeof(umlcfloat_single) * ACount);
    System.FillByte(ABuffer^, ASize, 0);
  end;
end;

procedure BufferFill
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AValue:  umlcfloat_single);
//var ASize: Cardinal;
begin
  //if (Assigned(ABuffer)) then
  //begin
  //  ASize :=
  //    (sizeof(umlcfloat_single) * ACount);
  //  System.FillByte(ABuffer^, ASize, AValue);
  //end;
end;

procedure BufferDrop
  (var   ABuffer: Pointer;
   const ACount:  Cardinal);
var ASize: Cardinal;
begin
  if (Assigned(ABuffer)) then
  begin
    ASize :=(
      sizeof(umlcfloat_single) * ACount);
    System.FreeMem(ABuffer, ASize);
  end;
end;

function BufferAt
  (const ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal): Pointer;
var AOffset: Cardinal; ADest: umlcpfloat_single;
    CanContinue: Boolean;
begin
  Result := nil;
  CanContinue := true and
    Assigned(ABuffer) and
    (ACount > 0) and
    (AIndex < ACount);
  if (CanContinue) then
  begin
    AOffset :=
      (sizeof(umlcfloat_single) * AIndex);
    ADest := ABuffer;
    System.Inc(ADest, AOffset);
    Result := ADest;
  end;
end;

(* global properties *)

procedure BufferGetAt
  (const ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   var   ADest:   umlcpfloat_single);
var ASource: umlcpfloat_single;
begin
  ASource :=
    BufferAt(ABuffer, ACount, AIndex);
  if (Assigned(ASource)) then
  begin
    System.Move(ASource^, ADest^, sizeof(umlcfloat_single));
  end;
end;

procedure BufferSetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   const ASource: umlcpfloat_single);
var ADest: umlcpfloat_single;
begin
  ADest :=
    BufferAt(ABuffer, ACount, AIndex);
  if (Assigned(ADest)) then
  begin
    System.Move(ADest^, ASource^, sizeof(umlcfloat_single));
  end;
end;



end.

