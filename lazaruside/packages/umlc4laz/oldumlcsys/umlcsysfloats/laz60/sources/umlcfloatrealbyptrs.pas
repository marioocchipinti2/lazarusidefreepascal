(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfloatrealbyptrs;

{$mode objfpc}{$H+}

interface
uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes,
  dummy;

// ---

const

 MOD_umlcfloat_realbyptrs : TUMLCModule =
   ($94,$4B,$91,$B3,$EC,$77,$24,$4F,$86,$A9,$0D,$64,$F7,$13,$3D,$D6);

// ---

(* global functions *)

function ConstToPtr
  (const AValue: umlcfloat_real): pointer;

procedure DropPtr
  (var ADestPtr: pointer);

function IsEmpty
  (const ASource: pointer): Boolean; overload;

(* global procedures *)

procedure Clear
  (out ADest: pointer); overload;



implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcfloat_real): pointer;
var P: umlcpfloat_real;
begin
  System.GetMem(P, sizeof(umlcfloat_real));
  P^ := AValue;
  Result := pointer(P);
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcfloat_real));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(umlcfloat_real), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;




end.

