(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfloatcurrencies;

{$mode objfpc}{$H+}

interface
uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes,
  dummy;

// ---

const

 MOD_umlcfloatcurrencies : TUMLCModule =
   ($65,$1D,$29,$F5,$5C,$57,$81,$4E,$A6,$5A,$F0,$B4,$6D,$E5,$9C,$8A);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcfloat_currency): Boolean; overload;

(* global standard procedures *)

  procedure Clear
    (out ADest: umlcfloat_currency); overload;

(* global operators *)

  procedure Assign
    (out   ADest:   umlcfloat_currency;
     const ASource: umlcfloat_currency); overload; // operator :=

  function Compare
    (const A, B: umlcfloat_currency): umlctcomparison;

  function Different
    (const A, B: umlcfloat_currency): Boolean; overload; // operator <>
  function Equal
    (const A, B: umlcfloat_currency): Boolean; overload; // operator =

  function Greater
    (const A, B: umlcfloat_currency): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcfloat_currency): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcfloat_currency): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcfloat_currency): Boolean; overload; // operator <=


implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcfloat_currency): Boolean;
begin
  Result :=
    (ASource = 0);
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcfloat_currency);
begin
  System.FillByte(ADest, sizeof(umlcfloat_currency), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcfloat_currency;
   const ASource: umlcfloat_currency);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcfloat_currency): umlccomparison;
begin
  Result := cmpEqual;
  if (A < B)
    then Result := cmpLower
  else if (A > B)
    then Result := cmpHigher;
end;

function Different
  (const A, B: umlcfloat_currency): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Equal
  (const A, B: umlcfloat_currency): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Greater
  (const A, B: umlcfloat_currency): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcfloat_currency): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcfloat_currency): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcfloat_currency): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;


end.

