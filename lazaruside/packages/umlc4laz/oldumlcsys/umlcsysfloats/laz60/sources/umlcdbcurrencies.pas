(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdbcurrencies;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes,
  dummy;

// ---

const

 MOD_umlcdbcurrencies : TUMLCModule =
   ($EB,$A3,$3F,$8F,$76,$8F,$D3,$43,$85,$24,$86,$F1,$2F,$C4,$34,$67);

// ---



  // Las funciones "Float" utilizan "Double",
  // porque el tipo "Float" no existe !!!
  // "TFloatField" y "TCurrencyField" usan el tipo "Double" !!!

  // "Float" functions use "Double",
  // because "Float" type doesn't exist !!!
  // "TFloatField" & "TCurrencyField" use the "Double" type !!!

// ---

(* global standard functions *)

  function Compare(const A, B: umlcdbcurrency): TComparison;


(* global operators *)

  function Equal(const A, B: umlcdbcurrency): Boolean; // operator =

implementation

(* global standard functions *)

function Compare(const A, B: umlcdbcurrency): TComparison;
begin
  if (A = B)
    then Result := cmpEqual
  else if (A < B)
    then Result := cmpLower
  else Result := cmpHigher;
end;

(* global operators *)

function Equal(const A, B: umlcdbcurrency): Boolean;
begin
  Result := (A = B);
end;



end.

