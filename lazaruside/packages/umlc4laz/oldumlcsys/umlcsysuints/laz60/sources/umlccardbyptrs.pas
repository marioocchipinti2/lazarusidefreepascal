unit umlccardbyptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

  MOD_umlccardptrs : TUMLCModule =
    ($35,$D0,$99,$7C,$22,$87,$56,$48,$80,$93,$D0,$99,$D2,$71,$03,$C6);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: Cardinal): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: Cardinal): pointer;
var P: PCardinal;
begin
  System.GetMem(P, sizeof(Cardinal));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(Cardinal));
end;



end.

