unit umlcuint32s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 32 bits unsigned integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint32s : TUMLCModule =
   ($5A,$15,$B3,$B3,$19,$F6,$33,$40,$8A,$2A,$08,$A7,$8F,$E7,$94,$DF);

// ---

(* global functions *)





(* global procedures *)



(* global operators *)

  procedure Assign
    (out   ADest:   umlcuint_32;
     const ASource: umlcuint_32); overload; // operator :=

  function Equal
    (const A, B: umlcuint_32): Boolean; overload; // operator =
  function Different
    (const A, B: umlcuint_32): Boolean; overload; // operator <>
  function Greater
    (const A, B: umlcuint_32): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcuint_32): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcuint_32): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcuint_32): Boolean; overload; // operator <=

implementation

(* global functions *)




(* global procedures *)



(* global operators *)

procedure Assign
  (out   ADest:   umlcuint_32;
   const ASource: umlcuint_32);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcuint_32): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Different
  (const A, B: umlcuint_32): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Greater
  (const A, B: umlcuint_32): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcuint_32): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcuint_32): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcuint_32): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;




end.

