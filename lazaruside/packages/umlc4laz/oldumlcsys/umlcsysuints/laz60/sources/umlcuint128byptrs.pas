unit umlcuint128byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint128ptrs : TUMLCModule =
   ($2C,$B9,$0A,$34,$E8,$28,$B4,$4F,$9B,$61,$25,$F9,$1C,$7B,$BE,$CA);

// ---

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;

function ConstToPtr
 (const AValue: umlcuint_128): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;
var P: umlcpuint_128 absolute ASource;
begin
  Result := (P <> nil);
  if (Result) then
  begin
    // Result := (P^. = 0);
  end;
end;

function ConstToPtr
  (const AValue: umlcuint_128): pointer;
var P: umlcpuint_128;
begin
  System.GetMem(P, sizeof(umlcuint_128));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcuint_128));
end;



end.

