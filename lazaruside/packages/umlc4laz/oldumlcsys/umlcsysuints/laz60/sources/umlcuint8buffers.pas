(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuint8buffers;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle uintory in 1D 8 bits segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcuinttypes, umlcuint8s,
  dummy;

// ---

const

  MOD_umlcuint8buffers: TUMLCModule =
    ($2B,$4D,$1B,$E0,$53,$5C,$15,$40,$BC,$9E,$21,$0B,$CB,$D3,$B1,$EC);

// ---

(* global types *)

type
  umlcuintbuffer_8 = (* alias of *) pointer;

// ---

(* global functions *)

function BufferIsEmpty
  (var   ABuffer: Pointer;
   const ACount:  Cardinal): Boolean; overload;

function BufferSize
  (const ACount: Cardinal): Cardinal; overload;

function BufferNew
  (const ACount: Cardinal): Pointer; overload;

function BufferIsValidIndex
  (const ACount:  Cardinal;
   const AIndex:  Cardinal): Boolean; overload;

function BufferAt
  (const ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal): Pointer;  overload;

(* global procedures *)

procedure BufferClear
  (var   ABuffer: Pointer;
   const ACount:  Cardinal); overload;

procedure BufferFill
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AValue:  umlcuint_8); overload;

procedure BufferDrop
  (var   ABuffer: Pointer;
   const ACount: Cardinal); overload;

(* global properties *)

procedure BufferGetAt
  (const ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   var   ADest:   pointer); overload;

procedure BufferSetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   const ASource: pointer); overload;

implementation

(* global functions *)

function BufferIsEmpty
  (var   ABuffer: Pointer;
   const ACount:  Cardinal): Boolean;
var I: Cardinal; Match: Boolean; Item: umlcpuint_8;
begin
  Result := false;
  if (Assigned(ABuffer) and (ACount > 0)) then
  begin
    Item := umlcpuint_8(ABuffer);
    Match := false;
    I := 0;
    while ((I <= System.Pred(ACount)) and (not Match)) do
    begin
      Match :=
        (Item^ <> 0);

      System.Inc(I);
    end;

    Result := not Match;
  end;
end;

function BufferSize
  (const ACount: Cardinal): Cardinal;
begin
  Result :=
    (sizeof(umlcuint_8) * ACount);
end;

function BufferNew
  (const ACount: Cardinal): Pointer;
var ASize: Cardinal;
begin
  Result := nil;
  ASize  :=
    (sizeof(umlcuint_8) * ACount);
  System.GetMem(Result, ASize);
end;

function BufferIsValidIndex
  (const ACount:  Cardinal;
   const AIndex:  Cardinal): Boolean;
begin
  Result :=
    (AIndex < ACount);
end;

(* global procedures *)

procedure BufferClear
  (var   ABuffer: Pointer;
   const ACount:  Cardinal);
var ASize: Cardinal;
begin
  if (Assigned(ABuffer)) then
  begin
    ASize :=
      (sizeof(umlcuint_8) * ACount);
    System.FillByte(ABuffer^, ASize, 0);
  end;
end;

procedure BufferFill
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AValue:  umlcuint_8);
var ASize: Cardinal;
begin
  if (Assigned(ABuffer)) then
  begin
    ASize :=
      (sizeof(umlcuint_8) * ACount);
    System.FillByte(ABuffer^, ASize, AValue);
  end;
end;

procedure BufferDrop
  (var   ABuffer: Pointer;
   const ACount:  Cardinal);
var ASize: Cardinal;
begin
  if (Assigned(ABuffer)) then
  begin
    ASize :=(
      sizeof(umlcuint_8) * ACount);
    System.FreeMem(ABuffer, ASize);
  end;
end;

function BufferAt
  (const ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal): Pointer;
var AOffset: Cardinal; ADest: umlcpuint_8;
    CanContinue: Boolean;
begin
  Result := nil;
  CanContinue := true and
    Assigned(ABuffer) and
    (ACount > 0) and
    (AIndex < ACount);
  if (CanContinue) then
  begin
    AOffset :=
      (sizeof(umlcuint_8) * AIndex);
    ADest := ABuffer;
    System.Inc(ADest, AOffset);
    Result := ADest;
  end;
end;

(* global properties *)

procedure BufferGetAt
  (const ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   var   ADest:   pointer);
var ASource: umlcpuint_8;
begin
  ASource :=
    BufferAt(ABuffer, ACount, AIndex);
  if (Assigned(ASource)) then
  begin
    System.Move(ASource^, ADest^, sizeof(umlcuint_8));
  end;
end;

procedure BufferSetAt
  (var   ABuffer: Pointer;
   const ACount:  Cardinal;
   const AIndex:  Cardinal;
   const ASource: pointer);
var ADest: umlcpuint_8;
begin
  ADest :=
    BufferAt(ABuffer, ACount, AIndex);
  if (Assigned(ADest)) then
  begin
    System.Move(ADest^, ASource^, sizeof(umlcuint_8));
  end;
end;



end.

