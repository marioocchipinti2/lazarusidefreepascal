unit umlcuint16byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint16s : TUMLCModule =
   ($44,$3D,$61,$41,$9A,$61,$23,$4B,$8F,$05,$51,$B1,$89,$B2,$4A,$86);

// ---


(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;

function ConstToPtr
  (const AValue: umlcuint_16): pointer;

procedure DropPtr
  (var ADestPtr: pointer);

implementation

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;
var P: umlcpuint_16 absolute ASource;
begin
  Result := (P <> nil);
  if (Result) then
  begin
    Result := (P^ = 0);
  end;
end;

function ConstToPtr
  (const AValue: umlcuint_16): pointer;
var P: umlcpuint_16;
begin
  System.GetMem(P, sizeof(umlcuint_16));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcuint_16));
end;


end.

