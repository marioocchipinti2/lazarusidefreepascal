(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuint256s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 256 bits unsigned integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint256s : TUMLCModule =
   ($AF,$FB,$9E,$7A,$AA,$31,$10,$47,$BA,$66,$C3,$62,$9E,$E2,$B6,$CB);

// ---

(* global functions *)





(* global procedures *)

procedure Clear
  (out ADest: umlcuint_256); overload;

procedure Encode128To256
  (out   ADest: umlcuint_256;
   const ALo:   umlcuint_128;
   const AHi:   umlcuint_128);

procedure Decode256To128
  (out   ALo:     umlcuint_128;
   out   AHi:     umlcuint_128;
   const ASource: umlcuint_256);

(* global operators *)

procedure Assign
  (out   ADest:   umlcuint_256;
   const ASource: umlcuint_256); overload; // operator :=

function Equal
  (const A, B: umlcuint_256): Boolean; overload; // operator =
function Different
  (const A, B: umlcuint_256): Boolean; overload; // operator <>
function Greater
  (const A, B: umlcuint_256): Boolean; overload; // operator >
function Lesser
  (const A, B: umlcuint_256): Boolean; overload; // operator <
function GreaterEqual
  (const A, B: umlcuint_256): Boolean; overload; // operator >=
function LesserEqual
  (const A, B: umlcuint_256): Boolean; overload; // operator <=

implementation
uses umlcuint128s;

(* global procedures *)

procedure Clear
  (out ADest: umlcuint_256);
begin
  System.FillByte(ADest, sizeof(umlcuint_256), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

procedure Encode128To256
  (out   ADest: umlcuint_256;
   const ALo:   umlcuint_128;
   const AHi:   umlcuint_128);
begin
  umlcuint256s.Clear(ADest);
  System.Move(ALo, ADest.Value[0], sizeof(umlcuint_128));
  System.Move(AHi, ADest.Value[1], sizeof(umlcuint_128));
end;

procedure Decode256To128
  (out   ALo:     umlcuint_128;
   out   AHi:     umlcuint_128;
   const ASource: umlcuint_256);
begin
  umlcuint128s.Clear(ALo);
  umlcuint128s.Clear(AHi);
  System.Move(ASource.Value[0], ALo, sizeof(umlcuint_128));
  System.Move(ASource.Value[1], AHi, sizeof(umlcuint_128));
end;


(* global operators *)

procedure Assign
  (out   ADest:   umlcuint_256;
   const ASource: umlcuint_256);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcuint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_256));
  Result :=
    (ErrorCode = cmpEqual);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Different
  (const A, B: umlcuint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_256));
  Result :=
    (ErrorCode <> cmpEqual);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Greater
  (const A, B: umlcuint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_256));
  Result :=
    (ErrorCode = cmpHigher);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Lesser
  (const A, B: umlcuint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_256));
  Result :=
    (ErrorCode = cmpLower);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function GreaterEqual
  (const A, B: umlcuint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_256));
  Result :=
    ((ErrorCode = cmpEqual) or (ErrorCode = cmpHigher));
end;

function LesserEqual
  (const A, B: umlcuint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_256));
  Result :=
    ((ErrorCode = cmpEqual) or (ErrorCode = cmpLower));
end;




end.

