(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsysuints__rtti;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules,
  dummy;


// ---

const

  MOD_umlctrees : TUMLCModule =
    ($3F,$EB,$8D,$1A,$2D,$45,$C5,$43,$93,$99,$28,$D3,$01,$5F,$24,$B9);

// ---


implementation

end.

