unit umlcuint8s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 8 bits unsigned integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint8s : TUMLCModule =
   ($6B,$75,$7D,$5B,$65,$96,$96,$44,$86,$B6,$2A,$5D,$E2,$92,$D7,$E9);

// ---

(* global functions *)




(* global procedures *)



(* global operators *)

  procedure Assign
    (out   ADest:   umlcuint_8;
     const ASource: umlcuint_8); overload; // operator :=

  function Equal
    (const A, B: umlcuint_8): Boolean; overload; // operator =
  function Different
    (const A, B: umlcuint_8): Boolean; overload; // operator <>

  function Greater
    (const A, B: umlcuint_8): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcuint_8): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcuint_8): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcuint_8): Boolean; overload; // operator <=

implementation

(* global functions *)




(* global procedures *)



(* global operators *)

procedure Assign
  (out   ADest:   umlcuint_8;
   const ASource: umlcuint_8);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcuint_8): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Different
  (const A, B: umlcuint_8): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Greater
  (const A, B: umlcuint_8): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcuint_8): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcuint_8): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcuint_8): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;




end.

