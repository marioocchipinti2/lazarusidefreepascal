unit umlcuint64s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 64 bits unsigned integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint64s : TUMLCModule =
   ($F8,$A5,$8C,$B2,$45,$B9,$EF,$4F,$B9,$CE,$11,$CB,$AF,$AC,$32,$31);

// ---

(* global functions *)





(* global procedures *)



(* global operators *)

  procedure Assign
    (out   ADest:   umlcuint_64;
     const ASource: umlcuint_64); overload; // operator :=

  function Equal
    (const A, B: umlcuint_64): Boolean; overload; // operator =
  function Different
    (const A, B: umlcuint_64): Boolean; overload; // operator <>
  function Greater
    (const A, B: umlcuint_64): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcuint_64): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcuint_64): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcuint_64): Boolean; overload; // operator <=

implementation

(* global functions *)




(* global procedures *)



(* global operators *)

procedure Assign
  (out   ADest:   umlcuint_64;
   const ASource: umlcuint_64);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcuint_64): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Different
  (const A, B: umlcuint_64): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Greater
  (const A, B: umlcuint_64): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcuint_64): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcuint_64): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcuint_64): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;




end.

