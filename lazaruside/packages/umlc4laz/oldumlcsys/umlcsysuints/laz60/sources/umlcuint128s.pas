(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuint128s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 128 bits unsigned integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint128s : TUMLCModule =
   ($AF,$FB,$9E,$7A,$AA,$31,$10,$47,$BA,$66,$C3,$62,$9E,$E2,$B6,$CB);

// ---

(* global functions *)





(* global procedures *)

procedure Clear
  (out ADest: umlcuint_128); overload;

procedure Encode64To128
  (out   ADest: umlcuint_128;
   const ALo:   umlcuint_64;
   const AHi:   umlcuint_64);

procedure Decode128To64
  (out   ALo:     umlcuint_64;
   out   AHi:     umlcuint_64;
   const ASource: umlcuint_128);

(* global operators *)

procedure Assign
  (out   ADest:   umlcuint_128;
   const ASource: umlcuint_128); overload; // operator :=

function Equal
  (const A, B: umlcuint_128): Boolean; overload; // operator =
function Different
  (const A, B: umlcuint_128): Boolean; overload; // operator <>
function Greater
  (const A, B: umlcuint_128): Boolean; overload; // operator >
function Lesser
  (const A, B: umlcuint_128): Boolean; overload; // operator <
function GreaterEqual
  (const A, B: umlcuint_128): Boolean; overload; // operator >=
function LesserEqual
  (const A, B: umlcuint_128): Boolean; overload; // operator <=

implementation

(* global procedures *)

procedure Clear
  (out ADest: umlcuint_128);
begin
  System.FillByte(ADest, sizeof(umlcuint_128), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

procedure Encode64To128
  (out   ADest: umlcuint_128;
   const ALo:   umlcuint_64;
   const AHi:   umlcuint_64);
begin
  umlcuint128s.Clear(ADest);
  System.Move(ALo, ADest.Value[0], sizeof(umlcuint_64));
  System.Move(AHi, ADest.Value[1], sizeof(umlcuint_64));
end;

procedure Decode128To64
  (out   ALo:     umlcuint_64;
   out   AHi:     umlcuint_64;
   const ASource: umlcuint_128);
begin
  ALo := 0;
  AHi := 0;
  System.Move(ASource.Value[0], ALo, sizeof(umlcuint_64));
  System.Move(ASource.Value[1], AHi, sizeof(umlcuint_64));
end;


(* global operators *)

procedure Assign
  (out   ADest:   umlcuint_128;
   const ASource: umlcuint_128);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcuint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_128));
  Result :=
    (ErrorCode = cmpEqual);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Different
  (const A, B: umlcuint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_128));
  Result :=
    (ErrorCode <> cmpEqual);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Greater
  (const A, B: umlcuint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_128));
  Result :=
    (ErrorCode = cmpHigher);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Lesser
  (const A, B: umlcuint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_128));
  Result :=
    (ErrorCode = cmpLower);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function GreaterEqual
  (const A, B: umlcuint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_128));
  Result :=
    ((ErrorCode = cmpEqual) or (ErrorCode = cmpHigher));
end;

function LesserEqual
  (const A, B: umlcuint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_128));
  Result :=
    ((ErrorCode = cmpEqual) or (ErrorCode = cmpLower));
end;




end.

