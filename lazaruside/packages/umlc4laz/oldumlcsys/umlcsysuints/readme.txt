readme.txt
==========

The "umlcsyssints" folder contains the UMLCat set of libraries for the FreePascal &
Lazarus Programming Framework.

This package is an extension to the related "umlcsys" base / system library package,
with Unsigned Integer Numeric types and operations.

