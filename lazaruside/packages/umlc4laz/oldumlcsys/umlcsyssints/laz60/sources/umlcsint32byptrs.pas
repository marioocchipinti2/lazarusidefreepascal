unit umlcsint32byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint32ptrs : TUMLCModule =
  ($76,$42,$91,$C9,$19,$5E,$06,$48,$BC,$3A,$35,$F0,$CB,$AD,$34,$02);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: umlcsint_32): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcsint_32): pointer;
var P: umlcpsint_32;
begin
  System.GetMem(P, sizeof(umlcsint_32));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcsint_32));
end;



end.

