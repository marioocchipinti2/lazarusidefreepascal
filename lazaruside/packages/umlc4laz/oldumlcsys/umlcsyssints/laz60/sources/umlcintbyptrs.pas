unit umlcintbyptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

  MOD_umlcintptrs : TUMLCModule =
    ($19,$C6,$FA,$47,$5B,$72,$F3,$40,$8F,$79,$AD,$02,$49,$1A,$9A,$E0);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: Integer): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: Integer): pointer;
var P: PInteger;
begin
  System.GetMem(P, sizeof(Integer));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(Integer));
end;



end.

