unit umlcsint16byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint16s : TUMLCModule =
  ($D5,$34,$55,$95,$81,$7A,$FE,$46,$9F,$4E,$E0,$34,$33,$94,$B9,$9B);

// ---


(* global functions *)

function ConstToPtr
  (const AValue: umlcsint_16): pointer;

procedure DropPtr
  (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcsint_16): pointer;
var P: umlcpsint_16;
begin
  System.GetMem(P, sizeof(umlcsint_16));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcsint_16));
end;


end.

