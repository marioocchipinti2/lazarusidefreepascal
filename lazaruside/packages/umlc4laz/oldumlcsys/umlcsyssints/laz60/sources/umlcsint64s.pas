unit umlcsint64s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 64 bits signed integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint64s : TUMLCModule =
   ($F8,$A3,$6A,$8F,$61,$D3,$13,$4C,$A2,$46,$E9,$53,$0E,$5F,$D1,$E4);

 // ---

 (* global functions *)




 (* global procedures *)



 (* global operators *)

   procedure Assign
     (out   ADest:   umlcsint_64;
      const ASource: umlcsint_64); overload; // operator :=

   function Equal
     (const A, B: umlcsint_64): Boolean; overload; // operator =
   function Different
     (const A, B: umlcsint_64): Boolean; overload; // operator <>
   function Greater
     (const A, B: umlcsint_64): Boolean; overload; // operator >
   function Lesser
     (const A, B: umlcsint_64): Boolean; overload; // operator <
   function GreaterEqual
     (const A, B: umlcsint_64): Boolean; overload; // operator >=
   function LesserEqual
     (const A, B: umlcsint_64): Boolean; overload; // operator <=


implementation

(* global functions *)




(* global procedures *)


(* global operators *)

procedure Assign
  (out   ADest:   umlcsint_64;
   const ASource: umlcsint_64);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcsint_64): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Different
  (const A, B: umlcsint_64): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Greater
  (const A, B: umlcsint_64): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcsint_64): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcsint_64): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcsint_64): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

end.

