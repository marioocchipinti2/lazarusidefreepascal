unit umlcsint128s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 128 bits signed integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint128s : TUMLCModule =
   ($6D,$57,$B6,$EE,$94,$9B,$A0,$4A,$A1,$85,$3C,$14,$E4,$11,$DE,$5D);

 // ---

 (* global functions *)




 (* global procedures *)

   procedure Clear
     (var ADest: umlcsint_128); overload;


 (* global operators *)

   procedure Assign
     (out   ADest:   umlcsint_128;
      const ASource: umlcsint_128); overload; // operator :=

   function Equal
     (const A, B: umlcsint_128): Boolean; overload; // operator =
   function Different
     (const A, B: umlcsint_128): Boolean; overload; // operator <>
   function Greater
     (const A, B: umlcsint_128): Boolean; overload; // operator >
   function Lesser
     (const A, B: umlcsint_128): Boolean; overload; // operator <
   function GreaterEqual
     (const A, B: umlcsint_128): Boolean; overload; // operator >=
   function LesserEqual
     (const A, B: umlcsint_128): Boolean; overload; // operator <=



implementation

(* global functions *)




(* global procedures *)

procedure Clear
  (var ADest: umlcsint_128);
begin
  System.FillByte(ADest, sizeof(umlcsint_128), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;


(* global operators *)

procedure Assign
  (out   ADest:   umlcsint_128;
   const ASource: umlcsint_128);
begin
  System.Move(ASource, ADest, sizeof(umlcsint_128));
end;

function Equal
  (const A, B: umlcsint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_128));
  Result :=
    (ErrorCode = cmpEqual);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Different
  (const A, B: umlcsint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_128));
  Result :=
    (ErrorCode <> cmpEqual);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Greater
  (const A, B: umlcsint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_128));
  Result :=
    (ErrorCode = cmpHigher);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Lesser
  (const A, B: umlcsint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_128));
  Result :=
    (ErrorCode = cmpLower);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function GreaterEqual
  (const A, B: umlcsint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_128));
  Result :=
    ((ErrorCode = cmpEqual) or (ErrorCode = cmpHigher));
end;

function LesserEqual
  (const A, B: umlcsint_128): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_128));
  Result :=
    ((ErrorCode = cmpEqual) or (ErrorCode = cmpLower));
end;


end.

