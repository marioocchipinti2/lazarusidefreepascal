unit umlcsint512s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 512 bits signed integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint512s : TUMLCModule =
   ($7D,$BA,$3C,$9E,$B5,$2D,$0A,$4A,$8D,$C8,$38,$73,$58,$63,$3E,$03);

 // ---

 (* global functions *)




 (* global procedures *)

   procedure Clear
     (out ADest: umlcsint_512); overload;


 (* global operators *)

   procedure Assign
     (out   ADest:   umlcsint_512;
      const ASource: umlcsint_512); overload; // operator :=

   function Equal
     (const A, B: umlcsint_512): Boolean; overload; // operator =
   function Different
     (const A, B: umlcsint_512): Boolean; overload; // operator <>
   function Greater
     (const A, B: umlcsint_512): Boolean; overload; // operator >
   function Lesser
     (const A, B: umlcsint_512): Boolean; overload; // operator <
   function GreaterEqual
     (const A, B: umlcsint_512): Boolean; overload; // operator >=
   function LesserEqual
     (const A, B: umlcsint_512): Boolean; overload; // operator <=


implementation

(* global functions *)




(* global procedures *)

procedure Clear
  (out ADest: umlcsint_512);
begin
  System.FillByte(ADest, sizeof(umlcsint_512), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;


(* global operators *)

procedure Assign
  (out   ADest:   umlcsint_512;
   const ASource: umlcsint_512);
begin
  System.Move(ASource, ADest, sizeof(umlcsint_512));
end;

function Equal
  (const A, B: umlcsint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_512));
  Result :=
    (ErrorCode = cmpEqual);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Different
  (const A, B: umlcsint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_512));
  Result :=
    (ErrorCode <> cmpEqual);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Greater
  (const A, B: umlcsint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_512));
  Result :=
    (ErrorCode = cmpHigher);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Lesser
  (const A, B: umlcsint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_512));
  Result :=
    (ErrorCode = cmpLower);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function GreaterEqual
  (const A, B: umlcsint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_512));
  Result :=
    ((ErrorCode = cmpEqual) or (ErrorCode = cmpHigher));
end;

function LesserEqual
  (const A, B: umlcsint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_512));
  Result :=
    ((ErrorCode = cmpEqual) or (ErrorCode = cmpLower));
end;


end.

