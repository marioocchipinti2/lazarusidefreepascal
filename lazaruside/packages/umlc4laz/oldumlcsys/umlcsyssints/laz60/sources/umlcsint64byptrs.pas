unit umlcsint64byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint64ptrs : TUMLCModule =
   ($EB,$ED,$58,$ED,$04,$4C,$BE,$4F,$AE,$30,$EA,$5E,$AB,$1C,$65,$A6);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: umlcsint_64): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcsint_64): pointer;
var P: umlcpsint_64;
begin
  System.GetMem(P, sizeof(umlcsint_64));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcsint_64));
end;



end.

