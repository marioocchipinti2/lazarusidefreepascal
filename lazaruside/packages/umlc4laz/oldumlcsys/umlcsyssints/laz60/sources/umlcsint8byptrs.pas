unit umlcsint8byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint8s : TUMLCModule =
  ($C5,$5E,$F8,$6B,$62,$AB,$25,$4E,$AB,$02,$F8,$C2,$30,$99,$E5,$B8);

// ---


(* global functions *)

function ConstToPtr
  (const AValue: umlcsint_8): pointer;

procedure DropPtr
  (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcsint_8): pointer;
var P: umlcpsint_8;
begin
  System.GetMem(P, sizeof(umlcsint_8));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcsint_8));
end;


end.

