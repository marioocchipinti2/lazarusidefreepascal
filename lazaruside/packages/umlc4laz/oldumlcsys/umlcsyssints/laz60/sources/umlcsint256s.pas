unit umlcsint256s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 256 bits signed integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint256s : TUMLCModule =
   ($C5,$B7,$84,$66,$D1,$9B,$0E,$47,$A5,$5D,$ED,$77,$C6,$A8,$9A,$FC);

 // ---

 (* global functions *)




 (* global procedures *)

   procedure Clear
     (out ADest: umlcsint_256); overload;


 (* global operators *)

   procedure Assign
     (out   ADest:   umlcsint_256;
      const ASource: umlcsint_256); overload; // operator :=

   function Equal
     (const A, B: umlcsint_256): Boolean; overload; // operator =
   function Different
     (const A, B: umlcsint_256): Boolean; overload; // operator <>
   function Greater
     (const A, B: umlcsint_256): Boolean; overload; // operator >
   function Lesser
     (const A, B: umlcsint_256): Boolean; overload; // operator <
   function GreaterEqual
     (const A, B: umlcsint_256): Boolean; overload; // operator >=
   function LesserEqual
     (const A, B: umlcsint_256): Boolean; overload; // operator <=


implementation

(* global functions *)




(* global procedures *)

procedure Clear
  (out ADest: umlcsint_256);
begin
  System.FillByte(ADest, sizeof(umlcsint_256), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcsint_256;
   const ASource: umlcsint_256);
begin
  System.Move(ASource, ADest, sizeof(umlcsint_256));
end;

function Equal
  (const A, B: umlcsint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_256));
  Result :=
    (ErrorCode = cmpEqual);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Different
  (const A, B: umlcsint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_256));
  Result :=
    (ErrorCode <> cmpEqual);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Greater
  (const A, B: umlcsint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_256));
  Result :=
    (ErrorCode = cmpHigher);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Lesser
  (const A, B: umlcsint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_256));
  Result :=
    (ErrorCode = cmpLower);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function GreaterEqual
  (const A, B: umlcsint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_256));
  Result :=
    ((ErrorCode = cmpEqual) or (ErrorCode = cmpHigher));
end;

function LesserEqual
  (const A, B: umlcsint_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcsint_256));
  Result :=
    ((ErrorCode = cmpEqual) or (ErrorCode = cmpLower));
end;


end.

