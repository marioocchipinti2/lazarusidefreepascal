unit umlcsint32s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 32 bits signed integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint32s : TUMLCModule =
   ($80,$DE,$11,$74,$9E,$F5,$52,$49,$92,$62,$91,$E4,$48,$59,$0B,$42);

 // ---

 (* global functions *)




 (* global procedures *)



 (* global operators *)

   procedure Assign
     (out   ADest:   umlcsint_32;
      const ASource: umlcsint_32); overload; // operator :=

   function Equal
     (const A, B: umlcsint_32): Boolean; overload; // operator =
   function Different
     (const A, B: umlcsint_32): Boolean; overload; // operator <>
   function Greater
     (const A, B: umlcsint_32): Boolean; overload; // operator >
   function Lesser
     (const A, B: umlcsint_32): Boolean; overload; // operator <
   function GreaterEqual
     (const A, B: umlcsint_32): Boolean; overload; // operator >=
   function LesserEqual
     (const A, B: umlcsint_32): Boolean; overload; // operator <=



implementation

(* global functions *)




(* global procedures *)


(* global operators *)

procedure Assign
  (out   ADest:   umlcsint_32;
   const ASource: umlcsint_32);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcsint_32): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Different
  (const A, B: umlcsint_32): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Greater
  (const A, B: umlcsint_32): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcsint_32): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcsint_32): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcsint_32): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;


end.

