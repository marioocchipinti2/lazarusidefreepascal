(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsimplesequences;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids, umlctypes, umlcstdtypes,
  umlcnormobjects,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  umlcsequences,
  dummy;

const

  // ---

  IDTUMLCSimpleSequence : TUMLCType =
    ($2D,$6A,$77,$9A,$D3,$27,$9A,$42,$82,$81,$77,$87,$1C,$3D,$02,$61);

  // ---

type

(* TUMLCSimpleSequence *)

  TUMLCSimpleSequence = class(TUMLCPointerListSequence)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    //function IsEmpty(): Boolean; virtual;

    //procedure Clear(); virtual;
  end;


implementation

end.

