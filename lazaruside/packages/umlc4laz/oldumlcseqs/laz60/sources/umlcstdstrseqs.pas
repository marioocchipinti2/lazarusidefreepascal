(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdstrseqs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids, umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcstdobjs,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  umlcsequences,
  umlcsimplesequences,
  dummy;

const

  // ---

  ID_TUMLCStringSequence : TUMLCType =
    ($B0,$A5,$EF,$73,$90,$C5,$07,$47,$BF,$B5,$28,$12,$58,$08,$76,$67);

  ID_TUMLCStringSequenceForwardIterator : TUMLCType =
    ($5A,$2B,$C7,$40,$E9,$FC,$D3,$40,$8B,$29,$AA,$0E,$FC,$8F,$C8,$D2);

  ID_TUMLCStringSequenceBackwardIterator : TUMLCType =
    ($26,$81,$C3,$44,$E3,$56,$E5,$4B,$84,$F4,$8F,$67,$FD,$CE,$BF,$D4);

  // ---

type

(* TUMLCStringSequence *)

  TUMLCStringSequence =
    class(TUMLCSimpleSequence)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    procedure InternalAssignFrom
      (const ASource: IUMLCInternalCloneableObject); override;
  protected
    (* protected declarations *)

    function InternalNewSequence
      (): TUMLCSequence; override;

    function InternalNewItem
      (): Pointer; override;

    procedure InternalDropItem
      (var AItem: Pointer); override;

    function InternalItemSize(): Integer; override;

    function InternalCompareItems
      (const A: Pointer;
       const B: Pointer): TComparison; override;

    function AreEqualMembers
      (const A: Pointer;
       const B: Pointer): Boolean; override;

    function InternalGenerateForwardIterator
      (): TUMLCSequenceIterator; override;

    function InternalGenerateBackwardIterator
      (): TUMLCSequenceIterator; override;
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* protected declarations *)

    function AggregateLowestValue(): shortstring;

    function AggregateHighestValue(): shortstring;

    function AggregateConcat(): shortstring;

    function AggregateConcatSep
      (const AStartDelim: string;
       const AFinishDelim: string): shortstring;
  public
    (* public declarations *)

    function NullOf(): TUMLCStringSequence;

    function NotNullOf(): TUMLCStringSequence;

    function FirstOf(): TUMLCStringSequence;

    function LastOf(): TUMLCStringSequence;

    function FirstOrNullOf(): TUMLCStringSequence;

    function LastOrNullOf(): TUMLCStringSequence;
  public
    (* public declarations *)

    //function IsEmpty(): Boolean; virtual;

    function Extract(): shortstring;

    procedure Delete();

    procedure Insert(const AItem: shortstring);

    procedure AssignFrom
      (const ASource: TUMLCStringSequence); // nonvirtual;
    procedure AssignTo
      (var ADest: TUMLCStringSequence); // nonvirtual;

    function GenerateIterator
      (): TUMLCSequenceIterator; // nonvirtual;

    function GenerateBackwardIterator
      (): TUMLCSequenceIterator; // nonvirtual;

    procedure ReleaseIterator
      (var AIterator: TUMLCSequenceIterator); // nonvirtual;
  end;

(* TUMLCStringSequenceForwardIterator *)

  TUMLCStringSequenceForwardIterator =
    class(TUMLCPointerListSequenceForwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function Read: shortstring;
  end;

(* TUMLCStringSequenceBackwardIterator *)

  TUMLCStringSequenceBackwardIterator =
    class(TUMLCPointerListSequenceBackwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function Read: shortstring;
  end;

implementation

(* TUMLCStringSequence *)

procedure TUMLCStringSequence.InternalAssignFrom
  (const ASource: IUMLCInternalCloneableObject);
var ASequence: TUMLCStringSequence;
    AIterator: TUMLCSequenceIterator;
    AItem: shortstring;
begin
  ASequence :=
    (ASource as TUMLCStringSequence);
  AIterator :=
    ASequence.GenerateIterator();

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCStringSequenceForwardIterator).Read();
    Self.Insert(AItem);

    AIterator.MoveNext();
  end;

  ASequence.ReleaseIterator(AIterator);
end;

function TUMLCStringSequence.InternalNewSequence
  (): TUMLCSequence;
begin
  Result := TUMLCStringSequence.Create();
end;

function TUMLCStringSequence.InternalNewItem
  (): Pointer;
var P: PShortString;
begin
  P :=
    New(PShortString);
  P^ := '';

  Result := P;
end;


procedure TUMLCStringSequence.InternalDropItem
  (var AItem: Pointer);
var ADest: PShortString;
begin
  if (Assigned(AItem)) then
  begin
    ADest := AItem;
    AItem := nil;
    Dispose(ADest);
  end;
end;

function TUMLCStringSequence.InternalItemSize(): Integer;
begin
  Result :=
    sizeof(ShortString);
end;

function TUMLCStringSequence.InternalCompareItems
  (const A: Pointer;
   const B: Pointer): TComparison;
var C, D: PShortString;
begin
  C := PShortString(A);
  D := PShortString(B);

  if (C^ < D^)
    then Result := cmpLower
  else if (C^ > D^)
    then Result := cmpHigher
  else Result := cmpEqual;
end;

function TUMLCStringSequence.AreEqualMembers
  (const A: Pointer;
   const B: Pointer): Boolean;
var C, D: PShortstring;
begin
  Result :=
    (Assigned(A) and Assigned(B));
  if (Result) then
  begin
    C := PShortstring(A);
    D := PShortstring(B);
    Result :=
      (C^ = D^)
  end;
end;

function TUMLCStringSequence.InternalGenerateForwardIterator(): TUMLCSequenceIterator;
var AIterator: TUMLCStringSequenceForwardIterator;
begin
  AIterator :=
    TUMLCStringSequenceForwardIterator.Create();
  AIterator.Sequence := Self;
  Self.InternalPrepareIterator();

  AIterator.Start();

  Result :=
    AIterator;
end;

function TUMLCStringSequence.InternalGenerateBackwardIterator(): TUMLCSequenceIterator;
var AIterator: TUMLCStringSequenceBackwardIterator;
begin
  AIterator :=
    TUMLCStringSequenceBackwardIterator.Create();
  AIterator.Sequence := Self;
  Self.InternalPrepareIterator();

  AIterator.Start();

  Result :=
    AIterator;
end;

constructor TUMLCStringSequence.Create();
begin
  inherited (* nonvirtual *) Create();
  Self.FBaseType := umlcstdtypes.ID_String;

  System.GetMem(Self.FDefaultItemValue, Sizeof(ShortString));
  System.FillChar(Self.FDefaultItemValue^, Sizeof(ShortString), 0);
end;

destructor TUMLCStringSequence.Destroy();
begin
  System.FreeMem(Self.FDefaultItemValue, Sizeof(ShortString));

  inherited Destroy();
end;

function TUMLCStringSequence.AggregateLowestValue(): shortstring;
var AItem: PShortString;
begin
  Result := '';

  AItem :=
    Self.InternalAggregateLowestValue();
  if (Assigned(AItem)) then
  begin
    Result :=
      AItem^;
  end;
end;

function TUMLCStringSequence.AggregateHighestValue(): shortstring;
var AItem: PShortString;
begin
  Result := '';

  AItem :=
    Self.InternalAggregateHighestValue();
  if (Assigned(AItem)) then
  begin
    Result :=
      AItem^;
  end;
end;

function TUMLCStringSequence.AggregateConcat(): shortstring;
var AItem: PShortstring;
    AIterator: TUMLCSequenceIterator;
begin
  Result := '';

  AIterator :=
    Self.InternalGenerateForwardIterator();

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      AIterator.InternalRead();

    Result :=
      (Result + AItem^);

    AIterator.MoveNext();
  end;

  Self.InternalReleaseIterator(AIterator);
end;

function TUMLCStringSequence.AggregateConcatSep
  (const AStartDelim: string;
   const AFinishDelim: string): shortstring;
var AItem: PShortstring;
    AIterator: TUMLCSequenceIterator;
begin
  Result := '';

  AIterator :=
    Self.InternalGenerateForwardIterator();

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      AIterator.InternalRead();

    Result :=
      (Result + AStartDelim + AItem^ + AFinishDelim);

    AIterator.MoveNext();
  end;

  Self.InternalReleaseIterator(AIterator);
end;

function TUMLCStringSequence.NullOf(): TUMLCStringSequence;
begin
  Result :=
    (Self.InternalNullOf() as TUMLCStringSequence);
end;

function TUMLCStringSequence.NotNullOf(): TUMLCStringSequence;
begin
  Result :=
    (Self.InternalNotNullOf() as TUMLCStringSequence);
end;

function TUMLCStringSequence.FirstOf(): TUMLCStringSequence;
begin
  Result :=
    (Self.InternalFirstOf() as TUMLCStringSequence);
end;

function TUMLCStringSequence.LastOf(): TUMLCStringSequence;
begin
  Result :=
    (Self.InternalLastOf() as TUMLCStringSequence);
end;

function TUMLCStringSequence.FirstOrNullOf(): TUMLCStringSequence;
begin
  Result :=
    (Self.InternalFirstOrNullOf() as TUMLCStringSequence);
end;

function TUMLCStringSequence.LastOrNullOf(): TUMLCStringSequence;
begin
  Result :=
    (Self.InternalLastOrNullOf() as TUMLCStringSequence);
end;

function TUMLCStringSequence.Extract(): shortstring;
var P: ^shortstring;
begin
  P := FItems.Extract(FItems.Count - 1);
  Result := P^;
end;

procedure TUMLCStringSequence.Delete();
var P: ^shortstring;
begin
  P := FItems.Extract(FItems.Count - 1);
  FreeMem(P, Sizeof(shortstring));
end;

procedure TUMLCStringSequence.Insert(const AItem: shortstring);
var P: ^shortstring;
begin
  GetMem(P, Sizeof(shortstring));
  P^ := AItem;
  Self.InternalInsert(P);
end;

procedure TUMLCStringSequence.AssignFrom
  (const ASource: TUMLCStringSequence);
begin
  Self.InternalAssignFrom(ASource);
end;

procedure TUMLCStringSequence.AssignTo
  (var ADest: TUMLCStringSequence);
var D: IUMLCInternalCloneableObject;
begin
  D := (ADest as IUMLCInternalCloneableObject);
  Self.InternalAssignTo(D);
end;

function TUMLCStringSequence.GenerateIterator
  (): TUMLCSequenceIterator;
begin
  Result :=
    (Self.InternalGenerateForwardIterator() as TUMLCSequenceIterator);
end;

function TUMLCStringSequence.GenerateBackwardIterator
  (): TUMLCSequenceIterator;
begin
  Result :=
    (Self.InternalGenerateBackwardIterator() as TUMLCSequenceIterator);
end;

procedure TUMLCStringSequence.ReleaseIterator
  (var AIterator: TUMLCSequenceIterator);
begin
  // ...  as ...
  Self.InternalReleaseIterator(AIterator);
end;

(* TUMLCStringSequenceForwardIterator *)

function TUMLCStringSequenceForwardIterator.Read: shortstring;
var P: ^shortstring;
begin
  P := Self.InternalRead();
  Result := P^;
end;

(* TUMLCStringSequenceBackwardIterator *)

function TUMLCStringSequenceBackwardIterator.Read: shortstring;
var P: ^shortstring;
begin
  P := Self.InternalRead();
  Result := P^;
end;


end.

