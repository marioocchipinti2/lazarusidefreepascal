readme.txt
==========

The "umlcseqs" folder contains the UMLCat set of libraries for the FreePascal &
Lazarus Programming Framework.

This package contains classes to implement Sequential Algebra items.

