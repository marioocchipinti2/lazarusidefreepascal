unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcbooleans,
  umlcsequences,
  umlcsimplesequences,
  umlcstdseqs,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnClear: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnlPanel: TPanel;
    sbStatusBar: TStatusBar;
    procedure btnClearClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function PrepareSequence
      (): TUMLCDoubleSequence;

    procedure DisplaySequence
      (const SeqName: string;
       const ASequence: TUMLCDoubleSequence);

    procedure InvertDemo();
  end;


  //TFilterPredicateFunctor =
  function NonPiNumber
    (const AItem: Pointer): Boolean;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

//TFilterPredicateFunctor =
function NonPiNumber
  (const AItem: Pointer): Boolean;
var ADblItem: PDouble;
begin
  ADblItem :=
    PDouble(AItem);
  Result :=
    ((ADblItem^ < 3.14159) or (ADblItem^ > 3.14161));
end;

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

function Tfrmmain.PrepareSequence
  (): TUMLCDoubleSequence;
begin
  Result :=
    TUMLCDoubleSequence.Create;

  Result.Insert(3.1416);
  Result.Insert(2);
  Result.Insert(4);
  Result.Insert(0.0);
  Result.Insert(8.2);
  Result.Insert(3.1416);
  Result.Insert(2);
  Result.Insert(99.99);
  Result.Insert(99.99);
  Result.Insert(0.0);
end;

procedure Tfrmmain.DisplaySequence
  (const SeqName: string;
   const ASequence: TUMLCDoubleSequence);
var AIterator: TUMLCSequenceIterator;
    AItem: Double; S: string;
begin
  AIterator :=
    ASequence.GenerateIterator();

  S := SeqName;
  S := S + ': ';
  S := S + '[';

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCDoubleSequenceForwardIterator).Read();

    S := S + FloatToStr(AItem);
    S := S + ', ';

    AIterator.MoveNext();
  end;

  S := S + ']';
  mmConsole.Lines.Add(S);

  ASequence.ReleaseIterator(AIterator);
end;

procedure Tfrmmain.InvertDemo();
var A, C: TUMLCDoubleSequence;
begin
  mmConsole.Lines.Add('Sequential Algebra');
  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('Filter Sequence Demo');
  mmConsole.Lines.Add('');

  A :=
    Self.PrepareSequence();

  mmConsole.Lines.Add('Before:');
  mmConsole.Lines.Add('');

  Self.DisplaySequence('A', A);

  mmConsole.Lines.Add('');

  mmConsole.Lines.Add('After:');
  mmConsole.Lines.Add('');

  C :=
   (FilterByCopy(A, @NonPiNumber) as TUMLCDoubleSequence);

  Self.DisplaySequence('A', A);
  Self.DisplaySequence('C', C);

  C.Free;
  A.Free;
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
begin
  Self.InvertDemo();
end;

procedure Tfrmmain.btnClearClick(Sender: TObject);
begin
  mmConsole.Lines.Clear;
end;

end.

