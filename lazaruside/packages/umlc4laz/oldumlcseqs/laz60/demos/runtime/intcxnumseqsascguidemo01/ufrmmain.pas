unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcxnums,
  umlcxnumstrs,
  umlccxnumseqs,
  umlcbooleans,
  umlcsequences,
  umlcsimplesequences,
  umlcstdseqs,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnClear: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnlPanel: TPanel;
    sbStatusBar: TStatusBar;
    procedure btnClearClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function PrepareSequence
      (): TUMLCIntegerComplexNumberSequence;

    procedure DisplaySequence
      (const ASeqName: string;
       const ASequence: TUMLCIntegerComplexNumberSequence);

    procedure SortAscDemo();
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

function Tfrmmain.PrepareSequence
  (): TUMLCIntegerComplexNumberSequence;
var AItem: TUMLCIntegerComplexNumber;
begin
  Result := TUMLCIntegerComplexNumberSequence.Create;

  IntCplxClear(AItem);

  IntCplxEncode(AItem, 4, 5);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 0, -1);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 4, 5);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 0, 0);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 9, 10);
  Result.Insert(AItem);

  IntCplxEncode(AItem, -1, 0);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 0, 0);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 9, 10);
  Result.Insert(AItem);

  IntCplxEncode(AItem, -1, -1);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 0, 0);
  Result.Insert(AItem);
end;

procedure Tfrmmain.DisplaySequence
  (const ASeqName: string;
   const ASequence: TUMLCIntegerComplexNumberSequence);
var AIterator: TUMLCSequenceIterator;
    AIterator2: TUMLCIntegerComplexNumberSequenceForwardIterator;
    AItem: TUMLCIntegerComplexNumber;
    S, C: string;
begin
  AIterator :=
    ASequence.GenerateIterator();

  S := ASeqName;
  S := S + ': ';
  S := S + '[';

  IntCplxClear(AItem);

  while (not AIterator.IsDone()) do
  begin
    AIterator2 :=
      (AIterator as TUMLCIntegerComplexNumberSequenceForwardIterator);
    AIterator2.Read(AItem);

    IntCplxToStr(AItem, C);

    S := S + C;
    S := S + ', ';

    AIterator.MoveNext();
  end;

  S := S + ']';
  mmConsole.Lines.Add(S);

  ASequence.ReleaseIterator(AIterator);
end;

procedure Tfrmmain.SortAscDemo();
var A, C: TUMLCIntegerComplexNumberSequence;
begin
  mmConsole.Lines.Add('Sequential Algebra');
  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('Sort Ascendant Sequence Demo');
  mmConsole.Lines.Add('');

  A := Self.PrepareSequence();

  mmConsole.Lines.Add('Before:');
  mmConsole.Lines.Add('');

  Self.DisplaySequence('A', A);

  mmConsole.Lines.Add('');

  mmConsole.Lines.Add('After:');
  mmConsole.Lines.Add('');

  C :=
   (SortAscByCopy(A) as TUMLCIntegerComplexNumberSequence);

  Self.DisplaySequence('A', A);
  Self.DisplaySequence('C', C);

  C.Free;
  A.Free;
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
begin
  Self.SortAscDemo();
end;

procedure Tfrmmain.btnClearClick(Sender: TObject);
begin
  mmConsole.Lines.Clear;
end;

end.

