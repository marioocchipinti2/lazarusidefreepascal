unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcbooleans,
  umlcsequences,
  umlcsimplesequences,
  umlcstdseqs,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnClear: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnlPanel: TPanel;
    sbStatusBar: TStatusBar;
    procedure btnClearClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function PrepareSequence
      (): TUMLCBooleanSequence;

    procedure DisplaySequence
      (const SeqName: string;
       const ASequence: TUMLCBooleanSequence);

    procedure ScalarCountDemo();
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

function Tfrmmain.PrepareSequence
  (): TUMLCBooleanSequence;
begin
  Result := TUMLCBooleanSequence.Create;

  Result.Insert(true);
  Result.Insert(false);
  Result.Insert(false);
  Result.Insert(true);
  Result.Insert(true);
  Result.Insert(true);
  Result.Insert(false);
  Result.Insert(false);
  Result.Insert(false);
  Result.Insert(true);
  Result.Insert(false);
end;

procedure Tfrmmain.DisplaySequence
  (const SeqName: string;
   const ASequence: TUMLCBooleanSequence);
var AIterator: TUMLCSequenceIterator;
    AItem: Boolean; S: string;
begin
  AIterator :=
    ASequence.GenerateIterator();

  S := SeqName;
  S := S + ': ';
  S := S + '[';

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCBooleanSequenceForwardIterator).Read();

    S := S + umlcbooleans.BoolToStr(AItem);
    S := S + ', ';

    AIterator.MoveNext();
  end;

  S := S + ']';
  mmConsole.Lines.Add(S);

  ASequence.ReleaseIterator(AIterator);
end;

procedure Tfrmmain.ScalarCountDemo();
var A: TUMLCBooleanSequence;
    ACount: LongInt; S: string;
begin
  mmConsole.Lines.Add('Sequential Algebra');
  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('Scalar Count Sequence Demo');
  mmConsole.Lines.Add('');

  A := Self.PrepareSequence();

  Self.DisplaySequence('A', A);

  mmConsole.Lines.Add('');

  ACount :=
    A.ScalarCount();

  mmConsole.Lines.Add('Count:');

  S := IntToStr(ACount);
  mmConsole.Lines.Add(S);

  A.Free;
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
begin
  Self.ScalarCountDemo();
end;

procedure Tfrmmain.btnClearClick(Sender: TObject);
begin
  mmConsole.Lines.Clear;
end;

end.

