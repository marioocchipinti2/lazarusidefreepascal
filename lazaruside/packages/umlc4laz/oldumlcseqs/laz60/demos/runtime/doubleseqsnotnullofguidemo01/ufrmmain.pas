unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcsequences,
  umlcsimplesequences,
  umlcstdseqs,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnClear: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnlPanel: TPanel;
    sbStatusBar: TStatusBar;
    procedure btnClearClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function PrepareSequence
      (): TUMLCDoubleSequence;

    procedure DisplaySequence
      (const ASeqName: string;
       const ASequence: TUMLCDoubleSequence);

    procedure NotNullOfDemo();
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

function Tfrmmain.PrepareSequence
  (): TUMLCDoubleSequence;
begin
  Result := TUMLCDoubleSequence.Create;

  Result.Insert(0.0);
  Result.Insert(3.1416);
  Result.Insert(2);
  Result.Insert(4);
  Result.Insert(0.033);
  Result.Insert(8.2);
  Result.Insert(0.0);
  Result.Insert(3.1416);
  Result.Insert(2);
  Result.Insert(0.01);
  Result.Insert(0.0);
  Result.Insert(99.99);
  Result.Insert(99.99);
end;

procedure Tfrmmain.DisplaySequence
  (const ASeqName: string;
   const ASequence: TUMLCDoubleSequence);
var AIterator: TUMLCSequenceIterator;
    AItem: Double; S: string;
begin
  AIterator :=
    ASequence.GenerateIterator();

  S := ASeqName;
  S := S + ': ';
  S := S + '[';

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCDoubleSequenceForwardIterator).Read();

    S := S + FloatToStr(AItem);
    S := S + ', ';

    AIterator.MoveNext();
  end;

  S := S + ']';
  mmConsole.Lines.Add(S);

  ASequence.ReleaseIterator(AIterator);
end;

procedure Tfrmmain.NotNullOfDemo();
var A, C: TUMLCDoubleSequence;
begin
  mmConsole.Lines.Add('Sequential Algebra');
  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('NotNullOf Sequence Demo');
  mmConsole.Lines.Add('');

  A :=
    Self.PrepareSequence();

  mmConsole.Lines.Add('Before:');
  mmConsole.Lines.Add('');

  Self.DisplaySequence('A', A);

  mmConsole.Lines.Add('');

  mmConsole.Lines.Add('After:');
  mmConsole.Lines.Add('');

  C :=
   A.NotNullOf();

  Self.DisplaySequence('A', A);
  Self.DisplaySequence('C', C);

  //C.Free;
  //A.Free;
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
begin
  Self.NotNullOfDemo();
end;

procedure Tfrmmain.btnClearClick(Sender: TObject);
begin
  mmConsole.Lines.Clear;
end;

end.

