unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcsequences,
  umlcsimplesequences,
  umlcstdseqs,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnClear: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnlPanel: TPanel;
    sbStatusBar: TStatusBar;
    procedure btnClearClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function PrepareSequence
      (): TUMLCIntegerSequence;

    procedure DisplaySequence
      (const SeqName: string;
       const Seq: TUMLCIntegerSequence);

    procedure DeclarationDemo();
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

function Tfrmmain.PrepareSequence
  (): TUMLCIntegerSequence;
begin
  Result := TUMLCIntegerSequence.Create;

  Result.Insert(0);
  Result.Insert(2);
  Result.Insert(4);
  Result.Insert(6);
  Result.Insert(8);
  Result.Insert(10);
end;

procedure Tfrmmain.DisplaySequence
  (const SeqName: string;
   const Seq: TUMLCIntegerSequence);
var AIterator: TUMLCSequenceIterator;
    AItem: Integer; S: string;
begin
  AIterator :=
    Seq.GenerateIterator();

  S := SeqName;
  S := S + ': ';
  S := S + '[';

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCIntegerSequenceForwardIterator).Read();

    S := S + IntToStr(AItem);
    S := S + ', ';

    AIterator.MoveNext();
  end;

  S := S + ']';
  mmConsole.Lines.Add(S);

  Seq.ReleaseIterator(AIterator);
end;

procedure Tfrmmain.DeclarationDemo();
var S: TUMLCIntegerSequence;
begin
  mmConsole.Lines.Add('Sequential Algebra');
  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('Declaration Demo');
  mmConsole.Lines.Add('');

  S := Self.PrepareSequence();

  Self.DisplaySequence('S', S);

  S.Free;
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
begin
  Self.DeclarationDemo();
end;

procedure Tfrmmain.btnClearClick(Sender: TObject);
begin
  mmConsole.Lines.Clear;
end;

end.

