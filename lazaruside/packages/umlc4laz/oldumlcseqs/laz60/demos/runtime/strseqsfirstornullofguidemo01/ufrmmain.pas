unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcsequences,
  umlcsimplesequences,
  umlcstdseqs,
  umlcstdstrseqs,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnClear: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnlPanel: TPanel;
    sbStatusBar: TStatusBar;
    procedure btnClearClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function PrepareSequence
      (): TUMLCStringSequence;

    procedure DisplaySequence
      (const ASeqName: string;
       const ASequence: TUMLCStringSequence);

    procedure FirstOrNullOfDemo();
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

function Tfrmmain.PrepareSequence
  (): TUMLCStringSequence;
begin
  Result := TUMLCStringSequence.Create;

  Result.Insert('orange');
  Result.Insert('cherry');
  Result.Insert('banana');
  Result.Insert('watermelon');
  Result.Insert('watermelon');
  Result.Insert('pear');
  Result.Insert('coconut');
  Result.Insert('grapefruit');
  Result.Insert('cherry');
  Result.Insert('kiwi');  
end;

procedure Tfrmmain.DisplaySequence
  (const ASeqName: string;
   const ASequence: TUMLCStringSequence);
var AIterator: TUMLCSequenceIterator;
    AItem: shortstring; S: string;
begin
  AIterator :=
    ASequence.GenerateIterator();

  S := ASeqName;
  S := S + ': ';
  S := S + '[';

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCStringSequenceForwardIterator).Read();

    S := S + #34 + (AItem) + #34;
    S := S + ', ';

    AIterator.MoveNext();
  end;

  S := S + ']';
  mmConsole.Lines.Add(S);

  ASequence.ReleaseIterator(AIterator);
end;

procedure Tfrmmain.FirstOrNullOfDemo();
var A, B, C, D: TUMLCStringSequence;
begin
  mmConsole.Lines.Add('Sequential Algebra');
  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('FirstOrNullOf Sequence Demo');
  mmConsole.Lines.Add('');

  A :=
    Self.PrepareSequence();
  B :=
    TUMLCStringSequence.Create;

  Self.DisplaySequence('A', A);
  Self.DisplaySequence('B', B);
  mmConsole.Lines.Add('');

  C :=
   A.FirstOrNullOf();
  D :=
   B.FirstOrNullOf();

  Self.DisplaySequence('C', C);
  Self.DisplaySequence('D', D);


  //D.Free;
  //C.Free;
  //B.Free;
  //A.Free;
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
begin
  Self.FirstOrNullOfDemo();
end;

procedure Tfrmmain.btnClearClick(Sender: TObject);
begin
  mmConsole.Lines.Clear;
end;

end.

