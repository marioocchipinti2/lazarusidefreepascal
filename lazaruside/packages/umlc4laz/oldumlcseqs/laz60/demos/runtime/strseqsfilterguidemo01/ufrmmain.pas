unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcsequences,
  umlcsimplesequences,
  umlcstdseqs,
  umlcstdstrseqs,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnClear: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnlPanel: TPanel;
    sbStatusBar: TStatusBar;
    procedure btnClearClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function PrepareSequence
      (): TUMLCStringSequence;

    procedure DisplaySequence
      (const ASeqName: string;
       const ASequence: TUMLCStringSequence);

    procedure FilterDemo();
  end;

  //TFilterPredicateFunctor =
  function IsBerry(const AItem: Pointer): Boolean;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

//TFilterPredicateFunctor =
function IsBerry(const AItem: Pointer): Boolean;
var AStrItem: PShortString;
begin
  AStrItem :=
    PShortString(AItem);
  Result :=
    (AStrItem^ = 'cherry');
  if (not Result)
    then
  Result :=
    (AStrItem^ = 'strawberry');
  if (not Result)
    then
  Result :=
    (AStrItem^ = 'blueberry')
end;

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

function Tfrmmain.PrepareSequence
  (): TUMLCStringSequence;
begin
  Result := TUMLCStringSequence.Create;

  Result.Insert('cherry');
  Result.Insert('banana');
  Result.Insert('strawberry');
  Result.Insert('watermelon');
  Result.Insert('watermelon');
  Result.Insert('strawberry');
  Result.Insert('pear');
  Result.Insert('coconut');
  Result.Insert('blueberry');
  Result.Insert('grapefruit');
  Result.Insert('cherry');
end;

procedure Tfrmmain.DisplaySequence
  (const ASeqName: string;
   const ASequence: TUMLCStringSequence);
var AIterator: TUMLCSequenceIterator;
    AItem: shortstring; S: string;
begin
  AIterator :=
    ASequence.GenerateIterator();

  S := ASeqName;
  S := S + ': ';
  S := S + '[';

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCStringSequenceForwardIterator).Read();

    S := S + #39 + (AItem) + #39;
    S := S + ', ';

    AIterator.MoveNext();
  end;

  S := S + ']';
  mmConsole.Lines.Add(S);

  ASequence.ReleaseIterator(AIterator);
end;

procedure Tfrmmain.FilterDemo();
var A, C: TUMLCStringSequence;
begin
  mmConsole.Lines.Add('Sequential Algebra');
  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('Filter Sequence Demo');
  mmConsole.Lines.Add('');

  A :=
    Self.PrepareSequence();

  mmConsole.Lines.Add('Before:');
  mmConsole.Lines.Add('');

  Self.DisplaySequence('A', A);

  mmConsole.Lines.Add('');

  mmConsole.Lines.Add('After:');
  mmConsole.Lines.Add('');

  C :=
   (FilterByCopy(A, @IsBerry) as TUMLCStringSequence);

  Self.DisplaySequence('A', A);
  Self.DisplaySequence('C', C);

  //C.Free;
  //A.Free;
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
begin
  Self.FilterDemo();
end;

procedure Tfrmmain.btnClearClick(Sender: TObject);
begin
  mmConsole.Lines.Clear;
end;

end.

