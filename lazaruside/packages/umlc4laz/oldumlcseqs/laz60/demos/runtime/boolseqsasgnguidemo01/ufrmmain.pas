unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcbooleans,
  umlcsequences,
  umlcsimplesequences,
  umlcstdseqs,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnClear: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnlPanel: TPanel;
    sbStatusBar: TStatusBar;
    procedure btnClearClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function PrepareSequence
      (): TUMLCBooleanSequence;

    procedure DisplaySequence
      (const SeqName: string;
       const Seq: TUMLCBooleanSequence);

    procedure AssignmentDemo();
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

function Tfrmmain.PrepareSequence
  (): TUMLCBooleanSequence;
begin
  Result := TUMLCBooleanSequence.Create;

  Result.Insert(true);
  Result.Insert(false);
  Result.Insert(false);
  Result.Insert(true);
  Result.Insert(true);
  Result.Insert(true);
  Result.Insert(false);
  Result.Insert(false);
  Result.Insert(false);
end;

procedure Tfrmmain.DisplaySequence
  (const SeqName: string;
   const Seq: TUMLCBooleanSequence);
var AIterator: TUMLCSequenceIterator;
    AItem: Boolean; S: string;
begin
  AIterator :=
    Seq.GenerateIterator();

  S := SeqName;
  S := S + ': ';
  S := S + '[';

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCBooleanSequenceForwardIterator).Read();

    S := S + umlcbooleans.BoolToStr(AItem);
    S := S + ', ';

    AIterator.MoveNext();
  end;

  S := S + ']';
  mmConsole.Lines.Add(S);

  Seq.ReleaseIterator(AIterator);
end;

procedure Tfrmmain.AssignmentDemo();
var A, B: TUMLCBooleanSequence;
begin
  mmConsole.Lines.Add('Sequential Algebra');
  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('Assigment Demo');
  mmConsole.Lines.Add('');

  A := Self.PrepareSequence();
  B := TUMLCBooleanSequence.Create;

  mmConsole.Lines.Add('Before:');
  mmConsole.Lines.Add('');

  Self.DisplaySequence('A', A);
  Self.DisplaySequence('B', B);

  mmConsole.Lines.Add('');

  mmConsole.Lines.Add('After:');
  mmConsole.Lines.Add('');

  B.AssignFrom(A);

  Self.DisplaySequence('A', A);
  Self.DisplaySequence('B', B);

  B.Free;
  A.Free;
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
begin
  Self.AssignmentDemo();
end;

procedure Tfrmmain.btnClearClick(Sender: TObject);
begin
  mmConsole.Lines.Clear;
end;

end.

