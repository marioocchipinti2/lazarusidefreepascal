unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcsequences,
  umlcsimplesequences,
  umlcxnums,
  umlcxnumstrs,
  umlccxnumseqs,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnClear: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnlPanel: TPanel;
    sbStatusBar: TStatusBar;
    procedure btnClearClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function PrepareSequence
      (): TUMLCIntegerComplexNumberSequence;

    procedure DisplaySequence
      (const ASeqName: string;
       const ASequence: TUMLCIntegerComplexNumberSequence);

    procedure DeclarationDemo();
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

function Tfrmmain.PrepareSequence
  (): TUMLCIntegerComplexNumberSequence;
var AItem: TUMLCIntegerComplexNumber;
begin
  Result := TUMLCIntegerComplexNumberSequence.Create;

  IntCplxClear(AItem);

  IntCplxEncode(AItem, 4, 5);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 0, -1);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 4, 5);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 0, 0);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 9, 10);
  Result.Insert(AItem);

  IntCplxEncode(AItem, -1, 0);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 0, 0);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 9, 10);
  Result.Insert(AItem);

  IntCplxEncode(AItem, -1, -1);
  Result.Insert(AItem);

  IntCplxEncode(AItem, 0, 0);
  Result.Insert(AItem);
end;

procedure Tfrmmain.DisplaySequence
  (const ASeqName: string;
   const ASequence: TUMLCIntegerComplexNumberSequence);
var AIterator: TUMLCSequenceIterator;
    AIterator2: TUMLCIntegerComplexNumberSequenceForwardIterator;
    AItem: TUMLCIntegerComplexNumber;
    S, C: string;
begin
  AIterator :=
    ASequence.GenerateIterator();

  S := ASeqName;
  S := S + ': ';
  S := S + '[';

  IntCplxClear(AItem);

  while (not AIterator.IsDone()) do
  begin
    AIterator2 :=
      (AIterator as TUMLCIntegerComplexNumberSequenceForwardIterator);
    AIterator2.Read(AItem);

    IntCplxToStr(AItem, C);

    S := S + C;
    S := S + ', ';

    AIterator.MoveNext();
  end;

  S := S + ']';
  mmConsole.Lines.Add(S);

  ASequence.ReleaseIterator(AIterator);
end;

procedure Tfrmmain.DeclarationDemo();
var S: TUMLCIntegerComplexNumberSequence;
begin
  mmConsole.Lines.Add('Sequential Algebra');
  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('Declaration Demo');
  mmConsole.Lines.Add('');

  S := Self.PrepareSequence();

  Self.DisplaySequence('S', S);

  S.Free;
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
begin
  Self.DeclarationDemo();
end;

procedure Tfrmmain.btnClearClick(Sender: TObject);
begin
  mmConsole.Lines.Clear;
end;

end.

