unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcsequences,
  umlcsimplesequences,
  umlcstdseqs,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnClear: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnlPanel: TPanel;
    sbStatusBar: TStatusBar;
    procedure btnClearClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function PrepareSequenceA
      (): TUMLCIntegerSequence;

    function PrepareSequenceB
      (): TUMLCIntegerSequence;

    procedure DisplaySequence
      (const SeqName: string;
       const Seq: TUMLCIntegerSequence);

    procedure UnionDemo();
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

function Tfrmmain.PrepareSequenceA
  (): TUMLCIntegerSequence;
begin
  Result := TUMLCIntegerSequence.Create;

  Result.Insert(0);
  Result.Insert(2);
  Result.Insert(4);
  Result.Insert(6);
  Result.Insert(8);
  Result.Insert(10);
end;

function Tfrmmain.PrepareSequenceB
  (): TUMLCIntegerSequence;
begin
  Result := TUMLCIntegerSequence.Create;

  Result.Insert(0);
  Result.Insert(1);
  Result.Insert(3);
  Result.Insert(5);
  Result.Insert(7);
  Result.Insert(9);
  Result.Insert(10);
end;

procedure Tfrmmain.DisplaySequence
  (const SeqName: string;
   const Seq: TUMLCIntegerSequence);
var AIterator: TUMLCSequenceIterator;
    AItem: Integer; S: string;
begin
  AIterator :=
    Seq.GenerateIterator();

  S := SeqName;
  S := S + ': ';
  S := S + '[';

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCIntegerSequenceForwardIterator).Read();

    S := S + IntToStr(AItem);
    S := S + ', ';

    AIterator.MoveNext();
  end;

  S := S + ']';
  mmConsole.Lines.Add(S);

  Seq.ReleaseIterator(AIterator);
 end;

procedure Tfrmmain.UnionDemo();
var A, B, C: TUMLCIntegerSequence;
begin
  mmConsole.Lines.Add('Sequential Algebra');
  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('Union Demo');
  mmConsole.Lines.Add('');

  A := Self.PrepareSequenceA();
  B := Self.PrepareSequenceB();

  mmConsole.Lines.Add('Before:');
  mmConsole.Lines.Add('');

  Self.DisplaySequence('A', A);
  Self.DisplaySequence('B', B);

  mmConsole.Lines.Add('');

  mmConsole.Lines.Add('After:');
  mmConsole.Lines.Add('');

  C :=
    (umlcsequences.UnionByCopy(A, B) as TUMLCIntegerSequence);

  Self.DisplaySequence('A', A);
  Self.DisplaySequence('B', B);
  Self.DisplaySequence('C', C);

  B.Free;
  A.Free;
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
begin
  Self.UnionDemo();
end;

procedure Tfrmmain.btnClearClick(Sender: TObject);
begin
  mmConsole.Lines.Clear;
end;

end.

