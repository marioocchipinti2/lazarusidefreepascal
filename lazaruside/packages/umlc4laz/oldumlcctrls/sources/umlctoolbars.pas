unit umlctoolbars;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  GraphType,
  Graphics, Controls,
  contnrs,
  umlcpanels, umlcspeedbtns,
  dummy;

type
  TButtonIconSize =
  (
    biTiny,      // 16
    biSmall,     // 24
    biMedium,    // 32
    biLarge,     // 64
    biExtraLarge // 128
  );

  // ...
  TCustomUMLCToolBarGroup = class;
  // ...
  TUMLCToolBarRow = class;
  // ...
  TUMLCToolBar = class;

(* IUMLCToolBarControl *)

  IUMLCToolBarControl = interface
    ['{8494B82A-D12A-C248-8446-140A84A1C1BA}']

    function readToolBarRow(): TUMLCToolBarRow;
    function readToolBarGroup(): TCustomUMLCToolBarGroup;
  end;

(* TUMLCToolButton *)

  TUMLCToolButton = class(TCustomUMLCSpeedButton, IUMLCToolBarControl)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FToolBarRow:   TUMLCToolBarRow;
    FToolBarGroup: TCustomUMLCToolBarGroup;
  protected
    (* Protected declarations *)

    function readToolBarRow(): TUMLCToolBarRow;
    function readToolBarGroup(): TCustomUMLCToolBarGroup;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    procedure AssignToolBarRow(const AValue: TUMLCToolBarRow);
    procedure AssignToolBarGroup(const AValue: TCustomUMLCToolBarGroup);
  end;

(* TUMLCToolBar *)

  TUMLCToolBar = class(TCustomUMLCPanel)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FToolControls: TFPObjectList;
    FCounter:     Integer;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    function InsertToolButton(): TUMLCToolButton;

    function ControlCount(): Integer;
    function ControlAt(const AIndex: Integer): TUMLCToolButton;
  end;

(* TUMLCToolBarRow *)

  TUMLCToolBarRow = class(TCustomUMLCPanel)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FToolBars: TFPObjectList;
    FCounter:  Integer;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    function InsertToolBar(): TUMLCToolBar;

    function ToolBarCount(): Integer;
    function ToolBarAt(const AIndex: Integer): TUMLCToolBar;
  end;

(* TCustomUMLCToolBarGroup *)

  TCustomUMLCToolBarGroup = class(TCustomUMLCPanel)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FToolBarRows: TFPObjectList;

    FCounter: Integer;
  protected
    (* Protected declarations *)

    procedure ActivateFirst(); override;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
    (* Public declarations *)

    function InsertToolBarRow(): TUMLCToolBarRow;

    function ToolBarRowCount(): Integer;
    function ToolBarRowAt(const AIndex: Integer): TUMLCToolBarRow;
  end;

(* TUMLCToolBarGroup *)

  TUMLCToolBarGroup = class(TCustomUMLCToolBarGroup)
  published
    (* Published declarations *)

    (* TCustomUMLCPanel: *)

{$IFDEF MSWINDOWS}
    property DragCursor;
    property DragKind;
    {$IFDEF DELPHI}
    property Ctl3D;
    property ParentCtl3D;
    {$ENDIF}

    property OnEndDock;
    property OnStartDock;
{$ENDIF}

    (* TCustomPanel: *)

    property Align;
    property Anchors;
    property Constraints;
    property DragMode;
    property Enabled;
    property ParentShowHint;
    property PopupMenu;
    property TabOrder;
    property TabStop;
    property Visible;

    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnResize;
    property OnStartDrag;

    (* TCustomUMLCToolBarGroup: *)

  end;

implementation

(* TUMLCToolButton *)

function TUMLCToolButton.readToolBarRow(): TUMLCToolBarRow;
begin
  Result := FToolBarRow;
end;

function TUMLCToolButton.readToolBarGroup(): TCustomUMLCToolBarGroup;
begin
  Result := FToolBarGroup;
end;

constructor TUMLCToolButton.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FToolBarRow   := nil;
  FToolBarGroup := nil;

  Self.Flat  := true;
  Self.Width := 100;
end;

destructor TUMLCToolButton.Destroy();
begin
  inherited Destroy();
end;

procedure TUMLCToolButton.AssignToolBarRow
  (const AValue: TUMLCToolBarRow);
begin
  FToolBarRow := AValue;
end;

procedure TUMLCToolButton.AssignToolBarGroup
  (const AValue: TCustomUMLCToolBarGroup);
begin
  FToolBarGroup := AValue;
end;

(* TUMLCToolBar *)

constructor TUMLCToolBar.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Self.BorderWidth := 0;
  Self.BevelInner  := TGraphicsBevelCut.bvNone;
  Self.BevelOuter  := TGraphicsBevelCut.bvNone;
  Self.Height      := 21;
  Self.ParentColor := false;
  Self.Color := Graphics.clCream;

  FToolControls := TFPObjectList.Create(false);
  FCounter := 0;
end;

destructor TUMLCToolBar.Destroy();
begin
  FToolControls.Free();
  inherited Destroy();
end;

function TUMLCToolBar.InsertToolButton(): TUMLCToolButton;
begin
  Result := nil;

  Result := TUMLCToolButton.Create(Self);
  Result.Top   := 0;
  Result.Left  := 0;
  Result.Align := TAlign.alLeft;

  Result.Name  := 'ToolButton' + IntToStr(FCounter);
  Inc(FCounter);

  Self.InsertControl(Result);
  Result.Caption := '';

  FToolControls.Add(Result);
end;

function TUMLCToolBar.ControlCount(): Integer;
begin
  Result := FToolControls.Count;
end;

function TUMLCToolBar.ControlAt(const AIndex: Integer): TUMLCToolButton;
var ACount: Integer;
begin
  Result := nil;
  ACount := FToolControls.Count;
  if ((AIndex >= 0) and (AIndex <= ACount)) then
  begin
    Result := TUMLCToolButton(FToolControls[AIndex]);
  end;
end;

(* TUMLCToolBarRow *)

constructor TUMLCToolBarRow.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Self.BorderWidth := 0;
  Self.BevelInner  := TGraphicsBevelCut.bvNone;
  Self.BevelOuter  := TGraphicsBevelCut.bvNone;
  Self.Height      := 21;
  Self.ParentColor := false;
  Self.Color := Graphics.clLtGray;

  FToolBars := TFPObjectList.Create(false);
  FCounter := 0;
end;

destructor TUMLCToolBarRow.Destroy();
begin
  FToolBars.Free();
  inherited Destroy();
end;

function TUMLCToolBarRow.InsertToolBar(): TUMLCToolBar;
begin
  Result := nil;

  Result := TUMLCToolBar.Create(Self);
  Result.Top   := 0;
  Result.Left  := 0;
  Result.Align := TAlign.alLeft;

  Result.Name  := 'ToolBar' + IntToStr(FCounter);
  Inc(FCounter);

  Self.InsertControl(Result);
  Result.Caption := '';

  FToolBars.Add(Result);
end;

function TUMLCToolBarRow.ToolBarCount(): Integer;
begin
  Result := FToolBars.Count;
end;

function TUMLCToolBarRow.ToolBarAt
  (const AIndex: Integer): TUMLCToolBar;
var ACount: Integer;
begin
  Result := nil;
  ACount := FToolBars.Count;
  if ((AIndex >= 0) and (AIndex <= ACount)) then
  begin
    Result := TUMLCToolBar(FToolBars[AIndex]);
  end;
end;

(* TCustomUMLCToolBarGroup *)

procedure TCustomUMLCToolBarGroup.ActivateFirst();
var AToolBar: TUMLCToolBarRow;
begin
  //inherited ActivateFirst();
  AToolBar := InsertToolBarRow();
end;

constructor TCustomUMLCToolBarGroup.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Self.Color := Graphics.clDkGray;
  //Self.BorderWidth := 0;
  //Self.BevelInner  := TGraphicsBevelCut.bvNone;
  //Self.BevelOuter  := TGraphicsBevelCut.bvNone;

  FToolBarRows := TFPObjectList.Create(false);

  FCounter := 0;
end;

destructor TCustomUMLCToolBarGroup.Destroy();
begin
  FToolBarRows.Free();
  inherited Destroy();
end;

function TCustomUMLCToolBarGroup.InsertToolBarRow(): TUMLCToolBarRow;
begin
  Result := nil;

  Result := TUMLCToolBarRow.Create(Self);
  Result.Top   := 0;
  Result.Left  := 0;
  Result.Align := TAlign.alTop;

  Result.Name  := 'ToolBarRow' + IntToStr(FCounter);
  Inc(FCounter);

  Self.InsertControl(Result);
  Result.Caption := '';

  FToolBarRows.Add(Result);
end;

function TCustomUMLCToolBarGroup.ToolBarRowCount(): Integer;
begin
  Result := FToolBarRows.Count;
end;

function TCustomUMLCToolBarGroup.ToolBarRowAt
  (const AIndex: Integer): TUMLCToolBarRow;
var ACount: Integer;
begin
  Result := nil;
  ACount := FToolBarRows.Count;
  if ((AIndex >= 0) and (AIndex <= ACount)) then
  begin
    Result := TUMLCToolBarRow(FToolBarRows[AIndex]);
  end;
end;

end.

