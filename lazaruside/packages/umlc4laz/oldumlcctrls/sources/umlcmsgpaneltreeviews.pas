(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmsgpaneltreeviews;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Forms,
  umlcguids,
  umlcguidstrs,
  umlctreestates,
  umlctreenodes,
  umlctreecntrs,
  umlcmsgtypes,
  umlcmsgtreecntrs,
  umlccontroltreenodes,
  umlcpaneltreeviews,
  dummy;

(**
 ** This unit contains several classes.
 **
 ** The main class is a descendant ot "TCustomUMLCPanelTreeView",
 ** a panel that encapsulates a treeview control,
 ** to add some specific features,
 ** like the "Decorator" Software Design Pattern.
 **
 ** This control, supports the subscriber part,
 ** of the Publisher-Subscriber Design Pattern,
 ** ("Observer" Pattern),
 ** implemented with messages.
 **
 ** There is a non-visual treecontainer, that is the publisher complement.
 **)

type

(* TUMLCMsgPanelTreeViewNode *)

  TUMLCMsgPanelTreeViewNode = class(TUMLCPanelTreeviewNode)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FContainerTreeNode: TUMLCContainerTreeNode;
  protected
    (* Protected declarations *)

    (* Accesors declarations *)

    function getContainerTreeNode(): TUMLCContainerTreeNode; virtual;

    procedure setContainerTreeNode(const AValue: TUMLCContainerTreeNode); virtual;
  public
    (* Protected Friend declarations *)

    procedure InternalExplore(); override;
  public
    (* Public declarations *)

    (* Never published declarations *)

    property ContainerTreeNode: TUMLCContainerTreeNode
      read FContainerTreeNode write FContainerTreeNode;
  end;

(* TUMLCMsgPanelTreeViewCollection *)

  TUMLCMsgPanelTreeViewCollection = class(TUMLCPanelTreeviewCollection)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;
  public
    (* Public declarations *)

    function InsertRoot(): TUMLCTreeNode; override;
    procedure DropRoot(); override;
  end;

(* TCustomUMLCMsgPanelTreeView *)

  TCustomUMLCMsgPanelTreeView =
    class(
      TCustomUMLCPanelTreeView,
      IUMLCMessageClient,
      IUMLCSingleServerMessageClient
      )
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FTreeContainer: TCustomUMLCMsgTreeContainer;
  protected
    (* Protected declarations *)

    function getServer(): IUMLCMessageServer;
    function getTreeContainer(): TCustomUMLCMsgTreeContainer;

    procedure setServer(const AValue: IUMLCMessageServer);
    procedure setTreeContainer(const AValue: TCustomUMLCMsgTreeContainer);
  protected
    (* Protected declarations *)

    (* Message Handlers declarations *)

    procedure ServerAssignHandler
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure ServerDeassignHandler
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure TreeNodeBeforeInsertHandler
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure TreeNodeAfterInsertHandler
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure TreeNodeBeforeRemoveHandler
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure TreeNodeAfterRemoveHandler
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure TreeNodeBeforeChangeTextHandler
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure TreeNodeAfterChangeTextHandler
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure TreeNodeBeforeChangeSelectedHandler
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure TreeNodeAfterChangeSelectedHandler
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
  protected
    (* Protected declarations *)

    FDestRootNode: TUMLCMsgPanelTreeViewNode;

    FMsgHandlerList : TUMLCMessageHandlerList;
  protected
    (* Protected declarations *)

    function CreateCollectionByClass(): TUMLCPanelTreeviewCollection; override;

    function MatchesNode
      (const ANode: TUMLCTreeNode; const AParam: pointer): Boolean;
    function NodeOf
      (const ASourceNode: TUMLCContainerTreeNode): TUMLCMsgPanelTreeViewNode;

    procedure InsertRootNode
      (const AContainerRootNode: TUMLCContainerTreeNode); virtual;
    procedure InsertNode
      (const AContainerParentNode: TUMLCContainerTreeNode); virtual;

    procedure AssignHandlers(); virtual;

    procedure AnswerMessage
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure Notification
      (AComponent: TComponent; Operation: TOperation); override;
  public
    (* Public declarations *)

    function AsComponent(): TComponent;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    property TreeContainer: TCustomUMLCMsgTreeContainer
      read getTreeContainer write setTreeContainer;

    property Server: IUMLCMessageServer
      read getServer write setServer;
  end;

(* TUMLCMsgPanelTreeView *)

  TUMLCMsgPanelTreeView = class(TCustomUMLCMsgPanelTreeView)
  published
    (* published declarations *)

    (* TPanel: *)

    property Align;
    property Alignment;
    property Anchors;
    property AutoSize;
    property BorderSpacing;
    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property BidiMode;
    property BorderWidth;
    property BorderStyle;
    property Caption;
    property ChildSizing;
    property ClientHeight;
    property ClientWidth;
    property Color;
    property Constraints;
    property DockSite;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property FullRepaint;
    property ParentBidiMode;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property UseDockManager;
    property Visible;

    property OnClick;
    property OnContextPopup;
    property OnDockDrop;
    property OnDockOver;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGetSiteInfo;
    property OnGetDockCaption;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property OnStartDock;
    property OnStartDrag;
    property OnUnDock;

    //property ShowGrid;
    //property Text;

    (* TCustomUMLCPanel: *)

    property Activated;
    property ReadOnly;

    property OnChange;

    (* TCustomUMLCPanelTreeView: *)

    property AutoExpand;
    // "HideSelection" -> "HideFocus"
    property HideFocus;
    property ShowRoot;
    property Images;
    property Items;
    property States;
    property RowFocus;

    property BeforeExplore;
    property BeforeCollapse;
    property BeforeExpand;
    property BeforeEmpty;

    property OnExplore;

    property AfterExplore;
    property AfterCollapse;
    property AfterExpand;
    property AfterEmpty;

    (* TCustomUMLCMsgPanelTreeView: *)

    property TreeContainer;
    property Server;
  end;

implementation

(* TUMLCMsgPanelTreeViewNode *)

function TUMLCMsgPanelTreeViewNode.getContainerTreeNode(): TUMLCContainerTreeNode;
begin
  Result := FContainerTreeNode;
end;

procedure TUMLCMsgPanelTreeViewNode.setContainerTreeNode
  (const AValue: TUMLCContainerTreeNode);
begin
  FContainerTreeNode := AValue;
end;

procedure TUMLCMsgPanelTreeViewNode.InternalExplore();
var AContainerParentNode, AContainerChildNode: TUMLCContainerTreeNode;
    APanelChildNode: TUMLCMsgPanelTreeViewNode;
    AText: string;
    EachIndex, LastIndex: Integer;
begin
  // in this step, the control node, must not have subitems

  AContainerParentNode := Self.ContainerTreeNode;

  LastIndex := (AContainerParentNode.Count() - 1);
  for EachIndex := 0 to LastIndex do
  begin
    AContainerChildNode :=
      TUMLCContainerTreeNode(AContainerParentNode.List().Items[EachIndex]);

    APanelChildNode :=
      (Self.Insert() as TUMLCMsgPanelTreeViewNode);
    Application.ProcessMessages();

    AText := AContainerChildNode.Text;
    APanelChildNode.Text := AText;

    APanelChildNode.ContainerTreeNode := AContainerChildNode;
  end;
end;

(* TUMLCMsgPanelTreeViewCollection *)

function TUMLCMsgPanelTreeViewCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCMsgPanelTreeViewNode.Create();
  Result.DoCreate();
end;

function TUMLCMsgPanelTreeViewCollection.InsertRoot(): TUMLCTreeNode;
begin
  Result := inherited InsertRoot();
  Application.ProcessMessages();
  // Goal: Inserts the root item to the collection.
  // Objetivo: Inserta el elemento raiz en la coleccion.
end;

procedure TUMLCMsgPanelTreeViewCollection.DropRoot();
begin
  inherited DropRoot();
  Application.ProcessMessages();
end;

(* TCustomUMLCMsgPanelTreeView *)

function TCustomUMLCMsgPanelTreeView.getServer(): IUMLCMessageServer;
begin
  Result := IUMLCMessageServer(FTreeContainer);
  // Goal: "Server" property set method.
  // Objetivo: Metodo lectura propiedad "Server".
end;

function TCustomUMLCMsgPanelTreeView.getTreeContainer(): TCustomUMLCMsgTreeContainer;
begin
  Result := FTreeContainer
  // Goal: "TreeContainer" property get method .
  // Objetivo: Metodo lectura para propiedad "TreeContainer".
end;

procedure TCustomUMLCMsgPanelTreeView.setServer(const AValue: IUMLCMessageServer);
begin
  if (AValue = nil) then
  begin
    FTreeContainer := nil;
  end else
  begin
    FTreeContainer := TCustomUMLCMsgTreeContainer(AValue.AsComponent());
  end;
  // Goal: "Server" property set method.
  // Objetivo: Metodo escritura propiedad "Server".
end;

procedure TCustomUMLCMsgPanelTreeView.setTreeContainer
  (const AValue: TCustomUMLCMsgTreeContainer);
begin
  FTreeContainer := AValue;
  // Goal: "TreeContainer" property set method .
  // Objetivo: Metodo escritura para propiedad "TreeContainer"
end;

procedure TCustomUMLCMsgPanelTreeView.ServerAssignHandler
  (const AMsgRec: TUMLCMessageParamsRecord);
var ASourceRootNode: TUMLCContainerTreeNode;
  AText: string;
begin
  // reassign data server
  FTreeContainer := TCustomUMLCMsgTreeContainer(AMsgRec.Sender);

  // clear previous items, if any
  Self.Items.DropRoot();

  // match root nodes
  ASourceRootNode := (FTreeContainer.Items.Root() as TUMLCContainerTreeNode);
  if (ASourceRootNode <> nil) then
  begin
    FDestRootNode := (Self.Items.InsertRoot() as TUMLCMsgPanelTreeViewNode);

    AText := ASourceRootNode.Text;
    FDestRootNode.Text := AText;

    FDestRootNode.ContainerTreeNode := ASourceRootNode;
  end;
end;

procedure TCustomUMLCMsgPanelTreeView.ServerDeassignHandler
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  // clear previous items, if any
  Self.Items.DropRoot();

  // reassign data server
  FTreeContainer := nil;
end;

procedure TCustomUMLCMsgPanelTreeView.TreeNodeBeforeInsertHandler
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCMsgPanelTreeView.TreeNodeAfterInsertHandler
  (const AMsgRec: TUMLCMessageParamsRecord);
var AContainerParentNode, AContainerNewNode: TUMLCContainerTreeNode;
begin
  AContainerParentNode := (AMsgRec.Sender as TUMLCContainerTreeNode);
  AContainerNewNode    := TUMLCContainerTreeNode(AMsgRec.Param);
  if (AContainerNewNode <> nil) then
  begin
    if (AContainerNewNode.IsRoot()) then
    begin
      InsertRootNode(AContainerNewNode);
    end else
    begin
      InsertNode(AContainerParentNode);
    end;
  end;
end;

procedure TCustomUMLCMsgPanelTreeView.TreeNodeBeforeRemoveHandler
  (const AMsgRec: TUMLCMessageParamsRecord);
var AContainerNode: TUMLCContainerTreeNode;
    ATreeviewNode:  TUMLCMsgPanelTreeViewNode;
begin
  AContainerNode := (AMsgRec.Sender as TUMLCContainerTreeNode);

  // find the node from treeview that references the given parent node,
  // from the container
  ATreeviewNode := NodeOf(AContainerNode);
  if (ATreeviewNode <> nil) then
  begin
    ATreeviewNode.Remove();
  end;
end;

procedure TCustomUMLCMsgPanelTreeView.TreeNodeAfterRemoveHandler
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCMsgPanelTreeView.TreeNodeBeforeChangeTextHandler
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCMsgPanelTreeView.TreeNodeAfterChangeTextHandler
  (const AMsgRec: TUMLCMessageParamsRecord);
var AContainerNode: TUMLCContainerTreeNode;
    ATreeviewNode:  TUMLCMsgPanelTreeViewNode;
var AText: string;
begin
  AContainerNode := (AMsgRec.Sender as TUMLCContainerTreeNode);

  // find the node from treeview that references the given parent node,
  // from the container
  ATreeviewNode := NodeOf(AContainerNode);
  if (ATreeviewNode <> nil) then
  begin
    AText := PString(AMsgRec.Param)^;
    ATreeviewNode.Text := AText;
  end;
end;

procedure TCustomUMLCMsgPanelTreeView.TreeNodeBeforeChangeSelectedHandler
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  Self.DoNothing();
end;

procedure TCustomUMLCMsgPanelTreeView.TreeNodeAfterChangeSelectedHandler
  (const AMsgRec: TUMLCMessageParamsRecord);
var ANode: TUMLCContainerTreeNode;
    APanelNode: TUMLCMsgPanelTreeViewNode;
var ASelected: Boolean;
begin
  ANode := (AMsgRec.Sender as TUMLCContainerTreeNode);

  // find the node from treeview that references the given parent node,
  // from the container
  APanelNode := NodeOf(ANode);
  if (APanelNode <> nil) then
  begin
    ASelected := PBoolean(AMsgRec.Param)^;
    APanelNode.Selected := ASelected;
  end;
end;

function TCustomUMLCMsgPanelTreeView.CreateCollectionByClass(): TUMLCPanelTreeviewCollection;
begin
  Result := TUMLCMsgPanelTreeViewCollection.Create();
  Result.DoCreate();
  // Goal: Create inheretable (polimorphic) collection.
  // Objetivo: Crear coleccion heredable (polimorfica).
end;

function TCustomUMLCMsgPanelTreeView.MatchesNode
  (const ANode: TUMLCTreeNode; const AParam: pointer): Boolean;
var PanelNode: TUMLCMsgPanelTreeViewNode;
    ThisNode: TUMLCContainerTreeNode;
begin
  PanelNode := (ANode as TUMLCMsgPanelTreeViewNode);
  ThisNode  := TUMLCContainerTreeNode(AParam);
  Result    := (PanelNode.ContainerTreeNode = ThisNode);
end;

function TCustomUMLCMsgPanelTreeView.NodeOf
  (const ASourceNode: TUMLCContainerTreeNode): TUMLCMsgPanelTreeViewNode;
var ARootNode: TUMLCMsgPanelTreeViewNode;
begin
  Result := nil;

  ARootNode := TUMLCMsgPanelTreeViewNode(Self.Items.Root());
  if (ARootNode <> nil) then
  begin
    Result :=
      TUMLCMsgPanelTreeViewNode(ARootNode.FirstThat
        (@MatchesNode, ASourceNode));
  end;
end;

procedure TCustomUMLCMsgPanelTreeView.InsertRootNode
  (const AContainerRootNode: TUMLCContainerTreeNode);
var ATreeviewRootNode: TUMLCMsgPanelTreeViewNode;
    AText: string;
begin
  // remove previous root if any
  Self.Items.DropRoot();

  // match root nodes
  ATreeviewRootNode := (Self.Items.InsertRoot() as TUMLCMsgPanelTreeViewNode);

  AText := AContainerRootNode.Text;
  ATreeviewRootNode.Text := AText;

  ATreeviewRootNode.ContainerTreeNode := AContainerRootNode;
end;

procedure TCustomUMLCMsgPanelTreeView.InsertNode
  (const AContainerParentNode: TUMLCContainerTreeNode);
var ATreeviewParentNode: TUMLCMsgPanelTreeViewNode;
begin
  // find the node from treeview that references the given parent node,
  // from the container
  ATreeviewParentNode := NodeOf(AContainerParentNode);
  if (ATreeviewParentNode <> nil) then
  begin
    // remove all subitems
    ATreeviewParentNode.Empty();

    // refresh subitems
    //ATreeviewParentNode.InternalExplore();

    //ATreeviewParentNode.Hide();

    ATreeviewParentNode.UpdateExpand();
  end;
end;

procedure TCustomUMLCMsgPanelTreeView.AssignHandlers();
var AMsgID: TGUID;
begin
  umlcguidstrs.DoubleStrToGUID
    (msgServerAssign, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}ServerAssignHandler);

  umlcguidstrs.DoubleStrToGUID
    (msgServerDeassign, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}ServerDeassignHandler);

  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeBeforeInsert, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}TreeNodeBeforeInsertHandler);

  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterInsert, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}TreeNodeAfterInsertHandler);

  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeBeforeRemove, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}TreeNodeBeforeRemoveHandler);

  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterRemove, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}TreeNodeAfterRemoveHandler);

  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeBeforeChangeText, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}TreeNodeBeforeChangeTextHandler);

  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterChangeText, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}TreeNodeAfterChangeTextHandler);

  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeBeforeChangeSelected, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}TreeNodeBeforeChangeSelectedHandler);

  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterChangeSelected, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}TreeNodeAfterChangeSelectedHandler);
end;

procedure TCustomUMLCMsgPanelTreeView.AnswerMessage
  (const AMsgRec: TUMLCMessageParamsRecord);
var AHandler: TUMLCMsgEventHandler;
begin
  AHandler := FMsgHandlerList.HandlerOf(AMsgRec.Message);
  if (AHandler <> nil) then
  begin
    AHandler(AMsgRec);
  end;
end;

procedure TCustomUMLCMsgPanelTreeView.Notification
  (AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);

  if (FTreeContainer <> nil) then
  begin
    if ((Operation = opRemove) and (AComponent = FTreeContainer)) then
    begin
      FTreeContainer := nil;
    end;
  end;
  // Goal: To detect & update when associated components,
  // have been removed.

  // Objetivo: Detectar y actualizar cuando,
  // los componentes asociados se han removido.
end;

function TCustomUMLCMsgPanelTreeView.AsComponent(): TComponent;
begin
  Result := Self;
end;

constructor TCustomUMLCMsgPanelTreeView.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FMsgHandlerList := TUMLCMessageHandlerList.Create();

  AssignHandlers();
end;

destructor TCustomUMLCMsgPanelTreeView.Destroy();
begin
  FMsgHandlerList.Free();
  FMsgHandlerList := nil;
  inherited Destroy();
end;

end.

