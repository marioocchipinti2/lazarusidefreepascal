(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcformcontrols;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, Controls,
  Forms,
  umlcctrls, umlcpanels, umlcforms, umlcbitbtns,
  dummy;

type

(* TCustomUMLCFormPanelControl *)

  TCustomUMLCFormPanelControl = class(TCustomUMLCPanel)
  private
    (* Private declarations *)

  protected
    (* Protected declarations *)

    FIsActivated: Boolean;

    FOnActivate: TNotifyEvent;
    FOnClose: TCloseEvent;
    FOnCloseQuery: TCloseQueryEvent;
    FOnCreate: TNotifyEvent;
    FOnDeactivate: TNotifyEvent;
    FOnDestroy: TNotifyEvent;
    FOnDropFiles: TDropFilesEvent;
    FOnHelp: THelpEvent;
    FOnHide: TNotifyEvent;
    FOnShortcut: TShortCutEvent;
    FOnShow: TNotifyEvent;
    FOnWindowStateChange: TNotifyEvent;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;

    procedure Show();
    function ShowModal(): Integer; virtual;

    function Active(): Boolean;

    { May Publish events declarations }

    property OnActivate: TNotifyEvent read FOnActivate write FOnActivate;
    property OnClose: TCloseEvent read FOnClose write FOnClose;
    property OnCloseQuery : TCloseQueryEvent
      read FOnCloseQuery write FOnCloseQuery;
    property OnCreate: TNotifyEvent read FOnCreate write FOnCreate;
    property OnDeactivate: TNotifyEvent read FOnDeactivate write FOnDeactivate;
    property OnDestroy: TNotifyEvent read FOnDestroy write FOnDestroy;
    property OnDropFiles: TDropFilesEvent read FOnDropFiles write FOnDropFiles;
    property OnHelp: THelpEvent read FOnHelp write FOnHelp;
    property OnHide: TNotifyEvent read FOnHide write FOnHide;
    property OnResize;
    property OnShortcut: TShortcutEvent read FOnShortcut write FOnShortcut;
    property OnShow: TNotifyEvent read FOnShow write FOnShow;
    property OnWindowStateChange: TNotifyEvent
      read FOnWindowStateChange write FOnWindowStateChange;
  end;

  TCustomUMLCFormPanel = class;

(* TCustomUMLCFormPanelTitleBar *)

  ///<summary>
  ///It's a panel that simulates a menubar.
  ///</summary>
  TCustomUMLCFormPanelTitleBar = class(TCustomUMLCPanel)
  private
    (* Private declarations *)

  protected
    (* Protected declarations *)

    (* fields declarations *)

    FCurrentWindowState: TWindowState;

    // initial window state
    FWindowState:    TWindowState;

    FMenuButton:     TCustomUMLCBitBtn;
    FMinimizeButton: TCustomUMLCBitBtn;
    FMaximizeButton: TCustomUMLCBitBtn;
    FExitButton:     TCustomUMLCBitBtn;
  protected
    (* Protected declarations *)

    (* accesor declarations *)

    function getCurrentWindowState: TWindowState;
    function getWindowState: TWindowState;

    procedure setCurrentWindowState(const Value: TWindowState);
    procedure setWindowState(const Value: TWindowState);
  protected
    (* Protected declarations *)

    (* properties declarations *)

    property CurrentWindowState: TWindowState
      read getCurrentWindowState write setCurrentWindowState;
    property WindowState: TWindowState
      read getWindowState write setWindowState;
  protected
    (* Protected declarations *)

    procedure MinimizeEvent(Sender: TObject);
    procedure MaximizeEvent(Sender: TObject);
    procedure ExitEvent(Sender: TObject);

    procedure createButtons(); dynamic;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    procedure Close();
  end;

(* TCustomUMLCFormPanelMenuBar *)

  ///<summary>
  ///It's a panel that simulates a menubar.
  ///</summary>
  TCustomUMLCFormPanelMenuBar = class(TCustomUMLCPanel)
  private
    (* Private declarations *)

  protected
    (* Protected declarations *)

  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  end;

(* TCustomUMLCFormPanel *)

  ///<summary>
  /// This panel will have a scrollbar and a title bar.
  ///</summary>
  TCustomUMLCFormPanel = class(TCustomUMLCFormPanelControl)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FTitleBar: TCustomUMLCFormPanelTitleBar;
    FMenu:     TCustomUMLCFormPanelMenuBar;
    FTitleBarIsAuto: Boolean;
  protected
    (* Protected declarations *)

    function CreateTitleBarClass(): TCustomUMLCFormPanelTitleBar; dynamic;
  protected
    (* Protected declarations *)

    function getTitleBar(): TCustomUMLCFormPanelTitleBar; dynamic;
    procedure setTitleBar(const Value: TCustomUMLCFormPanelTitleBar); dynamic;

    function getMenu(): TCustomUMLCFormPanelMenuBar; dynamic;
    procedure setMenu(const Value: TCustomUMLCFormPanelMenuBar); dynamic;
  protected
    (* Protected declarations *)

    property TitleBarIsAuto: Boolean
      read FTitleBarIsAuto write FTitleBarIsAuto;

    property TitleBar: TCustomUMLCFormPanelTitleBar
      read getTitleBar write setTitleBar;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;

    (* Can Be Published declarations *)

    property Menu: TCustomUMLCFormPanelMenuBar
      read getMenu write setMenu;
  end;

(* TUMLCFormPanel *)

  TUMLCFormPanel = class(TCustomUMLCFormPanel)
  published
    (* Published declarations *)

    property Menu;

    property OnActivate;
    property OnClose;
    property OnCloseQuery;
    property OnCreate;
    property OnDeactivate;
    property OnDestroy;
    property OnDropFiles;
    property OnHelp;
    property OnHide;
    property OnResize;
    property OnShortcut;
    property OnShow;
    property OnWindowStateChange;
  end;

implementation

(* TCustomUMLCFormPanelTitleBar *)

function TCustomUMLCFormPanelTitleBar.getCurrentWindowState: TWindowState;
begin
  Result := FCurrentWindowState;
end;

function TCustomUMLCFormPanelTitleBar.getWindowState: TWindowState;
begin
  Result := FWindowState;
end;

procedure TCustomUMLCFormPanelTitleBar.setCurrentWindowState
  (const Value: TWindowState);
begin
  FCurrentWindowState := Value;
end;

procedure TCustomUMLCFormPanelTitleBar.setWindowState
  (const Value: TWindowState);
begin
  FWindowState := Value;
end;

procedure TCustomUMLCFormPanelTitleBar.MinimizeEvent(Sender: TObject);
begin
  Self.Close;
end;

procedure TCustomUMLCFormPanelTitleBar.MaximizeEvent(Sender: TObject);
begin
  Self.Close;
end;

procedure TCustomUMLCFormPanelTitleBar.ExitEvent(Sender: TObject);
begin
  Self.Close;
end;

procedure TCustomUMLCFormPanelTitleBar.createButtons();
var AWidth: Integer;
begin
  FMenuButton        := TCustomUMLCBitBtn.Create(Self);
  FMenuButton.Height := (Self.Height - 2);
  FMenuButton.Width  := (Self.Height - 2);
  FMenuButton.Top    := 1;
  AWidth := FMenuButton.Width;
  FMenuButton.Left   := (Self.Width - AWidth);
  FMenuButton.Color:=clGray;
  FMenuButton.Align:= alLeft;
  Self.InsertControl(FMenuButton);

  FExitButton        := TCustomUMLCBitBtn.Create(Self);
  FExitButton.Height := (Self.Height - 2);
  FExitButton.Width  := (Self.Height - 2);
  FExitButton.Top    := 1;
  AWidth := FExitButton.Width;
  FExitButton.Left   := (Self.Width - AWidth);
  FExitButton.Color:=clGray;
  FExitButton.Align:= alRight;
  FExitButton.OnClick:=@ExitEvent;
  Self.InsertControl(FExitButton);

  FMaximizeButton  := TCustomUMLCBitBtn.Create(Self);
  FMaximizeButton.Height:= (Self.Height - 2);
  FMaximizeButton.Width:= (Self.Height - 2);
  FMaximizeButton.Top:=1;
  AWidth := FExitButton.Width;
  FMaximizeButton.Left   := (Self.Width - AWidth);
  FMaximizeButton.Color:=clGray;
  FMaximizeButton.Align:= alRight;
  FMaximizeButton.OnClick:=@MaximizeEvent;
  Self.InsertControl(FMaximizeButton);

  FMinimizeButton  := TCustomUMLCBitBtn.Create(Self);
  FMinimizeButton.Height:= (Self.Height - 2);
  FMinimizeButton.Width:= (Self.Height - 2);
  FMinimizeButton.Top:=1;
  AWidth := FExitButton.Width;
  FMinimizeButton.Left   := (Self.Width - AWidth);
  FMinimizeButton.Color:=clGray;
  FMinimizeButton.Align:= alRight;
  FMinimizeButton.OnClick:=@MinimizeEvent;
  Self.InsertControl(FMinimizeButton);
end;

constructor TCustomUMLCFormPanelTitleBar.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Self.Height:=32;
  Self.Align:= alTop;
  Self.Color:=clBlue;
  Self.BorderStyle:=bsNone;
  createButtons;
end;

destructor TCustomUMLCFormPanelTitleBar.Destroy();
begin
  inherited Destroy();
end;

function FormByControl((*in/bycopy*) Control: TControl): TCustomForm;
var Found: Boolean;
begin
  Result := nil; Found := false;
  while (Assigned(Control) and (not Found)) do
  begin
    Found := (Control is TCustomForm);
    if (not Found)
      then Control := Control.Parent;
  end;
  if (Found)
    then Result := (Control as TCustomForm);
end;

procedure TCustomUMLCFormPanelTitleBar.Close();
var AForm: TCustomForm;
begin
  AForm := FormByControl(Self);
  AForm.Close();
end;

(* TCustomUMLCFormPanelMenuBar *)

constructor TCustomUMLCFormPanelMenuBar.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TCustomUMLCFormPanelMenuBar.Destroy();
begin
  inherited Destroy();
end;

(* TCustomUMLCFormPanelControl *)

constructor TCustomUMLCFormPanelControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  //FKeyValueTypeList := TUMLCKeyValueTypeList.Create;
  Align := alClient;
  FIsActivated := false;
end;

destructor TCustomUMLCFormPanelControl.Destroy();
begin
  //FKeyValueTypeList.Empty();
  //FKeyValueTypeList.Free();
  inherited Destroy();
end;

procedure TCustomUMLCFormPanelControl.Show();
begin
  //
end;

function TCustomUMLCFormPanelControl.ShowModal(): Integer;
begin
  Result := -1;
end;

function TCustomUMLCFormPanelControl.Active(): Boolean;
begin
  Result := FIsActivated;
end;

(* TCustomUMLCFormPanel *)

function TCustomUMLCFormPanel.CreateTitleBarClass(): TCustomUMLCFormPanelTitleBar;
begin
  Result := TCustomUMLCFormPanelTitleBar.Create(Self);
end;

function TCustomUMLCFormPanel.getTitleBar(): TCustomUMLCFormPanelTitleBar;
begin
  Result := self.FTitleBar;
end;

procedure TCustomUMLCFormPanel.setTitleBar
  (const Value: TCustomUMLCFormPanelTitleBar);
begin
  self.FTitleBar := Value;
end;

function TCustomUMLCFormPanel.getMenu(): TCustomUMLCFormPanelMenuBar;
begin
  Result := FMenu;
end;

procedure TCustomUMLCFormPanel.setMenu(const Value: TCustomUMLCFormPanelMenuBar);
begin
  FMenu := Value;
end;

constructor TCustomUMLCFormPanel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  TitleBarIsAuto := True;
  Self.FTitleBar := CreateTitleBarClass;
  Self.FTitleBar.Align:=alTop;
  Self.InsertControl(FTitleBar);
end;

destructor TCustomUMLCFormPanel.Destroy();
begin
  //FKeyValueTypeList.Empty();
  //FKeyValueTypeList.Free();
  inherited Destroy();
end;


end.

