(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccomboctrls;

interface
uses
(*.IFDEF MSWINDOWS*)
  //Windows,
  //Messages,
  Graphics,
  Controls,
  Forms,
  ExtCtrls,
  Buttons,
(*.ENDIF*)
  SysUtils,
  Classes,
  //Types,
  ActnList,
{$IFDEF FPC}
  LResources,
{$ENDIF}
  umlcpanels,
  umlcspeedbtns,
  dummy;

const
  MainButtonWidth  = 22;
  MainButtonHeight = 22;

  ComboButtonWidth  = 13;
  ComboButtonHeight = 22;

//  TestArea = 5;
  TestArea = 0;

type
  TUMLCOrientation = (doCustom, doTop, doBottom, doLeft, doRight);
  TUMLCDirection   = (ddDown, ddUp, ddLeft, ddRight);
  TUMLCPosition    = (dpTop, dpBottom, dpLeft, dpRight);

(* TCustomUMLCComboControl *)

  TUMLCComboControlSpeedButton = class;

  TCustomUMLCComboControl = class(TCustomUMLCPanel)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FOrientation: TUMLCOrientation;
    FDirection:   TUMLCDirection;
    FPosition:    TUMLCPosition;

    FComboButton: TUMLCComboControlSpeedButton;
  protected
    (* Protected declarations *)

    function getDirection(): TUMLCDirection;
    function getOrientation(): TUMLCOrientation;
    function getPosition(): TUMLCPosition;

    procedure setDirection(AValue: TUMLCDirection);
    procedure setOrientation(AValue: TUMLCOrientation);
    procedure setPosition(AValue: TUMLCPosition);
  protected
    (* Protected declarations *)

    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;

    procedure DoComboButtonOnClick; virtual; abstract;

    procedure LoadButtonImage(const ResName: string);
    procedure AssignOrientation();

    procedure CreateControls(); virtual; abstract;
    procedure DestroyControls(); virtual; abstract;
    procedure ReCreateControls(); virtual;

    procedure DelegateOnResize();
    procedure Resize(); override;

    procedure ComboButtonDelegateOnClick(Sender: TObject);
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
  public
    (* Public declarations *)

    property Direction: TUMLCDirection
       read getDirection write setDirection;
    property Orientation: TUMLCOrientation
       read getOrientation write setOrientation;
    property Position: TUMLCPosition
       read getPosition write setPosition;
  end;

(* TUMLCComboControlSpeedButton *)

  TUMLCComboControlSpeedButton = class(TCustomUMLCSpeedButton)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FComboControl: TCustomUMLCComboControl;
  public
    (* Public declarations *)

    property ComboControl: TCustomUMLCComboControl
      read FComboControl write FComboControl;
  end;

implementation

{$IFDEF DELPHI}
{$R 'sdvcomboctrlsres.dcr'}
{$ENDIF}

(* TCustomUMLCComboControl *)

procedure TCustomUMLCComboControl.GetChildren(Proc: TGetChildProc; Root: TComponent);
begin
//
end;

procedure TCustomUMLCComboControl.LoadButtonImage(const ResName: string);
begin
  FComboButton.Glyph.LoadFromResourceName(HInstance, ResName);
  FComboButton.NumGlyphs := 1;
  FComboButton.Enabled := false;
  FComboButton.Enabled := true;
end;

procedure TCustomUMLCComboControl.AssignOrientation();
begin
  case (FOrientation) of
    doTop:
    begin
      FPosition  := dpRight; // drop button
      FDirection := ddDown;  // drop button*s image
    end;
    doBottom:
    begin
      FPosition  := dpRight; // drop button
      FDirection := ddUp;  // drop button*s image
    end;
    doLeft:
    begin
      FPosition  := dpTop;   // drop button
      FDirection := ddRight; // drop button*s image
    end;
    doRight:
    begin
      FPosition  := dpTop;  // drop button
      FDirection := ddLeft; // drop button*s image
    end;
    // doCustom:
    else (*Nothing*);
  end;
end;

procedure TCustomUMLCComboControl.ReCreateControls();
begin
  DestroyControls();
  CreateControls();
end;

procedure TCustomUMLCComboControl.DelegateOnResize();
begin
  if (OnResize <> nil) then
  begin
    OnResize(Self);
  end;
end;

procedure TCustomUMLCComboControl.Resize();
begin
  DelegateOnResize();
end;

procedure TCustomUMLCComboControl.ComboButtonDelegateOnClick(Sender: TObject);
begin
  DoComboButtonOnClick();
end;

function TCustomUMLCComboControl.getDirection(): TUMLCDirection;
begin
  Result := FDirection;
end;

function TCustomUMLCComboControl.getOrientation(): TUMLCOrientation;
begin
  Result := FOrientation;
  // Goal: "Orientation" property get method.
  // Objetivo: Metodo lectura para propiedad "Orientation".
end;

function TCustomUMLCComboControl.getPosition(): TUMLCPosition;
begin
  Result := FPosition;
end;

procedure TCustomUMLCComboControl.setDirection(AValue: TUMLCDirection);
begin
  if (FOrientation = doCustom) then
  begin
    FDirection := AValue;
    ReCreateControls();
  end;
end;

procedure TCustomUMLCComboControl.setOrientation(AValue: TUMLCOrientation);
begin
  FOrientation := AValue;
  ReCreateControls();
  // Goal: "Orientation" property set method.
  // Objetivo: Metodo escritura para propiedad "Orientation".
end;

procedure TCustomUMLCComboControl.setPosition(AValue: TUMLCPosition);
begin
  if (FOrientation = doCustom) then
  begin
    FPosition := AValue;
    ReCreateControls();
  end;
end;

constructor TCustomUMLCComboControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  ControlStyle :=
    ControlStyle - [csAcceptsControls, csSetCaption] + [csOpaque];
  if (not NewStyleControls)
    then ControlStyle := ControlStyle + [csFramed];

  BevelOuter := bvNone;
  BevelInner := bvNone;

  FOrientation := doTop; // full control orientation
  FDirection   := ddDown;   // drop button*s image
  FPosition    := dpRight;  // drop button

  // Goal: To prepare the control.
end;


initialization
  {$IFDEF FPC}
  {$I 'umlccomboctrlsres.lrs'}
  {$ENDIF}
end.
