(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccontroltreenodes;

{$mode objfpc}{$H+}

interface
uses
  SysUtils, Classes,
  umlclists,
  umlctreestates,
  umlctreenodes,
  umlcactcntrls,
  umlctextreenodes,
  dummy;

type

 (* TOnUMLCControlTreeNodeEvent *)

   TUMLCControlTreeNode = class;

   TOnUMLCControlTreeNodeCanEvent =
     function (const ANode: TUMLCControlTreeNode): Boolean of object;
   TOnUMLCControlTreeNodeEvent =
     procedure (const ANode: TUMLCControlTreeNode) of object;

 (* TUMLCControlTreeNode *)

   TUMLCControlTreeNode = class(TUMLCTextTreeNode)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     // properties related to visual controls
     FEnabled:  Boolean;

     // there can be several, maybe hidden, randomly selected nodes,
     // but, only one visible focused node
     FFocused:   Boolean;
     FSelected:  Boolean;

     FVisible:  Boolean;

     FImageIndex:    Integer; // item Enabled and not Focused
     FFocusedIndex:  Integer; // item Enabled and Focused
     FDisabledIndex: Integer; // item disabled and not Focused
     FKeepIndex:     Integer; // item disabled and Focused

     FInternalState: TTreeStates;
   protected
     (* Protected declarations *)

     (* accessors declarations *)

     function getEnabled(): Boolean; virtual;

     // there can be several, maybe hidden, randomly selected nodes,
     // but, only one visible focused node
     function getFocused(): Boolean; virtual;
     function getSelected(): Boolean; virtual;

     function getState(): TTreeStates;

     procedure setEnabled(const AValue: Boolean); virtual;

     // there can be several, maybe hidden, randomly selected nodes,
     // but, only one visible focused node
     procedure setFocused(const AValue: Boolean); virtual;
     procedure setSelected(const AValue: Boolean); virtual;

     procedure setState(const AValue: TTreeStates); virtual;

     function getImageIndex: Integer;
     function getFocusedIndex: Integer; virtual;
     function getDisabledIndex: Integer;
     function getKeepIndex: Integer;

     procedure setImageIndex(const AValue: Integer);
     procedure setFocusedIndex(const AValue: Integer);
     procedure setDisabledIndex(const AValue: Integer);
     procedure setKeepIndex(const AValue: Integer);

     function getInternalState(): TTreeStates; virtual;
     procedure setInternalState(const AValue: TTreeStates); virtual;
   public
     (* Protected Friend declarations *)

     procedure InternalExplore(); virtual;
     procedure InternalCollapse(); virtual;
     procedure InternalExpand(); virtual;
     procedure InternalEmpty(); virtual;
     procedure InternalHide(); virtual;
   protected
     (* Protected declarations *)

     procedure UpdateImageIndex(); virtual;
   public
     (* Protected Friend declarations *)

     (* DO NOT become published declarations *)

     // "InternalState" property does not refresh visually,
     // the state, of the node, "State" property does
     property InternalState: TTreeStates
       read getInternalState write setInternalState;
   public
     (* Public declarations *)

     procedure DoCreate(); override;
   public
     (* Public declarations *)

     procedure Explore(); (* nonvirtual; *)
     procedure Collapse(); (* nonvirtual; *)
     procedure Expand(); (* nonvirtual; *)
     procedure Empty(); (* nonvirtual; *)

     procedure UpdateExpand(); (* nonvirtual; *)

     procedure Hide(); (* nonvirtual; *)
   published
     (* Published declarations *)

     property Enabled: Boolean
       read getEnabled write setEnabled;

     // there can be several, maybe hidden, randomly selected nodes,
     // but, only one visible focused node
     property Focused: Boolean
       read getFocused write setFocused;
     property Selected: Boolean
       read getSelected write setSelected;

     // "InternalState" property does not refresh visually,
     // the state, of the node, "State" property does
     property State: TTreeStates
       read getState write setState;

     property ImageIndex: Integer
       read getImageIndex write setImageIndex;
     property SelectedIndex: Integer
       read getFocusedIndex write setFocusedIndex;
     property DisabledIndex: Integer
       read getDisabledIndex write setDisabledIndex;
     property KeepIndex: Integer
       read getKeepIndex write setKeepIndex;
   end;

 (* TUMLCControlTreeCollection *)

   TUMLCControlTreeCollection = class(TUMLCTextTreeCollection)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     function CreateNodeByClass(): TUMLCTreeNode; override;

     procedure InternalConfirmedInsert
       (const AParentNode: TUMLCTreeNode; var ANewNode: TUMLCTreeNode); override;
     procedure InternalConfirmedInsertAt
       (const AParentNode: TUMLCTreeNode;
        const AIndex: Integer; var ANewNode: TUMLCTreeNode); override;
   protected
     (* Protected declarations *)

     FBeforeExplore:  TOnUMLCControlTreeNodeEvent;
     FBeforeCollapse: TOnUMLCControlTreeNodeEvent;
     FBeforeExpand:   TOnUMLCControlTreeNodeEvent;
     FBeforeEmpty:    TOnUMLCControlTreeNodeEvent;

     FOnExplore:      TOnUMLCControlTreeNodeEvent;

     FAfterExplore:   TOnUMLCControlTreeNodeEvent;
     FAfterCollapse:  TOnUMLCControlTreeNodeEvent;
     FAfterExpand:    TOnUMLCControlTreeNodeEvent;
     FAfterEmpty:     TOnUMLCControlTreeNodeEvent;
   public
     (* Protected Friend declarations *)

     procedure DelegateBeforeExplore(const ANode: TUMLCControlTreeNode); (*nonvirtual;*)
     procedure DelegateBeforeCollapse(const ANode: TUMLCControlTreeNode); (*nonvirtual;*)
     procedure DelegateBeforeExpand(const ANode: TUMLCControlTreeNode); (*nonvirtual;*)
     procedure DelegateBeforeEmpty(const ANode: TUMLCControlTreeNode); (*nonvirtual;*)

     procedure DelegateOnExplore(const ANode: TUMLCControlTreeNode); (*nonvirtual;*)

     procedure DelegateAfterExplore(const ANode: TUMLCControlTreeNode); (*nonvirtual;*)
     procedure DelegateAfterCollapse(const ANode: TUMLCControlTreeNode); (*nonvirtual;*)
     procedure DelegateAfterExpand(const ANode: TUMLCControlTreeNode); (*nonvirtual;*)
     procedure DelegateAfterEmpty(const ANode: TUMLCControlTreeNode); (*nonvirtual;*)
   public
     (* Public declarations *)

     property BeforeExplore: TOnUMLCControlTreeNodeEvent
       read FBeforeExplore write FBeforeExplore;
     property BeforeCollapse: TOnUMLCControlTreeNodeEvent
       read FBeforeCollapse write FBeforeCollapse;
     property BeforeExpand: TOnUMLCControlTreeNodeEvent
       read FBeforeExpand write FBeforeExpand;
     property BeforeEmpty: TOnUMLCControlTreeNodeEvent
       read FBeforeEmpty write FBeforeEmpty;

     property OnExplore: TOnUMLCControlTreeNodeEvent
       read FOnExplore write FOnExplore;

     property AfterExplore: TOnUMLCControlTreeNodeEvent
       read FAfterExplore write FAfterExplore;
     property AfterCollapse: TOnUMLCControlTreeNodeEvent
       read FAfterCollapse write FAfterCollapse;
     property AfterExpand: TOnUMLCControlTreeNodeEvent
       read FAfterExpand write FAfterExpand;
     property AfterEmpty: TOnUMLCControlTreeNodeEvent
       read FAfterEmpty write FAfterEmpty;
   end;

implementation

(* TUMLCControlTreeNode *)

function TUMLCControlTreeNode.getEnabled(): Boolean;
begin
  Result := FEnabled;
end;

function TUMLCControlTreeNode.getFocused: Boolean;
begin
  Result := FFocused;
end;

function TUMLCControlTreeNode.getSelected(): Boolean;
begin
  Result := FSelected;
end;

function TUMLCControlTreeNode.getState(): TTreeStates;
begin
  Result := FInternalState;
end;

procedure TUMLCControlTreeNode.setEnabled(const AValue: Boolean);
begin
  if (FEnabled <> AValue) then
  begin
    FEnabled := AValue;
  end;
end;

procedure TUMLCControlTreeNode.setFocused(const AValue: Boolean);
begin
  if (FFocused <> AValue) then
  begin
    FFocused := AValue;
  end;
end;

procedure TUMLCControlTreeNode.setSelected(const AValue: Boolean);
begin
  // @to-do: change state image ???
  if (FSelected <> AValue) then
  begin
    FSelected := AValue;
  end;
end;

procedure TUMLCControlTreeNode.setState(const AValue: TTreeStates);
begin
  if (FInternalState <> AValue) then
  begin
    FInternalState := AValue;
  end;
end;

function TUMLCControlTreeNode.getImageIndex(): Integer;
begin
  Result := FImageIndex;
end;

function TUMLCControlTreeNode.getFocusedIndex(): Integer;
begin
  Result := FFocusedIndex;
end;

function TUMLCControlTreeNode.getDisabledIndex(): Integer;
begin
  Result := FDisabledIndex;
end;

function TUMLCControlTreeNode.getKeepIndex(): Integer;
begin
  Result := FKeepIndex;
end;

procedure TUMLCControlTreeNode.setImageIndex(const AValue: Integer);
begin
  FImageIndex := AValue;
  UpdateImageIndex();
end;

procedure TUMLCControlTreeNode.setFocusedIndex(const AValue: Integer);
begin
  FFocusedIndex := AValue;
  UpdateImageIndex();
end;

procedure TUMLCControlTreeNode.setDisabledIndex(const AValue: Integer);
begin
  FDisabledIndex := AValue;
  UpdateImageIndex();
end;

procedure TUMLCControlTreeNode.setKeepIndex(const AValue: Integer);
begin
  FKeepIndex := AValue;
  UpdateImageIndex();
end;

function TUMLCControlTreeNode.getInternalState(): TTreeStates;
begin
  Result := FInternalState;
end;

procedure TUMLCControlTreeNode.setInternalState(const AValue: TTreeStates);
begin
  if (FInternalState <> AValue) then
  begin
    FInternalState := AValue;
  end;
end;

procedure TUMLCControlTreeNode.InternalExplore();
begin
  if (HasItems()) then
  begin
    InternalState := umlctreestates.tsExpanded;
  end else
  begin
    InternalState := umlctreestates.tsEmpty;
  end;
end;

procedure TUMLCControlTreeNode.InternalCollapse();
begin
  if (HasItems()) then
  begin
    InternalState := umlctreestates.tsCollapsed;
  end else
  begin
    InternalState := umlctreestates.tsEmpty;
  end;
end;

procedure TUMLCControlTreeNode.InternalExpand();
begin
  if (HasItems()) then
  begin
    // change state & related state image
    InternalState := umlctreestates.tsExpanded;
  end else
  begin
    // change state & related state image
    InternalState := umlctreestates.tsEmpty;
  end;
end;

procedure TUMLCControlTreeNode.InternalEmpty();
begin
  InternalList.Empty();
  InternalState := umlctreestates.tsEmpty;
end;

procedure TUMLCControlTreeNode.InternalHide();
begin
  InternalState := umlctreestates.tsUnknown;
  // Goal: Makes subitems not visible,
  // NOT the node itself.
end;

procedure TUMLCControlTreeNode.UpdateImageIndex;
begin
  //
end;

procedure TUMLCControlTreeNode.DoCreate();
begin
  inherited DoCreate();

  FInternalState := umlctreestates.tsUnknown;
  FVisible := false;
  FEnabled := true;
end;

procedure TUMLCControlTreeNode.Explore();
var ACollection: TUMLCControlTreeCollection;
begin
  if (FInternalState = umlctreestates.tsUnknown) then
  begin
    InternalState := umlctreestates.tsExploring;

    ACollection
      := (InternalCollection  as TUMLCControlTreeCollection);

    ACollection.DelegateBeforeExplore(Self);

    InternalExplore();
    ACollection.DelegateOnExplore(Self);

    ACollection.DelegateAfterExplore(Self);

    Self.Expand();
  end;
end;

procedure TUMLCControlTreeNode.Collapse();
var ACollection: TUMLCControlTreeCollection;
begin
  if (FInternalState = umlctreestates.tsExpanded) then
  begin
    ACollection
      := (InternalCollection  as TUMLCControlTreeCollection);

    ACollection.DelegateBeforeCollapse(Self);
    InternalCollapse();
    ACollection.DelegateAfterCollapse(Self);
  end;
end;

procedure TUMLCControlTreeNode.Expand();
var ACollection: TUMLCControlTreeCollection;
    CanExpand: Boolean;
begin
  CanExpand :=
    ( (FInternalState = umlctreestates.tsExploring)
      or (FInternalState = umlctreestates.tsCollapsed)
    );
  if (CanExpand) then
  begin
    ACollection
      := (InternalCollection  as TUMLCControlTreeCollection);

    ACollection.DelegateBeforeExpand(Self);
    InternalExpand();
    ACollection.DelegateAfterExpand(Self);
  end;
end;

procedure TUMLCControlTreeNode.Empty();
var ACollection: TUMLCControlTreeCollection;
begin
  if (FInternalState <> umlctreestates.tsEmpty) then
  begin
    ACollection
      := (InternalCollection  as TUMLCControlTreeCollection);

    ACollection.DelegateBeforeEmpty(Self);
    InternalEmpty();
    ACollection.DelegateAfterEmpty(Self);
  end;
end;

procedure TUMLCControlTreeNode.UpdateExpand();
begin
  // hide visible child nodes from node,
  // if any, and its expanded,
  // otherwise, do nothing
  //if (Self.State = umlctreestates.tsExpanded) then
  //begin
    //Self.Collapse();
  //end;
  Self.Hide();

  // cannot "expand" without a "explore",
  // but, "explore" calls "expand"
  Self.Explore();

  // show again, all nodes, including new node
  Self.Expand();
end;

procedure TUMLCControlTreeNode.Hide();
var ACollection: TUMLCControlTreeCollection;
begin
  InternalHide();
  // Goal: Changes a node state to "unknown",
  // and hides any visible subitems.
end;

(* TUMLCControlTreeCollection *)

function TUMLCControlTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCControlTreeNode.Create();
  Result.DoCreate();
end;

procedure TUMLCControlTreeCollection.InternalConfirmedInsert
  (const AParentNode: TUMLCTreeNode; var ANewNode: TUMLCTreeNode);
var APanelParentNode, APanelNewNode: TUMLCControlTreeNode;
begin
  inherited InternalConfirmedInsert(AParentNode, ANewNode);

  // generate initial caption
  APanelParentNode   := (AParentNode as TUMLCControlTreeNode);
  APanelNewNode      := (ANewNode as TUMLCControlTreeNode);
  APanelNewNode.Text := Self.DefaultText();

  // update parent node state
  if (APanelParentNode <> nil) then
  begin
    case (APanelParentNode.InternalState) of
      umlctreestates.tsEmpty:     APanelParentNode.InternalState := umlctreestates.tsCollapsed;
      umlctreestates.tsCollapsed: APanelParentNode.InternalState := umlctreestates.tsCollapsed;
      umlctreestates.tsExpanded:  APanelParentNode.InternalState := umlctreestates.tsExpanded;
      umlctreestates.tsUnknown:   APanelParentNode.InternalState := umlctreestates.tsCollapsed;
      else ;
    end;
  end;
end;

procedure TUMLCControlTreeCollection.InternalConfirmedInsertAt
  (const AParentNode: TUMLCTreeNode; const AIndex: Integer;
   var ANewNode: TUMLCTreeNode);
var APanelParentNode, APanelNewNode: TUMLCControlTreeNode;
begin
  inherited InternalConfirmedInsertAt(AParentNode, AIndex, ANewNode);

  // generate initial caption
  APanelParentNode   := (AParentNode as TUMLCControlTreeNode);
  APanelNewNode      := (ANewNode as TUMLCControlTreeNode);
  APanelNewNode.Text := Self.DefaultText();

  // update parent node state
  if (APanelParentNode <> nil) then
  begin
    case (APanelParentNode.InternalState) of
      umlctreestates.tsEmpty:     APanelParentNode.InternalState := umlctreestates.tsCollapsed;
      umlctreestates.tsCollapsed: APanelParentNode.InternalState := umlctreestates.tsCollapsed;
      umlctreestates.tsExpanded:  APanelParentNode.InternalState := umlctreestates.tsExpanded;
      umlctreestates.tsUnknown:   APanelParentNode.InternalState := umlctreestates.tsCollapsed;
      else ;
    end;
  end;
end;

procedure TUMLCControlTreeCollection.DelegateBeforeExplore
  (const ANode: TUMLCControlTreeNode);
begin
  if (FBeforeExplore <> nil) then
  begin
    FBeforeExplore(ANode);
  end;
end;

procedure TUMLCControlTreeCollection.DelegateBeforeCollapse
  (const ANode: TUMLCControlTreeNode);
begin
  if (FBeforeCollapse <> nil) then
  begin
    FBeforeCollapse(ANode);
  end;
end;

procedure TUMLCControlTreeCollection.DelegateBeforeExpand
  (const ANode: TUMLCControlTreeNode);
begin
  if (FBeforeExpand <> nil) then
  begin
    FBeforeExpand(ANode);
  end;
end;

procedure TUMLCControlTreeCollection.DelegateBeforeEmpty
  (const ANode: TUMLCControlTreeNode);
begin
  if (FBeforeEmpty <> nil) then
  begin
    FBeforeEmpty(ANode);
  end;
end;

procedure TUMLCControlTreeCollection.DelegateOnExplore
  (const ANode: TUMLCControlTreeNode);
begin
  if (FOnExplore <> nil) then
  begin
    FOnExplore(ANode);
  end;
end;

procedure TUMLCControlTreeCollection.DelegateAfterExplore
  (const ANode: TUMLCControlTreeNode);
begin
  if (FAfterExplore <> nil) then
  begin
    FAfterExplore(ANode);
  end;
end;

procedure TUMLCControlTreeCollection.DelegateAfterCollapse
  (const ANode: TUMLCControlTreeNode);
begin
  if (FAfterCollapse <> nil) then
  begin
    FAfterCollapse(ANode);
  end;
end;

procedure TUMLCControlTreeCollection.DelegateAfterExpand
  (const ANode: TUMLCControlTreeNode);
begin
  if (FAfterExpand <> nil) then
  begin
    FAfterExpand(ANode);
  end;
end;

procedure TUMLCControlTreeCollection.DelegateAfterEmpty
  (const ANode: TUMLCControlTreeNode);
begin
  if (FAfterEmpty <> nil) then
  begin
    FAfterEmpty(ANode);
  end;
end;

end.

