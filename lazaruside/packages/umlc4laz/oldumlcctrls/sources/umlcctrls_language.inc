(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

(* "umlcctrls_language.incpas" *)

{$UNDEF umlcctrls_language_english}
{$UNDEF umlcctrls_language_spanisheurope}
{$UNDEF umlcctrls_language_spanishlatam}
{$UNDEF umlcctrls_language_french}
{$UNDEF umlcctrls_language_german}
{$UNDEF umlcctrls_language_portuguese}
{$UNDEF umlcctrls_language_italian}

{$define umlcctrls_language_english}
{.define umlcctrls_language_spanisheurope}
{.define umlcctrls_language_spanishlatam}
{.define umlcctrls_language_french}
{.define umlcctrls_language_german}
{.define umlcctrls_language_portuguese}
{.define umlcctrls_language_italian}
