(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcpgctrls;

interface

uses
(*.IFDEF MSWINDOWS*)
  //Windows,
  //Messages,
  Graphics,
  Controls,
  ComCtrls,
  Forms,
(*.ENDIF*)
  SysUtils, Classes,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcactcntrls,
  dummy;

type

(* TUMLCTabStyle *)

  TUMLCTabStyle = (sdtsCustomTabs, sdtsShowTabs, sdtsHideTabs);

(* TCustomUMLCPageControl *)

  TCustomUMLCPageControl = class(TPageControl, IUMLCActivatedControl)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FActivated: Boolean;
    FTabStyle:  TUMLCTabStyle;
  protected
    (* Protected declarations *)

    function getActivated: Boolean; virtual;
    function getTabStyle: TUMLCTabStyle; virtual;

    procedure setActivated(const Value: Boolean); virtual;
    procedure setTabStyle(const Value: TUMLCTabStyle); virtual;
  protected
    (* Protected declarations *)

    procedure UpdateShowTabs(const Value: Boolean); virtual;
  protected
    (* Protected declarations *)

    procedure ActivateFirst(); virtual;
    procedure DeactivateLast(); virtual;

    procedure Activate(); virtual;
    procedure Deactivate(); virtual;
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;

    property Activated: Boolean
      read getActivated write setActivated;

    (* UnPublished declarations *)

    property TabStyle: TUMLCTabStyle
      read getTabStyle write setTabStyle;
  end;

(* TUMLCPageControl *)

  TUMLCPageControl = class(TCustomUMLCPageControl)
  published
    (* Published declarations *)

    (* TCustomUMLCPageControl: *)

    property TabStyle;
  end;

implementation

(* TCustomUMLCPageControl *)

function TCustomUMLCPageControl.getActivated: Boolean;
begin
  Result := FActivated;
  // Goal: "Activated" property get method.
  // Objetivo: Metodo lectura para propiedad "Activated".
end;

function TCustomUMLCPageControl.getTabStyle: TUMLCTabStyle;
begin
  Result := FTabStyle;
  // Goal: "TabStyle" property get method.
  // Objetivo: Metodo lectura para propiedad "TabStyle".
end;

procedure TCustomUMLCPageControl.setActivated(const Value: Boolean);
begin
  if (FActivated <> Value) then
  begin
    FActivated := Value;
    if (Value)
      then ActivateFirst
      else DeActivateLast;
  end;
  // Goal: "Activated" property set method.
  // Objetivo: Metodo escritura para propiedad "Activated".
end;

procedure TCustomUMLCPageControl.setTabStyle(const Value: TUMLCTabStyle);
begin
  if (FTabStyle <> Value) then
  begin
    FTabStyle := Value;

    case FTabStyle of
      sdtsCustomTabs: (*Nothing*);
      sdtsShowTabs:   UpdateShowTabs(true);
      sdtsHideTabs:   UpdateShowTabs(false);
    end;
  end;
  // Goal: "TabStyle" property set method.
  // Objetivo: Metodo escritura para propiedad "TabStyle".
end;

procedure TCustomUMLCPageControl.UpdateShowTabs(const Value: Boolean);
var AIndex, ACount: Integer;
begin
  ACount := Pred(PageCount);
  for AIndex := 0 to ACount do
  begin
    Pages[AIndex].TabVisible := Value;
  end;
  // Goal: Assign all tabs the same visible state.
  // Objetivo: Asignar a todas las etiquetas el mismo estado visible.
end;

procedure TCustomUMLCPageControl.ActivateFirst;
begin
  // Goal: Performa an specific action when the control is activated
  // by the first time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // activado la primera vez.
end;

procedure TCustomUMLCPageControl.DeactivateLast;
begin
  // Goal: Performa an specific action when the control is dectivated
  // by the last time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // deactivado por ultima vez.
end;

procedure TCustomUMLCPageControl.Activate();
begin
  // Goal: Perform an specific action when the control is activated
  // by the first time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // activado la primera vez.
end;

procedure TCustomUMLCPageControl.Deactivate();
begin
  // Goal: Perform an specific action when the control is dectivated
  // by the last time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // deactivado por ultima vez.
end;

constructor TCustomUMLCPageControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FTabStyle  := sdtsCustomTabs;
  FActivated := false;
end;

end.
