(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcitemcombos;

(*$R-,T-,H+,X+*)

interface
uses
(*.IFDEF MSWINDOWS*)
  Windows, Messages, Graphics,
  Controls, StdCtrls, Forms,
(*.ENDIF*)
  SysUtils, Classes,
  umlckeyconsts, umlcdays, umlcmonths, umlctimes,
  umlcconfigs, umlclangs, umlcenumstrs,
  umlccomponents,
  umlcctrls, umlccomboboxes,
  dummy;

type

(* TUMLCItemComboBoxObject *)

  TUMLCItemComboBoxObject = class(TObject)
  protected
    (* Protected declarations *)

    FInternalObject: TObject;
  public
    (* Public declarations *)

    constructor Create;  reintroduce; virtual;
    destructor Destroy; override;

    property InternalObject: TObject
      read FInternalObject write FInternalObject;
  end;
  TUMLCItemComboBoxObjectClass = class of TUMLCItemComboBoxObject;

(* TCustomUMLCItemCombobox *)

  TCustomUMLCItemCombobox = class(TCustomUMLCCombobox)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function InternalObjectClass: TUMLCItemComboBoxObjectClass; virtual;

    function getStringAt(Index: Integer): string;
    procedure setStringAt(Index: Integer; Value: string);
  public
    (* Public declarations *)

    {$IFDEF DELPHI}
    procedure AddItem(Item: String; AObject: TObject); override;
    {$ENDIF}
    {$IFDEF FPC}
    procedure AddItem(const Item: String; AnObject: TObject); override;
    {$ENDIF}

    property Strings[Index: Integer]: string
      read getStringAt write setStringAt;

    property AutoComplete default True;
    property AutoDropDown default False;

    {$IFDEF DELPHI}
    property BevelKind default bkNone;
    {$ENDIF}

    property ItemIndex default -1;
  end;

(* TCustomUMLCObjectCombobox *)

  TCustomUMLCObjectCombobox = class(TCustomUMLCItemCombobox)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function getObjectAt(Index: Integer): TObject;
    procedure setObjectAt(Index: Integer; Value: TObject);
  public
    (* Public declarations *)

    procedure AddObject(const ACaption: string; AObject: TObject); dynamic;

    property Objects[Index: Integer]: TObject
      read getObjectAt write setObjectAt;
  end;

(* TUMLCDataComboBoxObject *)

  TUMLCDataComboBoxObject = class(TUMLCItemComboBoxObject)
  protected
    (* Protected declarations *)

    FData: pointer;
  public
    (* Public declarations *)

    constructor Create;  override;
    destructor Destroy; override;

    property Data: pointer
      read FData write FData;
  end;

(* TCustomUMLCDataCombobox *)

  TCustomUMLCDataCombobox = class(TCustomUMLCItemCombobox)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function getDataAt(Index: Integer): pointer;
    procedure setDataAt(Index: Integer; Value: pointer);

    function InternalObjectClass: TUMLCItemComboBoxObjectClass; override;
  public
    (* Public declarations *)

    procedure AddData(const ACaption: string; AData: pointer); dynamic;

    property Data[Index: Integer]: pointer
      read getDataAt write setDataAt;
  end;

(* TUMLCIntegerComboBoxObject *)

  TUMLCIntegerComboBoxObject = class(TUMLCDataComboBoxObject)
  protected
    (* Protected declarations *)

    FValue: Integer;
  public
    (* Public declarations *)

    constructor Create; override;
    destructor Destroy; override;

    property Value: Integer
      read FValue write FValue;
  end;

(* TCustomUMLCIntegerCombobox *)

  TCustomUMLCIntegerCombobox = class(TCustomUMLCDataCombobox)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function getValueAt(Index: Integer): Integer;
    procedure setValueAt(Index: Integer; Value: Integer);

    function InternalObjectClass: TUMLCItemComboBoxObjectClass; override;
  public
    (* Public declarations *)

    procedure AddInteger(const ACaption: string; AValue: Integer); dynamic;

    property Value[Index: Integer]: Integer
      read getValueAt write setValueAt;
  end;

(* TUMLCDoubleIntegerRecord *)

  TUMLCDoubleIntegerRecord = record
    Hi, Lo: Integer;
  end;

(* TUMLCDoubleIntegerComboBoxObject *)

  TUMLCDoubleIntegerComboBoxObject = class(TUMLCDataComboBoxObject)
  protected
    (* Protected declarations *)

    FValue: TUMLCDoubleIntegerRecord;
  public
    (* Public declarations *)

    constructor Create; override;
    destructor Destroy; override;

    property Value: TUMLCDoubleIntegerRecord
      read FValue write FValue;
  end;

(* TCustomUMLCDoubleIntegerCombobox *)

  TCustomUMLCDoubleIntegerCombobox = class(TCustomUMLCDataCombobox)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function getValueAt(Index: Integer): TUMLCDoubleIntegerRecord;
    procedure setValueAt(Index: Integer; Value: TUMLCDoubleIntegerRecord);

    function InternalObjectClass: TUMLCItemComboBoxObjectClass; override;
  public
    (* Public declarations *)

    procedure AddDoubleInteger(const ACaption: string; AValue: TUMLCDoubleIntegerRecord); dynamic;

    property Value[Index: Integer]: TUMLCDoubleIntegerRecord
      read getValueAt write setValueAt;
  end;

(* TUMLCObjectCombobox *)

  TUMLCObjectCombobox = class(TCustomUMLCObjectCombobox)
  published
    (* Public declarations *)

(*
    property AutoComplete default True;
    property AutoDropDown default False;
    property BevelEdges;
    property BevelInner;
    property BevelKind default bkNone;
    property BevelOuter;
    property Style; {Must be published before Items}
    property Anchors;
    property BiDiMode;
    property CharCase;
    property Color;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragKind;
    property DragMode;
    property DropDownCount;
    property Enabled;
    property Font;
    property ImeMode;
    property ImeName;
    property ItemHeight;
    property ItemIndex default -1;
    property MaxLength;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Sorted;
    property TabOrder;
    property TabStop;
    property Text;
    property Visible;
    property OnChange;
    property OnClick;
    property OnCloseUp;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDrawItem;
    property OnDropDown;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMeasureItem;
    property OnSelect;
    property OnStartDock;
    property OnStartDrag;
    property Items; { Must be published after OnMeasureItem }
*)

    property AutoComplete;
    property AutoDropDown;
    {$IFDEF DELPHI}
    property BevelEdges;
    property BevelInner;
    property BevelKind;
    property BevelOuter;
    {$ENDIF}
    property Style; {Must be published before Items}
    property Anchors;
    property BiDiMode;
    property CharCase;
    property Color;
    property Constraints;
    {$IFDEF DELPHI}
    property Ctl3D;
    {$ENDIF}
    property DragCursor;
    property DragKind;
    property DragMode;
    property DropDownCount;
    property Enabled;
    property Font;
    {$IFDEF DELPHI}
    property ImeMode;
    property ImeName;
    {$ENDIF}
    property ItemHeight;
    property ItemIndex;
    property MaxLength;
    property ParentBiDiMode;
    property ParentColor;
    {$IFDEF DELPHI}
    property ParentCtl3D;
    {$ENDIF}
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Sorted;
    property TabOrder;
    property TabStop;
    property Text;
    property Visible;
    property OnChange;
    property OnClick;
    property OnCloseUp;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDrawItem;
    property OnDropDown;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMeasureItem;
    property OnSelect;
    property OnStartDock;
    property OnStartDrag;
    property Items; { Must be published after OnMeasureItem }
  end;

(* TUMLCDataCombobox *)

  TUMLCDataCombobox = class(TCustomUMLCDataCombobox)
  published
    (* Public declarations *)

    property AutoComplete;
    property AutoDropDown;
    {$IFDEF DELPHI}
    property BevelEdges;
    property BevelInner;
    property BevelKind;
    property BevelOuter;
    {$ENDIF}
    property Style; {Must be published before Items}
    property Anchors;
    property BiDiMode;
    property CharCase;
    property Color;
    property Constraints;
    {$IFDEF DELPHI}
    property Ctl3D;
    {$ENDIF}
    property DragCursor;
    property DragKind;
    property DragMode;
    property DropDownCount;
    property Enabled;
    property Font;
    {$IFDEF DELPHI}
    property ImeMode;
    property ImeName;
    {$ENDIF}
    property ItemHeight;
    property ItemIndex;
    property MaxLength;
    property ParentBiDiMode;
    property ParentColor;
    {$IFDEF DELPHI}
    property ParentCtl3D;
    {$ENDIF}
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Sorted;
    property TabOrder;
    property TabStop;
    property Text;
    property Visible;
    property OnChange;
    property OnClick;
    property OnCloseUp;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDrawItem;
    property OnDropDown;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMeasureItem;
    property OnSelect;
    property OnStartDock;
    property OnStartDrag;
    property Items; { Must be published after OnMeasureItem }
  end;

(* TUMLCIntegerCombobox *)

  TUMLCIntegerCombobox = class(TCustomUMLCIntegerCombobox)
  published
    (* Public declarations *)

    property AutoComplete;
    property AutoDropDown;
    {$IFDEF DELPHI}
    property BevelEdges;
    property BevelInner;
    property BevelKind;
    property BevelOuter;
    {$ENDIF}
    property Style; {Must be published before Items}
    property Anchors;
    property BiDiMode;
    property CharCase;
    property Color;
    property Constraints;
    {$IFDEF DELPHI}
    property Ctl3D;
    {$ENDIF}
    property DragCursor;
    property DragKind;
    property DragMode;
    property DropDownCount;
    property Enabled;
    property Font;
    {$IFDEF DELPHI}
    property ImeMode;
    property ImeName;
    {$ENDIF}
    property ItemHeight;
    property ItemIndex;
    property MaxLength;
    property ParentBiDiMode;
    property ParentColor;
    {$IFDEF DELPHI}
    property ParentCtl3D;
    {$ENDIF}
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Sorted;
    property TabOrder;
    property TabStop;
    property Text;
    property Visible;
    property OnChange;
    property OnClick;
    property OnCloseUp;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDrawItem;
    property OnDropDown;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMeasureItem;
    property OnSelect;
    property OnStartDock;
    property OnStartDrag;
    property Items; { Must be published after OnMeasureItem }
  end;

(* TUMLCDoubleIntegerCombobox *)

  TUMLCDoubleIntegerCombobox = class(TCustomUMLCDoubleIntegerCombobox)
  published
    (* Public declarations *)

    property AutoComplete;
    property AutoDropDown;
    {$IFDEF DELPHI}
    property BevelEdges;
    property BevelInner;
    property BevelKind;
    property BevelOuter;
    {$ENDIF}
    property Style; {Must be published before Items}
    property Anchors;
    property BiDiMode;
    property CharCase;
    property Color;
    property Constraints;
    {$IFDEF DELPHI}
    property Ctl3D;
    {$ENDIF}
    property DragCursor;
    property DragKind;
    property DragMode;
    property DropDownCount;
    property Enabled;
    property Font;
    {$IFDEF DELPHI}
    property ImeMode;
    property ImeName;
    {$ENDIF}
    property ItemHeight;
    property ItemIndex;
    property MaxLength;
    property ParentBiDiMode;
    property ParentColor;
    {$IFDEF DELPHI}
    property ParentCtl3D;
    {$ENDIF}
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Sorted;
    property TabOrder;
    property TabStop;
    property Text;
    property Visible;
    property OnChange;
    property OnClick;
    property OnCloseUp;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDrawItem;
    property OnDropDown;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMeasureItem;
    property OnSelect;
    property OnStartDock;
    property OnStartDrag;
    property Items; { Must be published after OnMeasureItem }
  end;

implementation

(* TUMLCItemComboBoxObject *)

constructor TUMLCItemComboBoxObject.Create;
begin
  inherited Create;
  FInternalObject := nil;
end;

destructor TUMLCItemComboBoxObject.Destroy;
begin
  FInternalObject.Free;
  inherited Destroy;
end;

(* TCustomUMLCItemCombobox *)

function TCustomUMLCItemCombobox.InternalObjectClass: TUMLCItemComboBoxObjectClass;
begin
  Result := TUMLCItemComboBoxObject;
end;

function TCustomUMLCItemCombobox.getStringAt(Index: Integer): string;
begin
  Result := InternalItems.Strings[Index];
end;

procedure TCustomUMLCItemCombobox.setStringAt(Index: Integer; Value: string);
begin
  InternalItems.Strings[Index] := Value;
end;

{$IFDEF DELPHI}
procedure TCustomUMLCItemCombobox.AddItem
  (Item: String; AObject: TObject);
var AItemObject: TUMLCItemComboBoxObject;
begin
  AItemObject := InternalObjectClass().Create;
  // create object to store object reference

  AItemObject.InternalObject := AObject;
  // redirect object reference

  //Items.AddObject(Item, AItemObject);

  inherited AddItem(Item, AItemObject);
  // add substitute object with reference to real data object
end;
{$ENDIF}
{$IFDEF FPC}
procedure TCustomUMLCItemCombobox.AddItem
  (const Item: String; AnObject: TObject);
var AItemObject: TUMLCItemComboBoxObject;
begin
  AItemObject := InternalObjectClass().Create;
  // create object to store object reference

  AItemObject.InternalObject := AnObject;
  // redirect object reference

  //Items.AddObject(Item, AItemObject);

  inherited AddItem(Item, AItemObject);
  // add substitute object with reference to real data object
end;
{$ENDIF}

(* TCustomUMLCObjectCombobox *)

function TCustomUMLCObjectCombobox.getObjectAt(Index: Integer): TObject;
var Item: TUMLCItemComboBoxObject;
begin
  Item := (InternalItems.Objects[Index] as TUMLCItemComboBoxObject);
  Result := Item.InternalObject;
end;

procedure TCustomUMLCObjectCombobox.setObjectAt(Index: Integer; Value: TObject);
var Item: TUMLCItemComboBoxObject;
begin
  Item := (InternalItems.Objects[Index] as TUMLCItemComboBoxObject);
  Item.InternalObject  := Value;
end;

procedure TCustomUMLCObjectCombobox.AddObject(const ACaption: string; AObject: TObject);
begin
  AddItem(ACaption, AObject);
  Application.ProcessMessages;
end;

(* TUMLCDataComboBoxObject *)

constructor TUMLCDataComboBoxObject.Create;
begin
  inherited Create;
  FData := nil;
end;

destructor TUMLCDataComboBoxObject.Destroy;
begin
  FData := nil;
  inherited Destroy;
end;

(* TCustomUMLCDataCombobox *)

function TCustomUMLCDataCombobox.getDataAt(Index: Integer): pointer;
var Item: TUMLCDataComboBoxObject;
begin
  Item := (InternalItems.Objects[Index] as TUMLCDataComboBoxObject);
  Result := Item.Data;
end;

procedure TCustomUMLCDataCombobox.setDataAt(Index: Integer; Value: pointer);
var Item: TUMLCDataComboBoxObject;
begin
  Item := (InternalItems.Objects[Index] as TUMLCDataComboBoxObject);
  Item.Data  := TObject(Value);
end;

function TCustomUMLCDataCombobox.InternalObjectClass: TUMLCItemComboBoxObjectClass;
begin
  Result := TUMLCDataComboBoxObject;
end;

procedure TCustomUMLCDataCombobox.AddData(const ACaption: string; AData: pointer);
var ADataObject: TUMLCDataComboBoxObject;
begin
  ADataObject := (InternalObjectClass().Create as TUMLCDataComboBoxObject);
  // create object to store object reference
  ADataObject.InternalObject := ADataObject;
  ADataObject.Data := AData;
  // redirect object reference

  Items.AddObject(ACaption, ADataObject);
  // add substitute object with reference to real data object
end;

(* TUMLCIntegerComboBoxObject *)

constructor TUMLCIntegerComboBoxObject.Create;
begin
  inherited Create;
  FValue := 0;
end;

destructor TUMLCIntegerComboBoxObject.Destroy;
begin
  FValue := 0;
  inherited Destroy;
end;

(* TCustomUMLCIntegerCombobox *)

function TCustomUMLCIntegerCombobox.getValueAt(Index: Integer): Integer;
var Item: TUMLCIntegerComboBoxObject;
begin
  Item := (InternalItems.Objects[Index] as TUMLCIntegerComboBoxObject);
  Result := Item.Value;
end;

procedure TCustomUMLCIntegerCombobox.setValueAt(Index: Integer; Value: Integer);
var Item: TUMLCIntegerComboBoxObject;
begin
  Item := (InternalItems.Objects[Index] as TUMLCIntegerComboBoxObject);
  Item.Value := Value;
end;

function TCustomUMLCIntegerCombobox.InternalObjectClass: TUMLCItemComboBoxObjectClass;
begin
  Result := TUMLCIntegerComboBoxObject;
end;

procedure TCustomUMLCIntegerCombobox.AddInteger
  (const ACaption: string; AValue: Integer);
var AObject: TUMLCIntegerComboBoxObject;
begin
  AObject := (InternalObjectClass().Create as TUMLCIntegerComboBoxObject);
  // create object to store object reference
  AObject.Data := nil;
  AObject.Value := AValue;
  // redirect object reference

  Items.AddObject(ACaption, AObject);
  // add substitute object with reference to real data object
end;

(* TUMLCDoubleIntegerComboBoxObject *)

constructor TUMLCDoubleIntegerComboBoxObject.Create;
begin
  inherited Create;
  FValue.Lo := 0;
  FValue.Hi := 0;
end;

destructor TUMLCDoubleIntegerComboBoxObject.Destroy;
begin
  FValue.Hi := 0;
  FValue.Lo := 0;
  inherited Destroy;
end;

(* TCustomUMLCDoubleIntegerCombobox *)

function TCustomUMLCDoubleIntegerCombobox.getValueAt(Index: Integer): TUMLCDoubleIntegerRecord;
var Item: TUMLCDoubleIntegerComboBoxObject;
begin
  Item := (InternalItems.Objects[Index] as TUMLCDoubleIntegerComboBoxObject);
  Result := Item.Value;
end;

procedure TCustomUMLCDoubleIntegerCombobox.setValueAt(Index: Integer; Value: TUMLCDoubleIntegerRecord);
var Item: TUMLCDoubleIntegerComboBoxObject;
begin
  Item := (InternalItems.Objects[Index] as TUMLCDoubleIntegerComboBoxObject);
  Item.Value := Value;
end;

function TCustomUMLCDoubleIntegerCombobox.InternalObjectClass: TUMLCItemComboBoxObjectClass;
begin
  Result := TUMLCDoubleIntegerComboBoxObject;
end;

procedure TCustomUMLCDoubleIntegerCombobox.AddDoubleInteger
  (const ACaption: string; AValue: TUMLCDoubleIntegerRecord);
var AObject: TUMLCDoubleIntegerComboBoxObject;
begin
  AObject := (InternalObjectClass().Create as TUMLCDoubleIntegerComboBoxObject);
  // create object to store object reference
  AObject.Data := nil;
  AObject.Value := AValue;
  // redirect object reference

  Items.AddObject(ACaption, AObject);
  // add substitute object with reference to real data object
end;

end.
