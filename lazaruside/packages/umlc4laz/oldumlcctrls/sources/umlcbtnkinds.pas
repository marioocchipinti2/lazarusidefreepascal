unit umlcbtnkinds;

interface
uses
  dummy;

type

(* TUMLCButtonKind *)

  TUMLCButtonKind =
  ( sdvbkCustom,
    sdvbkOK,
    sdvbkCancel,
    sdvbkHelp,
    sdvbkYes,
    sdvbkNo,
    sdvbkExit,
    sdvbkAbort,
    sdvbkRetry,
    sdvbkIgnore,
    sdvbkYesToAll,
    sdvbkNoToAll );

implementation

end.
