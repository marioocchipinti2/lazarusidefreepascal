unit umlcctrlenums;

interface
uses
  dummy;

type

(* TUMLCControlOrientation *)

  TUMLCControlOrientation =
  ( 
    corNone,
    corHorizontal,
    corVertical
  );

implementation

end.
