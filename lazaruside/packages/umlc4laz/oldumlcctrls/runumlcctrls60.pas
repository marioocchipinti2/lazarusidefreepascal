{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit runumlcctrls60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcaccolls, umlcbitbtns, umlcbtnkinds, umlcbuttons, umlccheckboxes, 
  umlccolorcombos, umlccomboboxes, umlccomboctrls, umlccomboedits, 
  umlccontroltreenodes, umlcctrlenums, umlcctrls, umlcdblinks, umlcdropbtns, 
  umlcdropmenubtns, umlcedits, umlcformcontrols, umlcforms, umlcgradpanels, 
  umlcheaderpanels, umlcitemcombos, umlcmaskeds, umlcmemos, 
  umlcmsgpaneltreeviews, umlcnormpanels, umlcpanels, umlcpaneltreeviews, 
  umlcpgctrls, umlcpublictreeviews, umlcspeedbtns, umlcstatetreeviews, 
  umlcstmemos, umlctabs, umlctogglebitbtns, umlctogglebuttons, 
  umlctogglespeedbtns, umlctoolbars, umlctreeviews;

implementation

end.
