rem echo "Hello World"
rem @ECHO OFF


set lazarusidepath=C:\softdev\lazaruside\lazaruside
rem set lazarusidepath=C:\softdev\lazaruside\lazaruside



cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcarrowdbnavs"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcarrownavs"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcbitbtns"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcbuttons"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlccheckboxes"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlccolorcombos"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlccomboboxes"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlccomboedits"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcdropbtns"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcdropmenubtns"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcedits"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcformcontrols"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcgradpanels"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcheaderpanels"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcitemcombos"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcmaskeds"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcmemos"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcmsgpaneltreeviews"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcpanels"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcpaneltabctrls"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcpaneltreeviews"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcpgctrls"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcpublictreeviews"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcspeedbtns"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcstatetreeviews"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlcstmemos"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlctogglebitbtns"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlctogglebuttons"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlctogglespeedbtns"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlctoolbar"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"

cd "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\resources\palette\umlctreeviews"
copy "*.lrs" "%lazarusidepath%\packages\umlc4laz\umlcctrls\laz60\design\*.lrs"



PAUSE
