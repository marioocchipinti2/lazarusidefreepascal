(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

(* "umlcstreams_language.inc" *)

{$UNDEF umlcstreams_language_english}
{$UNDEF umlcstreams_language_spanisheurope}
{$UNDEF umlcstreams_language_spanishlatam}
{$UNDEF umlcstreams_language_french}
{$UNDEF umlcstreams_language_german}
{$UNDEF umlcstreams_language_portuguese}
{$UNDEF umlcstreams_language_italian}

{$define umlcstreams_language_english}
{.define umlcstreams_language_spanisheurope}
{.define umlcstreams_language_spanishlatam}
{.define umlcstreams_language_french}
{.define umlcstreams_language_german}
{.define umlcstreams_language_portuguese}
{.define umlcstreams_language_italian}
