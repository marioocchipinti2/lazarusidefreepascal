(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstrfilestreams;

interface

uses
  SysUtils, Classes,
  umlcStreams, umlcRecStreams,
  dummy;

type

(* TCustomUMLCStrFileStream *)

  TCustomUMLCStrFileStream = class(TCustomUMLCRecordStream)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FConnected: Boolean;
    FText: string;

    function getText: string; virtual;

    procedure setText(const Value: string); virtual;
  public
    (* public declarations *)

    function IsInput: Boolean; override;
    function IsOutput: Boolean; override;

    function IsConnected: Boolean; override;

    function IsEoF: Boolean; override;

    function GetRecord(const Buffer: pointer): Boolean; override;
    function PutRecord(const Buffer: pointer): Boolean; override;

    function Connect: Boolean; override;
    function Disconnect: Boolean; override;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property Text: string
      read getText write setText;
  end;

(* TUMLCStrFilestream *)

  TUMLCStrFilestream = class(TCustomUMLCStrFileStream)
  published
    (* published declarations *)

    (* TCustomSDFileStream: *)

    property Text;
  end;

implementation

(* TCustomUMLCStrFileStream *)

function TCustomUMLCStrFileStream.getText: string;
begin
  Result := FText;
end;

procedure TCustomUMLCStrFileStream.setText(const Value: string);
begin
  if (Value <> FText) then
  begin
    FText := Value;
    FRecordCount := System.Length(FText);
  end;
end;

function TCustomUMLCStrFileStream.GetRecord(const Buffer: pointer): Boolean;
begin
  ClearRecord(Buffer);
  pansichar(Buffer)^ := FText[RecordIndex];
// (Buffer : pansichar)^ := FText[RecordIndex];
  Result := TRUE;
end;

function TCustomUMLCStrFileStream.PutRecord(const Buffer: pointer): Boolean;
begin
  FText[RecordIndex] := pansichar(Buffer)^;
//  FText[RecordIndex] := (Buffer : pansichar)^;
  Result := TRUE;
end;

function TCustomUMLCStrFileStream.IsInput: Boolean;
begin
  Result := TRUE;
end;

function TCustomUMLCStrFileStream.IsOutput: Boolean;
begin
  Result := FALSE;
end;

function TCustomUMLCStrFileStream.IsConnected: Boolean;
begin
  Result := FConnected;
end;

function TCustomUMLCStrFileStream.IsEoF: Boolean;
begin
  Result := (FRecordIndex > FRecordCount);
end;

function TCustomUMLCStrFileStream.Connect: Boolean;
begin
  FConnected := TRUE;
  FRecordIndex := 1;
  Result := TRUE;
end;

function TCustomUMLCStrFileStream.Disconnect: Boolean;
begin
  FConnected := FALSE;
  FRecordIndex := -1;
  Result := TRUE;
end;

constructor TCustomUMLCStrFileStream.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  AssignRecordSize(SizeOf(ansichar));
  FText := '';
  FConnected := FALSE;
end;

destructor TCustomUMLCStrFileStream.Destroy;
begin
  FConnected := FALSE;
  FText := '';
  inherited Destroy;
end;

end.
