(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfilestreams;

interface

uses
  SysUtils, Classes,
  umlcStreams, umlcRecStreams, umlcStateStreams,
  dummy;

(**
 ** Description:
 ** This unit implements a group of streams that support
 ** access to filesystem, reading or reading blocks of records.
 **)

type

  TFile = File;
  PFile = ^TFile;

(* TCustomUMLCFileStream *)

  TCustomUMLCFileStream = class(TCustomUMLCStateStream)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    (* fields declarations *)

    FFileRec: PFile;
    FPath: string;
  protected
    (* protected declarations *)

    (* accesors declarations *)

    function getPath(): string; virtual;

    procedure setPath(const Value: string); virtual;
  public
    (* public declarations *)

    (* read-only properties declarations *)

    function IsEoF(): Boolean; override;
  public
    (* public declarations *)

    function Connect(): Boolean; override;
    function Disconnect(): Boolean; override;

    function GetRecord(const Buffer: pointer): Boolean; override;
    function PutRecord(const Buffer: pointer): Boolean; override;
  public
    (* public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* public declarations *)

    (* properties declarations *)

    property Path: string
      read getPath write setPath;
  end;

(* TUMLCFileStream *)

  TUMLCFileStream = class(TCustomUMLCFileStream)
  published
    (* published declarations *)

    (* TCustomUMLCFileStream: *)

    property Path;
  end;

implementation

(* TCustomUMLCFileStream *)

function TCustomUMLCFileStream.getPath(): string;
begin
  Result := FPath;
end;

procedure TCustomUMLCFileStream.setPath(const Value: string);
begin
  if (Value <> FPath) then
  begin
    FPath := Value;
  end;
end;

function TCustomUMLCFileStream.IsEoF(): Boolean;
begin
  Result := System.EoF(FFileRec^);
end;

function TCustomUMLCFileStream.Connect(): Boolean;
begin
  FFileRec := System.New(PFile);
  // create temporal O.S. file handler

  System.Assign(FFileRec^, FPath);
  // assign file to O.S.

  case (FState) of
    ssClosed:
      begin
        FRecordIndex := 0;
      end;
    ssReset:
      begin
        System.Reset(FFileRec^, FRecordSize);
        FRecordCount := System.FileSize(FFileRec^);
        FRecordIndex := 0;
      end;
    ssRewrite:
      begin
        System.Rewrite(FFileRec^, FRecordSize);
        FRecordIndex := 0;
      end;
    ssAppend:
      begin
        System.Reset(FFileRec^, FRecordSize);
        FRecordIndex := System.FileSize(FFileRec^);
        FRecordCount := System.FileSize(FFileRec^);
        System.Seek(FFileRec^, FRecordIndex);
      end;
    else (*None*);
  end;

  Result := TRUE;
end;

function TCustomUMLCFileStream.Disconnect(): Boolean;
begin
  System.Close(FFileRec^);
  // close file

  System.Dispose(FFileRec); FFileRec := nil;
  // destroy temporal O.S. file handler

  Result := TRUE;
end;

function TCustomUMLCFileStream.GetRecord(const Buffer: pointer): Boolean;
var AmtTransferred: Integer;
begin
  ClearRecord(Buffer);
  System.Seek(FFileRec^, FRecordIndex);
  System.BlockRead(FFileRec^, Buffer^, 1, AmtTransferred);
  Result := TRUE;
end;

function TCustomUMLCFileStream.PutRecord(const Buffer: pointer): Boolean;
var AmtTransferred: Integer;
begin
  System.Seek(FFileRec^, FRecordIndex);
  System.BlockWrite(FFileRec^, Buffer^, 1, AmtTransferred);
  Result := TRUE;
end;

constructor TCustomUMLCFileStream.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FPath := '';
end;

destructor TCustomUMLCFileStream.Destroy();
begin
  FPath := '';
  inherited Destroy();
end;

end.
