(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansicharstreams;

interface

uses
  SysUtils, Classes,
  umlcStreams, umlcRecStreams, umlcRecRefStreams,
  dummy;

(**
 ** Description:
 ** This unit implements a group of streams that support
 ** access to one character (one byte) A.N.S.I. data.
 **
 ** This classes do not access files by themselves,
 ** they need to connected to other streams,
 ** similar to Java-style libraries.
 **)

type

(* TCustomUMLCANSICharStream *)

  TCustomUMLCANSICharStream = class(TCustomUMLCRecordReferenceStream)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    procedure setReference(const Value: TCustomUMLCStream); override;
  public
    (* public declarations *)

    function GetChar(var Buffer: ansichar): Boolean; virtual;
    function PutChar(const Buffer: ansichar): Boolean; virtual;

    function ReadChar(var Buffer: ansichar): Boolean; virtual;
    function WriteChar(const Buffer: ansichar): Boolean; virtual;

    procedure PrevChar;
    procedure NextChar;

    function Get(var Buffer: ansichar): Boolean;
    function Put(const Buffer: ansichar): Boolean;

    function Read(var Buffer: ansichar): Boolean;
    function Write(const Buffer: ansichar): Boolean;
  end;

(* TUMLCANSICharStream *)

  TUMLCANSICharStream = class(TCustomUMLCANSICharStream)
  published
    (* published declarations *)

    (* TCustomSDFileStream: *)

    property Reference;
  end;

implementation

(* TCustomUMLCANSICharStream *)

function TCustomUMLCANSICharStream.GetChar(var Buffer: ansichar): Boolean;
begin
  Result := Assigned(Reference);
  if (Result) then
  begin
    Result := RecordStream.GetRecord((@Buffer));
  end;
end;

function TCustomUMLCANSICharStream.PutChar(const Buffer: ansichar): Boolean;
begin
  Result := Assigned(Reference);
  if (Result) then
  begin
    Result := RecordStream.PutRecord((@Buffer));
  end;
end;

function TCustomUMLCANSICharStream.ReadChar(var Buffer: ansichar): Boolean;
begin
  Result := Assigned(Reference);
  if (Result) then
  begin
    Result := GetChar(Buffer);
    if (Result)
      then RecordStream.NextRecord;
  end;
end;

function TCustomUMLCANSICharStream.WriteChar(const Buffer: ansichar): Boolean;
begin
  Result := Assigned(Reference);
  if (Result) then
  begin
    Result := PutChar(Buffer);
    if (Result)
      then RecordStream.NextRecord;
  end;
end;

procedure TCustomUMLCANSICharStream.PrevChar;
begin
  PrevRecord;
end;

procedure TCustomUMLCANSICharStream.NextChar;
begin
  NextRecord;
end;

procedure TCustomUMLCANSICharStream.setReference(const Value: TCustomUMLCStream);
begin
  if (Value <> FReference) then
  begin
    FReference := Value;
    if (FReference <> nil)
      then RecordStream.AssignRecordSize(SizeOf(ansichar));
  end;
end;

function TCustomUMLCANSICharStream.Get(var Buffer: ansichar): Boolean;
begin
  Result := GetChar(Buffer);
end;

function TCustomUMLCANSICharStream.Put(const Buffer: ansichar): Boolean;
begin
  Result := PutChar(Buffer);
end;

function TCustomUMLCANSICharStream.Read(var Buffer: ansichar): Boolean;
begin
  Result := ReadChar(Buffer);
end;

function TCustomUMLCANSICharStream.Write(const Buffer: ansichar): Boolean;
begin
  Result := WriteChar(Buffer);
end;

end.
