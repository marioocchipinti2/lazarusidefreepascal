unit umlcstreamsregister;

interface
uses
  Classes,
{$IFDEF DELPHI}
{$IFDEF VER140}
  DesignIntf, DesignEditors,
{$ELSE}
  DsgnIntf,
{$ENDIF}
{$ENDIF}
{$IFDEF FPC}  
  LResources,
{$ENDIF} 
  umlcfilestreams,
  umlcansicharstreams,
  umlcshortansistrstreams,
  umlcansitextstreams,
  umlcstrfilestreams,
  umlcstrliststreams,
  dummy;

  procedure Register;

implementation

{$IFDEF DELPHI}
{$R 'umlcfilestreams.dcr'}
{$R 'umlcansicharstreams.dcr'}
{$R 'umlcansitextstreams.dcr'}
{$R 'umlcshortansistrstreams.dcr'}
{$R 'umlcstrfilestreams.dcr'}
{$R 'umlcstrliststreams.dcr'}
{$ENDIF} 

procedure Register;
const PaletteName = 'UMLCat Streams';
begin
  RegisterComponents(PaletteName, [TUMLCFileStream]);
  RegisterComponents(PaletteName, [TUMLCStrFileStream]);
  RegisterComponents(PaletteName, [TUMLCStringListStream]);

  RegisterComponents(PaletteName, [TUMLCAnsiCharStream]);
  RegisterComponents(PaletteName, [TUMLCShortAnsiStringStream]);
  RegisterComponents(PaletteName, [TUMLCAnsiTextStream]);
  RegisterComponents(PaletteName, [TUMLCAnsiSourceStream]);
end;

initialization
{$IFDEF FPC} 
{$I 'umlcfilestreams.lrs'}
{$I 'umlcansicharstreams.lrs'}
{.I 'umlcshortansistrstreams.lrs'}
{$I 'umlcansitextstreams.lrs'}
{$I 'umlcstrfilestreams.lrs'}
{$I 'umlcstrliststreams.lrs'}
{$ENDIF} 
end.
