readme.txt
==========

The "umlcstreams" folder contains the UMLCat set of libraries for the FreePascal &
Lazarus Programming Framework.

This package contains basic units and visual controls,
for input and output access.

