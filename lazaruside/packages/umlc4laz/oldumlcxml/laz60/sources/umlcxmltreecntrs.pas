(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

unit umlcxmltreecntrs;

interface

uses
  SysUtils, Classes,
  umlctreenodes, umlctreecntrs,
  umlcTagProps, umlctagattrdocs,
  umlcxmlFileTokens,
  dummy;

type

(* TUMLCXMLItem *)

  TUMLCXMLItem = class(TUMLCTagAttributesItem)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FToken: TUMLCXMLFileToken;

    procedure PropToText(var S: string);
  public
    (* public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  public
    (* public declarations *)

    function ToStartText(): string; override;
    function ToFinishText(): string; override;
    function ToText(): string; override;
  published
    (* published declarations *)

    property Token: TUMLCXMLFileToken
      read FToken write FToken;
  end;

(* TUMLCXMLCollection *)

  TUMLCXMLCollection = class(TUMLCTagAttributesCollection)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;
  public
    (* public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  published
    (* published declarations *)
  end;

(* TCustomUMLCXMLTreeContainer *)

  TCustomUMLCXMLTreeContainer = class(TCustomUMLCTagAttributesTreeContainer)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    function CreateCollectionByClass(): TUMLCContainerTreeCollection; override;
  public
    (* public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  end;

{ TUMLCXMLTreeContainer }

  TUMLCXMLTreeContainer = class(TCustomUMLCXMLTreeContainer)
  published
    (* published declarations *)

    { TCustomSDTreeContainer: }

    (*
    property OnInsert;
    property OnRemove;
    *)
  end;

implementation

(* TUMLCXMLItem *)

procedure TUMLCXMLItem.PropToText(var S: string);
var I, PropCount: Integer; EachProp: TUMLCTagProperty;
begin
  PropCount := Properties.Count;
  for I := 0 to Pred(PropCount) do
  begin
    EachProp := (Properties.Items[i] as TUMLCTagProperty);
    S := S + EachProp.Text;
  end;
  // agregar las propiedades con sus valores para el nodo indicado
  // add the properties with their values for the given node
end;

procedure TUMLCXMLItem.DoCreate();
begin
  inherited DoCreate();

  FToken := xmlfiletkNone;
end;

procedure TUMLCXMLItem.DoDestroy();
begin
  FToken := xmlfiletkNone;

  inherited DoDestroy();
end;

function TUMLCXMLItem.ToStartText(): string;
begin
  Result := FKeyword;
  // to-do: include properties

  Result := '';
  case (Token) of
    xmlfiletkEoF: ;
    xmlfiletkEoPg: ;
    xmlfiletkEoLn: ;
    xmlfiletkSpace: ;

    xmlfiletkComment: ;
    xmlfiletkEncoding: ;

    xmlfiletkStart:
      begin
        Result := Result + '<+' + FKeyword;
        PropToText(Result);
        Result := Result + '>';
      end;
    xmlfiletkFinish:
      Result := Result + '<-' + FKeyword + '>';
    xmlfiletkSingle:
      begin
        Result := '<';
        PropToText(Result);
        Result := Result + '>';
      end;

    xmlfiletkText: ;

    xmlfiletkEntity: ;

    xmlfiletkDecimal: ;
    xmlfiletkHexaDecimal: ;

    // xmlfiletkNone: ;
    else ;
  end;
end;

function TUMLCXMLItem.ToFinishText(): string;
begin
  Result := '<-' + FKeyword;
  if (Token = xmlfiletkEncoding)
    then Result := Result + ' ?>'
    else Result := Result + '>';
  // to-do: include properties
end;

function TUMLCXMLItem.ToText(): string;
begin
  Result := FKeyword;
  // to-do: include properties

  Result := '';
  case (Token) of
    xmlfiletkEoF: ;
    xmlfiletkEoPg:  Result := Result + #12;
    xmlfiletkEoLn:  Result := Result + #13;
    xmlfiletkSpace: Result := Result + #32;

    xmlfiletkText:  Result := FValue;

    xmlfiletkComment: ;
    xmlfiletkEncoding: ;

    xmlfiletkStart, // to-do
    xmlfiletkSingle:
      begin
        Result := '<' + FKeyword;
        PropToText(Result);
      Result := Result + '>';
      end;

    xmlfiletkEntity: ;

    xmlfiletkDecimal: ;
    xmlfiletkHexaDecimal: ;

    // xmlfiletkNone: ;
    // xmlfiletkStart: ;
    // xmlfiletkFinish: ;
    else ;
  end;
end;

(* TUMLCXMLCollection *)

function TUMLCXMLCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCXMLItem.Create();
  Result.DoCreate();
end;

procedure TUMLCXMLCollection.DoCreate();
begin
  inherited DoCreate();
  {Your Code...}
end;

procedure TUMLCXMLCollection.DoDestroy();
begin
  {Your Code...}
  inherited DoDestroy();
end;

(* TCustomUMLCXMLTreeContainer *)

function TCustomUMLCXMLTreeContainer.CreateCollectionByClass(): TUMLCContainerTreeCollection;
begin
  Result := TUMLCXMLCollection.Create();
end;

constructor TCustomUMLCXMLTreeContainer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  {Your Code...}
end;

destructor TCustomUMLCXMLTreeContainer.Destroy();
begin
  {Your Code...}
  inherited Destroy();
end;

end.
 
