(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)
 
unit umlcxmlfileansisymbols;

interface
uses
  SysUtils, Classes,
  umlcshortansistrs,
  umlcxmlfiletokens,
  umlcxmlfiletreenodetokens,
  dummy;

type
  TUMLCXMLFileANSISymbol = record
    Token:      TUMLCXMLFileToken;
    TreeToken:  TUMLCXMLFileTreeNodeToken;
    Text:       shortansistring;
    StartRow,
    StartCol,
    FinishRow,
    FinishCol:  Integer;
  end;
  PUMLCXMLFileANSISymbol = ^TUMLCXMLFileANSISymbol;

  TUMLCXMLFileANSISymbolrecord = record
    Token:      TUMLCXMLFileToken;
    TreeToken:  TUMLCXMLFileTreeNodeToken;
    Key:        Integer;
    StartRow,
    StartCol,
    FinishRow,
    FinishCol:  Integer;
  end;

  procedure Clear(var Symbol: TUMLCXMLFileANSISymbol);
  procedure Assign(var Dest, Source: TUMLCXMLFileANSISymbol);
  procedure Concat(var Dest, Source: TUMLCXMLFileANSISymbol);
  procedure Move(var Dest, Source: TUMLCXMLFileANSISymbol);
  procedure Exchange(var A, B: TUMLCXMLFileANSISymbol);

  procedure Compact
    (const Source: TUMLCXMLFileANSISymbol;
     SourceKey: Integer; var Dest: TUMLCXMLFileANSISymbolrecord);

implementation

procedure Clear(var Symbol: TUMLCXMLFileANSISymbol);
begin
  System.FillChar(Symbol, SizeOf(Symbol), 0);
end;

procedure Assign(var Dest, Source: TUMLCXMLFileANSISymbol);
begin
  Dest.Text      := Source.Text;
  Dest.Token     := Source.Token;
  Dest.TreeToken := Source.TreeToken;

  Dest.StartRow  := Source.StartRow;
  Dest.StartCol  := Source.StartCol;
  Dest.FinishRow := Source.FinishRow;
  Dest.FinishCol := Source.FinishCol;
end;

procedure Concat(var Dest, Source: TUMLCXMLFileANSISymbol);
begin
  Dest.Token := xmlfiletkText;
  Dest.Text  := Dest.Text + Source.Text;

  if (Dest.StartRow = 0) then
  begin
    Dest.StartRow := Source.StartRow;
  end;
  if (Dest.StartCol = 0) then
  begin
    Dest.StartCol := Source.StartCol;
  end;

  Dest.FinishRow := Source.FinishRow;
  Clear(Source);
end;

procedure Move(var Dest, Source: TUMLCXMLFileANSISymbol);
begin
  Dest := Source;
  Clear(Source);
end;

procedure Exchange(var A, B: TUMLCXMLFileANSISymbol);
var C: TUMLCXMLFileANSISymbol;
begin
  C := A;
  A := B;
  B := C;
end;

procedure Compact
 (const Source: TUMLCXMLFileANSISymbol;
  SourceKey: Integer; var Dest: TUMLCXMLFileANSISymbolrecord);
begin
  with Dest do
  begin
    Token     := Source.Token;
    Key       := SourceKey;
    StartRow  := Source.StartRow;
    StartCol  := Source.StartCol;
    FinishRow := Source.FinishRow;
    FinishCol := Source.FinishCol;
  end;
end;

end.
