(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

unit umlcxmlfilegroups;

interface
uses
  SysUtils,
  dummy;

type
  TUMLCXMLFileGroup = 
  (
  xmlfilegrDigit,       // '0' .. '9'
  xmlfilegrAF,          // 'a' .. 'f', 'A' .. 'F' ( used in hexadecimal numbers )
  xmlfilegrGWYZ,        // 'g' .. 'w', 'y', 'z', 'G'..'W', 'Y', 'Z' ( any identifier or text )
  xmlfilegrX,           // 'x', 'X' ( used in hexadecimal escape characters )
  xmlfilegrAmpersand,   // '&' ( entities* start marker )
  xmlfilegrPound,       // '#' ( used in decimal escape characters )
  xmlfilegrQuestion,    // '?' ( used in encoding tag )
  xmlfilegrExclamation, // '!' ( used in comments )
  xmlfilegrSlash,       // '/'
  xmlfilegrSemicolon,   // ';'  ( entities* finish marker )
  xmlfilegrLesser,      // '<'
  xmlfilegrGreater,     // '>'
  xmlfilegrSingleQuote, // pascal style string
  xmlfilegrDoubleQuote, // c/c++ style string
  xmlfilegrSpace,       // space
  xmlfilegrEoLn,        // End Of Line marker
  xmlfilegrEoPg,        // End Of Page marker
  xmlfilegrEoF,         // End Of File marker
  xmlfilegrElse         // any other character
  );
  PUMLCXMLFileGroup = ^TUMLCXMLFileGroup;

  function GroupToStr({copy} Value: TUMLCXMLFileGroup): string;
  function StrToGroup(const Value: string): TUMLCXMLFileGroup;

  function CharToGroup(const Value: ansichar): TUMLCXMLFileGroup;

implementation

const EnumToStrArray: array[TUMLCXMLFileGroup] of string = 
(
  'xmlfilegrDigit',
  'xmlfilegrAF',
  'xmlfilegrGWYZ',
  'xmlfilegrX',
  'xmlfilegrAmpersand',
  'xmlfilegrPound',
  'xmlfilegrQuestion',
  'xmlfilegrExclamation',
  'xmlfilegrSlash',
  'xmlfilegrSemicolon',
  'xmlfilegrLesser',
  'xmlfilegrGreater',
  'xmlfilegrSingleQuote',
  'xmlfilegrDoubleQuote',
  'xmlfilegrSpace',
  'xmlfilegrEoLn',
  'xmlfilegrEoPg',
  'xmlfilegrEoF',
  'xmlfilegrElse'
);

function GroupToStr({copy} Value: TUMLCXMLFileGroup): string;
begin
  if (Value < Low(TUMLCXMLFileGroup))
    then Value := Low(TUMLCXMLFileGroup);
  if (Value > High(TUMLCXMLFileGroup))
    then Value := High(TUMLCXMLFileGroup);
  Result := EnumToStrArray[Value];
end;

function StrToGroup(const Value: string): TUMLCXMLFileGroup;
var i: TUMLCXMLFileGroup; Found: Boolean;
begin
  i := Low(TUMLCXMLFileGroup); Found := FALSE;
  while ((i <= High(TUMLCXMLFileGroup)) and (not Found)) do
  begin
    Found := SameText(EnumToStrArray[i], Value);
    Inc(i);
  end;

  if (Found)
    then Result := Pred(i)
    else Result := Low(TUMLCXMLFileGroup);
end;

function CharToGroup(const Value: ansichar): TUMLCXMLFileGroup;
begin
  case (Value) of
    '0'..'9': Result := xmlfilegrDigit;
    'A'..'F',
    'a'..'f': Result := xmlfilegrAF;
    ':',
    'G'..'W', 'Y', 'Z',
    'g'..'w', 'y', 'z': Result := xmlfilegrGWYZ;
    'x', 'X': Result := xmlfilegrX;
    '&':      Result := xmlfilegrAmpersand;
    '#':      Result := xmlfilegrPound;
    '?':      Result := xmlfilegrQuestion;
    '!':      Result := xmlfilegrExclamation;
    '/':      Result := xmlfilegrSlash;
    ';':      Result := xmlfilegrSemicolon;
    '<':      Result := xmlfilegrLesser;
    '>':      Result := xmlfilegrGreater;
    #39:      Result := xmlfilegrSingleQuote;
    #34:      Result := xmlfilegrDoubleQuote;
    #32, #8,#9:      Result := xmlfilegrSpace;  // <-- hack !!!
    #13, #10: Result := xmlfilegrEoLn;
    #12:      Result := xmlfilegrEoPg;
    #26:      Result := xmlfilegrEoF;
    else      Result := xmlfilegrElse;
  end;
end;


end.
