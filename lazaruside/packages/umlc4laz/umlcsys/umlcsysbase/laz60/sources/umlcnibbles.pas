(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnibbles;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  dummy;

// ---

const

 MOD_umlcnibbles : TUMLCModule =
   ($B5,$53,$AB,$66,$CA,$7F,$47,$43,$BE,$EF,$A9,$EE,$4B,$FE,$4F,$84);

// ---




implementation




end.

