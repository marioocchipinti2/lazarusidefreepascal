(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcobjlists;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface
uses
  SysUtils,
  umlcuuids,
  umlcmodules, umlctypes,
  umlcstdtypes,
  umlcresobjlists,
  dummy;

// ---

const

  MOD_umlcobjlists : TUMLCModule =
   ($85,$68,$E3,$49,$47,$B9,$B8,$41,$B7,$74,$BC,$D0,$90,$F2,$EC,$C4);

// ---

const

  // ---

  ID_TCustomUMLCList : TUMLCType =
    ($0F,$19,$01,$05,$E0,$A4,$5D,$45,$84,$09,$94,$B0,$F1,$52,$1F,$BF);

  ID_TUMLCPointerList : TUMLCType =
    ($B8,$B6,$D8,$93,$D6,$4E,$0B,$4B,$9D,$C3,$D5,$FE,$13,$AD,$52,$EB);

  ID_TUMLCOwnerList : TUMLCType =
    ($59,$A8,$D4,$1F,$89,$16,$06,$43,$B5,$46,$CB,$D2,$E5,$FB,$52,$8D);

  ID_TUMLCObjectList : TUMLCType =
    ($BE,$5D,$76,$10,$64,$DF,$8E,$4E,$8A,$5F,$56,$6F,$97,$0F,$76,$08);

  // ---


const

(* Maximum TList size *)

  MaxListSize = Maxint div 16;

(* List error *)

  IndexNotFound = -1;

  IgnoreIndex   = -1;

type

(* TUMLCListError *)

  TUMLCListError = class(Exception)

  end;

(* TUMLCPointerArray *)

  PUMLCPointerArray = ^TUMLCPointerArray;
  TUMLCPointerArray = array[0..MaxListSize - 1] of pointer;

  TListSortCompare = function (Item1, Item2: pointer): Integer;
  TListNotification = (lnAdded, lnExtracted, lnDeleted);

(* TCustomUMLCList *)

  TCustomUMLCList =
   class(TObject)
  private
    { Private declarations }

    FCapacity: Integer;
  protected
    { Protected declarations }

    FCount:  Integer;
    FList:   PUMLCPointerArray;

    function getInternalItems
      (Index: Integer): pointer;
    function getCapacity(): Integer;
    function getCount(): Integer;
    function getList(): PUMLCPointerArray;

    procedure setInternalItems
      (Index: Integer; Item: pointer);
    procedure setCapacity
      (const NewCapacity: Integer);
    procedure setCount
      (const Value: Integer);
    procedure setList
      (const Value: PUMLCPointerArray);

    procedure Grow; virtual;
//    procedure Notify(Ptr: pointer; Action: TListNotification); virtual;

    function InternalIndexOf
      (Item: pointer): Integer;
    function InternalFirst(): pointer;
    function InternalLast(): pointer;

    function InternalInsertFirst
      (const AItem: pointer): Integer;
    function InternalInsertLast
      (const AItem: pointer): Integer;

    procedure InternalInsertAt
      (Index: Integer; Item: pointer);

    procedure InternalEmpty();

    procedure InternalDelete();
    procedure InternalDeleteAt
      (const AIndex: Integer);

    function InternalRemove
      (const AItem: pointer): Integer;

    function InternalExtractFirst(): pointer;
    function InternalExtractLast(): pointer;

    function InternalExtractAt
      (const AIndex: Integer): pointer;
  public
    { public declarations }

    constructor Create(); virtual;
    destructor Destroy(); override;
  public
    { public declarations }

    class procedure ThrowError
      (const Msg: string; Data: Integer); overload; virtual;
    class procedure ThrowError
      (Msg: PResStringRec; Data: Integer); overload;
  public
    { public declarations }

    procedure Exchange(Index1, Index2: Integer);
    procedure Move(CurIndex, NewIndex: Integer);

    function IsEmpty(): Boolean;
    function Expand(): TCustomUMLCList;

    procedure Pack();
    procedure Sort(Compare: TListSortCompare);

    procedure Copy
      (const Source: TCustomUMLCList);
    procedure Intersect
      (const Source: TCustomUMLCList);
    procedure Union
      (const Source: TCustomUMLCList);

    { Unpublished declarations }

    property Capacity: Integer
      read getCapacity write setCapacity;
    property Count: Integer
      read getCount write setCount;
    property InternalItems[Index: Integer]: pointer
      read getInternalItems write setInternalItems; default;
    property List: PUMLCPointerArray
      read getList write setList;
  end;

{ TUMLCPointerListFirstThatFunc }

  TUMLCPointerListFirstThatFunc =
   function (const AItem: pointer; const AParam: pointer): Boolean of object;

{ TUMLCPointerListForEachProc }

  TUMLCPointerListForEachProc =
   procedure (const AItem: pointer; const AParam: pointer) of object;

(* TUMLCPointerList *)

  TUMLCPointerList =
   class(TCustomUMLCList)
  private
    { Private declarations }
  protected
    { Protected declarations }

    function getItems
      (AIndex: Integer): pointer;

    procedure setItems
      (AIndex: Integer; AItem: pointer);
  public
    { Public declarations }

    function IndexOf
      (const AItem: pointer): Integer;
    function First(): pointer;
    function Last(): pointer;

    function InsertFirst
      (const AItem: pointer): Integer;
    function InsertLast
      (const AItem: pointer): Integer;
    procedure InsertAt
      (const AIndex: Integer; const AItem: pointer);

    function ExtractFirst(): pointer;
    function ExtractLast(): pointer;

    function ExtractAt
      (const AIndex: Integer): pointer;

    procedure Delete();
    procedure DeleteAt
      (const AIndex: Integer);

    function Remove
      (const AItem: pointer): Integer;

    procedure Empty();

    function FirstThat
      (const Func: TUMLCPointerListFirstThatFunc; const AParam: pointer): pointer;

    procedure ForEach
      (const Proc: TUMLCPointerListForEachProc; const AParam: pointer);
    procedure ForBack
      (const Proc: TUMLCPointerListForEachProc; const AParam: pointer);

    property Items[Index: Integer]: pointer
      read getItems write setItems; default;
  end;

{ TUMLCOwnerList }

  TUMLCOwnerList = class(TCustomUMLCList)
  private
    { Private declarations }

    FOwnsObjects: Boolean;
  protected
    { Protected declarations }

//    function InternalCreateItem: pointer; virtual; abstract;
//    procedure InternalDestroyItem(const AItem: pointer); virtual; abstract;
  public
    { Public declarations }

    procedure DeleteAt
      (const AIndex: Integer);
    procedure Empty();
  public
    { Public declarations }

    property OwnsObjects: Boolean
      read FOwnsObjects write FOwnsObjects;
  end;

{ TUMLCObjectListInmediate }

  TUMLCObjectListInmediate =
    procedure (const AItem: TObject;
     const AParam: pointer) of object;

(* TUMLCObjectList *)

  TUMLCObjectList =
   class(TUMLCOwnerList)
  private
    { Private declarations }
  protected
    { Protected declarations }

    function getItems
      (AIndex: Integer): TObject;

    procedure setItems
      (AIndex: Integer; AItem: TObject);
  public
    { Public declarations }

    function IndexOf
      (const AItem: TObject): Integer;
    function First(): TObject;
    function Last(): TObject;

    function InsertFirst
      (const AItem: TObject): Integer;
    function InsertLast
      (const AItem: TObject): Integer;
    procedure InsertAt
      (const AIndex: Integer; const AItem: TObject);

    function ExtractFirst(): TObject;
    function ExtractLast(): TObject;

    function ExtractAt
      (const AIndex: Integer): TObject;

    procedure Delete();
    procedure DeleteAt
      (const AIndex: Integer);

    function Remove
      (const AItem: TObject): Integer;

    procedure Empty();

    procedure ForEach
      (const Proc: TUMLCObjectListInmediate; const AParam: pointer);
    procedure ForBack
      (const Proc: TUMLCObjectListInmediate; const AParam: pointer);

    property Items[AIndex: Integer]: TObject
      read getItems write setItems; default;
  end;

implementation

(* TCustomUMLCList *)

function TCustomUMLCList.getInternalItems
  (Index: Integer): pointer;
begin
  if ((Index < 0) or (Index >= FCount)) then
  begin
    ThrowError(@err_ListIndexError, Index);
  end;

  Result := FList^[Index];
end;

function TCustomUMLCList.getCapacity(): Integer;
begin
  Result := FCapacity;
end;

function TCustomUMLCList.getCount(): Integer;
begin
  Result := FCount;
end;

function TCustomUMLCList.getList(): PUMLCPointerArray;
begin
  Result := FList;
end;

procedure TCustomUMLCList.setInternalItems
  (Index: Integer; Item: pointer);
//var Temp: pointer;
begin
  if ((Index < 0) or (Index >= FCount)) then
  begin
    ThrowError(@err_ListIndexError, Index);
  end;

  if (Item <> FList^[Index]) then
  begin
//    Temp := FList^[Index];
    FList^[Index] := Item;
//    if (Temp <> nil)
//        then Notify(Temp, lnDeleted);
//    if (Item <> nil)
//        then Notify(Item, lnAdded);
  end;
end;

procedure TCustomUMLCList.setCapacity
  (const NewCapacity: Integer);
begin
  if ((NewCapacity < FCount) or (NewCapacity > MaxListSize)) then
  begin
    ThrowError(@err_ListIndexError, NewCapacity);
  end;

  if (NewCapacity <> FCapacity) then
  begin
    ReallocMem(FList, NewCapacity * SizeOf(pointer));
    FCapacity := NewCapacity;
  end;
end;

procedure TCustomUMLCList.setCount
  (const Value: Integer);
var NewCount, I: Integer;
begin
  NewCount := Value;
  if ((NewCount < 0) or (NewCount > MaxListSize)) then
  begin
    ThrowError(@err_ListIndexError, NewCount);
  end;

  if (NewCount > FCapacity) then
  begin
    Self.SetCapacity(NewCount);
  end;

  if (NewCount > FCount) then
  begin
    FillChar(FList^[FCount], (NewCount - FCount) * SizeOf(pointer), 0)
  end else
    // to while
    for I := FCount - 1 downto NewCount do
    begin
      Self.InternalDeleteAt(I);
    end;

  FCount := NewCount;
end;

procedure TCustomUMLCList.setList
  (const Value: PUMLCPointerArray);
begin
  FList := Value;
end;

constructor TCustomUMLCList.Create();
begin
  inherited Create();
end;

destructor TCustomUMLCList.Destroy();
begin
  Self.InternalEmpty();
  inherited Destroy();
end;

class procedure TCustomUMLCList.ThrowError
  (const Msg: string; Data: Integer);

  {$ifdef Delphi}
  function ReturnAddr(): pointer;
  asm
          MOV     EAX,[EBP+4]
  end;
  {$endif}

begin
  {$ifdef Delphi}
  raise TUMLCListError.CreateFmt(Msg, [Data]) at ReturnAddr();
  {$else}
  raise TUMLCListError.CreateFmt(Msg, [Data]);
  {$endif}
end;

class procedure TCustomUMLCList.ThrowError
  (Msg: PResStringRec; Data: Integer);
begin
  TCustomUMLCList.ThrowError(LoadResString(Msg), Data);
end;

procedure TCustomUMLCList.Exchange(Index1, Index2: Integer);
var Item: pointer;
begin
  if ((Index1 < 0) or (Index1 >= FCount)) then
  begin
    Self.ThrowError(@err_ListIndexError, Index1);
  end;

  if ((Index2 < 0) or (Index2 >= FCount)) then
  begin
    Self.ThrowError(@err_ListIndexError, Index2);
  end;

  Item := FList^[Index1];
  FList^[Index1] := FList^[Index2];
  FList^[Index2] := Item;
end;

procedure TCustomUMLCList.Move(CurIndex, NewIndex: Integer);
var Item: pointer;
begin
  if (CurIndex <> NewIndex) then
  begin
    if ((NewIndex < 0) or (NewIndex >= FCount)) then
    begin
      Self.ThrowError(@err_ListIndexError, NewIndex);
    end;

    Item := Self.getInternalItems(CurIndex);
    FList^[CurIndex] := nil;
    Self.InternalDeleteAt(CurIndex);
    Self.InternalInsertAt(NewIndex, nil);
    FList^[NewIndex] := Item;
  end;
end;

function TCustomUMLCList.IsEmpty(): Boolean;
begin
  Result := (Count < 1);
end;

function TCustomUMLCList.Expand(): TCustomUMLCList;
begin
  if (FCount = FCapacity) then
  begin
    Self.Grow();
  end;

  Result := Self;
end;

procedure TCustomUMLCList.Grow;
var Delta: Integer;
begin
  if (FCapacity > 64) then
  begin
    Delta := FCapacity div 4
  end else
  begin
    if (FCapacity > 8)
      then Delta := 16
      else Delta := 4;
  end;

  Self.SetCapacity(FCapacity + Delta);
end;

procedure TCustomUMLCList.Pack();
var I: Integer;
begin
  // while
  for I := FCount - 1 downto 0 do
  begin
    if (Self.InternalItems[I] = nil) then
    begin
      Self.InternalDeleteAt(I);
    end;
  end;
end;

procedure QuickSort
 (SortList: PUMLCPointerArray; L, R: Integer; Compare: TListSortCompare);
var I, J: Integer; P, T: pointer;
begin
  repeat
    I := L;
    J := R;
    P := SortList^[(L + R) shr 1];
    repeat
      while Compare(SortList^[I], P) < 0 do
      begin
        Inc(I);
      end;

      while Compare(SortList^[J], P) > 0 do
      begin
        Dec(J);
      end;

      if (I <= J) then
      begin
        T := SortList^[I];
        SortList^[I] := SortList^[J];
        SortList^[J] := T;
        Inc(I);
        Dec(J);
      end;
    until (I > J);

    if (L < J) then
    begin
      QuickSort(SortList, L, J, Compare);
    end;

    L := I;
  until (I >= R);
end;

procedure TCustomUMLCList.Sort(Compare: TListSortCompare);
begin
  if ((FList <> nil) and (Count > 0)) then
  begin
    QuickSort(FList, 0, Count - 1, Compare);
  end;
end;

(*
procedure TCustomUMLCList.Notify(Ptr: pointer; Action: TListNotification);
begin
end;
*)

function TCustomUMLCList.InternalIndexOf
  (Item: pointer): Integer;
begin
  Result := 0;
  while( (Result < FCount) and (FList^[Result] <> Item)) do
  begin
    Inc(Result);
  end;

  if (Result = FCount) then
  begin
    Result := IndexNotFound;
  end;
end;

function TCustomUMLCList.InternalFirst(): pointer;
begin
  Result := getInternalItems(0);
end;

function TCustomUMLCList.InternalLast(): pointer;
begin
  Result :=getInternalItems(FCount - 1);
end;

function TCustomUMLCList.InternalInsertFirst
  (const AItem: pointer): Integer;
begin
  Result := 0;
  Self.InternalInsertAt(0, AItem);
end;

function TCustomUMLCList.InternalInsertLast
  (const AItem: pointer): Integer;
begin
  Result := FCount;
  if (Result = FCapacity) then
  begin
    Grow();
  end;

  FList^[Result] := AItem;
  Inc(FCount);
//if (Item <> nil)
//  then Notify(Item, lnAdded);
end;

procedure TCustomUMLCList.InternalInsertAt
  (Index: Integer; Item: pointer);
begin
  if ((Index < 0) or (Index > FCount)) then
  begin
    ThrowError(@err_ListIndexError, Index);
  end;

  if (FCount = FCapacity) then
  begin
    Grow();
  end;

  if (Index < FCount) then
  begin
    System.Move(FList^[Index], FList^[Index + 1],
      (FCount - Index) * SizeOf(pointer));
  end;

  FList^[Index] := Item;
  Inc(FCount);
//if (Item <> nil) then
//  Notify(Item, lnAdded);
end;

procedure TCustomUMLCList.InternalEmpty();
begin
  SetCount(0);
  SetCapacity(0);
end;

procedure TCustomUMLCList.InternalDelete();
var AIndex: Integer;
begin
  AIndex :=
    Pred(Self.Count);
  Self.InternalDeleteAt(AIndex);
end;

procedure TCustomUMLCList.InternalDeleteAt
  (const AIndex: Integer);
//var Temp: pointer;
begin
  if ((AIndex < 0) or (AIndex >= FCount)) then
  begin
    ThrowError(@err_ListIndexError, AIndex);
  end;

//  Temp := InternalItems[AIndex];
  Dec(FCount);

  if (AIndex < FCount) then
  begin
    System.Move(FList^[AIndex + 1], FList^[AIndex],
      (FCount - AIndex) * SizeOf(pointer));
  end;
//if (Temp <> nil)
//  then Notify(Temp, lnDeleted);
end;

function TCustomUMLCList.InternalRemove
  (const AItem: pointer): Integer;
begin
  Result := InternalIndexOf(AItem);
  if (Result >= 0) then
  begin
    InternalDeleteAt(Result);
  end;
end;

function TCustomUMLCList.InternalExtractFirst(): pointer;
begin
  Result := nil;

  Result :=
    Self.InternalExtractAt(0);
end;

function TCustomUMLCList.InternalExtractLast(): pointer;
var AIndex: Integer;
begin
  Result := nil;

  AIndex :=
    Pred(Self.Count);
  Result :=
    Self.InternalExtractAt(AIndex);
end;

function TCustomUMLCList.InternalExtractAt
  (const AIndex: Integer): pointer;
begin
  Result := nil;
  if ((AIndex >= 0) and (AIndex <= Count)) then
  begin
    Result := getInternalItems(AIndex);
    FList^[AIndex] := nil;
    Self.InternalDeleteAt(AIndex);
//  Notify(Result, lnExtracted);
  end;
end;

procedure TCustomUMLCList.Copy
  (const Source: TCustomUMLCList);
var I: Integer;
begin
  InternalEmpty();
  Capacity := Source.Capacity;
  for I := 0 to Pred(Source.Count) do
  begin
    Self.InternalInsertLast(Source[I]);
  end;
end;

procedure TCustomUMLCList.Intersect
  (const Source: TCustomUMLCList);
var I, EachIndex: Integer;
begin
  // while
  for I := Count - 1 downto 0 do
  begin
    EachIndex := Source.InternalIndexOf(InternalItems[I]);
    if (EachIndex = IndexNotFound) then
    begin
      Self.InternalDeleteAt(I);
    end;
  end;
end;

procedure TCustomUMLCList.Union
  (const Source: TCustomUMLCList);
var I, EachIndex: Integer;
begin
  for I := 0 to Source.Count - 1 do
  begin
    EachIndex := Source.InternalIndexOf(InternalItems[I]);
    if (EachIndex = IndexNotFound) then
    begin
      Self.InternalInsertLast(Source[I]);
    end;
  end;
end;

(* TUMLCPointerList *)

function TUMLCPointerList.getItems
  (AIndex: Integer): pointer;
begin
  Result :=
    Self.getInternalItems(AIndex);
end;

procedure TUMLCPointerList.setItems
  (AIndex: Integer; AItem: pointer);
begin
  Self.setInternalItems
    (AIndex, AItem);
end;

function TUMLCPointerList.IndexOf
  (const AItem: pointer): Integer;
begin
  Result := Self.InternalIndexOf(AItem);
end;

function TUMLCPointerList.First(): pointer;
begin
  Result := Self.InternalFirst();
end;

function TUMLCPointerList.Last(): pointer;
begin
  Result := Self.InternalLast();
end;

function TUMLCPointerList.InsertFirst
  (const AItem: pointer): Integer;
begin
  Result :=
    Self.InternalInsertFirst(AItem);
end;

function TUMLCPointerList.InsertLast
  (const AItem: pointer): Integer;
begin
  Result :=
    Self.InternalInsertLast(AItem);
end;

procedure TUMLCPointerList.InsertAt
  (const AIndex: Integer; const AItem: pointer);
begin
  Self.InternalInsertAt(AIndex, AItem);
end;

function TUMLCPointerList.ExtractFirst(): pointer;
begin
  Result :=
    Self.InternalExtractFirst();
end;

function TUMLCPointerList.ExtractLast(): pointer;
begin
  Result :=
    Self.InternalExtractLast();
end;

function TUMLCPointerList.ExtractAt
  (const AIndex: Integer): pointer;
begin
  Result :=
    Self.InternalExtractAt(AIndex);
end;

procedure TUMLCPointerList.Delete();
begin
  Self.InternalDelete();
end;

procedure TUMLCPointerList.DeleteAt
  (const AIndex: Integer);
begin
  Self.InternalDeleteAt(AIndex);
end;

function TUMLCPointerList.Remove
  (const AItem: pointer): Integer;
begin
  Result :=
    Self.InternalRemove(AItem);
end;

procedure TUMLCPointerList.Empty();
begin
  Self.InternalEmpty();
end;

function TUMLCPointerList.FirstThat
  (const Func: TUMLCPointerListFirstThatFunc;
   const AParam: pointer): pointer;
var Found: Boolean; Index, LastIndex: Integer; Item: pointer;
begin
  Result := nil;

  Found := false;
  Index := 0;
  LastIndex  := Count;
  while ((not Found) and (Index < LastIndex)) do
  begin
    Item := Self.getInternalItems(Index);
    Found := Func(Item, AParam);
    Inc(Index);
  end;

  if (Found) then
  begin
    Result := Item;
  end;
end;

procedure TUMLCPointerList.ForEach
  (const Proc: TUMLCPointerListForEachProc; const AParam: pointer);
var Index: Integer; Item: pointer;
begin
  for Index := 0 to Pred(Count) do
  begin
    Item := getInternalItems(Index);
    Proc(Item, AParam);
  end;
end;

procedure TUMLCPointerList.ForBack
  (const Proc: TUMLCPointerListForEachProc; const AParam: pointer);
var Index: Integer; Item: pointer;
begin
  for Index := Pred(Count) downto 0 do
  begin
    Item := getInternalItems(Index);
    Proc(Item, AParam);
  end;
end;

{ TUMLCOwnerList }

procedure TUMLCOwnerList.DeleteAt
  (const AIndex: Integer);
//var Item: TObject;
begin
//  Item := getInternalItems(Index);
//  InternalDestroyItem(Item);
  InternalDeleteAt(AIndex);
  { Goal: Destroys of the list by its index .}
  { Objetivo: Destruye un elemento de la lista por su indice .}
end;

procedure TUMLCOwnerList.Empty();
var AIndex: Integer;
begin
  // to while
  for AIndex := Pred(Count) downto 0 do
  begin
    InternalDeleteAt(AIndex);
  end;

  InternalEmpty();
  { Goal: Destroys all the items of the list.}
  { Objetivo: Destruye todos los elementos de la lista .}
end;

(* TUMLCObjectList *)

function TUMLCObjectList.getItems
  (AIndex: Integer): TObject;
begin
  Result :=
    TObject(getInternalItems(AIndex));
end;

procedure TUMLCObjectList.setItems
  (AIndex: Integer; AItem: TObject);
begin
  setInternalItems(AIndex, AItem);
end;

function TUMLCObjectList.IndexOf
  (const AItem: TObject): Integer;
begin
  Result :=
    InternalIndexOf(AItem);
end;

function TUMLCObjectList.First(): TObject;
begin
  Result := TObject(InternalFirst());
end;

function TUMLCObjectList.Last(): TObject;
begin
  Result :=
    TObject(InternalLast());
end;

function TUMLCObjectList.InsertFirst
  (const AItem: TObject): Integer;
begin
  Result :=
    Integer(Self.InternalInsertFirst(AItem));
end;

function TUMLCObjectList.InsertLast
  (const AItem: TObject): Integer;
begin
  Result :=
    Integer(Self.InternalInsertLast(AItem));
end;

procedure TUMLCObjectList.InsertAt
  (const AIndex: Integer; const AItem: TObject);
begin
  Self.InternalInsertAt
    (AIndex, AItem);
end;

function TUMLCObjectList.ExtractFirst(): TObject;
begin
  Result :=
    TObject(Self.InternalExtractFirst());
end;

function TUMLCObjectList.ExtractLast(): TObject;
begin
  Result :=
    TObject(Self.InternalExtractLast());
end;

function TUMLCObjectList.ExtractAt
  (const AIndex: Integer): TObject;
begin
  Result :=
    TObject(Self.InternalExtractAt(AIndex));
end;

procedure TUMLCObjectList.Delete();
begin
  Self.InternalDelete();
end;

procedure TUMLCObjectList.DeleteAt
  (const AIndex: Integer);
begin
  Self.InternalDeleteAt(AIndex);
end;

function TUMLCObjectList.Remove
  (const AItem: TObject): Integer;
begin
  Result :=
    Self.InternalRemove(AItem);
end;

procedure TUMLCObjectList.Empty();
begin
  Self.InternalEmpty();
end;

procedure TUMLCObjectList.ForEach
  (const Proc: TUMLCObjectListInmediate; const AParam: pointer);
var AIndex: Integer; AItem: TObject;
begin
  for AIndex := 0 to Pred(Count) do
  begin
    AItem :=
      TObject(Self.getInternalItems(AIndex));
    Proc(AItem, AParam);
  end;
end;

procedure TUMLCObjectList.ForBack
  (const Proc: TUMLCObjectListInmediate; const AParam: pointer);
var AIndex: Integer; AItem: TObject;
begin
  for AIndex := Pred(Count) downto 0 do
  begin
    AItem :=
      TObject(Self.getInternalItems(AIndex));
    Proc(AItem, AParam);
  end;
end;

end.
