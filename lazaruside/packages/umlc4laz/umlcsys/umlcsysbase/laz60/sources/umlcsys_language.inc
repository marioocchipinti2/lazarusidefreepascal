(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

(* "umlcsys_language.incpas" *)

{$UNDEF umlcsys_language_english}
{$UNDEF umlcsys_language_spanisheurope}
{$UNDEF umlcsys_language_spanishlatam}
{$UNDEF umlcsys_language_french}
{$UNDEF umlcsys_language_german}
{$UNDEF umlcsys_language_portuguese}
{$UNDEF umlcsys_language_italian}

{$DEFINE umlcsys_language_english}
{.DEFINE umlcsys_language_spanisheurope}
{.DEFINE umlcsys_language_spanishlatam}
{.DEFINE umlcsys_language_french}
{.DEFINE umlcsys_language_german}
{.DEFINE umlcsys_language_portuguese}
{.DEFINE umlcsys_language_italian}

{.WARNING "umlcsys_language.incpas"}

{.FATAL "HELLO MACRO"}
