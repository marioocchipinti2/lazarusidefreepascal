(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsinttypes;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

(**
 **************************************************************************
 ** Description:
 ** To implement math oriented signed integer types.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcsinttypes : TUMLCModule =
   ($52,$6C,$6F,$1D,$9C,$D8,$05,$49,$8F,$D6,$DD,$02,$2E,$AF,$B8,$E7);

// ---

const

// ---

  ID_umlcsint_8 : TUMLCType =
    ($DF,$3C,$F8,$45,$2F,$30,$BF,$4A,$89,$FD,$FF,$C6,$FE,$6C,$28,$BB);

  ID_umlcsint_16 : TUMLCType =
    ($4F,$B6,$CE,$53,$87,$61,$2C,$47,$98,$F5,$36,$F8,$36,$0A,$FE,$C2);

  ID_umlcsint_32 : TUMLCType =
    ($3C,$E3,$C0,$41,$FD,$64,$2E,$40,$A9,$AB,$73,$CB,$3A,$7A,$06,$69);

  ID_umlcsint_64 : TUMLCType =
    ($33,$38,$CF,$50,$9C,$1B,$28,$47,$9F,$32,$1E,$98,$71,$D1,$70,$C9);

  ID_umlcsint_128 : TUMLCType =
    ($3E,$48,$76,$60,$F3,$6C,$FA,$40,$A2,$A4,$65,$00,$F6,$82,$26,$A1);

  ID_umlcsint_256 : TUMLCType =
    ($B0,$88,$AA,$70,$A9,$20,$A4,$49,$8F,$F7,$75,$16,$F1,$00,$BF,$9A);

  ID_umlcsint_512 : TUMLCType =
    ($B4,$C5,$51,$69,$FF,$E9,$07,$45,$A5,$C4,$A0,$3D,$73,$88,$A4,$CA);

// ---

const

// ---

  ID_TUMLCSignedIntegerTypes : TUMLCType =
    ($84,$98,$03,$EA,$F4,$E1,$59,$4A,$81,$DD,$ED,$17,$5A,$A5,$1E,$9D);

// ---

const

// ---

  ID_umlctsint_8 : TUMLCType =
    ($DF,$3C,$F8,$45,$2F,$30,$BF,$4A,$89,$FD,$FF,$C6,$FE,$6C,$28,$BB);

  ID_umlctsint_16 : TUMLCType =
    ($4F,$B6,$CE,$53,$87,$61,$2C,$47,$98,$F5,$36,$F8,$36,$0A,$FE,$C2);

  ID_umlctsint_32 : TUMLCType =
    ($3C,$E3,$C0,$41,$FD,$64,$2E,$40,$A9,$AB,$73,$CB,$3A,$7A,$06,$69);

  ID_umlctsint_64 : TUMLCType =
    ($33,$38,$CF,$50,$9C,$1B,$28,$47,$9F,$32,$1E,$98,$71,$D1,$70,$C9);

  ID_umlctsint_128 : TUMLCType =
    ($3E,$48,$76,$60,$F3,$6C,$FA,$40,$A2,$A4,$65,$00,$F6,$82,$26,$A1);

  ID_umlctsint_256 : TUMLCType =
    ($B0,$88,$AA,$70,$A9,$20,$A4,$49,$8F,$F7,$75,$16,$F1,$00,$BF,$9A);

  ID_umlctsint_512 : TUMLCType =
    ($B4,$C5,$51,$69,$FF,$E9,$07,$45,$A5,$C4,$A0,$3D,$73,$88,$A4,$CA);

// ---

const

// ---

  ID_umlcpsint_8 : TUMLCType =
    ($52,$24,$7F,$EA,$FD,$85,$8D,$43,$86,$25,$CE,$9A,$C9,$A9,$64,$DB);

  ID_umlcpsint_16 : TUMLCType =
    ($5E,$43,$66,$25,$F8,$FA,$49,$43,$BD,$7B,$0E,$20,$15,$BF,$99,$F6);

  ID_umlcpsint_32 : TUMLCType =
    ($84,$D4,$6F,$A9,$63,$79,$AD,$47,$B5,$EF,$5C,$86,$A6,$F7,$B8,$AB);

  ID_umlcpsint_64 : TUMLCType =
    ($E7,$AF,$9F,$32,$CE,$2E,$86,$4D,$9D,$F6,$16,$97,$04,$80,$1F,$A4);

  ID_umlcpsint_128 : TUMLCType =
    ($51,$45,$D9,$F4,$06,$63,$87,$48,$BD,$89,$3E,$D2,$C9,$93,$7B,$D5);

  ID_umlcpsint_256 : TUMLCType =
    ($2C,$B4,$35,$94,$09,$C5,$D7,$4E,$88,$B2,$A8,$9B,$64,$3E,$E5,$FF);

  ID_umlcpsint_512 : TUMLCType =
    ($2C,$B4,$35,$94,$09,$C5,$D7,$4E,$88,$B2,$A8,$9B,$64,$3E,$E5,$FF);

// ---

(* global standard types *)

type

  umlcsint_8    = (* alias of *) shortint;
  umlcsint_16   = (* alias of *) smallint; // array[0..1] of byte;
  umlcsint_32   = (* alias of *) longint;  // array[0..3] of byte;
  umlcsint_64   = (* alias of *) int64;    // packed record Value: array[0..7] of byte; end;

  umlcsint_128  = (* alias of *) packed record Value: array[0..15] of byte; end;
  umlcsint_256  = (* alias of *) packed record Value: array[0..31] of byte; end;
  umlcsint_512  = (* alias of *) packed record Value: array[0..63] of byte; end;

(* global additional types *)

type

  TUMLCSignedIntegerTypes = (* enum of *)
  (
    umlctypsint_Unknown,
    umlctypsint_8,
    umlctypsint_16,
    umlctypsint_32,
    umlctypsint_64,
    umlctypsint_128,
    umlctypsint_256,
    umlctypsint_512
  );

type

  umlctsint_8    = (* alias of *) umlcsint_8;
  umlctsint_16   = (* alias of *) umlcsint_16;
  umlctsint_32   = (* alias of *) umlcsint_32;
  umlctsint_64   = (* alias of *) umlcsint_64;
  umlctsint_128  = (* alias of *) umlcsint_128;
  umlctsint_256  = (* alias of *) umlcsint_256;
  umlctsint_512  = (* alias of *) umlcsint_512;

type

  umlcpsint_8    = ^umlcsint_8;
  umlcpsint_16   = ^umlcsint_16;
  umlcpsint_32   = ^umlcsint_32;
  umlcpsint_64   = ^umlcsint_64;
  umlcpsint_128  = ^umlcsint_128;
  umlcpsint_256  = ^umlcsint_256;
  umlcpsint_512  = ^umlcsint_512;

(* global functions *)

function SizeInBytesOf
  (const ASignedIntegerType: TUMLCSignedIntegerTypes): Cardinal;

implementation

(* global functions *)

function SizeInBytesOf
  (const ASignedIntegerType: TUMLCSignedIntegerTypes): Cardinal;
begin
  //umlctypsint_Unknown:
  Result := 0;

  case ASignedIntegerType of
    umlctypsint_8:
      Result := sizeof(umlcsint_8);
    umlctypsint_16:
      Result := sizeof(umlcsint_16);
    umlctypsint_32:
      Result := sizeof(umlcsint_32);
    umlctypsint_64:
      Result := sizeof(umlcsint_64);
    umlctypsint_128:
      Result := sizeof(umlcsint_128);
    umlctypsint_256:
      Result := sizeof(umlcsint_256);
    umlctypsint_512:
      Result := sizeof(umlcsint_512);
  end;
end;




end.

