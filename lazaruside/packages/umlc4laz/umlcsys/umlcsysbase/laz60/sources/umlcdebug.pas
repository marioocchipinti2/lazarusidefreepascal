(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdebug;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  dummy;

(* global types *)

type
  DebugWriteFunctor =
    procedure (* ^ *) (const AMsg: string) of object;

type
  DebugWriteEoLnFunctor =
    procedure (* ^ *) ( (* noparams *) ) of object;

type
  DebugWriteLnFunctor =
    procedure (* ^ *) (const AMsg: string) of object;

type
  TUMLCDebugClass = class
  public
  (* public declarations *)

    procedure DebugWrite(const AMsg: string);
    procedure DebugWriteEoLn();
    procedure DebugWriteLn(const AMsg: string);
  end;

(* global variables *)

var
  DebugObject:    TUMLCDebugClass;
  DebugWrite:     DebugWriteFunctor;
  DebugWriteEoLn: DebugWriteEoLnFunctor;
  DebugWriteLn:   DebugWriteLnFunctor;

implementation

procedure TUMLCDebugClass.DebugWrite
  (const AMsg: string);
begin
  System.Write(AMsg);
end;

procedure TUMLCDebugClass.DebugWriteEoLn();
begin
  self.DebugWrite(#13#10);
end;

procedure TUMLCDebugClass.DebugWriteLn
  (const AMsg: string);
begin
  self.DebugWrite(AMsg);
  self.DebugWriteEoLn();
end;

procedure UnitConstructor;
begin
  umlcdebug.DebugObject.Create();

  {$IFDEF FPC}
  umlcdebug.DebugWrite :=
    @umlcdebug.DebugObject.DebugWrite;
  {$ELSE}
  umlcdebug.DebugWrite :=
    (* @ *) umlcdebug.DebugObject.DebugWrite;
  {$ENDIF}

  {$IFDEF FPC}
  umlcdebug.DebugWriteEoLn :=
    @umlcdebug.DebugObject.DebugWriteEoLn;
  {$ELSE}
  umlcdebug.DebugWriteEoLn :=
    (* @ *) umlcdebug.DebugObject.DebugWriteEoLn;
  {$ENDIF}

  {$IFDEF FPC}
  umlcdebug.DebugWriteLn :=
    @umlcdebug.DebugObject.DebugWriteLn;
  {$ELSE}
  umlcdebug.DebugWriteLn :=
    (* @ *) umlcdebug.DebugObject.DebugWriteLn;;
  {$ENDIF}
end;

procedure UnitDestructor;
begin
  umlcdebug.DebugObject.Free();
end;

initialization
  UnitConstructor;
finalization
  UnitDestructor;
end.

