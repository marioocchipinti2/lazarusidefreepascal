(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcboolbyptrs;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcboolbyptrs : TUMLCModule =
   ($42,$51,$99,$E3,$0D,$CE,$26,$40,$82,$E7,$69,$C0,$04,$0A,$1B,$81);

 // ---


 (* global functions *)

 function ConstToPtr
   (const AValue: boolean): pointer;

 procedure DropPtr
   (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: boolean): pointer;
var P: pboolean;
begin
  System.GetMem(P, sizeof(boolean));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(boolean));
end;


end.

