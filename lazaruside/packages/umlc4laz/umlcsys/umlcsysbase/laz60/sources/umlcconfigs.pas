(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcconfigs;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

(* Espanol *)

  // TConfig. Es un tipo enumerado utilizado para almacenar y recuperar
  // datos de cualquier tipo simple como si fueran cadenas.

  // Se utiliza comunmente en archivos de configuracion.

(* English *)

  // TConfig. It's a enumerated type used for backup & restore any simple type
  // data as strings.

  // It's commonly used in configuration files.

interface
uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysUtils, Classes, Math,
//  Graphics, Controls, Forms, Dialogs,
  umlcmodules, umlctypes,
  //umlctimes, umlcdates, umlcBooleans, umlcTimeStamps,
dummy;

// ---

const

  MOD_umlcconfigs : TUMLCModule =
    ($51,$99,$71,$E1,$A9,$3F,$75,$49,$98,$10,$F9,$6A,$83,$E7,$44,$22);

// ---

type

{ TConfig }

  TConfig =
  ( cfgNone,     cfgCustom,   cfgPointer, cfgBoolean, cfgInteger,
    cfgFloat,    cfgCurrency, cfgString,  cfgDate,    cfgTime,
    cfgDateTime, cfgColor,    cfgFile,    cfgPath,    cfgChar,
    cfgArray );

  function SafeConfig(const Value: TConfig): TConfig;

  function ConfigToInt(const Value: TConfig): Integer;
  function IntToConfig(const Value: Integer): TConfig;

implementation

const
   IntToConfigArray: array[Ord(Low(TConfig))..Ord(High(TConfig))] of TConfig =
  ( cfgNone,     cfgCustom,   cfgPointer, cfgBoolean, cfgInteger,
    cfgFloat,    cfgCurrency, cfgString,  cfgDate,    cfgTime,
    cfgDateTime, cfgColor,    cfgFile,    cfgPath,    cfgChar,
    cfgArray
  );

function SafeConfig(const Value: TConfig): TConfig;
begin
  Result := Value;
  if Result < Low(TConfig)
    then Result := Low(TConfig);
  if Result > High(TConfig)
    then Result := High(TConfig);
  // Goal: Checks that a Config number is between 0 and 7.
  // Objetivo: Revisa que el numero de dia este entre 0 y 7.
end;

function ConfigToInt(const Value: TConfig): Integer;
begin
  Result := Ord(Value);
end;

function IntToConfig(const Value: Integer): TConfig;
begin
  Result := IntToConfigArray[Value];
end;


end.
