(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcglobal;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  SysUtils,
  umlcmodules, umlctypes,
  dummy;

// ---

const

  MOD_umlcglobal : TUMLCModule =
    ($00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$01);

// ---

implementation

//procedure UnitConstructor();
//begin
  //umlcmodules.RegisterModule
  //  (MOD_None, MOD_umlcglobal);
//&end;

//procedure UnitDestructor();
//begin
  //
//end;

//initialization
  //UnitConstructor;
//finalization
  //UnitDestructor;
end.

