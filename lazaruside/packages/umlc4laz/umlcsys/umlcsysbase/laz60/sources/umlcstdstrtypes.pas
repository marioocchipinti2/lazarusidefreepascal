(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdstrtypes;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

(**
 **************************************************************************
 ** Description:
 ** To define and implement,
 ** several encoding and memory size text (string) types.
 **************************************************************************
 **)

interface
uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlcstdchartypes,
  dummy;

// ---

const

 MOD_umlcstdstrtypes : TUMLCModule =
   ($F4,$CD,$79,$D6,$BB,$D6,$B7,$4D,$BE,$53,$6E,$F9,$9E,$56,$07,$4E);

// ---

(* global types *)

type
  Tansistring = (* alias of *) System.ansistring;
  Pansistring = (* alias of *) ^Tansistring;

type
  umlctansistring = (* alias of *) Tansistring;
  umlcpansistring = (* alias of *) Pansistring;

type

  // delphi*s "string" is a pointer similar to java*s references
  // el "string" de delphi es un apuntador similar a las referencias de java

  umlcansishortstring  = array[0..255] of ansichar;

  umlctansishortstring = umlcansishortstring;
  umlcpansishortstring = ^umlctansishortstring;
  // we'll use a pascal string (an array of char) instead
  // en vez de eso, usaremos una cadena pascal (arreglo de caracteres)

type
  umlcwidestring  = (* alias of *) widestring;

type
  umlctwidestring = (* alias of *) umlcwidestring;
  umlcpwidestring = ^umlcwidestring;

type

  // delphi*s "string" is a pointer similar to java*s references
  // el "string" de delphi es un apuntador similar a las referencias de java

  umlcwideshortstring  = array[0..255] of umlcwidechar;
  wideshortstring      = (* alias of *) umlcwideshortstring;

  umlctwideshortstring = (* alias of *) umlcwideshortstring;
  umlcpwideshortstring = ^umlctwideshortstring;

(* global constants *)

const
  ansiemptystr = '';

const
  umlcansishortstringsize = System.SizeOf(umlcansishortstring);

const
  wideemptystr = '';

const
  umlcwideshortstringsize = System.SizeOf(umlcwideshortstring);


implementation

procedure UnitConstructor();
begin
  //
end;

procedure UnitDestructor();
begin
  //
end;

initialization
  //UnitConstructor();
finalization
  //UnitDestructor();
end.

