(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlclibc;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface
uses
  SysUtils,
  dummy;

  function memcmp(const A, B: pointer; S: integer): integer;

  function CompareByte
    (const A, B; len: integer): integer;

  procedure FillByte
    (var x; count: integer; value:byte);

implementation

function memcmp(const A, B: pointer; S: integer): integer;
var I: Integer; Match: Boolean;
begin
  Result := -1;

  Match := true;
  I := 0;
  while (Match and (I < S)) do
  begin
    Match := (pbyte(A) = pbyte(b));
    Inc(I);
  end;

  if (not Match) then
  begin
    if (pbyte(A) < pbyte(b))
      then Result := -1
      else Result := +1;
  end;
end;

function CompareByte
  (const A, B; len: integer): integer;
begin
  Result := 0;
  {$IFDEF MSWINDOWS}
  {$IFDEF FPC}
  Result :=
    System.CompareByte(A, B, Len);
  {$ELSE}
  Result :=
    umlclibc.memcmp(@A, @B, Len);
  {$ENDIF}
  {$ENDIF}
  {$IFDEF LINUX}
  Result :=
    umlclibc.memcmp(@A, @B, Len);
  {$ENDIF}
end;

procedure FillByte
  (var x; count: integer; value:byte);
begin
  {$IFDEF MSWINDOWS}
  {$IFDEF FPC}
  System.FillByte(x, count, value);
  {$ELSE}
  System.FillChar(x, count, value);
  {$ENDIF}
  {$ENDIF}

  {$IFDEF LINUX}
  System.FillChar(x, count, value);
  {$ENDIF}
end;

//procedure UnitConstructor();
//begin
  //umlcmodules.RegisterModule
  //  (MOD_None, MOD_umlcglobal);
//end;

//procedure UnitDestructor();
//begin
  //
//end;

//initialization
  //UnitConstructor;
//finalization
  //UnitDestructor;
end.
