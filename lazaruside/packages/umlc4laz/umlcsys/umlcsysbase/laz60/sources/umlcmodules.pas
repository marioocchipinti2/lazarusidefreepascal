(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmodules;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  Classes, SysUtils,
  umlclibc,
  umlcuuids,
  dummy;

// ---

//const
//
// MOD_umlcmodules : TUMLCModule =
//   ($93,$3D,$85,$E8,$41,$86,$E2,$49,$A9,$D2,$96,$E2,$9E,$F6,$D6,$A6);

// ---

type
  TUMLCModule =
    type TUUID;

  // ---

const
  UnknownModule : TUMLCModule =
    ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ) ;

  MOD_UnknownModule : TUMLCModule =
    ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ) ;

  MOD_None : TUMLCModule =
    ( $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00 ) ;

// ---

type
  RegisterModuleFunctor =
    (* ^ *) function
      (const AParentModuleID: TUMLCModule;
       const AModuleID:       TUMLCModule): pointer;

// ---

function IsEmptyModule
  (const AType: TUMLCModule): Boolean;
function AreEqualModules
  (const A, B: TUMLCModule): Boolean;

procedure ClearModule
  (var AType: TUMLCModule);
procedure AssignModule
  (var A: TUMLCModule; const B: TUMLCModule);

function RegisterModule
  (const AParentModuleID: TUMLCModule;
   const AModuleID:       TUMLCModule): pointer;

implementation

// ---

var RegisterModuleSingleton: RegisterModuleFunctor;

// ---

function IsEmptyModule
  (const AType: TUMLCModule): Boolean;
begin
  Result :=
     IsEmptyUUID(AType);
end;

function AreEqualModules
  (const A, B: TUMLCModule): Boolean;
begin
  Result :=
    (umlclibc.CompareByte(A, B, SizeOf(TUMLCModule)) = 0);
end;

procedure ClearModule
  (var AType: TUMLCModule);
begin
  umlclibc.FillByte((* var *) AType, sizeof(TUMLCModule), 0);
end;

procedure AssignModule
  (var   A: TUMLCModule; const B: TUMLCModule);
begin
  System.Move((* const *) B, (* var *) A, sizeof(TUMLCModule));
end;

function RegisterModuleNothing
  (const AParentModuleID: TUMLCModule;
   const AModuleID:       TUMLCModule): pointer;
begin
  // Does nothing on purpouse.
  Result := nil;
end;

function RegisterModule
  (const AParentModuleID: TUMLCModule;
   const AModuleID:       TUMLCModule): pointer;
begin
  Result :=
    umlcmodules.RegisterModuleSingleton
      (AParentModuleID, AModuleID);
end;

procedure UnitConstructor();
begin
  umlcmodules.RegisterModuleSingleton :=
    @umlcmodules.RegisterModuleNothing;
end;

//procedure UnitDestructor();
//begin
  //
//end;

initialization
  umlcmodules.UnitConstructor();
//finalization
  //UnitDestructor;
end.

