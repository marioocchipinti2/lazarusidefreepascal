(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwin16colors;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

(* Espanol *)

  // Objetivo: Provee constantes para manejo de colores.

(* English *)

  // Goal: Provides constants for colors management.

interface
uses
{$IFDEF MSWINDOWS}
  Graphics,
{$ENDIF}
{$IFDEF LINUX}
  QGraphics,
{$ENDIF}
  SysUtils,
  umlcmodules, umlctypes,
dummy;

// ---

const

  MOD_umlcwin16colors : TUMLCModule =
   ($0C,$29,$98,$5A,$E9,$E0,$24,$42,$AC,$94,$09,$37,$1D,$25,$FB,$DE);

// ---

type
  win16color =
  (
  win16clBlack,
  win16clMaroon,
  win16clGreen,
  win16clOlive,
  win16clNavy,
  win16clPurple,
  win16clTeal,
  win16clGray,
  win16clSilver,
  win16clRed,
  win16clLime,
  win16clYellow,
  win16clBlue,
  win16clFuchsia,
  win16clAqua,
  win16clWhite
  );
  twin16color = win16color;
  pwin16color = ^twin16color;

  win16colorset = set of win16color;

const
  win16clLow  = win16clBlack;
  win16clHigh = win16clWhite;

  function SafeWin16Color(const Value: win16color): win16color;

  function ColorToWin16Color(const Value: TColor): win16color;
  function Win16colorToColor(const Value: win16color): TColor;

implementation

const
  win16colorToColorArray: array[win16color] of TColor =
   ( clBlack,  clMaroon,  clGreen, clOlive,
     clNavy,   clPurple,  clTeal,  clGray,
     clSilver, clRed,     clLime,  clYellow,
     clBlue,   clFuchsia, clAqua,  clWhite );

function Safewin16color(const Value: win16color): win16color;
begin
  Result := Value;
  if (Result < Low(win16color))
    then Result := Low(win16color);
  if (Result > High(win16color))
    then Result := High(win16color);
  // Goal: Checks that a color number is valid.
  // Objetivo: Revisa que el numero de color es valido.
end;

function ColorToWin16Color(const Value: TColor): win16color;
var i: win16color; Found: Boolean;
begin
  i := Low(win16color); Found := FALSE;
  while ((i <= High(win16color)) and not Found) do
  begin
    Found := (Value = Win16ColorToColorArray[i]);
    Inc(I);
  end;

  if (Found)
    then Result := Pred(i)
    else Result := Low(win16color);
  // Goal: To cast a "TColor" value to a "win16color" value.
  // Objetivo: Convertir un valor "TColor" a un valor "win16color".
end;

function Win16colorToColor(const Value: win16color): TColor;
begin
  Result := Win16ColorToColorArray[Value];
  // Goal: To cast a "win16color" value to a "TColor" value.
  // Objetivo: Convertir un valor "win16color" a un valor "TColor".
end;


end.

