(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstddatetimetypes;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

(**
 **************************************************************************
 ** Description:
 ** To implement Date and Time related types.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcstddatetimetypes : TUMLCModule =
   ($89,$3B,$E4,$D2,$B7,$4D,$96,$47,$9A,$03,$08,$FE,$B2,$89,$2B,$2A);

// ---

const
  ID_UMLCDate : TUMLCType =
    ($72,$2E,$71,$5B,$09,$FC,$E4,$49,$B1,$A8,$FE,$07,$D8,$05,$AD,$5F);

  ID_UMLCTDate : TUMLCType =
    ($72,$2E,$71,$5B,$09,$FC,$E4,$49,$B1,$A8,$FE,$07,$D8,$05,$AD,$5F);
  ID_UMLPCDate : TUMLCType =
    ($71,$7D,$9C,$4E,$59,$5A,$E7,$44,$B1,$27,$26,$C9,$40,$D0,$F8,$B4);

// ---

const
  ID_umlctime : TUMLCType =
    ($B7,$9A,$2B,$6C,$65,$44,$2D,$47,$91,$93,$ED,$E2,$38,$B3,$62,$D5);

  ID_UMLCTTime : TUMLCType =
    ($B7,$9A,$2B,$6C,$65,$44,$2D,$47,$91,$93,$ED,$E2,$38,$B3,$62,$D5);
  ID_UMLCPTime : TUMLCType =
    ($16,$25,$83,$B8,$23,$24,$75,$4F,$BF,$32,$6B,$D9,$25,$B3,$16,$E5);

// ---

const
  ID_UMLCDateTime : TUMLCType =
    ($2E,$EF,$D3,$1D,$90,$53,$51,$4F,$86,$78,$EF,$79,$47,$14,$D4,$3E);

  ID_UMLCTDateTime : TUMLCType =
    ($2E,$EF,$D3,$1D,$90,$53,$51,$4F,$86,$78,$EF,$79,$47,$14,$D4,$3E);
  ID_UMLCPDateTime : TUMLCType =
    ($9F,$74,$15,$19,$EE,$2C,$EB,$4D,$85,$7A,$87,$0F,$84,$FE,$21,$11);

// ---

  ID_TTimeStamp : TUMLCType =
    ($3C,$19,$B3,$F3,$9C,$21,$E1,$49,$B8,$44,$D5,$DE,$6B,$FA,$37,$B9);
  ID_PTimeStamp : TUMLCType =
    ($35,$E6,$07,$5A,$E2,$DF,$D1,$4E,$B5,$FA,$DC,$D2,$88,$42,$A1,$F0);

  ID_UMLCTimeStamp : TUMLCType =
    ($3C,$19,$B3,$F3,$9C,$21,$E1,$49,$B8,$44,$D5,$DE,$6B,$FA,$37,$B9);

  ID_UMLCTTimeStamp : TUMLCType =
    ($3C,$19,$B3,$F3,$9C,$21,$E1,$49,$B8,$44,$D5,$DE,$6B,$FA,$37,$B9);
  ID_UMLCPTimeStamp : TUMLCType =
    ($35,$E6,$07,$5A,$E2,$DF,$D1,$4E,$B5,$FA,$DC,$D2,$88,$42,$A1,$F0);

// ---

(* global standard types *)

type
  umlcstddate      = (* alias of *) System.TDate;
  umlcstdtime      = (* alias of *) System.TTime;
  umlcstddatetime  = (* alias of *) System.TDateTime;
  umlcstdtimestamp = (* alias of *) SysUtils.TTimeStamp;

type
  umlctstddate      = (* alias of *) umlcstddate;
  umlcpstddate      = ^umlcstddate;

  umlctstdtime      = (* alias of *) umlcstdtime;
  umlcpstdtime      = ^umlcstdtime;

  umlctstddatetime  = (* alias of *) umlcstddatetime;
  umlcpstddatetime  = ^umlcstddatetime;

  umlctstdtimestamp = (* alias of *) umlcstdtimestamp;
  umlcpstdtimestamp = ^umlcstdtimestamp;

// ---

type
  umlcdate = TDate;

  //TDate = (* alias of *) umlcdate;
  //PDate = ^umlcdate;

  UMLCTDate = (* alias of *) umlcdate;
  UMLCPDate = ^umlcdate;

// ---

type
  umlctime = TTime;

  TTime = (* alias of *) umlctime;
  PTime = ^TTime;

  umlcttime = (* alias of *) umlctime;
  umlcptime = ^umlctime;

// ---

type
  umlcdatetime = System.TDateTime;

  umlctdatetime = (* alias of *) umlcdatetime;
  umlcpdatetime = ^umlctime;

// ---

type
  //TTimeStamp = record
  //  Year: Word;
  //  Month: Word;
  //  Day: Word;
  //  Hour: Word;
  //  Minute: Word;
  //  Second: Word;
  //  Milliseconds: Word;
  //end;
  PTimeStamp = ^TTimeStamp;

  umlctimestamp = TTimeStamp;

  umlcttimestamp = (* alias of *) TTimeStamp;
  umlcptimestamp = (* alias of *) PTimeStamp;

(* global constants *)

const
  NoDate = 0;

const
  NoTime = 0;

const
  NoDateTime = 0;

const
  NoTimeStamp: umlctimestamp =
    (Time: 0; Date: 0);
  //NoTimeStamp: TTimeStamp =
  //  (Year: 0; Month: 0; Day: 0; Hour: 0; Minute: 0; Second: 0; Milliseconds: 0);

implementation




end.

