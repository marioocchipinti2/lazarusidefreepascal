(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlclists;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface
uses
  SysUtils, 
  umlcreslists,
  dummy;

const

(* Maximum TList size *)

  MaxListSize = Maxint div 16;

(* List error *)

  IndexNotFound = -1;

  IgnoreIndex   = -1;

type

(* TUMLCListError *)

  TUMLCListError = class(Exception);

(* TUMLCPointerArray *)

  PUMLCPointerArray = ^TUMLCPointerArray;
  TUMLCPointerArray = array[0..MaxListSize - 1] of pointer;

  TListSortCompare = function (Item1, Item2: pointer): Integer;
  TListNotification = (lnAdded, lnExtracted, lnDeleted);

(* TCustomUMLCList *)

  TCustomUMLCList = class(TObject)
  private
    { Private declarations }

    FCapacity: Integer;
  protected
    { Protected declarations }

    FCount:  Integer;
    FList:   PUMLCPointerArray;

    function getInternalItems(Index: Integer): pointer;
    function getCapacity(): Integer;
    function getCount(): Integer;
    function getList(): PUMLCPointerArray;

    procedure setInternalItems(Index: Integer; Item: pointer);
    procedure setCapacity(const NewCapacity: Integer);
    procedure setCount(const Value: Integer);
    procedure setList(const Value: PUMLCPointerArray);

    procedure Grow; virtual;
//    procedure Notify(Ptr: pointer; Action: TListNotification); virtual;

    function InternalIndexOf(Item: pointer): Integer;
    function InternalFirst(): pointer;
    function InternalLast(): pointer;

    function InternalInsert(const AItem: pointer): Integer;
    procedure InternalInsertAt(Index: Integer; Item: pointer);

    procedure InternalEmpty();
    procedure InternalDeleteAt(const AIndex: Integer);

    function InternalRemove(const AItem: pointer): Integer;
    function InternalExtract(const AIndex: Integer): pointer;
  public
    { Public declarations }

    constructor Create(); virtual;
    destructor Destroy(); override;

    class procedure Error(const Msg: string; Data: Integer); overload; virtual;
    class procedure Error(Msg: PResStringRec; Data: Integer); overload;

    procedure Exchange(Index1, Index2: Integer);
    procedure Move(CurIndex, NewIndex: Integer);

    function IsEmpty(): Boolean;
    function Expand(): TCustomUMLCList;

    procedure Pack();
    procedure Sort(Compare: TListSortCompare);

    procedure Copy(const Source: TCustomUMLCList);
    procedure Intersect(const Source: TCustomUMLCList);
    procedure Union(const Source: TCustomUMLCList);

    { Unpublished declarations }

    property Capacity: Integer
      read getCapacity write setCapacity;
    property Count: Integer
      read getCount write setCount;
    property InternalItems[Index: Integer]: pointer
      read getInternalItems write setInternalItems; default;
    property List: PUMLCPointerArray
      read getList write setList;
  end;

{ TUMLCPointerListFirstThatFunc }

  TUMLCPointerListFirstThatFunc =
   function (const AItem: pointer; const AParam: pointer): Boolean of object;

{ TUMLCPointerListForEachProc }

  TUMLCPointerListForEachProc =
   procedure (const AItem: pointer; const AParam: pointer) of object;

(* TUMLCPointerList *)

  TUMLCPointerList = class(TCustomUMLCList)
  private
    { Private declarations }
  protected
    { Protected declarations }

    function getItems(AIndex: Integer): pointer;

    procedure setItems(AIndex: Integer; AItem: pointer);
  public
    { Public declarations }

    function IndexOf(const AItem: pointer): Integer;
    function First(): pointer;
    function Last(): pointer;

    function Insert(const AItem: pointer): Integer;
    procedure InsertAt(const AIndex: Integer; const AItem: pointer);

    function Extract(const AIndex: Integer): pointer;
    procedure DeleteAt(const AIndex: Integer);
    function Remove(const AItem: pointer): Integer;
    procedure Empty();

    function FirstThat
      (const Func: TUMLCPointerListFirstThatFunc; const AParam: pointer): pointer;

    procedure ForEach
      (const Proc: TUMLCPointerListForEachProc; const AParam: pointer);
    procedure ForBack
      (const Proc: TUMLCPointerListForEachProc; const AParam: pointer);

    property Items[Index: Integer]: pointer
      read getItems write setItems; default;
  end;

{ TUMLCOwnerList }

  TUMLCOwnerList = class(TCustomUMLCList)
  private
    { Private declarations }

    FOwnsObjects: Boolean;
  protected
    { Protected declarations }

//    function InternalCreateItem: pointer; virtual; abstract;
//    procedure InternalDestroyItem(const AItem: pointer); virtual; abstract;
  public
    { Public declarations }

    procedure DeleteAt(const AIndex: Integer);
    procedure Empty();

    property OwnsObjects: Boolean
      read FOwnsObjects write FOwnsObjects;
  end;

{ TUMLCObjectListInmediate }

  TUMLCObjectListInmediate =
    procedure (const AItem: TObject;
     const AParam: pointer) of object;

(* TUMLCObjectList *)

  TUMLCObjectList = class(TUMLCOwnerList)
  private
    { Private declarations }
  protected
    { Protected declarations }

    function getItems(AIndex: Integer): TObject;

    procedure setItems(AIndex: Integer; AItem: TObject);
  public
    { Public declarations }

    function IndexOf(const AItem: TObject): Integer;
    function First(): TObject;
    function Last(): TObject;

    function Insert(const AItem: TObject): Integer;
    procedure InsertAt(const AIndex: Integer; const AItem: TObject);

    function Extract(const AIndex: Integer): TObject;
    procedure DeleteAt(const AIndex: Integer);
    function Remove(const AItem: TObject): Integer;
    procedure Empty();

    procedure ForEach
      (const Proc: TUMLCObjectListInmediate; const AParam: pointer);
    procedure ForBack
      (const Proc: TUMLCObjectListInmediate; const AParam: pointer);

    property Items[AIndex: Integer]: TObject
      read getItems write setItems; default;
  end;

implementation

(* TCustomUMLCList *)

function TCustomUMLCList.getInternalItems(Index: Integer): pointer;
begin
  if ((Index < 0) or (Index >= FCount)) then
  begin
    Error(@err_ListIndexError, Index);
  end;

  Result := FList^[Index];
end;

function TCustomUMLCList.getCapacity(): Integer;
begin
  Result := FCapacity;
end;

function TCustomUMLCList.getCount(): Integer;
begin
  Result := FCount;
end;

function TCustomUMLCList.getList(): PUMLCPointerArray;
begin
  Result := FList;
end;

procedure TCustomUMLCList.setInternalItems(Index: Integer; Item: pointer);
//var Temp: pointer;
begin
  if ((Index < 0) or (Index >= FCount)) then
  begin
    Error(@err_ListIndexError, Index);
  end;

  if (Item <> FList^[Index]) then
  begin
//    Temp := FList^[Index];
    FList^[Index] := Item;
//    if (Temp <> nil)
//        then Notify(Temp, lnDeleted);
//    if (Item <> nil)
//        then Notify(Item, lnAdded);
  end;
end;

procedure TCustomUMLCList.setCapacity(const NewCapacity: Integer);
begin
  if ((NewCapacity < FCount) or (NewCapacity > MaxListSize)) then
  begin
    Error(@err_ListIndexError, NewCapacity);
  end;

  if (NewCapacity <> FCapacity) then
  begin
    ReallocMem(FList, NewCapacity * SizeOf(pointer));
    FCapacity := NewCapacity;
  end;
end;

procedure TCustomUMLCList.setCount(const Value: Integer);
var NewCount, I: Integer;
begin
  NewCount := Value;
  if ((NewCount < 0) or (NewCount > MaxListSize)) then
  begin
    Error(@err_ListIndexError, NewCount);
  end;

  if (NewCount > FCapacity) then
  begin
    SetCapacity(NewCount);
  end;

  if (NewCount > FCount) then
  begin
    FillChar(FList^[FCount], (NewCount - FCount) * SizeOf(pointer), 0)
  end else
    // to while
    for I := FCount - 1 downto NewCount do
    begin
      InternalDeleteAt(I);
    end;

  FCount := NewCount;
end;

procedure TCustomUMLCList.setList(const Value: PUMLCPointerArray);
begin
  FList := Value;
end;

constructor TCustomUMLCList.Create();
begin
  inherited Create();
end;

destructor TCustomUMLCList.Destroy();
begin
  InternalEmpty();
  inherited Destroy();
end;

class procedure TCustomUMLCList.Error(const Msg: string; Data: Integer);

  {$ifdef Delphi}
  function ReturnAddr(): pointer;
  asm
          MOV     EAX,[EBP+4]
  end;
  {$endif}

begin
  {$ifdef Delphi}
  raise TUMLCListError.CreateFmt(Msg, [Data]) at ReturnAddr();
  {$else}
  raise TUMLCListError.CreateFmt(Msg, [Data]);
  {$endif}
end;

class procedure TCustomUMLCList.Error(Msg: PResStringRec; Data: Integer);
begin
  TCustomUMLCList.Error(LoadResString(Msg), Data);
end;

procedure TCustomUMLCList.Exchange(Index1, Index2: Integer);
var Item: pointer;
begin
  if ((Index1 < 0) or (Index1 >= FCount)) then
  begin
    Error(@err_ListIndexError, Index1);
  end;

  if ((Index2 < 0) or (Index2 >= FCount)) then
  begin
    Error(@err_ListIndexError, Index2);
  end;

  Item := FList^[Index1];
  FList^[Index1] := FList^[Index2];
  FList^[Index2] := Item;
end;

procedure TCustomUMLCList.Move(CurIndex, NewIndex: Integer);
var Item: pointer;
begin
  if (CurIndex <> NewIndex) then
  begin
    if ((NewIndex < 0) or (NewIndex >= FCount)) then
    begin
      Error(@err_ListIndexError, NewIndex);
    end;

    Item := getInternalItems(CurIndex);
    FList^[CurIndex] := nil;
    InternalDeleteAt(CurIndex);
    InternalInsertAt(NewIndex, nil);
    FList^[NewIndex] := Item;
  end;
end;

function TCustomUMLCList.IsEmpty(): Boolean;
begin
  Result := (Count < 1);
end;

function TCustomUMLCList.Expand(): TCustomUMLCList;
begin
  if (FCount = FCapacity) then
  begin
    Grow();
  end;

  Result := Self;
end;

procedure TCustomUMLCList.Grow;
var Delta: Integer;
begin
  if (FCapacity > 64) then
  begin
    Delta := FCapacity div 4
  end else
  begin
    if (FCapacity > 8)
      then Delta := 16
      else Delta := 4;
  end;

  SetCapacity(FCapacity + Delta);
end;

procedure TCustomUMLCList.Pack();
var I: Integer;
begin
  // while
  for I := FCount - 1 downto 0 do
  begin
    if (InternalItems[I] = nil) then
    begin
      InternalDeleteAt(I);
    end;
  end;
end;

procedure QuickSort
 (SortList: PUMLCPointerArray; L, R: Integer; Compare: TListSortCompare);
var I, J: Integer; P, T: pointer;
begin
  repeat
    I := L;
    J := R;
    P := SortList^[(L + R) shr 1];
    repeat
      while Compare(SortList^[I], P) < 0 do
      begin
        Inc(I);
      end;

      while Compare(SortList^[J], P) > 0 do
      begin
        Dec(J);
      end;

      if (I <= J) then
      begin
        T := SortList^[I];
        SortList^[I] := SortList^[J];
        SortList^[J] := T;
        Inc(I);
        Dec(J);
      end;
    until (I > J);

    if (L < J) then
    begin
      QuickSort(SortList, L, J, Compare);
    end;

    L := I;
  until (I >= R);
end;

procedure TCustomUMLCList.Sort(Compare: TListSortCompare);
begin
  if ((FList <> nil) and (Count > 0)) then
  begin
    QuickSort(FList, 0, Count - 1, Compare);
  end;
end;

(*
procedure TCustomUMLCList.Notify(Ptr: pointer; Action: TListNotification);
begin
end;
*)

function TCustomUMLCList.InternalIndexOf(Item: pointer): Integer;
begin
  Result := 0;
  while( (Result < FCount) and (FList^[Result] <> Item)) do
  begin
    Inc(Result);
  end;

  if (Result = FCount) then
  begin
    Result := IndexNotFound;
  end;
end;

function TCustomUMLCList.InternalFirst(): pointer;
begin
  Result := getInternalItems(0);
end;

function TCustomUMLCList.InternalLast(): pointer;
begin
  Result := getInternalItems(FCount - 1);
end;

function TCustomUMLCList.InternalInsert(const AItem: pointer): Integer;
begin
  Result := FCount;
  if (Result = FCapacity) then
  begin
    Grow();
  end;

  FList^[Result] := AItem;
  Inc(FCount);
//if (Item <> nil)
//  then Notify(Item, lnAdded);
end;

procedure TCustomUMLCList.InternalInsertAt(Index: Integer; Item: pointer);
begin
  if ((Index < 0) or (Index > FCount)) then
  begin
    Error(@err_ListIndexError, Index);
  end;

  if (FCount = FCapacity) then
  begin
    Grow();
  end;

  if (Index < FCount) then
  begin
    System.Move(FList^[Index], FList^[Index + 1],
      (FCount - Index) * SizeOf(pointer));
  end;

  FList^[Index] := Item;
  Inc(FCount);
//if (Item <> nil) then
//  Notify(Item, lnAdded);
end;

procedure TCustomUMLCList.InternalEmpty();
begin
  SetCount(0);
  SetCapacity(0);
end;

procedure TCustomUMLCList.InternalDeleteAt(const AIndex: Integer);
//var Temp: pointer;
begin
  if ((AIndex < 0) or (AIndex >= FCount)) then
  begin
    Error(@err_ListIndexError, AIndex);
  end;

//  Temp := InternalItems[AIndex];
  Dec(FCount);

  if (AIndex < FCount) then
  begin
    System.Move(FList^[AIndex + 1], FList^[AIndex],
      (FCount - AIndex) * SizeOf(pointer));
  end;
//if (Temp <> nil)
//  then Notify(Temp, lnDeleted);
end;

function TCustomUMLCList.InternalRemove(const AItem: pointer): Integer;
begin
  Result := InternalIndexOf(AItem);
  if (Result >= 0) then
  begin
    InternalDeleteAt(Result);
  end;
end;

function TCustomUMLCList.InternalExtract(const AIndex: Integer): pointer;
begin
  Result := nil;
  if ((AIndex >= 0) and (AIndex <= Count)) then
  begin
    Result := getInternalItems(AIndex);
    FList^[AIndex] := nil;
    InternalDeleteAt(AIndex);
//  Notify(Result, lnExtracted);
  end;
end;

procedure TCustomUMLCList.Copy(const Source: TCustomUMLCList);
var I: Integer;
begin
  InternalEmpty();
  Capacity := Source.Capacity;
  for I := 0 to Pred(Source.Count) do
  begin
    InternalInsert(Source[I]);
  end;
end;

procedure TCustomUMLCList.Intersect(const Source: TCustomUMLCList);
var I, EachIndex: Integer;
begin
  // while
  for I := Count - 1 downto 0 do
  begin
    EachIndex := Source.InternalIndexOf(InternalItems[I]);
    if (EachIndex = IndexNotFound) then
    begin
      InternalDeleteAt(I);
    end;
  end;
end;

procedure TCustomUMLCList.Union(const Source: TCustomUMLCList);
var I, EachIndex: Integer;
begin
  for I := 0 to Source.Count - 1 do
  begin
    EachIndex := Source.InternalIndexOf(InternalItems[I]);
    if (EachIndex = IndexNotFound) then
    begin
      InternalInsert(Source[I]);
    end;
  end;
end;

(* TUMLCPointerList *)

function TUMLCPointerList.getItems(AIndex: Integer): pointer;
begin
  Result := getInternalItems(AIndex);
end;

procedure TUMLCPointerList.setItems(AIndex: Integer; AItem: pointer);
begin
  setInternalItems(AIndex, AItem);
end;

function TUMLCPointerList.IndexOf(const AItem: pointer): Integer;
begin
  Result := InternalIndexOf(AItem);
end;

function TUMLCPointerList.First(): pointer;
begin
  Result := InternalFirst();
end;

function TUMLCPointerList.Last(): pointer;
begin
  Result := InternalLast();
end;

function TUMLCPointerList.Insert(const AItem: pointer): Integer;
begin
  Result := InternalInsert(AItem);
end;

procedure TUMLCPointerList.InsertAt
  (const AIndex: Integer; const AItem: pointer);
begin
  InternalInsertAt(AIndex, AItem);
end;

function TUMLCPointerList.Extract(const AIndex: Integer): pointer;
begin
  Result := InternalExtract(AIndex);
end;

procedure TUMLCPointerList.DeleteAt(const AIndex: Integer);
begin
  InternalDeleteAt(AIndex);
end;

function TUMLCPointerList.Remove(const AItem: pointer): Integer;
begin
  Result := InternalRemove(AItem);
end;

procedure TUMLCPointerList.Empty();
begin
  InternalEmpty();
end;

function TUMLCPointerList.FirstThat
  (const Func: TUMLCPointerListFirstThatFunc;
   const AParam: pointer): pointer;
var Found: Boolean; Index, LastIndex: Integer; Item: pointer;
begin
  Result := nil;

  Found := false;
  Index := 0;
  LastIndex  := Count;
  while ((not Found) and (Index < LastIndex)) do
  begin
    Item := getInternalItems(Index);
    Found := Func(Item, AParam);
    Inc(Index);
  end;

  if (Found) then
  begin
    Result := Item;
  end;
end;

procedure TUMLCPointerList.ForEach
  (const Proc: TUMLCPointerListForEachProc; const AParam: pointer);
var Index: Integer; Item: pointer;
begin
  for Index := 0 to Pred(Count) do
  begin
    Item := getInternalItems(Index);
    Proc(Item, AParam);
  end;
end;

procedure TUMLCPointerList.ForBack
  (const Proc: TUMLCPointerListForEachProc; const AParam: pointer);
var Index: Integer; Item: pointer;
begin
  for Index := Pred(Count) downto 0 do
  begin
    Item := getInternalItems(Index);
    Proc(Item, AParam);
  end;
end;

{ TUMLCOwnerList }

procedure TUMLCOwnerList.DeleteAt(const AIndex: Integer);
//var Item: TObject;
begin
//  Item := getInternalItems(Index);
//  InternalDestroyItem(Item);
  InternalDeleteAt(AIndex);
  { Goal: Destroys of the list by its index .}
  { Objetivo: Destruye un elemento de la lista por su indice .}
end;

procedure TUMLCOwnerList.Empty();
var AIndex: Integer;
begin
  // to while
  for AIndex := Pred(Count) downto 0 do
  begin
    InternalDeleteAt(AIndex);
  end;

  InternalEmpty();
  { Goal: Destroys all the items of the list.}
  { Objetivo: Destruye todos los elementos de la lista .}
end;

(* TUMLCObjectList *)

function TUMLCObjectList.getItems(AIndex: Integer): TObject;
begin
  Result := TObject(getInternalItems(AIndex));
end;

procedure TUMLCObjectList.setItems(AIndex: Integer; AItem: TObject);
begin
  setInternalItems(AIndex, AItem);
end;

function TUMLCObjectList.IndexOf(const AItem: TObject): Integer;
begin
  Result := InternalIndexOf(AItem);
end;

function TUMLCObjectList.First(): TObject;
begin
  Result := TObject(InternalFirst());
end;

function TUMLCObjectList.Last(): TObject;
begin
  Result := TObject(InternalLast());
end;

function TUMLCObjectList.Insert(const AItem: TObject): Integer;
begin
  Result := Integer(InternalInsert(AItem));
end;

procedure TUMLCObjectList.InsertAt(const AIndex: Integer; const AItem: TObject);
begin
  InternalInsertAt(AIndex, AItem);
end;

function TUMLCObjectList.Extract(const AIndex: Integer): TObject;
begin
  Result := TObject(InternalExtract(AIndex));
end;

procedure TUMLCObjectList.DeleteAt(const AIndex: Integer);
begin
  InternalDeleteAt(AIndex);
end;

function TUMLCObjectList.Remove(const AItem: TObject): Integer;
begin
  Result := InternalRemove(AItem);
end;

procedure TUMLCObjectList.Empty();
begin
  InternalEmpty();
end;

procedure TUMLCObjectList.ForEach
  (const Proc: TUMLCObjectListInmediate; const AParam: pointer);
var AIndex: Integer; AItem: TObject;
begin
  for AIndex := 0 to Pred(Count) do
  begin
    AItem := TObject(getInternalItems(AIndex));
    Proc(AItem, AParam);
  end;
end;

procedure TUMLCObjectList.ForBack
  (const Proc: TUMLCObjectListInmediate; const AParam: pointer);
var AIndex: Integer; AItem: TObject;
begin
  for AIndex := Pred(Count) downto 0 do
  begin
    AItem := TObject(getInternalItems(AIndex));
    Proc(AItem, AParam);
  end;
end;

end.
