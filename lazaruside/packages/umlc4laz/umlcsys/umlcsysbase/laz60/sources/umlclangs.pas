(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlclangs;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface
uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  dummy;

// ---

const

 MOD_umlclangs : TUMLCModule =
   ($51,$6D,$F5,$F6,$42,$5A,$F7,$4A,$80,$53,$6D,$0B,$74,$70,$F5,$49);

// ---

 // ISO 639 language codes

 // Note:
 // 1. "SpanishEurope"  added.
 // 2. "PortugueseEurope" added.
 // 3. "SpanishAmerica" added.
 // 4. "PortugueseBrasil" added.
 // 5. "JapaneseAynu" added.
 // 6. "None" added.

type

  TUMLCLanguage =
  (
//    lgNone,
    lgNeutral,

    lgAfar,
    lgAbkhazian,
    lgAvestan,
    lgAfrikaans,
    lgAkan,
    lgAmharic,
    lgAragonese,
    lgArabic,
    lgAssamese,
    lgAvaric,
    lgAymara,
    lgAzerbaijani,
    lgBashkir,
    lgBelarusian,
    lgBulgarian,
    lgBihari,
    lgBislama,
    lgBambara,
    lgBengali,
    lgTibetan,
    lgBreton,
    lgBosnian,
    lgCatalan,
    lgChechen,
    lgChamorro,
    lgCorsican,
    lgCree,
    lgCzech,
    lgChuvash,
    lgWelsh,
    lgDanish,
    lgGerman,
    lgDivehi,
    lgDzongkha,
    lgEwe,
    lgGreek,
    lgEnglish,
    lgEnglishAmerican,
    lgEnglishBritish,
    lgEnglishAustralian,
    lgEsperanto,
    lgSpanish,
    lgEstonian,
    lgBasque,
    lgPersian,
    lgFulah,
    lgFinnish,
    lgFijian,
    lgFaroese,
    lgFrench,
    lgWalloonBelgique,
    lgFrisian,
    lgIrish,
    lgGaelic,
    lgGallegan,
    lgGuarani,
    lgGujarati,
    lgManx,
    lgHausa,
    lgHebrew,
    lgHindi,
    lgHiriMotu,
    lgCroatian,
    lgHaitian,
    lgHungarian,
    lgArmenian,
    lgHerero,
    lgInterlingua,
    lgIndonesian,
    lgInterlingue,
    lgIgbo,
    lgSichuanYi,
    lgInupiaq,
    lgIdo,
    lgIcelandic,
    lgItalian,
    lgInumlcitut,
    lgJapanese,
    lgJavanese,
    lgGeorgian,
    lgKongo,
    lgKikuyu,
    lgKuanyama,
    lgKazakh,
    lgGreenlandic,
    lgKhmer,
    lgKannada,
    lgKorean,
    lgKanuri,
    lgKashmiri,
    lgKurdish,
    lgCornish,
    lgKomi,
    lgKirghiz,
    lgLatin,
    lgLuxembourgish,
    lgGanda,
    lgLimburgan,
    lgLingala,
    lgLao,
    lgLithuanian,
    lgLubaKatanga,
    lgLatvian,
    lgMalagasy,
    lgMarshallese,
    lgMaori,
    lgMacedonian,
    lgMalayalam,
    lgMongolian,
    lgMoldavian,
    lgMarathi,
    lgMalay,
    lgMaltese,
    lgBurmese,
    lgNauru,
    lgNorwegianBokmaal,
    lgNdebeleNorth,
    lgNepali,
    lgNdonga,
    lgDutch,
    lgFlemish,
    lgNorwegianNynorsk,
    lgNorwegian,
    lgNdebeleSouth,
    lgNavajo,
    lgChichewa,
    lgOccitan,
    lgOjibwa,
    lgOromo,
    lgOriya,
    lgOssetian,
    lgPanjabi,
    lgPali,
    lgPolish,
    lgPushto,
    lgPortuguese,
    lgQuechua,
    lgRaetoRomance,
    lgRundi,
    lgRomanian,
    lgRussian,
    lgKinyarwanda,
    lgSanskrit,
    lgSardinian,
    lgSindhi,
    lgNorthernSami,
    lgSango,
    lgSinhalese,
    lgSlovak,
    lgSlovenian,
    lgSamoan,
    lgShona,
    lgSomali,
    lgAlbanian,
    lgSerbian,
    lgSwati,
    lgSothoSouthern,
    lgSundanese,
    lgSwedish,
    lgSwahili,
    lgTamil,
    lgTelugu,
    lgTajik,
    lgThai,
    lgTigrinya,
    lgTurkmen,
    lgTagalog,
    lgTswana,
    lgTonga,
    lgTurkish,
    lgTsonga,
    lgTatar,
    lgTwi,
    lgTahitian,
    lgUighur,
    lgUkrainian,
    lgUrdu,
    lgUzbek,
    lgVenda,
    lgVietnamese,
    lgVolapuk,
    lgWalloon,
    lgWolof,
    lgXhosa,
    lgYiddish,
    lgYoruba,
    lgZhuang,
    lgChinese,
    lgZulu,

    lgSpanishEurope,
    lgSpanishAmerica,
    lgPortugueseEurope,
    lgPortugueseBrasil,
    lgJapaneseAynu
  );

  function SafeLang
    (const AValue: TUMLCLanguage): TUMLCLanguage;

implementation


function SafeLang
  (const AValue: TUMLCLanguage): TUMLCLanguage;
begin
  Result := AValue;
  if (Result < Low(TUMLCLanguage))
    then Result := Low(TUMLCLanguage);
  if (Result > High(TUMLCLanguage))
    then Result := High(TUMLCLanguage);
  // Goal: Checks that a language is valid.
  // Objetivo: Revisa que el lenguaje sea valido.
end;

end.
