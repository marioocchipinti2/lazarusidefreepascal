{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcsysbase60;

{$warn 5023 off : no warning about unused units}
interface

uses
  dummy, umlcansibreaks, umlcboolbyptrs, umlcbooleans, umlccomparisons, 
  umlcconfigs, umlcconstparams, umlcdays, umlcdebug, umlcdoscolors, 
  umlcenumstrs, umlcglobal, umlckeyconsts, umlclangs, umlclibc, umlclists, 
  umlcmaskarrays, umlcmemory, umlcmodules, umlcmonths, umlcnibblebuffers, 
  umlcnibblebyptrs, umlcnibbles, umlcobjlists, umlcreclists, umlcreslists, 
  umlcresobjlists, umlcrgbs, umlcsinttypes, umlcsizedparams, umlcstdbooltypes, 
  umlcstdcharconsts, umlcstdchartypes, umlcstddatetimetypes, 
  umlcstdfloattypes, umlcstdnullstrtypes, umlcstdstrtypes, umlcstdtypes, 
  umlcsys_rtti, umlctypes, umlcuinttypes, umlcuuidbyptrs, umlcuuids, 
  umlcwin16colors, umlcxnums, umlckeyvaluetypes;

implementation

end.
