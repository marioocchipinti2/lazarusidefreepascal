(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file UMLC.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdbtables;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids, umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlcactivatedcontrols,
  umlcmsgtypes, umlcmsgctrls,
  DB,
  umlcdbs, umlcdbdatasets,
  dummy;

  // toxdo: "*custom*"
  
const

  // ---

  ID_TUMLCCustomDBTable : TUMLCType =
    ($8E,$E9,$A1,$E3,$EC,$73,$FA,$44,$8C,$7B,$19,$67,$7D,$6C,$ED,$F8);

  // ---

type

  (* TUMLCCustomDBTable *)

  TUMLCCustomDBTable = class(TUMLCDBDataset)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    //FClients: TUMLCMsgClientList;
  protected
    (* Protected declarations *)

    //function getClients(): TUMLCMsgClientList;
    //
    //procedure setClients(const AValue: TUMLCMsgClientList);
  protected
    (* Protected declarations *)

    //function CreateClients(): TUMLCMsgClientList; virtual;
  public
    (* Public declarations *)

    //procedure InsertClient(const AClient: IUMLCMessageClient);
    //procedure RemoveClient(const AClient: IUMLCMessageClient);
    //
    //function AsComponent(): TComponent;
    //
    //procedure SendMessage
    //  (const AMsgRec: TUMLCMessageParamsRecord);
    //procedure SendMessageSingle
    //  (const AClient: IUMLCMessageClient; const AMsgRec: TUMLCMessageParamsRecord);
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    //function getDatabase(): TUMLCDatabase;
    //
    //procedure setDatabase(const AValue: TUMLCDatabase);
  public
    (* Public declarations *)

    //(* Read-Only properties *)
    //
    //function ClientsCount(): Integer;
    //function HasClients(): Boolean;
    //
    //(* Never Published declarations *)
    //
    //property Clients: TUMLCMsgClientList
    //  read getClients write setClients;

    (* Unpublished declarations *)

    //property Database: TUMLCDatabase
    //  read getDatabase write setDatabase;
  end;


implementation

(* TUMLCCustomDBTable *)

constructor TUMLCCustomDBTable.Create
  (AOwner: TComponent);
begin
  // toxdo: ...

  inherited Create(AOwner);
end;

destructor TUMLCCustomDBTable.Destroy();
begin
  // toxdo: ...

  inherited Destroy();
end;



end.

