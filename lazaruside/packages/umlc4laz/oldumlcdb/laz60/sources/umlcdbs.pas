(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file UMLC.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdbs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids, umlcguidstrs,
  umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlcactivatedcontrols,
  umlcmsgtypes, umlcmsgctrls,
  DB,
  dummy;

  // toxdo: "*custom*"

const

  // ---

  ID_TUMLCDatabase : TUMLCType =
    ($66,$6E,$01,$E7,$A1,$C8,$A7,$4F,$80,$11,$03,$37,$AC,$F0,$66,$81);

  ID_TUMLCLocalDatabase : TUMLCType =
    ($6B,$EC,$2D,$32,$1F,$97,$A8,$45,$85,$8F,$61,$D4,$52,$75,$FA,$45);

  // ---

const
  msgDatabasePathChanged =
    '{53423E40-3DB0D147-90ACD384-516B3762}';

type

  (* TUMLCDatabase *)

  TUMLCDatabase = class(TUMLCMsgServer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    //FClients: TUMLCMsgClientList;
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;

    destructor Destroy(); override;
  public
    (* Public declarations *)

    //(* Read-Only properties *)
    //
    //function ClientsCount(): Integer;
    //function HasClients(): Boolean;
    //
    //(* Never Published declarations *)
    //
    //property Clients: TUMLCMsgClientList
    //  read getClients write setClients;
  end;

  (* TUMLCLocalDatabase *)

  TUMLCLocalDatabase = class(TUMLCDatabase)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FPath: string;
  protected
    (* Protected declarations *)

    function getPath(): string;

    procedure setPath
      (const AValue: string);
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;

    destructor Destroy(); override;
  public
    (* Public declarations *)

    //(* Read-Only properties *)
    //
    //function ClientsCount(): Integer;
    //function HasClients(): Boolean;
    //
    //(* Never Published declarations *)
    //
    //property Clients: TUMLCMsgClientList
    //  read getClients write setClients;

    (* Unpublished declarations *)

    property Path: string
      read getPath write setPath;
  end;

implementation

(* TUMLCDatabase *)

constructor TUMLCDatabase.Create
  (AOwner: TComponent);
begin
  inherited Create(AOwner);

  // toxdo: ...
end;

destructor TUMLCDatabase.Destroy();
begin
  // toxdo: ...

  inherited Destroy();
end;

(* TUMLCLocalDatabase *)

constructor TUMLCLocalDatabase.Create
  (AOwner: TComponent);
begin
  inherited Create(AOwner);

  Self.FPath := '';
end;

destructor TUMLCLocalDatabase.Destroy();
begin
  Self.FPath := '';

  inherited Destroy();
end;

function TUMLCLocalDatabase.getPath(): string;
begin
  Result :=
    Self.FPath;
  // Goal: "Path" property set method.
  // Objetivo: Metodo lectura propiedad "Path".
end;

procedure TUMLCLocalDatabase.setPath
  (const AValue: string);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  if (Self.FPath <> AValue) then
  begin
    // perform change
    Self.setPath(AValue);

    // notify change to client
    umlcguidstrs.DoubleStrToGUID
      (msgDatabasePathChanged, AMsgRec.Message);

    AMsgRec.Sender  := Self;
    AMsgRec.Param   := nil;
    Self.SendMessage(AMsgRec);
  end;
  // Goal: "Path" property set method.
  // Objetivo: Metodo escritura propiedad "Path".
end;


end.

