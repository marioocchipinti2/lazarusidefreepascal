(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file UMLC.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdbdatasets;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids, umlctypes, umlcstdtypes,
  umlccomparisons,
  umlccomponents,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlcactivatedcontrols,
  umlcmsgtypes, umlcmsgctrls,
  DB,
  umlcdbs,
  dummy;
  
  // toxdo: "*custom*"
  
const

  // ---

  ID_TCustomUMLCActivatedDataset : TUMLCType =
    ($1F,$7B,$27,$3F,$4C,$BC,$DD,$49,$81,$02,$85,$17,$06,$58,$AC,$EF);

  ID_TCustomUMLCActivatedDataset : TUMLCType =
    ($A3,$71,$42,$5F,$B1,$6E,$1A,$43,$87,$12,$B0,$8F,$D0,$21,$98,$19);

  ID_TCustomUMLCDBDataset : TUMLCType =
    ($70,$35,$AF,$32,$D1,$65,$EF,$4A,$AC,$66,$F4,$29,$F8,$E1,$FE,$A8);

  // ---

type
  (* TCustomUMLCActivatedDataset *)

  TCustomUMLCActivatedDataset =
    class(TDataset, IUMLCHalfNormalizedObject)
  public
    (* public declarations *)

     // let's provide a default virtual without parameters destructor
     destructor Destroy(); override;

     // similar to Java's "ToString()"
     function AsText(): string; virtual;

     procedure DoNothing(); // nonvirtual;
  end;

  (* TCustomUMLCActivatedDataset *)

  TCustomUMLCActivatedDataset =
    class(TUMLCHalfNormalizedComponent, IUMLCActivatedControl)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FActivated: Boolean;

    function getActivated(): Boolean;
    procedure setActivated(const Value: Boolean);
  protected
    (* Protected declarations *)

    procedure ActivateFirst(); virtual;
    procedure DeactivateLast(); virtual;

    procedure Activate(); virtual;
    procedure Deactivate(); virtual;
  public
    (* Public declarations *)

    property Activated: Boolean
      read getActivated write setActivated;
  end;

  (* TCustomUMLCDBDataset *)

  TCustomUMLCDBDataset =
    class(TCustomUMLCActivatedDataset, IUMLCMessageClient)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FServer: TUMLCMsgServer;
    FOnAnswerMessage: TUMLCMsgEventHandler;
  protected
    (* Protected declarations *)

    function getServer(): TUMLCMsgServer; virtual;
    function getInternalDatabase(): TUMLCDatabase; virtual;

    procedure setServer
      (const AValue: TUMLCMsgServer); virtual;
    procedure setInternalDatabase
      (const AValue: TUMLCDatabase); virtual;
  protected
    (* Protected declarations *)

    procedure DelegateOnAnswerMessage
      (const AMsgRec: TUMLCMessageParamsRecord);

    procedure Notification
      (AComponent: TComponent; Operation: TOperation); override;
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;
  public
    (* Public declarations *)

    procedure AnswerMessage
     (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    function AsComponent(): TComponent;
  protected
    (* Protected declarations *)

    property Server: TUMLCMsgServer
      read getServer write setServer;

    property InternalDatabase: TUMLCDatabase
      read getInternalDatabase write setInternalDatabase;
  published
    (* Published declarations *)

    //property Server: TUMLCMsgServer
    //  read getServer write setServer;
    //property InternalDatabase: TUMLCDatabase
    //  read getInternalDatabase write setInternalDatabase;

    property OnAnswerMessage: TUMLCMsgEventHandler
      read FOnAnswerMessage write FOnAnswerMessage;
  end;

implementation

(* TCustomUMLCActivatedDataset *)

destructor TCustomUMLCActivatedDataset.Destroy();
begin
  inherited Destroy();
end;

function TCustomUMLCActivatedDataset.AsText(): string;
begin
  Result := ClassName();
end;

procedure TCustomUMLCActivatedDataset.DoNothing();
begin
  // Does nothing on purpose !!!
end;

(* TCustomUMLCActivatedDataset *)

function TCustomUMLCActivatedDataset.getActivated(): Boolean;
begin
  Result :=
    Self.FActivated;
  // Goal: "Activated" property get method.
  // Objetivo: Metodo lectura para propiedad "Activated".
end;

procedure TCustomUMLCActivatedDataset.setActivated
    (const Value: Boolean);
begin
  if (Self.FActivated <> Value) then
  begin
    Self.FActivated := Value;
    if (Value) then
    begin
      Self.ActivateFirst()
    end else
    begin
      Self.DeActivateLast();
    end;
  end;
  // Goal: "Activated" property set method.
  // Objetivo: Metodo escritura para propiedad "Activated".
end;

procedure TCustomUMLCActivatedDataset.ActivateFirst();
begin
  // Goal: Performa an specific action when the control is activated
  // by the first time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // activado la primera vez.
end;

procedure TCustomUMLCActivatedDataset.DeactivateLast();
begin
  // Goal: Performa an specific action when the control is dectivated
  // by the last time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // deactivado por ultima vez.
end;

procedure TCustomUMLCActivatedDataset.Activate();
begin
  // Goal: Performa an specific action when the control is activated
  // by the first time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // activado la primera vez.
end;

procedure TCustomUMLCActivatedDataset.Deactivate();
begin
  // Goal: Performa an specific action when the control is dectivated
  // by the last time.

  // Objetivo: Realizar una accion especifica cuando el control ha sido
  // deactivado por ultima vez.
end;

(* TCustomUMLCDBDataset *)

procedure TCustomUMLCDBDataset.DelegateOnAnswerMessage
 (const AMsgRec: TUMLCMessageParamsRecord);
begin
  if (Self.FOnAnswerMessage <> nil) then
  begin
    Self.FOnAnswerMessage(AMsgRec);
  end;
end;

procedure TCustomUMLCDBDataset.AnswerMessage
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  Self.DelegateOnAnswerMessage(AMsgRec);
end;

function TCustomUMLCDBDataset.AsComponent(): TComponent;
begin
  Result := Self;
end;

function TCustomUMLCDBDataset.getServer(): TUMLCMsgServer;
begin
  Result :=
    Self.FServer;
  // Goal: "Server" property set method.
  // Objetivo: Metodo lectura propiedad "Server".
end;

function TCustomUMLCDBDataset.getInternalDatabase(): TUMLCDatabase;
begin
  Result :=
    (Self.getServer() as TUMLCDatabase);
  // Goal: "InternalDatabase" property set method.
  // Objetivo: Metodo lectura propiedad "InternalDatabase".
end;

procedure TCustomUMLCDBDataset.setServer
  (const AValue: TUMLCMsgServer);
begin
  if (Self.FServer <> AValue) then
  begin
    if (Self.FServer <> nil) then
    begin
      Self.FServer.RemoveClient(Self);
    end;
    // erase client from previous server
    // borrar cliente de servidor anterior

    Self.FServer := AValue;
    // update reference
    // actualizar referencia

    if (Self.FServer <> nil) then
    begin
      Self.FServer.InsertClient(Self);
    end;
    // insert client in new server
    // insertar cliente en servidor nuevo
  end;
  // Goal: "Server" property set method.
  // Objetivo: Metodo escritura propiedad "Server".
end;

procedure TCustomUMLCDBDataset.setInternalDatabase
  (const AValue: TUMLCDatabase);
begin
  Self.setServer
    (AValue as TUMLCDatabase);
  // Goal: "InternalDatabase" property set method.
  // Objetivo: Metodo escritura propiedad "InternalDatabase".
end;

procedure TCustomUMLCDBDataset.Notification
  (AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if ((Operation = opRemove) and (AComponent = FServer)) then
  begin
    if (Self.FServer <> nil) then
    begin
      Self.FServer := nil;
    end;
  end;
  // Goal: To detect & update when associated components,
  // have been removed.

  // Objetivo: Detectar y actualizar cuando,
  // los componentes asociados se han removido.
end;

constructor TCustomUMLCDBDataset.Create
  (AOwner: TComponent);
begin
  inherited;
  Self.FServer := nil;
end;



end.

