(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file UMLC.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdbcmds;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids, umlcguidstrs,
  umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlcactivatedcontrols,
  umlcmsgtypes, umlcmsgctrls,
  DB,
  umlcdbs, umlcdbdatasets,
  dummy;
  
  // toxdo: "*custom*"
  
const

  // ---

  ID_TUMLCCustomDBComponent : TUMLCType =
    ($36,$68,$98,$00,$B7,$9C,$95,$40,$A4,$1C,$17,$CA,$1F,$67,$BD,$1F);

  ID_TUMLCCustomDBCommand : TUMLCType =
    ($8D,$35,$E1,$F3,$60,$64,$07,$4B,$9C,$9D,$1A,$59,$B1,$93,$E7,$F0);

  // ---

  ID_TUMLCCustomDBSQLCommand : TUMLCType =
    ($30,$E4,$CC,$DD,$BD,$91,$63,$43,$A1,$91,$08,$14,$02,$13,$AB,$7B);

  ID_TUMLCCustomDBSQLInsert : TUMLCType =
    ($D1,$DD,$AD,$10,$ED,$C2,$C4,$43,$A3,$B6,$0E,$1B,$01,$D8,$24,$81);

  ID_TUMLCCustomDBSQLUpdate : TUMLCType =
    ($13,$B8,$47,$1D,$88,$CC,$AE,$4A,$AD,$07,$2A,$99,$62,$04,$D1,$FF);

  ID_TUMLCCustomDBSQLDelete : TUMLCType =
    ($3A,$84,$41,$E8,$6D,$6F,$02,$40,$91,$36,$44,$10,$C0,$76,$48,$A2);

  // ---

//const
//  msgDatabaseCommandChanged =
//    '{A41C1105-30F6784F-A480D084-95042E91}';


type

  (* TUMLCCustomDBComponent *)

  TUMLCCustomDBComponent =
    class(TUMLCMsgClient)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    //FClients: TUMLCMsgClientList;
  protected
    (* Protected declarations *)

    function getInternalDatabase(): TUMLCDatabase; virtual;

    procedure setInternalDatabase
      (const AValue: TUMLCDatabase); virtual;
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;
    destructor Destroy(); override;
  protected
    (* Protected declarations *)

    property InternalDatabase: TUMLCDatabase
      read getInternalDatabase write setInternalDatabase;
  end;

  (* TUMLCCustomDBCommand *)

  TUMLCCustomDBCommand =
    class(TUMLCCustomDBComponent)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FCommand: string;
  protected
    (* Protected declarations *)

    FBeforeExecute: TNotifyEvent;
    FAfterExecute: TNotifyEvent;
  protected
    (* Protected declarations *)

    function getCommand(): string;

    procedure setCommand
      (const AValue: string);
  protected
    (* Protected declarations *)

    (* Unpublished declarations *)

    property Command: string
      read getCommand write setCommand;

    //property BeforeExecute: TNotifyEvent
    //  read FBeforeExecute write FBeforeExecute;
    //
    //property AfterExecute: TNotifyEvent
    //  read FAfterExecute write FAfterExecute;
  protected
    (* Protected declarations *)

     procedure DelegateBeforeExecute();
     procedure ConfirmedExecute(); virtual;
     procedure DelegateAfterExecute();

     procedure InternalExecute();
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;
    destructor Destroy(); override;
  end;

  (* TUMLCCustomDBSQLCommand *)

  TUMLCCustomDBSQLCommand =
    class(TUMLCDBCommand)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    procedure Execute();

    (* Unpublished declarations *)

    property BeforeExecute: TNotifyEvent
      read FBeforeExecute write FBeforeExecute;

    property AfterExecute: TNotifyEvent
      read FAfterExecute write FAfterExecute;
  end;

  (* TUMLCCustomDBSQLInsert *)

  TUMLCCustomDBSQLInsert =
    class(TUMLCDBCommand)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    procedure Insert();

    (* Unpublished declarations *)

    property BeforeInsert: TNotifyEvent
      read FBeforeExecute write FBeforeExecute;

    property AfterInsert: TNotifyEvent
      read FAfterExecute write FAfterExecute;
  end;

  (* TUMLCCustomDBSQLUpdate *)

  TUMLCCustomDBSQLUpdate =
    class(TUMLCDBCommand)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    procedure Update();

    (* Unpublished declarations *)

    property BeforeUpdate: TNotifyEvent
      read FBeforeExecute write FBeforeExecute;

    property AfterUpdate: TNotifyEvent
      read FAfterExecute write FAfterExecute;
  end;

  (* TUMLCCustomDBSQLDelete *)

  TUMLCCustomDBSQLDelete =
    class(TUMLCDBCommand)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    procedure Delete();

    (* Unpublished declarations *)

    property BeforeDelete: TNotifyEvent
      read FBeforeExecute write FBeforeExecute;

    property AfterDelete: TNotifyEvent
      read FAfterExecute write FAfterExecute;
  end;


implementation

(* TUMLCCustomDBComponent *)

constructor TUMLCCustomDBComponent.Create
  (AOwner: TComponent);
begin
  // toxdo: ...

  inherited Create(AOwner);
end;

destructor TUMLCCustomDBComponent.Destroy();
begin
  // toxdo: ...

  inherited Destroy();
end;

function TUMLCCustomDBComponent.getInternalDatabase (): TUMLCDatabase;
begin
  Result :=
    (Self.getServer() as TUMLCDatabase);
  // Goal: "InternalDatabase " property set method.
  // Objetivo: Metodo lectura propiedad "InternalDatabase ".
end;

procedure TUMLCCustomDBComponent.setInternalDatabase
  (const AValue: TUMLCDatabase);
begin
  Self.setServer(AValue);
  // Goal: "InternalDatabase " property set method.
  // Objetivo: Metodo escritura propiedad "InternalDatabase".
end;

(* TUMLCCustomDBCommand *)

constructor TUMLCDBCommand.Create
  (AOwner: TComponent);
begin
  inherited Create(AOwner);

  Self.FCommand := '';
end;

destructor TUMLCDBCommand.Destroy();
begin
  Self.FCommand := '';

  inherited Destroy();
end;

procedure TUMLCDBCommand.DelegateBeforeExecute();
begin
  if (Assigned(Self.FDelegateBeforeExecute)) then
  begin
    Self.FDelegateBeforeExecute(Self);
  end;
end;

procedure TUMLCDBCommand.ConfirmedExecute();
begin
  Self.DoNothing();
end;

procedure TUMLCDBCommand.DelegateAfterExecute();
begin
  if (Assigned(Self.FDelegateAfterExecute)) then
  begin
    Self.FDelegateAfterExecute(Self);
  end;
end;

procedure TUMLCDBCommand.InternalExecute();
begin
  Self.BeforeExecuce();
  Self.ConfirmedExecute();
  Self.AfterExecuce();
end;

function TUMLCDBCommand.getCommand(): string;
begin
  Result :=
    Self.FCommand;
  // Goal: "Command" property set method.
  // Objetivo: Metodo lectura propiedad "Command".
end;

procedure TUMLCDBCommand.setCommand
  (const AValue: string);
begin
  if (Self.FCommand <> AValue) then
  begin
    // perform change
    Self.setCommand(AValue);
  end;
  // Goal: "Command" property set method.
  // Objetivo: Metodo escritura propiedad "Command".
end;

(* TUMLCCustomDBSQLCommand *)

constructor TUMLCCustomDBSQLCommand.Create
  (AOwner: TComponent);
begin
  inherited Create(AOwner);

  // @toxdo: ...
end;

destructor TUMLCCustomDBSQLCommand.Destroy();
begin
  // @toxdo: ...

  inherited Destroy();
end;

procedure TUMLCCustomDBSQLCommand.Execute();
begin
  Self.InternalExecute();
end;

(* TUMLCCustomDBSQLInsert *)

constructor TUMLCCustomDBSQLInsert.Create
  (AOwner: TComponent);
begin
  inherited Create(AOwner);

  // @toxdo: ...
end;

destructor TUMLCCustomDBSQLInsert.Destroy();
begin
  // @toxdo: ...

  inherited Destroy();
end;

procedure TUMLCCustomDBSQLInsert.Insert();
begin
  Self.InternalExecute();
end;

(* TUMLCCustomDBSQLUpdate *)

constructor TUMLCCustomDBSQLUpdate.Create
  (AOwner: TComponent);
begin
  inherited Create(AOwner);

    // @toxdo: ...
end;

destructor TUMLCCustomDBSQLUpdate.Destroy();
begin
    // @toxdo: ...

  inherited Destroy();
end;

procedure TUMLCCustomDBSQLUpdate.Update();
begin
  Self.InternalExecute();
end;

(* TUMLCCustomDBSQLDelete *)

constructor TUMLCCustomDBSQLDelete.Create
  (AOwner: TComponent);
begin
  inherited Create(AOwner);

    // @toxdo: ...
end;

destructor TUMLCCustomDBSQLDelete.Destroy();
begin
  // @toxdo: ...

  inherited Destroy();
end;

procedure TUMLCCustomDBSQLDelete.Delete();
begin
  Self.InternalExecute();
end;

end.

