(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcinttypeobjs;

  // Encapsulate standard and simple types into objects,
  // like Java / ECMAScript

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcstdtypes,
  umlcnormobjs,
  umlcstdobjs,
  umlctypeobjs,
  dummy;

// ---

const

  MOD_umlcinttypeobjs : TUMLCModule =
    ($13,$EE,$53,$F2,$30,$58,$45,$45,$B7,$E5,$D9,$04,$98,$7A,$FD,$56);

// ---

const

  ID_TUMLCByteObject : TUMLCType =
    ($C7,$0E,$94,$48,$D8,$27,$D7,$4B,$AF,$E3,$F3,$9D,$05,$68,$14,$4C);

  ID_TUMLCWordObject : TUMLCType =
    ($8F,$50,$0E,$AF,$30,$CC,$2A,$4D,$94,$52,$F7,$F7,$E7,$40,$2C,$C5);

  ID_TUMLCLongWordObject : TUMLCType =
    ($6D,$79,$BD,$27,$6B,$0F,$1D,$4A,$89,$F0,$2A,$2A,$F7,$C9,$D1,$29);

  ID_TUMLCShortIntObject : TUMLCType =
    ($CA,$C7,$4C,$E8,$22,$2B,$2D,$45,$84,$05,$94,$7D,$05,$08,$4C,$5F);

  ID_TUMLCLongIntObject : TUMLCType =
    ($96,$0D,$CF,$37,$34,$42,$30,$45,$B0,$1D,$3C,$90,$47,$FA,$58,$75);

  ID_TUMLCInt64Object : TUMLCType =
    ($1B,$2E,$7E,$B1,$29,$5A,$A2,$49,$8C,$16,$7C,$EB,$5B,$4C,$CA,$95);

  ID_TUMLCIntegerObject : TUMLCType =
    ($1E,$FB,$4E,$54,$EA,$79,$D6,$41,$A2,$4A,$28,$DC,$EC,$A1,$9B,$A2);

  ID_TUMLCCardinalObject : TUMLCType =
    ($56,$23,$7C,$84,$55,$ED,$ED,$43,$92,$5F,$2C,$50,$52,$64,$7F,$27);

// ---

type

{ TUMLCByteObject }

  TUMLCByteObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: Byte;

    function getValue: Byte;
    procedure setValue(const AValue: Byte);
  public
    { public declarations }

    constructor CreateByValue(const AValue: Byte);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: Byte read getValue write setValue;
  end;

(* TUMLCWordObject *)

  TUMLCWordObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: Word;

    function getValue: Word;
    procedure setValue
      (const AValue: Word);
  public
    { public declarations }

    constructor CreateByValue
      (const AValue: Word);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: Word read getValue write setValue;
  end;

(* TUMLCLongWordObject *)

  TUMLCLongWordObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: LongWord;

    function getValue: LongWord;
    procedure setValue
      (const AValue: LongWord);
  public
    { public declarations }

    constructor CreateByValue
      (const AValue: LongWord);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: LongWord read getValue write setValue;
  end;

{ TUMLCShortIntObject }

  TUMLCShortIntObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: ShortInt;

    function getValue: ShortInt;
    procedure setValue(const AValue: ShortInt);
  public
    { public declarations }

    constructor CreateByValue(const AValue: ShortInt);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: ShortInt read getValue write setValue;
  end;

(* TUMLCLongIntObject *)

  TUMLCLongIntObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: LongInt;

    function getValue: LongInt;
    procedure setValue
      (const AValue: LongInt);
  public
    { public declarations }

    constructor CreateByValue
      (const AValue: LongInt);
  public
    { public declarations }

    function AsText(): string; override;

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    property Value: LongInt read getValue write setValue;
  end;

(* TUMLCInt64Object *)

  TUMLCInt64Object =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: Int64;

    function getValue: Int64;
    procedure setValue
      (const AValue: Int64);
  public
    { public declarations }

    constructor CreateByValue
      (const AValue: Int64);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: Int64 read getValue write setValue;
  end;

(* TUMLCCardinalObject *)

  TUMLCCardinalObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: Cardinal;

    function getValue: Cardinal;
    procedure setValue
      (const AValue: Cardinal);
  public
    { public declarations }

    constructor CreateByValue
      (const AValue: Cardinal);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: Cardinal read getValue write setValue;
  end;

(* TUMLCIntegerObject *)

  TUMLCIntegerObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: Integer;

    function getValue: Integer;
    procedure setValue
      (const AValue: Integer);
  public
    { public declarations }

    constructor CreateByValue
      (const AValue: Integer);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: Integer read getValue write setValue;
  end;

implementation

{ TUMLCByteObject }

function TUMLCByteObject.getValue: Byte;
begin
  Result := FValue;
end;

procedure TUMLCByteObject.setValue(const AValue: Byte);
begin
  FValue := AValue;
end;

constructor TUMLCByteObject.CreateByValue(const AValue: Byte);
begin
  FValue := AValue;
end;

procedure TUMLCByteObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCByteObject;
begin
  ATypeObject :=
    (ASource as TUMLCByteObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCByteObject.AsText(): string;
begin
  Result := IntToStr(FValue);
end;

(* TUMLCWordObject *)

function TUMLCWordObject.getValue: Word;
begin
  Result := FValue;
end;

procedure TUMLCWordObject.setValue
  (const AValue: Word);
begin
  FValue := AValue;
end;

constructor TUMLCWordObject.CreateByValue
  (const AValue: Word);
begin
  FValue := AValue;
end;

procedure TUMLCWordObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCWordObject;
begin
  ATypeObject :=
    (ASource as TUMLCWordObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCWordObject.AsText(): string;
begin
  Result := IntToStr(FValue);
end;

(* TUMLCLongWordObject *)

function TUMLCLongWordObject.getValue: LongWord;
begin
  Result := FValue;
end;

procedure TUMLCLongWordObject.setValue
  (const AValue: LongWord);
begin
  FValue := AValue;
end;

constructor TUMLCLongWordObject.CreateByValue
  (const AValue: LongWord);
begin
  FValue := AValue;
end;

procedure TUMLCLongWordObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCLongWordObject;
begin
  ATypeObject :=
    (ASource as TUMLCLongWordObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCLongWordObject.AsText(): string;
begin
  Result := IntToStr(FValue);
end;

{ TUMLCShortIntObject }

function TUMLCShortIntObject.getValue: ShortInt;
begin
  Result := FValue;
end;

procedure TUMLCShortIntObject.setValue(const AValue: ShortInt);
begin
  FValue := AValue;
end;

constructor TUMLCShortIntObject.CreateByValue(const AValue: ShortInt);
begin
  FValue := AValue;
end;

procedure TUMLCShortIntObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCShortIntObject;
begin
  ATypeObject :=
    (ASource as TUMLCShortIntObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCShortIntObject.AsText(): string;
begin
  Result := IntToStr(FValue);
end;

(* TUMLCLongIntObject *)

function TUMLCLongIntObject.getValue: LongInt;
begin
  Result := FValue;
end;

procedure TUMLCLongIntObject.setValue
  (const AValue: LongInt);
begin
  FValue := AValue;
end;

constructor TUMLCLongIntObject.CreateByValue
  (const AValue: LongInt);
begin
  FValue := AValue;
end;

function TUMLCLongIntObject.AsText(): string;
begin
  Result := IntToStr(FValue);
end;

procedure TUMLCLongIntObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCLongIntObject;
begin
  ATypeObject :=
    (ASource as TUMLCLongIntObject);
  Self.FValue := ATypeObject.FValue;
end;

(* TUMLCInt64Object *)

function TUMLCInt64Object.getValue: Int64;
begin
  Result := FValue;
end;

procedure TUMLCInt64Object.setValue
  (const AValue: Int64);
begin
  FValue := AValue;
end;

constructor TUMLCInt64Object.CreateByValue
  (const AValue: Int64);
begin
  FValue := AValue;
end;

procedure TUMLCInt64Object.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCInt64Object;
begin
  ATypeObject :=
    (ASource as TUMLCInt64Object);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCInt64Object.AsText(): string;
begin
  Result := IntToStr(FValue);
end;

(* TUMLCCardinalObject *)

function TUMLCCardinalObject.getValue: Cardinal;
begin
  Result := FValue;
end;

procedure TUMLCCardinalObject.setValue
  (const AValue: Cardinal);
begin
  FValue := AValue;
end;

constructor TUMLCCardinalObject.CreateByValue
  (const AValue: Cardinal);
begin
  FValue := AValue;
end;

procedure TUMLCCardinalObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCCardinalObject;
begin
  ATypeObject :=
    (ASource as TUMLCCardinalObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCCardinalObject.AsText(): string;
begin
  Result := IntToStr(FValue);
end;

(* TUMLCIntegerObject *)

function TUMLCIntegerObject.getValue: Integer;
begin
  Result := FValue;
end;

procedure TUMLCIntegerObject.setValue
  (const AValue: Integer);
begin
  FValue := AValue;
end;

constructor TUMLCIntegerObject.CreateByValue
  (const AValue: Integer);
begin
  FValue := AValue;
end;

procedure TUMLCIntegerObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCIntegerObject;
begin
  ATypeObject :=
    (ASource as TUMLCIntegerObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCIntegerObject.AsText(): string;
begin
  Result := IntToStr(FValue);
end;

end.

