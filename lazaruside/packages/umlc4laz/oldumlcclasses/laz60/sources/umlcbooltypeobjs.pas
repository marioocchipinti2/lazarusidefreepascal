(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbooltypeobjs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcstdtypes,
  umlcnormobjs,
  umlctypeobjs,
  umlcbooltostrconv,
  dummy;

// ---

const

  MOD_umlcbooltypeobjs : TUMLCModule =
    ($65,$1C,$96,$A5,$B3,$B0,$86,$48,$A7,$4A,$A9,$D6,$DF,$51,$DB,$CF);

// ---

const

// ---

  ID_TUMLCBooleanObject : TUMLCType =
    ($5C,$04,$88,$4B,$FD,$CD,$85,$45,$AC,$C4,$E8,$B8,$89,$B4,$C8,$62);

// ---

type

(* TUMLCBooleanObject *)

  TUMLCBooleanObject =
    class(TUMLCPrimitiveTypeObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FValue: Boolean;

    function getValue: Boolean;
    procedure setValue
      (const AValue: Boolean);
  public
    (* public declarations *)

    constructor CreateByValue
      (const AValue: Boolean);
  public
    (* public declarations *)

    function AsText: string; override;

    property Value: Boolean read getValue write setValue;
  end;


implementation

(* TUMLCBooleanObject *)

function TUMLCBooleanObject.getValue: Boolean;
begin
  Result := FValue;
end;

procedure TUMLCBooleanObject.setValue
(const AValue: Boolean);
begin
  FValue := AValue;
end;

constructor TUMLCBooleanObject.CreateByValue
  (const AValue: Boolean);
begin
  FValue := AValue;
end;

function TUMLCBooleanObject.AsText: string;
begin
  Result := umlcbooltostrconv.BoolToStr(FValue);
end;

end.

