(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlckeytypevaluetokens;

{$mode objfpc}{$H+}

interface

uses
  // ##temp {
  Crt, umlcstrings,
  // ##temp }
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcnormobjs,
  umlcobjlists,
  umlcreclists,
  umlconlystringkeytypevaluelists,
  dummy;

// ---

const

  MOD_umlckeytypevaluetokens : TUMLCModule =
    ($08,$44,$2F,$E9,$08,$18,$2E,$43,$A7,$6F,$8F,$47,$3E,$1A,$90,$FF);

// ---

type

{ TKeyValueTypeFileToken }

  TKeyValueTypeFileToken =
  (
    kvtfiletkNone,        // Sin definir
    kvtfiletkObject,      // '<object>...</object>'
    kvtfiletkProperties,  // '<properties>...</properties>'
    kvtfiletkItems,       // '<items>...</items>'
    kvtfiletkItem,        // '<item>...</item>'
    kvtfiletkKey,         // '<key>...</key>'
    kvtfiletkValue,       // '<value>...</value>'
    kvtfiletkType,        // '<type>...</type>'
    kvtfiletkText,        // '<...>Hello World</... >'
    kvtfiletkElse         // ...
  );
  PKeyValueTypeFileToken = ^TKeyValueTypeFileToken;

  function StrToKeyValueType
    (const AValue: string): TKeyValueTypeFileToken;
  function KeyValueTypeToStr
    ({copy} AValue: TKeyValueTypeFileToken): string;

  function XMLTagToKeyValueType
    (const AValue: string): TKeyValueTypeFileToken;
  
implementation

const KeyValueTypeToStrArray: array[TKeyValueTypeFileToken] of string =
(
  'kvtfiletkNone',
  'kvtfiletkObject',
  'kvtfiletkProperties',
  'kvtfiletkItems',
  'kvtfiletkItem',
  'kvtfiletkKey',
  'kvtfiletkValue',
  'kvtfiletkType',
  'kvtfiletkText',
  'kvtfiletkElse'
);

const KeyValueTypeToXMLTagArray: array[TKeyValueTypeFileToken] of string =
(
  'none',
  'object',
  'properties',
  'items',
  'item',
  'key',
  'value',
  'type',
  'text',
  'else'
);

function StrToKeyValueType
  (const AValue: string): TKeyValueTypeFileToken;
//var i: TKeyValueTypeFileToken; Found: Boolean;
var i: integer; Found: Boolean;
begin
  i := ord(Low(TKeyValueTypeFileToken)); Found := FALSE;
  while ((i <= ord(High(TKeyValueTypeFileToken))) and (not Found)) do
  begin
    Found := SameText(KeyValueTypeToStrArray[TKeyValueTypeFileToken(i)], AValue);
    i := (i + 1);
  end;

  if (Found)
    then Result := TKeyValueTypeFileToken(i - 1)
    else Result := Low(TKeyValueTypeFileToken);
end;

function KeyValueTypeToStr
  ({copy} AValue: TKeyValueTypeFileToken): string;
begin
  if (AValue < Low(TKeyValueTypeFileToken))
    then AValue := Low(TKeyValueTypeFileToken);
  if (AValue > High(TKeyValueTypeFileToken))
    then AValue := High(TKeyValueTypeFileToken);
  Result := KeyValueTypeToStrArray[AValue];
end;

function XMLTagToKeyValueType
  (const AValue: string): TKeyValueTypeFileToken;
//var i: TKeyValueTypeFileToken; Found: Boolean;
var i: integer; Found: Boolean;
begin
  i := ord(Low(TKeyValueTypeFileToken)); Found := FALSE;
  while ((i <= ord(High(TKeyValueTypeFileToken))) and (not Found)) do
  begin
    Found := SameText(KeyValueTypeToXMLTagArray[TKeyValueTypeFileToken(i)], AValue);
    i := (i + 1);
  end;

  if (Found)
    then Result := TKeyValueTypeFileToken(i - 1)
    else Result := Low(TKeyValueTypeFileToken);
end;


end.

