(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfloattypeobjs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcstdtypes,
  umlcnormobjs,
  umlcstdobjs,
  umlctypeobjs,
  dummy;

// ---

const

  MOD_umlcfloattypeobjs : TUMLCModule =
    ($CF,$2A,$6C,$1B,$6D,$BB,$4E,$41,$87,$65,$2E,$0D,$CE,$AE,$AE,$A8);

// ---

const

  ID_TUMLCRealObject : TUMLCType =
    ($6B,$54,$4C,$EE,$B4,$D4,$4C,$43,$A5,$CC,$9E,$F6,$19,$0E,$EC,$4F);

  ID_TUMLCSingleObject : TUMLCType =
    ($AB,$96,$4A,$84,$A5,$FD,$71,$4B,$BB,$EE,$63,$00,$28,$D5,$D5,$B2);

  ID_TUMLCDoubleObject : TUMLCType =
    ($88,$D4,$DC,$E2,$4E,$BA,$75,$4F,$AD,$CD,$83,$6E,$8F,$38,$1D,$02);

  ID_TUMLCExtendedObject : TUMLCType =
    ($8F,$7A,$14,$3A,$72,$70,$D1,$4F,$BC,$54,$B1,$61,$1B,$9A,$13,$EF);

// ---

type

(* TUMLCRealObject *)

  TUMLCRealObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: Real;

    function getValue: Real;
    procedure setValue
      (const AValue: Real);
  public
    { public declarations }

    constructor CreateByValue
     (const AValue: Real);
  public
    { public declarations }

    function AsText(): string; override;

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    property Value: Real read getValue write setValue;
  end;

(* TUMLCSingleObject *)

  TUMLCSingleObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: Single;

    function getValue: Single;
    procedure setValue
       (const AValue: Single);
  public
    { public declarations }

    constructor CreateByValue
       (const AValue: Single);
  public
    { public declarations }

    function AsText(): string; override;

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    property Value: Single read getValue write setValue;
  end;

(* TUMLCDoubleObject *)

  TUMLCDoubleObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: Double;

    function getValue: Double;
    procedure setValue
       (const AValue: Double);
  public
    { public declarations }

    constructor CreateByValue
       (const AValue: Double);
  public
    { public declarations }

    function AsText(): string; override;

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    property Value: Double read getValue write setValue;
  end;

(* TUMLCExtendedObject *)

  TUMLCExtendedObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: Extended;

    function getValue: Extended;
    procedure setValue
       (const AValue: Extended);
  public
    { public declarations }

    constructor CreateByValue
       (const AValue: Extended);
  public
    { public declarations }

    function AsText(): string; override;

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    property Value: Extended read getValue write setValue;
  end;

implementation

(* TUMLCRealObject *)

function TUMLCRealObject.getValue: Real;
begin
  Result := FValue;
end;

procedure TUMLCRealObject.setValue
  (const AValue: Real);
begin
  FValue := AValue;
end;

constructor TUMLCRealObject.CreateByValue
   (const AValue: Real);
begin
  FValue := AValue;
end;

function TUMLCRealObject.AsText(): string;
begin
  Result := FloatToStr(FValue);
end;

procedure TUMLCRealObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCRealObject;
begin
  ATypeObject :=
    (ASource as TUMLCRealObject);
  Self.FValue := ATypeObject.FValue;
end;

(* TUMLCSingleObject *)

function TUMLCSingleObject.getValue: Single;
begin
  Result := FValue;
end;

procedure TUMLCSingleObject.setValue
   (const AValue: Single);
begin
  FValue := AValue;
end;

constructor TUMLCSingleObject.CreateByValue
   (const AValue: Single);
begin
  FValue := AValue;
end;

function TUMLCSingleObject.AsText(): string;
begin
  Result := FloatToStr(FValue);
end;

procedure TUMLCSingleObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCSingleObject;
begin
  ATypeObject :=
    (ASource as TUMLCSingleObject);
  Self.FValue := ATypeObject.FValue;
end;

(* TUMLCDoubleObject *)

function TUMLCDoubleObject.getValue: Double;
begin
  Result := FValue;
end;

procedure TUMLCDoubleObject.setValue
   (const AValue: Double);
begin
  FValue := AValue;
end;

constructor TUMLCDoubleObject.CreateByValue
   (const AValue: Double);
begin
  FValue := AValue;
end;

function TUMLCDoubleObject.AsText(): string;
begin
  Result := FloatToStr(FValue);
end;

procedure TUMLCDoubleObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCDoubleObject;
begin
  ATypeObject :=
    (ASource as TUMLCDoubleObject);
  Self.FValue := ATypeObject.FValue;
end;

(* TUMLCExtendedObject *)

function TUMLCExtendedObject.getValue: Extended;
begin
  Result := FValue;
end;

procedure TUMLCExtendedObject.setValue
   (const AValue: Extended);
begin
  FValue := AValue;
end;

constructor TUMLCExtendedObject.CreateByValue
   (const AValue: Extended);
begin
  FValue := AValue;
end;

function TUMLCExtendedObject.AsText(): string;
begin
  Result := FloatToStr(FValue);
end;

procedure TUMLCExtendedObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCExtendedObject;
begin
  ATypeObject :=
    (ASource as TUMLCExtendedObject);
  Self.FValue := ATypeObject.FValue;
end;

end.

