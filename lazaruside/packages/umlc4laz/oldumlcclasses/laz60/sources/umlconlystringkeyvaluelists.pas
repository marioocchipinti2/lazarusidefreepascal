(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlconlystringkeyvaluelists;

{$mode objfpc}{$H+}

interface
uses
  Classes,
  SysUtils,
  umlcmodules, umlctypes,
  umlcstrings,
  umlcnormobjs,
  umlcobjlists,
  umlcstrstrlists,
  umlckeyvaluemodes,
  dummy;

// ---

const

  MOD_umlconlystringkeyvaluelists : TUMLCModule =
    ($D3,$3F,$8D,$21,$F2,$E5,$85,$45,$AC,$06,$19,$E2,$A1,$C4,$33,$4C);

// ---

type

(* TUMLCOnlyStringKeyValueItem *)

  TUMLCOnlyStringKeyValueItem =
    class(TUMLCNormalizedObject)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    _Key:   string;
    _Value: string;
  public
    (* Public declarations *)

    (* accesor declarations *)

    function getKey(): string; virtual;
    procedure setKey(const AValue: string); virtual;

    function getValue(): string; virtual;
    procedure setValue(const AValue: string); virtual;
  public
    (* Public declarations *)

    constructor CreateItem(const akey, avalue: string); virtual;
  public
    (* Public declarations *)

    function EqualKey(const AKey: string): Boolean;
    function EqualValue(const AValue: string): Boolean;

    function SameKey(const AKey: string): Boolean;
    function SameValue(const AValue: string): Boolean;
  public
    (* Public declarations *)

    property Key: string
      read getKey write setKey;
    property Value: string
      read getValue write setValue;
  end;

(* TUMLCOnlyStringKeyValueListInmediate *)

  TUMLCOnlyStringKeyValueListInmediate =
    procedure
      (const Item: TUMLCOnlyStringKeyValueItem;
       const Param: pointer) of object;

(* TUMLCOnlyStringKeyValueList *)

  TUMLCOnlyStringKeyValueList =
    class(TUMLCObjectList)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    _AllowDuplicates: boolean;
    _IgnoreKeyCase: boolean;
    _IgnoreValueCase: boolean;
  public
    (* Public declarations *)

    (* Accessors declarations *)

    function getAllowDuplicates(): boolean;
    procedure setAllowDuplicates(const AValue: boolean);

    function getIgnoreKeyCase(): boolean;
    procedure setIgnoreKeyCase(const AValue: boolean);

    function getIgnoreValueCase(): boolean;
    procedure setIgnoreValueCase(const AValue: boolean);

    function getItems
      (AIndex: Integer): TUMLCOnlyStringKeyValueItem;
    procedure setItems
      (AIndex: Integer; Item: TUMLCOnlyStringKeyValueItem);
  protected
    (* Protected declarations *)

    function MatchKeys(const A, B: string): Boolean;
    function MatchValues(const A, B: string): Boolean;
  public
    (* Public declarations *)

    constructor Create(); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    function IndexOfKey(const AKey: string): Integer;
    function IndexOfValue(const AValue: string): Integer;

    function KeyFound(const AKey: string): Boolean;
    function ValueFound(const AValue: string): Boolean;

    function ChangeKey
      (const APrevKey, ANewKey: string): Boolean;
    function ChangeValue
      (const AKey, AValue: string): Boolean;

    function ExtractKeysCopy(): TStrings;
    function ExtractValuesCopy(): TStrings;

    function InsertLast
      (const AItem: TUMLCOnlyStringKeyValueItem): Integer;

    procedure InsertAt
      (const AIndex: Integer; const AItem: TUMLCOnlyStringKeyValueItem);

    function ExtractFirst(): TUMLCOnlyStringKeyValueItem;
    function ExtractLast(): TUMLCOnlyStringKeyValueItem;

    function ExtractAt
      (const AIndex: Integer): TUMLCOnlyStringKeyValueItem;

    procedure Delete();
    procedure DeleteAt
      (const AIndex: Integer);

    function Remove
      (const AItem: TUMLCOnlyStringKeyValueItem): Integer;

    procedure Empty();
  public
    (* Public declarations *)

    procedure ForEach
      (const AProc: TUMLCOnlyStringKeyValueListInmediate; const AParam: pointer);
    procedure ForBack
      (const AProc: TUMLCOnlyStringKeyValueListInmediate; const AParam: pointer);
  public
    (* Public declarations *)

    property AllowDuplicates: boolean
      read getAllowDuplicates write setAllowDuplicates;
    property IgnoreKeyCase: boolean
      read getIgnoreKeyCase write setIgnoreKeyCase;
    property IgnoreValueCase: boolean
      read getIgnoreValueCase write setIgnoreValueCase;

    property Items[AIndex: Integer]: TUMLCOnlyStringKeyValueItem
      read getItems write setItems; default;
  end;

implementation

(* TUMLCOnlyStringKeyValueItem *)

constructor TUMLCOnlyStringKeyValueItem.CreateItem(const akey, avalue: string);
begin
  Self._Key := akey;
  Self._Value := avalue;
end;

function TUMLCOnlyStringKeyValueItem.EqualKey(const AKey: string): Boolean;
begin
  Result := (Self._Key = AKey);
end;

function TUMLCOnlyStringKeyValueItem.EqualValue(const AValue: string): Boolean;
begin
  Result := (Self._Value = AValue);
end;

function TUMLCOnlyStringKeyValueItem.SameKey(const AKey: string): Boolean;
begin
  Result := umlcstrings.SameText(Self._Key, AKey);
end;

function TUMLCOnlyStringKeyValueItem.SameValue(const AValue: string): Boolean;
begin
  Result := umlcstrings.SameText(Self._Value, AValue);
end;

function TUMLCOnlyStringKeyValueItem.getKey(): string;
begin
  Result := _Key;
end;

procedure TUMLCOnlyStringKeyValueItem.setKey(const AValue: string);
begin
  _Key := AValue;
end;

function TUMLCOnlyStringKeyValueItem.getValue(): string;
begin
  Result := _Value;
end;

procedure TUMLCOnlyStringKeyValueItem.setValue(const AValue: string);
begin
  _Value := AValue;
end;

(* TUMLCOnlyStringKeyValueList *)

function TUMLCOnlyStringKeyValueList.getAllowDuplicates(): boolean;
begin
  Result := Self._AllowDuplicates;
end;

procedure TUMLCOnlyStringKeyValueList.setAllowDuplicates(const AValue: boolean);
begin
  Self._AllowDuplicates := AValue;
end;

function TUMLCOnlyStringKeyValueList.getIgnoreKeyCase(): boolean;
begin
  Result := Self._IgnoreKeyCase;
end;

procedure TUMLCOnlyStringKeyValueList.setIgnoreKeyCase(const AValue: boolean);
begin
  Self._IgnoreKeyCase := AValue;
end;

function TUMLCOnlyStringKeyValueList.getIgnoreValueCase(): boolean;
begin
  Result := Self._IgnoreValueCase;
end;

procedure TUMLCOnlyStringKeyValueList.setIgnoreValueCase(const AValue: boolean);
begin
  Self._IgnoreValueCase := AValue;
end;

function TUMLCOnlyStringKeyValueList.getItems(AIndex: Integer): TUMLCOnlyStringKeyValueItem;
begin
  Result := TUMLCOnlyStringKeyValueItem(getInternalItems(AIndex));
end;

procedure TUMLCOnlyStringKeyValueList.setItems
  (AIndex: Integer; Item: TUMLCOnlyStringKeyValueItem);
begin
  setInternalItems(AIndex, Item);
end;

function TUMLCOnlyStringKeyValueList.MatchKeys(const A, B: string): Boolean;
begin
  Result := false;

  if (IgnoreKeyCase) then
  begin
    Result := SameText(A, B);
  end else
  begin
    Result := (A = B)
  end;
end;

function TUMLCOnlyStringKeyValueList.MatchValues(const A, B: string): Boolean;
begin
  Result := false;

  if (IgnoreValueCase) then
  begin
    Result := SameText(A, B);
  end else
  begin
    Result := (A = B)
  end;
end;

constructor TUMLCOnlyStringKeyValueList.Create();
begin
  inherited Create();
  // properties not used in constructor,
  // initial values require to use properties* fields
  _AllowDuplicates := true;
  _IgnoreKeyCase := true;
  _IgnoreValueCase := true;
end;

destructor TUMLCOnlyStringKeyValueList.Destroy();
begin
  InternalEmpty();
  inherited Destroy();
end;

// busca la primera ocurrencia de la llave indicada
function TUMLCOnlyStringKeyValueList.IndexOfKey(const AKey: string): Integer;
var Item: TUMLCOnlyStringKeyValueItem; EachIndex, ALast: Integer; Found: boolean;
begin
  Result := -1;
  ALast  := (Self.Count - 1);

  EachIndex := 0; Found := false;
  while ((not Found) and (EachIndex <= ALast)) do
  begin
    Item := TUMLCOnlyStringKeyValueItem(getInternalItems(EachIndex));

    Found := Self.MatchKeys(Item.Key, AKey);
    Inc(EachIndex);
  end;

  if (Found) then
  begin
    Result := (EachIndex - 1);
  end;
end;

function TUMLCOnlyStringKeyValueList.IndexOfValue(const AValue: string): Integer;
var Item: TUMLCOnlyStringKeyValueItem; EachIndex, ALast: Integer; Found: boolean;
begin
  Result := -1;
  ALast  := (Self.Count - 1);

  EachIndex := 0; Found := false;
  while ((not Found) and (EachIndex <= ALast)) do
  begin
    Item := TUMLCOnlyStringKeyValueItem(getInternalItems(EachIndex));

    Found := Self.MatchValues(Item.Value, AValue);
    Inc(EachIndex);
  end;

  if (Found) then
  begin
    Result := (EachIndex - 1);
  end;
end;

function TUMLCOnlyStringKeyValueList.KeyFound(const AKey: string): Boolean;
begin
  Result := (Self.IndexOfKey(AKey) > -1);
end;

function TUMLCOnlyStringKeyValueList.ValueFound(const AValue: string): Boolean;
begin
  Result := (Self.IndexOfValue(AValue) > -1);
end;

function TUMLCOnlyStringKeyValueList.ChangeKey(const APrevKey, ANewKey: string): Boolean;
var ThisIndex, DupIndex: Integer; ThisItem: TUMLCOnlyStringKeyValueItem;
begin
  Result := false;
  ThisIndex := Self.IndexOfKey(APrevKey);
  DupIndex  := Self.IndexOfKey(ANewKey);

  Result := ((ThisIndex > -1) and (DupIndex = -1));
  if (Result) then
  begin
    ThisItem := TUMLCOnlyStringKeyValueItem(getInternalItems(ThisIndex));

    ThisItem.Key := ANewKey;

    setInternalItems(ThisIndex, ThisItem);
  end;
end;

function TUMLCOnlyStringKeyValueList.ChangeValue(const AKey, AValue: string): Boolean;
var ThisIndex: Integer; ThisItem: TUMLCOnlyStringKeyValueItem;
begin
  Result := false;
  ThisIndex := Self.IndexOfKey(AKey);

  Result := (ThisIndex > -1);
  if (Result) then
  begin
    ThisItem := TUMLCOnlyStringKeyValueItem(getInternalItems(ThisIndex));

    ThisItem.Value := AValue;

    setInternalItems(ThisIndex, ThisItem);
  end;
end;

function TUMLCOnlyStringKeyValueList.ExtractKeysCopy(): TStrings;
var EachIndex, LastIndex: Integer; AItem: TUMLCOnlyStringKeyValueItem;
begin
  Result := TStringList.Create();

  LastIndex := (Self.Count - 1);
  for EachIndex := 0 to LastIndex do
  begin
    AItem := Self.Items[EachIndex];
    Result.Add(AItem.Key);
  end;
  // Goal: Returns a "TStringList" filled with a copy,
  // of all keys. The list must be deallocated by the programmer, later.
end;

function TUMLCOnlyStringKeyValueList.ExtractValuesCopy(): TStrings;
var EachIndex, LastIndex: Integer; AItem: TUMLCOnlyStringKeyValueItem;
//var EachIndex, LastIndex: Integer; AItem: TUMLCOnlyStringKeyValueItem;
begin
  Result := TStringList.Create();

  LastIndex := (Self.Count - 1);
  for EachIndex := 0 to LastIndex do
  begin
    AItem := Self.Items[EachIndex];
    Result.Add(AItem.Value);
  end;


  (*
  LastIndex := (Self.Count - 1);
  for EachIndex := 0 to LastIndex do
  begin
    AItem := Self.Items[EachIndex];
    TumlcstringstringList(Result).AddWithData(AItem.Key, AItem.Value);
  end;
  *)
  // Goal: Returns a "TStringList" filled with a copy,
  // of all values. The list must be deallocated by the programmer, later.
end;

function TUMLCOnlyStringKeyValueList.InsertLast
  (const AItem: TUMLCOnlyStringKeyValueItem): Integer;
begin
  Result := -1;
  if (AllowDuplicates) then
  begin
    Result :=
      InternalInsertLast(AItem);
  end else
  begin
    // buscar no este duplicado
    if (not Self.KeyFound(AItem.Key)) then
    begin
       Result :=
         InternalInsertLast(AItem);
    end;
  end;
end;

procedure TUMLCOnlyStringKeyValueList.InsertAt(const AIndex: Integer;
  const AItem: TUMLCOnlyStringKeyValueItem);
begin
  if (AllowDuplicates) then
  begin
    InternalInsertAt(AIndex, AItem);
  end else
  begin
    // buscar no este duplicado
    if (not Self.KeyFound(AItem.Key)) then
    begin
       InternalInsertAt(AIndex, AItem);
    end;
  end;
end;

function TUMLCOnlyStringKeyValueList.ExtractFirst
  (): TUMLCOnlyStringKeyValueItem;
begin
  Result :=
    TUMLCOnlyStringKeyValueItem(Self.InternalExtractFirst());
end;

function TUMLCOnlyStringKeyValueList.ExtractLast
  (): TUMLCOnlyStringKeyValueItem;
begin
  Result :=
    TUMLCOnlyStringKeyValueItem(Self.InternalExtractLast());
end;

function TUMLCOnlyStringKeyValueList.ExtractAt
  (const AIndex: Integer): TUMLCOnlyStringKeyValueItem;
begin
  Result :=
    TUMLCOnlyStringKeyValueItem(InternalExtractAt(AIndex));
end;

procedure TUMLCOnlyStringKeyValueList.Delete();
begin
  Self.InternalDelete();
end;

procedure TUMLCOnlyStringKeyValueList.DeleteAt
  (const AIndex: Integer);
begin
  Self.InternalDeleteAt(AIndex);
end;

function TUMLCOnlyStringKeyValueList.Remove
  (const AItem: TUMLCOnlyStringKeyValueItem): Integer;
begin
  Result := InternalRemove(AItem);
end;

procedure TUMLCOnlyStringKeyValueList.Empty();
begin
  InternalEmpty();
end;

procedure TUMLCOnlyStringKeyValueList.ForEach
  (const AProc: TUMLCOnlyStringKeyValueListInmediate;
   const AParam: pointer);
var Index: Integer; Item: TUMLCOnlyStringKeyValueItem;
begin
  for Index := 0 to Pred(Count) do
  begin
    Item := TUMLCOnlyStringKeyValueItem(getInternalItems(Index));
    AProc(Item, AParam);
  end;
end;

procedure TUMLCOnlyStringKeyValueList.ForBack
  (const AProc: TUMLCOnlyStringKeyValueListInmediate;
   const AParam: pointer);
  var Index: Integer; Item: TUMLCOnlyStringKeyValueItem;
begin
  for Index := Pred(Count) downto 0 do
  begin
    Item := TUMLCOnlyStringKeyValueItem(getInternalItems(Index));
    AProc(Item, AParam);
  end;
end;

end.

