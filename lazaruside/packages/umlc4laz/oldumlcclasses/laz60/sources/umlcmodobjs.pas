(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmodobjs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcnormobjs,
  dummy;

// ---

const

  MOD_umlcmodobjs : TUMLCModule =
   ($5E,$F0,$AD,$C9,$3C,$27,$7E,$48,$84,$CE,$92,$02,$E4,$58,$CF,$2D);

// ---

const

// ---

ID_TUMLCModularObjectClass : TUMLCType =
 ($0B,$EB,$1D,$3F,$E2,$12,$3C,$40,$86,$55,$61,$54,$14,$66,$F1,$3A);

ID_TUMLCModuleClass : TUMLCType =
 ($C5,$F6,$C7,$E7,$21,$0D,$BB,$45,$AF,$B5,$D3,$2F,$31,$C4,$28,$5E);

ID_TUMLCProgramClass : TUMLCType =
 ($76,$AA,$E7,$88,$EE,$C6,$B4,$4C,$98,$BA,$66,$AB,$60,$D3,$5E,$45);

// ---


type

{ TUMLCModularObjectClass }

  TUMLCModularObjectClass =
    class(TUMLCNormalizedObject)
  private
    { private declarations }
  protected
    { protected declarations }

    procedure Start; virtual; abstract;
    procedure Run; virtual; abstract;
    procedure Finish; virtual; abstract;
  public
    { public declarations }

    constructor Create(); override;
    destructor Destroy(); override;
  end;

type
  TOnStartEventHandler  =
    procedure (Sender: TUMLCModularObjectClass) of object;
  TOnFinishEventHandler   =
    procedure (Sender: TUMLCModularObjectClass) of object;

type

{ TUMLCModuleClass }

  TUMLCModuleClass =
    class(TUMLCModularObjectClass)
  private
    { private declarations }
  protected
    { protected declarations }

    FOnStart:  TOnStartEventHandler;
    FOnFinish: TOnFinishEventHandler;

    procedure DelegateStart;
    procedure DelegateFinish;

    procedure InternalStart; virtual;
    procedure InternalFinish; virtual;

    procedure Start; override;
    procedure Run; override;
    procedure Finish; override;
  public
    { public declarations }

    procedure Execute;
  public
    { public declarations }

    constructor Create(); override;
    destructor Destroy(); override;
  end;

type
  TOnRunEventHandler =
    procedure (Sender: TUMLCModularObjectClass) of object;

{ TUMLCProgramClass }

  TUMLCProgramClass =
    class(TUMLCModuleClass)
  private
    { private declarations }
  protected
    { protected declarations }

    FOnRun:  TOnRunEventHandler;
    FParams: TStringList;

    procedure DelegateRun;

    procedure InternalRun; virtual;

    procedure Run; override;
  public
    { public declarations }

    function ParamCount(): Integer;
    function ParamAt(const AIndex: Integer): string;

    constructor Create(); override;
    destructor Destroy(); override;
  end;

implementation

{ TUMLCModularObjectClass }

constructor TUMLCModularObjectClass.Create();
begin
  inherited Create;
end;

destructor TUMLCModularObjectClass.Destroy();
begin
  inherited Destroy;
end;

{ TUMLCModuleClass }

constructor TUMLCModuleClass.Create();
begin
  inherited Create;

  FOnStart := nil;
  FOnFinish := nil;
end;

destructor TUMLCModuleClass.Destroy();
begin
  inherited Destroy;
end;

procedure TUMLCModuleClass.DelegateStart;
begin
  if (Assigned(FOnStart)) then
  begin
    Self.FOnStart(self);
  end;
end;

procedure TUMLCModuleClass.DelegateFinish;
begin
  if (Assigned(FOnFinish)) then
  begin
    Self.FOnFinish(self);
  end;
end;

procedure TUMLCModuleClass.InternalStart;
begin
  Self.DoNothing();
end;

procedure TUMLCModuleClass.InternalFinish;
begin
  Self.DoNothing();
end;

procedure TUMLCModuleClass.Start;
begin
  Self.InternalStart;
  Self.DelegateStart;
end;

procedure TUMLCModuleClass.Run;
begin
  Self.DoNothing();
end;

procedure TUMLCModuleClass.Finish;
begin
  Self.InternalFinish;
  Self.DelegateFinish;
end;

procedure TUMLCModuleClass.Execute;
begin
  Self.Start();
  Self.Finish();
end;

{ TUMLCProgramClass }

function TUMLCProgramClass.ParamCount(): Integer;
begin
  Result := Self.FParams.Count;
end;

function TUMLCProgramClass.ParamAt
  (const AIndex: Integer): string;
begin
  Result := Self.FParams[AIndex];
end;

constructor TUMLCProgramClass.Create();
begin
  inherited Create;

  Self.FOnRun := nil;

  Self.FParams :=
    TStringList.Create();
end;

destructor TUMLCProgramClass.Destroy();
begin
  Self.FParams.Free;
  inherited Destroy;
end;

procedure TUMLCProgramClass.DelegateRun;
begin
  if (Assigned(FOnRun)) then
  begin
    Self.FOnRun(self);
  end;
end;

procedure TUMLCProgramClass.InternalRun;
begin
  Self.DoNothing();
end;

procedure TUMLCProgramClass.Run;
begin
  Self.InternalRun;
  Self.DelegateRun;
end;


end.

