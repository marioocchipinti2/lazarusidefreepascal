(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlconlystringkeytypevaluelists;

{$mode objfpc}{$H+}

interface
uses
  Classes,
  SysUtils,
  umlcmodules, umlctypes,
  umlcnormobjs,
  umlcobjlists,
  umlckeyvaluemodes,
  dummy;

// ---

const

  MOD_umlconlystringkeytypevaluelists : TUMLCModule =
    ($53,$38,$24,$38,$65,$B0,$4B,$4A,$BE,$BD,$B2,$26,$EE,$D0,$D2,$AA);

// ---

type

(* TUMLCOnlyStringKeyTypeValueItem *)

  TUMLCOnlyStringKeyTypeValueItem =
    class(TUMLCNormalizedObject)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FKey:    string;
    FTypeID: string;
    FValue:  string;
  public
    (* Public declarations *)

    constructor CreateItem
      (const AKey, ATypeID, AValue: string); virtual;

    function MatchesKey(AKey: string): Boolean;

    function AsText(): string; override;
  public
    (* Public declarations *)

    (* accesors declarations *)

    function getKey: string; virtual;
    procedure setKey(const AKey: string); virtual;

    function getTypeID: string; virtual;
    procedure setTypeID(const ATypeID: string); virtual;

    function getValue: string; virtual;
    procedure setValue(const AValue: string); virtual;
  public
    (* Public declarations *)

    (* properties declarations *)

    property Key: string
      read getKey write setKey;

    property TypeID: string
      read getTypeID write setTypeID;

    property Value: string
      read getValue write setValue;
  end;

(* TUMLCKeyValueTypeListInmediate *)

  TUMLCOnlyStringKeyValueTypeListInmediate =
    procedure
      (const AItem: TUMLCOnlyStringKeyTypeValueItem;
       const AParam: pointer) of object;

(* TUMLCOnlyStringKeyTypeValueList *)

  TUMLCOnlyStringKeyTypeValueList =
    class(TUMLCObjectList)
  private
    (* Private declarations *)

  protected
    (* Protected declarations *)

    FAllowDuplicates: boolean;
    FIgnoreKeyCase: boolean;
    FIgnoreValueCase: boolean;
  public
    (* Public declarations *)

    (* Accessors declarations *)

    function getAllowDuplicates(): boolean;
    procedure setAllowDuplicates(const value: boolean);

    function getIgnoreKeyCase(): boolean;
    procedure setIgnoreKeyCase(const value: boolean);

    function getIgnoreValueCase(): boolean;
    procedure setIgnoreValueCase(const value: boolean);

    function getItems(AIndex: Integer): TUMLCOnlyStringKeyTypeValueItem;
    procedure setItems(AIndex: Integer; Item: TUMLCOnlyStringKeyTypeValueItem);
  protected
    (* Protected declarations *)

    function MatchKeys(const A, B: string): Boolean;
    function MatchValues(const A, B: string): Boolean;
  public
    (* Public declarations *)

    constructor Create(); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    function IndexOfKey(const AKey: string): Integer;
    function IndexOfValue(const AValue: string): Integer;

    function KeyFound(const AKey: string): Boolean;
    function ValueFound(const AValue: string): Boolean;

    function ChangeValue
      (const AKey, AValue: string): Boolean;

    function InsertFirst
      (const AItem: TUMLCOnlyStringKeyTypeValueItem): Integer;
    function InsertLast
      (const AItem: TUMLCOnlyStringKeyTypeValueItem): Integer;

    procedure InsertAt
      (const AIndex: Integer; const AItem: TUMLCOnlyStringKeyTypeValueItem);

    function ExtractFirst(): TUMLCOnlyStringKeyTypeValueItem;
    function ExtractLast(): TUMLCOnlyStringKeyTypeValueItem;

    function ExtractAt
      (const AIndex: Integer): TUMLCOnlyStringKeyTypeValueItem;

    procedure Delete();
    procedure DeleteAt
      (const AIndex: Integer);

    function Remove
      (const AItem: TUMLCOnlyStringKeyTypeValueItem): Integer;
    procedure Empty();

    function ExtractKeysCopy(): TStringList;
    function ExtractTypesCopy(): TStringList;
    function ExtractValuesCopy(): TStringList;
  public
    (* Public declarations *)

    procedure ForEachForward
      (const Proc: TUMLCOnlyStringKeyValueTypeListInmediate;
       const AParam: pointer);
    procedure ForEachBackward
      (const Proc: TUMLCOnlyStringKeyValueTypeListInmediate;
       const AParam: pointer);
  public
    (* Public declarations *)

    property AllowDuplicates: boolean
      read getAllowDuplicates write setAllowDuplicates;

    property IgnoreKeyCase: boolean
      read getIgnoreKeyCase write setIgnoreKeyCase;
    property IgnoreValueCase: boolean
      read getIgnoreValueCase write setIgnoreValueCase;
    property Items[AIndex: Integer]: TUMLCOnlyStringKeyTypeValueItem
      read getItems write setItems; default;
  end;

implementation

(* TUMLCOnlyStringKeyTypeValueItem *)

constructor TUMLCOnlyStringKeyTypeValueItem.CreateItem
  (const AKey, ATypeID, AValue: string);
begin
  FKey := AKey;
  FValue := AValue;
  FTypeID := ATypeID;
end;

function TUMLCOnlyStringKeyTypeValueItem.MatchesKey(AKey: string): Boolean;
begin
  // not case sensitive
  Result := Sametext(self.Key, AKey);
end;

function TUMLCOnlyStringKeyTypeValueItem.AsText(): string;
begin
  Result :=
    '{' +
    'Key:"'   + FKey    + '",' +
    'Type:"'  + FTypeID + '",' +
    'Value:"' + FValue  + '",' +
    '}';
end;

function TUMLCOnlyStringKeyTypeValueItem.getKey(): string;
begin
  Result := FKey;
end;

procedure TUMLCOnlyStringKeyTypeValueItem.setKey(const AKey: string);
begin
  FKey := AKey;
end;

function TUMLCOnlyStringKeyTypeValueItem.getValue(): string;
begin
  Result := FValue;
end;

procedure TUMLCOnlyStringKeyTypeValueItem.setValue(const AValue: string);
begin
  FValue := AValue;
end;

function TUMLCOnlyStringKeyTypeValueItem.getTypeID(): string;
begin
  Result := FTypeID;
end;

procedure TUMLCOnlyStringKeyTypeValueItem.setTypeID(const ATypeID: string);
begin
  FTypeID := ATypeID;
end;

(* TUMLCOnlyStringKeyTypeValueList *)

function TUMLCOnlyStringKeyTypeValueList.getAllowDuplicates(): boolean;
begin
  Result := FAllowDuplicates;
end;

procedure TUMLCOnlyStringKeyTypeValueList.setAllowDuplicates(const value: boolean);
begin
  FAllowDuplicates := value;
end;

function TUMLCOnlyStringKeyTypeValueList.getIgnoreKeyCase(): boolean;
begin
  Result := FIgnoreKeyCase;
end;

procedure TUMLCOnlyStringKeyTypeValueList.setIgnoreKeyCase(const value: boolean);
begin
  FIgnoreKeyCase := value;
end;

function TUMLCOnlyStringKeyTypeValueList.getIgnoreValueCase(): boolean;
begin
  Result := FIgnoreValueCase;
end;

procedure TUMLCOnlyStringKeyTypeValueList.setIgnoreValueCase(const value: boolean);
begin
  FIgnoreValueCase := value;
end;

function TUMLCOnlyStringKeyTypeValueList.getItems(AIndex: Integer): TUMLCOnlyStringKeyTypeValueItem;
begin
  Result := TUMLCOnlyStringKeyTypeValueItem(getInternalItems(AIndex));
end;

procedure TUMLCOnlyStringKeyTypeValueList.setItems(AIndex: Integer; Item: TUMLCOnlyStringKeyTypeValueItem);
begin
  setInternalItems(AIndex, Item);
end;

function TUMLCOnlyStringKeyTypeValueList.MatchKeys(const A, B: string): Boolean;
begin
  Result := false;

  if (IgnoreKeyCase) then
  begin
    Result := SameText(A, B);
  end else
  begin
    Result := (A = B)
  end;
end;

function TUMLCOnlyStringKeyTypeValueList.MatchValues(const A, B: string): Boolean;
begin
  Result := false;

  if (IgnoreValueCase) then
  begin
    Result := SameText(A, B);
  end else
  begin
    Result := (A = B)
  end;
end;

constructor TUMLCOnlyStringKeyTypeValueList.Create();
begin
  inherited Create();
  // properties not used in constructor,
  // initial values require to use properties* fields
  FAllowDuplicates := true;
  FIgnoreKeyCase := true;
  FIgnoreValueCase := true;
end;

destructor TUMLCOnlyStringKeyTypeValueList.Destroy();
begin
  InternalEmpty();
  inherited Destroy();
end;

// busca la primera ocurrencia de la llave indicada
function TUMLCOnlyStringKeyTypeValueList.IndexOfKey(const AKey: string): Integer;
var Item: TUMLCOnlyStringKeyTypeValueItem; EachIndex, ALast: Integer; Found: boolean;
begin
  Result := -1;
  ALast  := (Self.Count - 1);

  EachIndex := 0; Found := false;
  while ((not Found) and (EachIndex <= ALast)) do
  begin
    Item := TUMLCOnlyStringKeyTypeValueItem(getInternalItems(EachIndex));

    Found := Self.MatchKeys(Item.Key, AKey);
    Inc(EachIndex);
  end;

  if (Found) then
  begin
    Result := (EachIndex - 1);
  end;
end;

function TUMLCOnlyStringKeyTypeValueList.IndexOfValue(const AValue: string): Integer;
var Item: TUMLCOnlyStringKeyTypeValueItem; EachIndex, ALast: Integer; Found: boolean;
begin
  Result := -1;
  ALast  := (Self.Count - 1);

  EachIndex := 0; Found := false;
  while ((not Found) and (EachIndex <= ALast)) do
  begin
    Item := TUMLCOnlyStringKeyTypeValueItem(getInternalItems(EachIndex));

    Found := Self.MatchValues(Item.Value, AValue);
    Inc(EachIndex);
  end;

  if (Found) then
  begin
    Result := (EachIndex - 1);
  end;
end;

function TUMLCOnlyStringKeyTypeValueList.KeyFound(const AKey: string): Boolean;
begin
  Result := (Self.IndexOfKey(AKey) > -1);
end;

function TUMLCOnlyStringKeyTypeValueList.ValueFound(const AValue: string): Boolean;
begin
  Result := (Self.IndexOfValue(AValue) > -1);
end;

function TUMLCOnlyStringKeyTypeValueList.ChangeValue
  (const AKey, AValue: string): Boolean;
var Index: Integer; Item: TUMLCOnlyStringKeyTypeValueItem;
begin
  Result := false;
  Index := Self.IndexOfKey(AKey);

  Result := (Index > -1);
  if (Result) then
  begin
    Item :=
      TUMLCOnlyStringKeyTypeValueItem(getInternalItems(Index));

    Item.Value := AValue;

    setInternalItems(Index, Item);
  end;
end;

function TUMLCOnlyStringKeyTypeValueList.InsertFirst
  (const AItem: TUMLCOnlyStringKeyTypeValueItem): Integer;
begin
  Result :=
    InternalInsertFirst(AItem);
end;

function TUMLCOnlyStringKeyTypeValueList.InsertLast
  (const AItem: TUMLCOnlyStringKeyTypeValueItem): Integer;
begin
  Result := -1;
  if (AllowDuplicates) then
  begin
    Result :=
      InternalInsertLast(AItem);
  end else
  begin
    // buscar no este duplicado
    if (not Self.KeyFound(AItem.Key)) then
    begin
       Result :=
         InternalInsertLast(AItem);
    end;
  end;
end;

procedure TUMLCOnlyStringKeyTypeValueList.InsertAt(const AIndex: Integer;
  const AItem: TUMLCOnlyStringKeyTypeValueItem);
begin
  if (AllowDuplicates) then
  begin
    InternalInsertAt(AIndex, AItem);
  end else
  begin
    // buscar no este duplicado
    if (not Self.KeyFound(AItem.Key)) then
    begin
       InternalInsertAt(AIndex, AItem);
    end;
  end;
end;

function TUMLCOnlyStringKeyTypeValueList.ExtractFirst(): TUMLCOnlyStringKeyTypeValueItem;
begin
  Result := TUMLCOnlyStringKeyTypeValueItem
    (Self.InternalExtractFirst());
end;

function TUMLCOnlyStringKeyTypeValueList.ExtractLast(): TUMLCOnlyStringKeyTypeValueItem;
begin
  Result := TUMLCOnlyStringKeyTypeValueItem
    (Self.InternalExtractLast());
end;

function TUMLCOnlyStringKeyTypeValueList.ExtractAt
  (const AIndex: Integer): TUMLCOnlyStringKeyTypeValueItem;
begin
  Result := TUMLCOnlyStringKeyTypeValueItem
    (InternalExtractAt(AIndex));
end;

procedure TUMLCOnlyStringKeyTypeValueList.Delete();
begin
  Self.InternalDelete();
end;

procedure TUMLCOnlyStringKeyTypeValueList.DeleteAt
  (const AIndex: Integer);
begin
  Self.InternalDeleteAt(AIndex);
end;

function TUMLCOnlyStringKeyTypeValueList.Remove
  (const AItem: TUMLCOnlyStringKeyTypeValueItem): Integer;
begin
  Result := InternalRemove(AItem);
end;

procedure TUMLCOnlyStringKeyTypeValueList.Empty();
begin
  InternalEmpty();
end;

function TUMLCOnlyStringKeyTypeValueList.ExtractKeysCopy(): TStringList;
var EachIndex, LastIndex: Integer; AItem: TUMLCOnlyStringKeyTypeValueItem;
begin
  Result := TStringList.Create();

  LastIndex := (Self.Count - 1);
  for EachIndex := 0 to LastIndex do
  begin
    AItem := Self.Items[EachIndex];
    Result.Add(AItem.Key);
  end;
  // Goal: Returns a "TStringList" filled with a copy,
  // of all keys.
  // The list must be deallocated by the programmer, later.
end;

function TUMLCOnlyStringKeyTypeValueList.ExtractTypesCopy(): TStringList;
var EachIndex, LastIndex: Integer; AItem: TUMLCOnlyStringKeyTypeValueItem;
begin
  Result := TStringList.Create();

  LastIndex := (Self.Count - 1);
  for EachIndex := 0 to LastIndex do
  begin
    AItem := Self.Items[EachIndex];
    Result.Add(AItem.Key);
  end;
  // Goal: Returns a "TStringList" filled with a copy,
  // of all types.
  // The list must be deallocated by the programmer, later.
end;

function TUMLCOnlyStringKeyTypeValueList.ExtractValuesCopy(): TStringList;
var EachIndex, LastIndex: Integer; AItem: TUMLCOnlyStringKeyTypeValueItem;
begin
  Result := TStringList.Create();

  LastIndex := (Self.Count - 1);
  for EachIndex := 0 to LastIndex do
  begin
    AItem := Self.Items[EachIndex];
    Result.Add(AItem.Value);
  end;
  // Goal: Returns a "TStringList" filled with a copy,
  // of all values.
  // The list must be deallocated by the programmer, later.
end;

procedure TUMLCOnlyStringKeyTypeValueList.ForEachForward(const Proc: TUMLCOnlyStringKeyValueTypeListInmediate;
  const AParam: pointer);
var Index: Integer; Item: TUMLCOnlyStringKeyTypeValueItem;
begin
  for Index := 0 to Pred(Count) do
  begin
    Item := TUMLCOnlyStringKeyTypeValueItem(getInternalItems(Index));
    Proc(Item, AParam);
  end;
end;

procedure TUMLCOnlyStringKeyTypeValueList.ForEachBackward(const Proc: TUMLCOnlyStringKeyValueTypeListInmediate;
  const AParam: pointer);
  var Index: Integer; Item: TUMLCOnlyStringKeyTypeValueItem;
begin
  for Index := Pred(Count) downto 0 do
  begin
    Item := TUMLCOnlyStringKeyTypeValueItem(getInternalItems(Index));
    Proc(Item, AParam);
  end;
end;

end.

