(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlckeyvaluemodes;

{$mode objfpc}{$H+}

interface
uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcstrings,
  umlcnormobjs,
  umlcobjlists,
  dummy;

// ---

const

  MOD_umlckeyvaluemodes : TUMLCModule =
    ($68,$6C,$8C,$6F,$5E,$52,$67,$43,$B3,$65,$79,$AC,$B5,$67,$61,$9D);

// ---

type

(* TUMLCKeyValueModes *)

  TUMLCKeyValueModes = (kvtNone, kvtKey, kvtMode, kvtValue);

implementation

end.

