(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstrstrlists;

// Declares a TStringList descendant,
// where data objects are TStringObjects,
// that can be removed automatically.

{$mode objfpc}{$H+}

interface
uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlctypeobjs, umlcstrtypeobjs,
  umlcobjlists,
  dummy;

// ---

const

  MOD_umlcstrstrlists : TUMLCModule =
    ($0F,$35,$2F,$09,$5C,$2E,$C6,$44,$BF,$39,$46,$C5,$0E,$57,$E9,$4D);

// ---

type

  (* TUMLCStringStringList *)

    TUMLCStringStringList =
      class(TStringList)
    private
      (* Private declarations *)
    protected
      (* Protected declarations *)
    public
      (* Public declarations *)

      // TObject.Create() is non-virtual
      // let's provide a default virtual without parameters constructor
      constructor Create(); reintroduce; virtual;
      // let's provide a default virtual without parameters destructor
      destructor Destroy(); override;
    public
      (* Public declarations *)
      function AddWithData(const AItem, AData: string): Integer; virtual;

      function IndexOfItem(const AItem: string): Integer; virtual;
      function IndexOfData(const AData: string): Integer; virtual;
    end;

implementation

(* TUMLCStringStringList *)

constructor TUMLCStringStringList.Create();
begin
  inherited Create();
  // call each item data object destructor
  // and deallocate from memory
  Self.OwnsObjects := true;
end;

destructor TUMLCStringStringList.Destroy();
begin
  Self.Clear();
  inherited Destroy();
end;

function TUMLCStringStringList.AddWithData
  (const AItem, AData: string): Integer;
var AString: TUMLCStringObject;
begin
  Result := -1;

  // prepare string storage
  AString := TUMLCStringObject.Create();
  AString.Value := AData;

  Result := Self.AddObject(AItem, AString);
end;

function TUMLCStringStringList.IndexOfItem(const AItem: string): Integer;
begin
  Result := Self.IndexOf(AItem);
end;

function TUMLCStringStringList.IndexOfData(const AData: string): Integer;
begin
  Result := -1;
end;

end.

