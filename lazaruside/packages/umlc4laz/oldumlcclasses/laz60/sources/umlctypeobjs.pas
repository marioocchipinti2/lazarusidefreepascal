(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctypeobjs;

  // Encapsulate standard and simple types into objects,
  // like Java / ECMAScript

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcuuids,
  umlcmodules, umlctypes,
  umlcstdtypes,
  umlcnormobjs,
  umlcstdobjs,
  umlcbooleans,
  umlcbooltostrconv,
  umlcuuidtostrconv,
  dummy;

// ---

const

  MOD_umlctypeobjs : TUMLCModule =
    ($71,$7E,$5D,$61,$91,$1A,$2C,$40,$A2,$E1,$E6,$A4,$FD,$BB,$A1,$48);

// ---

const

// ---

 ID_TUMLCPrimitiveTypeObject : TUMLCType =
   ($77,$B2,$CB,$38,$8D,$CF,$33,$4F,$90,$A8,$87,$D4,$48,$BF,$9D,$11);

// ---

 ID_TUMLCUUIDObject : TUMLCType =
   ($29,$11,$AA,$22,$A7,$D4,$44,$43,$95,$2D,$26,$18,$68,$D3,$33,$09);

// ---

type

(* TUMLCPrimitiveTypeObject *)

  TUMLCPrimitiveTypeObject =
    class(TUMLCCloneableObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function AsText: string; override;
  end;

(* TUMLCUUIDObject *)

  TUMLCUUIDObject =
    class(TUMLCPrimitiveTypeObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FValue: TUUID;

    function getValue: TUUID;
    procedure setValue
      (const AValue: TUUID);
  public
    (* public declarations *)

    constructor CreateByValue
      (const AValue: TUUID);
  public
    (* public declarations *)

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: TUUID read getValue write setValue;
  end;

implementation

(* TUMLCPrimitiveTypeObject *)

function TUMLCPrimitiveTypeObject.AsText: string;
begin
  Result := '';
end;

(* TUMLCUUIDObject *)

function TUMLCUUIDObject.getValue: TUUID;
begin
  Result := FValue;
end;

procedure TUMLCUUIDObject.setValue(const AValue: TUUID);
begin
  FValue := AValue;
end;

constructor TUMLCUUIDObject.CreateByValue
  (const AValue: TUUID);
begin
  FValue := AValue;
end;

procedure TUMLCUUIDObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCUUIDObject;
begin
  ATypeObject :=
    (ASource as TUMLCUUIDObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCUUIDObject.AsText(): string;
var S: TSingleUUIDStr;
begin
  S := '';
  umlcuuidtostrconv.UUIDToSingleStr(FValue, S);
  Result := S;
end;



end.

