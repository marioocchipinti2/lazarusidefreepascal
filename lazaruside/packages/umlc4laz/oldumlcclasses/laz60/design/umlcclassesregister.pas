unit umlcclassesregister;

interface
uses
  Classes,
{$IFDEF FPC}  
  LResources,
{$ENDIF}

  umlcclosenavs,

  umlcarrownavs,
  //umlcdbarrownavs,

  umlcpaneltabctrls,

  dummy;

  procedure Register;

implementation

{$IFDEF DELPHI}
  {$R 'umlcclosenavs.lrs'}

  {$R 'umlcarrownavs.lrs'}
  {.R 'umlcdbarrownavs.lrs'}

  {$R 'umlcpaneltabctrls.lrs'}
{$ENDIF}

procedure Register;
const
  NavigatorPalette = 'UMLCat Navigators';
begin
  RegisterComponents(NavigatorPalette, [TUMLCCloseNavigator]);

  RegisterComponents(NavigatorPalette, [TUMLCDelegateArrowNavigator]);

  //RegisterComponents(NavigatorPalette, [TUMLCDBArrowNavigator]);


  RegisterComponents(NavigatorPalette, [TUMLCPanelTabControl]);
end;

initialization
{$IFDEF FPC} 
  {$I 'umlcclosenavs.lrs'}

  {$I 'umlcarrownavs.lrs'}
  {.I 'umlcdbarrownavs.lrs'}

  {$I 'umlcpaneltabctrls.lrs'}
{$ENDIF}


end.

