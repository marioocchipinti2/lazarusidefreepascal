(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcscannerstates;

interface
uses
  dummy;

(* TUMLCState *)

type
  TUMLCState = Integer;

const
  stStart:  TUMLCState = 01;
  stFinish: TUMLCState = 00;

  stErrorUnexpectedEoF:    TUMLCState = -1;
  stErrorUnexpectedEoLn:   TUMLCState = -2;
  stErrorUnexpectedChar:   TUMLCState = -3;
  stErrorUnexpectedStart:  TUMLCState = -4;
  stErrorUnexpectedFinish: TUMLCState = -5;

implementation

end.

