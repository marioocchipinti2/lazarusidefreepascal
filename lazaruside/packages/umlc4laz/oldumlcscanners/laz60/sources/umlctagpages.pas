(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctagpages;

interface

uses
  //Windows,
  SysUtils, Classes,
  umlctreenodes, umlctreecntrs,
  umlctagprops, umlctagstyles,
  dummy;

type

(* TUMLCTagItem *)

  TUMLCTagItem = class(TUMLCContainerTreeNode)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FKeyword: string;
    FStyle:   TUMLCTagStyle;

    FValue:   string;
    // valor unico almacenado en etiqueta como bloques de texto
    // single value stored in tag such text blocks

    FUsesBreak: Boolean;
    // generar salto de linea despues de marcadores inicio o final de bloque ?
    // generate a line break after block*s start or finish markers ?

    FProperties: TUMLCTagProperties;
    // varios valores almacenados en propiedades
    // '<body forecolor = "%CCAA00" backcolor = "%AF07B3"> '

    // several values stored in properties such
    // '<body forecolor = "%CCAA00" backcolor = "%AF07B3"> '
  public
    (* public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;

    function ToStartText(): string; dynamic;
    function ToFinishText(): string; dynamic;
    function ToText(): string; dynamic;

    function PropByKeyword(const Keyword: string): TUMLCTagProperty;
    function RegisterProperty(const Keyword: string): TUMLCTagProperty;
    function RegisterPropertyValue
      (const Keyword, Value: string): TUMLCTagProperty;

    procedure CopyPropTo(const Dest: TUMLCTagItem);
  published
    (* published declarations *)

    property UsesBreak: Boolean
      read FUsesBreak write FUsesBreak;
    property Keyword: string
      read FKeyword write FKeyword;
    property Value: string
      read FValue write FValue;
    property Style: TUMLCTagStyle
      read FStyle write FStyle;
    property Properties: TUMLCTagProperties
      read FProperties write FProperties;
  end;

(* TUMLCTagCollection *)

  TUMLCTagCollection = class(TUMLCContainerTreeCollection)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;
  public
    (* public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  published
    (* published declarations *)
  end;

(* TCustomUMLCTagPage *)

  TCustomUMLCTagPage = class(TCustomUMLCTreeContainer)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    function CreateCollectionByClass(): TUMLCContainerTreeCollection; override;
  public
    (* public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

(* TUMLCTagPage *)

  TUMLCTagPage = class(TCustomUMLCTagPage)
  published
    (* published declarations *)

    { TCustomUMLCTreeContainer: }

    (*
    property OnInsert;
    property OnRemove;
    property OnUpdateName;
    property OnUpdateText;
    *)
  end;

implementation

(* TUMLCTagItem *)

procedure TUMLCTagItem.DoCreate();
begin
  inherited DoCreate();

  FKeyword := '';
  FUsesBreak := FALSE;
  FStyle := tsNone;

  FValue   := '';
  FProperties := TUMLCTagProperties.Create(TUMLCTagProperty);
end;

procedure TUMLCTagItem.DoDestroy();
begin
  FProperties.Free;
  FValue   := '';

  FStyle := tsNone;
  FKeyword := '';

  inherited DoDestroy();
end;

function TUMLCTagItem.ToStartText(): string;
begin
  Result := FKeyword;
  // to-do: include properties
end;

function TUMLCTagItem.ToFinishText(): string;
begin
  Result := FKeyword;
  // to-do: include properties
end;

function TUMLCTagItem.ToText(): string;
begin
  Result := FKeyword;
  // to-do: include properties
end;

function TUMLCTagItem.PropByKeyword(const Keyword: string): TUMLCTagProperty;
var Found: Boolean; Index: Integer;
begin
  Index := 0; Found := FALSE; Result := nil;
  while ((Index < Count) and (not Found)) do
  begin
    Result := (FProperties.Items[Index] as TUMLCTagProperty);
    Found  := ANSISameText(Result.Keyword, Keyword);
    System.Inc(Index);
  end;
  if not Found
    then Result := nil;
end;

function TUMLCTagItem.RegisterProperty(const Keyword: string): TUMLCTagProperty;
begin
  Result := PropByKeyword(Keyword);
  if (Result = nil) then
  begin
    Result := (FProperties.Add as TUMLCTagProperty);
    Result.Keyword := Keyword;
  end;
end;

function TUMLCTagItem.RegisterPropertyValue
  (const Keyword, Value: string): TUMLCTagProperty;
begin
  Result := PropByKeyword(Keyword);
  if (Result = nil) then
  begin
    Result := (FProperties.Add as TUMLCTagProperty);
    Result.Keyword := Keyword;
    Result.Value   := Value;
  end;
end;

procedure TUMLCTagItem.CopyPropTo(const Dest: TUMLCTagItem);
var I: Integer; S, D: TUMLCTagProperty;
begin
  Dest.Properties.Clear;

  for I := 0 to Pred(Self.Properties.Count) do
  begin
    S := Self.Properties.PropByIndex(i);
    D := Dest.RegisterProperty(S.Keyword);
    D.Value := S.Value;
  end;
end;

(* TUMLCTagCollection *)

function TUMLCTagCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCTagItem.Create();
  Result.DoCreate();
end;

procedure TUMLCTagCollection.DoCreate();
begin
  inherited DoCreate();
  {Your Code...}
end;

procedure TUMLCTagCollection.DoDestroy();
begin
  {Your Code...}
  inherited DoDestroy();
end;

(* TCustomUMLCTagPage *)

function TCustomUMLCTagPage.CreateCollectionByClass(): TUMLCContainerTreeCollection;
begin
  Result := TUMLCTagCollection.Create();
  Result.DoCreate();
end;

constructor TCustomUMLCTagPage.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  {Your Code...}
end;

destructor TCustomUMLCTagPage.Destroy();
begin
  {Your Code...}
  inherited Destroy();
end;

end.
 
