(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcscanners;

interface
uses
  SysUtils, Classes,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcactcntrls,
  umlccomponents,
  umlcstreams,
  umlcscannerstates,
  umlcscanneroptions,
  dummy;

 const

   // ---

   ID_TUMLCScannerOptions : TUMLCType =
     ($29,$1D,$45,$95,$77,$B4,$AB,$49,$82,$31,$E6,$FA,$10,$D7,$0C,$05);

   ID_TCustomUMLCScanner : TUMLCType =
     ($AA,$EB,$39,$9A,$42,$85,$62,$4C,$B0,$77,$DB,$49,$B7,$63,$74,$49);

   // ---

type

(* TUMLCScannerOptions *)

  TUMLCScannerOptions = class(TUMLCExtendedNormalizedComponent)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    (* fields declarations *)

    FSpecials: TUMLCScannerOption;
    FTabs:     TUMLCScannerOption;
    FSpaces:   TUMLCScannerOption;
    FEoLn:     TUMLCScannerOption;
    FEoPg:     TUMLCScannerOption;
    FEoF:      TUMLCScannerOption;
  protected
    (* protected declarations *)

    (* accesors declarations *)

    function getSpecials(): TUMLCScannerOption;
    function getTabs(): TUMLCScannerOption;
    function getSpaces(): TUMLCScannerOption;
    function getEoLn(): TUMLCScannerOption;
    function getEoPg(): TUMLCScannerOption;
    function getEoF(): TUMLCScannerOption;

    procedure setSpecials(const Value: TUMLCScannerOption);
    procedure setTabs(const Value: TUMLCScannerOption);
    procedure setSpaces(const Value: TUMLCScannerOption);
    procedure setEoLn(const Value: TUMLCScannerOption);
    procedure setEoPg(const Value: TUMLCScannerOption);
    procedure setEoF(const Value: TUMLCScannerOption);
  public
    (* public declarations *)

    constructor Create(AOwner: TComponent); override;

    procedure Clear();
  public
    (* public declarations *)

    property Specials: TUMLCScannerOption
      read getSpecials write setSpecials;
    property Tabs: TUMLCScannerOption
      read getTabs write setTabs;
    property Spaces: TUMLCScannerOption
      read getSpaces write setSpaces;
    property EoLn: TUMLCScannerOption
      read getEoLn write setEoLn;
    property EoPg: TUMLCScannerOption
      read getEoPg write setEoPg;
    property EoF: TUMLCScannerOption
      read getEoF write setEoF;
  end;

(* TCustomUMLCScanner *)

  TCustomUMLCScanner = class(TUMLCExtendedNormalizedComponent)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    (* fields declarations *)

    FInternalOptions: TUMLCScannerOptions;

    FStream: TCustomUMLCStream;

    FCurrentRow: Integer;
    FCurrentCol: Integer;
  protected
    (* protected declarations *)

    (* accesors declarations *)

    function getCurrentRow(): Integer; virtual;
    function getCurrentCol(): Integer; virtual;

    function getInternalOptions(): TUMLCScannerOptions; virtual;

    procedure setCurrentRow(const Value: Integer); virtual;
    procedure setCurrentCol(const Value: Integer); virtual;

    procedure setInternalOptions(const Value: TUMLCScannerOptions); virtual;
  protected
    (* protected declarations *)

    function CreateOptions(): TUMLCScannerOptions; virtual;

    procedure Notification
      (AComponent: TComponent; Operation: TOperation); override;
  protected
    (* protected declarations *)

    (* properties declarations *)

    property InternalOptions: TUMLCScannerOptions
      read getInternalOptions write setInternalOptions;
  public
    (* public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
    (* public declarations *)

    function Start(): Boolean; virtual;
    function Next(): Boolean; virtual;
    function Finish(): Boolean; virtual;
  public
    (* public declarations *)

    (* read-only properties declarations *)

    function Options(): TUMLCScannerOptions;
  public
    (* public declarations *)

    (* property declarations *)

    property CurrentRow: Integer
      read getCurrentRow write setCurrentRow;
    property CurrentCol: Integer
      read getCurrentCol write setCurrentCol;
  end;

implementation

(* TUMLCScannerOptions *)

function TUMLCScannerOptions.getTabs: TUMLCScannerOption;
begin
  Result := FTabs;
end;

function TUMLCScannerOptions.getSpecials(): TUMLCScannerOption;
begin
  Result := FSpecials;
end;

function TUMLCScannerOptions.getSpaces(): TUMLCScannerOption;
begin
  Result := FSpaces;
end;

function TUMLCScannerOptions.getEoLn(): TUMLCScannerOption;
begin
  Result := FEoLn;
end;

function TUMLCScannerOptions.getEoPg(): TUMLCScannerOption;
begin
  Result := FEoPg;
end;

function TUMLCScannerOptions.getEoF(): TUMLCScannerOption;
begin
  Result := FEoF;
end;

procedure TUMLCScannerOptions.setSpecials(const Value: TUMLCScannerOption);
begin
  FSpecials := Value;
end;

procedure TUMLCScannerOptions.setTabs(const Value: TUMLCScannerOption);
begin

end;

procedure TUMLCScannerOptions.setSpaces(const Value: TUMLCScannerOption);
begin
  FSpaces := Value;
end;

procedure TUMLCScannerOptions.setEoLn(const Value: TUMLCScannerOption);
begin
  FEoLn := Value;
end;

procedure TUMLCScannerOptions.setEoPg(const Value: TUMLCScannerOption);
begin
  FEoPg := Value;
end;

procedure TUMLCScannerOptions.setEoF(const Value: TUMLCScannerOption);
begin
  FEoF := Value;
end;

constructor TUMLCScannerOptions.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Clear;
end;

procedure TUMLCScannerOptions.Clear();
begin
  FSpecials := scnopIgnoreTag;
  FTabs     := scnopIgnoreTag;
  FSpaces   := scnopIgnoreTag;
  FEoLn     := scnopIgnoreTag;
  FEoPg     := scnopIgnoreTag;
  FEoF      := scnopIgnoreTag;
end;

(* TCustomUMLCScanner *)

function TCustomUMLCScanner.getCurrentRow(): Integer;
begin
  Result := FCurrentRow;
end;

function TCustomUMLCScanner.getCurrentCol(): Integer;
begin
  Result := FCurrentCol;
end;

function TCustomUMLCScanner.getInternalOptions(): TUMLCScannerOptions;
begin
  Result := FInternalOptions;
end;

procedure TCustomUMLCScanner.setCurrentRow(const Value: Integer);
begin
  if (Value <> FCurrentRow) then
  begin
    FCurrentRow := Value;
  end;
end;

procedure TCustomUMLCScanner.setCurrentCol(const Value: Integer);
begin
  if (Value <> FCurrentCol) then
  begin
    FCurrentCol := Value;
  end;
end;

procedure TCustomUMLCScanner.setInternalOptions(const Value: TUMLCScannerOptions);
begin
//  if (Value <> FInternalOptions) then
//  begin
    FInternalOptions := Value;
//  end;
end;

function TCustomUMLCScanner.CreateOptions(): TUMLCScannerOptions;
begin
  Result := TUMLCScannerOptions.Create(Self);
end;

procedure TCustomUMLCScanner.Notification
  (AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then
  begin
    if (FStream = AComponent)
      then FStream := nil;
  end;
end;

constructor TCustomUMLCScanner.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FStream  := nil;
  FInternalOptions := CreateOptions;
end;

destructor TCustomUMLCScanner.Destroy;
begin
//  FInternalOptions.Free;
  FStream := nil;
  inherited Destroy;
end;

function TCustomUMLCScanner.Start(): Boolean;
begin
  Result := FALSE;
end;

function TCustomUMLCScanner.Next(): Boolean;
begin
  Result := FALSE;
end;

function TCustomUMLCScanner.Finish(): Boolean;
begin
  Result := FALSE;
end;

function TCustomUMLCScanner.Options(): TUMLCScannerOptions;
begin
  Result := Self.FInternalOptions;
end;

end.
