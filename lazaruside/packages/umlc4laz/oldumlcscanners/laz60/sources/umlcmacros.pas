(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmacros;

interface
uses
  SysUtils, Classes,
  dummy;

type

{ TUMLCMacroStyle }

  TUMLCMacroStyle = (msNone, msExpansion, msCustom);

{ TUMLCMacroExecuteEvent }

  TUMLCMacro = class;
  TUMLCMacroExecuteEvent =
    {^}function (Macro: TUMLCMacro; const AParam: pointer): string of object;

{ TUMLCMacro }

  TCustomUMLCMacroContainer = class;
  TUMLCMacro = class(TCollectionItem)
  private
    { private declarations }
  protected
    { protected declarations }

    FKeyword: string;
    FExpansion: string;
    FStyle: TUMLCMacroStyle;

    FOnExecute: TUMLCMacroExecuteEvent;

    function DelegateOnExecute(const AParam: pointer): string;
  public
    { public declarations }

    constructor Create(ACollection: TCollection); override;

    function Execute(const AParam: pointer): string; dynamic;
  published
    { published declarations }

    property Keyword: string
      read FKeyword write FKeyword;
    property Expansion: string
      read FExpansion write FExpansion;
    property Style: TUMLCMacroStyle
      read FStyle write FStyle;

    property OnExecute: TUMLCMacroExecuteEvent
      read FOnExecute write FOnExecute;
  end;

{ TUMLCMacros }

  TUMLCMacros = class(TCollection)
  private
    { private declarations }
  protected
    { protected declarations }
  public
    { public declarations }

    constructor Create(AOwner: TPersistent; AItemClass: TCollectionItemClass);
    destructor Destroy(); override;
  published
    { published declarations }
  end;

  TMacroNotFoundEvent =
    procedure (Sender: TObject; const Macro: string) of object;

{ TCustomUMLCMacroContainer }

  TCustomUMLCMacroContainer = class(TComponent)
  private
    { private declarations }
  protected
    { protected declarations }

    FItems: TUMLCMacros;

    FOnMacroNotFound:   TMacroNotFoundEvent;

    procedure DelegateOnMacroNotFound(const Macro: string);

    procedure MacroNotFound(const Macro: string); dynamic;
  public
    { public declarations }

    function MacroByKeyword(const Keyword: string): TUMLCMacro;
    function RegisterExpansion(const Keyword, Expansion: string): TUMLCMacro;
    function RegisterCustom(const Keyword: string): TUMLCMacro;
    function Execute(const Keyword: string; const Param: pointer): string;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property Items: TUMLCMacros
      read FItems write FItems;

    property OnMacroNotFound: TMacroNotFoundEvent
      read FOnMacroNotFound write FOnMacroNotFound;
  end;

{ TUMLCMacroContainer }

  TUMLCMacroContainer = class(TCustomUMLCMacroContainer)
  published
    { published declarations }

    property Items;

    property OnMacroNotFound;
  end;

implementation

{ TUMLCMacro }

function TUMLCMacro.DelegateOnExecute(const AParam: pointer): string;
begin
  Result := '';
  if Assigned(FOnExecute) then Result := FOnExecute(Self, AParam);
end;

constructor TUMLCMacro.Create(ACollection: TCollection);
begin
  inherited Create(ACollection);
  FKeyword := '';
  FExpansion := '';
  FStyle := msNone;

  FOnExecute := nil;
end;

function TUMLCMacro.Execute(const AParam: pointer): string;
begin
  case (FStyle) of
    msExpansion: Result := FExpansion;
    msCustom:    Result := DelegateOnExecute(AParam);
//    msNone: ;
    else Result := '';
  end;
end;

{ TUMLCMacros }

constructor TUMLCMacros.Create(AOwner: TPersistent; AItemClass: TCollectionItemClass);
begin
  inherited Create(AItemClass);
  {Your Code...}
end;

destructor TUMLCMacros.Destroy();
begin
  {Your Code...}
  inherited Destroy();
end;

{ TCustomUMLCMacroContainer }

procedure TCustomUMLCMacroContainer.DelegateOnMacroNotFound
  (const Macro: string);
begin
  if Assigned(FOnMacroNotFound) then FOnMacroNotFound(Self, Macro);
end;

procedure TCustomUMLCMacroContainer.MacroNotFound(const Macro: string);
begin
  DelegateOnMacroNotFound(Macro);
end;

function TCustomUMLCMacroContainer.
  MacroByKeyword(const Keyword: string): TUMLCMacro;
var Found: Boolean; Index: Integer;
begin
  Index := 0; Found := FALSE; Result := nil;
  while ((Index < Items.Count) and (not Found)) do
  begin
    Result := (Items.Items[Index] as TUMLCMacro);
    Found  := ANSISameText(Result.Keyword, Keyword);
    System.Inc(Index);
  end;
  if not Found
    then Result := nil;
end;

function TCustomUMLCMacroContainer.RegisterExpansion
  (const Keyword, Expansion: string): TUMLCMacro;
begin
  Result := (Items.Add as TUMLCMacro);
  Result.Style   := msExpansion;
  Result.Keyword := Keyword;
  Result.Expansion := Expansion;
end;

function TCustomUMLCMacroContainer.RegisterCustom
  (const Keyword: string): TUMLCMacro;
begin
  Result := (Items.Add as TUMLCMacro);
  Result.Style   := msCustom;
  Result.Keyword := Keyword;
end;

function TCustomUMLCMacroContainer.Execute
  (const Keyword: string; const Param: pointer): string;
var Item: TUMLCMacro;
begin
  Result := '';
  Item := MacroByKeyword(Keyword);
  if (Assigned(Item))
    then Result := Item.Execute(Param)
    else MacroNotFound(Keyword);
end;

constructor TCustomUMLCMacroContainer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FItems := TUMLCMacros.Create(Self, TUMLCMacro);
  {Your Code...}
end;

destructor TCustomUMLCMacroContainer.Destroy;
begin
  {Your Code...}
  FItems.Free;
  inherited Destroy;
end;

end.
 
