unit umlcscannersregister;

interface
uses
  Classes,
{$IFDEF FPC}  
  LResources,
{$ENDIF}
  umlcmacros,
  umlctagdictionaries,
  umlctagpages,
  dummy;

  procedure Register;

implementation

{$IFDEF DELPHI}
{$R 'umlcmacros.dcr'}
{$R 'umlctagdictionaries.dcr'}
{$R 'umlctagpages.dcr'}
{.R 'umlctagtreeviews.dcr'}
{$ENDIF} 

procedure Register;
const
  ScannersPaletteName = 'UMLCat Scanners';
begin
  RegisterComponents(ScannersPaletteName, [TUMLCMacroContainer]);
  RegisterComponents(ScannersPaletteName, [TUMLCTagDictionary]);
  RegisterComponents(ScannersPaletteName, [TUMLCTagPage]);
end;

initialization
{$IFDEF FPC} 
{$I 'umlcmacros.lrs'}
{$I 'umlctagdictionaries.lrs'}
{$I 'umlctagpages.lrs'}
{$ENDIF}
end.
