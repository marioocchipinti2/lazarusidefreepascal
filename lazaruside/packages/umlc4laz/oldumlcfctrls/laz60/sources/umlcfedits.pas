(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)
unit umlcfedits;

interface
uses
{.IFDEF MSWINDOWS}
  Controls, Forms,
  Graphics, StdCtrls,
{.ENDIF}
  SysUtils, Classes,
  umlcKeyConsts,
  umlccomponents,
  umlcguidstrs,
  umlcmsgtypes, umlcmsgctrls,
  //umlcMsgLists,
{.IFDEF MSWINDOWS}
  umlcfmngrs,
  umlcedits, umlcmemos, umlcmaskeds,
{.ENDIF}
  dummy;

type

(* TCustomUMLCFocusEdit *)

  TCustomUMLCFocusEdit = class(TCustomUMLCEdit, IUMLCMessageClient)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FAlignment: TAlignment;
    // Justificar texto
    // Justify text

    FManager: TCustomUMLCFocusableControlMngr;
    FMsgHandlerList : TUMLCMessageHandlerList;
  protected
    (* Protected declarations *)

    (* Message Handlers declarations *)

    procedure HandlerServerAssign
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerServerDeassign
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrCharCaseChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrDefaultColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDefaultFontColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDefaultFontNameChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDefaultFontSizeChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrFocusedColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrFocusedFontColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrFocusedFontNameChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrFocusedFontSizeChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrDisabledColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDisabledFontColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDisabledFontNameChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDisabledFontSizeChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrReadOnlyColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrReadOnlyFontColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrReadOnlyFontNameChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrReadOnlyFontSizeChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrClear
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrRefresh
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
  protected
    (* Protected declarations *)

    procedure DoEnter(); override;
    procedure DoExit(); override;

    function getManager(): TCustomUMLCFocusableControlMngr;
    function getEnabled(): Boolean; override;
    function getReadOnly(): Boolean; override;

    procedure setManager(AValue: TCustomUMLCFocusableControlMngr);
    procedure setEnabled(const AValue: Boolean); override;
    procedure setReadOnly(const AValue: Boolean); override;

    function AsComponent(): TComponent;

    procedure AssignHandlers(); virtual;

    procedure AnswerMessage
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure setAlignment(const AValue: TAlignment); virtual;

    procedure ConfirmedChangeDefaultColor();
    procedure ConfirmedChangeDefaultFontColor();
    procedure ConfirmedChangeDefaultFontName();
    procedure ConfirmedChangeDefaultFontSize();

    procedure ConfirmedChangeFocusedColor();
    procedure ConfirmedChangeFocusedFontColor();
    procedure ConfirmedChangeFocusedFontName();
    procedure ConfirmedChangeFocusedFontSize();

    procedure ConfirmedChangeDisabledColor();
    procedure ConfirmedChangeDisabledFontColor();
    procedure ConfirmedChangeDisabledFontName();
    procedure ConfirmedChangeDisabledFontSize();

    procedure ConfirmedChangeReadOnlyColor();
    procedure ConfirmedChangeReadOnlyFontColor();
    procedure ConfirmedChangeReadOnlyFontName();
    procedure ConfirmedChangeReadOnlyFontSize();

    procedure TryChangeDefaultColor();
    procedure TryChangeDefaultFontColor();
    procedure TryChangeDefaultFontName();
    procedure TryChangeDefaultFontSize();

    procedure TryChangeFocusedColor();
    procedure TryChangeFocusedFontColor();
    procedure TryChangeFocusedFontName();
    procedure TryChangeFocusedFontSize();

    procedure TryChangeDisabledColor();
    procedure TryChangeDisabledFontColor();
    procedure TryChangeDisabledFontName();
    procedure TryChangeDisabledFontSize();

    procedure TryChangeReadOnlyColor();
    procedure TryChangeReadOnlyFontColor();
    procedure TryChangeReadOnlyFontName();
    procedure TryChangeReadOnlyFontSize();

    procedure TryChangeAutoSelect();
    procedure TryChangeCharCase();

    procedure TryClear();
    procedure TryRefresh();

    procedure Notification(AComponent: TComponent;
      Operation: TOperation); override;
  public
    (* Public declarations *)

    procedure KeyPress(var Key: Char); override;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;

    (* Never Published declarations *)

    property Alignment: TAlignment
      read FAlignment write setAlignment default taLeftJustify;
    property Manager: TCustomUMLCFocusableControlMngr
      read getManager write setManager default NIL;
  end;
  { Goal: To change color when focused ;}
  { Objetivo: Cambiar el color al enfocarse ;}

(* TUMLCFocusEdit *)

  TUMLCFocusEdit = class(TCustomUMLCFocusEdit)
  published
    (* Published declarations *)

 {.IFDEF MSWINDOWS}
    property BiDiMode;

    {$IFDEF DELPHI}
    property Ctl3D;
    property ImeMode;
    property ImeName;
    {$ENDIF}

    property DragCursor;
    property DragKind;
    property ParentBiDiMode;
    property PasswordChar;

    property OnEndDock;
    property OnStartDock;
{.ENDIF}

//    property AutoSelect;
//    property CharCase;
//    property Color;
//    property Font;
//    property ParentColor;
//    property ParentCtl3D;

      {$IFDEF DELPHI}
      property ParentCtl3D;
      property OEMConvert;
      {$ENDIF}

//    property ParentFont;

    property Anchors;
    property AutoSize;
    property BorderStyle;
    property Constraints;
    property DragMode;
    property Enabled;
    property HideSelection;
    property MaxLength;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Text;
    property Visible;

    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;

    property Alignment;
    property Manager;
  end;
  { Goal: To change color when focused ;}
  { Objetivo: Cambiar el color al enfocarse ;}

implementation

(* TCustomUMLCFocusEdit *)

procedure TCustomUMLCFocusEdit.KeyPress(var Key: Char);
var ANextControl: TWinControl; ValidKey: Boolean; TextLenght: Word;
begin
  inherited KeyPress(Key);
  // Call TEdit.KeyPress(Key);
  // Llamar TEdit.KeyPress(Key);

  ValidKey := Word(Key) >= 32;  //S�lo c�racteres
  if (ValidKey)
    then TextLenght := Length(Text + Key) - SelLength
    else TextLenght := 0;

  //verificar si la tecla presionada es la tecla definida como "exitkey"
  if ((Manager <> nil) and Manager.AutoForward and (Key = Char(VK_ENTER))) or
     ((Manager <> nil) and Manager.AutoTab and (TextLenght = MaxLength) and
     (MaxLength > 0) and ValidKey) then
  begin
    // obtener el siguiente componente de la lista de "taborder"

    ANextControl := NextControl(GetParentForm(Self as TControl) as TWinControl,
      Self as TWinControl,TRUE,TRUE,FALSE);
    // si la lista no esta vacia ir al siguiente componente

    if (ANextControl <> nil) then
    begin
      ANextControl.SetFocus();
    end;
  end
  { Goal: To focus the next control when pressing the "ExitKey" .}
  { Objetivo: Enfocar el siguiente control al presionar la tecla "ExitKey" }
end;

constructor TCustomUMLCFocusEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  // Call TComponent.Create;
  // Llamar TComponent.Create;

  FAlignment := taLeftJustify;
  FManager := NIL;
  // Clean the new properties at start
  // Limpiar las nuevas propiedades al inicio

  FMsgHandlerList := TUMLCMessageHandlerList.Create();
  AssignHandlers();
  { Goal: To prepare the component .}
  { Objetivo: Preparar el componente .}
end;

destructor TCustomUMLCFocusEdit.Destroy();
begin
  FMsgHandlerList.Free();
  FMsgHandlerList := nil;

  Manager := nil;
  // Clean the new properties at finish
  // Limpiar las nuevas propiedades al terminar

  inherited Destroy();
  { Goal: To unprepare the component .}
  { Objetivo: Despreparar el componente .}
end;

function TCustomUMLCFocusEdit.getManager(): TCustomUMLCFocusableControlMngr;
begin
  Result := FManager;
  { Goal: "Manager" property get method .}
  { Objetivo: Metodo lectura propiedad "Manager" .}
end;

function TCustomUMLCFocusEdit.getEnabled(): Boolean;
begin
  Result := inherited getEnabled();
  { Goal: "Enabled" property get method .}
  { Objetivo: Metodo lectura propiedad "Enabled" .}
end;

function TCustomUMLCFocusEdit.getReadOnly(): Boolean;
begin
  Result := inherited getReadOnly();
  { Goal: "ReadOnly" property get method .}
  { Objetivo: Metodo lectura propiedad "ReadOnly" .}
end;

procedure TCustomUMLCFocusEdit.setManager(AValue:TCustomUMLCFocusableControlMngr);
begin
  if (FManager <> AValue) then
  begin
    // remover cliente de administrador anterior
    // remove client from previous manager
    if (FManager <> nil) then
    begin
      FManager.RemoveClient(Self);
    end;

    FManager := AValue;
    // update manager
    // actualizat administrador

    // insertar cliente en nuevo administrador
    // insert client into new manager
    if (FManager <> nil) then
    begin
      FManager.InsertClient(Self);
    end;
  end;

  TryRefresh();
  { Goal: "Manager" property get method .}
  { Objetivo: Metodo lectura propiedad "Manager" .}
end;

procedure TCustomUMLCFocusEdit.setEnabled(const AValue: Boolean);
begin
  inherited setEnabled(AValue);
  if (Manager <> nil) then
  begin
    TryRefresh();
  end;
  { Goal: "Enabled" property get method .}
  { Objetivo: Metodo lectura propiedad "Enabled" .}
end;

procedure TCustomUMLCFocusEdit.setReadOnly(const AValue: Boolean);
begin
  inherited setReadOnly(AValue);
  if (Manager <> nil) then
  begin
    TryRefresh();
  end;
  { Goal: "ReadOnly" property get method .}
  { Objetivo: Metodo lectura propiedad "ReadOnly" .}
end;

function TCustomUMLCFocusEdit.AsComponent(): TComponent;
begin
  Result := Self;
end;

procedure TCustomUMLCFocusEdit.AssignHandlers();
var AMsgID: TGUID;
begin
  umlcguidstrs.DoubleStrToGUID
    (msgServerAssign, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerServerAssign);

  umlcguidstrs.DoubleStrToGUID
    (msgServerDeassign, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerServerDeassign);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrCharCaseChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrCharCaseChanged);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDefaultColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDefaultColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDefaultFontColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDefaultFontColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDefaultFontNameChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDefaultFontNameChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDefaultFontSizeChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDefaultFontSizeChanged);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrFocusedColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrFocusedColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrFocusedFontColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrFocusedFontColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrFocusedFontNameChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrFocusedFontNameChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrFocusedFontSizeChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrFocusedFontSizeChanged);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDisabledColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDisabledColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDisabledFontColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDisabledFontColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDisabledFontNameChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDisabledFontNameChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDisabledFontSizeChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDisabledFontSizeChanged);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrReadOnlyColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrReadOnlyColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrReadOnlyFontColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrReadOnlyFontColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrReadOnlyFontNameChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrReadOnlyFontNameChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrReadOnlyFontSizeChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrReadOnlyFontSizeChanged);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrClear, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrClear);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrRefresh, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrRefresh);
end;

procedure TCustomUMLCFocusEdit.AnswerMessage
  (const AMsgRec: TUMLCMessageParamsRecord);
var AHandler: TUMLCMsgEventHandler;
begin
  AHandler := FMsgHandlerList.HandlerOf(AMsgRec.Message);
  if (AHandler <> nil) then
  begin
    AHandler(AMsgRec);
  end;
end;

procedure TCustomUMLCFocusEdit.HandlerServerAssign
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryRefresh();
end;

procedure TCustomUMLCFocusEdit.HandlerServerDeassign
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryClear();
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrCharCaseChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeCharCase();
  { Goal: To change case of "TEdit" .}
  { Objetivo: Cambiar el caso de "TEdit".}
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrDefaultColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDefaultColor();
  { Goal: To change background color of control .}
  { Objetivo: Cambiar el color de fondo del control .}
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrDefaultFontColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDefaultFontColor();
  { Goal: To change text color of control .}
  { Objetivo: Cambiar el color de texto del control .}
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrDefaultFontNameChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDefaultFontName();
  { Goal: To change text color of control .}
  { Objetivo: Cambiar el color de texto del control .}
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrDefaultFontSizeChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDefaultFontSize();
  { Goal: To change text color of control .}
  { Objetivo: Cambiar el color de texto del control .}
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrFocusedColorChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeFocusedColor();
  { Goal: To change focused color of control .}
  { Objetivo: Cambiar el color enfocado del control .}
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrFocusedFontColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeFocusedFontColor();
  { Goal: To change focused font of control .}
  { Objetivo: Cambiar fuente enfocada del control .}
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrFocusedFontNameChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeFocusedFontName();
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrFocusedFontSizeChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeFocusedFontSize();
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrDisabledColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDisabledColor();
  { Goal: To change disabled color of control .}
  { Objetivo: Cambiar el color deshabilitado del control .}
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrDisabledFontColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDisabledFontColor();
  { Goal: To change disabled font of control .}
  { Objetivo: Cambiar la fuente deshabilitada del control .}
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrDisabledFontNameChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDisabledFontName();
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrDisabledFontSizeChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDisabledFontSize();
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrReadOnlyColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeReadOnlyColor();
  { Goal: To change ReadOnly color of control .}
  { Objetivo: Cambiar el color deshabilitado del control .}
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrReadOnlyFontColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeReadOnlyFontColor();
  { Goal: To change ReadOnly font of control .}
  { Objetivo: Cambiar la fuente Solo-Lectura del control .}
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrReadOnlyFontNameChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeReadOnlyFontName();
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrReadOnlyFontSizeChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeReadOnlyFontSize();
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrClear
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryClear();
end;

procedure TCustomUMLCFocusEdit.HandlerFocusMngrRefresh
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryRefresh();
  { Goal: To refresh the same color properties of control .}
  { Objetivo: Refrescar las propiedades con los mismos colores del control .}
end;

procedure TCustomUMLCFocusEdit.DoEnter();
begin
//  Self.Name := Self.Name;

  TryRefresh();
  // Update attributes
  // Modificar atributos

  Repaint();
  inherited DoEnter();
end;

procedure TCustomUMLCFocusEdit.DoExit();
begin
//  Self.Name := Self.Name;

  TryChangeAutoSelect();
  TryRefresh();
  TryChangeCharCase();
  // Update attributes
  // Modificar atributos

  Repaint();
  inherited DoExit();
end;

procedure TCustomUMLCFocusEdit.setAlignment(const AValue: TAlignment);
begin
  if (FAlignment <> AValue) then
  begin
    FAlignment := AValue;
    Repaint();
  end;
  { Goal: To justify the component .}
  { Objetivo: Justificar el componente .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeDefaultColor();
begin
  Color := Manager.readGraphicDefaultColor();
  { Goal: Change the enabled color .}
  { Objetivo: Cambiar el color al habilitar .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeDefaultFontColor();
begin
  Font.Color := Manager.readGraphicDefaultFontColor();
  { Goal: Change the default font color .}
  { Objetivo: Cambiar el color del tipofuente por defacto .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeDefaultFontName();
begin
  Font.Name := Manager.DefaultFontName;
  { Goal: Change the default font color .}
  { Objetivo: Cambiar el color del tipofuente por defacto .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeDefaultFontSize();
begin
  Font.Size := Manager.DefaultFontSize;
  { Goal: Change the default font color .}
  { Objetivo: Cambiar el color del tipofuente por defacto .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeFocusedColor();
begin
  Color := Manager.readGraphicFocusedColor();
  { Goal: Change the focused color .}
  { Objetivo: Cambiar el color al enfocar .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeFocusedFontColor();
begin
  Font.Color := Manager.readGraphicFocusedFontColor();
  { Goal: Change the enabled font .}
  { Objetivo: Cambiar el tipofuente habilitado .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeFocusedFontName();
begin
  Font.Name := Manager.FocusedFontName;
  { Goal: Change the focused font color .}
  { Objetivo: Cambiar el color del tipofuente enfocado .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeFocusedFontSize();
begin
  Font.Size := Manager.FocusedFontSize;
  { Goal: Change the focused font color .}
  { Objetivo: Cambiar el color del tipofuente enfocado .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeDisabledColor();
begin
  Color := Manager.readGraphicDisabledColor();
  { Goal: Change the disabled color .}
  { Objetivo: Cambiar el color al deshabilitar .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeDisabledFontColor();
begin
  Font.Color := Manager.readGraphicDisabledFontColor();
  { Goal: Change the disabled font .}
  { Objetivo: Cambiar el tipofuente deshabilitado .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeDisabledFontName();
begin
  Font.Name := Manager.DisabledFontName;
  { Goal: Change the disabled font color .}
  { Objetivo: Cambiar el color del tipofuente deshabilitado .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeDisabledFontSize();
begin
  Font.Size := Manager.DisabledFontSize;
  { Goal: Change the disabled font color .}
  { Objetivo: Cambiar el color del tipofuente deshabilitado .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeReadOnlyColor();
begin
  Color := Manager.readGraphicReadOnlyColor();
  { Goal: Change the readonly color .}
  { Objetivo: Cambiar el color de sololectura .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeReadOnlyFontColor();
begin
  Font.Color := Manager.readGraphicReadOnlyFontColor();;
  { Goal: Change the readonly font .}
  { Objetivo: Cambiar el tipofuente de sololectura .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeReadOnlyFontName();
begin
  Font.Name := Manager.ReadOnlyFontName;
  { Goal: Change the read-only font color .}
  { Objetivo: Cambiar el color del tipofuente solo-lectura .}
end;

procedure TCustomUMLCFocusEdit.ConfirmedChangeReadOnlyFontSize();
begin
  Font.Size := Manager.ReadOnlyFontSize;
  { Goal: Change the read-only font color .}
  { Objetivo: Cambiar el color del tipofuente solo-lectura .}
end;

procedure TCustomUMLCFocusEdit.TryChangeDefaultColor();
begin
  if ((Manager <> nil) and Manager.Enabled) then
  begin
    ConfirmedChangeDefaultColor();
  end;
  { Goal: Change the non focused color .}
  { Objetivo: Cambiar el color para no enfocado .}
end;

procedure TCustomUMLCFocusEdit.TryChangeDefaultFontColor();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDefaultFontColor();
  end;
  { Goal: Change the default font .}
  { Objetivo: Cambiar el tipofuente por defacto .}
end;

procedure TCustomUMLCFocusEdit.TryChangeDefaultFontName();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDefaultFontName();
  end;
  { Goal: Change the default font .}
  { Objetivo: Cambiar el tipofuente por defacto .}
end;

procedure TCustomUMLCFocusEdit.TryChangeDefaultFontSize();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDefaultFontSize();
  end;
  { Goal: Change the default font .}
  { Objetivo: Cambiar el tipofuente por defacto .}
end;

procedure TCustomUMLCFocusEdit.TryChangeFocusedColor();
begin
  if ((Manager <> nil) and Manager.Enabled) then
  begin
    ConfirmedChangeFocusedColor();
  end;
  { Goal: Change the focused color .}
  { Objetivo: Cambiar el color para enfocado .}
end;

procedure TCustomUMLCFocusEdit.TryChangeFocusedFontColor();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeFocusedFontColor();
  end;
  { Goal: Change the focus font .}
  { Objetivo: Cambiar el tipofuente enfocado .}
end;

procedure TCustomUMLCFocusEdit.TryChangeFocusedFontName();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeFocusedFontName();
  end;
  { Goal: Change the focused font .}
  { Objetivo: Cambiar el tipofuente por defacto .}
end;

procedure TCustomUMLCFocusEdit.TryChangeFocusedFontSize();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeFocusedFontSize();
  end;
end;

procedure TCustomUMLCFocusEdit.TryChangeDisabledColor();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDisabledColor();
  end;
  { Goal: Change the Disabled color .}
  { Objetivo: Cambiar el color para enfocado .}
end;

procedure TCustomUMLCFocusEdit.TryChangeDisabledFontColor();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeReadOnlyFontColor();
  end;
  { Goal: Change the disabled font .}
  { Objetivo: Cambiar el tipofuente deshabilitado .}
end;

procedure TCustomUMLCFocusEdit.TryChangeDisabledFontName();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDisabledFontName();
  end;
end;

procedure TCustomUMLCFocusEdit.TryChangeDisabledFontSize();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDisabledFontSize();
  end;
end;

procedure TCustomUMLCFocusEdit.TryChangeReadOnlyColor();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeReadOnlyColor();
  end;
  { Goal: Change the ReadOnly color .}
  { Objetivo: Cambiar el color para SoloLectura .}
end;

procedure TCustomUMLCFocusEdit.TryChangeReadOnlyFontColor();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeReadOnlyFontColor();
  end;
  { Goal: Change the ReadOnly font .}
  { Objetivo: Cambiar el tipofuente para SoloLectura .}
end;

procedure TCustomUMLCFocusEdit.TryChangeReadOnlyFontName();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeReadOnlyFontName();
  end;
end;

procedure TCustomUMLCFocusEdit.TryChangeReadOnlyFontSize();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeReadOnlyFontSize();
  end;
end;

procedure TCustomUMLCFocusEdit.TryChangeAutoSelect();
begin
  if ((Manager <> nil) and Manager.Enabled) then
  begin
    AutoSelect := Manager.AutoSelect;
  end;
  { Goal: Update the "AutoSelect" state .}
  { Objetivo: Actualiza el estado de "AutoSelect" .}
end;

procedure TCustomUMLCFocusEdit.TryChangeCharCase();
begin
  if ((Manager <> nil) and Manager.Enabled) then
  begin
    CharCase := Manager.CharCase;
  end;
  { Goal: Update the "CharCase" state .}
  { Objetivo: Actualiza el estatus de "CharCase" .}
end;

procedure TCustomUMLCFocusEdit.TryClear();
begin
  Self.Font.Size  := DefaultFontSize();
  Self.Font.Name  := DefaultFontName();
  Self.Font.Color := DefaultForegroundColor();
  Self.Color      := DefaultBackgroundColor();
  Self.CharCase   := TEditCharCase.ecNormal;
  Self.AutoSelect := false;
end;

procedure TCustomUMLCFocusEdit.TryRefresh();
begin
  if ((Manager <> nil) and Manager.Enabled) then
  begin
    if (Self.Enabled) then
    begin
      if (Self.ReadOnly) then
      begin
        ConfirmedChangeReadOnlyColor();
        ConfirmedChangeReadOnlyFontColor();
      end else
      begin
        if (Self.Focused) then
        begin
          ConfirmedChangeFocusedColor();
          ConfirmedChangeFocusedFontColor();
        end else
        begin
          ConfirmedChangeDefaultColor();
          ConfirmedChangeDefaultFontColor();
        end;
      end;
    end else
    begin
      ConfirmedChangeDisabledColor();
      ConfirmedChangeDisabledFontColor();
    end;
  end;

  TryChangeAutoSelect();
  TryChangeCharCase();
  { Goal: Update properties .}
  { Objetivo: Actualizar propiedades .}
end;

procedure TCustomUMLCFocusEdit.Notification
 (AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (
    (Operation = opRemove) and
    (FManager <> nil) and
    (AComponent = Manager)
    ) then
  begin
     FManager := nil;
  end;
  { Goal: To detect & update when associated components ,}
  { have been removed .}

  { Objetivo: Detectar y actualizar cuando ,}
  { los componentes asociados se han removido .}
end;

procedure Prepare();
begin
  // ...
end;

procedure UnPrepare();
begin
  // ...
end;

initialization
  Prepare();
finalization
  Unprepare();
end.

