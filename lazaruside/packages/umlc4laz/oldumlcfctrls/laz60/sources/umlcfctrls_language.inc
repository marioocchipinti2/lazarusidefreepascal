(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

(* "umlcfctrls_language.inc" *)

{$UNDEF umlcfctrls_language_english}
{$UNDEF umlcfctrls_language_spanisheurope}
{$UNDEF umlcfctrls_language_spanishlatam}
{$UNDEF umlcfctrls_language_french}
{$UNDEF umlcfctrls_language_german}
{$UNDEF umlcfctrls_language_portuguese}
{$UNDEF umlcfctrls_language_italian}

{$DEFINE umlcfctrls_language_english}
{.DEFINE umlcfctrls_language_spanisheurope}
{.DEFINE umlcfctrls_language_spanishlatam}
{.DEFINE umlcfctrls_language_french}
{.DEFINE umlcfctrls_language_german}
{.DEFINE umlcfctrls_language_portuguese}
{.DEFINE umlcfctrls_language_italian}
