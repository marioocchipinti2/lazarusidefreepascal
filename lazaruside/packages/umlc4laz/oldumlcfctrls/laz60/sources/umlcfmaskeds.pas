(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)
unit umlcfmaskeds;

interface
uses
{.IFDEF MSWINDOWS}
  Controls, Forms,
  Graphics, StdCtrls,
{.ENDIF}
  SysUtils, Classes,
  umlcKeyConsts,
  umlccomponents,
  umlcguidstrs,
  umlcmsgtypes, umlcmsgctrls,
  //umlcMsgLists,
{.IFDEF MSWINDOWS}
  umlcfmngrs,
  umlcedits, umlcmemos, umlcmaskeds,
{.ENDIF}
  dummy;

type

(* TCustomUMLCFocusMaskEdit *)

  TCustomUMLCFocusMaskEdit = class(TCustomUMLCMaskEdit, IUMLCMessageClient)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FAlignment: TAlignment;
    // Justificar texto
    // Justify text
    FManager: TCustomUMLCFocusMngr;
    FMsgHandlerList : TUMLCMessageHandlerList;
  protected
    (* Protected declarations *)

    (* Message Handlers declarations *)

    procedure HandlerServerAssign
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerServerDeassign
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrCharCaseChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrDefaultColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDefaultFontColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDefaultFontNameChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDefaultFontSizeChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrFocusedColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrFocusedFontColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrFocusedFontNameChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrFocusedFontSizeChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrDisabledColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDisabledFontColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDisabledFontNameChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrDisabledFontSizeChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrReadOnlyColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrReadOnlyFontColorChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrReadOnlyFontNameChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrReadOnlyFontSizeChanged
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure HandlerFocusMngrClear
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
    procedure HandlerFocusMngrRefresh
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;
  protected
    (* Protected declarations *)

    procedure DoEnter(); override;
    procedure DoExit(); override;

    function getManager(): TCustomUMLCFocusMngr;
    function getEnabled(): Boolean; override;
    function getReadOnly(): Boolean; override;

    procedure setManager(AValue: TCustomUMLCFocusMngr);
    procedure setEnabled(const AValue: Boolean); override;
    procedure setReadOnly(const AValue: Boolean); override;

    function AsComponent(): TComponent;

    procedure AssignHandlers(); virtual;

    procedure AnswerMessage
      (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    procedure setAlignment(const AValue: TAlignment); virtual;

    procedure ConfirmedChangeDefaultColor();
    procedure ConfirmedChangeDefaultFontColor();
    procedure ConfirmedChangeDefaultFontName();
    procedure ConfirmedChangeDefaultFontSize();

    procedure ConfirmedChangeFocusedColor();
    procedure ConfirmedChangeFocusedFontColor();
    procedure ConfirmedChangeFocusedFontName();
    procedure ConfirmedChangeFocusedFontSize();

    procedure ConfirmedChangeDisabledColor();
    procedure ConfirmedChangeDisabledFontColor();
    procedure ConfirmedChangeDisabledFontName();
    procedure ConfirmedChangeDisabledFontSize();

    procedure ConfirmedChangeReadOnlyColor();
    procedure ConfirmedChangeReadOnlyFontColor();
    procedure ConfirmedChangeReadOnlyFontName();
    procedure ConfirmedChangeReadOnlyFontSize();

    procedure TryChangeFocusedColor();
    procedure TryChangeFocusedFontColor();
    procedure TryChangeFocusedFontName();
    procedure TryChangeFocusedFontSize();

    procedure TryChangeDefaultColor();
    procedure TryChangeDefaultFontColor();
    procedure TryChangeDefaultFontName();
    procedure TryChangeDefaultFontSize();

    procedure TryChangeDisabledColor();
    procedure TryChangeDisabledFontColor();
    procedure TryChangeDisabledFontName();
    procedure TryChangeDisabledFontSize();

    procedure TryChangeReadOnlyColor();
    procedure TryChangeReadOnlyFontColor();
    procedure TryChangeReadOnlyFontName();
    procedure TryChangeReadOnlyFontSize();

    procedure TryChangeAutoSelect();
    procedure TryChangeCharCase();

    procedure TryClear();
    procedure TryRefresh();

    procedure Notification(AComponent: TComponent;
      Operation: TOperation); override;
  public
    (* Public declarations *)

    procedure KeyPress(var Key: Char); override;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;

    (* Never Published declarations *)

    property Alignment: TAlignment
      read FAlignment write setAlignment default taLeftJustify;
    property Manager: TCustomUMLCFocusMngr
      read getManager write setManager default NIL;
  end;
  { Goal: To change color when focused ;}
  { Objetivo: Cambiar el color al enfocarse ;}

{ TUMLCFocusMaskEdit }

  TUMLCFocusMaskEdit = class(TCustomUMLCFocusMaskEdit)
  published
    (* Published declarations *)

    (* TCustomEdit: *)

{.IFDEF MSWINDOWS}
    property BiDiMode;
    {$IFDEF DELPHI}
    property Ctl3D;
    property ImeMode;
    property ImeName;
    property OEMConvert;
    {$ENDIF}
    property DragCursor;
    property DragKind;
    property ParentBiDiMode;
    property PasswordChar;

    property OnEndDock;
    property OnStartDock;
{.ENDIF}

//    property AutoSelect;
//    property CharCase;
//    property Color;
//    property Font;
//    property ParentColor;
//    property ParentFont;

    {$IFDEF DELPHI}
    property ParentCtl3D;
    {$ENDIF}

    property Anchors;
    property AutoSize;
    property BorderStyle;
    property Constraints;
    property DragMode;
    property Enabled;
    property HideSelection;
    property MaxLength;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Text;
    property Visible;

    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;

    (* TCustomUMLCFocusMaskEdit: *)

    property EditMask;

    (* TCustomEdit: *)

    property Alignment;
    property Manager;
  end;
  { Goal: To change color when focused ;}
  { Objetivo: Cambiar el color al enfocarse ;}

implementation

(* TCustomUMLCFocusMaskEdit *)

procedure TCustomUMLCFocusMaskEdit.KeyPress(var Key: Char);
var ANextControl: TWinControl; ValidKey: Boolean; TextLenght: Word;
begin
  inherited KeyPress(Key);
  // Call TMaskEdit.KeyPress(Key);
  // Llamar TMaskEdit.KeyPress(Key);

  ValidKey := Word(Key) >= 32;  //S�lo c�racteres
  if (ValidKey)
    then TextLenght := Length(Text + Key) - SelLength
    else TextLenght := 0;

  //verificar si la tecla presionada es la tecla definida como "exitkey"
  if ((Manager <> nil) and Manager.AutoForward and (Key = Char(VK_ENTER))) or
     ((Manager <> nil) and Manager.AutoTab and (TextLenght = MaxLength) and
     (MaxLength > 0) and ValidKey) then
  begin
    // obtener el siguiente componente de la lista de "taborder"

    ANextControl := NextControl(GetParentForm(Self as TControl) as TWinControl,
      Self as TWinControl,TRUE,TRUE,FALSE);
    // si la lista no esta vacia ir al siguiente componente

    if (ANextControl <> nil) then
    begin
      ANextControl.SetFocus;
    end;
  end
  { Goal: To focus the next control when pressing the "ExitKey" .}
  { Objetivo: Enfocar el siguiente control al presionar la tecla "ExitKey" }
end;

constructor TCustomUMLCFocusMaskEdit.Create(AOwner: TComponent);
begin
  inherited;
  // Call TComponent.Create;
  // Llamar TComponent.Create;

  FAlignment := taLeftJustify;
  FManager := NIL;
  // Clean the new propierties at start
  // Limpiar las nuevas propiedades al inicio

  FMsgHandlerList := TUMLCMessageHandlerList.Create();
  AssignHandlers();
  { Goal: To prepare the component .}
  { Objetivo: Preparar el componente .}
end;

destructor TCustomUMLCFocusMaskEdit.Destroy();
begin
  FMsgHandlerList.Free();
  FMsgHandlerList := nil;

  Manager := nil;
  // Clean the new propierties at finish
  // Limpiar las nuevas propiedades al terminar

  inherited Destroy();
  { Goal: To unprepare the component .}
  { Objetivo: Despreparar el componente .}
end;

function TCustomUMLCFocusMaskEdit.getManager(): TCustomUMLCFocusMngr;
begin
  Result := FManager;
  { Goal: "Manager" property get method .}
  { Objetivo: Metodo lectura propiedad "Manager" .}
end;

function TCustomUMLCFocusMaskEdit.getEnabled(): Boolean;
begin
  Result := inherited getEnabled();
  { Goal: "Enabled" property get method .}
  { Objetivo: Metodo lectura propiedad "Enabled" .}
end;

function TCustomUMLCFocusMaskEdit.getReadOnly(): Boolean;
begin
  Result := inherited getReadOnly();
  { Goal: "ReadOnly" property get method .}
  { Objetivo: Metodo lectura propiedad "ReadOnly" .}
end;

procedure TCustomUMLCFocusMaskEdit.setManager(AValue:TCustomUMLCFocusMngr);
begin
  if (FManager <> AValue) then
  begin
    if (FManager <> nil)
      then FManager.RemoveClient(Self);
    // remover cliente de administrador anterior
    // remove client from previous manager

    FManager := AValue;
    // update manager
    // actualizat administrador

    if (FManager <> nil)
      then FManager.InsertClient(Self);
    // insertar cliente en nuevo administrador
    // insert client into new manager
  end;
  (*
  TryChangeAutoSelect();
  TryChangeFocusedColor();
  TryChangeDefaultColor();
  TryChangeCharCase();
  *)
  { Goal: "Manager" property get method .}
  { Objetivo: Metodo lectura propiedad "Manager" .}
end;

procedure TCustomUMLCFocusMaskEdit.setEnabled(const AValue: Boolean);
begin
  inherited setEnabled(AValue);
  if (Manager <> nil) then
  begin
    if (Enabled) then
    begin
      ConfirmedChangeDefaultColor();
      ConfirmedChangeDefaultFontColor();
    end else
    begin
      ConfirmedChangeDisabledColor();
      ConfirmedChangeDisabledFontColor();
    end;
  end;
  { Goal: "Enabled" property get method .}
  { Objetivo: Metodo lectura propiedad "Enabled" .}
end;

procedure TCustomUMLCFocusMaskEdit.setReadOnly(const AValue: Boolean);
begin
  inherited setReadOnly(AValue);
  if (Manager <> nil) then
  begin
    if (ReadOnly) then
    begin
      ConfirmedChangeReadOnlyColor();
      ConfirmedChangeReadOnlyFontColor();
    end else
    begin
      ConfirmedChangeDefaultColor();
      ConfirmedChangeDefaultFontColor();
    end;
  end;
  { Goal: "ReadOnly" property get method .}
  { Objetivo: Metodo lectura propiedad "ReadOnly" .}
end;

function TCustomUMLCFocusMaskEdit.AsComponent(): TComponent;
begin
  Result := Self;
end;

procedure TCustomUMLCFocusMaskEdit.AssignHandlers();
var AMsgID: TGUID;
begin
  umlcguidstrs.DoubleStrToGUID
    (msgServerAssign, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerServerAssign);

  umlcguidstrs.DoubleStrToGUID
    (msgServerDeassign, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerServerDeassign);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrCharCaseChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrCharCaseChanged);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDefaultColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDefaultColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDefaultFontColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDefaultFontColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDefaultFontNameChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDefaultFontNameChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDefaultFontSizeChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDefaultFontSizeChanged);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrFocusedColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrFocusedColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrFocusedFontColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrFocusedFontColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrFocusedFontNameChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrFocusedFontNameChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrFocusedFontSizeChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrFocusedFontSizeChanged);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDisabledColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDisabledColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDisabledFontColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDisabledFontColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDisabledFontNameChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDisabledFontNameChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrDisabledFontSizeChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrDisabledFontSizeChanged);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrReadOnlyColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrReadOnlyColorChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrReadOnlyFontColorChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrRefresh);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrReadOnlyFontNameChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrReadOnlyFontNameChanged);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrReadOnlyFontSizeChanged, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrReadOnlyFontSizeChanged);

  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrClear, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrClear);
  umlcguidstrs.DoubleStrToGUID
    (msgFocusMngrRefresh, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}HandlerFocusMngrRefresh);
end;

procedure TCustomUMLCFocusMaskEdit.AnswerMessage
  (const AMsgRec: TUMLCMessageParamsRecord);
var AHandler: TUMLCMsgEventHandler;
begin
  AHandler := FMsgHandlerList.HandlerOf(AMsgRec.Message);
  if (AHandler <> nil) then
  begin
    AHandler(AMsgRec);
  end;
end;

procedure TCustomUMLCFocusMaskEdit.HandlerServerAssign
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryRefresh();
end;

procedure TCustomUMLCFocusMaskEdit.HandlerServerDeassign
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryClear();
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrCharCaseChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeCharCase();
  { Goal: To change case of control .}
  { Objetivo: Cambiar el caso del control .}
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrDefaultColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDefaultColor();
  { Goal: To change default font color of control .}
  { Objetivo: Cambiar el color del tipofuente por defacto del control .}
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrFocusedColorChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeFocusedColor();
  { Goal: To change focused color of "TMaskEdit" .}
  { Objetivo: Cambiar el color enfocado de "TMaskEdit".}
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrFocusedFontColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeFocusedFontColor();
  { Goal: To change focused font of "TMaskEdit" .}
  { Objetivo: Cambiar fuente enfocada de "TMaskEdit".}
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrFocusedFontNameChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeFocusedFontName();
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrFocusedFontSizeChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeFocusedFontSize();
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrDefaultFontColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDefaultColor();
  { Goal: To change font of "TMaskEdit" .}
  { Objetivo: Cambiar el tipofuente de "TMaskEdit".}
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrDefaultFontNameChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDefaultFontName();
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrDefaultFontSizeChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDefaultFontSize();
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrDisabledColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDisabledColor();
  { Goal: To change disabled color of "TMaskEdit" .}
  { Objetivo: Cambiar el color deshabilitado de "TMaskEdit".}
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrDisabledFontColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDisabledFontColor();
  { Goal: To change disabled font of "TMaskEdit" .}
  { Objetivo: Cambiar la fuente deshabilitada de "TMaskEdit".}
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrDisabledFontNameChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDisabledFontName();
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrDisabledFontSizeChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeDisabledFontSize();
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrReadOnlyColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeReadOnlyColor();
  { Goal: To change ReadOnly color of "TMaskEdit" .}
  { Objetivo: Cambiar el color deshabilitado de "TMaskEdit".}
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrReadOnlyFontColorChanged
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeReadOnlyFontColor();
  { Goal: To change ReadOnly font of "TMaskEdit" .}
  { Objetivo: Cambiar la fuente deshabilitada de "TMaskEdit".}
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrReadOnlyFontNameChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeReadOnlyFontName();
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrReadOnlyFontSizeChanged
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryChangeReadOnlyFontSize();
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrClear
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryClear();
end;

procedure TCustomUMLCFocusMaskEdit.HandlerFocusMngrRefresh
(const AMsgRec: TUMLCMessageParamsRecord);
begin
  TryRefresh();
  { Goal: To refresh the same color properties of "TEdit" .}
  { Objetivo: Refrescar las propiedades con los mismos colores de "TEdit".}
end;

procedure TCustomUMLCFocusMaskEdit.DoEnter();
begin
  TryRefresh();
  // Update attributes
  // Modificar atributos

  Repaint();
  inherited DoEnter();
end;

procedure TCustomUMLCFocusMaskEdit.DoExit();
begin
  TryChangeAutoSelect();
  TryRefresh();
  TryChangeCharCase();
  // Update attributes
  // Modificar atributos

  Repaint();
  inherited DoExit();
end;

procedure TCustomUMLCFocusMaskEdit.setAlignment(const AValue: TAlignment);
begin
  if (FAlignment <> AValue) then
  begin
    FAlignment := AValue;
    Repaint();
  end;
  { Goal: To justify the component .}
  { Objetivo: Justificar el componente .}
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeDefaultColor();
begin
  Color := Manager.DefaultColor;
  Font.Color := Manager.DefaultFontColor;
  { Goal: Change the enabled color .}
  { Objetivo: Cambiar el color al habilitar .}
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeDefaultFontColor();
begin
  Font.Color := Manager.DefaultFontColor;
  { Goal: Change the enabled font .}
  { Objetivo: Cambiar el tipofuente habilitado .}
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeDefaultFontName();
begin
  Font.Name := Manager.DefaultFontName;
  { Goal: Change the default font color .}
  { Objetivo: Cambiar el color del tipofuente por defacto .}
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeDefaultFontSize();
begin
  Font.Size := Manager.DefaultFontSize;
  { Goal: Change the default font color .}
  { Objetivo: Cambiar el color del tipofuente por defacto .}
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeFocusedColor();
begin
  Color := Manager.FocusedColor;
  { Goal: Change the focused color .}
  { Objetivo: Cambiar el color al enfocar .}
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeFocusedFontColor;
begin
  Font.Color := Manager.FocusedFontColor;
  { Goal: Change the enabled font .}
  { Objetivo: Cambiar el tipofuente habilitado .}
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeFocusedFontName();
begin
  Font.Name := Manager.FocusedFontName;
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeFocusedFontSize();
begin
  Font.Size := Manager.FocusedFontSize;
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeDisabledColor();
begin
  Color := Manager.DisabledColor;
  { Goal: Change the disabled color .}
  { Objetivo: Cambiar el color al deshabilitar .}
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeDisabledFontColor();
begin
  Font.Color := Manager.DisabledFontColor;
  { Goal: Change the disabled font .}
  { Objetivo: Cambiar el tipofuente deshabilitado .}
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeDisabledFontName();
begin
  Font.Name := Manager.DisabledFontName;
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeDisabledFontSize();
begin
  Font.Size := Manager.DisabledFontSize;
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeReadOnlyColor();
begin
  Color := Manager.ReadOnlyColor;
  { Goal: Change the readonly color .}
  { Objetivo: Cambiar el color de sololectura .}
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeReadOnlyFontColor();
begin
  Font.Color := Manager.ReadOnlyFontColor;
  { Goal: Change the readonly font .}
  { Objetivo: Cambiar el tipofuente de sololectura .}
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeReadOnlyFontName();
begin
  Font.Name := Manager.ReadOnlyFontName;
end;

procedure TCustomUMLCFocusMaskEdit.ConfirmedChangeReadOnlyFontSize();
begin
  Font.Size := Manager.ReadOnlyFontSize;
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeFocusedColor();
begin
  if ((Manager <> nil) and Manager.Enabled) then
  begin
    ConfirmedChangeFocusedColor();
  end;
  { Goal: Change the focused color .}
  { Objetivo: Cambiar el color para enfocado .}
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeFocusedFontColor();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeFocusedFontColor();
  end;
  { Goal: Change the focus font .}
  { Objetivo: Cambiar el tipofuente enfocado .}
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeFocusedFontName();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeFocusedFontName();
  end;
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeFocusedFontSize();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeFocusedFontSize();
  end;
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeDefaultColor();
begin
  if ((Manager <> nil) and Manager.Enabled) then
  begin
    ConfirmedChangeDefaultColor();
  end;
  { Goal: Change the non focused color .}
  { Objetivo: Cambiar el color para no enfocado .}
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeDefaultFontColor;
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDefaultFontColor();
  end;
  { Goal: Change the enabled font .}
  { Objetivo: Cambiar el tipofuente habilitado .}
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeDefaultFontName();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDefaultFontName();
  end;
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeDefaultFontSize();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDefaultFontSize();
  end;
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeDisabledColor();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDisabledColor();
  end;
  { Goal: Change the Disabled color .}
  { Objetivo: Cambiar el color para enfocado .}
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeDisabledFontColor();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeReadOnlyFontColor();
  end;
  { Goal: Change the disabled font .}
  { Objetivo: Cambiar el tipofuente deshabilitado .}
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeDisabledFontName();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDisabledFontName();
  end;
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeDisabledFontSize();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeDisabledFontSize();
  end;
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeReadOnlyColor();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeReadOnlyColor();
  end;
  { Goal: Change the ReadOnly color .}
  { Objetivo: Cambiar el color para SoloLectura .}
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeReadOnlyFontColor();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeReadOnlyFontColor();
  end;
  { Goal: Change the ReadOnly font .}
  { Objetivo: Cambiar el tipofuente para SoloLectura .}
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeReadOnlyFontName();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeReadOnlyFontName();
  end;
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeReadOnlyFontSize();
begin
  if ((Manager <> nil) and not Manager.Enabled) then
  begin
    ConfirmedChangeReadOnlyFontSize();
  end;
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeAutoSelect();
begin
  if ((Manager <> nil) and Manager.Enabled) then
  begin
    AutoSelect := Manager.AutoSelect;
  end;
  { Goal: Update the "AutoSelect" state .}
  { Objetivo: Actualiza el estado de "AutoSelect" .}
end;

procedure TCustomUMLCFocusMaskEdit.TryChangeCharCase();
begin
  if ((Manager <> nil) and Manager.Enabled) then
  begin
    CharCase := Manager.CharCase;
  end;
  { Goal: Update the "CharCase" state .}
  { Objetivo: Actualiza el estado de "CharCase" .}
end;

procedure TCustomUMLCFocusMaskEdit.TryClear;
begin
  Self.Font.Size  := DefaultFontSize();
  Self.Font.Name  := DefaultFontName();
  Self.Font.Color := DefaultForegroundColor();
  Self.Color      := DefaultBackgroundColor();
  Self.CharCase   := TEditCharCase.ecNormal;
  Self.AutoSelect := false;
end;

procedure TCustomUMLCFocusMaskEdit.TryRefresh();
begin
  if ((Manager <> nil) and Manager.Enabled) then
  begin
    if (Self.Enabled) then
    begin
      if (Self.ReadOnly) then
      begin
        ConfirmedChangeReadOnlyColor();
        ConfirmedChangeReadOnlyFontColor();
      end else
      begin
        if (Self.Focused) then
        begin
          ConfirmedChangeFocusedColor();
          ConfirmedChangeFocusedFontColor();
        end else
        begin
          ConfirmedChangeDefaultColor();
          ConfirmedChangeDefaultFontColor();
        end;
      end;
    end else
    begin
      ConfirmedChangeDisabledColor();
      ConfirmedChangeDisabledFontColor();
    end;
  end;

  TryChangeAutoSelect();
  TryChangeCharCase();
  { Goal: Update properties .}
  { Objetivo: Actualizar propiedades .}
end;

procedure TCustomUMLCFocusMaskEdit.Notification
 (AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (FManager <> nil) and (AComponent = Manager)
     then FManager := nil;
  { Goal: To detect & update when associated components ,}
  { have been removed .}

  { Objetivo: Detectar y actualizar cuando ,}
  { los componentes asociados se han removido .}
end;

procedure Prepare();
begin
  // ...
end;

procedure UnPrepare();
begin
  // ...
end;

initialization
  Prepare();
finalization
  Unprepare();
end.

