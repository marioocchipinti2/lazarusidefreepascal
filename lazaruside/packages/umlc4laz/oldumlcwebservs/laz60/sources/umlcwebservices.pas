(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcwebservices;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcnormobjects,
  umlclists,
  dummy;

type

(* TUMLCWebService *)

  TUMLCWebService = class;

(* TUMLCWebServiceParameter *)

  TUMLCWebServiceParameter = class(TUMLCHalfNormalizedObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

  end;

(* TUMLCWebServiceResult *)

  TUMLCWebServiceResult = class(TUMLCHalfNormalizedObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

  end;

(* TUMLCWebServiceMethod *)

  TUMLCWebServiceMethod = class(TUMLCHalfNormalizedObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    F_Parameters: TUMLCObjectList;

    F_Result:     TUMLCWebServiceResult;
  public
    (* public declarations *)

    constructor Create(); override;
    destructor Destroy(); override;
  public
    (* public declarations *)

  end;

(* TUMLCWebService *)

  TUMLCWebService = class(TUMLCHalfNormalizedObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    F_Methods: TUMLCObjectList;
  public
    (* public declarations *)

    constructor Create(); override;
    destructor Destroy(); override;
  public
    (* public declarations *)

  end;

implementation

(* TUMLCWebServiceParameter *)


(* TUMLCWebServiceResult *)


(* TUMLCWebServiceMethod *)

constructor TUMLCWebServiceMethod.Create();
begin
  inherited Create();

  Self.F_Parameters :=
    TUMLCObjectList.Create;
  Self.F_Result :=
    TUMLCWebServiceResult.Create;
end;

destructor TUMLCWebServiceMethod.Destroy();
begin
  Self.F_Result.Free;
  Self.F_Parameters.Free;

  inherited Destroy();
end;

(* TUMLCWebService *)

constructor TUMLCWebService.Create();
begin
  inherited Create();

  Self.F_Methods :=
    TUMLCObjectList.Create;
end;

destructor TUMLCWebService.Destroy();
begin
  Self.F_Methods.Free;

  inherited Destroy();
end;

end.

