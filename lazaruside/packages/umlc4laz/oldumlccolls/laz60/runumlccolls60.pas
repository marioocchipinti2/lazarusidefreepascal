{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit runumlccolls60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcstrconstlists, umlccollsenums, umlccollsiterators, umlcnormlists, 
  umlcnormstacks, umlcnormvectors, umlcnormvectorlists, umlcnormmatrices, 
  umlcnormarrays, umlcnormcubes;

implementation

end.
