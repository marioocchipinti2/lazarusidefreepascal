(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnormlists;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  dummy;

const

  // ---

  ID_IUMLCNormObjList : TUMLCType =
    ($43,$DD,$75,$5B,$B8,$71,$F3,$4F,$97,$D3,$B5,$DD,$A9,$D9,$13,$5D);


  // ---

  ID_TUMLCNormObjList : TUMLCType =
    ($6A,$BD,$DC,$F9,$EB,$1F,$D2,$4E,$83,$55,$61,$B0,$23,$65,$95,$26);

  // ---

type

  IUMLCNormObjList =
    interface(IUMLCHalfNormalizedObject)
    ['{43DD755B-B871-F34F-97D3-B5DDA9D9135D}']

    function IsEmpty(): Boolean;

    procedure Add
      (var AItem: TUMLCHalfNormalizedObject);

    procedure Remove();

    procedure Clear();

    function Extract
      (): TUMLCHalfNormalizedObject;
  end;

//(* TUMLCNormObjListIterator *)
//
//  TUMLCNormObjListIterator = (* forward *) class;

(* TUMLCNormObjList *)

  TUMLCNormObjList =
    class(TUMLCInternalCloneableObject)
  private
    (* private declarations *)

  protected
    (* protected declarations *)
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)

    function IsEmpty(): Boolean;

    procedure Add
      (var AItem: TUMLCHalfNormalizedObject);

    procedure Remove();

    procedure Clear();

    function Extract
      (): TUMLCHalfNormalizedObject;
  end;

implementation

(* TUMLCNormObjList *)

constructor TUMLCNormObjList.Create();
begin
  inherited Create();

  //Self.FIteratorCount := 0;
  //
  //Self.FBaseType := umlctypes.UnknownType;
end;

destructor TUMLCNormObjList.Destroy();
begin
  //Self.DoNothing();
  inherited Destroy();
end;

function TUMLCNormObjList.IsEmpty(): Boolean;
begin
  Result := false;
end;

procedure TUMLCNormObjList.Add
  (var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjList.Remove();
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjList.Clear();
begin
  //Self.DoNothing();
end;

function TUMLCNormObjList.Extract(): TUMLCHalfNormalizedObject;
begin
  Result := nil;
end;




end.

