(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstrconstlists;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcnormobjects,
  umlclists,
  dummy;

type

(* TUMLCStringConstantList *)

  TUMLCStringConstantList = class(TUMLCHalfNormalizedObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

  end;


implementation

end.

