(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnormcubes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  umlcnormarrays,
  dummy;

const

  // ---

  ID_IUMLCNormObjCube : TUMLCType =
    ($C4,$91,$E8,$C4,$62,$93,$25,$4B,$96,$A7,$72,$4F,$CC,$64,$E6,$62);

  ID_TUMLCNormObjCube : TUMLCType =
    ($D7,$72,$7F,$28,$F3,$37,$AC,$43,$A0,$BE,$C3,$24,$A7,$D3,$02,$85);

  // ---


type

  IUMLCNormObjCube =
    interface(
      IUMLCHalfNormalizedObject
      // , IUMLCNormObjArray
    )
    ['{C491E8C4-6293-254B-96A7-724FCC64E662}']

    function readDimCount(): LongInt;

    function readFullCount(): LongInt;

    // ---

    function IsValidIndex
      (const ACol, ARow, ADepth: LongInt): Boolean;

    function readColSize(): LongInt;

    function readRowSize(): LongInt;

    function readDepthSize(): LongInt;

    function getAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       const ADepthIndex: LongInt): TUMLCHalfNormalizedObject;

    procedure setAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       const ADepthIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure DeleteAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       const ADepthIndex: LongInt);

    procedure ExtractAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       const ADepthIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure Clear();
  end;

type

//(* TUMLCNormObjCubeIterator *)
//
//  TUMLCNormObjCubeIterator = (* forward *) class;

(* TUMLCNormObjCube *)

  TUMLCNormObjCube =
    class(TUMLCInternalCloneableObject, IUMLCNormObjCube)
  private
    (* private declarations *)

  protected
    (* protected declarations *)

    FAssignedColSize: Boolean;
    FAssignedRowSize: Boolean;
    FAssignedDepthSize: Boolean;

    FColSize: LongInt;
    FRowSize: LongInt;
    FDepthSize: LongInt;
  public
    (* public declarations *)

    constructor Create(); override;
//
//    constructor CreateBySize
//      (const AColSize: LongInt;
//       const ARowSize: LongInt); virtual;

    destructor Destroy(); override;
  public
    (* public declarations *)

    function readDimCount(): LongInt;

    function readFullCount(): LongInt;
  public
    (* public declarations *)

    function IsValidIndex
      (const ACol, ARow, ADepth: LongInt): Boolean;
  public
    (* public declarations *)

    function readColSize(): LongInt;

    function readRowSize(): LongInt;

    function readDepthSize(): LongInt;
  public
    (* public declarations *)

    function getAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       const ADepthIndex: LongInt): TUMLCHalfNormalizedObject;

    procedure setAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       const ADepthIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);
  public
    (* public declarations *)

    procedure DeleteAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       const ADepthIndex: LongInt);

    procedure ExtractAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       const ADepthIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure Clear();
  end;

implementation

(* TUMLCNormObjCube *)

constructor TUMLCNormObjCube.Create();
begin
  inherited Create();

  //Self.FAssignedColSize := false;
  //Self.FColSize := 0;
  //Self.FAssignedRowSize := false;
  //Self.FRowSize := 0;
end;

destructor TUMLCNormObjCube.Destroy();
begin
  //Self.FRowSize := ARowSize;
  inherited Destroy();
end;

function TUMLCNormObjCube.readDimCount(): LongInt;
begin
  Result := 3;
end;

function TUMLCNormObjCube.readFullCount(): LongInt;
begin
  Result := 0;
end;

function TUMLCNormObjCube.IsValidIndex
  (const ACol, ARow, ADepth: LongInt): Boolean;
begin
  Result := false;
end;

function TUMLCNormObjCube.readColSize(): LongInt;
begin
  Result:=
    Self.FColSize;
end;

function TUMLCNormObjCube.readRowSize(): LongInt;
begin
  Result:=
    Self.FRowSize;
end;

function TUMLCNormObjCube.readDepthSize(): LongInt;
begin
  Result:=
    Self.FRowSize;
end;

function TUMLCNormObjCube.getAt
  (const AColIndex: LongInt;
   const ARowIndex: LongInt;
   const ADepthIndex: LongInt): TUMLCHalfNormalizedObject;
begin
  Result:=
    nil;
end;

procedure TUMLCNormObjCube.setAt
  (const AColIndex: LongInt;
   const ARowIndex: LongInt;
   const ADepthIndex: LongInt;
   var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjCube.DeleteAt
  (const AColIndex: LongInt;
   const ARowIndex: LongInt;
   const ADepthIndex: LongInt);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjCube.ExtractAt
  (const AColIndex: LongInt;
   const ARowIndex: LongInt;
   const ADepthIndex: LongInt;
   var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjCube.Clear();
begin
  //Self.DoNothing();
end;


end.

