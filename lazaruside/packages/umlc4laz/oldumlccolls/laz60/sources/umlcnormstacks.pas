(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnormstacks;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  dummy;

const

  // ---

  ID_IUMLCNormObjStack : TUMLCType =
  ($27,$47,$52,$AB,$F3,$CF,$75,$40,$AF,$22,$0E,$F4,$55,$B4,$2F,$AD);

  ID_TUMLCNormObjStack : TUMLCType =
    ($2E,$A8,$FF,$4C,$32,$00,$11,$4E,$A3,$5E,$DA,$AC,$F2,$5D,$34,$69);

  // ---

type

  IUMLCNormObjStack =
    interface(IUMLCHalfNormalizedObject)
    ['{274752AB-F3CF-7540-AF22-0EF455B42FAD}']

    procedure Push
      (var AItem: TUMLCHalfNormalizedObject);

    procedure Pop
      (var AItem: TUMLCHalfNormalizedObject);

    procedure Delete();

    procedure Clear();
  end;

//(* TUMLCNormObjStackIterator *)
//
//  TUMLCNormObjStackIterator = (* forward *) class;

(* TUMLCNormObjStack *)

  TUMLCNormObjStack =
    class(TUMLCInternalCloneableObject, IUMLCNormObjStack)
  private
    (* private declarations *)

  protected
    (* protected declarations *)
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)

    procedure Push
      (var AItem: TUMLCHalfNormalizedObject);

    procedure Pop
      (var AItem: TUMLCHalfNormalizedObject);

    procedure Delete();

    procedure Clear();
  end;

implementation

(* TUMLCNormObjStack *)

constructor TUMLCNormObjStack.Create();
begin
  inherited Create();

  //Self.FIteratorCount := 0;
  //
  //Self.FBaseType := umlctypes.UnknownType;
end;

destructor TUMLCNormObjStack.Destroy();
begin
  //Self.DoNothing();
  inherited Destroy();
end;

procedure TUMLCNormObjStack.Push
  (var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjStack.Pop
  (var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjStack.Delete();
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjStack.Clear();
begin
  //Self.DoNothing();
end;





end.

