(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnormvectors;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  umlcnormarrays,
  dummy;

const

  // ---

  ID_IUMLCNormObjVector : TUMLCType =
    ($A1,$0B,$D1,$B3,$B2,$B8,$1F,$4A,$B5,$4E,$D7,$DC,$F7,$BB,$22,$34);

  ID_TUMLCNormObjVector : TUMLCType =
    ($B9,$98,$F3,$23,$89,$6F,$C3,$42,$A7,$48,$D9,$C1,$AE,$94,$60,$E5);


  // ---

type

  IUMLCNormObjVector =
    interface(
      IUMLCHalfNormalizedObject
      // , IUMLCNormObjArray
    )
    ['{A10BD1B3-B2B8-1F4A-B54E-D7DCF7BB2234}']

    function readDimCount(): LongInt;

    function readFullCount(): LongInt;

    // ---

    function IsValidIndex
      (const AIndex: LongInt): Boolean;

    function readSize(): LongInt;

    function getAt
      (const AIndex: LongInt): TUMLCHalfNormalizedObject;

    procedure setAt
      (const AIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure DeleteAt
      (const AIndex: LongInt);

    procedure ExtractAt
      (const AIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure Clear();
  end;

type

//(* TUMLCNormObjVectorIterator *)
//
//  TUMLCNormObjVectorIterator = (* forward *) class;

(* TUMLCNormObjVector *)

  TUMLCNormObjVector =
    class(TUMLCInternalCloneableObject, IUMLCNormObjVector)
  private
    (* private declarations *)

  protected
    (* protected declarations *)

    FAssignedSize: Boolean;

    FSize: LongInt;
  public
    (* public declarations *)

    constructor Create(); override;

    constructor CreateByCount
      (const ACount: LongInt); virtual;

    destructor Destroy(); override;
  public
    (* public declarations *)

    function readDimCount(): LongInt;

    function readFullCount(): LongInt;
  public
    (* public declarations *)

    procedure AssignSize
      (const ASize: LongInt);

    function IsValidIndex
      (const AIndex: LongInt): Boolean;
  public
    (* public declarations *)

    function readSize(): LongInt;
  public
    (* public declarations *)

    function getAt
      (const AIndex: LongInt): TUMLCHalfNormalizedObject;

    procedure setAt
      (const AIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);
  public
    (* public declarations *)

    procedure DeleteAt
      (const AIndex: LongInt);

    procedure ExtractAt
      (const AIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure Clear();
  end;



implementation

(* TUMLCNormObjVector *)

constructor TUMLCNormObjVector.Create();
begin
  inherited Create();

  Self.FAssignedSize := false;
  Self.FSize := 0;
end;

constructor TUMLCNormObjVector.CreateByCount
  (const ACount: LongInt);
begin
  inherited Create();

  Self.FAssignedSize :=
    (ACount > 0);
  if(Self.FAssignedSize) then
  begin
    Self.FSize := ACount;
  end;
end;

destructor TUMLCNormObjVector.Destroy();
begin
  inherited Destroy();
end;

function TUMLCNormObjVector.readDimCount(): LongInt;
begin
  Result := 1;
end;

function TUMLCNormObjVector.readFullCount(): LongInt;
begin
  Result := 0;
end;

procedure TUMLCNormObjVector.AssignSize
  (const ASize: LongInt);
begin
  if (not Self.FAssignedSize) then
  begin
    Self.FSize := ASize;
    Self.FAssignedSize := true;
  end;
end;

function TUMLCNormObjVector.IsValidIndex
  (const AIndex: LongInt): Boolean;
begin
  Result:= false;
end;

function TUMLCNormObjVector.readSize(): LongInt;
begin
  Result:=
    Self.FSize;
end;

function TUMLCNormObjVector.getAt
  (const AIndex: LongInt): TUMLCHalfNormalizedObject;
begin
  Result:= nil;
end;

procedure TUMLCNormObjVector.setAt
  (const AIndex: LongInt;
   var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjVector.DeleteAt
  (const AIndex: LongInt);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjVector.ExtractAt
  (const AIndex: LongInt;
   var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjVector.Clear();
begin
  //Self.DoNothing();
end;




end.

