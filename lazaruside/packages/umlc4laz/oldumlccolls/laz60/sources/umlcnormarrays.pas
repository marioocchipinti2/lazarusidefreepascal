(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnormarrays;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  dummy;

 const

   // ---

   ID_IUMLCNormObjArray : TUMLCType =
    ($BB,$D1,$A8,$10,$AA,$0B,$5A,$4E,$93,$3E,$98,$FD,$84,$9E,$59,$01);

   // ---


type

  IUMLCNormObjArray =
    interface(
      IUMLCHalfNormalizedObject
    )
    ['{BBD1A810-AA0B-5A4E-933E-98FD849E5901}']

    function readDimCount(): LongInt;

    function readFullCount(): LongInt;
  end;


implementation




end.

